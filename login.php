<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <?
    // include "header.php";
    $err = $_GET["err"];
    ?>
    <div class="container mt-5">
        <form id="formulario" class="login">
            <h1 style="text-align: center; color: #ee171e;">Login Propytex</h1>
            <div class="col-md-4 offset-md-4 pt-5">
                <div class="login-card-light p-3 shadow-lg rounded">
                    <div class="pt-3">
                        <h2 class="text-danger">Acceso Propytex</h2>
                    </div>

                    <form class="mt-5">
                        <div class="form-group">
                            <input type="text" name="nombre" id="nombre" placeholder="Correo" onfocus="cerrarError()" class="form-control form-control-sm">
                        </div>

                        <div class="form-group">
                            <input type="password" name="pass" id="pass" placeholder="Contraseña" onfocus="cerrarError()" class="form-control form-control-sm">
                        </div>

                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="rememberCheckBox">
                            <label class="form-check-label text-dark" for="rememberCheckBox">Recuérdame</label>
                        </div>

                        <div id="error">
                            <?
                            if($err == 11){
                                $err = 0;?></br>
                                <div  class="alert alert-danger" role="alert" id="alert">
                                    <label>Error: Sesión expirada </label>     
                                </div>
                            <?}else{} ?>
                        </div>

                        <div class="mt-5">
                            <button class="btn btn-sm btn-danger col" type="submit" id="submit">
                                Iniciar Sesión
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </form>
    </div>

    <style>
    .login-card-light {
        background: linear-gradient(145deg, #eee, #fff);
    }

    body {
        font-size: 13px;
    }
    </style>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js">
    </script>
    <script src="js/bootstrap.min.js">
    </script>
    <script src="js/app.js">
    </script>

    <script>
    $(document).ready(function(){
        var height = $(window).height();
        $('#leftSideImg').height(height);
    });

    function cerrarError(){
        document.getElementById("error").innerHTML="";
    }
    </script>
    
</body>
</html>