<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico.png">
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon.ico.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://unpkg.com/bootstrap-css-only@4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/@fortawesome/fontawesome-free@5.13.0/css/all.css" rel="stylesheet">
    <link href="https://unpkg.com/angular-calendar@0.28.16/css/angular-calendar.css" rel="stylesheet">
    <link href="https://unpkg.com/flatpickr@4.6.3/dist/flatpickr.css" rel="stylesheet">
</head>

<body>
    <style>
        a {
            color: #ee171e;
        }
        
        li>a.nav-link {
            font-family: 'Poppins', sans-serif;
            color: rgb(51, 51, 51);
            font-weight: 400;
            font-size: 18px;
            margin: 0px 10px;
        }
        
        a.nav-link:hover {
            transform: 1s linear;
            font-weight: 500;
        }
        
        .active>.nav-link {
            font-weight: 900;
            color: #EE171E;
        }
        
        .bg-redflip {
            background-color: #ee171e!important;
            -webkit-box-shadow: 0px 3px 10px 3px rgba(0, 0, 0, 0.45);
            -moz-box-shadow: 0px 3px 10px 3px rgba(0, 0, 0, 0.45);
            box-shadow: 0px 3px 10px 3px rgba(0, 0, 0, 0.45);
        }
        
        .sticky-top {
            top: -1px!important;
        }
        
        ul>li>a.blanco {
            color: white!important;
        }
        
        #logo {
            width: 120px;
        }
        
        @media (max-width:992px) {
            #logo {
                width: 100px;
            }
        }
        /* FOOTER */
        
        footer {
            background-color: #434343;
            margin-top: -50px;
            padding-bottom: 10px;
        }
        
        .h4-title {
            color: #ee171e;
            margin: 10px 0;
        }
        
        .pt-25 {
            padding-top: 16px;
            margin-bottom: 10px;
            color: white;
            font-size: 22px;
        }
        
        .p-footer {
            color: #e0e0e0;
            font-size: 20px;
            font-family: 'Catamaran', sans-serif;
            /* text-align: justify; */
        }
        
        .footer-a {
            color: white;
            font-family: 'Catamaran', sans-serif;
            font-weight: 100;
            font-size: 18px;
        }
        
        .footer-c {
            color: white;
            font-family: 'Catamaran', sans-serif;
            font-weight: 400;
            font-size: 16px;
        }
        
        .rrss-icon {
            margin: 10px;
        }
        
        a.rrss {
            color: black;
            text-decoration: none;
            margin-right: 30px;
        }
        
        .png-inline--fa {
            zoom: 0;
            transition: transform 1s;
        }
        
        .png-inline--fa:hover {
            transform: scale(1.1);
        }
        
        .fa-facebook-square:hover {
            color: #3b5998;
            transition: all .4s ease-in-out;
        }
        
        .fa-linkedin:hover {
            color: #2867B2;
            transition: all .4s ease-in-out;
        }
        
        .fa-instagram-square:hover {
            color: #cd486b;
            /* background-image: url(assets/img/insta.png);
    background-size: 27.99px;
    background-repeat: no-repeat; */
            transition: .4s ease-in-out;
        }
        
        .footer-a:hover {
            color: #ee171e;
            -webkit-transition: .4s ease;
            transition: .4s ease;
        }
        
        .img-footer {
            background-image: url(assets/img/footer.png);
            background-repeat: no-repeat;
            background-size: cover;
            height: 250px;
            margin-top: -300px;
        }
        
        .img-logo-spons {
            width: 150px;
        }
        
        .pl-200 {
            padding-left: 200px;
        }
        
        @media only screen and (min-width:575px) and (max-width:992px) {
            .img-footer {
                margin-top: -200px;
            }
        }
        
        @media (max-width:576px) {
            .img-footer {
                margin-top: -210px;
            }
            .li-subA {
                line-height: 30px;
            }
        }
        
        section#reserva,
        section#informacion {
            margin-bottom: 300px;
        }
        
        .container-footer {
            padding: 0 10%!important;
            padding-top: 100px!important;
        }
        
        .img-footer {
            background-image: url(http://redflip.net/contenido-redflip/assets/img/footer.png);
            background-repeat: no-repeat;
            background-size: cover;
            height: 250px;
            margin-top: -300px;
        }
        
        .bgR-redflip {
            background-color: #ee171e;
        }
        /* Fin footer */
        
        .far {
            color: red;
        }
        
        .btn-home {
            font-family: 'Poppins', sans-serif;
            font-size: 16px;
            margin: 0px 20px 0px 0px;
            padding: 10px 30px;
            border-radius: 20px;
            -webkit-box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.45);
            -moz-box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.45);
            box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.45);
        }
        
        .btn-primaryR {
            background-color: #ee171e;
            padding: 10px 55px;
            color: #fff!important;
        }
        
        .btn-primaryR:hover {
            background-color: #da151b;
            color: #fff;
        }
        
        .btn-secondaryR {
            background-color: rgb(211, 211, 211);
        }
        
        .btn-secondaryR:hover {
            background-color: #c2c2c2;
        }
        
        .card-title {
            text-align: center;
        }
        
        .list-group-item {
            text-align: center;
        }
        
        .label-home {
            font-size: 16px;
        }
        
        .span-home {
            font-size: 18px;
            font-family: 'Catamaran', sans-serif;
            font-weight: 900;
            /* width: 70%; */
            float: left;
            margin: 15px 0;
        }
        
        .span-home-button {
            font-size: 18px;
            font-family: 'Catamaran', sans-serif;
            font-weight: 900;
            /* width: 70%; */
            float: left;
            margin: 15px 0;
        }
        
        .list-group-item {
            border-bottom: 2px dashed #6ec3be!important;
        }
        
        .card-title {
            font-size: 28px;
        }
        /* Media Hell */
        
        @media only screen and (min-width:768px) and (max-width:991px) {
            .card-img-top {
                width: 70%;
                margin: 0 auto;
                height: 70vh;
            }
        }
        
        @media only screen and (min-width:768px) and (max-width:992px) {
            .span-home {
                width: 70%;
            }
            .label-home {
                font-size: 23px;
                font-weight: 600;
            }
            .header-home {
                margin-top: 0px !important;
            }
            .btn-home {
                font-size: 15px;
                padding: 10px 10px;
            }
            .btn-primaryR {
                padding: 10px 34px;
            }
        }
        
        @media only screen and (min-width:540px) and (max-width:767px) {
            .h1-home {
                text-align: center;
            }
            .span-home {
                width: 70%;
            }
            .label-home {
                font-size: 18px;
                font-weight: 600;
            }
            .header-home {
                margin-top: 0px !important;
                margin-bottom: 100px;
            }
            .btn-home {
                font-size: 12px;
                padding: 10px 5px;
                margin: 0px 0px 0px 0px;
            }
            .btn-primaryR {
                padding: 10px 25px;
            }
            .img {
                width: 100%;
                position: absolute;
                margin-top: 40px;
            }
        }
        
        @media only screen and (min-width:400px) and (max-width:539px) {
            .h1-home {
                text-align: center;
            }
            .span-home {
                width: 100%;
            }
            .label-home {
                font-size: 16px;
                font-weight: 600;
            }
            .header-home {
                margin-top: 0px !important;
                margin-bottom: 50px;
            }
            .btn-home {
                font-size: 12px;
                padding: 10px 5px;
                margin: 0px 0px 0px 0px;
            }
            .btn-primaryR {
                padding: 10px 25px;
                margin-bottom: 10px;
            }
            .img {
                width: 100%;
                position: absolute;
                margin-top: 40px;
            }
        }
        
        @media only screen and (min-width:330px) and (max-width:399px) {
            .h1-home {
                text-align: center;
            }
            .span-home {
                width: 100%;
            }
            .label-home {
                font-size: 16px;
                font-weight: 600;
            }
            .header-home {
                margin-top: 0px !important;
                margin-bottom: 50px;
                padding-left: 20px;
            }
            .btn-home {
                font-size: 12px;
                padding: 10px 5px;
                margin: 0px 0px 0px 0px;
            }
            .btn-primaryR {
                padding: 10px 25px;
                margin-bottom: 10px;
            }
            .img {
                margin-top: 20px;
                width: 130px !important;
                height: 220px;
            }
        }
        
        @media only screen and (min-width:300px) and (max-width:329px) {
            .h1-home {
                text-align: center;
                font-size: 35px;
            }
            .span-home {
                width: 100%;
            }
            .label-home {
                font-size: 14px;
                font-weight: 600;
            }
            .header-home {
                margin-top: 0px !important;
                margin-bottom: 50px;
                padding-left: 20px;
            }
            .btn-home {
                font-size: 11px;
                padding: 10px 5px;
                margin: 0px 0px 0px 0px;
            }
            .btn-primaryR {
                padding: 10px 20px;
                margin-bottom: 10px;
            }
            .img {
                margin-top: 20px;
                width: 120px !important;
                height: 180px;
            }
        }
        
        @media only screen and (min-width:280px) and (max-width:299px) {
            .h1-home {
                text-align: center;
                font-size: 35px;
            }
            .span-home {
                width: 100%;
            }
            .label-home {
                font-size: 12px;
                font-weight: 600;
            }
            .header-home {
                margin-top: 0px !important;
                margin-bottom: 50px;
                padding-left: 25px;
            }
            .btn-home {
                font-size: 11px;
                padding: 10px 5px;
                margin: 0px 0px 0px 0px;
            }
            .btn-primaryR {
                padding: 10px 20px;
                margin-bottom: 10px;
            }
            .img {
                margin-top: 20px;
                width: 120px !important;
                height: 180px;
            }
        }
    </style>
    <nav id="navBar" class="navbar navbar-expand-lg navbar-light bg-light section-header" (scroll)="onWindowScroll($event);">
        <a class="navbar-brand" href="#">
            <img src="http://www.redflip.net/contenido-redflip/assets/img/Logo-Redflip-Sin-Borde.png" alt="Logo Redflip" id="logo">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
</button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href=""> Inicio
                </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://www.redflip.com"> Encuentra tu hogar
                </a>
                </li>
                <li class="nav-item">
                    <!-- <a class="nav-link" style="color:white !important;" href="index.php"> Iniciar Sesión -->
                    <a class="btn btn-outline-danger" href="index.php">Iniciar Sesión 
                    </a>
                </li>

            </ul>
        </div>
    </nav>

    <!-- Section home desktop -->
    <section id="home" class="d-none d-sm-none d-md-block d-lg-block d-xl-block">
        <div class="container-fluid container-home">
            <div class="row bg-home">
                <div class="col-4">
                    <img class="img" src="http://www.redflip.net/contenido-redflip/assets/img/familyflip.png" alt="" style="width: 100%">
                </div>
                <div class="col-8 header-home" style="margin-top: 15vh">
                    <h1 class="h1-home">Intranet Redflip </h1>
                    <span class="span-home">
                    <label class="label-container">
                    <i class="far fa-check-circle"></i><label class="label-home pl-1 pr-2"> Mas Oportunidades</label>
                    </label>
                    <label class="label-container">
                    <i class="far fa-check-circle"></i><label class="label-home pl-1 pr-2"> Mejor Tecnología</label>
                    </label>
                    <label class="label-container">
                    <i class="far fa-check-circle"></i><label class="label-home pl-1 pr-2"> Horarios Flexibles </label>
                </label>
                </span>
                    <span class="span-home-button">
                    <a href="https://redflip.net/contenido-redflip/index.html#/trabaja-con-nosotros" class="btn btn-primaryR btn-home">Trabaja con nosotros</a>
                    <a href="https://redflip.net/contenido-redflip/index.html#/home" class="btn btn-secondaryR btn-home">¿Buscas tu próximo hogar?</a>
                </span>
                </div>
            </div>
        </div>
    </section>
    <!-- section home movil -->
    <section id="home" class="d-block d-sm-block d-md-none d-lg-none d-xl-none ">
        <div class="container-fluid container-home">
            <div class="row bg-home">
                <div class="col-12">
                    <h1 class="h1-home">Intranet Redflip </h1>
                </div>
                <div class="col-4">
                    <img class="img" src="http://www.redflip.net/contenido-redflip/assets/img/familyflip.png" alt="" style="width: 100%">
                </div>
                <div class="col-8 header-home" style="margin-top: 15vh">

                    <span class="span-home">
                <label class="label-container">
                <i class="far fa-check-circle"></i><label class="label-home pl-1 pr-2"> Mas Oportunidades</label>
                </label>
                <label class="label-container">
                <i class="far fa-check-circle"></i><label class="label-home pl-1 pr-2"> Mejor Tecnología</label>
                </label>
                <label class="label-container">
                <i class="far fa-check-circle"></i><label class="label-home pl-1 pr-2"> Horarios Flexibles </label>
            </label>
            </span>
                    <span class="span-home-button">
                <a href="https://redflip.net/contenido-redflip/index.html#/trabaja-con-nosotros" class="btn btn-primaryR btn-home">Trabaja con nosotros</a>
                <a href="https://redflip.net/contenido-redflip/index.html#/home" class="btn btn-secondaryR btn-home">¿Buscas tu próximo hogar?</a>
            </span>
                </div>
            </div>
        </div>
    </section>
    <!-- fin section home movil -->

    <section>
        <div class="contaniner mt-5">
            <div class="row m-0">
                <div class="col-12" style="text-align: center">
                    <h2>Intranet Redflip</h2>
                </div>
            </div>
            <div class="row m-0">
                <div class="col-12 mt-3">
                    <p style="text-align: center; width:60%; margin-left: 22%">
                        Esta intranet está dirigida hacia los corredores que pertenecen a Redflip, la cual utilizará información proporcionada por la misma empresa, con el objetivo de mejorar la comunicación entre corredor y el cliente, ya sea interesado y/o propietario, accediendo
                        al correo corporativo para componer y enviar información detallada de las propiedades de nuestra base de datos.
                    </p>
                </div>
            </div>

        </div>

    </section>
    <section id="about" class="pb-5">
        <div class="container" style="margin-bottom: 45%;">
            <img src="assets/img/fondoR1.png" alt="" class="img-about">
            <div class="row about-home mt-5">
                <div class="col-12">
                    <h2 class="h2-title" style="text-align: center">¿Por qué elegir Redflip?</h2>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="card" style="width: auto; border:none;">
                        <img src="http://www.redflip.net/contenido-redflip/assets/img/corredor.png" class="card-img-top" alt="Corredor">
                        <div class="card-body">
                            <h5 class="card-title">Integraciones API's</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Otorgaremos a todos nuestros corredores un correo corporativo </li>
                                <li class="list-group-item" style="border: none !important;">Entregaremos una plantilla que ayudará a nuestros corredores a contactar más clientes</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="card" style="width: auto; border:none;">
                        <img src="http://www.redflip.net/contenido-redflip/assets/img/ahorro-comision.png" class="card-img-top" alt="Ahorro comisión">
                        <div class="card-body">
                            <h5 class="card-title">Gestión de contactos e interesados</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item ">Organiza tu cartera de interesados y genera una respuesta rápida a tus clientes</li>
                                <li class="list-group-item " style="border: none !important;">Te ofrecemos capacitaciones para que puedas seguir creciendo como corredor</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="card" style="width: auto; border:none;">
                        <img src="http://www.redflip.net/contenido-redflip/assets/img/tour-vitrual-3d.png" class="card-img-top" alt="Tour Virtual 3D">
                        <div class="card-body">
                            <h5 class="card-title">Horarios flexibles</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Tu manejas tu propio horario</li>
                                <li class="list-group-item" style="border: none !important;">El unico principio: Dar excelente servicio a tus clientes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="img-footer"></div>
    <!-- Footer -->
    <footer class="page-footer font-small unique-color-dark">


        <!-- Footer Links -->
        <div class="container text-center text-md-left mt-5 col-12 container-footer">

            <!-- Grid row -->
            <div class="row mt-3">

                <!-- Grid column -->
                <div class="col-md-6 col-lg-3 col-xl-3 mx-auto mb-4">

                    <!-- Content -->
                    <h6 class="text-uppercase font-weight-bold">
                        <img src="http://redflip.net/contenido-redflip/assets/img/Logo-Redflip-Sin-Borde-blanco.png" alt="" width="150px">
                    </h6>
                    <hr class="bgR-redflip accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <p class="p-footer">Redflip® es la primera corredora de propiedades en Chile en base a COMISIÓN FIJA independiente del valor de venta o arriendo de la propiedad.</p>

                    <h6 class="text-uppercase pt-25">Con el apoyo de</h6>
                    <hr class="bgR-redflip accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <div class="container col-12">
                        <div class="row">
                            <div class="col-12">
                                <img src="http://redflip.net/contenido-redflip/assets/img/startup-logo.png" alt="" class="img-logo-spons">
                            </div>
                            <br><br>
                            <div class="col-12">
                                <img src="http://redflip.net/contenido-redflip/assets/img/corfo-logo.png" alt="" class="img-logo-spons">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-6 col-lg-3 col-xl-3 mx-auto mb-4">

                    <!-- Links -->
                    <h6 class="text-uppercase pt-25">Buscar por comuna</h6>
                    <hr class="bgR-redflip accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <br>
                    <div class="container col-12">
                        <div class="row">
                            <div class="col-6">
                                <a href="https://redflip.com/area/nunoa/" class="footer-a">Ñuñoa</a><br>
                                <a href="https://redflip.com/area/chicureo/" class="footer-a">Chicureo</a><br>
                                <a href="https://redflip.com/area/la-florida/" class="footer-a">La Florida</a><br>
                                <a href="https://redflip.com/area/la-reina/" class="footer-a">La Reina</a><br>
                                <a href="https://redflip.com/area/las-condes/" class="footer-a">Las Condes</a><br>
                                <a href="https://redflip.com/area/lo-barnechea/" class="footer-a">Lo Barnechea</a><br>
                                <a href="https://redflip.com/area/maipu/" class="footer-a">Maipú</a>
                            </div>
                            <div class="col-6">
                                <a href="https://redflip.com/area/penalolen/" class="footer-a">Peñalolén</a><br>
                                <a href="https://redflip.com/area/providencia/" class="footer-a">Providencia</a><br>
                                <a href="https://redflip.com/area/pucon/" class="footer-a">Pucón</a><br>
                                <a href="https://redflip.com/area/san-miguel/" class="footer-a">San Miguel</a><br>
                                <a href="https://redflip.com/area/santiago/" class="footer-a">Santiago</a><br>
                                <a href="https://redflip.com/area/vitacura/" class="footer-a">Vitacura</a>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-6 col-lg-3 col-xl-3 mx-auto mb-4">

                    <!-- Links -->
                    <h6 class="text-uppercase pt-25">Buscar por tipo</h6>
                    <hr class="bgR-redflip accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <br>
                    <div class="container col-12">
                        <div class="row">
                            <div class="col-6">
                                <a href="https://redflip.com/listings/casa/" class="footer-a">Casa</a><br>
                                <a href="https://redflip.com/listings/comercial/" class="footer-a">Comercial</a><br>
                                <a href="https://redflip.com/listings/departamento/" class="footer-a">Departamento</a><br>
                            </div>
                            <div class="col-6">
                                <a href="https://redflip.com/listings/oficina/" class="footer-a">Oficina</a><br>
                                <a href="https://redflip.com/listings/terreno/" class="footer-a">Terreno</a><br>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-6 col-lg-3 col-xl-2 mx-auto mb-md-0 mb-4">

                    <!-- Links -->
                    <h6 class="text-uppercase pt-25">Contacto</h6>
                    <hr class="bgR-redflip accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <div class="container col-12">
                        <div class="row">
                            <div class="col-12">
                                <a href="tel:+56993509559" class="footer-a">+56 9 9350 9559</a><br>
                                <a href="tel:+56992235580" class="footer-a">+56 9 9223 5580</a><br>
                                <a href="mailto:contacto@redflip.com" class="footer-a">contacto@redflip.com</a>
                            </div>
                        </div>
                    </div>

                    <h6 class="text-uppercase pt-25">Redes sociales</h6>
                    <hr class="bgR-redflip accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <div class="container col-12">
                        <div class="row">
                            <div class="col-12">
                                <a class="rrss" href="https://www.facebook.com/redflip.cl/">
                                    <i class="fab fa-facebook-square fa-2x"></i>
                                </a>
                                <a class="rrss" href="https://www.linkedin.com/company/redflip">
                                    <i class="fab fa-linkedin fa-2x"></i>
                                </a>

                                <a class="rrss" href="https://www.instagram.com/redflip_cl/">
                                    <i class="fab fa-instagram-square fa-2x"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <a class="footer-c" href="https://redflip.net/contenido-redflip/index.html#/politicas">Política de Privacidad</a>
                            </div>
                            <div class="col-12">
                                <a class="footer-c" href="https://redflip.net/contenido-redflip/index.html#/terminos">Términos y Condiciones</a>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Grid column -->
            </div>
            <!-- Grid row -->
        </div>
        <!-- Footer Links -->
        <!-- Copyright -->

        <div class="container-fluid footer-copyright center py-3">
            <div class="row">
                <div class="col-12" style="text-align: center;">
                    <a class="footer-a" href="#">© 2020 Todos los derechos reservados</a>
                </div>
            </div>
        </div>
        <!-- Copyright -->


    </footer>
    <!-- Footer -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script src="../js/all.min.js"></script>
    <script src="../js/menu.js"></script>
    <script src="../js/notificacion.js"></script>
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script src="../js/push.js"></script>
</body>

</html>