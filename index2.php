<!DOCTYPE html>
<html lang="es-cl">
    <head>
        <!--=== Required meta tags ===-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!--=== Title ===-->
        <title>Iniciar sesión</title>
        <!--=== Favicon Icon ===-->
        <link rel="shortcut icon" type="image/png" href="#">
        <!--=== Bootstrap CSS ===-->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <!--=== Font Awesome ===-->
        <!--=== Style CSS ===-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <?
        // include "header.php";
        $err = $_GET["err"];
        ?>

		

		<!--=== JS ===-->
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- <script src="js/main-login.js"></script> -->
        <script src="js/app.js"></script>

        <script>
        $(document).ready(function(){
            var height = $(window).height();
            $('#leftSideImg').height(height);
        });

        function cerrarError(){
            document.getElementById("error").innerHTML="";
        }

        

        </script>
    </body>
</html>