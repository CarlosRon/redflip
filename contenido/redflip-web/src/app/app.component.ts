import { Component } from '@angular/core';
import { ContactoService } from './components/shared/contacto/contacto.service';
// import { faFacebook } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'redflip-web';
}
