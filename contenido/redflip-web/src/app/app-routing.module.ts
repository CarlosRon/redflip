import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { VdeoarrComponent } from './components/vdeoarr/vdeoarr.component';
// import { Module } from 'path-to-source';



const ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'vdeoarr', component: VdeoarrComponent },
  // { path: 'routePath', component: Component },
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
