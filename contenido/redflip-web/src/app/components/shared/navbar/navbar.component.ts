import { Component } from '@angular/core';
import { HostListener } from '@angular/core';
// import { htmlToElement, ClassNameInput } from '../../../../../../../js/fullcalendar/core/main';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent {

  constructor() { }

  @HostListener('window:scroll', ['$event'])

  onWindowScroll(e) {
    const element = document.querySelector('.navbar');
    const logo = (document.getElementById('logo') as HTMLImageElement);
    const navColor = document.querySelectorAll('.nav-link');
    if (window.pageYOffset > element.clientHeight) {
      element.classList.remove('bg-transparent');
      element.classList.add('bg-redflip');
      logo.src = 'assets/img/Logo-Redflip-Sin-Borde-blanco.png';
      for(let i = 0; i<navColor.length; i++){
        navColor[i].classList.add('blanco');
      }
    } else {
      element.classList.remove('bg-redflip');
      element.classList.add('bg-transparent');
      logo.src = 'assets/img/Logo-Redflip-Sin-Borde.png';
      for(let i = 0; i<navColor.length; i++){
        navColor[i].classList.remove('blanco');
      }
    }
  }
}
