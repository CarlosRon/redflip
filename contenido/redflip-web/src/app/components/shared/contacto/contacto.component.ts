import { Component, OnInit } from '@angular/core';
import { ContactoService } from './contacto.service';
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  constructor(private mail: ContactoService) { }

  ngOnInit(): void {
  }

  onClickRadio(option: String) {
    const comuna = document.getElementById('Comuna');
    const dormitorio = document.getElementById('Dormitorio');
    const bano = document.getElementById('Bano');
    if(option === 'info'){
      comuna.style.display = 'none';
      dormitorio.style.display = 'none';
      bano.style.display = 'none';
    }else{
      comuna.style.display = '';
      dormitorio.style.display = '';
      bano.style.display = '';
    }
  }

enviar(){
// this.mail.enviarMail();
console.log(this.mail.enviarMail())
}

}
