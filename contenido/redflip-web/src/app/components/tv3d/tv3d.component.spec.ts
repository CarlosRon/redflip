import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tv3dComponent } from './tv3d.component';

describe('Tv3dComponent', () => {
  let component: Tv3dComponent;
  let fixture: ComponentFixture<Tv3dComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Tv3dComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Tv3dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
