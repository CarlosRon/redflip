import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VdeoarrComponent } from './vdeoarr.component';

describe('VdeoarrComponent', () => {
  let component: VdeoarrComponent;
  let fixture: ComponentFixture<VdeoarrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VdeoarrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VdeoarrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
