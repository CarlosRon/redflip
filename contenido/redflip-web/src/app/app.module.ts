import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Rutas
import { AppRoutingModule } from './app-routing.module';

// Servicios

// Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { PropiedadesComponent } from './components/propiedades/propiedades.component';
import { CorredoresComponent } from './components/corredores/corredores.component';
import { ContactoComponent } from './components/shared/contacto/contacto.component';
import {HttpClientModule} from '@angular/common/http';
import {ContactoService } from './components/shared/contacto/contacto.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Component } from '@angular/core';
import { AboutComponent } from './components/about/about.component';
import { CalculadoraComponent } from './components/calculadora/calculadora.component';
import { Tv3dComponent } from './components/tv3d/tv3d.component';
import { VdeoarrComponent } from './components/vdeoarr/vdeoarr.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    PropiedadesComponent,
    CalculadoraComponent,
    Tv3dComponent,
    VdeoarrComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    ContactoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
