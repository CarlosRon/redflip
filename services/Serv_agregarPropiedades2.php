<?
$idProp=$_GET["id"];
// include '../controlador/obtenerPropiedad.php';
include '../controlador/obtenerPropietario.php';


?>



<section>

  <div class="container-fluid" style="height:auto;margin-bottom:120px;">

    <div class="row">

      <div class="column col-xs-12 col-sm-12 col-md-12 col-lg-12">

          <h3 class="h3-prop mtb-50">Agregar Propietario - Propiedad</h3>



          <form id="formulario" action="../controlador/agregarPropiedad2.php" method ="POST">

            <div class="container-fluid">

              <div class="row">

                  <!-- [PROPIETARIO] -->

                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6 editProp" tabindex="1">

                  <h6 class="mtb-20">Propietario</h6>



                  <input type="hidden" class="form-control" id="idProp" name="idProp" value="<? echo $idProp?>">
                  <input type="hidden" class="form-control" id="fk_persona" name="fk_persona" value="<? echo $fk_persona?>">



                  <div class="form-group row">

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputEmail1">Nombre*</label>

                      <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Nombre" value="<?echo ($nomProp)?>" disabled>

                      <div id="err-nom"></div>

                    </div>

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Apellido*</label>

                      <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" value="<?echo $ape?>" disabled>

                      <div id="err-ape"></div>

                    </div>

                  </div>



                  <div class="form-group row">

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Teléfono</label>

                      <input type="text" class="form-control" id="tel" name="tel" placeholder="Ej. +56 9 1234 5678" value="<?echo $telefono?>" disabled>

                    </div>

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Correo</label>

                      <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo" value="<?echo $correo?>" disabled>

                    </div>

                  </div>



                  <div class="form-group row">

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Rut</label>

                      <input type="text" class="form-control" id="rut" name="rut" placeholder="Ej. 12345678-9" value="<?echo $rut?>" disabled>

                    </div>

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">F. de nacimiento</label>

                      <input type="date" class="form-control" id="date" name="date" placeholder="DD/MM/YYY"  value="<?echo $fec_nac?>" disabled>

                    </div>

                  </div>







                  <!-- [INVOLUCRADOS] -->

                  <h6 class="mtb-20">Involucrados</h6>

                    <div class="form-group row">

                      <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                        <label class="input-form" for="exampleInputEmail1">Corredor</label> </br>

                        <input type="hidden" class="form-control" id="idcorredor" name="idcorredor" value="<? echo $id?>" >

                    

                        <select id="corredor" name="corredor" class="selectEditProp">

                          <?
                          $sql ="Select Corredor.id_corredor, Persona.nombre, Persona.apellido from Corredor, Persona where Corredor.fk_persona = Persona.id_persona";

                          $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                  if($fk_corredor == $row['id_corredor']){
                                    echo "<option value=".$row['id_corredor']." selected>".utf8_encode($row['nombre'])." " .utf8_encode($row["apellido"]). "</option>";
                                  }
                                  
                                    echo "<option value=".$row['id_corredor'].">".utf8_encode($row['nombre'])." " .utf8_encode($row["apellido"]). "</option>";
                                  
                                }
                            }
                          
                          ?>

                        </select>

                      </div>

                      <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                        <label class="input-form" for="exampleInputEmail1">Operador</label> </br>

                        <input type="hidden" class="form-control" id="operador" name="operador" value=<? echo $id?> >

        

                        <select id="operario" name="operario" class="selectEditProp">

                          <?
                          $sql = "Select Operario.id_operario, Persona.nombre, Persona.apellido From Operario, Persona where Persona.id_persona = Operario.fk_persona";
                          $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                  if($fk_operario == $row['id_operario']){
                                    echo "<option value=".$row['id_operario']." selected>".utf8_encode($row['nombre'])." " .utf8_encode($row["apellido"]). "</option>";
                                  }else{
                                    echo "<option value=".$row['id_operario'].">".utf8_encode($row['nombre'])." " .utf8_encode($row["apellido"]). "</option>";
                                  }
                                  
                                    
                                  
                                }
                            }

                          ?>

                        </select>

                      </div>

                    </div>

                </div>

                

                <!-- [PROPIEDAD] -->

                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6 editPropA" tabindex="1">

                  <h6 class="mtb-20">Propiedad</h6>


                  <div class="form-group row">
                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                      <label class="input-form" for="exampleInputPassword1">Origen</label></br>
                        <select id="origen" name="origen" class="selectEditProp">
                          <?
                            $sql = "Select * from Origen";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                  if($fk_origen == $row['id_origen']){
                                    echo "<option value=".$row['id_origen']." selected>".$row['nom_origen']."</option>";
                                  }else{
                                    echo "<option value=".$row['id_origen']." >".$row['nom_origen']."</option>";
                                  }
                                }
                            }
                          ?>
                        </select>
                    </div>
                    
                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                      <label class="input-form" for="exampleInputPassword1">Tipo de propiedad</label></br>
                        <select id="tipo_prop" name="tipo_prop" class="selectEditProp">
                        <?
                          $sql = "Select * from tipo_propiedad";
                          $result = $conn->query($sql);
                          if ($result->num_rows > 0) {
                              // output data of each row
                              while($row = $result->fetch_assoc()) {
                                if($fk_origen == $row['id_origen']){
                                  echo "<option value=".$row['id_tipo_propiedad']." selected>".$row['tipo']."</option>";
                                }else{
                                  echo "<option value=".$row['id_tipo_propiedad']." >".$row['tipo']."</option>";
                                }
                              }
                          }
                        ?>
                        </select>
                    </div>
                  
                  </div>

                  <div class="form-group row">

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Tipo Operación</label></br>


                        <select id="tipo_op" name="tipo_op" class="selectEditProp">

                          <?

                            $sqlOperacion = "SELECT * FROM Venta_Arriendo";

        

                            $result = $conn->query($sqlOperacion);

                              if ($result->num_rows > 0) {

                                // output data of each row

                                while($row = $result->fetch_assoc()) {

                                  if($row["id"] == $operacion){

                                    echo "<option value=". utf8_encode($row['id'])." selected>". utf8_encode($row['descripcion'])."</option>";

                                  }else{

                                    echo "<option value=". utf8_encode($row['id'])." >". utf8_encode($row['descripcion'])."</option>";  

                                  }

                                }

                              }else{

                              }

                          ?>

                        </select>

                    </div>



                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Status</label></br>

                        <select id="status" name="status" class="selectEditProp">

                        <?
                        
                        $sql = "Select * from Status";
                        $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                  
                                  if($fk_status == $row['id_status']){

                                    echo "<option value=".$row['id_status']." selected>".utf8_encode($row['status'])."</option>";
                                  }else{
                                     echo "<option value=".$row['id_status'].">".utf8_encode($row['status'])."</option>";
                                  }
                                   
                                  
                                }
                            }
                        ?>

                        </select>

                    </div>

                  </div>



                  <div class="form-group row">

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Dormitorios</label>

                      <input type="number" class="form-control" id="dormitorios" placeholder="Ej. 1, 2 ..." name="dormitorios" value="<? echo $dorm?>">

                    </div>

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Baños</label>

                      <input type="number" class="form-control" id="baños" placeholder="Ej. 1, 2 ..." name="baños" value="<? echo $baños?>">

                    </div>

                  </div>

        

                  <div class="form-group row">

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Estacionamiento</label>

                      <input type="number" class="form-control" id="estacionamiento" placeholder="Ej. 1, 2 ..." name="estacionamiento" value="<? echo $estacionamiento?>">

                    </div>

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Bodega</label>

                      <input type="number" class="form-control" id="bodega" placeholder="Ej. 1, 2 ..." name="bodega" value="<? echo $bodega?>">

                    </div>

                  </div>

        

                  <div class="form-group row">

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Metros Totales</label>

                      <input type="number" class="form-control" id="mTotales" placeholder="Ej. 1, 2 ..." name="mTotales" value="<? echo $mTotales?>">

                    </div>

                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">

                      <label class="input-form" for="exampleInputPassword1">Metros Útiles</label>

                      <input type="number" class="form-control" id="mUtiles" placeholder="Ej. 1, 2 ..." name="mUtiles"  value="<? echo $mUtiles ?>">

                    </div>

                  </div>



                  <h6 class="mtb-20">Dirección</h6>

                    <input type="hidden" class="form-control" id="direccion" name="direccion" value="<? echo $fk_direccion?>">

        

                      <div class="form-group row">

                        <div class="column col-xs-12 col-sm-12 col-md-5 col-lg-5">

                          <label class="input-form" for="exampleInputPassword1">Calle</label>

                          <input type="text" class="form-control" id="calle" placeholder="ej. Av. kennedy" name="calle" value="<? echo utf8_encode($calle)?>" ></input>

                        </div>

                        <div class="column col-xs-12 col-sm-12 col-md-2 col-lg-3">

                          <label class="input-form" for="exampleInputPassword1">Número</label>

                          <input type="text" class="form-control" id="numero" placeholder="Ej. 1..." name="numero" value="<?echo $num?>">

                        </div>

                        <div class="column col-xs-12 col-sm-12 col-md-5 col-lg-4">

                          <label class="input-form" for="exampleInputPassword1">Comuna</label> </br>

                          <input type="hidden" class="form-control" id="idcomuna" name="idcomuna" value="<? echo $fk_comuna?>">

                            <select id="comuna" name="comuna" class="selectEditProp">

                              <?

                                $sqlComuna = "SELECT * FROM Comuna";

        

                                $result = $conn->query($sqlComuna);

                                  if ($result->num_rows > 0) {

                                  // output data of each row

                                    while($row = $result->fetch_assoc()) {

                                      if($fk_comuna == $row['id']){

                                        echo "<option value=". utf8_encode($row['id'])." selected>". utf8_encode($row['nombre'])."</option>";

                                      }else{

                                        echo "<option value=". utf8_encode($row['id'])." >". utf8_encode($row['nombre'])."</option>";  

                                      }

                                    }

                                  }else{

                                  }

                              ?>

                            </select>

                        </div>

                      </div>

                      <div class="form-group row">

                          <div class="column col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <label class="input-form" for="exampleInputPassword1">Referencia</label>

                            <input type="text" class="form-control" id="ref" placeholder="ej. metro manquehue" name="ref" value="<? echo utf8_encode($ref) ;?>">

                            </div>

                      </div>

                      <!-- [PRECIO] -->

                    <h6 class="mtb-20">Precio</h6>

                      <div class="form-group row">

                          <div class="column col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <label class="input-form" for="exampleInputPassword1">Monto</label>

                            <input type="number" class="form-control" id="monto" placeholder="1200" name="monto" value="<? echo $monto?>">

                          </div>



                        <div class="column col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <label class="input-form" for="exampleInputPassword1">Divisa</label></br>
                                  
                            <input type="hidden" class="form-control" id="iddivisa" name="iddivisa" value="<? echo $divisa?>"> 

                              <select id="divisa" name="divisa" class="selectEditProp">

                                <?

                                  $sqlDivisa = "SELECT id,nombre FROM Divisa";

                            

                                  $result = $conn->query($sqlDivisa);

                                    if ($result->num_rows > 0) {

                                      // output data of each row

                                      while($row = $result->fetch_assoc()) {

                                        if($divisa == $row["id"]){

                                          echo "<option value=". utf8_encode($row['id'])." selected>". utf8_encode($row['nombre'])."</option>";

                                        }else{

                                          echo "<option value=". utf8_encode($row['id'])." >". utf8_encode($row['nombre'])."</option>";  

                                        }

                                      }

                                    }else{

                                    }

                                ?>

                              </select>

                        </div>

                      </div>

                </div>



            <button type="submit" class="btn-def btn-login bg-redflip-red">Guardar</button>

          </form>

      </div>

    </div>

  </div>

</section>