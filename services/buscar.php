<?php 
include "../conexion.php";
include "funciones.php";
include "uf.php";
include "modal.php";
include "modalDuplicar.php";

// filtro pop up linea 554
$salida = "";
$conta = 1;

if(isset($_POST["iniciar"]) && isset($_POST["cantidad"])){

    // $iniciar = $_POST["iniciar"];
    // $cantidad = $_POST["cantidad"];
    $query = "SELECT per.nombre
    , per.apellido
    , per.telefono
    , per.correo
    , per.id_persona as idPer
    , dir.calle
    , dir.numero
    , dir.referencia
    , dir.id as idDir
    , com.nombre as Comuna
    , com.id as idCom
    , vent.descripcion V_A
    , vent.id as idVent
    , divi.nombre as divisa
    , divi.id as idDiv
    , propiedad.id_propiedad as idPropiedad
    , propiedad.fk_propietario
    , propiedad.nota
    , propiedad.monto
    , propiedad.id_externo
    , est.tipo
    , est.id as idEst
    , sub.fase
    , sub.id as idSub
    , propietario.cont
    , propietario.id_propietario as idPropietario
    , propietario.contactos
    , Origen.nom_origen as ori
    , Origen.id_origen as id_or
    , formulario.*
    , Estatus.status
    FROM Persona per
    INNER JOIN Propietario propietario
    , Estado_prop est
    , Sub_estado sub
    , Propiedad propiedad
    , Divisa divi, Direccion dir
    , Comuna com    
    , Venta_Arriendo vent
    , Origen    
    , Estatus 
    , formulario
    WHERE per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propietario.id_propietario = propiedad.fk_propietario
    AND propiedad.divisa = divi.id
    AND formulario.Direccion_id = dir.id
    and dir.fk_comuna = com.id
    AND propiedad.fk_estado_propiedad = 1
    AND propiedad.fk_venta_arriendo = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND propiedad.fk_status = Estatus.id_status
    AND propiedad.fk_formulario = formulario.id_formulario
    AND propiedad.carga = 0
    AND formulario.aprobado = 1
    order by formulario.fecha desc";
}else{

    $query = "SELECT distinct per.nombre
    , per.apellido
    , per.telefono
    , per.correo
    , per.id_persona as idPer
    , dir.calle
    , dir.numero
    , dir.referencia
    , dir.id as idDir
    , com.nombre as Comuna
    , com.id as idCom
    , vent.descripcion V_A
    , vent.id as idVent
    , divi.nombre as divisa
    , divi.id as idDiv
    , propiedad.id_propiedad as idPropiedad
    , propiedad.fk_propietario
    , propiedad.nota
    , propiedad.monto
    , propiedad.id_externo
    , est.tipo
    , est.id as idEst
    , sub.fase
    , sub.id as idSub
    , propietario.cont
    , propietario.id_propietario as idPropietario
    , propietario.contactos
    , Origen.nom_origen as ori
    , Origen.id_origen as id_or
    , formulario.*
    , Estatus.status
    FROM Persona per
    INNER JOIN Propietario propietario
    , Estado_prop est
    , Sub_estado sub
    , Propiedad propiedad
    , Divisa divi, Direccion dir
    , Comuna com    
    , Venta_Arriendo vent
    , Origen    
    , Estatus 
    , formulario
    WHERE per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propietario.id_propietario = propiedad.fk_propietario
    AND propiedad.divisa = divi.id
    AND formulario.Direccion_id = dir.id
    and dir.fk_comuna = com.id
    AND propiedad.fk_venta_arriendo = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND propiedad.fk_status = Estatus.id_status
    AND propiedad.fk_formulario = formulario.id_formulario
    AND propiedad.carga = 0
    AND formulario.aprobado = 1
    AND propiedad.fk_estado_propiedad = 1
    order by formulario.fecha desc";
// AND formulario.aprobado = 1 --Replicar en todas las consultas
}



if(isset($_POST['consulta1']) && isset($_POST['consulta2']) && isset($_POST['consulta3']) && isset($_POST['consulta4'])){

    $q1 = $conn -> real_escape_string($_POST['consulta1']);
    $q1 = utf8_decode($q1);
    $q2 = $conn -> real_escape_string($_POST['consulta2']);
    $q3 = $conn -> real_escape_string($_POST['consulta3']);
    $q4 = $conn -> real_escape_string($_POST['consulta4']);
    
    if($q4 == 'Alta'){

        $query = "SELECT per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , divi.nombre as divisa
        , divi.id as idDiv
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Divisa divi, Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        WHERE per.id_persona = propietario.fk_persona 
        AND propietario.fk_estado_prop = est.id
        AND est.fk_sub_estado = sub.id
        AND propietario.id_propietario = propiedad.fk_propietario
        AND propiedad.divisa = divi.id
        AND propiedad.fk_direccion = dir.id
        and dir.fk_comuna = com.id
        AND propiedad.fk_venta_arriendo = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND propiedad.fk_status = Estatus.id_status
        AND propiedad.fk_estado_propiedad = 1
        AND vent.descripcion LIKE" ."'%" .$q1. "%'
        AND est.tipo LIKE" ."'%" .$q2. "%'
        AND sub.fase LIKE" ."'%" .$q3. "%'
        AND propietario.cont > 31
        AND propiedad.fk_formulario = formulario.id_formulario
        AND formulario.aprobado = 1
        order by formulario.fecha asc";

    }else if($q4 == "Media"){

        $q4 = 20;

        $query = "SELECT per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , divi.nombre as divisa
        , divi.id as idDiv
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Divisa divi, Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        WHERE per.id_persona = propietario.fk_persona
        AND propietario.fk_estado_prop = est.id
        AND est.fk_sub_estado = sub.id
        AND propietario.id_propietario = propiedad.fk_propietario
        AND propiedad.divisa = divi.id
        AND propiedad.fk_direccion = dir.id
        and dir.fk_comuna = com.id
        AND propiedad.fk_venta_arriendo = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND propiedad.fk_estado_propiedad = 1
        AND propiedad.fk_status = Estatus.id_status
        AND vent.descripcion LIKE" ."'%" .$q1. "%'
        AND est.tipo LIKE" ."'%" .$q2. "%' 
        AND sub.fase LIKE" ."'%" .$q3. "%'
        AND propietario.cont >= $q4
        AND propietario.cont <=30
        AND propiedad.fk_formulario = formulario.id_formulario
        AND formulario.aprobado = 1
        order by formulario.fecha asc"; 

    }else if($q4 == "Baja"){

        $query = "SELECT per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , divi.nombre as divisa
        , divi.id as idDiv
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Divisa divi, Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        WHERE per.id_persona = propietario.fk_persona
        AND propietario.fk_estado_prop = est.id
        AND est.fk_sub_estado = sub.id
        AND propietario.id_propietario = propiedad.fk_propietario
        AND propiedad.divisa = divi.id
        AND propiedad.fk_direccion = dir.id
        and dir.fk_comuna = com.id
        AND propiedad.fk_venta_arriendo = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND propiedad.fk_estado_propiedad = 1
        AND propiedad.fk_status = Estatus.id_status
        AND vent.descripcion LIKE" ."'%" .$q1. "%'
        AND est.tipo LIKE" ."'%" .$q2. "%'
        AND sub.fase LIKE" ."'%" .$q3. "%'
        AND propietario.cont >= 0
        AND propietario.cont <20
        AND propiedad.fk_formulario = formulario.id_formulario
        AND formulario.aprobado = 1
        order by formulario.fecha asc"; 

    }else{

        $query = "SELECT per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , divi.nombre as divisa
        , divi.id as idDiv
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Divisa divi, Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        WHERE per.id_persona = propietario.fk_persona
        AND propietario.fk_estado_prop = est.id
        AND est.fk_sub_estado = sub.id
        AND propietario.id_propietario = propiedad.fk_propietario
        AND propiedad.divisa = divi.id
        AND propiedad.fk_direccion = dir.id
        and dir.fk_comuna = com.id
        AND propiedad.fk_venta_arriendo = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND propiedad.fk_estado_propiedad = 1
        AND propiedad.fk_status = Estatus.id_status
        AND vent.descripcion LIKE" ."'%" .$q1. "%'
        AND est.tipo LIKE" ."'%" .$q2. "%'
        AND sub.fase LIKE" ."'%" .$q3. "%'
        AND propiedad.fk_formulario = formulario.id_formulario
        AND formulario.aprobado = 1
        order by formulario.fecha asc ";

    }

}else if(isset($_POST['consulta1']) && isset($_POST['consulta2']) && isset($_POST['consulta3']) && isset($_POST['consulta4']) && isset($_POST['consulta5'])){



    $q1 = $conn -> real_escape_string($_POST['consulta1']);
    $q1 = utf8_decode($q1);
    $q2 = $conn -> real_escape_string($_POST['consulta2']);
    $q3 = $conn -> real_escape_string($_POST['consulta3']);
    $q4 = $conn -> real_escape_string($_POST['consulta4']);
    $q5 = $conn -> real_escape_string($_POST['consulta5']);

    $query = "SELECT per.nombre
    , per.apellido
    , per.telefono
    , per.correo
    , per.id_persona as idPer
    , dir.calle
    , dir.numero
    , dir.referencia
    , dir.id as idDir
    , com.nombre as Comuna
    , com.id as idCom
    , vent.descripcion V_A
    , vent.id as idVent
    , divi.nombre as divisa
    , divi.id as idDiv
    , propiedad.id_propiedad as idPropiedad
    , propiedad.fk_propietario
    , propiedad.nota
    , propiedad.monto
    , propiedad.id_externo
    , est.tipo
    , est.id as idEst
    , sub.fase
    , sub.id as idSub
    , propietario.cont
    , propietario.id_propietario as idPropietario
    , propietario.contactos
    , Origen.nom_origen as ori
    , Origen.id_origen as id_or
    , formulario.*
    , Estatus.status
    FROM Persona per
    INNER JOIN Propietario propietario
    , Estado_prop est
    , Sub_estado sub
    , Propiedad propiedad
    , Divisa divi, Direccion dir
    , Comuna com    
    , Venta_Arriendo vent
    , Origen    
    , Estatus 
    , formulario
    WHERE per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propietario.id_propietario = propiedad.fk_propietario
    AND propiedad.divisa = divi.id
    AND propiedad.fk_direccion = dir.id
    and dir.fk_comuna = com.id
    AND propiedad.fk_venta_arriendo = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND propiedad.fk_estado_propiedad = 1
    AND propiedad.fk_status = Estatus.id_status
    AND( vent.descripcion LIKE" ."'" .$q1. "%'
    OR per.nombre LIKE" ."'" .$q1. "%'
    OR per.apellido LIKE" ."'" .$q1. "%'
    OR per.telefono LIKE" ."'" .$q1. "%'
    OR per.correo LIKE" ."'" .$q1. "%'
    OR dir.calle LIKE" ."'" .$q1. "%'
    OR com.nombre LIKE" ."'" .$q1. "%'
    OR propiedad.monto LIKE" ."'" .$q1. "%'
    OR est.tipo LIKE" ."'" .$q1. "%'
    OR sub.fase LIKE" ."'" .$q1. "%'
    OR Estatus.status LIKE" ."'" .$q1. "%'
    OR vent.descripcion LIKE" ."'%" .$q1. "%')
    AND est.tipo LIKE" ."'%" .$q2. "%'
    AND sub.fase LIKE" ."'%" .$q3. "%'
    AND propiedad.fk_formulario = formulario.id_formulario   
    AND formulario.aprobado = 1 
    order by formulario.fecha asc";
    
}else if(isset($_POST['consulta1'])){

    $q1 = $conn -> real_escape_string($_POST['consulta1']);
    $q1 = utf8_decode($q1);
    echo "<script>console.log('$q1')</script>";

    $query = "SELECT per.nombre
    , per.apellido
    , per.telefono
    , per.correo
    , per.id_persona as idPer
    , dir.calle
    , dir.numero
    , dir.referencia
    , dir.id as idDir
    , com.nombre as Comuna
    , com.id as idCom
    , vent.descripcion V_A
    , vent.id as idVent
    , divi.nombre as divisa
    , divi.id as idDiv
    , propiedad.id_propiedad as idPropiedad
    , propiedad.fk_propietario
    , propiedad.nota
    , propiedad.monto
    , propiedad.id_externo
    , est.tipo
    , est.id as idEst
    , sub.fase
    , sub.id as idSub
    , propietario.cont
    , propietario.id_propietario as idPropietario
    , propietario.contactos
    , Origen.nom_origen as ori
    , Origen.id_origen as id_or
    , formulario.*
    , Estatus.status
    FROM Persona per
    INNER JOIN Propietario propietario
    , Estado_prop est
    , Sub_estado sub
    , Propiedad propiedad
    , Divisa divi, Direccion dir
    , Comuna com    
    , Venta_Arriendo vent
    , Origen    
    , Estatus 
    , formulario
    WHERE per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propietario.id_propietario = propiedad.fk_propietario
    AND propiedad.divisa = divi.id
    AND propiedad.fk_direccion = dir.id
    and dir.fk_comuna = com.id
    AND propiedad.fk_venta_arriendo = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND propiedad.fk_estado_propiedad = 1
    AND propiedad.fk_status = Estatus.id_status
    AND( vent.descripcion LIKE" ."'" .$q1. "%'
    OR per.nombre LIKE" ."'" .$q1. "%'
    OR per.apellido LIKE" ."'" .$q1. "%'
    OR per.telefono LIKE" ."'" .$q1. "%'
    OR per.correo LIKE" ."'" .$q1. "%'
    OR dir.calle LIKE" ."'" .$q1. "%'
    OR com.nombre LIKE" ."'" .$q1. "%'
    OR propiedad.monto LIKE" ."'" .$q1. "%'
    OR est.tipo LIKE" ."'" .$q1. "%'
    OR sub.fase LIKE" ."'" .$q1. "%'
    OR Estatus.status LIKE" ."'" .$q1. "%' ) 
    AND propiedad.fk_formulario = formulario.id_formulario 
    AND formulario.aprobado = 1  
    order by formulario.fecha asc";

}else if($_POST["filtro"] == "popup"){
    $operacion = $_POST["operacion"];
    $estado = $_POST["estado"];
    $fase = $_POST["fase"];
    $importancia = $_POST["importancia"];
    $comuna = $_POST["comuna"];
    $tipo_prop = $_POST["tipo_prop"];
    $habitacionesMin = $_POST["habitacionesMin"];
    $habitacionesMax = $_POST["habitacionesMax"];
    $bannosMin = $_POST["bannosMin"];
    $bannosMax = $_POST["bannosMax"];
    $sUtilMin = $_POST["sUtilMin"];
    $sUtilMax = $_POST["sUtilMax"];
    $precioMin = $_POST["precioMin"];
    $precioMax = $_POST["precioMax"];
    $orden = $_POST["orden"];
    $origen = $_POST["origen"];
    $status = $_POST["status"];
    $divisa = $_POST["divisa"];

    if($bannosMin == "" && $bannosMax != ""){
        $bannosMin = "0";
    }

    if($bannosMax == "" && $bannosMin != ""){
        $bannosMax = "99";
    }

    if($habitacionesMin == "" && $habitacionesMax != ""){
        $habitacionesMin = "0";
    }

    if($habitacionesMax == "" && $habitacionesMin != ""){
        $habitacionesMax = "99";
    }

    if($precioMin == "" && $precioMax != ""){
        $precioMin = 0;
    }

    if($precioMax == "" && $precioMin != ""){
        $precioMax = 394000010244;
    }

    if($sUtilMin == "" && $sUtilMax != ""){
        $sUtilMin = 0;
    }

    if($sUtilMax == "" && $sUtilMin != ""){
        $sUtilMax = 510072000;
    }
    
    

    //----------------------
    if($importancia == 'Alta'){

        $query = "SELECT per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , divi.nombre as divisa
        , divi.id as idDiv
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.fk_status
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , propietario.fk_origen
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        , propiedad.fk_tipo_propiedad
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Divisa divi, Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        WHERE per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propietario.id_propietario = propiedad.fk_propietario
    AND propiedad.divisa = divi.id
    AND formulario.Direccion_id = dir.id
    and dir.fk_comuna = com.id
    AND propiedad.fk_estado_propiedad = 1
    AND propiedad.fk_venta_arriendo = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND propiedad.fk_status = Estatus.id_status
    AND propiedad.fk_formulario = formulario.id_formulario
    AND propiedad.carga = 0
    AND formulario.aprobado = 1";

    if($operacion != ""){
        $query .= " AND vent.descripcion LIKE '%$operacion'";
    }
   
    if($estado != ""){
        $query .= " AND est.tipo LIKE '%$estado'";
    }
    
    if($fase != ""){
        $query .= " AND sub.fase LIKE '$fase'";
    }
    $query .= " AND propietario.cont > 31";
        if($divisa != ""){
            if($precioMin != "" || $precioMax != ""){
                $query .=" AND (formulario.monto1 >= $precioMin AND  formulario.monto1 <= $precioMax)";
                $query .=" AND formulario.divisa_monto1 = $divisa";
            }            
        }if($divisa == "" && ($precioMin != "" || $precioMax != "")){
            $query .=" AND(formulario.monto1 >= $precioMin AND  formulario.monto1 <= $precioMax)";
        }     
        if($bannosMin != "" || $bannosMax != ""){
            $query .=" AND formulario.cant_bannos >= '$bannosMin' AND  formulario.cant_bannos <= '$bannosMax'";
        }
        if($sUtilMin != "" || $sUtilMax != ""){
            $query .=" AND formulario.superficie_util >= $sUtilMin AND  formulario.superficie_util <= $sUtilMax";
        }
        if($habitacionesMin != "" || $habitacionesMax != ""){
            $query .=" AND formulario.dormitorios >= '$habitacionesMin' AND formulario.dormitorios <= '$habitacionesMax' ";
        }
        if($tipo_prop != ""){
            $query.=" AND propiedad.fk_tipo_propiedad = $tipo_prop ";
        }
        if($comuna != ""){
            $query.=" AND com.id = $comuna";
        }
        if($origen != ""){
            $query .=" AND propietario.fk_origen = $origen ";
        }
        if($status != ""){
            $query .= " AND propiedad.fk_status = $status";
        }
        if($orden != ""){
            switch ($orden) {
                case 1:
                    $query.=" order by formulario.valor_clp desc";
                    break;
                case 2:
                    $query.=" order by formulario.valor_uf desc";
                    break;
                case 3:
                    $query.=" order by formulario.valor_clp asc";
                    break;
                case 4:
                    $query.=" order by formulario.valor_uf asc";
                    break;
                case 5:
                    $query.=" order by formulario.dormitorios asc";
                    break;
                case 6:
                    $query.=" order by formulario.dormitorios desc";
                    break;
                case 7:
                    $query.=" order by formulario.cant_bannos asc";
                    break;
                case 8:
                    $query.=" order by formulario.cant_bannos desc";
                    break;
            }
        }else{
            $query .=" order by formulario.fecha asc";
        }
        
        
        

    }else if($importancia == "Media"){

        $importancia = 20;

        $query = "SELECT per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , divi.nombre as divisa
        , divi.id as idDiv
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.fk_status
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.fk_origen
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Divisa divi, Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        WHERE per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propietario.id_propietario = propiedad.fk_propietario
    AND propiedad.divisa = divi.id
    AND formulario.Direccion_id = dir.id
    and dir.fk_comuna = com.id
    AND propiedad.fk_estado_propiedad = 1
    AND propiedad.fk_venta_arriendo = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND propiedad.fk_status = Estatus.id_status
    AND propiedad.fk_formulario = formulario.id_formulario
    AND propiedad.carga = 0
    AND formulario.aprobado = 1";
    if($operacion != ""){
        $query .= " AND vent.descripcion LIKE '%$operacion'";
    }
   
    if($estado != ""){
        $query .= " AND est.tipo LIKE '%$estado'";
    }
    
    if($fase != ""){
        $query .= " AND sub.fase LIKE '$fase'";
    }
    $query .=" AND propietario.cont >= $importancia
    ";
        if($divisa != ""){
            if($precioMin != "" || $precioMax != ""){
                $query .=" AND (formulario.monto1 >= $precioMin AND  formulario.monto1 <= $precioMax)";
                $query .=" AND formulario.divisa_monto1 = $divisa";
            }
           
        }if($divisa == "" && ($precioMin != "" || $precioMax != "")){
            $query .=" AND(formulario.monto1 >= $precioMin AND  formulario.monto1 <= $precioMax)";
        }  
        if($bannosMin != "" || $bannosMax != ""){
            $query .=" AND formulario.cant_bannos >= '$bannosMin' AND  formulario.cant_bannos <= '$bannosMax'";
        }
        if($sUtilMin != "" || $sUtilMax != ""){
            $query .=" AND formulario.superficie_util >= $sUtilMin AND  formulario.superficie_util <= $sUtilMax";
        }
        if($habitacionesMin != "" || $habitacionesMax != ""){
            $query .=" AND formulario.dormitorios >= '$habitacionesMin' AND formulario.dormitorios <= '$habitacionesMax' ";
        }
        if($tipo_prop != ""){
            $query.=" AND propiedad.fk_tipo_propiedad = $tipo_prop ";
        }
        if($comuna != ""){
            $query.=" AND com.id = $comuna";
        }
        if($origen != ""){
            $query .=" AND propietario.fk_origen = $origen ";
        }
        if($status != ""){
            $query .= " AND propiedad.fk_status = $status";
        }
        if($orden != ""){
            switch ($orden) {
                case 1:
                    $query.=" order by formulario.valor_clp desc";
                    break;
                case 2:
                    $query.=" order by formulario.valor_uf desc";
                    break;
                case 3:
                    $query.=" order by formulario.valor_clp asc";
                    break;
                case 4:
                    $query.=" order by formulario.valor_uf asc";
                    break;
                case 5:
                    $query.=" order by formulario.dormitorios asc";
                    break;
                case 6:
                    $query.=" order by formulario.dormitorios desc";
                    break;
                case 7:
                    $query.=" order by formulario.cant_bannos asc";
                    break;
                case 8:
                    $query.=" order by formulario.cant_bannos desc";
                    break;
            }
        }else{
            $query .=" order by formulario.fecha asc";
        }
       

    }else if($importancia == "Baja"){

        $query = "SELECT per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , divi.nombre as divisa
        , divi.id as idDiv
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.fk_status
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , propietario.fk_origen
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Divisa divi, Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        WHERE per.id_persona = propietario.fk_persona
        AND propietario.fk_estado_prop = est.id
        AND est.fk_sub_estado = sub.id
        AND propietario.id_propietario = propiedad.fk_propietario
        AND propiedad.divisa = divi.id
        AND formulario.Direccion_id = dir.id
        and dir.fk_comuna = com.id
        AND propiedad.fk_estado_propiedad = 1
        AND propiedad.fk_venta_arriendo = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND propiedad.fk_status = Estatus.id_status
        AND propiedad.fk_formulario = formulario.id_formulario
        AND propiedad.carga = 0
        AND formulario.aprobado = 1";
        if($operacion != ""){
        $query .= " AND vent.descripcion LIKE '%$operacion'";
    }
   
    if($estado != ""){
        $query .= " AND est.tipo LIKE '%$estado'";
    }
    
    if($fase != ""){
        $query .= " AND sub.fase LIKE '$fase'";
    }
        if($divisa != ""){
            if($precioMin != "" || $precioMax != ""){
                $query .=" AND (formulario.monto1 >= $precioMin AND  formulario.monto1 <= $precioMax)";
                $query .=" AND formulario.divisa_monto1 = $divisa";
            }
            
        }if($divisa == "" && ($precioMin != "" || $precioMax != "")){
            $query .=" AND(formulario.monto1 >= $precioMin AND  formulario.monto1 <= $precioMax)";
        }   
        if($bannosMin != "" || $bannosMax != ""){
            $query .=" AND formulario.cant_bannos >= '$bannosMin' AND  formulario.cant_bannos <= '$bannosMax'";
        }
        if($sUtilMin != "" || $sUtilMax != ""){
            $query .=" AND formulario.superficie_util >= $sUtilMin AND  formulario.superficie_util <= $sUtilMax";
        }
        if($habitacionesMin != "" || $habitacionesMax != ""){
            $query .=" AND formulario.dormitorios >= '$habitacionesMin' AND formulario.dormitorios <= '$habitacionesMax' ";
        }
        if($tipo_prop != ""){
            $query.=" AND propiedad.fk_tipo_propiedad = $tipo_prop ";
        }
        if($comuna != ""){
            $query.=" AND com.id = $comuna";
        }
        if($origen != ""){
            $query .=" AND propietario.fk_origen = $origen ";
        }
        if($status != ""){
            $query .= " AND propiedad.fk_status = $status";
        }
        if($orden != ""){
            switch ($orden) {
                case 1:
                    $query.=" order by formulario.valor_clp desc";
                    break;
                case 2:
                    $query.=" order by formulario.valor_uf desc";
                    break;
                case 3:
                    $query.=" order by formulario.valor_clp asc";
                    break;
                case 4:
                    $query.=" order by formulario.valor_uf asc";
                    break;
                case 5:
                    $query.=" order by formulario.dormitorios asc";
                    break;
                case 6:
                    $query.=" order by formulario.dormitorios desc";
                    break;
                case 7:
                    $query.=" order by formulario.cant_bannos asc";
                    break;
                case 8:
                    $query.=" order by formulario.cant_bannos desc";
                    break;
            }
        }else{
            $query .=" order by formulario.fecha asc";
        }
    }else{
    //----------------------
    $query = "SELECT per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , divi.nombre as divisa
        , divi.id as idDiv
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.fk_status
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , propietario.fk_origen
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Divisa divi, Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        WHERE per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propietario.id_propietario = propiedad.fk_propietario
    AND propiedad.divisa = divi.id
    AND formulario.Direccion_id = dir.id
    and dir.fk_comuna = com.id
    AND propiedad.fk_estado_propiedad = 1
    AND propiedad.fk_venta_arriendo = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND propiedad.fk_status = Estatus.id_status
    AND propiedad.fk_formulario = formulario.id_formulario
    AND propiedad.carga = 0
    AND formulario.aprobado = 1";
    if($operacion != ""){
        $query .= " AND vent.descripcion LIKE '%$operacion'";
    }
   
    if($estado != ""){
        $query .= " AND est.tipo LIKE '%$estado'";
    }
    
    if($fase != ""){
        $query .= " AND sub.fase LIKE '$fase'";
    }
    
        if($divisa != ""){
            if($precioMin != "" || $precioMax != ""){
                $query .=" AND (formulario.monto1 >= $precioMin AND  formulario.monto1 <= $precioMax)";
                $query .=" AND formulario.divisa_monto1 = $divisa";
            }
           
        }if($divisa == "" && ($precioMin != "" || $precioMax != "")){
            $query .=" AND(formulario.monto1 >= $precioMin AND  formulario.monto1 <= $precioMax)";
        }             
        if($bannosMin != "" || $bannosMax != ""){
            $query .=" AND formulario.cant_bannos >= '$bannosMin' AND  formulario.cant_bannos <= '$bannosMax'";
        }
        if($sUtilMin != "" || $sUtilMax != ""){
            $query .=" AND formulario.superficie_util >= $sUtilMin AND  formulario.superficie_util <= $sUtilMax";
        }
        if($habitacionesMin != "" || $habitacionesMax != ""){
            $query .=" AND formulario.dormitorios >= '$habitacionesMin' AND formulario.dormitorios <= '$habitacionesMax' ";
        }
        if($tipo_prop != ""){
            $query.=" AND formulario.fk_tipo_propiedad = $tipo_prop ";
        }
        if($comuna != ""){
            $query.=" AND com.id = $comuna";
        }
        if($origen != ""){
            $query .=" AND propietario.fk_origen = $origen ";
        }
        if($status != ""){
            $query .= " AND propiedad.fk_status = $status";
        }
        if($orden != ""){
            switch ($orden) {
                case 1:
                    $query.=" order by formulario.valor_clp desc";
                    break;
                case 2:
                    $query.=" order by formulario.valor_uf desc";
                    break;
                case 3:
                    $query.=" order by formulario.valor_clp asc";
                    break;
                case 4:
                    $query.=" order by formulario.valor_uf asc";
                    break;
                case 5:
                    $query.=" order by formulario.dormitorios asc";
                    break;
                case 6:
                    $query.=" order by formulario.dormitorios desc";
                    break;
                case 7:
                    $query.=" order by formulario.cant_bannos asc";
                    break;
                case 8:
                    $query.=" order by formulario.cant_bannos desc";
                    break;
            }
        }else{
            $query .=" order by formulario.fecha desc";
        }
    }
}
// echo $query;






$resultado = $conn->query($query);
if($resultado->num_rows >0){

    $salida.= "
        <table class='table table-responsive table-hover table-striped table-min' id='tabla_mixta'>
            <thead class='thead-dark tab-center'>
                <tr>
                    <th class='' scope='col'> ID </th>
                    <th class='' scope='col'> Propietario </th>
                    <th class='' scope='col'> Tipo </th>
                    <th class='' scope='col'> OP </th>	
                    <th class='' scope='col'> Dirección </th>
                    <th class='' scope='col'> Comuna </th>
                    <th class='' scope='col'> m<sup>2</sup> </th>
                    <th class='' scope='col'> TOT </th>
                    <th class='' scope='col'> HAB </th>
                    <th class='' scope='col'> BA </th>
                    <th class='' scope='col'> EST </th>
                    <th class='' scope='col'> Valor $ </th>
                    <th class='' scope='col'> UF </th>
                    <th class='' scope='col'> Fase </th>
                    <th class='' scope='col'> EJEC </th>
                    <th class='' scope='col'> ACC </th>
                </tr>
            </thead>
        <tbody>
        ";

$table_hidden= "

        <table class='table table-responsive table-hover table-striped table-min' id='export_to_excel' style='display:none!important'>

        <thead>
            <tr class='thead-dark .trProp'>
                <th class='thProp stackTab' scope='col'> ID </th>
                <th class='thProp stackTab' scope='col'> Propietario </th>
                <th class='thProp stackTab' scope='col'> Tipo </th>
                <th class='thProp stackTab' scope='col'> OP </th>
                <th class='thProp stackTab' scope='col'> Dirección </th>
                <th class='thProp stackTab' scope='col'> Comuna </th>
                <th class='thProp stackTab' scope='col'> m<sup>2</sup> </th>
                <th class='thProp stackTab' scope='col'> TOT </th>
                <th class='thProp stackTab' scope='col'> HAB </th>
                <th class='thProp stackTab' scope='col'> BA </th>
                <th class='thProp stackTab' scope='col'> EST </th>
                <th class='thProp stackTab' scope='col'> Valor $ </th>
                <th class='thProp stackTab' scope='col'> UF </th>
                <th class='thProp stackTab' scope='col'> Fase </th>
                <th class='thProp stackTab' scope='col'> EJEC </th>
            </tr>
        </thead>
    <tbody> 
    ";

    
while($row = $resultado-> fetch_assoc()){
    $propietario ="";
    $nota = "";
    $nombre = "";
    $apellido ="";
    $telefono ="";
    $correo = "";
    $calle ="";  
    $numero ="";
    $comuna = "";
    $operacion = "";
    $divisa_monto1 = "";
    $divisa_monto2 = "";
    $op_mostrar = "";
    $tipo_prop_display = "";
    $direccion_display = "";
    $formulario_monto1_peso = "";
    $formulario_monto1_uf = "";

    
//    -------------------------------------------------------------------importancia baja------------------------------------------------------------------------------------------
    if($row["cont"]< 20){
        $salida.= "
        <tr class='tr-style' >";

        $propietario = $row["fk_propietario"];
        $propietario_nombre = "";

        $sql_propietario = "SELECT Persona.nombre, Persona.apellido FROM Persona, Propietario WHERE Persona.id_persona = Propietario.fk_persona AND Propietario.id_propietario = $propietario;";
        $result_propietario = $conn->query($sql_propietario);

        if ($result_propietario->num_rows > 0) {
            while($row_propietario = $result_propietario->fetch_assoc()) {
                $propietario_n = $row_propietario["nombre"];
                $propietario_a = $row_propietario["apellido"];
                $propietario_nombre = $propietario_n." ".$propietario_a;
            }
        } else {

        }

        $nota = $row["nota"];
        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $telefono = utf8_encode($row["telefono"]) ;
        $correo = utf8_encode($row["correo"] );
        $fk_tipo_prop = $row["fk_tipo_propiedad"];
        $formulario_monto1 = $row["monto1"];
        $formulario_monto2 = $row["monto2"];
        
        $operacion = utf8_encode($row["operacion"]);
        $divisa_monto1 = utf8_encode($row["divisa_monto1"]);
        $divisa_monto2 = utf8_encode($row["divisa_monto2"]);
        
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row['fase']);
        
        
        $estacionamiento = utf8_encode($row["estacionamiento"]);
        $mTotales = utf8_encode($row["metros_totales"]);
        $mutiles = utf8_encode($row["metros_utiles"]);
        $bodega = utf8_encode($row["bodega"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row["fase"]);
        $dias = utf8_encode($row["cont"]);
        $idPropiedad = $row["idPropiedad"];
        $idExterno = $row["id_externo"];
        $ori = utf8_encode($row["ori"]);
        $idForm = $row["id_formulario"];
        $fecha = $row["fecha"];

        // datos

        //Tipo Prop

        switch ($fk_tipo_prop){
            case '1':
                $tipo_prop_display = "Depto.";
            break;
            case '2':
                $tipo_prop_display = "Casa";
            break;
            case '3':
                $tipo_prop_display = "Local Com.";
            break;
            case '4':
                $tipo_prop_display = "Of.";
            break;
            case '5':
                $tipo_prop_display = "Terr.";
            break;
            case '6':
                $tipo_prop_display = "Parc.";
            break;
            case '7':
                $tipo_prop_display = "Bod.";
            break;
            case '8':
                $tipo_prop_display = "Galp.";
            break;
            case '9':
                $tipo_prop_display = "No def.";
            break;
            case '12':
                $tipo_prop_display = "Casa Of.";
            break;
            case '13':
                $tipo_prop_display = "Depto. Ofi.";
            break;
            case '14':
                $tipo_prop_display = "Casa Terr.";
            break;
            case '15':
                $tipo_prop_display = "Casa Parc.";
            break;
            default: $tipo_prop_display = 'Undefined';
        }

        // OP

        if($operacion == 1){
            $op_mostrar = "Ven.";
        }else if($operacion == 2){
            $op_mostrar = "Arr.";
        }else{
            $op_mostrar = "Amb.";
        }

        //Direccion

        $calle = utf8_encode($row["calle"]);  
        $numero = utf8_encode($row["numero"]);

        if($letra != ""){
            $direccion_display = $calle." ".$numero." ".$letra;
        }else{
            $direccion_display = $calle." ".$numero;
        }

        //comuna

        $comuna = utf8_encode($row["Comuna"]);

        //superficie
        $sup_util = $row["superficie_util"];
        $sup_total = $row["superficie_total"];

        if($sup_util == ""){
            $sup_util = "S/D";
        }

        if($sup_total == ""){
            $sup_total = "S/D";
        }
        
        //habitaciones

        $dormitorios = utf8_encode($row["dormitorios"]); 

        if($dormitorios == ""){
            $dormitorios = "S/D";
        }

        //baños

        $cant_bannos = $row["cant_bannos"];

        if($cant_bannos == ""){
            $cant_bannos = "S/D";
        }

        //valor pesos || valor UF
        if($formulario_monto1 == "" || $formulario_monto1 == 0 || $divisa_monto1 == ""){
            $formulario_monto1_peso = "S/D";
            $formulario_monto1_uf = "S/D";
        }else{
            if($divisa_monto1 == "1"){
                $formulario_monto1_uf = moneda_chilena_sin_peso($formulario_monto1) . " UF";
                $formulario_monto1_peso = moneda_chilena($formulario_monto1 * $valor_uf);
            }else if ($divisa_monto1 == "2"){
                $formulario_monto1_peso = moneda_chilena($formulario_monto1);
                $formulario_monto1_uf = moneda_chilena_sin_peso(round($formulario_monto1 / $valor_uf)) . " UF";
            }
        }
        
        
        //estado-fase-idk

        //ejecutivo
        $fk_corredor = $row["fk_corredor"];
        if($fk_corredor != ""){
            $sql_corredor2 = "SELECT Persona.* FROM Persona, Corredor WHERE id_corredor = $fk_corredor AND Corredor.fk_persona = Persona.id_persona;";
            // echo $sql_corredor2;
            $result_corredor = $conn->query($sql_corredor2);

            if ($result_corredor->num_rows > 0) {
                while($row_corredor = $result_corredor->fetch_assoc()) {
                    $nom_corredor = $row_corredor["nombre"];
                    $ape_corredor = $row_corredor["apellido"];
                }
                $corredor = subStr($nom_corredor, 0, 1).subStr($ape_corredor, 0, 1);
                $corredor = strToUpper($corredor);
            } 
        }else {
            $corredor = "S/D";
        }

        //Estacionamiento

        if($estacionamiento == ""){
            $estacionamiento = "S/D";
        }

        

        //para el editar
        $prop = $row["fk_tipo_propiedad"];
        switch ($prop) {
            case '1':
                $prop = "depto";
                break;
            case '2':
                $prop = "casa";
                break;
            case '3':
                $prop = "local";
                break;
            case '4':
                $prop = "ofi";
                break;
        }

        $op = $row["operacion"];
        switch ($op) {
            case '1':
                $op = "ven";
                break;
            case '2':
                $op = "arr";
                break;
            case '3':
                $op = "amb";
                break;
        }

        
        

        $telDes =substr($telefono, 0, 3)." ".substr($telefono, 3, 1). " ".substr($telefono, 4);
        $salida .= $origen."

            <td class=''><pre class='tabPre'> <i class='fas fa-circle fa-xs ci-gre'></i>".$idForm."</pre></td>    
            <td class=''><pre class='tabPre'><a onclick='datosPropietario(event,".$propietario.")' href='#'>".utf8_encode($propietario_nombre)."</a></pre></td> 
            <td class=''><pre class='tabPre'>".$tipo_prop_display."</pre></td> 
            <td class=''><pre class='tabPre'>".$op_mostrar."</pre></td> 
            <td class=''><pre class='tabPre'>".$direccion_display."</pre></td> 
            <td class=''><pre class='tabPre'>".$comuna."</pre></td> 
            <td class=''><pre class='tabPre'>".$sup_util."</pre></td> 
            <td class=''><pre class='tabPre'>".$sup_total."</pre></td> 
            <td class=''><pre class='tabPre'>".$dormitorios."</pre></td> 
            <td class=''><pre class='tabPre'>".$cant_bannos."</pre></td>
            <td class=''><pre class='tabPre'>".$estacionamiento."</pre></td>
            <td class=''><pre class='tabPre'>".$formulario_monto1_peso."</pre></td> 
            <td class=''><pre class='tabPre'>".$formulario_monto1_uf."</pre></td> 
            
            ";
            $conta ++;
            // Contactos input
            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
            $thisFase = "";
                if($row['fase'] == "Contacto"){
                    $salida.="
                    <td class='contacto' id='select_".$row["idPer"]."'>
                        <select class='btn btn-r dropdown-toggle selectFaseContacto' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }else{
                $salida.="
                    <td class='' id='select_".$row["idPer"]."'>
                        <select class= 'btn btn-r dropdown-toggle selectFase' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
            }
                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        // $table_hidden.="<td>".$row2['fase'] ."</td>";
                        // $table_hidden.="<td>".$row['contactos'] ."</td>";
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                    }
                } 
                // echo "<script>console.log($this)</script>"
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='btn-border inputFase' id='input_".$row['idPropietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['idPropietario'].")' >";
                }
                
            $sqlEvent = "Select COUNT(*) as event from Calendar where fk_propiedad = $idPropiedad";
            $result = $conn->query($sqlEvent);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                  $event = $row["event"];  
                }
            }
            

            $salida.="<td class=''><pre class='tabPre'>".$corredor."</pre></td>

            <td class=''>
                <div class='btn-group dropleft'>
                    <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        <i class='fas fa-cog'></i>
                    </button>
                <div class='dropdown-menu'>
                <a class='dropdown-item' href='ControladorPlantillasEditar.php?id=".$idForm."&source=2&prop=".$prop."&op=".$op."'><i class='fas fa-edit'></i> Modificar</a>
                <a class='dropdown-item' href='' onClick='desPropiedad(".$idPropiedad."); return false;'><i class='fas fa-user-slash'></i> Deshabilitar</a>
                
                <a class='dropdown-item' href='' onclick='redirigir($idForm, `2`);return false;'><i class='far fa-file-alt'></i> Ver formulario</a>";

                if(strlen($nota) > 0 || $event != 0){
                    $salida.="
                    <a class='dropdown-item  bg-redflip-orange' href='notas.php?id=".$idPropiedad."'><i class='far fa-sticky-note'></i> Notas</a>";
                }else{
                    $salida.="
                    <a class='dropdown-item ' href='notas.php?id=".$idPropiedad."'><i class='far fa-sticky-note'></i> Notas</a>";
                }
            $salida.="<a class='dropdown-item' href='' onclick='cambiarEstatus($idForm); return false;' ><i class='fas fa-sync'></i> Cambiar Estatus</a>
            <a class='dropdown-item' href='https://api.whatsapp.com/send?phone=".$telefono."&text=Hola%20quiero%20info' target='_blank' ";

                    if(strlen($telefono) != 12){
                        $salida.="disabled";

                    }else{
                        $salida.="";
                    }
                    
                    $salida.="><i class='fab fa-whatsapp'></i> Whatsapp</a> 
                    <a class='dropdown-item' href='publicacion.php?id=$idForm' ><i class='fas fa-sync'></i> Publicación</a>
                    <a class='dropdown-item' onclick='duplicarForm(event, $idForm)'><i class='far fa-clone'></i> Duplicar</a>
            </td>
        </tr>";
            

        $table_hidden.= "
            <tr>  
                <td>".$idForm."</td>    
                <td>".utf8_encode($propietario_nombre)."</td>    
                <td>".$tipo_prop_display."</td>    
                <td>".$op_mostrar."</td>
                <td>".$direccion_display."</td>
                <td>".$comuna."</td>
                <td>".$sup_util."</td>
                <td>".$sup_total."</td>
                <td class='center-tab'".$dormitorios."</td>
                <td>".$cant_bannos."</td>
                <td>".$estacionamiento."</td>
                <td>".$formulario_monto1_peso."</td>
                <td>".$formulario_monto1_uf."</td>
                <td>".$fase."</td>
                <td>".$corredor."</td>
            </tr>
        ";

        // ------------------------------------------------------------------------------importancia Media--------------------------------------------------------------------------------------------------------------

    }else if($row["cont"] >= 20 && $row["cont"] <=30){
        $salida.= "
        <tr class='tr-style' >";

        $propietario = $row["fk_propietario"];
        $propietario_nombre = "";

        $sql_propietario = "SELECT Persona.nombre, Persona.apellido FROM Persona, Propietario WHERE Persona.id_persona = Propietario.fk_persona AND Propietario.id_propietario = $propietario;";
        $result_propietario = $conn->query($sql_propietario);

        if ($result_propietario->num_rows > 0) {
            while($row_propietario = $result_propietario->fetch_assoc()) {
                $propietario_n = $row_propietario["nombre"];
                $propietario_a = $row_propietario["apellido"];
                $propietario_nombre = $propietario_n." ".$propietario_a;
            }
        } else {

        }
        $nota = $row["nota"];
        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $telefono = utf8_encode($row["telefono"]) ;
        $correo = utf8_encode($row["correo"] );
        $fk_tipo_prop = $row["fk_tipo_propiedad"];
        $formulario_monto1 = $row["monto1"];
        $formulario_monto2 = $row["monto2"];
        
        $operacion = utf8_encode($row["operacion"]);
        $divisa_monto1 = utf8_encode($row["divisa_monto1"]);
        $divisa_monto2 = utf8_encode($row["divisa_monto2"]);
        
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row['fase']);
        
        
        $estacionamiento = utf8_encode($row["estacionamiento"]);
        $mTotales = utf8_encode($row["metros_totales"]);
        $mutiles = utf8_encode($row["metros_utiles"]);
        $bodega = utf8_encode($row["bodega"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row["fase"]);
        $dias = utf8_encode($row["cont"]);
        $idPropiedad = $row["idPropiedad"];
        $idExterno = $row["id_externo"];
        $ori = utf8_encode($row["ori"]);
        $idForm = $row["id_formulario"];
        $fecha = $row["fecha"];

        // datos

        //Tipo Prop

        switch ($fk_tipo_prop){
            case '1':
                $tipo_prop_display = "Depto.";
            break;
            case '2':
                $tipo_prop_display = "Casa";
            break;
            case '3':
                $tipo_prop_display = "Local Com.";
            break;
            case '4':
                $tipo_prop_display = "Of.";
            break;
            case '5':
                $tipo_prop_display = "Terr.";
            break;
            case '6':
                $tipo_prop_display = "Parc.";
            break;
            case '7':
                $tipo_prop_display = "Bod.";
            break;
            case '8':
                $tipo_prop_display = "Galp.";
            break;
            case '9':
                $tipo_prop_display = "No def.";
            break;
            case '12':
                $tipo_prop_display = "Casa Of.";
            break;
            case '13':
                $tipo_prop_display = "Depto. Ofi.";
            break;
            case '14':
                $tipo_prop_display = "Casa Terr.";
            break;
            case '15':
                $tipo_prop_display = "Casa Parc.";
            break;
            default: $tipo_prop_display = 'Undefined';
        }

        // OP

        if($operacion == 1){
            $op_mostrar = "Ven.";
        }else if($operacion == 2){
            $op_mostrar = "Arr.";
        }else{
            $op_mostrar = "Amb.";
        }

        //Direccion

        $calle = utf8_encode($row["calle"]);  
        $numero = utf8_encode($row["numero"]);

        if($letra != ""){
            $direccion_display = $calle." ".$numero." ".$letra;
        }else{
            $direccion_display = $calle." ".$numero;
        }

        //comuna

        $comuna = utf8_encode($row["Comuna"]);

        //superficie
        $sup_util = $row["superficie_util"];
        $sup_total = $row["superficie_total"];

        if($sup_util == ""){
            $sup_util = "S/D";
        }

        if($sup_total == ""){
            $sup_total = "S/D";
        }
        
        //habitaciones

        $dormitorios = utf8_encode($row["dormitorios"]); 

        if($dormitorios == ""){
            $dormitorios = "S/D";
        }

        //baños

        $cant_bannos = $row["cant_bannos"];

        if($cant_bannos == ""){
            $cant_bannos = "S/D";
        }

        //valor pesos || valor UF
        if($formulario_monto1 == "" || $formulario_monto1 == 0 || $divisa_monto1 == ""){
            $formulario_monto1_peso = "S/D";
            $formulario_monto1_uf = "S/D";
        }else{
            if($divisa_monto1 == "1"){
                $formulario_monto1_uf = moneda_chilena_sin_peso($formulario_monto1) . " UF";
                $formulario_monto1_peso = moneda_chilena($formulario_monto1 * $valor_uf);
            }else if ($divisa_monto1 == "2"){
                $formulario_monto1_peso = moneda_chilena($formulario_monto1);
                $formulario_monto1_uf = moneda_chilena_sin_peso(round($formulario_monto1 / $valor_uf)) . " UF";
            }
        }
        
        
        //estado-fase-idk

        //ejecutivo
        $fk_corredor = $row["fk_corredor"];
        if($fk_corredor != ""){
            $sql_corredor2 = "SELECT Persona.* FROM Persona, Corredor WHERE id_corredor = $fk_corredor AND Corredor.fk_persona = Persona.id_persona;";
            // echo $sql_corredor2;
            $result_corredor = $conn->query($sql_corredor2);

            if ($result_corredor->num_rows > 0) {
                while($row_corredor = $result_corredor->fetch_assoc()) {
                    $nom_corredor = $row_corredor["nombre"];
                    $ape_corredor = $row_corredor["apellido"];
                }
                $corredor = subStr($nom_corredor, 0, 1).subStr($ape_corredor, 0, 1);
                $corredor = strToUpper($corredor);
            } 
        }else {
            $corredor = "S/D";
        }

        if($estacionamiento == ""){
            $estacionamiento = "S/D";
        }

            //para el editar
        $prop = $row["fk_tipo_propiedad"];
        switch ($prop) {
            case '1':
                $prop = "depto";
                break;
            case '2':
                $prop = "casa";
                break;
            case '3':
                $prop = "local";
                break;
            case '4':
                $prop = "ofi";
                break;
        }

        $op = $row["operacion"];
        switch ($op) {
            case '1':
                $op = "ven";
                break;
            case '2':
                $op = "arr";
                break;
            case '3':
                $op = "amb";
                break;
        }

        
            $telDes =substr($telefono, 0, 3)." ".substr($telefono, 3, 1). " ".substr($telefono, 4);
            
            $salida .= $origen."
  
            <td class=''><pre class='tabPre'> <i class='fas fa-circle fa-xs ci-yel'></i>".$idForm."</pre></td>    
            <td class=''><pre class='tabPre'><a onclick='datosPropietario(event,".$propietario.")' href='#'>".utf8_encode($propietario_nombre)."</a></pre></td> 
            <td class=''><pre class='tabPre'>".$tipo_prop_display."</pre></td> 
            <td class=''><pre class='tabPre'>".$op_mostrar."</pre></td> 
            <td class=''><pre class='tabPre'>".$direccion_display."</pre></td> 
            <td class=''><pre class='tabPre'>".$comuna."</pre></td> 
            <td class=''><pre class='tabPre'>".$sup_util."</pre></td> 
            <td class=''><pre class='tabPre'>".$sup_total."</pre></td> 
            <td class=''><pre class='tabPre'>".$dormitorios."</pre></td> 
            <td class=''><pre class='tabPre'>".$cant_bannos."</pre></td>
            <td class=''><pre class='tabPre'>".$estacionamiento."</pre></td>
            <td class=''><pre class='tabPre'>".$formulario_monto1_peso."</pre></td> 
            <td class=''><pre class='tabPre'>".$formulario_monto1_uf."</pre></td>";

            $conta ++;

            // Contactos input

            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
            $thisFase = "";
                if($row['fase'] == "Contacto"){
                    $salida.="<td class='contacto' id='select_".$row["idPer"]."'>
                            <select class= 'btn btn-r dropdown-toggle' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }else{
                    $salida.="<td class=''id='select_".$row["idPer"]."'>
                            <select class= 'btn btn-r dropdown-toggle' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }
                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        // $table_hidden.="<td>".$row2['fase'] ."</td>";
                        // $table_hidden.="<td>".$row['contactos'] ."</td>";
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                    }
                } 
                // echo "<script>console.log($this)</script>"
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='btn-border' id='input_".$row['idPropietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['idPropietario'].")' >";
                }

                $sqlEvent = "Select COUNT(*) as event from Calendar where fk_propiedad = $idPropiedad";
                $result = $conn->query($sqlEvent);
    
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                      $event = $row["event"];  
                    }
                }
                
                $salida.="<td class=''><pre class='tabPre'>".$corredor."</pre></td>
    
                <td class=''>
                    <div class='btn-group dropleft'>
                        <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            <i class='fas fa-cog'></i>
                        </button>
                    <div class='dropdown-menu'>
                    <a class='dropdown-item' href='ControladorPlantillasEditar.php?id=".$idForm."&source=2&prop=".$prop."&op=".$op."'><i class='fas fa-edit'></i> Modificar</a>
                    <a class='dropdown-item' href='' onClick='desPropiedad(".$idPropiedad."); return false;'><i class='fas fa-user-slash'></i> Deshabilitar</a>
                    
                    <a class='dropdown-item' href='' onclick='redirigir($idForm, `2`);return false;'><i class='far fa-file-alt'></i> Ver formulario</a>";

                    if(strlen($nota) > 0 || $event != 0){
                        $salida.="
                        <a class='dropdown-item  bg-redflip-orange' href='notas.php?id=".$idPropiedad."'><i class='far fa-sticky-note'></i> Notas</a>";
                    }else{
                        $salida.="
                        <a class='dropdown-item' href='' onclick='cambiarEstatus($idForm); return false;' ><i class='fas fa-sync'></i> Cambiar Estatus</a>
                        <a class='dropdown-item ' href='notas.php?id=".$idPropiedad."'><i class='far fa-sticky-note'></i> Notas</a>";
                    }
                    $salida.="
                </td>
            </tr>";

        $table_hidden.= "
            <tr>  
                <td>".$idForm."</td>    
                <td>".utf8_encode($propietario_nombre)."</td>    
                <td>".$tipo_prop_display."</td>    
                <td>".$op_mostrar."</td>
                <td>".$direccion_display."</td>
                <td>".$comuna."</td>
                <td>".$sup_util."</td>
                <td>".$sup_total."</td>
                <td class='center-tab'".$dormitorios."</td>
                <td>".$cant_bannos."</td>
                <td>".$estacionamiento."</td>
                <td>".$formulario_monto1_peso."</td>
                <td>".$formulario_monto1_uf."</td>
                <td>".$fase."</td>
                <td>".$corredor."</td>
            </tr>
        ";
    // ----------------------------------------------Importancia Alta----------------------------------------------------------------------------------------------------------
    }else if($row["cont"] > 30){
        $salida.= "
        <tr class='tr-style' >";
        
        $propietario = $row["fk_propietario"];
        $propietario_nombre = "";

        $sql_propietario = "SELECT Persona.nombre, Persona.apellido FROM Persona, Propietario WHERE Persona.id_persona = Propietario.fk_persona AND Propietario.id_propietario = $propietario;";
        $result_propietario = $conn->query($sql_propietario);

        if ($result_propietario->num_rows > 0) {
            while($row_propietario = $result_propietario->fetch_assoc()) {
                $propietario_n = $row_propietario["nombre"];
                $propietario_a = $row_propietario["apellido"];
                $propietario_nombre = $propietario_n." ".$propietario_a;
            }
        } else {

        }
        $nota = $row["nota"];
        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $telefono = utf8_encode($row["telefono"]) ;
        $correo = utf8_encode($row["correo"] );
        $fk_tipo_prop = $row["fk_tipo_propiedad"];
        $formulario_monto1 = $row["monto1"];
        $formulario_monto2 = $row["monto2"];
        
        $operacion = utf8_encode($row["operacion"]);
        $divisa_monto1 = utf8_encode($row["divisa_monto1"]);
        $divisa_monto2 = utf8_encode($row["divisa_monto2"]);
        
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row['fase']);
        
        
        $estacionamiento = utf8_encode($row["estacionamiento"]);
        $mTotales = utf8_encode($row["metros_totales"]);
        $mutiles = utf8_encode($row["metros_utiles"]);
        $bodega = utf8_encode($row["bodega"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row["fase"]);
        $dias = utf8_encode($row["cont"]);
        $idPropiedad = $row["idPropiedad"];
        $idExterno = $row["id_externo"];
        $ori = utf8_encode($row["ori"]);
        $idForm = $row["id_formulario"];
        $fecha = $row["fecha"];

        // datos

        //Tipo Prop

        switch ($fk_tipo_prop){
            case '1':
                $tipo_prop_display = "Depto.";
            break;
            case '2':
                $tipo_prop_display = "Casa";
            break;
            case '3':
                $tipo_prop_display = "Local Com.";
            break;
            case '4':
                $tipo_prop_display = "Of.";
            break;
            case '5':
                $tipo_prop_display = "Terr.";
            break;
            case '6':
                $tipo_prop_display = "Parc.";
            break;
            case '7':
                $tipo_prop_display = "Bod.";
            break;
            case '8':
                $tipo_prop_display = "Galp.";
            break;
            case '9':
                $tipo_prop_display = "No def.";
            break;
            case '12':
                $tipo_prop_display = "Casa Of.";
            break;
            case '13':
                $tipo_prop_display = "Depto. Ofi.";
            break;
            case '14':
                $tipo_prop_display = "Casa Terr.";
            break;
            case '15':
                $tipo_prop_display = "Casa Parc.";
            break;
            default: $tipo_prop_display = 'Undefined';
        }

        // OP

        if($operacion == 1){
            $op_mostrar = "Ven.";
        }else if($operacion == 2){
            $op_mostrar = "Arr.";
        }else{
            $op_mostrar = "Amb.";
        }

        //Direccion

        $calle = utf8_encode($row["calle"]);  
        $numero = utf8_encode($row["numero"]);

        if($letra != ""){
            $direccion_display = $calle." ".$numero." ".$letra;
        }else{
            $direccion_display = $calle." ".$numero;
        }

        //comuna

        $comuna = utf8_encode($row["Comuna"]);

        //superficie
        $sup_util = $row["superficie_util"];
        $sup_total = $row["superficie_total"];

        if($sup_util == ""){
            $sup_util = "S/D";
        }

        if($sup_total == ""){
            $sup_total = "S/D";
        }
        
        //habitaciones

        $dormitorios = utf8_encode($row["dormitorios"]); 

        if($dormitorios == ""){
            $dormitorios = "S/D";
        }

        //baños

        $cant_bannos = $row["cant_bannos"];

        if($cant_bannos == ""){
            $cant_bannos = "S/D";
        }

        //valor pesos || valor UF
        if($formulario_monto1 == "" || $formulario_monto1 == 0 || $divisa_monto1 == ""){
            $formulario_monto1_peso = "S/D";
            $formulario_monto1_uf = "S/D";
        }else{
            if($divisa_monto1 == "1"){
                $formulario_monto1_uf = moneda_chilena_sin_peso($formulario_monto1) . " UF";
                $formulario_monto1_peso = moneda_chilena($formulario_monto1 * $valor_uf);
            }else if ($divisa_monto1 == "2"){
                $formulario_monto1_peso = moneda_chilena($formulario_monto1);
                $formulario_monto1_uf = moneda_chilena_sin_peso(round($formulario_monto1 / $valor_uf)) . " UF";
            }
        }
        
        
        //estado-fase-idk

        //ejecutivo
        $fk_corredor = $row["fk_corredor"];
        if($fk_corredor != ""){
            $sql_corredor2 = "SELECT Persona.* FROM Persona, Corredor WHERE id_corredor = $fk_corredor AND Corredor.fk_persona = Persona.id_persona;";
            // echo $sql_corredor2;
            $result_corredor = $conn->query($sql_corredor2);

            if ($result_corredor->num_rows > 0) {
                while($row_corredor = $result_corredor->fetch_assoc()) {
                    $nom_corredor = $row_corredor["nombre"];
                    $ape_corredor = $row_corredor["apellido"];
                }
                $corredor = subStr($nom_corredor, 0, 1).subStr($ape_corredor, 0, 1);
                $corredor = strToUpper($corredor);
            } 
        }else {
            $corredor = "S/D";
        }

        if($estacionamiento == ""){
            $estacionamiento = "S/D";
        }
         
        $telDes =substr($telefono, 0, 3)." ".substr($telefono, 3, 1). " ".substr($telefono, 4);


        
        
       $salida .= $origen."

        <td class=''><pre class='tabPre'> <i class='fas fa-circle fa-xs ci-red'></i>".$idForm."</pre></td>    
        <td class=''><pre class='tabPre'><a onclick='datosPropietario(event,".$propietario.")' href='#'>".utf8_encode($propietario_nombre)."</a></pre></td> 
        <td class=''><pre class='tabPre'>".$tipo_prop_display."</pre></td> 
        <td class=''><pre class='tabPre'>".$op_mostrar."</pre></td> 
        <td class=''><pre class='tabPre'>".$direccion_display."</pre></td> 
        <td class=''><pre class='tabPre'>".$comuna."</pre></td> 
        <td class=''><pre class='tabPre'>".$sup_util."</pre></td> 
        <td class=''><pre class='tabPre'>".$sup_total."</pre></td> 
        <td class=''><pre class='tabPre'>".$dormitorios."</pre></td> 
        <td class=''><pre class='tabPre'>".$cant_bannos."</pre></td>
        <td class=''><pre class='tabPre'>".$estacionamiento."</pre></td>
        <td class=''><pre class='tabPre'>".$formulario_monto1_peso."</pre></td> 
        <td class=''><pre class='tabPre'>".$formulario_monto1_uf."</pre></td>";
        $conta ++;
         // Contactos input
            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
            $thisFase = "";
                if($row['fase'] == "Contacto"){
                    $salida.="<td class='contacto' id='select_".$row["idPer"]."'>
                                <select class= 'btn btn-r dropdown-toggle' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }else{
                $salida.="<td class='' id='select_".$row["idPer"]."'>
                                <select class= 'btn btn-r dropdown-toggle' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }
                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        // $table_hidden.="<td>".$row2['fase'] ."</td>";
                        // $table_hidden.="<td>".$row['contactos'] ."</td>";
                        
                        
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                        
                    }
                    
                } 
                // echo "<script>console.log($this)</script>"
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='btn-border' id='input_".$row['idPropietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['idPropietario'].")' >";
                }
                $sqlEvent = "Select COUNT(*) as event from Calendar where fk_propiedad = $idPropiedad";
                $result = $conn->query($sqlEvent);
    
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                      $event = $row["event"];  
                    }
                }

                $salida.="<td class='tab-center'>".$corredor."</td>
    
                <td class=''>
                    <div class='btn-group dropleft'>
                        <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            <i class='fas fa-cog'></i>
                        </button>
                    <div class='dropdown-menu'>
                    <a class='dropdown-item' href='editarFormulario.php?id=".$idForm."&source=2'><i class='fas fa-edit'></i> Modificar</a>
                    <a class='dropdown-item' href='' onClick='desPropiedad(".$idPropiedad."); return false;'><i class='fas fa-user-slash'></i> Deshabilitar</a>
                    
                    <a class='dropdown-item' href='' onclick='redirigir($idForm, `2`);return false;'><i class='far fa-file-alt'></i> Ver formulario</a>";

                    if(strlen($nota) > 0 || $event != 0){
                        $salida.="
                        <a class='dropdown-item  bg-redflip-orange' href='notas.php?id=".$idPropiedad."'><i class='far fa-sticky-note'></i> Notas</a>";
                    }else{
                        $salida.="
                        <a class='dropdown-item' href='' onclick='cambiarEstatus($idForm); return false;' ><i class='fas fa-sync'></i> Cambiar Estatus</a>
                        <a class='dropdown-item ' href='notas.php?id=".$idPropiedad."'><i class='far fa-sticky-note'></i> Notas</a>";
                    }
                    $salida.="
                </td>
            </tr>";

        $table_hidden.= "
        <tr>  
            <td>".$idForm."</td>    
            <td>".utf8_encode($propietario_nombre)."</td>    
            <td>".$tipo_prop_display."</td>    
            <td>".$op_mostrar."</td>
            <td>".$direccion_display."</td>
            <td>".$comuna."</td>
            <td>".$sup_util."</td>
            <td>".$sup_total."</td>
            <td class='center-tab'".$dormitorios."</td>
            <td>".$cant_bannos."</td>
            <td>".$estacionamiento."</td>
            <td>".$formulario_monto1_peso."</td>
            <td>".$formulario_monto1_uf."</td>
            <td>".$fase."</td>
            <td>".$corredor."</td>
        </tr>
        ";
    }
}

$salida.="</tbody></table>";

}else{
//Tablas preguntar de donde son aaa
    $salida.= "
    <table class='table table-responsive table-hover table-striped table-min' id='tabla_mixta' style='display:inline-table;'>
        <thead class='thead-dark tab-center'>
            <tr class=''>
                <th class='' scope='col'> ID </th>
                <th class='' scope='col'> Propietario </th>
                <th class='' scope='col'> Tipo </th>
                <th class='' scope='col'> OP </th>
                <th class='' scope='col'> Dirección </th>	
                <th class='' scope='col'> Comuna </th>
                <th class='' scope='col'> m<sup>2</sup> </th>
                <th class='' scope='col'> TOT </th>
                <th class='' scope='col'> HAB </th>
                <th class='' scope='col'> BA </th>
                <th class='' scope='col'> EST </th>
                <th class='' scope='col'> Valor $ </th>
                <th class='' scope='col'> UF </th>
                <th class='' scope='col'> Fase </th>
                <th class='' scope='col'> EJEC </th>	
            </tr>
        </thead>
        <tbody class=''>
            <tr>
                <td class= 'tab-center' colspan=15 > No hay Datos</td>
            </tr>    
        </tbody>
    </table>
";

$table_hidden= "


<table class='table table-striped' id='export_to_excel' style='display:none!important'>

        <thead>
            <tr class='thead-dark'>
                <th scope='col'> ID </th>
                <th scope='col'> Propietario </th>
                <th scope='col'> Tipo </th>
                <th scope='col'> OP </th>	
                <th scope='col'> Dirección </th>
                <th scope='col'> Comuna </th>
                <th scope='col'> m<sup>2</sup> </th>
                <th scope='col'> TOT </th>
                <th class='center-tab' scope='col'> HAB </th>
                <th scope='col'> BA </th>
                <th scope='col'> EST </th>
                <th scope='col'> Valor $ </th>
                <th scope='col'> UF </th>
                <th scope='col'> Fase </th>
                <th scope='col'> EJEC </th>	
            </tr>
        </thead>
    <tbody>

<tr>

<td class= 'propTabN center-tab' colspan=15 > No hay Datos</td>

</tr>
</tbody>
</table>";
}


// $salida.='
// <script>
//     tabla();
// </script>
// ';
echo $salida.$table_hidden;

$conn->close();

?>

