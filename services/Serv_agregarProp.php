<?
include '../conexion.php';
?>

<section>
  <div class="container ">
    <div class="row">
      <div class="colum col-lg-6">
        <div class="imgFondo"></div>

      </div>
      <div class="colum col-xs-12 col-sm-12 col-md-6 col-lg-6 containerPropietario">
        <form id="formulario" style="margin-bottom:150px;">
          <h3 class="h3-prop mb-50">Ingresar nuevo propietario</h3>

          <div class="form-group row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-">
              <label class="input-form" for="exampleInputEmail1">Nombre*</label>
              <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Nombre" value="">
              <div id="err-nom"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-">
              <label class="input-form" for="exampleInputPassword1">Apellido*</label>
              <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" value="">
              <div id="err-ape"></div>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-">
              <label class="input-form" for="exampleInputPassword1">Teléfono</label>
              <input type="text" class="form-control" id="tel" name="tel" placeholder="+56 9 1234 5678" value="">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-">
              <label class="input-form" for="exampleInputPassword1">Correo</label>
              <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo" value="">
            </div>
          </div>

          <div class="form-group row">
                      <div class="column col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <label class="input-form" for="exampleInputPassword1">Calle</label>
                        <input type="text" class="form-control" id="calle" placeholder="ej. Av. kennedy" name="calle" value="" ></input>
                      </div>
                      <div class="column col-xs-12 col-sm-12 col-md-2 col-lg-3">
                        <label class="input-form" for="exampleInputPassword1">Número</label>
                        <input type="text" class="form-control" id="numero" placeholder="Ej. 1..." name="numero" value="">
                      </div>

                      <div class="column col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <label class="input-form" for="exampleInputPassword1">Comuna</label> </br>
                          <select id="comuna" name="comuna" class="selectEditProp">
                            <?
                              $sqlComuna = "SELECT * FROM Comuna";
      
                              $result = $conn->query($sqlComuna);
                                if ($result->num_rows > 0) {
                                // output data of each row
                                  while($row = $result->fetch_assoc()) {
                                    if($fk_comuna == $row['id']){
                                      echo "<option value=". utf8_encode($row['id'])." selected>". utf8_encode($row['nombre'])."</option>";
                                    }else{
                                      echo "<option value=". utf8_encode($row['id'])." >". utf8_encode($row['nombre'])."</option>";  
                                    }
                                  }
                                }else{
                                }
                            ?>
                          </select>
                      </div>
                    </div>
                    <div class="form-group row">
                        <div class="column col-xs-12 col-sm-12 col-md-12 col-lg-12">
                          <label class="input-form" for="exampleInputPassword1">Referencia</label>
                          <input type="text" class="form-control" id="ref" placeholder="ej. metro manquehue" name="ref" value="">
                          </div>
                    </div>
                    <div class="form-group row">
                  <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                      <label class="input-form" for="exampleInputPassword1">Origen</label></br>
                        <select id="origen" name="origen" class="selectEditProp">
                          <?
                            $sql = "Select * from Origen";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {                                  
                                    echo "<option value=".$row['id_origen']." >".$row['nom_origen']."</option>";                                  
                                }
                            }
                          ?>
                        </select>
                  </div>
                </div>

          <button type="submit" id="ingresar" class="mt-30 btn-def btn-login bg-redflip-red">Guardar</button>

        </form>
      </div>
    </div>
  </div>
</section>