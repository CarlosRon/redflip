<head>

</head>
<body>
 


<table class="table table-striped" id="export_to_excel">

					<?
                        include '../conexion.php';

					$id=$_SESSION['id'];

                    $sql2 = 'SELECT DISTINCT Persona.id_persona , Persona.nombre , Persona.apellido , Persona.rut , Persona.fec_nac , Persona.telefono , Persona.correo , Propietario.cont , Sub_estado.fase, Estado_prop.tipo from Persona INNER JOIN Propietario , usuario, Estado_prop, Sub_estado where Persona.id_persona = usuario.fk_persona AND usuario.fk_estado_us = 1 AND Propietario.fk_persona = Persona.id_persona AND Estado_prop.id = Propietario.fk_estado_prop AND Sub_estado.id = Estado_prop.fk_sub_estado';
					
					$sql = 'SELECT per.nombre
                    , per.apellido
                    , per.telefono
                    , per.correo
                    , per.id_persona as idPer
                    , dir.calle
                    , dir.numero
                    , dir.referencia
                    , dir.id as idDir
                    , com.nombre as Comuna
                    , com.id as idCom
                    , vent.descripcion V_A
                    , vent.id as idVent
                    , divi.nombre as divisa
                    , divi.id as idDiv
                    , propiedad.monto
                    , propiedad.id_propiedad as idPropiedad
                    , est.tipo
                    , est.id as idEst
                    , sub.fase
                    , sub.id as idSub
                    , propietario.cont
                    , propietario.id_propietario as idPropietario
                    FROM Persona per 
                    INNER JOIN Propietario propietario
                    , Estado_prop est
                    , Sub_estado sub
                    , Propiedad propiedad
                    , Divisa divi, Direccion dir
                    , Comuna com
                    , Venta_Arriendo vent 
                    WHERE per.id_persona = propietario.fk_persona 
                    AND propietario.fk_estado_prop = est.id 
                    AND est.fk_sub_estado = sub.id 
                    AND propietario.id_propietario = propiedad.fk_propietario 
                    AND propiedad.divisa = divi.id 
                    AND propiedad.fk_direccion = dir.id 
                    and dir.fk_comuna = com.id 
                    AND propiedad.fk_venta_arriendo = vent.id
                    AND propiedad.fk_estado_propiedad = 1';	

			$result = $conn->query($sql);

if ($result->num_rows > 0) {
    ?>

<tr class="thead-dark">

    <th scope='col' class="th-cabecera"> Datos de propietario </th>
    <th scope='col'> # </th>
    <th scope='col'> Nombre </th>
    <th scope='col'> Apellido </th>	
    <th scope='col'> Teléfono </th>
    <th scope='col'> Correo </th>
    <th scope='col'> Dirección </th>
    <th scope='col'> Comuna </th>
    <th scope='col'> Venta o Arriendo </th>
    <th scope='col'> Precio </th>
    <th scope='col'> Estado </th>
    <th scope='col'> Fase </th>
    <th class="center-tab" scope='col'> Contador </th>
    <th class="center-tab" scope='col'> Acciones </th>	
</tr>
    <?
    $cont = 0;
    // output data of each row
    while($row = $result->fetch_assoc()) {
        ++$cont;
    // echo '<form id="formId"><input type="text" name='.$row["id_persona"].' value='.$row["id_persona"].' style="display:none"></form>';
    $num = mysqli_num_rows ( $result );
    // for($cont = 1; $cont<=$result->num_row; $cont++){
       
    echo "<tr class=' tb-data-single' >";
    echo "<td class='th-cabecera'></td>";
    echo "<td class='table-item'>".$cont."</td>";    
    echo "<td class='table-item'>" . utf8_encode($row["nombre"]) . "</td>";    
    echo "<td class='table-item'>" . utf8_encode($row["apellido"]) . "</td>";
    echo "<td class='table-item'>" . utf8_encode($row["telefono"]) . "</td>";
    echo "<td class='table-item'>" . utf8_encode($row["correo"] ). "</td>";
    echo "<td class='table-item'>" . utf8_encode($row["calle"]) ." ". utf8_encode($row["numero"]) ."</td>";
    echo "<td class='table-item'>" . utf8_encode($row["Comuna"]) . "</td>";
    echo "<td class="."' table-item ".utf8_encode($row["V_A"])."'".">" . utf8_encode($row["V_A"]) . "</td>";
    echo "<td class='table-item'>" . utf8_encode($row["monto"]) ." " . utf8_encode($row["divisa"]) . "</td>";
    echo "<td class='table-item'>" . utf8_encode($row["tipo"]) . "</td>";
    echo "<td class='table-item'>" . utf8_encode($row["fase"]) . "</td>";
    echo "<td class='center-tab'>" . utf8_encode($row["cont"]) . "</td>";
    echo '<td class=center-tab>
    <button class="btn-action bg-turquoise" onClick="EditarPropiedadR('.$row["idPropiedad"].')">editar</button>
    <button class="btn-action bg-darkgrey" onClick="desPropiedad('.$row["idPropiedad"].')">Deshabilitar</button>' ;
    echo "</tr>";
    
    }

} else {
	// echo "datos invalidos";
	// echo $row["id_usuario"];
	// echo $_SESSION['id'];
    // echo $result;
    ?>
    <tr class="thead-dark">
    <th scope='col' class="th-cabecera"> Datos de propietario </th>
    <th scope='col'> # </th>
    <th scope='col'> Nombre </th>
    <th scope='col'> Apellido </th>	
    <th scope='col'> Teléfono </th>
    <th scope='col'> Correo </th>
    <th scope='col'> Dirección </th>
    <th scope='col'> Comuna </th>
    <th scope='col'> Venta o Arriendo </th>
    <th scope='col'> Precio </th>
    <th scope='col'> Estado </th>
    <th scope='col'> Fase </th>
    <th class="center-tab" scope='col'> Contador </th>
    <th class="center-tab" scope='col'> Acciones </th>
    <tr>
    <td colspan="13" class='center-tab'>No hay registros</td>
    </tr>
    <?php
}$conn->close();
	?>

</table>
<form action="process.php" method="post" target="_blank" id="formExport">
                <input type="hidden" id="data_to_send" name="data_to_send" />
                <input type="hidden" id="nombre" name="nombre" value="Propietarios VS Propiedades" />
            </form>
<button type="submit" id="submitExport">export</button>

<script src="../js/stacktable.js"></script>
<script src="../js/accionProp.js"></script>
<script>
if(screen.width < 1200){
     $('table').stacktable();
    }
    console.log(screen.width);
</script>
<script>
document.getElementById('submitExport').addEventListener('click', function(e) {
    e.preventDefault();
    let export_to_excel = document.getElementById('export_to_excel');
    let data_to_send = document.getElementById('data_to_send');
    data_to_send.value = export_to_excel.outerHTML;
    document.getElementById('formExport').submit();
});
</script>
<script src="../js/isotope.pkgd.min.js"></script>
<script>

</script>
</body>
