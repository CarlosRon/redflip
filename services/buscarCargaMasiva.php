<?php 
include "../conexion.php";
include "funciones.php";

$salida = "";
$conta = 1;

if(isset($_POST["iniciar"]) && isset($_POST["cantidad"])){

    // $iniciar = $_POST["iniciar"];
    // $cantidad = $_POST["cantidad"];
    $query = "SELECT per.nombre
    , per.apellido
    , per.telefono
    , per.correo
    , per.id_persona as idPer
    , dir.calle
    , dir.numero
    , dir.referencia
    , dir.id as idDir
    , com.nombre as Comuna
    , com.id as idCom
    , vent.descripcion V_A
    , vent.id as idVent
    , divi.nombre as divisa
    , divi.id as idDiv
    , propiedad.id_propiedad as idPropiedad
    , propiedad.fk_propietario
    , propiedad.nota
    , propiedad.monto
    , propiedad.id_externo
    , est.tipo
    , est.id as idEst
    , sub.fase
    , sub.id as idSub
    , propietario.cont
    , propietario.id_propietario as idPropietario
    , propietario.contactos
    , Origen.nom_origen as ori
    , Origen.id_origen as id_or
    , formulario.*
    , Status.status
    FROM Persona per
    INNER JOIN Propietario propietario
    , Estado_prop est
    , Sub_estado sub
    , Propiedad propiedad
    , Divisa divi, Direccion dir
    , Comuna com    
    , Venta_Arriendo vent
    , Origen    
    , Status 
    , formulario
    WHERE per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propietario.id_propietario = propiedad.fk_propietario
    AND propiedad.divisa = divi.id
    AND propiedad.fk_direccion = dir.id
    and dir.fk_comuna = com.id
    AND propiedad.fk_venta_arriendo = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND propiedad.fk_status = Status.id_status
    AND propiedad.fk_estado_propiedad = 1
    order by propiedad.id_propiedad asc";
}else{

    // $query = "SELECT distinct per.nombre
    // , per.apellido
    // , per.telefono
    // , per.correo
    // , per.id_persona as idPer
    // , dir.calle
    // , dir.numero
    // , dir.referencia
    // , dir.id as idDir
    // , com.nombre as Comuna
    // , com.id as idCom
    // , vent.descripcion V_A
    // , vent.id as idVent
    // , divi.nombre as divisa
    // , divi.id as idDiv
    // , propiedad.id_propiedad as idPropiedad
    // , propiedad.fk_propietario
    // , propiedad.nota
    // , propiedad.monto
    // , propiedad.id_externo
    // , est.tipo
    // , est.id as idEst
    // , sub.fase
    // , sub.id as idSub
    // , propietario.cont
    // , propietario.id_propietario as idPropietario
    // , propietario.contactos
    // , Origen.nom_origen as ori
    // , Origen.id_origen as id_or
    // , formulario.*
    // , Status.status
    // FROM Persona per
    // INNER JOIN Propietario propietario
    // , Estado_prop est
    // , Sub_estado sub
    // , Propiedad propiedad
    // , Divisa divi, Direccion dir
    // , Comuna com    
    // , Venta_Arriendo vent
    // , Origen    
    // , Status 
    // , formulario
    // WHERE per.id_persona = propietario.fk_persona
    // -- AND propietario.fk_estado_prop = est.id
    // -- AND est.fk_sub_estado = sub.id
    // -- AND propietario.id_propietario = propiedad.fk_propietario
    // -- AND propiedad.divisa = divi.id
    // -- AND propiedad.fk_direccion = dir.id
    // -- and dir.fk_comuna = com.id
    // -- AND propiedad.fk_venta_arriendo = vent.id
    // AND propietario.fk_origen = 9
    // -- AND propietario.fk_origen = Origen.id_origen
    // -- AND propiedad.fk_status = Status.id_status
    // AND propiedad.fk_estado_propiedad = 1
    // AND propiedad.fk_formulario = formulario.id_formulario
    // AND formulario.aprobado = 1
    // AND propiedad.fk_estado_propiedad = 3
    // order by propiedad.id_propiedad asc";
// AND formulario.aprobado = 1 --Replicar en todas las consultas
$query = "SELECT distinct per.nombre
    , per.apellido
    , per.telefono
    , per.correo
    , per.id_persona as idPer
    , dir.calle
    , dir.numero
    , dir.referencia
    , dir.id as idDir
    , com.nombre as Comuna
    , com.id as idCom
    , vent.descripcion V_A
    , vent.id as idVent
    , propiedad.id_propiedad as idPropiedad
    , propiedad.fk_propietario
    , propiedad.nota
    , propiedad.monto
    , propiedad.id_externo
    , est.tipo
    , est.id as idEst
    , sub.fase
    , sub.id as idSub
    , propietario.cont
    , propietario.id_propietario as idPropietario
    , propietario.contactos
    , Origen.nom_origen as ori
    , Origen.id_origen as id_or
    , formulario.*
    , Estatus.status
    FROM Persona per
    INNER JOIN Propietario propietario
    , Estado_prop est
    , Sub_estado sub
    , Propiedad propiedad
    , Direccion dir
    , Comuna com    
    , Venta_Arriendo vent
    , Origen    
    , Estatus 
    , formulario
    where per.id_persona = propietario.fk_persona
    AND propietario.fk_estado_prop = est.id
    AND est.fk_sub_estado = sub.id
    AND propiedad.fk_formulario = formulario.id_formulario
    AND	formulario.fk_propietario = propietario.id_propietario
    AND formulario.aprobado = 1
    AND propietario.fk_origen = 9
    AND propiedad.carga = 1    
    AND formulario.Direccion_id = dir.id
    AND dir.fk_comuna = com.id
    AND formulario.operacion = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND Estatus.id_status = propiedad.fk_status
    order by formulario.id_formulario asc";
}



if(isset($_POST['consulta1']) && isset($_POST['consulta2']) && isset($_POST['consulta3']) && isset($_POST['consulta4'])){

    $q1 = $conn -> real_escape_string($_POST['consulta1']);
    $q1 = utf8_decode($q1);
    $q2 = $conn -> real_escape_string($_POST['consulta2']);
    $q3 = $conn -> real_escape_string($_POST['consulta3']);
    $q4 = $conn -> real_escape_string($_POST['consulta4']);
    
    if($q4 == 'Alta'){

        // $query = "SELECT per.nombre
        // , per.apellido
        // , per.telefono
        // , per.correo
        // , per.id_persona as idPer
        // , dir.calle
        // , dir.numero
        // , dir.referencia
        // , dir.id as idDir
        // , com.nombre as Comuna
        // , com.id as idCom
        // , vent.descripcion V_A
        // , vent.id as idVent
        // , divi.nombre as divisa
        // , divi.id as idDiv
        // , propiedad.id_propiedad as idPropiedad
        // , propiedad.fk_propietario
        // , propiedad.nota
        // , propiedad.monto
        // , propiedad.id_externo
        // , est.tipo
        // , est.id as idEst
        // , sub.fase
        // , sub.id as idSub
        // , propietario.cont
        // , propietario.id_propietario as idPropietario
        // , propietario.contactos
        // , Origen.nom_origen as ori
        // , Origen.id_origen as id_or
        // , formulario.*
        // , Status.status
        // FROM Persona per
        // INNER JOIN Propietario propietario
        // , Estado_prop est
        // , Sub_estado sub
        // , Propiedad propiedad
        // , Divisa divi, Direccion dir
        // , Comuna com    
        // , Venta_Arriendo vent
        // , Origen    
        // , Status 
        // , formulario
        // WHERE per.id_persona = propietario.fk_persona 
        // AND propietario.fk_estado_prop = est.id
        // AND est.fk_sub_estado = sub.id
        // AND propietario.id_propietario = propiedad.fk_propietario
        // AND propiedad.divisa = divi.id
        // AND propiedad.fk_direccion = dir.id
        // and dir.fk_comuna = com.id
        // AND propiedad.fk_venta_arriendo = vent.id
        // AND propietario.fk_origen = Origen.id_origen
        // AND propiedad.fk_status = Status.id_status
        // AND propiedad.fk_estado_propiedad = 1
        // AND vent.descripcion LIKE" ."'%" .$q1. "%'
        // AND est.tipo LIKE" ."'%" .$q2. "%'
        // AND sub.fase LIKE" ."'%" .$q3. "%'
        // AND propietario.cont > 31
        // AND propiedad.fk_formulario = formulario.id_formulario
        // AND formulario.aprobado = 1
        // order by propiedad.id_propiedad asc";

        $query="SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status        
        AND vent.descripcion LIKE" ."'%" .$q1. "%'
        AND est.tipo LIKE" ."'%" .$q2. "%'
        AND sub.fase LIKE" ."'%" .$q3. "%'
        AND propietario.cont > 31
        order by formulario.id_formulario asc";

    }else if($q4 == "Media"){

        $q4 = 20;

        // $query = "SELECT per.nombre
        // , per.apellido
        // , per.telefono
        // , per.correo
        // , per.id_persona as idPer
        // , dir.calle
        // , dir.numero
        // , dir.referencia
        // , dir.id as idDir
        // , com.nombre as Comuna
        // , com.id as idCom
        // , vent.descripcion V_A
        // , vent.id as idVent
        // , divi.nombre as divisa
        // , divi.id as idDiv
        // , propiedad.id_propiedad as idPropiedad
        // , propiedad.fk_propietario
        // , propiedad.nota
        // , propiedad.monto
        // , propiedad.id_externo
        // , est.tipo
        // , est.id as idEst
        // , sub.fase
        // , sub.id as idSub
        // , propietario.cont
        // , propietario.id_propietario as idPropietario
        // , propietario.contactos
        // , Origen.nom_origen as ori
        // , Origen.id_origen as id_or
        // , formulario.*
        // , Status.status
        // FROM Persona per
        // INNER JOIN Propietario propietario
        // , Estado_prop est
        // , Sub_estado sub
        // , Propiedad propiedad
        // , Divisa divi, Direccion dir
        // , Comuna com    
        // , Venta_Arriendo vent
        // , Origen    
        // , Status 
        // , formulario
        // WHERE per.id_persona = propietario.fk_persona
        // AND propietario.fk_estado_prop = est.id
        // AND est.fk_sub_estado = sub.id
        // AND propietario.id_propietario = propiedad.fk_propietario
        // AND propiedad.divisa = divi.id
        // AND propiedad.fk_direccion = dir.id
        // and dir.fk_comuna = com.id
        // AND propiedad.fk_venta_arriendo = vent.id
        // AND propietario.fk_origen = Origen.id_origen
        // AND propiedad.fk_estado_propiedad = 1
        // AND propiedad.fk_status = Status.id_status
        // AND vent.descripcion LIKE" ."'%" .$q1. "%'
        // AND est.tipo LIKE" ."'%" .$q2. "%' 
        // AND sub.fase LIKE" ."'%" .$q3. "%'
        // AND propietario.cont >= $q4
        // AND propietario.cont <=30
        // AND propiedad.fk_formulario = formulario.id_formulario
        // AND formulario.aprobado = 1
        // order by propiedad.id_propiedad asc"; 

        $query = "SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status
        AND vent.descripcion LIKE" ."'%" .$q1. "%'
        AND est.tipo LIKE" ."'%" .$q2. "%' 
        AND sub.fase LIKE" ."'%" .$q3. "%'
        AND propietario.cont >= $q4
        AND propietario.cont <=30
        order by formulario.id_formulario asc";

    }else if($q4 == "Baja"){

        // $query = "SELECT per.nombre
        // , per.apellido
        // , per.telefono
        // , per.correo
        // , per.id_persona as idPer
        // , dir.calle
        // , dir.numero
        // , dir.referencia
        // , dir.id as idDir
        // , com.nombre as Comuna
        // , com.id as idCom
        // , vent.descripcion V_A
        // , vent.id as idVent
        // , divi.nombre as divisa
        // , divi.id as idDiv
        // , propiedad.id_propiedad as idPropiedad
        // , propiedad.fk_propietario
        // , propiedad.nota
        // , propiedad.monto
        // , propiedad.id_externo
        // , est.tipo
        // , est.id as idEst
        // , sub.fase
        // , sub.id as idSub
        // , propietario.cont
        // , propietario.id_propietario as idPropietario
        // , propietario.contactos
        // , Origen.nom_origen as ori
        // , Origen.id_origen as id_or
        // , formulario.*
        // , Status.status
        // FROM Persona per
        // INNER JOIN Propietario propietario
        // , Estado_prop est
        // , Sub_estado sub
        // , Propiedad propiedad
        // , Divisa divi, Direccion dir
        // , Comuna com    
        // , Venta_Arriendo vent
        // , Origen    
        // , Status 
        // , formulario
        // WHERE per.id_persona = propietario.fk_persona
        // AND propietario.fk_estado_prop = est.id
        // AND est.fk_sub_estado = sub.id
        // AND propietario.id_propietario = propiedad.fk_propietario
        // AND propiedad.divisa = divi.id
        // AND propiedad.fk_direccion = dir.id
        // and dir.fk_comuna = com.id
        // AND propiedad.fk_venta_arriendo = vent.id
        // AND propietario.fk_origen = Origen.id_origen
        // AND propiedad.fk_estado_propiedad = 1
        // AND propiedad.fk_status = Status.id_status
        // AND vent.descripcion LIKE" ."'%" .$q1. "%'
        // AND est.tipo LIKE" ."'%" .$q2. "%'
        // AND sub.fase LIKE" ."'%" .$q3. "%'
        // AND propietario.cont >= 0
        // AND propietario.cont <20
        // AND propiedad.fk_formulario = formulario.id_formulario
        // AND formulario.aprobado = 1
        // order by propiedad.id_propiedad asc"; 
        $query = "SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status
        AND vent.descripcion LIKE" ."'%" .$q1. "%'
        AND est.tipo LIKE" ."'%" .$q2. "%'
        AND sub.fase LIKE" ."'%" .$q3. "%'
        AND propietario.cont >= 0
        AND propietario.cont <20
        order by formulario.id_formulario asc";

    }else{

        // $query = "SELECT per.nombre
        // , per.apellido
        // , per.telefono
        // , per.correo
        // , per.id_persona as idPer
        // , dir.calle
        // , dir.numero
        // , dir.referencia
        // , dir.id as idDir
        // , com.nombre as Comuna
        // , com.id as idCom
        // , vent.descripcion V_A
        // , vent.id as idVent
        // , divi.nombre as divisa
        // , divi.id as idDiv
        // , propiedad.id_propiedad as idPropiedad
        // , propiedad.fk_propietario
        // , propiedad.nota
        // , propiedad.monto
        // , propiedad.id_externo
        // , est.tipo
        // , est.id as idEst
        // , sub.fase
        // , sub.id as idSub
        // , propietario.cont
        // , propietario.id_propietario as idPropietario
        // , propietario.contactos
        // , Origen.nom_origen as ori
        // , Origen.id_origen as id_or
        // , formulario.*
        // , Status.status
        // FROM Persona per
        // INNER JOIN Propietario propietario
        // , Estado_prop est
        // , Sub_estado sub
        // , Propiedad propiedad
        // , Divisa divi, Direccion dir
        // , Comuna com    
        // , Venta_Arriendo vent
        // , Origen    
        // , Status 
        // , formulario
        // WHERE per.id_persona = propietario.fk_persona
        // AND propietario.fk_estado_prop = est.id
        // AND est.fk_sub_estado = sub.id
        // AND propietario.id_propietario = propiedad.fk_propietario
        // AND propiedad.divisa = divi.id
        // AND propiedad.fk_direccion = dir.id
        // and dir.fk_comuna = com.id
        // AND propiedad.fk_venta_arriendo = vent.id
        // AND propietario.fk_origen = Origen.id_origen
        // AND propiedad.fk_estado_propiedad = 1
        // AND propiedad.fk_status = Status.id_status
        // AND vent.descripcion LIKE" ."'%" .$q1. "%'
        // AND est.tipo LIKE" ."'%" .$q2. "%'
        // AND sub.fase LIKE" ."'%" .$q3. "%'
        // AND propiedad.fk_formulario = formulario.id_formulario
        // AND formulario.aprobado = 1
        // order by propiedad.id_propiedad asc ";

        $query = "SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status
        AND vent.descripcion LIKE" ."'%" .$q1. "%'
        AND est.tipo LIKE" ."'%" .$q2. "%'
        AND sub.fase LIKE" ."'%" .$q3. "%'
        order by formulario.id_formulario asc";

    }

}else if(isset($_POST['consulta1']) && isset($_POST['consulta2']) && isset($_POST['consulta3']) && isset($_POST['consulta4']) && isset($_POST['consulta5'])){



    $q1 = $conn -> real_escape_string($_POST['consulta1']);
    $q1 = utf8_decode($q1);
    $q2 = $conn -> real_escape_string($_POST['consulta2']);
    $q3 = $conn -> real_escape_string($_POST['consulta3']);
    $q4 = $conn -> real_escape_string($_POST['consulta4']);
    $q5 = $conn -> real_escape_string($_POST['consulta5']);

    // $query = "SELECT per.nombre
    // , per.apellido
    // , per.telefono
    // , per.correo
    // , per.id_persona as idPer
    // , dir.calle
    // , dir.numero
    // , dir.referencia
    // , dir.id as idDir
    // , com.nombre as Comuna
    // , com.id as idCom
    // , vent.descripcion V_A
    // , vent.id as idVent
    // , divi.nombre as divisa
    // , divi.id as idDiv
    // , propiedad.id_propiedad as idPropiedad
    // , propiedad.fk_propietario
    // , propiedad.nota
    // , propiedad.monto
    // , propiedad.id_externo
    // , est.tipo
    // , est.id as idEst
    // , sub.fase
    // , sub.id as idSub
    // , propietario.cont
    // , propietario.id_propietario as idPropietario
    // , propietario.contactos
    // , Origen.nom_origen as ori
    // , Origen.id_origen as id_or
    // , formulario.*
    // , Status.status
    // FROM Persona per
    // INNER JOIN Propietario propietario
    // , Estado_prop est
    // , Sub_estado sub
    // , Propiedad propiedad
    // , Divisa divi, Direccion dir
    // , Comuna com    
    // , Venta_Arriendo vent
    // , Origen    
    // , Status 
    // , formulario
    // WHERE per.id_persona = propietario.fk_persona
    // AND propietario.fk_estado_prop = est.id
    // AND est.fk_sub_estado = sub.id
    // AND propietario.id_propietario = propiedad.fk_propietario
    // AND propiedad.divisa = divi.id
    // AND propiedad.fk_direccion = dir.id
    // and dir.fk_comuna = com.id
    // AND propiedad.fk_venta_arriendo = vent.id
    // AND propietario.fk_origen = Origen.id_origen
    // AND propiedad.fk_estado_propiedad = 1
    // AND propiedad.fk_status = Status.id_status
    // AND( vent.descripcion LIKE" ."'" .$q1. "%'
    // OR per.nombre LIKE" ."'" .$q1. "%'
    // OR per.apellido LIKE" ."'" .$q1. "%'
    // OR per.telefono LIKE" ."'" .$q1. "%'
    // OR per.correo LIKE" ."'" .$q1. "%'
    // OR dir.calle LIKE" ."'" .$q1. "%'
    // OR com.nombre LIKE" ."'" .$q1. "%'
    // OR propiedad.monto LIKE" ."'" .$q1. "%'
    // OR est.tipo LIKE" ."'" .$q1. "%'
    // OR sub.fase LIKE" ."'" .$q1. "%'
    // OR Status.status LIKE" ."'" .$q1. "%'
    // OR vent.descripcion LIKE" ."'%" .$q1. "%')
    // AND est.tipo LIKE" ."'%" .$q2. "%'
    // AND sub.fase LIKE" ."'%" .$q3. "%'
    // AND propiedad.fk_formulario = formulario.id_formulario   
    // AND formulario.aprobado = 1 
    // order by propiedad.id_propiedad asc";


    $query = "SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status
        AND( vent.descripcion LIKE" ."'" .$q1. "%'
        OR per.nombre LIKE" ."'" .$q1. "%'
        OR per.apellido LIKE" ."'" .$q1. "%'
        OR per.telefono LIKE" ."'" .$q1. "%'
        OR per.correo LIKE" ."'" .$q1. "%'
        OR dir.calle LIKE" ."'" .$q1. "%'
        OR com.nombre LIKE" ."'" .$q1. "%'
        OR propiedad.monto LIKE" ."'" .$q1. "%'
        OR est.tipo LIKE" ."'" .$q1. "%'
        OR sub.fase LIKE" ."'" .$q1. "%'
        OR Estatus.status LIKE" ."'" .$q1. "%'
        OR vent.descripcion LIKE" ."'%" .$q1. "%')
        AND est.tipo LIKE" ."'%" .$q2. "%'
        AND sub.fase LIKE" ."'%" .$q3. "%'
        order by formulario.id_formulario asc";
    
}else if(isset($_POST['consulta1'])){

    $q1 = $conn -> real_escape_string($_POST['consulta1']);
    $q1 = utf8_decode($q1);
    // echo "<script>console.log('$q1')</script>";

    // $query = "SELECT per.nombre
    // , per.apellido
    // , per.telefono
    // , per.correo
    // , per.id_persona as idPer
    // , dir.calle
    // , dir.numero
    // , dir.referencia
    // , dir.id as idDir
    // , com.nombre as Comuna
    // , com.id as idCom
    // , vent.descripcion V_A
    // , vent.id as idVent
    // , divi.nombre as divisa
    // , divi.id as idDiv
    // , propiedad.id_propiedad as idPropiedad
    // , propiedad.fk_propietario
    // , propiedad.nota
    // , propiedad.monto
    // , propiedad.id_externo
    // , est.tipo
    // , est.id as idEst
    // , sub.fase
    // , sub.id as idSub
    // , propietario.cont
    // , propietario.id_propietario as idPropietario
    // , propietario.contactos
    // , Origen.nom_origen as ori
    // , Origen.id_origen as id_or
    // , formulario.*
    // , Status.status
    // FROM Persona per
    // INNER JOIN Propietario propietario
    // , Estado_prop est
    // , Sub_estado sub
    // , Propiedad propiedad
    // , Divisa divi, Direccion dir
    // , Comuna com    
    // , Venta_Arriendo vent
    // , Origen    
    // , Status 
    // , formulario
    // WHERE per.id_persona = propietario.fk_persona
    // AND propietario.fk_estado_prop = est.id
    // AND est.fk_sub_estado = sub.id
    // AND propietario.id_propietario = propiedad.fk_propietario
    // AND propiedad.divisa = divi.id
    // AND propiedad.fk_direccion = dir.id
    // and dir.fk_comuna = com.id
    // AND propiedad.fk_venta_arriendo = vent.id
    // AND propietario.fk_origen = Origen.id_origen
    // AND propiedad.fk_estado_propiedad = 1
    // AND propiedad.fk_status = Status.id_status
    // AND( vent.descripcion LIKE" ."'" .$q1. "%'
    // OR per.nombre LIKE" ."'" .$q1. "%'
    // OR per.apellido LIKE" ."'" .$q1. "%'
    // OR per.telefono LIKE" ."'" .$q1. "%'
    // OR per.correo LIKE" ."'" .$q1. "%'
    // OR dir.calle LIKE" ."'" .$q1. "%'
    // OR com.nombre LIKE" ."'" .$q1. "%'
    // OR propiedad.monto LIKE" ."'" .$q1. "%'
    // OR est.tipo LIKE" ."'" .$q1. "%'
    // OR sub.fase LIKE" ."'" .$q1. "%'
    // OR Status.status LIKE" ."'" .$q1. "%' ) 
    // AND propiedad.fk_formulario = formulario.id_formulario 
    // AND formulario.aprobado = 1  
    // order by propiedad.id_propiedad asc";

    $query = "SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status
        AND( vent.descripcion LIKE" ."'" .$q1. "%'
        OR per.nombre LIKE" ."'" .$q1. "%'
        OR per.apellido LIKE" ."'" .$q1. "%'
        OR per.telefono LIKE" ."'" .$q1. "%'
        OR per.correo LIKE" ."'" .$q1. "%'
        OR dir.calle LIKE" ."'" .$q1. "%'
        OR com.nombre LIKE" ."'" .$q1. "%'
        OR propiedad.monto LIKE" ."'" .$q1. "%'
        OR est.tipo LIKE" ."'" .$q1. "%'
        OR sub.fase LIKE" ."'" .$q1. "%'
        OR Estatus.status LIKE" ."'" .$q1. "%' ) 
        order by formulario.id_formulario asc";

}else if(isset($_POST["filtro"]) && $_POST["filtro"] == "popup"){
    $operacion = $_POST["operacion"];
    $estado = $_POST["estado"];
    $fase = $_POST["fase"];
    $importancia = $_POST["importancia"];
    $comuna = $_POST["comuna"];
    $tipo_prop = $_POST["tipo_prop"];
    $habitaciones = $_POST["habitaciones"];
    $bannos = $_POST["bannos"];
    $precioMin = $_POST["precioMin"];
    $precioMax = $_POST["precioMax"];
    $orden = $_POST["orden"];
    $origen = $_POST["origen"];
    $status = $_POST["status"];
    

    //----------------------
    if($importancia == 'Alta'){

        // $query = "SELECT per.nombre
        // , per.apellido
        // , per.telefono
        // , per.correo
        // , per.id_persona as idPer
        // , dir.calle
        // , dir.numero
        // , dir.referencia
        // , dir.id as idDir
        // , com.nombre as Comuna
        // , com.id as idCom
        // , vent.descripcion V_A
        // , vent.id as idVent
        // , divi.nombre as divisa
        // , divi.id as idDiv
        // , propiedad.id_propiedad as idPropiedad
        // , propiedad.fk_propietario
        // , propiedad.nota
        // , propiedad.monto
        // , propiedad.fk_status
        // , propiedad.id_externo
        // , est.tipo
        // , est.id as idEst
        // , sub.fase
        // , sub.id as idSub
        // , propietario.cont
        // , propietario.id_propietario as idPropietario
        // , propietario.contactos
        // , propietario.fk_origen
        // , Origen.nom_origen as ori
        // , Origen.id_origen as id_or
        // , formulario.*
        // , Status.status
        // , propiedad.fk_tipo_propiedad
        // FROM Persona per
        // INNER JOIN Propietario propietario
        // , Estado_prop est
        // , Sub_estado sub
        // , Propiedad propiedad
        // , Divisa divi, Direccion dir
        // , Comuna com    
        // , Venta_Arriendo vent
        // , Origen    
        // , Status 
        // , formulario
        // WHERE per.id_persona = propietario.fk_persona 
        // AND propietario.fk_estado_prop = est.id
        // AND est.fk_sub_estado = sub.id
        // AND propietario.id_propietario = propiedad.fk_propietario
        // AND propiedad.divisa = divi.id
        // AND propiedad.fk_direccion = dir.id
        // and dir.fk_comuna = com.id
        // AND propiedad.fk_venta_arriendo = vent.id
        // AND propietario.fk_origen = Origen.id_origen
        // AND propiedad.fk_status = Status.id_status
        // AND propiedad.fk_estado_propiedad = 1
        // AND vent.descripcion LIKE '%$operacion'
        // AND est.tipo LIKE '%$estado'
        // AND sub.fase LIKE '%$fase'
        // AND propietario.cont > 31
        // AND propiedad.fk_formulario = formulario.id_formulario
        // AND formulario.aprobado = 1";
        $query = "SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status
        AND vent.descripcion LIKE '%$operacion'
        AND est.tipo LIKE '%$estado'
        AND sub.fase LIKE '%$fase'
        AND propietario.cont > 31
        AND propiedad.fk_formulario = formulario.id_formulario";
        if($precioMin != "" && $precioMax != ""){
            $query .=" AND(formulario.valor_clp >= '$precioMin' AND  formulario.valor_clp <= '$precioMax' OR formulario.valor_uf >= '$precioMin' AND  formulario.valor_uf <= '$precioMax') ";
        }
        if($bannos != ""){
            $query .=" AND formulario.cant_bannos = $bannos ";
        }
        if($habitaciones != ""){
            $query .=" AND formulario.dormitorios = $habitaciones ";
        }
        if($tipo_prop != ""){
            $query.=" AND propiedad.fk_tipo_propiedad = $tipo_prop ";
        }
        if($comuna != ""){
            $query.=" AND com.id = $comuna";
        }
        if($origen != ""){
            $query .=" AND propietario.fk_origen = $origen ";
        }
        if($status != ""){
            $query .= " AND propiedad.fk_status = $status";
        }
        if($orden != ""){
            switch ($orden) {
                case 1:
                    $query.=" order by formulario.valor_clp desc";
                    break;
                case 2:
                    $query.=" order by formulario.valor_uf desc";
                    break;
                case 3:
                    $query.=" order by formulario.valor_clp asc";
                    break;
                case 4:
                    $query.=" order by formulario.valor_uf asc";
                    break;
                case 5:
                    $query.=" order by formulario.dormitorios asc";
                    break;
                case 6:
                    $query.=" order by formulario.dormitorios desc";
                    break;
                case 7:
                    $query.=" order by formulario.cant_bannos asc";
                    break;
                case 8:
                    $query.=" order by formulario.cant_bannos desc";
                    break;
            }
        }else{
            $query .=" order by propiedad.id_propiedad asc";
        }
        
        
        

    }else if($importancia == "Media"){

        $importancia = 20;

        // $query = "SELECT per.nombre
        // , per.apellido
        // , per.telefono
        // , per.correo
        // , per.id_persona as idPer
        // , dir.calle
        // , dir.numero
        // , dir.referencia
        // , dir.id as idDir
        // , com.nombre as Comuna
        // , com.id as idCom
        // , vent.descripcion V_A
        // , vent.id as idVent
        // , divi.nombre as divisa
        // , divi.id as idDiv
        // , propiedad.id_propiedad as idPropiedad
        // , propiedad.fk_propietario
        // , propiedad.nota
        // , propiedad.monto
        // , propiedad.fk_status
        // , est.tipo
        // , est.id as idEst
        // , sub.fase
        // , sub.id as idSub
        // , propietario.cont
        // , propietario.id_propietario as idPropietario
        // , propietario.fk_origen
        // , propietario.contactos
        // , Origen.nom_origen as ori
        // , Origen.id_origen as id_or
        // , formulario.*
        // , Status.status
        // FROM Persona per
        // INNER JOIN Propietario propietario
        // , Estado_prop est
        // , Sub_estado sub
        // , Propiedad propiedad
        // , Divisa divi, Direccion dir
        // , Comuna com    
        // , Venta_Arriendo vent
        // , Origen    
        // , Status 
        // , formulario
        // WHERE per.id_persona = propietario.fk_persona
        // AND propietario.fk_estado_prop = est.id
        // AND est.fk_sub_estado = sub.id
        // AND propietario.id_propietario = propiedad.fk_propietario
        // AND propiedad.divisa = divi.id
        // AND propiedad.fk_direccion = dir.id
        // and dir.fk_comuna = com.id
        // AND propiedad.fk_venta_arriendo = vent.id
        // AND propietario.fk_origen = Origen.id_origen
        // AND propiedad.fk_estado_propiedad = 1
        // AND propiedad.fk_status = Status.id_status
        // AND vent.descripcion LIKE '%$operacion'
        // AND est.tipo LIKE '%$estado'
        // AND sub.fase LIKE '%$fase'
        // AND propietario.cont >= $importancia
        // AND propiedad.fk_formulario = formulario.id_formulario";

        $query = "SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status
        AND vent.descripcion LIKE '%$operacion'
        AND est.tipo LIKE '%$estado'
        AND sub.fase LIKE '%$fase'
        AND propietario.cont >= $importancia
        AND propiedad.fk_formulario = formulario.id_formulario";
        if($precioMin != "" && $precioMax != ""){
            $query .=" AND(formulario.valor_clp >= '$precioMin' AND  formulario.valor_clp <= '$precioMax' OR formulario.valor_uf >= '$precioMin' AND  formulario.valor_uf <= '$precioMax') ";
        }
        if($bannos != ""){
            $query .=" AND formulario.cant_bannos = $bannos ";
        }
        if($habitaciones != ""){
            $query .=" AND formulario.dormitorios = $habitaciones ";
        }
        if($tipo_prop != ""){
            $query.=" AND propiedad.fk_tipo_propiedad = $tipo_prop ";
        }
        if($comuna != ""){
            $query.=" AND com.id = $comuna";
        }
        if($origen != ""){
            $query .=" AND propietario.fk_origen = $origen ";
        }
        if($status != ""){
            $query .= " AND propiedad.fk_status = $status";
        }
        if($orden != ""){
            switch ($orden) {
                case 1:
                    $query.=" order by formulario.valor_clp desc";
                    break;
                case 2:
                    $query.=" order by formulario.valor_uf desc";
                    break;
                case 3:
                    $query.=" order by formulario.valor_clp asc";
                    break;
                case 4:
                    $query.=" order by formulario.valor_uf asc";
                    break;
                case 5:
                    $query.=" order by formulario.dormitorios asc";
                    break;
                case 6:
                    $query.=" order by formulario.dormitorios desc";
                    break;
                case 7:
                    $query.=" order by formulario.cant_bannos asc";
                    break;
                case 8:
                    $query.=" order by formulario.cant_bannos desc";
                    break;
            }
        }else{
            $query .=" order by propiedad.id_propiedad asc";
        }
       

    }else if($importancia == "Baja"){

        // $query = "SELECT per.nombre
        // , per.apellido
        // , per.telefono
        // , per.correo
        // , per.id_persona as idPer
        // , dir.calle
        // , dir.numero
        // , dir.referencia
        // , dir.id as idDir
        // , com.nombre as Comuna
        // , com.id as idCom
        // , vent.descripcion V_A
        // , vent.id as idVent
        // , divi.nombre as divisa
        // , divi.id as idDiv
        // , propiedad.id_propiedad as idPropiedad
        // , propiedad.fk_propietario
        // , propiedad.nota
        // , propiedad.monto
        // , propiedad.fk_status
        // , est.tipo
        // , est.id as idEst
        // , sub.fase
        // , sub.id as idSub
        // , propietario.cont
        // , propietario.id_propietario as idPropietario
        // , propietario.contactos
        // , propietario.fk_origen
        // , Origen.nom_origen as ori
        // , Origen.id_origen as id_or
        // , formulario.*
        // , Status.status
        // FROM Persona per
        // INNER JOIN Propietario propietario
        // , Estado_prop est
        // , Sub_estado sub
        // , Propiedad propiedad
        // , Divisa divi, Direccion dir
        // , Comuna com    
        // , Venta_Arriendo vent
        // , Origen    
        // , Status 
        // , formulario
        // WHERE per.id_persona = propietario.fk_persona
        // AND propietario.fk_estado_prop = est.id
        // AND est.fk_sub_estado = sub.id
        // AND propietario.id_propietario = propiedad.fk_propietario
        // AND propiedad.divisa = divi.id
        // AND propiedad.fk_direccion = dir.id
        // and dir.fk_comuna = com.id
        // AND propiedad.fk_venta_arriendo = vent.id
        // AND propietario.fk_origen = Origen.id_origen
        // AND propiedad.fk_estado_propiedad = 1
        // AND propiedad.fk_status = Status.id_status
        // AND vent.descripcion LIKE '%$operacion'
        // AND est.tipo LIKE '%$estado'
        // AND sub.fase LIKE '%$fase'
        // AND propiedad.fk_formulario = formulario.id_formulario";

        $query = "SELECT distinct per.nombre
        , per.apellido
        , per.telefono
        , per.correo
        , per.id_persona as idPer
        , dir.calle
        , dir.numero
        , dir.referencia
        , dir.id as idDir
        , com.nombre as Comuna
        , com.id as idCom
        , vent.descripcion V_A
        , vent.id as idVent
        , propiedad.id_propiedad as idPropiedad
        , propiedad.fk_propietario
        , propiedad.nota
        , propiedad.monto
        , propiedad.id_externo
        , est.tipo
        , est.id as idEst
        , sub.fase
        , sub.id as idSub
        , propietario.cont
        , propietario.id_propietario as idPropietario
        , propietario.contactos
        , Origen.nom_origen as ori
        , Origen.id_origen as id_or
        , formulario.*
        , Estatus.status
        FROM Persona per
        INNER JOIN Propietario propietario
        , Estado_prop est
        , Sub_estado sub
        , Propiedad propiedad
        , Direccion dir
        , Comuna com    
        , Venta_Arriendo vent
        , Origen    
        , Estatus 
        , formulario
        where per.id_persona = propietario.fk_persona
        AND propiedad.fk_formulario = formulario.id_formulario
        AND	formulario.fk_propietario = propietario.id_propietario
        AND formulario.aprobado = 1
        AND propietario.fk_origen = 9
        AND est.fk_sub_estado = sub.id
        AND propietario.fk_estado_prop = est.id
        AND formulario.Direccion_id = dir.id
        AND dir.fk_comuna = com.id
        AND formulario.operacion = vent.id
        AND propietario.fk_origen = Origen.id_origen
        AND Estatus.id_status = propiedad.fk_status
        AND vent.descripcion LIKE '%$operacion'
        AND est.tipo LIKE '%$estado'
        AND sub.fase LIKE '%$fase'
        AND propiedad.fk_formulario = formulario.id_formulario";

        if($precioMin != "" && $precioMax != ""){
            $query .=" AND(formulario.valor_clp >= '$precioMin' AND  formulario.valor_clp <= '$precioMax' OR formulario.valor_uf >= '$precioMin' AND  formulario.valor_uf <= '$precioMax') ";
        }
        if($bannos != ""){
            $query .=" AND formulario.cant_bannos = $bannos ";
        }
        if($habitaciones != ""){
            $query .=" AND formulario.dormitorios = $habitaciones ";
        }
        if($tipo_prop != ""){
            $query.=" AND propiedad.fk_tipo_propiedad = $tipo_prop ";
        }
        if($comuna != ""){
            $query.=" AND com.id = $comuna";
        }
        if($origen != ""){
            $query .=" AND propietario.fk_origen = $origen ";
        }
        if($status != ""){
            $query .= " AND propiedad.fk_status = $status";
        }
        if($orden != ""){
            switch ($orden) {
                case 1:
                    $query.=" order by formulario.valor_clp desc";
                    break;
                case 2:
                    $query.=" order by formulario.valor_uf desc";
                    break;
                case 3:
                    $query.=" order by formulario.valor_clp asc";
                    break;
                case 4:
                    $query.=" order by formulario.valor_uf asc";
                    break;
                case 5:
                    $query.=" order by formulario.dormitorios asc";
                    break;
                case 6:
                    $query.=" order by formulario.dormitorios desc";
                    break;
                case 7:
                    $query.=" order by formulario.cant_bannos asc";
                    break;
                case 8:
                    $query.=" order by formulario.cant_bannos desc";
                    break;
            }
        }else{
            $query .=" order by propiedad.id_propiedad asc";
        }
    }else{
    //----------------------
    // $query = "SELECT per.nombre
    //     , per.apellido
    //     , per.telefono
    //     , per.correo
    //     , per.id_persona as idPer
    //     , dir.calle
    //     , dir.numero
    //     , dir.referencia
    //     , dir.id as idDir
    //     , com.nombre as Comuna
    //     , com.id as idCom
    //     , vent.descripcion V_A
    //     , vent.id as idVent
    //     , divi.nombre as divisa
    //     , divi.id as idDiv
    //     , propiedad.id_propiedad as idPropiedad
    //     , propiedad.fk_propietario
    //     , propiedad.nota
    //     , propiedad.monto
    //     , propiedad.fk_status
    //     , est.tipo
    //     , est.id as idEst
    //     , sub.fase
    //     , sub.id as idSub
    //     , propietario.cont
    //     , propietario.id_propietario as idPropietario
    //     , propietario.contactos
    //     , propietario.fk_origen
    //     , Origen.nom_origen as ori
    //     , Origen.id_origen as id_or
    //     , formulario.*
    //     , Status.status
    //     FROM Persona per
    //     INNER JOIN Propietario propietario
    //     , Estado_prop est
    //     , Sub_estado sub
    //     , Propiedad propiedad
    //     , Divisa divi, Direccion dir
    //     , Comuna com    
    //     , Venta_Arriendo vent
    //     , Origen    
    //     , Status 
    //     , formulario
    //     WHERE per.id_persona = propietario.fk_persona
    //     AND propietario.fk_estado_prop = est.id
    //     AND est.fk_sub_estado = sub.id
    //     AND propietario.id_propietario = propiedad.fk_propietario
    //     AND propiedad.divisa = divi.id
    //     AND propiedad.fk_direccion = dir.id
    //     and dir.fk_comuna = com.id
    //     AND propiedad.fk_venta_arriendo = vent.id
    //     AND propietario.fk_origen = Origen.id_origen
    //     AND propiedad.fk_estado_propiedad = 1
    //     AND propiedad.fk_status = Status.id_status
    //     AND vent.descripcion LIKE '%$operacion'
    //     AND est.tipo LIKE '%$estado'
    //     AND sub.fase LIKE '%$fase'
    //     AND propiedad.fk_formulario = formulario.id_formulario";

    
    $query = "SELECT distinct per.nombre
    , per.apellido
    , per.telefono
    , per.correo
    , per.id_persona as idPer
    , dir.calle
    , dir.numero
    , dir.referencia
    , dir.id as idDir
    , com.nombre as Comuna
    , com.id as idCom
    , vent.descripcion V_A
    , vent.id as idVent
    , propiedad.id_propiedad as idPropiedad
    , propiedad.fk_propietario
    , propiedad.nota
    , propiedad.monto
    , propiedad.id_externo
    , est.tipo
    , est.id as idEst
    , sub.fase
    , sub.id as idSub
    , propietario.cont
    , propietario.id_propietario as idPropietario
    , propietario.contactos
    , Origen.nom_origen as ori
    , Origen.id_origen as id_or
    , formulario.*
    , Estatus.status
    FROM Persona per
    INNER JOIN Propietario propietario
    , Estado_prop est
    , Sub_estado sub
    , Propiedad propiedad
    , Direccion dir
    , Comuna com    
    , Venta_Arriendo vent
    , Origen    
    , Estatus 
    , formulario
    where per.id_persona = propietario.fk_persona
    AND propiedad.fk_formulario = formulario.id_formulario
    AND	formulario.fk_propietario = propietario.id_propietario
    AND formulario.aprobado = 1
    AND propietario.fk_origen = 9
    AND est.fk_sub_estado = sub.id
    AND propietario.fk_estado_prop = est.id
    AND formulario.Direccion_id = dir.id
    AND dir.fk_comuna = com.id
    AND formulario.operacion = vent.id
    AND propietario.fk_origen = Origen.id_origen
    AND Estatus.id_status = propiedad.fk_status
    AND vent.descripcion LIKE '%$operacion'
    AND est.tipo LIKE '%$estado'
    AND sub.fase LIKE '%$fase'
    AND propiedad.fk_formulario = formulario.id_formulario";

        if($precioMin != "" && $precioMax != ""){
            $query .=" AND(formulario.valor_clp >= '$precioMin' AND  formulario.valor_clp <= '$precioMax' OR formulario.valor_uf >= '$precioMin' AND  formulario.valor_uf <= '$precioMax') ";
        }
        if($bannos != ""){
            $query .=" AND formulario.cant_bannos = $bannos ";
        }
        if($habitaciones != ""){
            $query .=" AND formulario.dormitorios = $habitaciones ";
        }
        if($tipo_prop != ""){
            $query.="AND propiedad.fk_tipo_propiedad = $tipo_prop ";
        }
        if($comuna != ""){
            $query.=" AND com.id = $comuna";
        }
        if($origen != ""){
            $query .=" AND propietario.fk_origen = $origen ";
        }
        if($status != ""){
            $query .= " AND propiedad.fk_status = $status";
        }
        if($orden != ""){
            switch ($orden) {
                case 1:
                    $query.=" order by formulario.valor_clp desc";
                    break;
                case 2:
                    $query.=" order by formulario.valor_uf desc";
                    break;
                case 3:
                    $query.=" order by formulario.valor_clp asc";
                    break;
                case 4:
                    $query.=" order by formulario.valor_uf asc";
                    break;
                case 5:
                    $query.=" order by formulario.dorm itorios asc";
                    break;
                case 6:
                    $query.=" order by formulario.dormitorios desc";
                    break;
                case 7:
                    $query.=" order by formulario.cant_bannos asc";
                    break;
                case 8:
                    $query.=" order by formulario.cant_bannos desc";
                    break;
            }
        }else{
            $query .=" order by propiedad.id_propiedad asc";
        }
    }

}
echo $query;
$resultado = $conn->query($query);

if($resultado->num_rows >0){

    $salida.= "<table class='table tab-pvpActivo table-striped' id='tabla_mixta'>

            <thead>
                <tr class='thead-dark trProp'>
                    <th class='thProp stackTab tabOri' scope='col'> origen </th>
                    <th class='thProp stackTab tabCont center-tab' scope='col'> # </th>
                    <th class='thProp stackTab tabNom ' scope='col'> Nombre </th>	
                    <th class='thProp stackTab tabTel' scope='col'> Teléfono </th>
                    <th class='thProp stackTab tabMail' scope='col'> Correo </th>
                    <th class='thProp stackTab tabDir' scope='col'> Dirección </th>
                    <th class='thProp stackTab tabCom' scope='col'> Comuna </th>
                    <th class='thProp stackTab tabPr' scope='col'> Precio </th>
                    <th class='thProp stackTab tabOp center-tab' scope='col'> Operación </th>
                    <th class='thProp stackTab tabSt' scope='col'> Status </th>
                    <th class='thProp stackTab tabEst' scope='col'> Estado </th>
                    <th class='thProp stackTab tabFas' scope='col'> Fase </th>
                    <th class='thProp stackTab tabDias center-tab' scope='col'> Días </th>
                    <th class='thProp stackTab tabAcc center-tab' scope='col'> Acciones </th>	
                </tr>
            </thead>
        <tbody>
        ";

$table_hidden= "

        <table class='table table-striped' id='export_to_excel' style='display:none'>

        <thead>
            <tr class='thead-dark .trProp'>
                <th class='thProp stackTab' scope='col'> origen </th>
                <th class='thProp stackTab' scope='col'> Nombre </th>
                <th class='thProp stackTab' scope='col'> Apellido </th>	
                <th class='thProp stackTab' scope='col'> Teléfono </th>
                <th class='thProp stackTab' scope='col'> Correo </th>
                <th class='thProp stackTab' scope='col'> Dirección </th>
                <th class='thProp stackTab' scope='col'> Comuna </th>
                <th class='thProp stackTab center-tab' scope='col'> Tipo de Operación </th>
                <th class='thProp stackTab' scope='col'> Monto </th>
                <th class='thProp stackTab' scope='col'> Divisa </th>
                <th class='thProp stackTab' scope='col'> Dormitorios </th>
                <th class='thProp stackTab' scope='col'> Baños </th>
                <th class='thProp stackTab' scope='col'> Estacionamiento </th>
                <th class='thProp stackTab' scope='col'> Metros Totales </th>
                <th class='thProp stackTab' scope='col'> Metros Utiles </th>
                <th class='thProp stackTab' scope='col'> Bodega </th>
                <th class='thProp stackTab' scope='col'> Status </th>
                <th class='thProp stackTab' scope='col'> Estado </th>
                <th class='thProp stackTab' scope='col'> Fase </th>	
            </tr>
        </thead>
    <tbody> 
    ";

    
while($row = $resultado-> fetch_assoc()){
//    -------------------------------------------------------------------importancia baja------------------------------------------------------------------------------------------
    if($row["cont"]< 20){
        $salida.= "
        <tr class=' trProp tb-data-single' >";

        switch ($row["id_or"]) {
            case 1:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-instagram' title='Instagram'></i></td>";
                    break;

                case 2:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                    break;

                case 3:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                    break;

                case 4:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-globe-americas' title='Web'></i></td>";
                    break;

                case 5:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Conserje'></i></td>";
                    break;

                case 6:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Cliente'></i></td>";
                    break;

                case 7:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Corredor'></i></td>";
                    break;

                case 8:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Redflip'></i></td>";
                    break;

                case 9:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='DaHunter'></i></td>";
                    break;

                case 10:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='BD Mailing'></i></td>";
                    break;

                case 11:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='Doorlist'></i></td>";
                    break;

                case 12:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='Open Casa'></i></td>";
                    break;

                case 13:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='GI0'></i></td>";
                    break;

                case 14:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='Vilenky'></i></td>";
                    break;
                case 15:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                    break;

                case 16:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                    break;
                default:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                    break;
            }
        
        $propietario = $row["fk_propietario"];
        $nota = $row["nota"];
        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $telefono = utf8_encode($row["telefono"]) ;
        $correo = utf8_encode($row["correo"] );
        $calle = utf8_encode($row["calle"]);  
        $numero = utf8_encode($row["numero"]);
        $comuna = utf8_encode($row["Comuna"]);
        $operacion = utf8_encode($row["operacion"]);
        if($operacion == "1"){
            $monto = $row["valor_uf"];
            $divisa = "UF";
            $operacion = "Venta";
        }else if($operacion == "2"){
            $monto = $row["valor_clp"];
            $divisa = "CLP";
            $operacion = "Arriendo";
        }else if($operacion == "3"){
            $monto = $row["valor_uf"];
            $divisa = "UF";
            $operacion = "Ambos";
        }else{
            $monto = $row["valor_uf"];
            $divisa = "UF";
            $operacion = "S/D";
        }
        
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row['fase']);
        $dormitorios = utf8_encode($row["dormitorios"]);
        $baños = utf8_encode($row["bannos"]);
        $estacionamiento = utf8_encode($row["estacionamiento"]);
        $mTotales = utf8_encode($row["metros_totales"]);
        $mutiles = utf8_encode($row["metros_utiles"]);
        $bodega = utf8_encode($row["bodega"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row["fase"]);
        $dias = utf8_encode($row["cont"]);
        $idPropiedad = $row["idPropiedad"];
        $idExterno = $row["id_externo"];
        $ori = utf8_encode($row["ori"]);
        $idForm = $row["id_formulario"];
        

        $telDes =substr($telefono, 0, 3)." ".substr($telefono, 3, 1). " ".substr($telefono, 4);
        $salida .= $origen."

            <td class='tdProp stackTab tabCont'> <i class='fas fa-circle fa-xs ci-gre'></i>".$idExterno."</td>    
            <td class='tdProp stackTab tabNom'>" . $nombre ." ". $apellido."</td> 
            <td class='tdProp stackTab tabTel'>" . $telDes . "</td>
            <td class='tdProp stackTab tabMail' >" . $correo. "</td>
            <td class='tdProp stackTab tabDir'>" . $calle ." ". $numero ."</td>
            <td class='tdProp stackTab tabCom'>" . $comuna . "</td>
            <td class='tdProp stackTab tabPr'>" . moneda_chilena($monto) ." " . $divisa . "</td>
            <td class='tdProp stackTab tabOp center-tab'".$operacion."'".">" . $operacion . "</td>            
            <td class='tdProp stackTab tabSt'>" . $status . "</td>
            <td class='tdProp stackTab tabEst'>" .$tipo . "</td>";
            $conta ++;
            // Contactos input
            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
            $thisFase = "";
                if($row['fase'] == "Contacto"){
                    $salida.="
                    <td class='tdProp stackTab tabFas' id='select_".$row["idPer"]."'>
                        <select class='selectFase contact' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }else{
                $salida.="
                    <td class='tdProp stackTab tabFas' id='select_".$row["idPer"]."'>
                        <select class= 'selectFase' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
            }
                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        // $table_hidden.="<td>".$row2['fase'] ."</td>";
                        // $table_hidden.="<td>".$row['contactos'] ."</td>";
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                    }
                } 
                // echo "<script>console.log($this)</script>"
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='number' id='input_".$row['idPropietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['idPropietario'].")' >";
                }
                
            $sqlEvent = "Select COUNT(*) as event from Calendar where fk_propiedad = $idPropiedad";
            $result = $conn->query($sqlEvent);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                  $event = $row["event"];  
                }
            }
            

            $salida.="<td class='tdProp stackTab tabDias center-tab'>" . $dias . "</td>

            <td class='tdProp stackTab tabAcc center-tab pb-10'>
                <button class='btn-acciones bg-redflip-turquoise min-p' onClick=EditarPropiedadR('".$idPropiedad."') title='Modificar' ><i class='fas fa-edit'></i></button>
                
                ";
                // if(strlen($nota) > 0 || $event != 0){
                //     $salida.="<button class='btn-acciones bg-redflip-orange min-p' onClick=verNotas('".$idPropiedad."') title='Notas' ><i class='far fa-sticky-note'></i></button>";
                // }else{
                // $salida.="<button class='btn-acciones bg-redflip-yellow min-p' onClick=verNotas('".$idPropiedad."') title='Notas' ><i class='far fa-sticky-note'></i></button>";
                // }
                $salida.="
                <button class='btn-acciones bg-redflip-form min-p' title='Ver formulario' onclick='redirigir($idForm, `2`)'><i class='fas fa-check'></i></button>
            </td>
        </tr>
        ";

        $table_hidden.= "
            <tr>  
                <td>" .$ori  . "</td>    
                <td>" . $nombre . "</td>    
                <td>" . $apellido . "</td>
                <td>" . $telefono . "</td>
                <td>" . $correo. "</td>
                <td>" . $calle." ". $numero ."</td>
                <td>" . $comuna . "</td>
                <td class='center-tab'".$operacion."'".">" . $operacion . "</td>
                <td>" . moneda_chilena($monto) ."</td>
                <td>" . $divisa . "</td>
                <td>" . $dormitorios . "</td>
                <td>" . $baños . "</td>
                <td>" . $estacionamiento . "</td>
                <td>" . $mTotales . "</td>
                <td>" . $mutiles . "</td>
                <td>" . $bodega. "</td>
                <td>" . $status . "</td>
                <td>" . $tipo . "</td>
                <td>" . $fase . "</td>
            </tr>
        ";

        // ------------------------------------------------------------------------------importancia Media--------------------------------------------------------------------------------------------------------------

    }else if($row["cont"] >= 20 && $row["cont"] <=30){
        $salida.= "
        <tr class='trProp tb-data-single' >";
        switch ($row["id_or"]) {
            case 1:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-instagram' title='Instagram'></i></td>";
                    break;

                case 2:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                    break;

                case 3:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                    break;

                case 4:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-globe-americas' title='Web'></i></td>";
                    break;

                case 5:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Conserje'></i></td>";
                    break;

                case 6:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Cliente'></i></td>";
                    break;

                case 7:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Corredor'></i></td>";
                    break;

                case 8:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Redflip'></i></td>";
                    break;

                case 9:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='DaHunter'></i></td>";
                    break;

                case 10:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='BD Mailing'></i></td>";
                    break;

                case 11:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='Doorlist'></i></td>";
                    break;

                case 12:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='Open Casa'></i></td>";
                    break;

                case 13:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='GI0'></i></td>";
                    break;

                case 14:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='Vilenky'></i></td>";
                    break;
                case 15:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                    break;

                case 16:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                    break;
                default:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                    break;
            }

            $propietario = $row["fk_propietario"];
            $nota = $row["nota"];
            $nombre = utf8_encode($row["nombre"]);
            $apellido = utf8_encode($row["apellido"]);
            $telefono = utf8_encode($row["telefono"]) ;
            $correo = utf8_encode($row["correo"] );
            $calle = utf8_encode($row["calle"]);  
            $numero = utf8_encode($row["numero"]);
            $comuna = utf8_encode($row["Comuna"]);
            $operacion = utf8_encode($row["operacion"]);
            if($operacion == "1"){
                $monto = $row["valor_uf"];
                $divisa = "UF";
                $operacion = "Venta";
            }else if($operacion == "2"){
                $monto = $row["valor_clp"];
                $divisa = "CLP";
                $operacion = "Arriendo";
            }else if($operacion == "3"){
                $monto = $row["valor_uf"];
                $divisa = "UF";
                $operacion = "Ambos";
            }else{
                $monto = $row["valor_uf"];
                $divisa = "UF";
                $operacion = "S/D";
            }
            $status = utf8_encode($row["status"]);
            $tipo = utf8_encode($row["tipo"]);
            $fase = utf8_encode($row['fase']);
            $dormitorios = utf8_encode($row["dormitorios"]);
            $baños = utf8_encode($row["bannos"]);
            $estacionamiento = utf8_encode($row["estacionamiento"]);
            $mTotales = utf8_encode($row["metros_totales"]);
            $mutiles = utf8_encode($row["metros_utiles"]);
            $bodega = utf8_encode($row["bodega"]);
            $status = utf8_encode($row["status"]);
            $tipo = utf8_encode($row["tipo"]);
            $fase = utf8_encode($row["fase"]);
            $dias = utf8_encode($row["cont"]);
            $idPropiedad = $row["idPropiedad"];
            $idExterno = $row["id_externo"];
            $ori = utf8_encode($row["ori"]);
            $idForm = $row["id_formulario"];

            $telDes =substr($telefono, 0, 3)." ".substr($telefono, 3, 1). " ".substr($telefono, 4);
            
            $salida .= $origen."

            <td class='tdProp stackTab tabCont'> <i class='fas fa-circle fa-xs ci-yel'></i>".$idExterno."</td>    
            <td class='tdProp stackTab tabNom'>" . $nombre ." ". $apellido."</td>
            <td class='tdProp stackTab tabTel'>" . $telDes. "</td>
            <td class='tdProp stackTab tabMail' >" . $correo. "</td>
            <td class='tdProp stackTab tabDir'>" . $calle ." ". $numero ."</td>
            <td class='tdProp stackTab tabCom'>" . $comuna . "</td>
            <td class='tdProp stackTab tabPr'>" . moneda_chilena($monto) ." " . $divisa . "</td>
            <td class='tdProp stackTab tabOp center-tab'".$operacion."'".">" . $operacion . "</td>            
            <td class='tdProp stackTab tabSt'>" . $status . "</td>
            <td class='tdProp stackTab tabEst'>" . $tipo . "</td>";

            $conta ++;

            // Contactos input

            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
            $thisFase = "";
                if($row['fase'] == "Contacto"){
                    $salida.="<td class='tdProp stackTab tabFas' id='select_".$row["idPer"]."'>
                                <select class= 'selectFase contact' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }else{
                $salida.="<td class='tdProp stackTab tabFas'id='select_".$row["idPer"]."'>
                            <select class= 'selectFase' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }
                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        // $table_hidden.="<td>".$row2['fase'] ."</td>";
                        // $table_hidden.="<td>".$row['contactos'] ."</td>";
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                    }
                } 
                // echo "<script>console.log($this)</script>"
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='number' id='input_".$row['idPropietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['idPropietario'].")' >";
                }

                $sqlEvent = "Select COUNT(*) as event from Calendar where fk_propiedad = $idPropiedad";
                $result = $conn->query($sqlEvent);
    
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                      $event = $row["event"];  
                    }
                }
                
                $salida.="<td class='tdProp stackTab tabDias center-tab'>" . $dias . "</td>
    
                <td class='tdProp stackTab tabAcc center-tab pb-10'>
                    <button class='btn-acciones bg-redflip-turquoise min-p' onClick=EditarPropiedadR('".$idPropiedad."') title='Modificar' ><i class='fas fa-edit'></i></button>
                    <button class='btn-acciones bg-redflip-darkgrey min-p' onClick=desPropiedad('".$idPropiedad."') title='Deshabilitar' ><i class='fas fa-user-slash'></i></button>
                    ";
                    if(strlen($nota) > 0 || $event != 0){
                        $salida.="<button class='btn-acciones bg-redflip-orange min-p' onClick=verNotas('".$idPropiedad."') title='Notas' ><i class='far fa-sticky-note'></i></button>";
                    }else{
                    $salida.="<button class='btn-acciones bg-redflip-yellow min-p' onClick=verNotas('".$idPropiedad."') title='Notas' ><i class='far fa-sticky-note'></i></button>";
                    }
                    $salida.="
                    <button class='btn-acciones bg-redflip-form min-p' title='Ver formulario' onclick='redirigir($idForm, `2`)'><i class='far fa-file-alt'></i></button>
                </td>
        </tr>
        ";

        $table_hidden.= "
            <tr>  
                <td>" .$ori  . "</td>    
                <td>" . $nombre . "</td>    
                <td>" . $apellido . "</td>
                <td>" . $telefono . "</td>
                <td>" . $correo. "</td>
                <td>" . $calle." ". $numero ."</td>
                <td>" . $comuna . "</td>
                <td class='center-tab'".$operacion."'".">" . $operacion . "</td>
                <td>" . moneda_chilena($monto) ."</td>
                <td>" . $divisa . "</td>
                <td>" . $dormitorios . "</td>
                <td>" . $baños . "</td>
                <td>" . $estacionamiento . "</td>
                <td>" . $mTotales . "</td>
                <td>" . $mutiles . "</td>
                <td>" . $bodega. "</td>
                <td>" . $status . "</td>
                <td>" . $tipo . "</td>
                <td>" . $fase . "</td>
            </tr>
        ";
    // ----------------------------------------------Importancia Alta----------------------------------------------------------------------------------------------------------
    }else if($row["cont"] > 30){
        $salida.= "<tr class='trProp tb-data-single' >";
        switch ($row["id_or"]) {
            case 1:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-instagram' title='Instagram'></i></td>";
                    break;

                case 2:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                    break;

                case 3:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                    break;

                case 4:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-globe-americas' title='Web'></i></td>";
                    break;

                case 5:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Conserje'></i></td>";
                    break;

                case 6:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Cliente'></i></td>";
                    break;

                case 7:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Corredor'></i></td>";
                    break;

                case 8:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-pacman' title='Referido Redflip'></i></td>";
                    break;

                case 9:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='DaHunter'></i></td>";
                    break;

                case 10:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='BD Mailing'></i></td>";
                    break;

                case 11:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-database' title='Doorlist'></i></td>";
                    break;

                case 12:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='Open Casa'></i></td>";
                    break;

                case 13:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='GI0'></i></td>";
                    break;

                case 14:
                    $origen = "<td class='tdProp stackTab tabOri'><i class='fas fa-sync' title='Vilenky'></i></td>";
                    break;
                case 15:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                    break;

                case 16:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                    break;
                default:
                    $origen = "<td class='tdProp stackTab tabOri'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                    break;
            }

        $propietario = $row["fk_propietario"];
        $nota = $row["nota"];
        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $telefono = utf8_encode($row["telefono"]) ;
        $correo = utf8_encode($row["correo"] );
        $calle = utf8_encode($row["calle"]);  
        $numero = utf8_encode($row["numero"]);
        $comuna = utf8_encode($row["Comuna"]);
        $operacion = utf8_encode($row["operacion"]);
        if($operacion == "1"){
            $monto = $row["valor_uf"];
            $divisa = "UF";
            $operacion = "Venta";
        }else if($operacion == "2"){
            $monto = $row["valor_clp"];
            $divisa = "CLP";
            $operacion = "Arriendo";
        }else if($operacion == "3"){
            $monto = $row["valor_uf"];
            $divisa = "UF";
            $operacion = "Ambos";
        }else{
            $monto = $row["valor_uf"];
            $divisa = "UF";
            $operacion = "S/D";
        }
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row['fase']);
        $dormitorios = utf8_encode($row["dormitorios"]);
        $baños = utf8_encode($row["bannos"]);
        $estacionamiento = utf8_encode($row["estacionamiento"]);
        $mTotales = utf8_encode($row["metros_totales"]);
        $mutiles = utf8_encode($row["metros_utiles"]);
        $bodega = utf8_encode($row["bodega"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row["fase"]);
        $dias = utf8_encode($row["cont"]);
        $idPropiedad = $row["idPropiedad"];
        $idExterno = $row["id_externo"];
        $ori = utf8_encode($row["ori"]);
        $idForm = $row["id_formulario"];
         
        $telDes =substr($telefono, 0, 3)." ".substr($telefono, 3, 1). " ".substr($telefono, 4);

       $salida .= $origen."

        <td class='tdProp stackTab tabCont'> <i class='fas fa-circle fa-xs ci-red'></i>".$idExterno."</td>    
        <td class='tdProp stackTab tabNom'>" . $nombre." ".$apellido."</td>
        <td class='tdProp stackTab tabTel'>" . $telDes . "</td>
        <td class='tdProp stackTab tabMail' >" . $correo. "</td>
        <td class='tdProp stackTab tabDir'>" . $calle ." ". $numero ."</td>
        <td class='tdProp stackTab tabCom'>" . $comuna . "</td>
        <td class='tdProp stackTab tabPr'>" . moneda_chilena($monto) ." " . $divisa . "</td>
        <td class='tdProp stackTab tabOp center-tab'".$operacion."'".">" . $operacion . "</td>        
        <td class='tdProp stackTab tabSt'>" . $status . "</td>
        <td class='tdProp stackTab tabEst'>" . $tipo . "</td>";
        $conta ++;
         // Contactos input
            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
            $thisFase = "";
                if($row['fase'] == "Contacto"){
                    $salida.="<td class='tdProp stackTab tabFas' id='select_".$row["idPer"]."'>
                                <select class= 'selectFase contact' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }else{
                $salida.="<td class='tdProp stackTab tabFas' id='select_".$row["idPer"]."'>
                                <select class= 'selectFase' name='".$row['idPer']."' id='".$row['idPer']."' onchange='cambiar(".$row['idPer'].", ".$row['idPropietario'].")'> ";
                }
                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        // $table_hidden.="<td>".$row2['fase'] ."</td>";
                        // $table_hidden.="<td>".$row['contactos'] ."</td>";
                        
                        
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                        
                    }
                    
                } 
                // echo "<script>console.log($this)</script>"
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='number' id='input_".$row['idPropietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['idPropietario'].")' >";
                }
                $sqlEvent = "Select COUNT(*) as event from Calendar where fk_propiedad = $idPropiedad";
                $result = $conn->query($sqlEvent);
    
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                      $event = $row["event"];  
                    }
                }

                $salida.="<td class='tdProp stackTab tabDias center-tab'>" . $dias . "</td>
    
                <td class='tdProp stackTab tabAcc center-tab pb-10'>
                    <button class='btn-acciones bg-redflip-turquoise min-p' onClick=EditarPropiedadR('".$idPropiedad."') title='Modificar' ><i class='fas fa-edit'></i></button>
                    <button class='btn-acciones bg-redflip-darkgrey min-p' onClick=desPropiedad('".$idPropiedad."') title='Deshabilitar' ><i class='fas fa-user-slash'></i></button>
                    ";
                    if(strlen($nota) > 0 || $event != 0){
                        $salida.="<button class='btn-acciones bg-redflip-orange min-p' onClick=verNotas('".$idPropiedad."') title='Notas' ><i class='far fa-sticky-note'></i></button>";
                    }else{
                        $salida.="<button class='btn-acciones bg-redflip-yellow min-p' onClick=verNotas('".$idPropiedad."') title='Notas' ><i class='far fa-sticky-note'></i></button>";
                    }
                    $salida.="
                    <button class='btn-acciones bg-redflip-form min-p' title='Ver formulario' onclick='redirigir($idForm, `2`)' ><i class='far fa-file-alt'></i></button>
                </td>
        </tr>
        ";

        $table_hidden.= "
            <tr>  
                <td>" .$ori  . "</td>    
                <td>" . $nombre . "</td>    
                <td>" . $apellido . "</td>
                <td>" . $telefono . "</td>
                <td>" . $correo. "</td>
                <td>" . $calle." ". $numero ."</td>
                <td>" . $comuna . "</td>
                <td class='center-tab'".$operacion."'".">" . $operacion . "</td>
                <td>" . moneda_chilena($monto) ."</td>
                <td>" . $divisa . "</td>
                <td>" . $dormitorios . "</td>
                <td>" . $baños . "</td>
                <td>" . $estacionamiento . "</td>
                <td>" . $mTotales . "</td>
                <td>" . $mutiles . "</td>
                <td>" . $bodega. "</td>
                <td>" . $status . "</td>
                <td>" . $tipo . "</td>
                <td>" . $fase . "</td>
            </tr>
        ";
    }
}

$salida.="</tbody></table>";

}else{

    $salida.= "<table class='table table-striped' id='tabla_mixta'>

    <thead>
        <tr class='trProp thead-dark'>
            <th class='tdProp stackTab tabOri' scope='col'> origen </th>
            <th class='tdProp stackTab tabCont center-tab' scope='col'> # </th>
            <th class='tdProp stackTab tabNom ' scope='col'> Nombre </th>
            <th class='tdProp stackTab tabApe' scope='col'> Apellido </th>	
            <th class='tdProp stackTab tabTel' scope='col'> Teléfono </th>
            <th class='tdProp stackTab tabMail' scope='col'> Correo </th>
            <th class='tdProp stackTab tabDir' scope='col'> Dirección </th>
            <th class='tdProp stackTab tabCom' scope='col'> Comuna </th>
            <th class='tdProp stackTab tabOp center-tab' scope='col'> Operación </th>
            <th class='tdProp stackTab tabPr' scope='col'> Precio </th>
            <th class='tdProp stackTab tabSt' scope='col'> Status </th>
            <th class='tdProp stackTab tabEst' scope='col'> Estado </th>
            <th class='tdProp stackTab tabFas' scope='col'> Fase </th>
            <th class='tdProp stackTab tabDias center-tab' scope='col'> Días </th>
            <th class='tdProp stackTab tabAcc center-tab' scope='col'> Acciones </th>	
        </tr>
    </thead>
<tbody class='tbodyProp'>

    <tr>

    <td class= 'tdProp stackTab propTabN center-tab' colspan=15 > No hay Datos</td>

    </tr>    
</tbody>
</table>
";

$table_hidden= "


<table class='table table-striped' id='export_to_excel' style='display:none'>

        <thead>
            <tr class='thead-dark'>
                <th scope='col'> origen </th>
                <th scope='col'> Nombre </th>
                <th scope='col'> Apellido </th>	
                <th scope='col'> Teléfono </th>
                <th scope='col'> Correo </th>
                <th scope='col'> Dirección </th>
                <th scope='col'> Comuna </th>
                <th class='center-tab' scope='col'> Tipo de Operación </th>
                <th scope='col'> Monto </th>
                <th scope='col'> Divisa </th>
                <th scope='col'> Dormitorios </th>
                <th scope='col'> Baños </th>
                <th scope='col'> Estacionamiento </th>
                <th scope='col'> Metros Totales </th>
                <th scope='col'> Metros Utiles </th>
                <th scope='col'> Bodega </th>
                <th scope='col'> Status </th>
                <th scope='col'> Estado </th>
                <th scope='col'> Fase </th>	
            </tr>
        </thead>
    <tbody>

<tr>

<td class= 'propTabN center-tab' colspan=15 > No hay Datos</td>

</tr>
</tbody>
</table>";
}

echo $salida.$table_hidden;

$conn->close();









    











?>