<?php

include '../conexion.php';
require 'Classes/PHPExcel/IOFactory.php';
include '../controlador/mcript.php';
include '../pages/valid_session.php';
// include '../pages/header_logon.php';



 $cont=0;
 $nom = $_POST["nom"];

 $nombreArchivo = "../uploadFiles/" . $nom;

 $objPHPExcel = PHPEXCEL_IOFactory::load($nombreArchivo);

 $objPHPExcel -> setActiveSheetIndex(0);
 echo "<table border=1 class='table table-striped' style='margin-top: 100px;'>

 <thead>

  <tr class='thead-dark'>

  <th scope='col'>Fecha de ingreso</th>

  <th scope='col'>Area Redflip</th>

  <th scope='col'>Nombre Corredor</th>

  <th scope='col'>Mail corredor</th>

  <th scope='col'>Nombre Operario</th>

  <th scope='col'>Nombre Completo</th>

  <th scope='col'>Rut</th>

  <th scope='col'>Teléfono</th>

  <th scope='col'>Correo</th>

  <th scope='col'>Operación</th>

  <th scope='col'>Tipo propiedad</th>

  <th scope='col'>Valor UF</th>
  
  <th scope='col'>Valor CLP</th>

  <th scope='col'>Calle</th>
  <th scope='col'>Número</th>
  <th scope='col'>Depto/letra</th>
  <th scope='col'>Condominio</th>
  <th scope='col'>Comuna</th>
  <th scope='col'>Exclusividad</th>
  <th scope='col'>Amoblado</th>
  <th scope='col'>Año de Construcción</th>
  <th scope='col'>Rol</th>
  <th scope='col'>Contribuciones trimestrales</th>
  <th scope='col'>Gastos Comunes</th>
  <th scope='col'>¿Qué incluyen?</th>
  <th scope='col'>Superficie Útil</th>
  <th scope='col'>Superficie Terraza</th>
  <th scope='col'>Superficie Total</th>
  <th scope='col'>SuperFicie Terreno</th>
  <th scope='col'>Orientación</th>
  <th scope='col'>Nº Dormitorios</th>
  <th scope='col'>Nº Baños</th>
  <th scope='col'>Cantidad Estacionamientos</th>
  <th scope='col'>Nº Estacionamiento(s)</th>
  <th scope='col'>Ubicación estacionamiento</th>
  <th scope='col'>Techado (si es nivel calle)</th>
  <th scope='col'>Cantidad de pisos Casa/depto</th>
  <th scope='col'>Piso Depto.</th>
  <th scope='col'>Cantidad de deptos x piso</th>
  <th scope='col'>Cantidad de ascensores x piso</th>
  <th scope='col'>Fin de la propiedad</th>
  <th scope='col'>Cantidad Bodega(s)</th>
  <th scope='col'>Nº de la Bodega(s)</th>
  <th scope='col'>Ubicación bodega</th>
  <th scope='col'>Material Pisos Espacio Comunes</th>
  <th scope='col'>Material Pisos Dormitorios</th>
  <th scope='col'>Material Piso Baños</th>
  <th scope='col'>Tipo de calefacción</th>
  <th scope='col'>Agua caliente</th>
  <th scope='col'>Casa / Living-comedor separado</th>
  <th scope='col'>Aire Acondicionado</th>
  <th scope='col'>Ascensor privado</th>
  <th scope='col'>Ático</th>
  <th scope='col'>Bodega</th>
  <th scope='col'>Cocina Americana</th>
  <th scope='col'>Cocina Encimera</th>
  <th scope='col'>Cocina Isla</th>
  <th scope='col'>Comedor de Diario</th>
  <th scope='col'>Cortina Hangaroa</th>
  <th scope='col'>Cortina Roller</th>
  <th scope='col'>Cortinas Eléctricas</th>
  <th scope='col'>Dorm. Servicio</th>
  <th scope='col'>Escritorio</th>
  <th scope='col'>Horno Empotrado</th>
  <th scope='col'>Jacuzzi</th>
  <th scope='col'>Logia</th>
  <th scope='col'>Malla Protección Terraza</th>
  <th scope='col'>Riego Automático</th>
  <th scope='col'>Sala de Estar</th>
  <th scope='col'>Sistema de Alarma</th>
  <th scope='col'>Termo Panel</th>
  <th scope='col'>Lavavajillas empotrado</th>
  <th scope='col'>Microondas empotrado</th>
  <th scope='col'>Refrigerador empotrado</th>
  <th scope='col'>Despensa</th>
  <th scope='col'>Hall de acceso</th>
  <th scope='col'>Pieza de planchado</th>
  <th scope='col'>Cocina amoblada</th>
  <th scope='col'>Citófono</th>
  <th scope='col'>Mansarda</th>
  <th scope='col'>Walking closet</th>
  <th scope='col'>Cerco eléctrico</th>
  <th scope='col'>Ascensor Edificio</th>
  <th scope='col'>Conserjería 24Hrs</th>
  <th scope='col'>Estacionamiento de Visita</th>
  <th scope='col'>Gimnasio edificio</th>
  <th scope='col'>Juegos Infantiles</th>
  <th scope='col'> Lavandería Edificio</th>
  <th scope='col'>Piscina</th>
  <th scope='col'>Portón Eléctrico</th>
  <th scope='col'>Quincho</th>
  <th scope='col'>Sala de Juegos</th>
  <th scope='col'>Sala de reuniones</th>
  <th scope='col'>Sala Multiuso</th>
  <th scope='col'>Sauna</th>
  <th scope='col'>Piscina temperada</th>
  <th scope='col'>Gourmet room</th>
  <th scope='col'>Azotea habilitada</th>
  <th scope='col'>cancha de tenis</th>
  <th scope='col'>Circuito cerrado de TV</th>
  <th scope='col'>Areas verdes</th>
  <th scope='col'>Sala de cine</th>
  <th scope='col'>Patio de servicio</th>
  <th scope='col'>Antejardín</th>
  <th scope='col'>Piscina temperada</th>
  <th scope='col'>Disponibilidad visitas</th>
  <th scope='col'>Disponibilidad de entrega</th>
  <th scope='col'>Notas</th>

 </tr>

 </thead>

 <tbody>";

 $numRows = $objPHPExcel -> setActiveSheetIndex(0) -> getHighestRow();

// //recorre el archivo .xlsx y saca los datos desde la fila numero 2 en adelante
 for($i = 2; $i <= $numRows; $i++){
    $fecha = $objPHPExcel-> getActiveSheet() ->getCell('B'.$i)-> getCalculatedValue();

    $area = $objPHPExcel-> getActiveSheet() ->getCell('C'.$i)-> getCalculatedValue();

    $corredor = $objPHPExcel-> getActiveSheet() ->getCell('D'.$i)-> getCalculatedValue();

    $mailCorredor = $objPHPExcel-> getActiveSheet() ->getCell('E'.$i)-> getCalculatedValue();

    $operario = $objPHPExcel-> getActiveSheet() ->getCell('F'.$i)-> getCalculatedValue();

    $propietario = $objPHPExcel-> getActiveSheet() ->getCell('G'.$i)-> getCalculatedValue();

    $rut = $objPHPExcel-> getActiveSheet() ->getCell('H'.$i)-> getCalculatedValue();

    $tel = $objPHPExcel-> getActiveSheet() ->getCell('I'.$i)-> getCalculatedValue();

    $mailProp = $objPHPExcel-> getActiveSheet() ->getCell('J'.$i)-> getCalculatedValue();

    $operacion = $objPHPExcel-> getActiveSheet() ->getCell('L'.$i)-> getCalculatedValue();

    $tipoProp = $objPHPExcel-> getActiveSheet() ->getCell('M'.$i)-> getCalculatedValue();

    $valorUF = $objPHPExcel-> getActiveSheet() ->getCell('N'.$i)-> getCalculatedValue();

    $valorCLP = $objPHPExcel-> getActiveSheet() ->getCell('O'.$i)-> getCalculatedValue();
    $calle = $objPHPExcel-> getActiveSheet() ->getCell('P'.$i)-> getCalculatedValue();
    $num = $objPHPExcel-> getActiveSheet() ->getCell('Q'.$i)-> getCalculatedValue();
    $letra = $objPHPExcel-> getActiveSheet() ->getCell('R'.$i)-> getCalculatedValue();
    $condominio = $objPHPExcel-> getActiveSheet() ->getCell('S'.$i)-> getCalculatedValue();
    $comuna = $objPHPExcel-> getActiveSheet() ->getCell('T'.$i)-> getCalculatedValue();
    $exclusividad = $objPHPExcel-> getActiveSheet() ->getCell('BJ'.$i)-> getCalculatedValue();
    $amoblado = $objPHPExcel-> getActiveSheet() ->getCell('BK'.$i)-> getCalculatedValue();
    $anoConstruccion = $objPHPExcel-> getActiveSheet() ->getCell('BL'.$i)-> getCalculatedValue();
    $rol = $objPHPExcel-> getActiveSheet() ->getCell('BM'.$i)-> getCalculatedValue();
    $contribucion = $objPHPExcel-> getActiveSheet() ->getCell('BN'.$i)-> getCalculatedValue();
    $gastosComunes = $objPHPExcel-> getActiveSheet() ->getCell('BO'.$i)-> getCalculatedValue();
    $queIncluyen = $objPHPExcel-> getActiveSheet() ->getCell('BP'.$i)-> getCalculatedValue();
    $supUtil = $objPHPExcel-> getActiveSheet() ->getCell('BQ'.$i)-> getCalculatedValue();
    $supTerraza = $objPHPExcel-> getActiveSheet() ->getCell('BR'.$i)-> getCalculatedValue();
    $supTotal = $objPHPExcel-> getActiveSheet() ->getCell('BS'.$i)-> getCalculatedValue();
    $supTerreno = $objPHPExcel-> getActiveSheet() ->getCell('BT'.$i)-> getCalculatedValue();
    $orientacion = $objPHPExcel-> getActiveSheet() ->getCell('BU'.$i)-> getCalculatedValue();
    $dorm = $objPHPExcel-> getActiveSheet() ->getCell('BV'.$i)-> getCalculatedValue();
    $bannos = $objPHPExcel-> getActiveSheet() ->getCell('BY'.$i)-> getCalculatedValue();
    $estacionamiento = $objPHPExcel-> getActiveSheet() ->getCell('CH'.$i)-> getCalculatedValue();
    $numEst = $objPHPExcel-> getActiveSheet() ->getCell('CI'.$i)-> getCalculatedValue();
    $ubicacionEst = $objPHPExcel-> getActiveSheet() ->getCell('CJ'.$i)-> getCalculatedValue();

    //empieza a crear requisitos para crear propiedades
    //busca el id de la comuna ingresada, si no la encuentra lanzará error
    // if(isset($nom) && isset($ape)){

    

    // if($comuna == "Ñuñoa"){
    //     $idComuna = 1;
    //     }else if($comuna == "Chicureo"){
    //         $idComuna = 2;

    //         $comuna == "";
    //     }else if($comuna == "La Florida"){
    //         $idComuna = 3;
    //     }else if($comuna == "La Reina"){
    //         $idComuna = 4;
    //     }else if($comuna == "Las Condes"){
    //         $idComuna = 5;
    //     }else if($comuna == "Lo Barnechea"){
    //         $idComuna = 6;
    //     }else if($comuna == "Maipú"){
    //         $idComuna = 7;
    //     }else if($comuna == "Peñalolén"){
    //         $idComuna = 8;
    //     }else if($comuna == "Providencia"){
    //         $idComuna = 9;
    //     }else if($comuna == "Pucón"){
    //         $idComuna = 10;
    //     }else if($comuna == "San Miguel"){
    //         $idComuna = 11;
    //     }else if($comuna == "Santiago"){
    //         $idComuna = 12;
    //         $comuna == "";
    //     }else if($comuna == "Vitacura"){
    //         $idComuna = 13;
    //     }else if($comuna == ""){
    //         echo "asdas";
    //     }else {
    //         echo $comuna;
    //         die;
    //     }

     
     
     
    echo "<tr>";
    echo "<td>$fecha</td>";
    echo "<td>$area</td>";
    echo "<td>$corredor</td>";
    echo "<td>$mailCorredor</td>";
    echo "<td>$operario</td>";
    echo "<td>$propietario</td>";
    echo "<td>$rut</td>";
    echo "<td>$tel</td>";
    echo "<td>$mailProp</td>";
    echo "<td>$operacion</td>";
    echo "<td>$tipoProp</td>";
    echo "<td>$valorUF</td>";
    echo "<td>$valorCLP</td>";
    echo "<td>$calle</td>";
    echo "<td>$num</td>";
    echo "<td>$letra</td>";
    echo "<td>$condominio</td>";
    echo "<td>$comuna</td>";
    echo "<td>$exclusividad</td>";
    echo "<td>$amoblado</td>";
    echo "<td>$anoConstruccion</td>";
    echo "<td>$rol</td>";
    echo "<td>$contribucion</td>";
    echo "<td>$gastosComunes</td>";
    echo "<td>$queIncluyen</td>";
    echo "<td>$supUtil</td>";
    echo "<td>$supTerreno</td>";
    echo "<td>$supTotal</td>";
    echo "<td>$supTerreno</td>";
    echo "<td>$orientacion</td>";
    echo "<td>$dorm</td>";
    echo "<td>$bannos</td>";
    echo "<td>$estacionamiento</td>";
    echo "<td>$numEst</td>";
    echo "<td>$ubicacionEst</td>";
 }

//     if($operacion == "arriendo" || $operacion == "Arriendo"){
//         $op = 2;
//     }else{
//         $op = 1;
//     }
// $origen = trim(strtolower($ori));
// $idOri = 1;

// switch ($origen) {
//     case "no definido":
//         $idOri = 1;
//         break;
//     case "facebook":
//         $idOri = 2;
//         break;
//     case "linkedin":
//         $idOri = 3;;
//         break;
//     case "Web":
//         $idOri = 4;;
//         break;
//     case "referidoconserje":
//         $idOri = 5;
//         break;
//     case "referidocliente":
//         $idOri = 6;
//         break;
//     case "referidocorredor":
//         $idOri = 7;
//         break;
//     case "dahunter":
//         $idOri = 8;
//         break;
//     case "bdmailing":
//         $idOri = 9;
//         break;
//     case "doorlist":
//         $idOri = 10;
//         break;
//     case "opencasa":
//         $idOri = 11;
//         break;
//     case "gi0":
//         $idOri = 12;
//         break;
//     case "vilenky":
//         $idOri = 13;
//     break;
//     case "instagram":
//         $idOri = 14;
//         break;
//     default:
//         $idOri = 1;
// }

// $status2 = trim(strtolower($status));
// $idStat = 1;
// switch($status2){
//     case "disponible":
//         $idStat = 1;
//     break;

//     case "v.redflip":
//         $idStat = 2;
//     break;

//     case "v.terceros":
//         $idStat = 3;
//     break;

//     case "a.redflip":
//         $idStat = 4;
//     break;

//     case "a.terceros":
//         $idStat = 5;
//     break;

//     case "pendientet.v.":
//         $idStat = 6;
//     break;

//     default:
//         $idStat = 1;
// }



    

        //inserta en tabla dirección

    // $sqlDirección = "INSERT INTO Direccion (calle, numero, referencia, fk_comuna ) VALUES (". "'" . $calle."', "."'" .$numero. "' , 'Referencia (cambiar)' ,"."'" .$idComuna. "')";
    // if ($conn->query($sqlDirección) === TRUE) {
    //     $cont = 2;
    // } else {
    //     echo "Error 3.1 sql en: ".$sqlDirección ;die;
    // }//fin if dirección
    

    // $sqlObtenerDireccion = "SELECT MAX(id) as id FROM Direccion";
    
    // $result = $conn->query($sqlObtenerDireccion);

    // if ($result->num_rows > 0) {
    //     // output data of each row
    //     while($row = $result->fetch_assoc()) {
    //         $idDirec = $row["id"];
            
            
    //     }
    // } else {
    //     echo "error 3.2 sql en: " . $sqlObtenerDireccion; die;
    // }//fin if obtener dirección


    //agregar propietario
    //crea correo y pass enciptada
    //Al ingresar propiedades masivas la contraseña queda como: inicial nombre mayuscula + inicial apellido minuscula + 1994

    // $pass =  strtoupper ($nombre[0]) . strtolower ($apellido[0]). "redflip" ;
    // $correo_encript = $encriptar(strtolower ($correo));
    // $pass_encript = $encriptar($pass);


    // $sql ='INSERT INTO Persona (nombre, apellido, rut, fec_nac, telefono, correo) VALUES
    //  ('."'".utf8_decode($nom)."'
    //  ,'".utf8_decode($ape)."'
    //  ,''
    //  ,null
    //  ,'".$tel."'
    //  ,'".$correo."')";

    //  if ($conn->query($sql) === TRUE) {
    //     $cont = 2;
    // } else {
    //     echo "error 3.3 SQL en: " . $sql;die;
    // }

    // $sqlSelectPer = "SELECT MAX(id_persona) as id from Persona";

    // $result = $conn->query($sqlSelectPer);

    // if ($result->num_rows > 0) {
    //     // output data of each row
    //     while($row = $result->fetch_assoc()) {
    //         $idPer = $row["id"];
    //         $cont = 2;
    //     }
    // } else {
    //     echo "error 3.4 SQl en: ". $sqlSelectPer;die;
    // }

    //insertar usuario
    // $sqlUs = 'INSERT INTO usuario (nom_us, pass, fk_rol, fk_estado_us, fk_persona)
    // VALUES ('."'".$correo_encript."',"."'".$pass_encript."',2,1, ". "'". $idPer."' )";

    // if ($conn->query($sqlUs) === TRUE) {
    //     $cont = 2;
    // } else {
    //     echo "error 3.5 SQL en: ". $sqlUs;die;
    // }

    //Inserta propietario
    // $sqlProp = 'INSERT INTO Propietario (fk_persona,cont,fk_estado_prop) VALUES ('.$idPer.', 0, 1);';

    // if ($conn->query($sqlProp) === TRUE) {
    //     $cont = 2;
    // } else {
        
    //     echo "error 3.6 SQL en: ". $sqlProp; die;
    // }

    // //Seleccion del ultimo propietario

    // $sqlSelectProp = "SELECT MAX(id_propietario) as id from Propietario";

    // $result = $conn->query($sqlSelectProp);

    // if ($result->num_rows > 0) {
    //     // output data of each row
    //     while($row = $result->fetch_assoc()) {
    //         $idProp = $row["id"];
    //         $cont = 2;
    //     }
    // } else {
    //     echo "error 3.7 SQl en: ". $sqlSelectProp; die;
    // }

    //insertar propiedad

    // $sqlProp = "INSERT INTO `Propiedad` (
    //     `id_propiedad`
    //     , `dormitorios`,
    //     `bannos`
    //     ,`estacionamiento`
    //     , `metros_totales`
    //     , `metros_utiles`
    //     , `bodega`
    //     , `monto`
    //     , `fk_propietario`
    //     , `fk_estado_propiedad`
    //     , `fk_corredor`
    //     , `fk_lista_administrativo`
    //     , `fk_lista_interesado`
    //     , `fk_lista_servicio`
    //     , `fk_direccion`
    //     , `divisa`
    //     , `fk_venta_arriendo`
    //     , `fk_status`
    //     , `fk_origen`
    //     , `fk_operario`
    //     ) 
    //      VALUES (
    //          NULL
    //          ,NULL
    //          ,NULL
    //          ,NULL
    //          ,NULL
    //          ,NULL
    //          ,NULL
    //          ,"."'".$precio."'"."
    //          ,".$idProp."
    //          , 1
    //          , 1
    //          , NULL
    //          , NULL
    //          , NULL
    //          ,".$idDirec."
    //          ,1
    //          ,$op
    //          ,$idStat
    //          ,$idOri
    //          ,1
    //         );";
        
    //         if ($conn->query($sqlProp) === TRUE) {
    //             $cont = 2;
    //         } else {
    //             echo "error 3.8 SQL en: ".$sqlProp ;die;
    //         }

    //     } 
//  }// fin for

// $conn->close();
// echo $cont;

//     echo "<td>$rut</td>";
//     echo "<td>$fec_nac</td>";
//     echo "<td>$tel</td>";
//     echo "<td>$correo</td>";
//     echo "</tr>";


//     $sqlPer="INSERT INTO Persona (nombre, apellido, rut, fec_nac, telefono, correo) VALUES ('$nombre', '$apellido', '$rut', '$fec_nac', '$tel', '$correo')";

//         if ($conn->query($sqlPer) === TRUE) {
//             echo "2";
//         } else {
//             echo "3";
//         }
//     $sqlIdPer = "SELECT MAX(Persona.id_persona) as id from Persona";

//     $result = $conn->query($sqlIdPer);

//     if ($result->num_rows > 0) {
//         // output data of each row
//         while($row = $result->fetch_assoc()) {
            
//             $idPer = $row["id"];
//             echo "2";
//         }
//     } else {
//         echo "error";
//     }

//     $sqlProp = "INSERT INTO Propietario (fk_persona, cont, fk_estado_prop) VALUES ($idPer, 0, 1)";
    
//     if ($conn->query($sqlProp) === TRUE) {
//             echo $sqlProp;
//     } else {
//             echo "3";
//             }
            
//     $pass =  strtoupper ($nombre[0]) . strtolower ($apellido[0]). substr($fec_nac, 0, 4);
    
//     $nomUs = $encriptar(strtolower ($correo));

//     $pass = $encriptar($pass);

//     $sqlUs = "INSERT INTO usuario (nom_us, pass, fk_rol, fk_estado_us, fk_persona)
//         VALUES ('$nomUs','$pass',2,1, '$idPer' )";


//         if ($conn->query($sqlUs) === TRUE) {
            
//         } else {
//             die;
//         }
//         $cont ++ ;

// }

// $conn->close();

// header("Location: ../pages/propVSpropiedad.php?cont=" . $cont);
?>