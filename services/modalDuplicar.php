
<div class="modal" id="modal2" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Duplicar Formulario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeModal2()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="tablaModal2">    
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModal2()">Cerrar</button>
      </div>
    </div>
  </div>
</div>