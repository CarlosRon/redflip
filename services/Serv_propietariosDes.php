<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
<table class="table table-striped" id="export_to_excel">

					<?
                        include '../conexion.php';
                        include '../pages/valid_session.php';

					$id=$_SESSION['id'];

                    $sql2 = 'SELECT DISTINCT Persona.id_persona 
                    , Persona.nombre 
                    , Persona.apellido 
                    , Persona.rut 
                    , Persona.fec_nac 
                    , Persona.telefono 
                    , Persona.correo 
                    , Propietario.cont 
                    , Sub_estado.fase
                    , Estado_prop.tipo 
                    from Persona INNER JOIN Propietario , usuario
                    , Estado_prop, Sub_estado 
                    where Persona.id_persona = usuario.fk_persona 
                    AND usuario.fk_estado_us = 2 
                    AND Propietario.fk_persona = Persona.id_persona 
                    AND Estado_prop.id = Propietario.fk_estado_prop 
                    AND Sub_estado.id = Estado_prop.fk_sub_estado';
					
					$sql = 'SELECT per.nombre,
					per.apellido,
					per.rut,
					per.fec_nac,
					per.telefono,
					per.correo,
					us.nom_us,
					us.pass
					from usuario us
					INNER JOIN
					Persona per
					WHERE
					us.idusuario = per.usuario_idusuario
					and
					per.usuario_idusuario
					='.$id;	

			$result = $conn->query($sql2);

if ($result->num_rows > 0) {
    ?>
    
<tr class="thead-dark">
    <th scope='col'> # </th>
    <th scope='col'> Nombre </th>
    <th scope='col'> Apellido </th>	
    <th scope='col'> Estado </th>
    <th scope='col'> Fase </th>
    <th scope='col'> Rut </th>
    <th scope='col'> Teléfono </th>
    <th scope='col'> Correo </th>
    <th scope='col'> Contador </th>
    <th scope='col'> Acciones </th>	
<tr> 

    <?
    $cont=0;
    // output data of each row
    while($row = $result->fetch_assoc()) {
    ++$cont;
    // echo '<form id="formId"><input type="text" name='.$row["id_persona"].' value='.$row["id_persona"].' style="display:none"></form>';
    echo "<td class='th-cabecera'></td>";
    echo "<td>" . $cont. "</td>"; 
    echo "<td>" . utf8_encode($row["nombre"]) . "</td>";    
    echo "<td>" . utf8_encode($row["apellido"]) . "</td>";
    echo "<td>" . utf8_encode($row["tipo"]) . "</td>";
    echo "<td>" . utf8_encode($row["fase"] ). "</td>";
    echo "<td>" . utf8_encode($row["rut"]) . "</td>";
    echo "<td>" . utf8_encode($row["telefono"]) . "</td>";
    echo "<td>" . utf8_encode($row["correo"]) . "</td>";
    echo "<td>" . utf8_encode($row["cont"]) . "</td>";
    echo '<td>
    <button class="btn-action bg-redflip-green" onClick="habilitarPropietario('.$row["id_persona"].')">Habilitar</button>
    <button class="btn-action bg-redflip-red" onClick="EliminarPropietario('.$row["id_persona"].')">Eliminar</button></td>';
	echo "</tr>";
    }

} else {
	// echo "datos invalidos";
	// echo $row["id_usuario"];
	// echo $_SESSION['id'];
    // echo $result;
    ?>
    <tr class="thead-dark">
    <th scope='col'> # </th>
    <th scope='col'> Nombre </th>
    <th scope='col'> Apellido </th>	
    <th scope='col'> Estado </th>
    <th scope='col'> Fase </th>
    <th scope='col'> Rut </th>
    <th scope='col'> Teléfono </th>
    <th scope='col'> Correo </th>
    <th scope='col'> Contador </th>
    <th scope='col'> Acciones </th>	
    
    </tr>
    <td colspan="10" class='center-tab'>No hay registros</td>
    <?php
}$conn->close();
	?>

</table>

<form action="process.php" method="post" target="_blank" id="formExport">
                <input type="hidden" id="data_to_send" name="data_to_send" />
                <input type="hidden" id="nombre" name="nombre" value="Propietarios Desactivados" />
            </form>
<button type="submit" id="submitExport">export</button>

<script src="../js/stacktable.js"></script>
<script src="../js/accionProp.js"></script>
<script>
if(screen.width < 1200){
     $('table').stacktable();
    }
    console.log(screen.width);
</script>

<script>
document.getElementById('submitExport').addEventListener('click', function(e) {
    e.preventDefault();
    let export_to_excel = document.getElementById('export_to_excel');
    let data_to_send = document.getElementById('data_to_send');
    data_to_send.value = export_to_excel.outerHTML;
    document.getElementById('formExport').submit();
});
</script>
</body>
