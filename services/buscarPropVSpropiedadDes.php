<?php 
include "../conexion.php";

$salida = "";
$conta = 1;
$query = "SELECT distinct per.nombre
, per.apellido
, per.telefono
, per.correo
, per.id_persona as idPer
, dir.calle
, dir.numero
, dir.referencia
, dir.id as idDir
, com.nombre as Comuna
, com.id as idCom
, vent.descripcion V_A
, vent.id as idVent
, divi.nombre as divisa
, divi.id as idDiv
, propiedad.id_propiedad as idPropiedad
, propiedad.fk_propietario
, propiedad.nota
, propiedad.monto
, propiedad.id_externo
, est.tipo
, est.id as idEst
, sub.fase
, sub.id as idSub
, propietario.cont
, propietario.id_propietario as idPropietario
, propietario.contactos
, Origen.nom_origen as ori
, Origen.id_origen as id_or
, formulario.*
, Estatus.status
FROM Persona per
INNER JOIN Propietario propietario
, Estado_prop est
, Sub_estado sub
, Propiedad propiedad
, Divisa divi, Direccion dir
, Comuna com    
, Venta_Arriendo vent
, Origen    
, Estatus 
, formulario
WHERE per.id_persona = propietario.fk_persona
AND propietario.fk_estado_prop = est.id
AND est.fk_sub_estado = sub.id
AND propietario.id_propietario = propiedad.fk_propietario
AND propiedad.divisa = divi.id
AND formulario.Direccion_id = dir.id
and dir.fk_comuna = com.id
AND propiedad.fk_venta_arriendo = vent.id
AND propietario.fk_origen = Origen.id_origen
AND propiedad.fk_status = Estatus.id_status
AND propiedad.fk_formulario = formulario.id_formulario
AND propiedad.carga = 0
AND formulario.aprobado = 1
AND propiedad.fk_estado_propiedad = 2
order by propiedad.id_propiedad asc
";

// if(isset($_POST['consulta1']) && isset($_POST['consulta2']) && isset($_POST['consulta3']) && isset($_POST['consulta4'])){
//     $q1 = $conn -> real_escape_string($_POST['consulta1']);
//     $q2 = $conn -> real_escape_string($_POST['consulta2']);
//     $q3 = $conn -> real_escape_string($_POST['consulta3']);
//     $q4 = $conn -> real_escape_string($_POST['consulta4']);

//     // echo "<script>console.log('aqui')</script>";
//     // echo "<script>console.log('$q2')</script>";
//     // echo "<script>console.log('$q3')</script>";
//     // echo "<script>console.log('$q4')</script>";

//     if($q4 == 'Alta'){

//     $query = "SELECT per.nombre
//         , per.apellido
//         , per.telefono
//         , per.correo
//         , per.id_persona as idPer
//         , dir.calle
//         , dir.numero
//         , dir.referencia
//         , dir.id as idDir
//         , com.nombre as Comuna
//         , com.id as idCom
//         , vent.descripcion V_A
//         , vent.id as idVent
//         , divi.nombre as divisa
//         , divi.id as idDiv
//         , propiedad.monto
//         , propiedad.dormitorios
//         , propiedad.bannos
//         , propiedad.estacionamiento
//         , propiedad.metros_totales
//         , propiedad.metros_utiles
//         , propiedad.bodega
//         , propiedad.id_propiedad as idPropiedad
//         , propiedad.fk_propietario
//         , est.tipo
//         , est.id as idEst
//         , sub.fase
//         , sub.id as idSub
//         , propietario.cont
//         , propietario.id_propietario as idPropietario
//         , Origen.nom_origen as ori
//         , Origen.id_origen as id_or
//         , Status.status
//         FROM Persona per 
//         INNER JOIN Propietario propietario
//         , Estado_prop est
//         , Sub_estado sub
//         , Propiedad propiedad
//         , Divisa divi, Direccion dir
//         , Comuna com
//         , Venta_Arriendo vent 
//         , Origen
//         , Status
//         WHERE per.id_persona = propietario.fk_persona 
//         AND propietario.fk_estado_prop = est.id 
//         AND est.fk_sub_estado = sub.id 
//         AND propietario.id_propietario = propiedad.fk_propietario 
//         AND propiedad.divisa = divi.id 
//         AND propiedad.fk_direccion = dir.id 
//         and dir.fk_comuna = com.id 
//         AND propiedad.fk_venta_arriendo = vent.id
//         AND propietario.fk_origen = Origen.id_origen
//         AND propiedad.fk_status = Status.id_status
//         AND propiedad.fk_estado_propiedad = 2
//         AND vent.descripcion LIKE" ."'%" .$q1. "%'
//         AND est.tipo LIKE" ."'%" .$q2. "%' 
//         AND sub.fase LIKE" ."'%" .$q3. "%'
//         AND propietario.cont > 31
//         order by propietario.cont desc
//     " ; 

//     }else if($q4 == "Media"){
//         $q4 = 20;
//         $query = "SELECT per.nombre
//             , per.apellido
//             , per.telefono
//             , per.correo
//             , per.id_persona as idPer
//             , dir.calle
//             , dir.numero
//             , dir.referencia
//             , dir.id as idDir
//             , com.nombre as Comuna
//             , com.id as idCom
//             , vent.descripcion V_A
//             , vent.id as idVent
//             , divi.nombre as divisa
//             , divi.id as idDiv
//             , propiedad.monto
//             , propiedad.dormitorios
//             , propiedad.bannos
//             , propiedad.estacionamiento
//             , propiedad.metros_totales
//             , propiedad.metros_utiles
//             , propiedad.bodega
//             , propiedad.id_propiedad as idPropiedad
//             , propiedad.fk_propietario
//             , est.tipo
//             , est.id as idEst
//             , sub.fase
//             , sub.id as idSub
//             , propietario.cont
//             , propietario.id_propietario as idPropietario
//             , Origen.nom_origen as ori
//             , Origen.id_origen as id_or
//             , Status.status
//             FROM Persona per 
//             INNER JOIN Propietario propietario
//             , Estado_prop est
//             , Sub_estado sub
//             , Propiedad propiedad
//             , Divisa divi, Direccion dir
//             , Comuna com
//             , Venta_Arriendo vent 
//             , Origen
//             , Status
//             WHERE per.id_persona = propietario.fk_persona 
//             AND propietario.fk_estado_prop = est.id 
//             AND est.fk_sub_estado = sub.id 
//             AND propietario.id_propietario = propiedad.fk_propietario 
//             AND propiedad.divisa = divi.id 
//             AND propiedad.fk_direccion = dir.id 
//             and dir.fk_comuna = com.id 
//             AND propiedad.fk_venta_arriendo = vent.id
//             AND propietario.fk_origen = Origen.id_origen
//             AND propiedad.fk_estado_propiedad = 2
//             AND propiedad.fk_status = Status.id_status
//             AND vent.descripcion LIKE" ."'%" .$q1. "%'
//             AND est.tipo LIKE" ."'%" .$q2. "%' 
//             AND sub.fase LIKE" ."'%" .$q3. "%'
//             AND propietario.cont >= $q4
//             AND propietario.cont <=30
//             order by propietario.cont desc
//         " ; 

//     }else if($q4 == "Baja"){
//         $query = "SELECT per.nombre
//             , per.apellido
//             , per.telefono
//             , per.correo
//             , per.id_persona as idPer
//             , dir.calle
//             , dir.numero
//             , dir.referencia
//             , dir.id as idDir
//             , com.nombre as Comuna
//             , com.id as idCom
//             , vent.descripcion V_A
//             , vent.id as idVent
//             , divi.nombre as divisa
//             , divi.id as idDiv
//             , propiedad.monto
//             , propiedad.dormitorios
//             , propiedad.bannos
//             , propiedad.estacionamiento
//             , propiedad.metros_totales
//             , propiedad.metros_utiles
//             , propiedad.bodega
//             , propiedad.id_propiedad as idPropiedad
//             , propiedad.fk_propietario
//             , est.tipo
//             , est.id as idEst
//             , sub.fase
//             , sub.id as idSub
//             , propietario.cont
//             , propietario.id_propietario as idPropietario
//             , Origen.nom_origen as ori
//             , Origen.id_origen as id_or
//             , Status.status
//             FROM Persona per 
//             INNER JOIN Propietario propietario
//             , Estado_prop est
//             , Sub_estado sub
//             , Propiedad propiedad
//             , Divisa divi, Direccion dir
//             , Comuna com
//             , Venta_Arriendo vent 
//             , Origen
//             , Status
//             WHERE per.id_persona = propietario.fk_persona 
//             AND propietario.fk_estado_prop = est.id 
//             AND est.fk_sub_estado = sub.id 
//             AND propietario.id_propietario = propiedad.fk_propietario 
//             AND propiedad.divisa = divi.id 
//             AND propiedad.fk_direccion = dir.id 
//             and dir.fk_comuna = com.id 
//             AND propiedad.fk_venta_arriendo = vent.id
//             AND propietario.fk_origen = Origen.id_origen
//             AND propiedad.fk_estado_propiedad = 2
//             AND propiedad.fk_status = Status.id_status
//             AND vent.descripcion LIKE" ."'%" .$q1. "%'
//             AND est.tipo LIKE" ."'%" .$q2. "%' 
//             AND sub.fase LIKE" ."'%" .$q3. "%'
//             AND propietario.cont >= 0
//             AND propietario.cont <20
//             order by propietario.cont desc
//         " ; 

//     }else{
//         $query = "SELECT per.nombre
//             , per.apellido
//             , per.telefono
//             , per.correo
//             , per.id_persona as idPer
//             , dir.calle
//             , dir.numero
//             , dir.referencia
//             , dir.id as idDir
//             , com.nombre as Comuna
//             , com.id as idCom
//             , vent.descripcion V_A
//             , vent.id as idVent
//             , divi.nombre as divisa
//             , divi.id as idDiv
//             , propiedad.monto
//             , propiedad.dormitorios
//             , propiedad.bannos
//             , propiedad.estacionamiento
//             , propiedad.metros_totales
//             , propiedad.metros_utiles
//             , propiedad.bodega
//             , propiedad.id_propiedad as idPropiedad
//             , propiedad.fk_propietario
//             , est.tipo
//             , est.id as idEst
//             , sub.fase
//             , sub.id as idSub
//             , propietario.cont
//             , propietario.id_propietario as idPropietario
//             , Origen.nom_origen as ori
//             , Origen.id_origen as id_or
//             , Status.status
//             FROM Persona per 
//             INNER JOIN Propietario propietario
//             , Estado_prop est
//             , Sub_estado sub
//             , Propiedad propiedad
//             , Divisa divi, Direccion dir
//             , Comuna com
//             , Venta_Arriendo vent 
//             , Origen
//             , Status
//             WHERE per.id_persona = propietario.fk_persona 
//             AND propietario.fk_estado_prop = est.id 
//             AND est.fk_sub_estado = sub.id 
//             AND propietario.id_propietario = propiedad.fk_propietario 
//             AND propiedad.divisa = divi.id 
//             AND propiedad.fk_direccion = dir.id 
//             and dir.fk_comuna = com.id 
//             AND propiedad.fk_venta_arriendo = vent.id
//             AND propietario.fk_origen = Origen.id_origen
//             AND propiedad.fk_estado_propiedad = 2
//             AND propiedad.fk_status = Status.id_status
//             AND vent.descripcion LIKE" ."'%" .$q1. "%'
//             AND est.tipo LIKE" ."'%" .$q2. "%' 
//             AND sub.fase LIKE" ."'%" .$q3. "%'
//             order by propietario.cont desc ";
//     }

// }else if(isset($_POST['consulta1']) && isset($_POST['consulta2']) && isset($_POST['consulta3']) && isset($_POST['consulta4']) && isset($_POST['consulta5'])){
//     $q1 = $conn -> real_escape_string($_POST['consulta1']);
//     $q2 = $conn -> real_escape_string($_POST['consulta2']);
//     $q3 = $conn -> real_escape_string($_POST['consulta3']);
//     $q4 = $conn -> real_escape_string($_POST['consulta4']);
//     $q5 = $conn -> real_escape_string($_POST['consulta5']);
//     $query = "SELECT per.nombre
//         , per.apellido
//         , per.telefono
//         , per.correo
//         , per.id_persona as idPer
//         , dir.calle
//         , dir.numero
//         , dir.referencia
//         , dir.id as idDir
//         , com.nombre as Comuna
//         , com.id as idCom
//         , vent.descripcion V_A
//         , vent.id as idVent
//         , divi.nombre as divisa
//         , divi.id as idDiv
//         , propiedad.monto
//         , propiedad.dormitorios
//         , propiedad.bannos
//         , propiedad.estacionamiento
//         , propiedad.metros_totales
//         , propiedad.metros_utiles
//         , propiedad.bodega
//         , propiedad.id_propiedad as idPropiedad
//         , propiedad.fk_propietario    
//         , est.tipo
//         , est.id as idEst
//         , sub.fase
//         , sub.id as idSub
//         , propietario.cont
//         , propietario.id_propietario as idPropietario
//         , Origen.nom_origen as ori
//         , Origen.id_origen as id_or
//         , Status.status
//         FROM Persona per 
//         INNER JOIN Propietario propietario
//         , Estado_prop est
//         , Sub_estado sub
//         , Propiedad propiedad
//         , Divisa divi, Direccion dir
//         , Comuna com
//         , Venta_Arriendo vent 
//         , Origen
//         , Status
//         WHERE per.id_persona = propietario.fk_persona 
//         AND propietario.fk_estado_prop = est.id 
//         AND est.fk_sub_estado = sub.id 
//         AND propietario.id_propietario = propiedad.fk_propietario 
//         AND propiedad.divisa = divi.id 
//         AND propiedad.fk_direccion = dir.id 
//         and dir.fk_comuna = com.id 
//         AND propiedad.fk_venta_arriendo = vent.id
//         AND propietario.fk_origen = Origen.id_origen
//         AND propiedad.fk_estado_propiedad = 2
//         AND propiedad.fk_status = Status.id_status
//         AND( vent.descripcion LIKE" ."'" .$q1. "%'
//         OR per.nombre LIKE" ."'" .$q1. "%'
//         OR per.apellido LIKE" ."'" .$q1. "%'
//         OR per.telefono LIKE" ."'" .$q1. "%'
//         OR per.correo LIKE" ."'" .$q1. "%'
//         OR dir.calle LIKE" ."'" .$q1. "%'
//         OR com.nombre LIKE" ."'" .$q1. "%'
//         OR propiedad.monto LIKE" ."'" .$q1. "%'
//         OR est.tipo LIKE" ."'" .$q1. "%' 
//         OR sub.fase LIKE" ."'" .$q1. "%'
//         OR Status.status LIKE" ."'" .$q1. "%')
//         AND vent.descripcion LIKE" ."'%" .$q1. "%'
//         AND est.tipo LIKE" ."'%" .$q2. "%' 
//         AND sub.fase LIKE" ."'%" .$q3. "%'  
//     order by propietario.cont desc
//     " ;
 
// }else{
//     $q1 = $conn -> real_escape_string($_POST['consulta1']);
//     $query = "SELECT per.nombre
//         , per.apellido
//         , per.telefono
//         , per.correo
//         , per.id_persona as idPer
//         , dir.calle
//         , dir.numero
//         , dir.referencia
//         , dir.id as idDir
//         , com.nombre as Comuna
//         , com.id as idCom
//         , vent.descripcion V_A
//         , vent.id as idVent
//         , divi.nombre as divisa
//         , divi.id as idDiv
//         , propiedad.monto
//         , propiedad.dormitorios
//         , propiedad.bannos
//         , propiedad.estacionamiento
//         , propiedad.metros_totales
//         , propiedad.metros_utiles
//         , propiedad.bodega
//         , propiedad.id_propiedad as idPropiedad
//         , propiedad.fk_propietario
//         , est.tipo
//         , est.id as idEst
//         , sub.fase
//         , sub.id as idSub
//         , propietario.cont
//         , propietario.id_propietario as idPropietario
//         , Origen.nom_origen as ori
//         , Origen.id_origen as id_or
//         , Status.status 
//         FROM Persona per 
//         INNER JOIN Propietario propietario
//         , Estado_prop est
//         , Sub_estado sub
//         , Propiedad propiedad
//         , Divisa divi, Direccion dir
//         , Comuna com
//         , Venta_Arriendo vent 
//         , Origen
//         , Status
//         WHERE per.id_persona = propietario.fk_persona 
//         AND propietario.fk_estado_prop = est.id 
//         AND est.fk_sub_estado = sub.id 
//         AND propietario.id_propietario = propiedad.fk_propietario 
//         AND propiedad.divisa = divi.id 
//         AND propiedad.fk_direccion = dir.id 
//         and dir.fk_comuna = com.id 
//         AND propiedad.fk_venta_arriendo = vent.id
//         AND propietario.fk_origen = Origen.id_origen
//         AND propiedad.fk_estado_propiedad = 2
//         AND propiedad.fk_status = Status.id_status
//         AND( vent.descripcion LIKE" ."'" .$q1. "%'
//         OR per.nombre LIKE" ."'" .$q1. "%'
//         OR per.apellido LIKE" ."'" .$q1. "%'
//         OR per.telefono LIKE" ."'" .$q1. "%'
//         OR per.correo LIKE" ."'" .$q1. "%'
//         OR dir.calle LIKE" ."'" .$q1. "%'
//         OR com.nombre LIKE" ."'" .$q1. "%'
//         OR propiedad.monto LIKE" ."'" .$q1. "%'
//         OR est.tipo LIKE" ."'" .$q1. "%' 
//         OR sub.fase LIKE" ."'" .$q1. "%'
//         OR Status.status LIKE" ."'" .$q1. "%' ) 
//         order by propietario.cont desc 
//     " ;
// }

// $resultado = $conn->query($query);

// if($resultado->num_rows >0){
    
//     $salida.= "<table class='table table-striped' id='tabla_mixta'>

//     <thead>

//         <tr class='thead-dark'>

//         <th scope='col'> origen </th>

//         <th class='center-tab' scope='col'> # </th>

//         <th scope='col'> Nombre </th>

//         <th scope='col'> Apellido </th>	

//         <th scope='col'> Teléfono </th>

//         <th scope='col'> Correo </th>

//         <th scope='col'> Dirección </th>

//         <th scope='col'> Comuna </th>

//         <th class='center-tab' scope='col'> Operación </th>

//         <th scope='col'> Precio </th>

//         <th scope='col'> Status </th>

//         <th scope='col'> Estado </th>

//         <th scope='col'> Fase </th>

//         <th class='center-tab' scope='col'> Días </th>

//         <th class='center-tab' scope='col'> Acciones </th>	

//         </tr>

//     </thead>

//     <tbody>   
// ";


// $cont = 1;
// while($row = $resultado-> fetch_assoc()){
// $salida.= "
//     <tr class=' tb-data-single' >
//     <td class='table-item'>".$cont."</td>    
//     <td class='table-item'>" . utf8_encode($row["nombre"]) . "</td>    
//     <td class='table-item'>" . utf8_encode($row["apellido"]) . "</td>
//     <td class='table-item'>" . utf8_encode($row["telefono"]) . "</td>
//     <td class='table-item'>" . utf8_encode($row["correo"] ). "</td>
//     <td class='table-item'>" . utf8_encode($row["calle"]) ." ". utf8_encode($row["numero"]) ."</td>
//     <td class='table-item'>" . utf8_encode($row["Comuna"]) . "</td>
//     <td class="."' table-item ".utf8_encode($row["V_A"])."'".">" . utf8_encode($row["V_A"]) . "</td>
//     <td class='table-item'>" . utf8_encode($row["monto"]) ." " . utf8_encode($row["divisa"]) . "</td>
//     <td class='table-item'>" . utf8_encode($row["tipo"]) . "</td>
//     <td class='table-item'>" . utf8_encode($row["fase"]) . "</td>
//     <td class='center-tab'>" . utf8_encode($row["cont"]) . "</td>
//     <td class=center-tab>
//     <button class='btn-action bg-redflip-green' onClick='habilitarPropiedad(".$row['idPropiedad'].")'>Habilitar</button>
//     <button class='btn-action bg-redflip-redB' onClick='EliminarPropiedad(".$row["idPropiedad"].")'>Eliminar</button></td>
//     </tr>
//     ";
//     $cont ++;
// }

// $salida.="</tbody></table>";

// }else{

//     $salida.= "<table class='table table-striped' id='export_to_excel'>
//     <thead>
//         <tr class='thead-dark'>

//         <th scope='col' class='th-cabecera'> Datos de propietario </th>
//         <th scope='col'> # </th>
//         <th scope='col'> Nombre </th>
//         <th scope='col'> Apellido </th>	
//         <th scope='col'> Teléfono </th>
//         <th scope='col'> Correo </th>
//         <th scope='col'> Dirección </th>
//         <th scope='col'> Comuna </th>
//         <th scope='col'> Venta o Arriendo </th>
//         <th scope='col'> Precio </th>
//         <th scope='col'> Estado </th>
//         <th scope='col'> Fase </th>
//         <th class='center-tab' scope='col'> Contador </th>
//         <th class='center-tab' scope='col'> Acciones </th>	
//         </tr>
//     </thead>
//     <tbody>
//     <tr>
//     <td class= 'center-tab' colspan=13 > no hay datos</td>
//     </tr>    
// ";

// }
// echo $salida;

// $conn->close();
// echo $query;
$resultado = $conn->query($query);

if($resultado->num_rows >0){
    $salida.= "
    <table class='table table-responsive table-hover table-striped'>
            <thead class='thead-dark tab-center'>
                <tr class=''>
                    <th class='' scope='col'> origen </th>
                    <th class='tab-center' scope='col'> # </th>
                    <th class=' ' scope='col'> Nombre </th>
                    <th class='' scope='col'> Teléfono </th>
                    <th class='' scope='col'> Correo </th>
                    <th class='' scope='col'> Dirección </th>
                    <th class='' scope='col'> Comuna </th>
                    <th class='tab-center' scope='col'> Operación </th>
                    <th class='' scope='col'> Precio </th>
                    <th class='' scope='col'> Status </th>
                    <th class='' scope='col'> Estado </th>
                    <th class='' scope='col'> Fase </th>
                    <th class='tab-center' scope='col'> Días </th>
                    <th class='tab-center' scope='col'> Acciones </th>
                </tr>
            </thead>
            <tbody>    
";

$table_hidden= "

        <table class='table table-striped' id='export_to_excel' style='display:none'>
        <thead>
            <tr class='thead-dark'>
                <th scope='col'> origen </th>
                <th scope='col'> Nombre </th>
                <th scope='col'> Apellido </th>	
                <th scope='col'> Teléfono </th>
                <th scope='col'> Correo </th>
                <th scope='col'> Dirección </th>
                <th scope='col'> Comuna </th>
                <th class='center-tab' scope='col'> Tipo de Operación </th>
                <th scope='col'> Monto </th>
                <th scope='col'> Divisa </th>
                <th scope='col'> Dormitorios </th>
                <th scope='col'> Baños </th>
                <th scope='col'> Estacionamiento </th>
                <th scope='col'> Metros Totales </th>
                <th scope='col'> Metros Utiles </th>
                <th scope='col'> Bodega </th>
                <th scope='col'> Status </th>
                <th scope='col'> Estado </th>
                <th scope='col'> Fase </th>	
            </tr>
        </thead>
        <tbody> ";


while($row = $resultado-> fetch_assoc()){

    if($row["cont"]< 20){
        $salida.= "
        <tr class='tr-style'>";
        switch ($row["id_or"]) {
            case 1:
                $origen = "<td class='tab-center'><i class='fab fa-instagram' title='Instagram'></i></td>";
                break;

            case 2:
                $origen = "<td class='tab-center'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                break;

            case 3:
                $origen = "<td class='tab-center'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                break;

            case 4:
                $origen = "<td class='tab-center'><i class='fas fa-globe-americas' title='Web'></i></td>";
                break;

            case 5:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido conserje'></i></td>";
                break;

            case 6:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido cliente'></i></td>";
                break;

            case 7:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido corredor'></i></td>";
                break;

            case 8:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='DaHunter'></i></td>";
                break;

            case 9:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='BD Mailing'></i></td>";
                break;

            case 10:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='DoorList'></i></td>";
                break;

            case 11:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Open Casa'></i></td>";
                break;

            case 12:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='GI0'></i></td>";
                break;

            case 13:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Vilenky'></i></td>";
                break;

            case 14:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;

            case 15:
                $origen = "<td class='tab-center'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                break;

            default:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;
        }

    $propietario = $row["fk_propietario"];
    $nombre = utf8_encode($row["nombre"]);
    $apellido = utf8_encode($row["apellido"]);
    $telefono = utf8_encode($row["telefono"]) ;
    $correo = utf8_encode($row["correo"] );
    $calle = utf8_encode($row["calle"]);  
    $numero = utf8_encode($row["numero"]);
    $comuna = utf8_encode($row["Comuna"]);
    $operacion = utf8_encode($row["V_A"]);
    $monto = utf8_encode($row["monto"]);
    $divisa = utf8_encode($row["divisa"]);
    $status = utf8_encode($row["status"]);
    $tipo = utf8_encode($row["tipo"]);
    $fase = utf8_encode($row['fase']);
    $dormitorios = utf8_encode($row["dormitorios"]);
    $baños = utf8_encode($row["bannos"]);
    $estacionamiento = utf8_encode($row["estacionamiento"]);
    $mTotales = utf8_encode($row["metros_totales"]);
    $mutiles = utf8_encode($row["metros_utiles"]);
    $bodega = utf8_encode($row["bodega"]);
    $status = utf8_encode($row["status"]);
    $tipo = utf8_encode($row["tipo"]);
    $fase = utf8_encode($row["fase"]);
    $dias = utf8_encode($row["cont"]);
    $idPropiedad = $row["idPropiedad"];
    $idPer = $row["idPer"];
    $ori = utf8_encode($row["ori"]);

       $salida .= $origen."

       <td class=''> <pre class='tabPre'><i class='fas fa-circle fa-xs ci-gre'></i>".$conta."</pre></td>    
       <td class=''><pre class='tabPre'>" . utf8_encode($row["nombre"]) . " " .utf8_encode($row["apellido"]). "</pre></td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["telefono"]) . "</pre></td>
       <td class='' >" . utf8_encode($row["correo"] ). "</td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["calle"]) ." ". utf8_encode($row["numero"]) ."</pre></td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["Comuna"]) . "</pre></td>
       <td class='tab-center'>" . utf8_encode($row["V_A"]) . "</td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["monto"]) ." " . utf8_encode($row["divisa"]) . "</pre></td>
       <td class=''>" . utf8_encode($row["status"]) . "</td>
       <td class=''>" . utf8_encode($row["tipo"]) . "</td>";
        $conta ++;

        $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
             $salida.="
             <td  class=''>
                <select class= 'btn btn-r dropdown-toggle' name='".$idPropiedad."' id='".$idPropiedad."' onchange='cambiar(".$idPropiedad.", ".$propietario.")'> ";
            while($row = $result->fetch_assoc()) {
                if($row["fase"] == $fase){                    
                    $salida.="<option  value=".$row["idEst"]." selected>".utf8_encode($row['fase']) ."</option>";
                }else{
                    $salida.="<option value=".$row["idEst"].">".utf8_encode($row['fase']) ."</option>";  
                }
            }

        };
        $salida.="</select>
        </td>
        <td class='tab-center'>" . $dias . "</td>
        
        <td class='tab-center' style='display:inline-flex;margin-top:8%;'>
            <button class='btn-acciones bg-redflip-green' onClick='habilitarPropiedad(".$idPropiedad.")' title='Habilitar'><i class='fas fa-user-check'></i></button>
            <button class='btn-acciones bg-redflip-redB' onClick='EliminarPropiedad(".$idPer.")' title='Eliminar'><i class='fas fa-user-times'></i></button></td>
        </td>
    </tr>";

        $table_hidden.= " <tr>  

                <td >" . $ori. "</td>    
                <td >" . $nombre . "</td>    
                <td >" . $apellido . "</td>
                <td >" . $telefono . "</td>
                <td >" . $correo. "</td>
                <td >" . $calle ." ". $numero ."</td>
                <td >" . $comuna . "</td>
                <td class='center-tab'".$operacion."'".">" . $operacion . "</td>
                <td >" . $monto ."</td>
                <td>" . $divisa . "</td>
                <td >" . $dormitorios . "</td>
                <td >" . $baños . "</td>
                <td >" . $estacionamiento . "</td>
                <td >" . $mTotales . "</td>
                <td >" . $mutiles. "</td>
                <td >" . $bodega . "</td>
                <td >" . $status . "</td>
                <td >" . $tipo . "</td>
                <td >" . $fase . "</td>
                
            </tr>
        
        ";

        // ****************************************************************************************MEDIO********************************************
        
    }else if($row["cont"] >= 20 && $row["cont"] <=30){
        $salida.= "
        <tr class=' tb-data-single' >";

        switch ($row["id_or"]) {
            case 1:
                $origen = "<td class='tab-center'><i class='fab fa-instagram' title='Instagram'></i></td>";
                break;

            case 2:
                $origen = "<td class='tab-center'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                break;

            case 3:
                $origen = "<td class='tab-center'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                break;

            case 4:
                $origen = "<td class='tab-center'><i class='fas fa-globe-americas' title='Web'></i></td>";
                break;

            case 5:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido conserje'></i></td>";
                break;

            case 6:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido cliente'></i></td>";
                break;

            case 7:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido corredor'></i></td>";
                break;

            case 8:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='DaHunter'></i></td>";
                break;

            case 9:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='BD Mailing'></i></td>";
                break;

            case 10:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='DoorList'></i></td>";
                break;

            case 11:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Open Casa'></i></td>";
                break;

            case 12:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='GI0'></i></td>";
                break;

            case 13:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Vilenky'></i></td>";
                break;

            case 14:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;

            case 15:
                $origen = "<td class='tdProp propTab tabOri'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                break;

            default:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;
        }

        $fase =$row["fase"];

        $propietario = $row["fk_propietario"];

        $cont = $row["cont"];

        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $telefono = utf8_encode($row["telefono"]) ;
        $correo = utf8_encode($row["correo"] );
        $calle = utf8_encode($row["calle"]);  
        $numero = utf8_encode($row["numero"]);
        $comuna = utf8_encode($row["Comuna"]);
        $operacion = utf8_encode($row["V_A"]);
        $monto = utf8_encode($row["monto"]);
        $divisa = utf8_encode($row["divisa"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row['fase']);
        $dormitorios = utf8_encode($row["dormitorios"]);
        $baños = utf8_encode($row["bannos"]);
        $estacionamiento = utf8_encode($row["estacionamiento"]);
        $mTotales = utf8_encode($row["metros_totales"]);
        $mutiles = utf8_encode($row["metros_utiles"]);
        $bodega = utf8_encode($row["bodega"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row["fase"]);
        $dias = utf8_encode($row["cont"]);
        $idPropiedad = $row["idPropiedad"];
        $ori = utf8_encode($row["ori"]);



       $salida .= $origen."

       <td class=''> <pre class='tabPre'><i class='fas fa-circle fa-xs ci-yel'></i>".$conta."</pre></td>    
       <td class=''><pre class='tabPre'>" . utf8_encode($row["nombre"]) . " " .utf8_encode($row["apellido"]). "</pre></td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["telefono"]) . "</pre></td>
       <td class='' >" . utf8_encode($row["correo"] ). "</td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["calle"]) ." ". utf8_encode($row["numero"]) ."</pre></td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["Comuna"]) . "</pre></td>
       <td class='tab-center'>" . utf8_encode($row["V_A"]) . "</td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["monto"]) ." " . utf8_encode($row["divisa"]) . "</pre></td>
       <td class=''>" . utf8_encode($row["status"]) . "</td>
       <td class=''>" . utf8_encode($row["tipo"]) . "</td>";
        $conta ++;

        $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
             $salida.="
                    <td  class=''>
                        <select class= 'btn btn-r dropdown-toggle' name='".$idPropiedad."' id='".$idPropiedad."' onchange='cambiar(".$idPropiedad.", ".$propietario.")'> ";
            while($row = $result->fetch_assoc()) {
               
                if($row["fase"] == $fase){                    
                    $salida.="<option  value=".$row["idEst"]." selected>".utf8_encode($row['fase']) ."</option>";
                }else{
                    $salida.="<option value=".$row["idEst"].">".utf8_encode($row['fase']) ."</option>";  
                }
            }
        };
        $salida.="</select>

        </td>
        <td class='tab-center'>" . $dias . "</td>
        
        <td class='tab-center' style='display:inline-flex;margin-top:8%;'>
            <button class='btn-acciones bg-redflip-green' onClick='habilitarPropiedad(".$idPropiedad.")' title='Habilitar'><i class='fas fa-user-check'></i></button>
            <button class='btn-acciones bg-redflip-redB' onClick='EliminarPropiedad(".$idPer.")' title='Eliminar'><i class='fas fa-user-times'></i></button></td>
        </td>
    </tr>";

        $table_hidden.= "
            <tr>  
                <td>" .$ori  . "</td>    
                <td>" . $nombre . "</td>    
                <td>" . $apellido . "</td>
                <td>" . $telefono . "</td>
                <td>" . $correo. "</td>
                <td>" . $calle." ". $numero ."</td>
                <td>" . $comuna . "</td>
                <td class='center-tab'".$operacion."'".">" . $operacion . "</td>
                <td>" . $monto ."</td>
                <td>" . $divisa . "</td>
                <td>" . $dormitorios . "</td>
                <td>" . $baños . "</td>
                <td>" . $estacionamiento . "</td>
                <td>" . $mTotales . "</td>
                <td>" . $mutiles . "</td>
                <td>" . $bodega. "</td>
                <td>" . $status . "</td>
                <td>" . $tipo . "</td>
                <td>" . $fase . "</td>
            </tr>
        ";
        
        // ********************************************************ALTO*******************************************************
    }else if($row["cont"] > 30){
        
        $salida.= "<tr class=' tb-data-single' >";

        switch ($row["id_or"]) {
            case 1:
                $origen = "<td class='tab-center'><i class='fab fa-instagram' title='Instagram'></i></td>";
                break;

            case 2:
                $origen = "<td class='tab-center'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                break;

            case 3:
                $origen = "<td class='tab-center'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                break;

            case 4:
                $origen = "<td class='tab-center'><i class='fas fa-globe-americas' title='Web'></i></td>";
                break;

            case 5:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido conserje'></i></td>";
                break;

            case 6:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido cliente'></i></td>";
                break;

            case 7:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido corredor'></i></td>";
                break;

            case 8:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='DaHunter'></i></td>";
                break;

            case 9:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='BD Mailing'></i></td>";
                break;

            case 10:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='DoorList'></i></td>";
                break;

            case 11:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Open Casa'></i></td>";
                break;

            case 12:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='GI0'></i></td>";
                break;

            case 13:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Vilenky'></i></td>";
                break;

            case 14:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;

            case 15:
                $origen = "<td class='tdProp propTab tabOri'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                break;

            default:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break; 
        }

        $fase =$row["fase"];

        $propietario = $row["fk_propietario"];

        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $telefono = utf8_encode($row["telefono"]) ;
        $correo = utf8_encode($row["correo"] );
        $calle = utf8_encode($row["calle"]);  
        $numero = utf8_encode($row["numero"]);
        $comuna = utf8_encode($row["Comuna"]);
        $operacion = utf8_encode($row["V_A"]);
        $monto = utf8_encode($row["monto"]);
        $divisa = utf8_encode($row["divisa"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row['fase']);
        $dormitorios = utf8_encode($row["dormitorios"]);
        $baños = utf8_encode($row["bannos"]);
        $estacionamiento = utf8_encode($row["estacionamiento"]);
        $mTotales = utf8_encode($row["metros_totales"]);
        $mutiles = utf8_encode($row["metros_utiles"]);
        $bodega = utf8_encode($row["bodega"]);
        $status = utf8_encode($row["status"]);
        $tipo = utf8_encode($row["tipo"]);
        $fase = utf8_encode($row["fase"]);
        $dias = utf8_encode($row["cont"]);
        $idPropiedad = $row["idPropiedad"];
        $ori = utf8_encode($row["ori"]);
         


       $salida .= $origen."

       <td class=''> <pre class='tabPre'><i class='fas fa-circle fa-xs ci-red'></i>".$conta."</pre></td>    
       <td class=''><pre class='tabPre'>" . utf8_encode($row["nombre"]) . " " .utf8_encode($row["apellido"]). "</pre></td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["telefono"]) . "</pre></td>
       <td class='' >" . utf8_encode($row["correo"] ). "</td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["calle"]) ." ". utf8_encode($row["numero"]) ."</pre></td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["Comuna"]) . "</pre></td>
       <td class='tab-center'>" . utf8_encode($row["V_A"]) . "</td>
       <td class=''><pre class='tabPre'>" . utf8_encode($row["monto"]) ." " . utf8_encode($row["divisa"]) . "</pre></td>
       <td class=''>" . utf8_encode($row["status"]) . "</td>
       <td class=''>" . utf8_encode($row["tipo"]) . "</td>";
        $conta ++;

        $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
             $salida.="
                    <td  class=''>
                        <select class= 'btn btn-r dropdown-toggle' name='".$idPropiedad."' id='".$idPropiedad."' onchange='cambiar(".$idPropiedad.", ".$propietario.")'> ";
            while($row = $result->fetch_assoc()) {
         
                if($row["fase"] == $fase){                    
                    $salida.="<option  value=".$row["idEst"]." selected>".utf8_encode($row['fase']) ."</option>";
                }else{
                    $salida.="<option value=".$row["idEst"].">".utf8_encode($row['fase']) ."</option>";  
                }
            }
        };

        $salida.="</select>

        </td>

        <td class='tab-center'>" . $dias . "</td>
        <td class='tab-center' style='display:inline-flex;margin-top:8%;'>
            <button class='btn-acciones bg-redflip-green' onClick='habilitarPropiedad(".$idPropiedad.")' title='Habilitar'><i class='fas fa-user-check'></i></button>
            <button class='btn-acciones bg-redflip-redB' onClick='EliminarPropiedad(".$idPer.")' title='Eliminar'><i class='fas fa-user-times'></i></button></td>
        </td>
    </tr>";
        
        $table_hidden.= "

            <tr>  
                <td>" .$ori  . "</td>    
                <td>" . $nombre . "</td>    
                <td>" . $apellido . "</td>
                <td>" . $telefono . "</td>
                <td>" . $correo. "</td>
                <td>" . $calle." ". $numero ."</td>
                <td>" . $comuna . "</td>
                <td class='center-tab'".$operacion."'".">" . $operacion . "</td>
                <td>" . $monto ."</td>
                <td>" . $divisa . "</td>
                <td>" . $dormitorios . "</td>
                <td>" . $baños . "</td>
                <td>" . $estacionamiento . "</td>
                <td>" . $mTotales . "</td>
                <td>" . $mutiles . "</td>
                <td>" . $bodega. "</td>
                <td>" . $status . "</td>
                <td>" . $tipo . "</td>
                <td>" . $fase . "</td>
            </tr>
        ";
    }
}

$salida.="</tbody></table>";

}else{

    $salida.= "
    <table class='table table-responsive table-hover table-striped' id='tabla_mixta' style='display:inline-table;'>
        <thead class='thead-dark tab-center'>
            <tr class=''>
                <th scope='col'> Origen </th>
                <th scope='col'> # </th>
                <th scope='col'> Nombre </th>
                <th scope='col'> Teléfono </th>
                <th scope='col'> Correo </th>
                <th scope='col'> Dirección </th>
                <th scope='col'> Comuna </th>
                <th class='' scope='col'>Operación </th>
                <th scope='col'> Precio </th>
                <th scope='col'> Status </th>
                <th scope='col'> Estado </th>
                <th scope='col'> Fase </th>
                <th class='' scope='col'> Contador </th>
                <th class='' scope='col'> Acciones </th>	
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class= 'tab-center' colspan=14 > No hay Datos</td>
            </tr>
        </tbody>
    </table>
    ";

    $table_hidden.= "
            <tr>  
                <td>" .$ori  . "</td>    
                <td>" . $nombre . "</td>    
                <td>" . $apellido . "</td>
                <td>" . $telefono . "</td>
                <td>" . $correo. "</td>
                <td>" . $calle." ". $numero ."</td>
                <td>" . $comuna . "</td>
                <td class='center-tab'".$operacion."'".">" . $operacion . "</td>
                <td>" . $monto ."</td>
                <td>" . $divisa . "</td>
                <td>" . $dormitorios . "</td>
                <td>" . $baños . "</td>
                <td>" . $estacionamiento . "</td>
                <td>" . $mTotales . "</td>
                <td>" . $mutiles . "</td>
                <td>" . $bodega. "</td>
                <td>" . $status . "</td>
                <td>" . $tipo . "</td>
                <td>" . $fase . "</td>
            </tr>
        ";
}
echo $salida.$table_hidden;
$conn->close();

?>