<?
  include '../conexion.php';
    if(!isset($_GET['id'])){
      header('Location: ../404.php');
      die;
    }
    
    $id=$_GET['id'];
    $sql2 = "Update usuario SET ultima_pag='https://indev9.com/redflip/pages/editarProp.php?id=$id' where id_usuario =". $_SESSION['id'];
    if ($conn->query($sql2) === TRUE) {
    } else {
    }
  include '../controlador/EditarPropietario.php';
?>

<section>
  <div class="container-fluid col-12" style="margin-bottom:150px;">
    <div class="row">
      <div class="column col-12">
        <h3 class="h3-prop mtb-50">Editar propietario</h3>
        <form id="formulario">
          <div class="container-fluid">
            <div class="row">
              <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6 " tabindex="1">
                <img src="../img/staying.svg" alt="">
              </div>
              <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6 editPropA" tabindex="1">
                <!-- [Nombre - Apellido] -->
                <div class="form-group row">
                  <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                    <input value=<?echo utf8_encode($id)?> type="hidden" class="form-control" id="id" name="id" aria-describedby="emailHelp" placeholder="Nombre">
                    <label class="input-form" for="exampleInputEmail1">Nombre</label>
                      <?if(isset($nom)){?>
                        <input value="<?echo utf8_encode($nom)?>" type="text" class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Nombre" onfocusout="validarNombre()">
                      <?
                      }else{
                      ?>
                        <input value="" type="text" class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Nombre">
                      <?
                      }
                      ?>
                  </div>
                  <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                    <label class="input-form" for="exampleInputPassword1">Apellido</label>
                      <?
                      if(isset($apellido)){
                        ?>
                        <input value="<?echo utf8_encode($apellido);?>" type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" onfocusout="validarApellido()">
                        <?
                      }else{
                        ?>
                        <input value="" type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido">
                        <?
                      }
                      ?>
                  </div>
                </div>
                <!-- [Estado - Fase] -->
                <div class="form-group row">
                  <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                    <label class="input-form" for="exampleInputPassword1">Estado</label>
                    <input value="<?echo utf8_encode($estado)?>" type="text" class="form-control" id="estado" name="estado" placeholder="estado" disabled>
                  </div>
                  <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                    <label class="input-form" for="exampleInputPassword1">Fase</label><br>
                    <input class="select-form" type="hidden" id="fase1" value=<? echo $fase?> >
                    
                    <select id="fase" name="fase" class="selectEditProp">
                    <? 
                    $sqlFase="Select * from Sub_estado";
                    $result = $conn->query($sqlFase);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                  if($fase == $row["fase"]){
                                   echo "<option value=".$row['id']." selected>".$row['fase']."</option>";
                                  }else{
                                    echo "<option value=".$row['id'].">".$row['fase']."</option>";
                                  }
                                }
                              }
                    
                    ?>
                          
                           
                          </select>
                  </div>
                </div>
                <!-- [RUT - F. Nac] -->
                <div class="form-group row">
                  <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                    <label class="input-form" for="exampleInputPassword1">Rut</label>
                    <input value="<?echo $rut?>" type="text" class="form-control" id="rut" name="rut" placeholder="12345678-9" onfocusout="validarRut2()">
                  </div>
                  <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                    <label class="input-form" for="exampleInputPassword1">Fecha de nacimiento</label>
                    <?if(isset($fec)){?>
                      <input value="<?echo $fec?>" type="date" class="form-control" id="fec_nac" name="fec_nac" placeholder="YYYY-MM-DD">
                    <?}else{?>
                      <input value="" type="date" class="form-control" id="fec_nac" name="fec_nac" placeholder="YYYY-MM-DD">
                    <?}?>
                  </div>
                </div>
                <!-- [Fono - Mail] -->
                <div class="form-group row">
                  <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                    <label class="input-form" for="exampleInputPassword1">Teléfono</label>
                    <input value="<?echo $tel?>" type="text" class="form-control" id="tel" name="tel" placeholder="+56 9 1234 5678" onfocusout="validarTelefono()">
                    <div id="error-tel"></div>
                  </div>
                  <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                    <label class="input-form" for="exampleInputPassword1">Correo</label>
                    <input value="<?echo $correo ?>" type="text" class="form-control" id="correo" name="correo" placeholder="Correo" onfocusout="validarCorreo()">
                  </div>
                </div>
                <!-- [Residencia] -->
                <div class="form-group row">
                  <div class="column col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <label class="input-form" for="exampleInputPassword1">Calle</label>
                    <input type="text" class="form-control" id="calle" placeholder="ej. Av. kennedy" name="calle" value="<? echo $calle; ?>" ></input>
                  </div>
                  <div class="column col-xs-12 col-sm-12 col-md-2 col-lg-3">
                    <label class="input-form" for="exampleInputPassword1">Número</label>
                    <input type="number" class="form-control" id="numero" placeholder="Ej. 1..." name="numero" value="<? echo $numero ?>">
                  </div>

                  <div class="column col-xs-12 col-sm-12 col-md-5 col-lg-4">
                    <label class="input-form" for="exampleInputPassword1">Comuna</label> </br>
                      <select id="comuna" name="comuna" class="selectEditProp">
                        <?
                          $sqlComuna = "SELECT * FROM Comuna";
  
                          $result = $conn->query($sqlComuna);
                            if ($result->num_rows > 0) {
                            // output data of each row
                              while($row = $result->fetch_assoc()) {
                                if($comuna == $row['id']){
                                  echo "<option value=". utf8_encode($row['id'])." selected>". utf8_encode($row['nombre'])."</option>";
                                }else{
                                  echo "<option value=". utf8_encode($row['id'])." >". utf8_encode($row['nombre'])."</option>";  
                                }
                              }
                            }else{
                            }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="form-group row">
                    <div class="column col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <label class="input-form" for="exampleInputPassword1">Referencia</label>
                      <input type="text" class="form-control" id="ref" placeholder="ej. metro manquehue" name="ref" value="<? echo $ref ?>">
                      </div>
                </div>

                <!-- [Origen] -->
                <div class="form-group row">
                  <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                      <label class="input-form" for="exampleInputPassword1">Origen</label></br>
                        <select id="origen" name="origen" class="selectEditProp">
                          <?
                            $sql = "Select * from Origen";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                  if($fk_origen == $row['id_origen']){
                                    echo "<option value=".$row['id_origen']." selected>".$row['nom_origen']."</option>";
                                  }else{
                                    echo "<option value=".$row['id_origen']." >".$row['nom_origen']."</option>";
                                  }
                                }
                            }
                          ?>
                        </select>
                  </div>
                </div>
                
                <button type="submit" class="btn-def btn-login bg-redflip-red">Guardar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      </div>
    </div>
  </div>
</section>