<?php 
include "../conexion.php";
$cont = 1;

$salida = "";
$tableHidden = "";
$query = 'SELECT DISTINCT 
                    *
                    from Interesado , Persona 
                    WHERE Interesado.fk_persona = Persona.id_persona'
                ;
echo $query;   
$resultado = $conn->query($query);

if($resultado->num_rows >0){
    $salida.= "
        <table class='table table-responsive table-hover table-striped' id='propietarios'>
            <thead>
                <tr class='thead-dark tab-center'>
                    <th class='' scope='col'> origen </th>
                    <th class='' scope='col'> # </th>
                    <th class='' scope='col'> Nombre </th>
                    <th class='' scope='col'> Teléfono </th>
                    <th class='' scope='col'> Corredor </th>
                    <th class='' scope='col'> Correo </th>
                    <th class=' center-tab' scope='col'> Comuna </th>
                    <th class=' center-tab' scope='col'> Orientacop </th>
                    <th class=' center-tab' scope='col'> Acciones </th>	
                <tr>
            </thead>
            <tbody>
        ";

        //ESTA TABLA SE IMPRIME EN EL EXCEL [NO C VE]
$tableHidden.= "
        <table class='table table-responsive table-hover table-striped' id='export_to_excel' style='display:none!important'>
            <thead>
                <tr class='thead-dark'>
                    <th class='tabOri' scope='col'> origen </th>
                    <th scope='col'> # </th>
                    <th scope='col'> Nombre </th>
                    <th scope='col'> Apellido </th>	
                    <th scope='col'> Estado </th>
                    <th class='fa-1600' scope='col'> Fase </th>
                    <th scope='col'> Contactos </th>
                    <th scope='col'> Rut </th>
                    <th scope='col'> Teléfono </th>
                    <th scope='col'> Correo </th>
                    <th class='center-tab' scope='col'> Días </th>	
                <tr>
            </thead>
            <tbody>
        ";
// echo $query;
    while($row = $resultado-> fetch_assoc()){

        //Prioridad Baja
        if($row["cont"] < 20){
             switch ($row["id_or"]) {
                case 1:
                    $origen = "<td class='tab-center'><i class='fab fa-instagram' title='Instagram'></i></td>";
                    break;

                case 2:
                    $origen = "<td class='tab-center'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                    break;

                case 3:
                    $origen = "<td class='tab-center'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                    break;

                case 4:
                    $origen = "<td class='tab-center'><i class='fas fa-globe-americas' title='Web'></i></td>";
                    break;

                case 5:
                    $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido conserje'></i></td>";
                    break;

                case 6:
                    $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido cliente'></i></td>";
                    break;

                case 7:
                    $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido corredor'></i></td>";
                    break;

                case 8:
                    $origen = "<td class='tab-center'><i class='fas fa-database' title='DaHunter'></i></td>";
                    break;

                case 9:
                    $origen = "<td class='tab-center'><i class='fas fa-database' title='BD Mailing'></i></td>";
                    break;

                case 10:
                    $origen = "<td class='tab-center'><i class='fas fa-sync' title='DoorList'></i></td>";
                    break;

                case 11:
                    $origen = "<td class='tab-center'><i class='fas fa-sync' title='Open Casa'></i></td>";
                    break;

                case 12:
                    $origen = "<td class='tab-center'><i class='fas fa-sync' title='GI0'></i></td>";
                    break;

                case 13:
                    $origen = "<td class='tab-center'><i class='fas fa-sync' title='Vilenky'></i></td>";
                    break;

                case 14:
                    $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                    break;

                case 15:
                    $origen = "<td class='tab-center'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                    break;

                default:
                    $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                    break;
            }

        $salida.= "

        <tr class='tr-style'>
        ".$origen."
            <td class=''> <pre class='tabPre'><i class='fas fa-circle fa-xs ci-gre'></i>".$row["id_propietario"]."</pre></td>
            <td class=''>" . utf8_encode($row["nombre"]) ."</td>
            <td class=''>" . utf8_encode($row["apellido"]) . "</td>
            <td class=''>" . $row["tipo"] . "</td>";
        
        $tableHidden.= "

        <tr class='' >
            <td class=''> ".$row["id_propietario"]."</td>
            <td class=''>" . utf8_encode($row["nombre"]) ."</td>
            <td class=''>" . utf8_encode($row["apellido"]) . "</td>
            <td class=''>" . $row["tipo"] . "</td>";


            // <td class='propTab'>" . utf8_encode($row["fase"] ). "</td>
            
            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
           $thisFase = "";

            if ($result->num_rows > 0) {
                switch ($row["id_or"]) {
                    case 1:
                        $origen = "<td class='tab-center'><i class='fab fa-instagram' title='Instagram'></i></td>";
                        break;
    
                    case 2:
                        $origen = "<td class='tab-center'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                        break;
    
                    case 3:
                        $origen = "<td class='tab-center'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                        break;
    
                    case 4:
                        $origen = "<td class='tab-center'><i class='fas fa-globe-americas' title='Web'></i></td>";
                        break;
    
                    case 5:
                        $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido conserje'></i></td>";
                        break;
    
                    case 6:
                        $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido cliente'></i></td>";
                        break;
    
                    case 7:
                        $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido corredor'></i></td>";
                        break;
    
                    case 8:
                        $origen = "<td class='tab-center'><i class='fas fa-database' title='DaHunter'></i></td>";
                        break;
    
                    case 9:
                        $origen = "<td class='tab-center'><i class='fas fa-database' title='BD Mailing'></i></td>";
                        break;
    
                    case 10:
                        $origen = "<td class='tab-center'><i class='fas fa-sync' title='DoorList'></i></td>";
                        break;
    
                    case 11:
                        $origen = "<td class='tab-center'><i class='fas fa-sync' title='Open Casa'></i></td>";
                        break;
    
                    case 12:
                        $origen = "<td class='tab-center'><i class='fas fa-sync' title='GI0'></i></td>";
                        break;
    
                    case 13:
                        $origen = "<td class='tab-center'><i class='fas fa-sync' title='Vilenky'></i></td>";
                        break;
    
                    case 14:
                        $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                        break;

                    case 15:
                        $origen = "<td class='tab-center'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                        break;
    
                    default:
                        $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                        break;
                }
                // output data of each row
                if($row['fase'] == "Contacto"){
                    $salida.="
                        <td class='contacto' id='select_".$row["id_persona"]."'>
                            <select class= 'btn btn-r dropdown-toggle' name='".$row['id_persona']."' id='".$row['id_persona']."' onchange='cambiar2(".$row['id_persona'].", ".$row['id_propietario'].")'> ";
                }else{
                    $salida.="
                    <td class='' id='select_".$row["id_persona"]."'>
                        <select class= 'btn btn-r dropdown-toggle' name='".$row['id_persona']."' id='".$row['id_persona']."' onchange='cambiar2(".$row['id_persona'].", ".$row['id_propietario'].")'> ";
                }

                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        $tableHidden.="<td>".$row2['fase'] ."</td>";
                        $tableHidden.="<td>".$row['contactos'] ."</td>";
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                    }
                }
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='btn-border' id='input_".$row['id_propietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['id_propietario'].")' >";
                }
            }
           
           $salida.= "
            <td class=''><pre class='tabPre'>" . $row["rut"] . "</pre></td>
            <td class=''>" . $row["telefono"] . "</td>
            <td class=''>" . utf8_encode($row["correo"]) . "</td>
            <td class='tab-center'>" . utf8_encode($row["cont"]) . "</td>
            <td class=''>
                <div class='btn-group dropleft'>
                    <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        <i class='fas fa-cog'></i>
                    </button>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='editarProp.php?id=".$row['id_persona']."'><i class='fas fa-edit'></i> Modificar</a>
                        <a class='dropdown-item' href='' onClick=desPropietario(".$row['id_persona'].")><i class='fas fa-user-slash'></i> Deshabilitar</a>
                        <a class='dropdown-item' href='https://api.whatsapp.com/send?phone=".$row["telefono"]."&text=Hola%20quiero%20info' target='_blank' ";

                    if(strlen($row["telefono"]) != 12){
                        $salida.="disabled";

                    }else{
                        $salida.="";
                    }
                    
                    $salida.="><i class='fab fa-whatsapp'></i> Whatsapp</a>
                </td>
            </tr>";

        $tableHidden.="<td class=''>" . $row["rut"] . "</td>
                        <td class=''>" . $row["telefono"] . "</td>
                        <td class=''>" . utf8_encode($row["correo"]) . "</td>
                        <td class=''>" .$row["cont"] . "</td>
                </tr>";

        $cont ++;

    }else if($row["cont"] >= 20 && $row["cont"] <=30){

        switch ($row["id_or"]) {
            case 1:
                $origen = "<td class='tab-center'><i class='fab fa-instagram' title='Instagram'></i></td>";
                break;

            case 2:
                $origen = "<td class='tab-center'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                break;

            case 3:
                $origen = "<td class='tab-center'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                break;

            case 4:
                $origen = "<td class='tab-center'><i class='fas fa-globe-americas' title='Web'></i></td>";
                break;

            case 5:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido conserje'></i></td>";
                break;

            case 6:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido cliente'></i></td>";
                break;

            case 7:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido corredor'></i></td>";
                break;

            case 8:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='DaHunter'></i></td>";
                break;

            case 9:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='BD Mailing'></i></td>";
                break;

            case 10:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='DoorList'></i></td>";
                break;

            case 11:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Open Casa'></i></td>";
                break;

            case 12:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='GI0'></i></td>";
                break;

            case 13:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Vilenky'></i></td>";
                break;

            case 14:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;

            case 15:
                $origen = "<td class='tab-center'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                break;

            default:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;
        }
        
        $salida.= "
        <tr class='tr-style'>
        ".$origen."
            <td class=''><pre class='tabPre'><i class='fas fa-circle fa-xs ci-yel'></i>".$row["id_propietario"]."</pre></td>
            <td class=''>" . utf8_encode($row["nombre"]) ."</td>
            <td class=''>" . utf8_encode($row["apellido"]) . "</td>
            <td class=''>" . $row["tipo"] . "</td>";

        $tableHidden.= "
        <tr class='' >
            <td class=''> ".$row["id_propietario"]."</td>
            <td class=''>" . utf8_encode($row["nombre"]) ."</td>
            <td class=''>" . utf8_encode($row["apellido"]) . "</td>
            <td class=''>" . $row["tipo"] . "</td>";



            // <td class='propTab'>" . utf8_encode($row["fase"] ). "</td>

            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
            $thisFase = "";
            if ($result->num_rows > 0) {
                // output data of each row
                if($row['fase'] == "Contacto"){
                    $salida.="
                    <td class='contacto' id='select_".$row["id_persona"]."'>
                        <select class= 'btn btn-r dropdown-toggle' name='".$row['id_persona']."' id='".$row['id_persona']."' onchange='cambiar2(".$row['id_persona'].", ".$row['id_propietario'].")'> ";
                }else{
                    $salida.="
                        <td class='' id='select_".$row["id_persona"]."'>
                            <select class= 'btn btn-r dropdown-toggle' name='".$row['id_persona']."' id='".$row['id_persona']."' onchange='cambiar2(".$row['id_persona'].", ".$row['id_propietario'].")'> ";
                }
                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        $tableHidden.="<td>".$row2['fase'] ."</td>";
                        $tableHidden.="<td>".$row['contactos'] ."</td>";
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                    }
                } 
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='btn-border' id='input_".$row['id_propietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['id_propietario'].")' >";
                }
            }
            
           $salida.= "
            <td class=''> <pre class='tabPre'>" . $row["rut"] . "</pre></td>
            <td class=''>" . $row["telefono"] . "</td>
            <td class=''>" . utf8_encode($row["correo"]) . "</td>
            <td class='tab-center'>" . utf8_encode($row["cont"]) . "</td>
            <td class=''>
                <div class='btn-group dropleft'>
                    <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        <i class='fas fa-cog'></i>
                    </button>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='editarProp.php?id=".$row['id_persona']."'><i class='fas fa-edit'></i> Modificar</a>
                        <a class='dropdown-item' href='' onClick=desPropietario(".$row['id_persona'].")><i class='fas fa-user-slash'></i> Deshabilitar</a>
                        <a class='dropdown-item' href='https://api.whatsapp.com/send?phone=".$row["telefono"]."&text=Hola%20quiero%20info' target='_blank' ";

                    if(strlen($row["telefono"]) != 12){
                        $salida.="disabled";

                    }else{
                        $salida.="";
                    }
                    
                    $salida.="><i class='fab fa-whatsapp'></i> Whatsapp</a>
                </td>
            </tr>";
             
            $tableHidden.="
                <td class=''>" . $row["rut"] . "</td>
                <td class=''>" .$row["telefono"]. "</td>
                <td class=''>" . utf8_encode($row["correo"]) . "</td>
                <td class=''>" .$row["cont"]. "</td>
            </tr>";
        $cont ++;

    }else if($row["cont"] > 30){
        switch ($row["id_or"]) {
            case 1:
                $origen = "<td class='tab-center'><i class='fab fa-instagram' title='Instagram'></i></td>";
                break;

            case 2:
                $origen = "<td class='tab-center'><i class='fab fa-facebook-square' title='Facebook'</i></td>";
                break;

            case 3:
                $origen = "<td class='tab-center'><i class='fab fa-linkedin' title='Linkedin'></i></td>";
                break;

            case 4:
                $origen = "<td class='tab-center'><i class='fas fa-globe-americas' title='Web'></i></td>";
                break;

            case 5:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido conserje'></i></td>";
                break;

            case 6:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido cliente'></i></td>";
                break;

            case 7:
                $origen = "<td class='tab-center'><i class='fas fa-pacman' title='Referido corredor'></i></td>";
                break;

            case 8:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='DaHunter'></i></td>";
                break;

            case 9:
                $origen = "<td class='tab-center'><i class='fas fa-database' title='BD Mailing'></i></td>";
                break;

            case 10:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='DoorList'></i></td>";
                break;

            case 11:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Open Casa'></i></td>";
                break;

            case 12:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='GI0'></i></td>";
                break;

            case 13:
                $origen = "<td class='tab-center'><i class='fas fa-sync' title='Vilenky'></i></td>";
                break;

            case 14:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;

            case 15:
                $origen = "<td class='tab-center'> <i class='far fa-file-alt' title='Formulario'></i> </td>";
                break;

            default:
                $origen = "<td class='tab-center'> <i class='fas fa-ghost' title='Sin definir'></i> </td>";
                break;
        }

        $salida.= "
            <tr class='tr-style' >
        ".$origen."
            <td class=''> <pre class='tabPre'><i class='fas fa-circle fa-xs ci-red'></i>".$row["id_propietario"]."</pre></td>
            <td class=''>" . utf8_encode($row["nombre"]) ."</td>
            <td class=''>" . utf8_encode($row["apellido"]) . "</td>
            <td class=''>" . $row["tipo"] . "</td>";

        $tableHidden.= "
        <tr class='' >
            <td class=''> ".$row["id_propietario"]."</td>
            <td class=''>" . utf8_encode($row["nombre"]) ."</td>
            <td class=''>" . utf8_encode($row["apellido"]) . "</td>
            <td class=''>" . $row["tipo"] . "</td>";

            // <td class='propTab'>" . utf8_encode($row["fase"] ). "</td>
            
            $sql="Select Sub_estado.id as idSub , fase, Estado_prop.id as idEst from Estado_prop, Sub_estado where Sub_estado.id = Estado_prop.fk_sub_estado";
            $result = $conn->query($sql);
            $thisFase = "";
            if ($result->num_rows > 0) {
                // output data of each row
                if($row['fase'] == "Contacto"){
                    $salida.="
                    <td class='contacto' id='select_".$row["id_persona"]."'>
                    <select class= 'btn btn-r dropdown-toggle' name='".$row['id_persona']."' id='".$row['id_persona']."' onchange='cambiar2(".$row['id_persona'].", ".$row['id_propietario'].")'> ";
                }else{
                    $salida.="
                    <td class='' id='select_".$row["id_persona"]."'>
                    <select class= 'btn btn-r dropdown-toggle' name='".$row['id_persona']."' id='".$row['id_persona']."' onchange='cambiar2(".$row['id_persona'].", ".$row['id_propietario'].")'> ";
                }
                while($row2 = $result->fetch_assoc()) {
                    if($row2["fase"] == $row["fase"]){                    
                        $salida.="<option  value=".$row2["idEst"]." selected>".$row2['fase'] ."</option>";
                        $thisFase = $row["fase"];
                        $tableHidden.="<td>".$row2['fase'] ."</td>";
                        $tableHidden.="<td>".$row['contactos'] ."</td>";
                    }else{
                        $salida.="<option value=".$row2["idEst"].">".$row2['fase'] ."</option>";  
                    }
                } 
                if($thisFase == "Contacto"){
                    $salida.="<input type='number' class='btn-border' id='input_".$row['id_propietario']."' value='".$row['contactos']."' onchange='insertarContactos(".$row['id_propietario'].")' >";
                }
            }
            
            $salida.= "
            <td class=''> <pre class='tabPre'>" . $row["rut"] . "</pre></td>
            <td class=''>" . $row["telefono"] . "</td>
            <td class=''>" . utf8_encode($row["correo"]) . "</td>
            <td class=''>" . utf8_encode($row["cont"]) . "</td>

            <td class=''>
                <div class='btn-group dropleft'>
                    <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        <i class='fas fa-cog'></i>
                    </button>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='editarProp.php?id=".$row['id_persona']."'><i class='fas fa-edit'></i> Modificar</a>
                        <a class='dropdown-item' href='' onClick=desPropietario(".$row['id_persona'].")><i class='fas fa-user-slash'></i> Deshabilitar</a>
                        <a class='dropdown-item' href='https://api.whatsapp.com/send?phone=".$row["telefono"]."&text=Hola%20quiero%20info' target='_blank' ";

                    if(strlen($row["telefono"]) != 12){
                        $salida.="disabled";

                    }else{
                        $salida.="";
                    }
                    
                    $salida.="><i class='fab fa-whatsapp'></i> Whatsapp</a>
                </td>
            </tr>";

            $tableHidden.="
            <td class=''>" . $row["rut"] . "</td>
            <td class=''>" .$row["telefono"] . "</td>
            <td class=''>" . utf8_encode($row["correo"]) . "</td>
            <td class='tab-center'>" . utf8_encode($row["cont"]) . "</td>
            

        </tr>";
        $cont ++;

        }
    }
}else{
    $salida.= "
        <table class='table table-responsive table-hover table-striped' style='display:inline-table!important;'>
            <thead class='thead-dark tab-center'>
                <tr>
                    <th scope='col'> Origen </th>
                    <th scope='col'> # </th>
                    <th scope='col'> Nombre </th>
                    <th scope='col'> Apellido </th>	
                    <th scope='col'> Estado </th>
                    <th scope='col'class='fa-1600'> Fase </th>
                    <th scope='col'> Rut </th>
                    <th scope='col'> Teléfono </th>
                    <th scope='col'> Correo </th>
                    <th class='' scope='col'> Días </th>
                    <th class='' scope='col'> Acciones </th>	
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class= 'tab-center' colspan=11 > no hay datos</td>
                </tr>
            </tbody>
        </table>
    ";
    $tableHidden.= "
        <table class='table table-striped' style='display:none!important;' id='export_to_excel'>
            <thead>
                <tr class='thead-dark'>
                    <th scope='col'> # </th>
                    <th scope='col'> Nombre </th>
                    <th scope='col'> Apellido </th>	
                    <th scope='col'> Estado </th>
                    <th scope='col'> Fase </th>
                    <th scope='col'> Rut </th>
                    <th scope='col'> Teléfono </th>
                    <th scope='col'> Correo </th>
                    <th class='center-tab' scope='col'> Días </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class= 'center.tab' colspan=10 > no hay datos</td>
                </tr>
            </tbody>
        </table>
    ";
}

echo $salida;
echo "</br>";
echo $tableHidden;
$conn->close();
?>