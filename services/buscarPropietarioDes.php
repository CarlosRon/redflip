<?php 

include "../conexion.php";

$salida = "";

$query = 'SELECT DISTINCT Persona.id_persona 
                    , Persona.nombre 
                    , Persona.apellido 
                    , Persona.rut 
                    , Persona.fec_nac 
                    , Persona.telefono 
                    , Persona.correo 
                    , Propietario.cont 
                    , Sub_estado.fase
                    , Estado_prop.tipo from Persona INNER JOIN Propietario 
                    , usuario, Estado_prop
                    , Sub_estado where Persona.id_persona = usuario.fk_persona 
                    AND usuario.fk_estado_us = 2 
                    AND Propietario.fk_persona = Persona.id_persona 
                    AND Estado_prop.id = Propietario.fk_estado_prop 
                    AND Sub_estado.id = Estado_prop.fk_sub_estado
                    AND Propietario.eliminado = false';


    if(isset($_POST["source"])){
            if($_POST["source"] == 1){

                $q1 = $conn -> real_escape_string($_POST['consulta2']);
                $q2 = $conn -> real_escape_string($_POST['consulta3']);
                
                

                $query = "SELECT DISTINCT Persona.id_persona 
                , Persona.nombre 
                , Persona.apellido 
                , Persona.rut 
                , Persona.fec_nac 
                , Persona.telefono 
                , Persona.correo 
                , Propietario.cont 
                , Sub_estado.fase
                , Estado_prop.tipo from Persona INNER JOIN Propietario 
                , usuario, Estado_prop
                , Sub_estado where Persona.id_persona = usuario.fk_persona 
                AND usuario.fk_estado_us = 2 
                AND Propietario.fk_persona = Persona.id_persona 
                AND Estado_prop.id = Propietario.fk_estado_prop 
                AND Sub_estado.id = Estado_prop.fk_sub_estado
                AND Propietario.eliminado = false
                AND Estado_prop.tipo LIKE" ."'%" .$q1. "%' 
                AND Sub_estado.fase LIKE" ."'%" .$q2. "%'" ; 

                

            }else if($_POST["source"] == 2){
                $estado = $conn -> real_escape_string($_POST['consulta2']);
                $fase = $conn -> real_escape_string($_POST['consulta3']);
                $search = $conn -> real_escape_string($_POST['consulta4']);
                
                

                $query = "SELECT DISTINCT Persona.id_persona 
                , Persona.nombre 
                , Persona.apellido 
                , Persona.rut 
                , Persona.fec_nac 
                , Persona.telefono 
                , Persona.correo 
                , Propietario.cont 
                , Sub_estado.fase
                , Estado_prop.tipo from Persona INNER JOIN Propietario 
                , usuario, Estado_prop
                , Sub_estado where Persona.id_persona = usuario.fk_persona 
                AND usuario.fk_estado_us = 2 
                AND Propietario.fk_persona = Persona.id_persona 
                AND Estado_prop.id = Propietario.fk_estado_prop 
                AND Sub_estado.id = Estado_prop.fk_sub_estado
                AND Propietario.eliminado = false
                AND Estado_prop.tipo LIKE '$estado%' 
                AND Sub_estado.fase LIKE '$fase%'
                AND (Estado_prop.tipo LIKE '$search%'
                OR Persona.nombre LIKE '$search%'
                OR Persona.apellido LIKE '$search%' 
                OR Sub_estado.fase LIKE '$search%' 
                OR Persona.rut LIKE '$search%' 
                OR Persona.telefono LIKE '$search%'
                OR Persona.correo LIKE '$search%' 
                )";

            }
    }

$resultado = $conn->query($query);

if($resultado->num_rows >0){ 
    $salida.= "
    <table class='table table-responsive table-hover table-striped r-1200' id='export_to_excel'>
        <thead class='thead-dark'>
            <tr>
                <th scope='col'> # </th>
                <th scope='col'> Nombre </th>
                <th scope='col'> Apellido </th>	
                <th scope='col'> Estado </th>
                <th scope='col'> Fase </th>
                <th scope='col'> Rut </th>
                <th scope='col'> Teléfono </th>
                <th scope='col'> Correo </th>
                <th scope='col'> Días </th>
                <th scope='col'> Acciones </th>	
            <tr>
        </thead>
        <tbody>
    ";
    $cont = 1;

        while($row = $resultado-> fetch_assoc()){
            $salida.= "
            <tr class='tr-style' >
                <td class='tab-center'>".$cont."</td>
                <td class=''>" . utf8_encode($row["nombre"]) ."</td>
                <td class=''>" . utf8_encode($row["apellido"]) . "</td>
                <td class=''>" . utf8_encode($row["tipo"]) . "</td>
                <td class=''>" . utf8_encode($row["fase"] ). "</td>
                <td class=''><pre class='tabPre'>" . utf8_encode($row["rut"]) . "</pre></td>
                <td class=''>" . utf8_encode($row["telefono"]) . "</td>
                <td class=''>" . utf8_encode($row["correo"]) . "</td>
                <td class='tab-center'>" . utf8_encode($row["cont"]) . "</td>
                <td class='tdProp propTab center-tab pb-10 tabAcc'>
                    <button class='btn-acciones bg-redflip-green' onClick='habilitarPropietario(".$row['id_persona'].")' title='Habilitar'><i class='fas fa-user-check'></i></button>
                    <button class='btn-acciones bg-redflip-red' onClick='EliminarPropietario(".$row['id_persona'].")' title='Eliminar'><i class='fas fa-user-times'></i></button>
                </td>
            </tr>";
            $cont ++;
            }
            $salida.="
        </tbody>
    </table>";
}else{

    $salida.= "
    <table class='table table-responsive table-hover table-striped r-1200' id='export_to_excel'>
        <thead class='thead-dark'>
            <tr>
                <th scope='col'> # </th>
                <th scope='col'> Nombre </th>
                <th scope='col'> Apellido </th>	
                <th scope='col'> Estado </th>
                <th scope='col'> Fase </th>
                <th scope='col'> Rut </th>
                <th scope='col'> Teléfono </th>
                <th scope='col'> Correo </th>
                <th scope='col'> Contador </th>
                <th scope='col'> Acciones </th>	
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class= 'propTabN center-tab' colspan=10 > no hay datos</td>
            </tr>
        </tbody>
    </table>
    ";
}
echo $salida;
$conn->close();
?>