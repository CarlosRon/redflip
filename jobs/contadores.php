<!-- Proceso CRON que se utiliza para actualizar los contadores diarios de los propietarios -->

<!-- Este contador se utiliza para llevar el control de cuanto tiempo lleva el propietario sin ser contactado -->

<!-- El CRON job debe ser configurado 4 horas antes de la hora que necesitas -->



<?php

include '../conexion.php';





$sql = "SELECT distinct per.nombre , per.apellido , per.telefono , per.correo , per.id_persona as idPer , dir.calle , dir.numero , dir.referencia , dir.id as idDir , com.nombre as Comuna , com.id as idCom , vent.descripcion V_A , vent.id as idVent , divi.nombre as divisa , divi.id as idDiv , propiedad.id_propiedad as idPropiedad , propiedad.fk_propietario ,propietario.fk_estado_prop, propiedad.nota , propiedad.monto , est.tipo , est.id as idEst , sub.fase , sub.id as idSub , propietario.cont , propietario.id_propietario as idPropietario , propietario.contactos , Origen.nom_origen as ori , Origen.id_origen as id_or , formulario.* , Status.status FROM Persona per INNER JOIN Propietario propietario , Estado_prop est , Sub_estado sub , Propiedad propiedad , Divisa divi, Direccion dir , Comuna com , Venta_Arriendo vent , Origen , Status , formulario WHERE per.id_persona = propietario.fk_persona AND propietario.fk_estado_prop = est.id AND est.fk_sub_estado = sub.id AND propietario.id_propietario = propiedad.fk_propietario AND propiedad.divisa = divi.id AND propiedad.fk_direccion = dir.id and dir.fk_comuna = com.id AND propiedad.fk_venta_arriendo = vent.id AND propietario.fk_origen = Origen.id_origen AND propiedad.fk_status = Status.id_status AND propiedad.fk_estado_propiedad = 1 AND propiedad.fk_formulario = formulario.id_formulario AND formulario.aprobado = 1 order by propiedad.id_propiedad asc";


$destinatario = "carlos.ron@lt-latam.com";

$asunto = "Contacto con propietario";
$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$result = $conn->query($sql);

$cont =0;

$importanciaMedia = 0;

$importanciaAlta = 0;

$cuerpo = "
                        <html> 

                           <head> 

                           <meta charset='UTF-8'>

                            <title>Prueba de correo</title> 

                            </head> 
                            <body>

                            <p>
                                ";


if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {

        $suma = $row["cont"]+1;



        $sql2 = "UPDATE Propietario SET cont =" . $suma . " WHERE id_propietario = " . $row["idPropietario"];

        



        if ($conn->query($sql2) === TRUE) {

            $cont ++;

            if($suma >= 20 && $suma <= 30 ){



                $importanciaMedia ++;



                

                            

            }else if($suma >= 31){

                $importanciaAlta ++;

            }

        }else{
            echo $sql2;
        }

        if($row["fk_estado_prop"] == 1 && ($row["cont"] + 1) >=2){
            $cuerpo.= 'El propietario '.$row["nombre"].', "N° ' . $row["idPropietario"] . '" debe ser contactado lo antes posible, lleva: "' .($row["cont"]+1).'" días';
            $cuerpo.= '--> https://indev9.com/redflip.indev9.com/pages/editarProp.php?id='.$row["idPer"].' </br>';
        }
        if($row["fk_estado_prop"] == 3 && ($row["cont"] + 1) >=2){
            $cuerpo.= 'El propietario '.$row["nombre"].', "N° ' . $row["idPropietario"] . '" Fue contactado hace '.($row["cont"]+1).' días ';
            $cuerpo.= '--> https://indev9.com/redflip.indev9.com/pages/editarProp.php?id='.$row["idPer"].' </br>';
        }
        if($row["fk_estado_prop"] == 4 && ($row["cont"] + 1) >=2){
            $cuerpo.= 'El propietario '.$row["nombre"].', "N° ' . $row["idPropietario"] . '" está en fase primera visita hace '.$row["cont"].' días ';
            $cuerpo.= '--> https://indev9.com/redflip.indev9.com/pages/editarProp.php?id='.$row["idPer"].' </br>';
        }
        if($row["fk_estado_prop"] == 2 && ($row["cont"] + 1) >=2){
            $cuerpo.= 'El propietario '.$row["nombre"].', "N° ' . $row["idPropietario"] . '" está en fase Tour Virtual hace '.$row["cont"].' días ';
            $cuerpo.= '--> https://indev9.com/redflip.indev9.com/pages/editarProp.php?id='.$row["idPer"].' </br>';
        }
        if($row["fk_estado_prop"] == 6 && ($row["cont"] + 1) >=2){
            $cuerpo.= 'El propietario '.$row["nombre"].', "N° ' . $row["idPropietario"] . '" está en fase Publicado hace '.$row["cont"].' días ';
            $cuerpo.= '--> https://indev9.com/redflip.indev9.com/pages/editarProp.php?id='.$row["idPer"].' </br>';
        }
        if($row["fk_estado_prop"] == 5 && ($row["cont"] + 1) >=2){
            $cuerpo.= 'El propietario '.$row["nombre"].', "N° ' . $row["idPropietario"] . '" está en fase Despublicado hace '.$row["cont"].' días ';
            $cuerpo.= '--> https://indev9.com/redflip.indev9.com/pages/editarProp.php?id='.$row["idPer"].' </br>';
        }
        if($row["fk_estado_prop"] == 7 && ($row["cont"] + 1) >=2){
            $cuerpo.= 'El propietario '.$row["nombre"].', "N° ' . $row["idPropietario"] . '" está en fase Finalizado hace '.$row["cont"].' días ';
            $cuerpo.= '--> https://indev9.com/redflip.indev9.com/pages/editarProp.php?id='.$row["idPer"].' </br>';
        }
        if($row["fk_estado_prop"] == 8 && ($row["cont"] + 1) >=2){
            $cuerpo.= 'El propietario '.$row["nombre"].', "N° ' . $row["idPropietario"] . '" está en fase Cancelado hace '.$row["cont"].' días ';
            $cuerpo.= '--> https://indev9.com/redflip.indev9.com/pages/editarProp.php?id='.$row["idPer"].' </br>';
        }
        

    }
    $cuerpo.="</p></body>";
    echo "Actualizacion exitosa de " . $cont . " registros </br>";



} else {

    $cuerpo = "No existen propietarios";

}

if(mail($destinatario,$asunto,utf8_decode($cuerpo), $cabeceras)){

    echo "Correo enviado </br>";

}else{

    echo "Correo NO enviado </br>";      

}



echo $cuerpo;

$conn->close();



?>