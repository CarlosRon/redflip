<!DOCTYPE html>
<html lang="es-cl">
    <head>
        <!--=== Required meta tags ===-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!--=== Title ===-->
        <title>Iniciar sesión</title>
        <!--=== Favicon Icon ===-->
        <link rel="shortcut icon" type="image/png" href="#">
        <!--=== Bootstrap CSS ===-->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <!--=== Font Awesome ===-->
        <!--=== Style CSS ===-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <?
        // include "header.php";
        $err = $_GET["err"];
        ?>

        <div id="login-cont" class="container col-12" >
            <div class="row">
                <div id="leftSideImg" class="column col-8">
                </div>
                
                <div class="column col-xs-12 col-sm-12 col-md-12 col-lg-4 form-container">
                    <form id="formulario" class="login">
                        <div class="logo-form"></div>
                        <label class="label-login" >Correo</label><br>
                        <input class="input-login" type="text" name="nombre" id="nombre" onfocus="cerrarError()"><br>

                        <label class="label-login" >Contraseña</label><br>
                        <input class="input-login" type="password" name="pass" id="pass" onfocus="cerrarError()">
                        <div id="error">

                            <?
                            if($err == 11){
                                $err = 0;?></br>
                                <div  class="alert alert-danger" role="alert" id="alert">
                                    <label>Error: Sesión expirada </label>     
                                </div>
                            <?}else{} ?>
                        </div>

                        <label class="label-login forgotPasswordLink"><a href="fPass.php">¿Has olvidado tu contraseña?</a></label><br>
                        <button class="btn-def btn-red" type="submit" value="Iniciar sesión" id="submit">Iniciar sesión</button><br>
                    </form>
                </div>
            </div>
        </div>

        <!--=== JS ===-->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- <script src="js/main-login.js"></script> -->
        <script src="js/app.js"></script>

        <script>
        $(document).ready(function(){
            var height = $(window).height();
            $('#leftSideImg').height(height);
        });

        function cerrarError(){
            document.getElementById("error").innerHTML="";
        }

        

        </script>
    </body>
</html>