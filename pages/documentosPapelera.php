<?php
    include 'sidebarMenu.php';
    include '../conexion.php';
    $sqlDocumento = "SELECT * FROM documento WHERE eliminado=TRUE";
    $result = $conn->query($sqlDocumento);
?>

    <style>
        .btn-matchBlue {
            background-color: #09aadb;
            color: #fff;
            font-size: 14px;
            font-weight: 100;
            padding: 3px 8px;
            border-radius: 5px;
            white-space: unset!important;
        }
        
        .btn-matchBlue:hover {
            color: #fff;
            font-size: 14px;
            background-color: #05789c;
        }
        
        .btn-matchBlue:focus {
            color: #fff;
            font-size: 14px;
            background-color: #05789c;
        }
        
        @media only screen and (min-width:577px) and (max-width:1024px) {
            .container-md {
                padding-left: 60px;
            }
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-12" style="padding-top:50px;">
                <h3 class="h3-prop mtb-50">Papelera de reciclaje</h3>
            </div>
        </div>
    </div>

    <div class="container container-xs-fluid container-sm-fluid container-md">
        <table class="table-responsive-xs table-responsive-sm table-responsive-md table-match">
            <thead class="thead-match">
                <tr class="tr-matchHead">
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Fecha de Eliminación</th>
                    <th scope="col">Subcarpeta</th>
                    <th scope="col">Tamaño</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $doc = $row;
                        echo '<tr class="tr-matchBody">
                            <td>'.$doc["id"].'</td>
                            <td>'.utf8_encode($doc["titulo"]).'</td>
                            <td>'.$doc["fechaElim"].'</td>';
                        $sqlCate = "SELECT nombre FROM categoria_documento WHERE id =".$doc["categoria"];
                        $resultCate = $conn->query($sqlCate);
                        if ($resultCate->num_rows > 0) {
                            while($rowCate = $resultCate->fetch_assoc()) {
                                echo '<td>'.utf8_encode($rowCate["nombre"]).'</td>';
                            }
                            } else {
                            echo '0 results';
                            } 
                        echo'<td>'.$doc["tamano"].'</td>
                            <td>
                                <a href="https://indev9.com/redflip/doc/0/'.$doc["nombre"].'" class="btn btn-matchBlue" download="'.$doc["titulo"].'"><i class="fas fa-download"></i> Descargar</a>
                                <a href="" onclick="restaurar('.$doc["id"].')" class="btn btn-matchGreen"><i class="fas fa-trash-restore"></i> Restaurar</a>
                        </tr>';
                    }
                } else {
                    echo "No hay archivos en la papelera";
                }
                $conn->close();       
            ?>
            </tbody>
        </table>
    </div>
    <script src="../js/documentosPapelera.js"></script>
    <?php
        include "footer.php";
    ?>