<?php
include "sidebarMenu.php";
include '../conexion.php';
include_once '../services/funciones.php';
$id = $_GET["id"];

$formulario;
$sql = "select * from formulario where id_formulario = $id";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    $formulario = $row;
}
$sql = "select * from Corredor,Persona where Corredor.fk_persona = Persona.id_persona AND id_corredor =". $formulario["fk_corredor"];
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    $corredor = $row;
}
$sql = "select * from Propietario,Persona where Propietario.fk_persona = Persona.id_persona AND id_propietario =". $formulario["fk_propietario"];
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    $propietario = $row;
}
$orientacion = "null";
$sql = "select * from Orientacion where fk_formulario = $id";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    $orientacion = $row;
}

$comuna = "null";
$sql = "SELECT fk_comuna FROM Direccion WHERE Direccion.id = (SELECT formulario.Direccion_id FROM formulario WHERE id_formulario = $id)";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    $comuna = $row["fk_comuna"];
}

$sqlMatch = "SELECT * from Perfil_busqueda where ";

$sqlMatch .= "Perfil_busqueda.fk_tipo_propiedad Like " . $formulario["fk_tipo_propiedad"] . " and Perfil_busqueda.fk_tipo_operacion Like " . $formulario["operacion"];

$sqlMatch .= " and dormDesde <= " . $formulario["dormitorios"] . " and dormHasta >= " . $formulario["dormitorios"]; //dormitorios

$sqlMatch .= " and bannosDesde <= " . $formulario["cant_bannos"] . " and bannosHasta >= " . $formulario["cant_bannos"]; //bannos

$sqlMatch .= " and m_utilesDesde <= " . limpiar($formulario["superficie_util"]) . " and m_utilesHasta >= " . limpiar($formulario["superficie_util"]); //mUtil

if($formulario["superficie_terraza"] != ""){
    $sqlMatch .= " and m_terrazaDesde <= " . limpiar($formulario["superficie_terraza"]) . " and m_terrazaHasta >= " . limpiar($formulario["superficie_terraza"]);//terraza
}
if($formulario["fk_tipo_prop"] == 2){
    $sqlMatch .= " and m_terrenoDesde <= " . limpiar($formulario["superficie_terreno"]) . " and m_terrenoHasta >= " . limpiar($formulario["superficie_terreno"]);
}

$sqlMatch .= " and ((precio_min <= " . limpiar($formulario["monto1"]) . " and precio_max >= " . limpiar($formulario["monto1"]) . ") or (precio_min <= " . limpiar($formulario["monto2"]) . " and precio_max >= " . limpiar($formulario["monto1"]) . "))"; //precios

// $sqlMatch .= " and bodega Like " . $formulario["bodega"]; //bodega no se solicita ya que es demaciado especifico

$sqlMatch .= " and estacionamientoDesde <= " . $formulario["estacionamiento"] . " and estacionamientoHasta >= " . $formulario["estacionamiento"]; //estacionamiento

// if($formulario["piso_depto"] != ""){
// $sqlMatch .= " and piso_depto Like " . $formulario["piso_depto"]; // piso_depto no se solicita ya que es demaciado especifico
// }

//suma para saber si existe orientacion en ese formulario
$sumaOrientacion = $orientacion["norte"] + $orientacion["sur"] + $orientacion["oriente"] + $orientacion["poniente"] + $orientacion["norOriente"] + $orientacion["norPoniente"] + $orientacion["surOriente"] +  $orientacion["surPoniente"];
if($sumaOrientacion != 0){
    $cont = 0; // para verificar si existe una orientacion antes para concatenar el OR
    $sqlMatch .= " and (";

    if($orientacion["norte"] == 1){
        $sqlMatch .= "orientacion LIKE '%N-%' ";
        $cont ++;
    }
    if($orientacion["sur"] == 1){
        if($cont > 0){
            $sqlMatch .= " OR ";
        }
        $sqlMatch .= "orientacion LIKE '%S-%' ";
        $cont ++;
    }
    if($orientacion["oriente"] == 1){
        if($cont > 0){
            $sqlMatch .= " OR ";
        }
        $sqlMatch .= "orientacion LIKE '%O-%' ";
        $cont ++;
    }
    if($orientacion["poniente"] == 1){
        if($cont > 0){
            $sqlMatch .= " OR ";
        }
        $sqlMatch .= "orientacion LIKE '%P-%' ";
        $cont ++;
    }
    if($orientacion["norOriente"] == 1){
        if($cont > 0){
            $sqlMatch .= " OR ";
        }
        $sqlMatch .= "orientacion LIKE '%NO%' ";
        $cont ++;
    }
    if($orientacion["norPoniente"] == 1){
        if($cont > 0){
            $sqlMatch .= " OR ";
        }
        $sqlMatch .= "orientacion LIKE '%NP%' ";
        $cont ++;
    }
    if($orientacion["surOriente"] == 1){
        if($cont > 0){
            $sqlMatch .= " OR ";
        }
        $sqlMatch .= "orientacion LIKE '%SO%' ";
        $cont ++;
    }
    if($orientacion["surPoniente"] == 1){
        if($cont > 0){
            $sqlMatch .= " OR ";
        }
        $sqlMatch .= "orientacion LIKE '%SP%' ";
    }

    $sqlMatch .= " )";
}
 //verifica que en el string comuna encuentre el id de la comuna seleccionada
if($comuna != ""){
    $sqlMatch .= " AND comuna LIKE '%$comuna%'";
}

//piscina
if($formulario["piscina"] == 1){
    $sqlMatch .= " AND piscina = 1";
}else{
    $sqlMatch .= " AND  ((piscina = 0 AND Episcina = 1) OR (piscina = 0 AND Episcina = 0))";
}

//cocina americana
if($formulario["cocina_ame"] == 1){
    $sqlMatch .= " AND cocina_americana = 1";
}else{
    $sqlMatch .= " AND  ((cocina_americana = 0 AND EcocinaAme = 1) OR (cocina_americana = 0 AND EcocinaAme = 0))";
}

//cocina isla
if($formulario["cocina_isla"] == 1){
    $sqlMatch .= " AND cocina_isla = 1";
}else{
    $sqlMatch .= " AND  ((cocina_isla = 0 AND EcocinaIsla = 1) OR (cocina_isla = 0 AND EcocinaIsla = 0))";
}

//comedor diario
if($formulario["comedor_diario"] == 1){
    $sqlMatch .= " AND comedor_diario = 1";
}else{
    $sqlMatch .= " AND  ((comedor_diario = 0 AND EcomedorDiario = 1) OR (comedor_diario = 0 AND EcomedorDiario = 0))";
}

//termo panel
if($formulario["term_panel"] == 1){
    $sqlMatch .= " AND termopanel = 1";
}else{
    $sqlMatch .= " AND  ((termopanel = 0 AND Etermopanel = 1) OR (termopanel = 0 AND Etermopanel = 0))";
}

//amoblado
if($formulario["amoblado"] == 1){
    $sqlMatch .= " AND amoblado = 1";
}else{
    $sqlMatch .= " AND  ((amoblado = 0 AND Eamoblado = 1) OR (amoblado = 0 AND Eamoblado = 0))";
}

//mascotas
if($formulario["mascotas"] == 1){
    $sqlMatch .= " AND mascotas = 1";
}else{
    $sqlMatch .= " AND  ((mascotas = 0 AND Emascotas = 1) OR (mascotas = 0 AND Emascotas = 0))";
}

//gim
if($formulario["gim"] == 1){
    $sqlMatch .= " AND gimnasio = 1";
}else{
    $sqlMatch .= " AND  ((gimnasio = 0 AND Egim = 1) OR (gimnasio = 0 AND Egim = 0))";
}

//dormitorio servicio
if($formulario["dorm_serv"] == 1){
    $sqlMatch .= " AND dorm_servicio = 1";
}else{
    $sqlMatch .= " AND  ((dorm_servicio = 0 AND Edorm_servicio = 1) OR (dorm_servicio = 0 AND Edorm_servicio = 0))";
}

//quincho
if($formulario["quincho"] == 1){
    $sqlMatch .= " AND quincho = 1";
}else{
    $sqlMatch .= " AND  ((quincho = 0 AND Equincho = 1) OR (quincho = 0 AND Equincho = 0))";
}

// echo $sqlMatch;

$result = $conn->query($sqlMatch);
$idsForm = array();
if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    // echo "<p style='color:green'>HAY MATCH, ID: ". $row['id']."</p>";
        $idForm = $row["id"];
        array_push($idsForm, $idForm);
  }
} else {
  
echo "no hay match";
die;

}
echo "<script>console.log('cantidad de match: ".json_encode($idsForm)."')</script>"
?>
<div id="master" class="container">
    <!-- Datos interesado -->
    <div class="row" style="background:#e0e0e0;">
        <div class="col-12">
            <h4 class="match-title">Formulario</h4>
        </div>
        <div class="col">
            <label for="nombre">Formulario id: <?=$formulario["id_formulario"]?></label><br>
            <label for="apellido">Nombre Formulario: <?=$formulario["nombre_form"]?></label><br>
            <label for="corredor">Corredor: <?=$corredor["nombre"] . " " . $corredor["apellido"]?></label>
        </div>
        <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
            <label for="correo" style="width: 100%;">Correo: <input type="text" id="correo" value=""class= "match-input" disabled></label><br>
            <label for="telefono" style="width: 100%;">Telefono: <input type="text" id="telefono" value=""class= "match-input" disabled></label>
        </div> -->
        
    </div>
    <!-- Match -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <h4 class="match-title">Match Interesados</h4>
                <div class="container-match">
                
                    <?php
                        $perfil_busqueda;
                        $comunas = array();
                        foreach ($idsForm as $value) { 

                            $Perfil = 'SELECT * from Perfil_busqueda, Interesado, Persona where
                             fk_interesado = id_interesado and fk_persona = id_persona and  id ='. $value;
                            $result = $conn->query($Perfil);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    // Datos para la card
                                    $perfil_busqueda = $row;
                                   $div_comunas = explode("-",$row["comuna"]);
                                   foreach ($div_comunas as $id_comuna){
                                        $sqlComuna = "SELECT * from Comuna where id = " . $id_comuna;
                                        // echo "<script>console.log('$sqlComuna')</script>";
                                        $result2 = $conn->query($sqlComuna);
                                        if ($result2->num_rows > 0) {
                                            // output data of each row
                                            while($row2 = $result2->fetch_assoc()) {
                                                array_push($comunas, utf8_encode($row2["nombre"]));
                                                // echo "<script>console.log('".json_encode(utf8_encode($row2["nombre"]))."')</script>";
                                            }
                                        }
                                   }
                                }
                            }else{
                                echo $Perfil;
                            }
                            ?>
                            
                            <br>
                            <div class="container-fluid">
                                <div class="row">
                                    <!-- Dir -->
                                    <div class="col-5">
                                        <label for="direccion"><b>Nombre:</b> </label>
                                    </div>
                                    <div class="col-7">
                                        <?=$perfil_busqueda["nombre"] . " " . $perfil_busqueda["apellido"]?>
                                    </div>
                                    <!-- M.Util -->
                                    <div class="col-5">
                                        <label for="metrosU"><b>Dormitorios</b></label>
                                    </div>
                                    <div class="col-7">
                                         <?=$perfil_busqueda["dormDesde"] . " - " . $perfil_busqueda["dormHasta"]?>
                                    </div>

                                    <!-- M.Totales -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Baños:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$perfil_busqueda["bannosDesde"] . " - " . $perfil_busqueda["bannosHasta"]?>
                                    </div>
                                         <!-- M.Util -->
                                    <div class="col-5">
                                        <label for="metrosU"><b>Metros útiles:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?= $perfil_busqueda["m_utilesDesde"] . " - " . $perfil_busqueda["m_utilesHasta"]?>
                                    </div>

                                    <!-- M. Terraza -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Precio:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$perfil_busqueda["precio_min"] . " - " . $perfil_busqueda["precio_max"]?>
                                    </div>
                                    <!-- Dormitorios -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Comunas:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <ul>
                                            <?php
                                            $contComunas = 0;
                                            foreach($comunas as $comuna){
                                                if($contComunas === 3){
                                                    
                                                    echo '<div id="demo" class="collapse">';
                                                    echo "<li>$comuna</li>";
                                                }else if($contComunas > 3){
                                                    echo '  
                                                                <li>'.$comuna.'</li>
                                                            ';
                                                }else{
                                                    echo '<li>'.$comuna.'</li>';
                                                    
                                                }
                                                $contComunas ++;
                                            }
                                            
                                            ?>
                                            </div>
                                            <button id="verMas" type="button" class="" onclick="verMas()" data-toggle="collapse" data-target="#demo" style="
                                            background: none;
                                            color: inherit;
                                            border: none;
                                            padding: 0;
                                            font: inherit;
                                            cursor: pointer;
                                            outline: inherit;
                                            color: #ee171e;
                                            text-decoration-line: underline;"
                                            >Ver más</button>
                                        </ul>
                                    </div>
                                    
                                    <?php }?>
                                    <!-- <div style='text-align:right; margin:0 20px; width:100%;'>
                                        <label for=''>Marcar para enviar</label>
                                        <input type='checkbox' name='' id='' onclick=''>
                                    </div> -->
                                    <!-- <a href="" style="float: right; margin: 0 50px; text-align: right; width: 100%;">Ver más</a> -->
                                </div>
                            </div>
                        </div>
                </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>
<script src="../js/dist/sweetalert2.all.min.js"></script>
 <script src="../js/match.js"></script>
 <script>
    
 </script>