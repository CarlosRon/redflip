<?php
    include 'sidebarMenu.php';
    include '../conexion.php';
    $sqlDocumento = "SELECT * FROM documento WHERE eliminado=FALSE";
    $result = $conn->query($sqlDocumento);
?>

<style>
    .file-upload {
        position: relative;
        overflow: hidden;
        border-radius: 5px !important;
        background: #E8505B;
        font-size: 14px;
        border:none !important;
        box-shadow: none !important;
        color: #fff !important;
        text-shadow:none;
        padding: 5px 30px !important;
        font-family: sans-serif;
        display:inline;
    }
    .file-upload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    #fileuploadurl{
        display: inline-block;
        border:none;
        vertical-align: middle;
        background: none;
        box-shadow: none;
        font-size: 12px;
        width:220px;
        padding-left:10px;
        text-overflow:ellipsis;
    }
</style>

<section class="pt-4 pb-4">
    <div class="container"> 
        <div class="row">
            <div class="col-12">
                <h3 class="h3-prop mtb-50">Ver Archivos</h3>
            </div>
        </div>

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-matchGreen bg-dark float-right mb-2" data-toggle="modal" data-target="#exampleModalCenter">
        Subir documentos
        </button>

        <!-- Modal SUBIR DOCUMENTO-->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark">
                        <h5 class="modal-title pb-1 c-redflip-white text-uppercase" id="exampleModalLongTitle">Subir documentos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="c-redflip-white">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" enctype="multipart/form-data" id="ingresoDoc" class="formDoc">

                            <div class="documentBox">      
                                <input type="text" name="tituloDoc" id="tituloDoc" required>
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label for="tituloDoc">Título</label>
                            </div>

                            <div class="documentBox">      
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label for="categoriaDoc" class="selectLabel">Categoría</label>

                                <select name="categoriaDoc" placeholder="" id="categoriaDoc"  style="width:48%;" require="" form="ingresoDoc" class="ddlDoc">
                                    <option value="" disabled selected>Seleccionar Categoría</option>
                                    <?php
                                        $sqlCategoriaDoc = "SELECT * from categoria_documento WHERE id NOT IN (0)";
                                        $result2 = $conn->query($sqlCategoriaDoc);
                                        if ($result2->num_rows > 0) {
                                            while($row2 = $result2->fetch_assoc()) {
                                            echo '<option value="'.$row2["id"].'">'.utf8_encode($row2["nombre"]).'</option>';
                                            }
                                        }
                                    ?>
                                </select>

                                <!-- <a href="#" class="btn btn-matchRed" onclick="crearCategoria()">Crear Categoría     Nueva</a> -->

                                <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-matchRed"  style="width:48%;" data-toggle="modal" data-target="#exampleModal2">
                                    Crear categoría
                                    </button>
                                    
                            </div>

                            <div class="documentBox file-upload btn-matchRed">
                                <label for="archivoDoc" class="selectLabel">Archivo:</label>
                                <span>Subir archivo</span>
                                <input type="file" name="archivoDoc" id="archivoDoc" class="upload" />
                            </div>
                            <input type="text" id="fileuploadurl" readonly>

                            <div class="float-right mt-4">
                                <button type="submit" class="btn btn-matchGreen">Subir archivo</button>
                                <button type="button" class="btn btn-matchGreen bg-secondary" data-dismiss="modal">Cerrar</button>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal CREAR CATEGORIA-->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title pb-1 c-redflip-white text-uppercase" id="exampleModalLabel2">Nueva categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="ingresoCat">
                    <div class="documentBox">      
                        <input type="text" name="nombreCat" id="nombreCat" required>
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        <label for="nombreCat">Nombre de la categoría</label>
                    </div>
                    <button type="submit" class="btn btn-matchGreen">Crear</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
            </div>
        </div>
    </div>

    <div class="container container-md-fluid container-sm-fluid container-md">
        <table class="table-responsive-lg table-match">
            <thead class="thead-match">
                <tr class="tr-matchHead">
                    <th scope="col">#</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Tamaño</th>
                    <th scope="col">Subcarpeta</th>
                    <th scope="col" class="pl-md-4 pl-lg-4">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $doc = $row;
                            $fecha = new DateTime($doc["fecha"]);
                            $fecha = date_format($fecha,'d-m-Y');
                            echo '<tr class="tr-matchBody">
                                    <td class="minW-tableMatch1">'.$doc["id"].'</td>
                                    <td class="minW-tableMatch2">'.$fecha.'</td>
                                    <td class="minW-tableMatch3">'.utf8_encode($doc["titulo"]).'</td>
                                    <td class="minW-tableMatch4">'.$doc["extension"].'</td>
                                    <td class="minW-tableMatch5">'.round(($doc["tamano"]/1024)).' KB</td>';

                            $sqlCate = "SELECT nombre FROM categoria_documento WHERE id =".$doc["categoria"];
                            $resultCate = $conn->query($sqlCate);
                            if ($resultCate->num_rows > 0) {
                                while($rowCate = $resultCate->fetch_assoc()) {
                                  echo '<td class="minW-tableMatch6">'.utf8_encode($rowCate["nombre"]).'</td>';
                                }
                              } else {
                                echo 'No hay archivos';
                              }
                              
                            echo'<td class="d-flex justify-content-around minW-tableMatch7"> 
                                    <a href="https://indev9.com/redflip/doc/'.$doc["categoria"].'/'.$doc["nombre"].'" class="btn btn-matchBlue" download="'.$doc["titulo"].".".$doc["extension"].'"><i class="fas fa-download"></i> Descargar</a>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-matchGreen" data-toggle="modal" data-target="#exampleModalCenter3">
                                        <i class="fas fa-edit"></i> Modificar MODAL
                                    </button>
                        
                                    <a href="documentosModForm.php?id='.$doc["id"].'" class="btn btn-matchGreen"><i class="fas fa-edit"></i> Modificar</a>
                                    <a href="" onclick="eliminarD('.$doc["id"].')" class="btn btn-matchRed"><i class="fas fa-trash-alt"></i> Eliminar</a>
                                </td>
                            </tr>';
                        }
                    } else {    
                        echo "0 results";
                    }
                    $conn->close();       
                ?>
            </tbody>
        </table>

        <!-- Modal MODIFICAR DOCUMENTO -->
        
        <div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark">
                        <h5 class="modal-title pb-1 c-redflip-white text-uppercase" id="exampleModalLongTitle">Modificar documentos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="c-redflip-white">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" enctype="multipart/form-data" id="modificaDoc" class="formDoc">

                            <div class="documentBox">      
                                <input type="text" name="tituloDoc" id="tituloDoc" value="<?=$documento["titulo"]?>" required>
                                <input type="hidden" value="<?=$id?>" name="id" id="id">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label for="tituloDoc">Título</label>
                            </div>

                            <div class="documentBox">      
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label for="categoriaDoc" class="selectLabel">Categoría</label>

                                
                                <select name="categoriaDoc" placeholder="" id="categoriaDoc" class="ddlDoc" style="width:100%;" form="modificaDoc" required>
                                    <option value="" disabled selected>Seleccionar Categoría</option>
                                    <?php
                                        $sqlCategoriaDoc = "SELECT * from categoria_documento WHERE id NOT IN (0)";
                                        $result = $conn->query($sqlCategoriaDoc);
                                        if ($result->num_rows > 0) {
                                            while($row = $result->fetch_assoc()) {
                                                if($documento["categoria"]==$row["id"]){
                                                    echo '<option selected value="'.$row["id"].'">'.utf8_encode($row["nombre"]).'</option>';
                                                }else{
                                                    echo '<option value="'.$row["id"].'">'.utf8_encode($row["nombre"]).'</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>

                            <div class="documentBox">
                                <div class="float-right mt-4">
                                    <button type="submit" class="btn btn-matchGreen">Modificar archivo</button>
                                    <button type="button" class="btn btn-matchGreen bg-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</section>
    <script src="../js/documentosMain.js"></script>
    <!-- ESTO LE PONE EL NOMBRE A LA SELECCIÓN DEL ARCHIVO -->
    <script>
        document.getElementById("archivoDoc").onchange = function () {
            document.getElementById("fileuploadurl").value = this.value;
        };
    </script>
    <!-- EL SCRIPT DE LA CAT. NUEVA UWU -->
    <script>
        function crearCategoria(){
            window.open("documentosCategoria.php", "Categoria Nueva", "width=500, height=600");
        }
    </script>
    <!-- NOSEQUESESTO -->
    <script src="../js/docs.js"></script>
    <!-- ESTE ESTABA EN DOCUMENTOS CATEGORÍA -->
    <script src="../js/docCat.js"></script>
    <!-- ESTE ESTABA EN DOCUMENTOS MOD FORM -->
    <script src="../js/docsMod.js"></script>

    <?php
        include "footer.php";
    ?>