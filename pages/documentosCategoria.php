<?php
    include 'sidebarMenu.php';
    include '../conexion.php';
?>

<body>
    <h1>Crear Nueva Categoría de Archivos</h1>
    <div class="container">
        <form method="post" action="" id="ingresoCat">
            <label for="">Nombre de la Categoría:</label>
            <input type="text" name="nombreCat" placeholder="" id="nombreCat" require="">
            <br>
            <br>
            <button type="submit">Crear Categoría</button>
        </form>
    </div>
    <script src="../js/docCat.js"></script>
    <?php 
        include 'footer.php';
    ?>
</body>