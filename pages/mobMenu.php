<?php
    include 'valid_session.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!--=== Required meta tags ===-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
     <!--=== Title ===-->
     <title></title>
     <!--=== Favicon Icon ===-->
     <link rel="icon" href="../favicon.ico" type="image/x-icon" />
     <!--=== Bootstrap CSS ===-->
     <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
     <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
     <!--=== Font Awesome ===-->
     <link rel="stylesheet" type="text/css" href="../css/all.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     
     <!--=== Style CSS ===-->
     <link rel="stylesheet" type="text/css" href="../css/menu.css">
     <link rel="stylesheet" type="text/css" href="../css/main.css">
     <!-- <link href="http://cdn.grapecity.com/spreadjs/hosted/css/gc.spread.sheets.excel2013white.10.1.0.css" rel="stylesheet" type="text/css" />   -->
    <link href="../css/toastr.css" rel="stylesheet"/>
</head>
<body>
    <div id="headerMobile">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand nav-redflip-brand mr-auto" href="index.php">
                <img src="../img/Logo-Redflip-Sin-Borde.png" class="logoNav" alt="Logo-Redflip-Sin-Borde" style="">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul>
                    <a href="index.php">
                        <li>Índice</li>
                    </a>
                    <?php if($_SESSION['rol'] == 1){?>
                    <a href="AgregarPropietario.php">
                        <li>Agregar Propietario</li>
                    </a>
                    <?php } 
                    if($_SESSION['rol'] == 1){?>
                    <a href="PropietariosAct.php">
                        <li>Propietarios Activos</li>
                    </a>
                    <?php } if($_SESSION['rol'] == 1){?> 
                    <a href="PropietariosDes.php">
                        <li>Propietarios Deshabilitados</li>
                    </a>
                    <?php } if($_SESSION['rol'] == 1){?> 
                    <a href="AgregarPropiedades.php">
                        <li>Agregar Propietario - Propiedad</li>
                    </a>
                    <?php } if($_SESSION['rol'] == 1){?> 
                    <a href="propVSpropiedad.php">
                        <li>Propiedades Activas</li>
                    </a>
                    <?php } if($_SESSION['rol'] == 1){?> 
                    <a href="desPropVSpropiedad.php">
                        <li>Propiedades Deshabilitadas</li>
                    </a>
                    <?php }?> 
                    <a href="../controlador/CrearFormularioVacio.php">
                        <li>Nuevo Formulario</li>
                    </a>

                    <hr>
                    <label for="" class="nomb"> Super Admin</label>
                    
                    <a href="../controlador/CrearFormularioVacio.php">
                        <li>Notificaciones</li>
                    </a>
                    <a href="../controlador/CrearFormularioVacio.php">
                        <li class="logOut">Cerrar sesión</li>
                    </a>
                </ul>
            </div>
        </nav>
    </div>
    
      <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  
      <script src="../js/bootstrap.min.js"></script>
  
      <script src="../js/all.min.js"></script>
   
</body>
</html> -->