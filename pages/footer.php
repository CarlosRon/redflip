
    <!--=== JS ===-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script src="../js/all.min.js"></script>
    <script src="../js/menu.js"></script>
    <script src="../js/notificacion.js"></script>
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script src="../js/push.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
    <script>
        $('.nav-toggle').click(function(e) {
            e.preventDefault();
        $("html").toggleClass("openNav");
        $(".nav-toggle").toggleClass("active");

        });
    </script>

    <script>
    function UF(){
        window.open('http://www.sii.cl/valores_y_fechas/uf/uf2020.htm', '_blank');
    }
    function dolar(){
        window.open('http://www.sii.cl/valores_y_fechas/dolar/dolar2020.htm', '_blank');
    }
    </script>
	<script>
        /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) {
            dropdown[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var dropdownContent = this.nextElementSibling;
                    if (dropdownContent.style.display === "block") {
                        dropdownContent.style.display = "none";
                    } else {
                        dropdownContent.style.display = "block";
                    }
            });
        }
    </script>
    
	<script>
        var ventana;
		fetch('https://mindicador.cl/api',{
        method: 'GET'
		})
		.then(res=>res.json())
		.then(data=> {
            var info= document.getElementById("uf");
            var infoM= document.getElementById("ufM");
            info.innerHTML += +data["uf"]["valor"];
            infoM.innerHTML += +data["uf"]["valor"];

            var info= document.getElementById("dolar");
            var infoM= document.getElementById("dolarM");
            info.innerHTML += +data["dolar"]["valor"];
            infoM.innerHTML += +data["dolar"]["valor"];
        })
        var inicio;
        function sesion (){
            Swal.fire({
            icon: 'error',
            title: 'Sesión expirada',
            text: 'Debes iniciar sesión nuevamente'
            })
            .then((result) => {
                if(result.value){
                    var ventana = window.open("https://indev9.com/redflip/pages/inicioPopUp.php", "ventana", "width=200,height=100");
                }
                
            });
        }
        
        function detectarInicio (usuario, password){
            var datos = new FormData();
            var nombre = usuario;
            var pass = password;
            datos.append("nombre", nombre);
            datos.append("pass", pass);

                fetch('../controlador/comprobar.php', {

                    method: 'POST',

                    body: datos

                }).then(res => res.json())

                .then(data => {
                    console.log(data);
                    if(data.resp == "2"){
                        // document.cookie = "conectado=";
                        // document.cookie = "conectado=true";
                        setCookie("conectado", "true");
                        Swal.close();
                        
                    }
                })
        }
        function cookie (){
            fetch('valid_session.php', {
                method: 'POST'
            }).then((res) => res.json())
            .then(data =>{
                if(data.sesion == "false"){
                    sesion();
                }
            })
        }
        setInterval('cookie()'
            , 60000);
        
            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                var expires = "expires="+ d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }
            function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
                }
            }
            return "";
            }
    </script>


<!-- <script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyBCNTXqVBEmpTbiMz-SB1oLHRZerc4_w0c",
    authDomain: "redflip-ebe80.firebaseapp.com",
    databaseURL: "https://redflip-ebe80.firebaseio.com",
    projectId: "redflip-ebe80",
    storageBucket: "redflip-ebe80.appspot.com",
    messagingSenderId: "787756096099",
    appId: "1:787756096099:web:049bed2fd4239e06d4b656"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  const messaging = firebase.messaging();
  // Add the public key generated from the console here.
    messaging.usePublicVapidKey("BBkTaDkiJjXjubLkpU6oSn9ypejYDRGTiEGyviZYeKq-gGRQwbaTxk20urOoExxKrZWb-S8ga06uy1ttd6qvTK8");
  // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then((currentToken) => {
    if (currentToken) {
        sendTokenToServer(currentToken);
        updateUIForPushEnabled(currentToken);
    } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        updateUIForPushPermissionRequired();
        setTokenSentToServer(false);
    }
    }).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    showToken('Error retrieving Instance ID token. ', err);
    setTokenSentToServer(false);
    });

    // Callback fired if Instance ID token is updated.
    messaging.onTokenRefresh(() => {
    messaging.getToken().then((refreshedToken) => {
        console.log('Token refreshed.');
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        setTokenSentToServer(false);
        // Send Instance ID token to app server.
        sendTokenToServer(refreshedToken);
        // ...
    }).catch((err) => {
        console.log('Unable to retrieve refreshed token ', err);
        showToken('Unable to retrieve refreshed token ', err);
    });
    });
</script> -->
    
    <!-- Entire bundle -->
<script src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.6/lib/draggable.bundle.js"></script>
<!-- legacy bundle for older browsers (IE11) -->
<script src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.6/lib/draggable.bundle.legacy.js"></script>
<!-- Draggable only -->
<script src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.6/lib/draggable.js"></script>
<!-- Sortable only -->
<script src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.6/lib/sortable.js"></script>
<!-- Droppable only -->
<script src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.6/lib/droppable.js"></script>
<!-- Swappable only -->
<script src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.6/lib/swappable.js"></script>
<!-- Plugins only -->
<script src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.6/lib/plugins.js"></script>
<script>
//   $( function() {
//     $( "#sortable" ).sortable();
//     $( "#sortable" ).disableSelection();
//   } );
  </script>
   