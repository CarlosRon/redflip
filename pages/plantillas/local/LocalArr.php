<?php
include '../conexion.php';
$idForm = $_GET["id"];
$idDirec = $_GET["idDirec"];
$idPer = $_GET["idPer"];
$idProp = $_GET["idProp"];
$idPropiedad = $_GET["idPropiedad"];

?>

    <section>
        <div class="container">
            <form action="" id="formC">

                <input type="hidden" id="idPer" name="idPer" value="<?echo $idPer?>">
                <input type="hidden" id="idProp" name="idProp" value="<?echo $idProp?>">
                <input type="hidden" id="idForm" name="idForm" value="<?echo $idForm?>">
                <input type="hidden" id="idPropiedad" name="idPropiedad" value="<?echo $idPropiedad?>">
                <input type="hidden" id="idDirec" name="idDirec" value="<?echo $idDirec?>">
                <input type="hidden" id="operacion" name="operacion" value="2">
                <input type="hidden" id="tipoProp" name="tipoProp" value="3">

                <!-- progressbar -->
                <ul id="progressBar">
                    <li class="active">Corredor</li>
                    <li>Propietario</li>
                    <li>Propiedad</li>
                </ul>

                <div class="container">
                    <div class="row">
                        <div class="column col-12">

                        </div>
                    </div>
                </div>
                <!-- fieldset 1 -->
                <fieldset id="1">

                    <h2 class="fs-title">Información Corredor</h2>
                    <h3 class="fs-subtitle">Paso 1</h3>


                    <div class="select-container">
                        <label for="select_area" class="label_titulo">Área Redflip</label>
                        <select name="select_area" id="select_area" class="dropdown">
                        <option value="null" disabled selected>Área Redflip*</option>
                        <option value="Redflip Habitacional">Redflip Habitacional</option>
                        <option value="Redflip Comercial">Redflip Comercial</option>
                        <option value="Redflip Luxury">Redflip Luxury</option>
                    </select>
                    </div>

                    <div class="select-container">
                        <label for="corredor" class="label_titulo">Corredor</label>
                        <select name="corredor" id="corredor" onchange="selectMail()">
                        <option value="null" disabled selected>Nombre Corredor*</option>
                        <?
                        $sqlCorredor="Select Corredor.id_corredor, Persona.nombre, Persona.apellido, Persona.correo from Corredor, Persona where Corredor.fk_persona = Persona.id_persona AND eliminado != 1";
                        $result = $conn->query($sqlCorredor);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo "<option value=".$row["id_corredor"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                            }
                        }
                        ?>
                    </select>
                    </div>

                    <input type="text" id="mail" value="" disabled>
                    <div class="select-container">
                        <label for="operario" class="label_titulo">Operario Cámara</label>
                        <select name="operario" id="operario">
                        <option value="null" disabled selected>Operario cámara*</option>
                        <?php
                        $sqlOperario = "Select Operario.id_operario, Persona.nombre, Persona.apellido from Operario, Persona where Operario.fk_persona = Persona.id_persona order by id_operario";
                        $result = $conn->query($sqlOperario);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo "<option value=".$row["id_operario"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                            }
                        }
                        ?>
                    </select>
                    </div>


                    <input type="submit" name="save" class=" action-button" value="Guardar" />
                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion1" class="paginacion paginacionIF" onchange="paginacion(1)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1" disabled>Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y otros</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Local Otro</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Otros Propiedad</option>
                        <option value="10">Visita y entrega</option>
                    </select>
                    </div>

                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 2 -->
                <fieldset id="2">
                    <h2 class="fs-title">Información Propietario</h2>
                    <h3 class="fs-subtitle">Paso 2</h3>

                    <label for="nombre" class="label_titulo">Nombre</label>
                    <input class="fs-nom" type="text" name="nombre" placeholder="Nombre*" id="nombre" onfocusout="validarNombre()" />

                    <label for="apellido" class="label_titulo">Apellido</label>
                    <input class="fs-ape" type="text" name="apellido" placeholder="Apellido*" id="apellido" onfocusout="validarApellido()" />

                    <label for="rut" class="label_titulo">Rut</label>
                    <input class="fs-rut" type="text" name="rut" placeholder="Rut" id="rut" onfocusout="validarRut2()" />

                    <div class="select-container">
                        <label for="origen" class="label_titulo">Origen</label>
                        <select name="origen" id="origen" class="fs-ori" onchange="cambioOrigen()">
                        <option value="" disabled selected>Origen*</option>
                        <option value="17" >Captado por Redflip</option>
                        <option value="7" >Referido Corredor</option>
                        <option value="6" >Referido Cliente</option>
                        <option value="5" >Referido Conserje</option>
                        <option value="18">Partner Redflip</option>                         
                    </select>
                    </div>

                    <div class="select-container">
                        <label for="captadaR" class="label_titulo">Seleccione:</label>
                        <select name="captadaR" id="captadaR" class="fs-ori">
                        <option value="" disabled selected>Seleccione opción*</option>
                        <option value="9" >Base de Datos</option>
                        <option value="19" >Google</option>
                        <option value="1" >Instagram</option>
                        <option value="2" >Facebook</option> 
                        <option value="23" >Desconocido</option>                  
                    </select>
                    </div>
                    <div class="select-container">
                        <label for="partnerR" class="label_titulo">Seleccione:</label>
                        <select name="partnerR" id="partnerR" class="fs-ori">
                        <option value="" disabled selected>Seleccione opción*</option>
                        <option value="11" >Doorlis</option>
                        <option value="20" >De Carlo</option>
                        <option value="12" >OpenCasa</option>
                        <option value="21" >UHomie</option>
                        <option value="13" >GI0</option>
                        <option value="2" >Stoffel & Valdivieso</option>
                        <option value="15" >Otro</option> 
                        <option value="23" >Desconocido</option>                     
                </select>
                    </div>
                    <div class="form-group">
                        <div class="selectDivisa-container">
                            <label for="telefono" class="label_titulo">Teléfono</label><br>
                            <select name="sub_fono" id="sub_fono" class="fs-suFono">
                        <option value="" disabled selected>Prefijo*</option>
                        <option value="+569">+569</option>
                        <option value="+562">+562</option>
                        <option value="+568">+568</option>
                        <option value="+56">+56</option>
                    </select>
                        </div>
                        <input class="fs-fono" type="tel" name="telefono" id="telefono" placeholder="Teléfono*" onfocusout="validarTelefono()" />
                    </div>

                    <label for="correo" class="label_titulo">Correo</label>
                    <input type="mail" name="correo" id="correo" placeholder="Correo*" onfocusout="validarCorreo()" />

                    <label class="txtProp">Fecha</label>
                    <input type="date" name="fecha" id="fecha" placeholder="Fecha" />

                    <hr>
                    <h3 class="fs-subtitle">Contactos Adicionales</h3>

                    <div class="divC">
                        <ol>
                            <div id="contactos">

                            </div>
                        </ol>


                        <a class="addC" id="addC" href="#" onclick="agregarContacto(); return false;"> Agregar contacto <i class="fas fa-plus-circle"></i></a>
                        <a class="delC" id="delC" href="#" onclick="eliminarContacto(); return false;"> Eliminar contacto <i class="fas fa-minus-circle"></i></a>
                    </div>


                    <input type="submit" name="save" class=" action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion2" class="paginacion" onchange="paginacion(2)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2" disabled>Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y otros</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Local Otro</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Otros Propiedad</option>
                        <option value="10">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (1/11)-->
                <fieldset id="3">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (1/8)</h3>

                    <div id="valor1" style="">
                        <label for="monto1" class="label_titulo">Monto</label><br>
                        <input class="fs-valor fs-tNumb" type="text" name="monto1" id="monto1" placeholder="Monto" style="width:75%!important" onfocusout="validarPrecio('monto1')" />
                        <select name="divisaMonto1" id="divisaMonto1" class="fs-divisa">
                                <option value="" disabled selected>Divisa</option>
                                <option value="1" >UF</option>
                                <option value="2" >CLP</option>
                            </select>
                    </div>

                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">Amoblado</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                    <input type="checkbox" id="amoblado" name="amoblado" onchange="amobla2()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div id="div_valor_amoblado" class="fs-tipoOpe" style="display:none">
                                <label for="valorAmob" class="label_titulo">Valor diferencia venta</label>
                                <input class="fs-valor fs-tNumb" type="text" name="valorAmob" id="valorAmob" placeholder="Valor de diferencia amoblado Venta" onfocusout="validarPrecio('valorAmob')" />
                            </div>
                        </div>
                    </div>

                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">No exclusivo</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                    <input type="checkbox" id="exclusividad" name="exclusividad" onchange="noExclusividad()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="pregExclusividad">
                        <div id="div_preg_Exclusividad" style="display:none">
                            <div id="scCantCorredor" class="">
                                <label for="cantCorredor" class="label_titulo">Cantidad de Corredores</label>
                                <select name="cantCorredor" id="cantCorredor" class="fs-tipoOpe">
                                    <option value="null" disabled selected>Cant. de corredores</option>
                                    <option value="1" >1</option>
                                    <option value="2" >2</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                    <option value="5" >5</option>
                                    <option value="6" >6</option>
                                </select>
                            </div>

                            <div id="scTiempoPublicacion" class="">
                                <label for="select_tiempoPublicacion" class="label_titulo">Tiempo de publicación</label>
                                <select name="select_tiempoPublicacion" id="select_tiempoPublicacion" class="fs-tipoOpe">
                                    <option value="null" disabled selected>Tiempo de publicación</option>
                                    <option value="1" >1 Semana</option>
                                    <option value="2" >2 Semanas</option>
                                    <option value="3" >3 Semanas</option>
                                    <option value="4" >1 Mes</option>
                                    <option value="5" >2 Meses</option>
                                    <option value="6" >3 Meses</option>
                                    <option value="7" >4 Meses</option>
                                    <option value="8" >5 Meses</option>
                                    <option value="9" >6 Meses</option>
                                    <option value="10" >+6 Meses</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <label for="infoPropVenta" class="label_titulo">¿Por qué quieres arrendar o vender?</label>
                    <textarea name="infoPropVenta" id="infoPropVenta" cols="30" rows="3" placeholder="¿Por qué quieres arrendar?" maxlength="1000"></textarea>


                    <input type="submit" name="save" class=" action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion3" class="paginacion" onchange="paginacion(3)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>
                            <option value="2">Propietario</option>
                            <option value="3" disabled>Tipo Propiedad</option>
                            <option value="4">Dirección</option>
                            <option value="5">Gasto común y otros</option>
                            <option value="6">Superficie y orientación</option>
                            <option value="7">Local Otro</option>
                            <option value="8">Estacionamiento y Bodegas</option>
                            <option value="9">Otros Propiedad</option>
                            <option value="10">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (2/11)-->
                <fieldset id="4">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (2/8)</h3>
                    <div class="form-group">
                        <label for="calle" class="label_titulo">Dirección</label><br>
                        <input class="fs-calle" type="text" name="calle" placeholder="Calle*" id="calle" />
                        <input class="fs-numb fs-tNumb" type="number" name="numero" placeholder="Número*" id="numero" />
                        <input class="fs-letra" type="text" name="letra" placeholder="Local" id="letra" />
                    </div>

                    <!-- <input class="fs-condominio" type="text" name="condominio" placeholder="Condominio" /> -->
                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td2" id="switchCond1" style="display: none">Condominio</td class="fs-td">
                            <td class="fs-td3" id="switchCond2" style="display: none">
                                <label class="switch" id="switchCond3" style="display: none">
                                            <input type="checkbox" id="condominio" name="condominio" onchange="condominioF()">
                                            <span class="slider round" id="switchCond4" style="display: none"></span>
                                        </label>
                            </td>
                            <td class="fs-td2" id="switchLot1" style="display: none">Loteo</td class="fs-td">
                            <td class="fs-td3" id="switchLot2" style="display: none">
                                <label class="switch" id="switchLot3" style="display: none">
                                            <input type="checkbox" id="loteo" name="loteo" onchange="loteoF()">
                                            <span class="slider round" id="switchLot4" style="display: none"></span>
                                        </label>
                            </td>
                            <td class="fs-td3">
                            <label for="region" class="label_titulo">Región</label><br>
                                <div class="selectCom-container">
                                    <select name="comuna" id="comuna" class="fs-comuna">
                                    <option value="null" disabled selected>Comuna*</option>
                                    <?php
                                    $sqlComuna = "SELECT * FROM Comuna 
                                    INNER JOIN Provincia ON Provincia.id_provincia = Comuna.fk_provincia 
                                    INNER JOIN Region ON Region.id_region = Provincia.fk_region 
                                    WHERE Region.id_region = 7 AND Comuna.id NOT IN (53) ORDER BY Comuna.nombre ASC";
                                    $result = $conn->query($sqlComuna);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }
                                        } 
                                    ?>
                                    <option value="53">S/D</option>
                                    </select>
                                </div>

                                <div class="selectCom-container">
                                    <select name="comuna11" id="comuna11" class="fs-comuna">
                                    <option value="null" disabled selected>Comuna*</option>
                                    <?php
                                    $sqlComuna = "SELECT * FROM Comuna 
                                    INNER JOIN Provincia ON Provincia.id_provincia = Comuna.fk_provincia 
                                    INNER JOIN Region ON Region.id_region = Provincia.fk_region 
                                    WHERE Region.id_region = 11 AND Comuna.id NOT IN (53) ORDER BY Comuna.nombre ASC";
                                    $result = $conn->query($sqlComuna);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }
                                        } 
                                    ?>
                                    <option value="53">S/D</option>
                                    </select>
                                </div>

                                <div class="selectCom-container">
                                    <select name="comuna12" id="comuna12" class="fs-comuna">
                                    <option value="null" disabled selected>Comuna*</option>
                                    <?php
                                    $sqlComuna = "SELECT * FROM Comuna 
                                    INNER JOIN Provincia ON Provincia.id_provincia = Comuna.fk_provincia 
                                    INNER JOIN Region ON Region.id_region = Provincia.fk_region 
                                    WHERE Region.id_region = 12 AND Comuna.id NOT IN (53) ORDER BY Comuna.nombre ASC";
                                    $result = $conn->query($sqlComuna);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }
                                        } 
                                    ?>
                                    <option value="53">S/D</option>
                                    </select>
                                </div>
                            </td>

                        </tr>
                    </table>

                    <label for="referencia" class="label_titulo">Referencia</label>
                    <input class="fs-referencia" type="text" name="referencia" id="referencia" placeholder="Referencia" />


                    <div class="form-group" style="display: none" id="divCondominio">
                        <input class="fs-calle" type="text" name="calleCond" placeholder="Calle*" id="calleCond" />
                        <input class="fs-numb fs-tNumb" type="number" name="numeroCond" placeholder="Número*" id="numeroCond" />
                        <input class="fs-letra" type="text" name="letraCond" placeholder="Aa/Dpto*" id="letraCond" />
                    </div>


                    <input type="submit" name="save" class=" action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion4" class="paginacion" onchange="paginacion(4)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Propiedad</option>
                            <option value="4" disabled>Dirección</option>
                            <option value="5">Gasto común y otros</option>
                            <option value="6">Superficie y orientación</option>
                            <option value="7">Local Otro</option>
                            <option value="8">Estacionamiento y Bodegas</option>
                            <option value="9">Otros Propiedad</option>
                            <option value="10">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (4/11)-->
                <fieldset id="5">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (3/8)</h3>

                    <div class="form-group">
                        <label for="construccion" class="label_titulo fs-construccion fs-tNumb">Año de Construcción</label>
                        <input class="fs-construccion fs-tNumb" type="number" name="construccion" id="construccion" placeholder="Año construcción" onfocusout="validarAno('construccion')" />
                    </div>

                    <!-- <input class="fs-gastoC fs-tNumb" type="text" name="gastoC" id="gastoC" placeholder="Valor Gasto común" onfocusout="validarPrecio('gastoC')" />
                    <textarea name="gastos_comun" id="gastos_comun" cols="30" rows="3" placeholder="¿Qué Incluyen los Gastos Comunes?" maxlength="1000"></textarea> -->

                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="next btn-matchBlue action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion5" class="paginacion" onchange="paginacion(5)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Propiedad</option>
                            <option value="4">Dirección</option>
                            <option value="5" disabled>Gasto común y otros</option>
                            <option value="6">Superficie y orientación</option>
                            <option value="7">Local Otro</option>
                            <option value="8">Estacionamiento y Bodegas</option>
                            <option value="9">Otros Propiedad</option>
                            <option value="10">Visita y entrega</option>    
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (6/11)-->
                <fieldset id="6">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (4/8)</h3>

                    <table class="fs-table">
                        <tr class="fs-tr">
                            <!-- <td class="fs-td3"><input class="fs-supT fs-tNumb" type="text" name="supT" id="supT" placeholder="Superficie total" onfocusout="validarPrecio('supT')" /></td> -->
                            <td class="fs-td3">
                                <label for="supU" class="label_titulo">Superficie Útil</label>
                                <input class="fs-supU fs-tNumb" type="text" name="supU" id="supU" placeholder="m2 util" onfocusout="validarPrecio('supU')" />
                            </td>
                        </tr>

                        <tr class="fs-tr">
                            <td class="fs-td3">
                                <label for="supT" class="label_titulo">Superficie Total</label>
                                <input class="fs-supT fs-tNumb" type="text" name="supT" id="supT" placeholder="m2 total" onfocusout="validarPrecio('supT')" />
                            </td>

                            <td class="fs-td3" id="supTerrTd">
                                <label for="supTerr" class="label_titulo">Superficie Terreno</label>
                                <input class="fs-supTerr fs-tNumb" type="text" name="supTerr" id="supTerr" placeholder="m2 terreno" onfocusout="validarPrecio('supTerr')" />
                            </td>
                        </tr>
                    </table>
                    <!-- Si se escribe en sup. Patio, debe aparecer -->
                    <div class="column col-12" id="usoGoceDiv" style="display:none">
                        <div class="fluid-container">
                            <div class="row">
                                <div class="column col-6">
                                    <label for="usoGoce">
                                        Uso y goce
                                    </label>
                                </div>
                                <div class="column col-6">
                                    <label class="switch">
                                        <input type="checkbox" id="usoGoce" name="usoGoce">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin dinamico -->
                    <!-- <input class="fs-pDpto fs-tNumb" type="number" name="CantPisosCasa" id="CantPisosCasa" placeholder="Cant. de pisos casa" style="display:none"/> -->

                    <div class="form-group">
                        <div id="scCantPisosCasa" class="select-container">
                            <select name="CantPisosCasa" id="CantPisosCasa" class="fs-ori" style="display:none">
                                <option value="" disabled selected>Cant. de pisos casa</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="column col-4">
                                <label class="txtProp">
                                    Orientación
                                </label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">N
                                    <input type="checkbox" id="norte" name="norte" onchange="">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">S
                                    <input type="checkbox" id="sur" name="sur" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">O
                                    <input type="checkbox" id="oriente" name="oriente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">P
                                    <input type="checkbox" id="poniente" name="poniente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">NO
                                    <input type="checkbox" id="nOriente" name="nOriente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">NP
                                    <input type="checkbox" id="nPoniente" name="nPoniente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">SO
                                    <input type="checkbox" id="sOriente" name="sOriente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">SP
                                    <input type="checkbox" id="sPoniente" name="sPoniente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion6" class="paginacion" onchange="paginacion(6)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Propiedad</option>
                            <option value="4">Dirección</option>
                            <option value="5">Gasto común y otros</option>
                            <option value="6" disabled>Superficie y orientación</option>
                            <option value="7">Local Otro</option>
                            <option value="8">Estacionamiento y Bodegas</option>
                            <option value="9">Otros Propiedad</option>
                            <option value="10">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (7/11)-->
                <fieldset id="7">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (5/8)</h3>

                    <div class="form-group">
                        <label for="plantasLibres" class="label_titulo">Plantas Libres</label>
                        <div id="selectPlantasLibres" class="select-container">
                            <select name="plantasLibres" id="plantasLibres" class="fs-ori">
                                <option value="" disabled selected>Plantas Libres</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>

                        <div id="selectPrivados" class="select-container">
                            <label for="privados" class="label_titulo">Privados</label>
                            <select name="privados" id="privados" class="fs-ori">
                                <option value="" disabled selected>Privados</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>

                        <div id="salaReu" class="select-container">
                            <label for="salaReu" class="label_titulo">Salas de Reuniones</label>
                            <select name="salaReu" id="salaReu">
                            <option value="" selected>Sala de Reuniones</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            </select>
                        </div>

                        <div id="selectBanosLocal" class="select-container">
                            <label for="" class="label_titulo">Baños</label>
                            <select name="bannos" id="bannos" class="fs-ori">
                                <option value="" disabled selected>Baños</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>

                        <div id="selectEstClientes" class="select-container">
                            <label for="estacionamientosClientes" class="label_titulo">Estacionamientos</label>
                            <select name="estacionamientosClientes" id="estClientes" class="fs-ori">
                                <option value="" disabled selected>Estacionamientos</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>

                    </div>
                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion7" class="paginacion" onchange="paginacion(7)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Propiedad</option>
                            <option value="4">Dirección</option>
                            <option value="5">Gasto común y otros</option>
                            <option value="6">Superficie y orientación</option>
                            <option value="7" disabled>Local Otro</option>
                            <option value="8">Estacionamiento y Bodegas</option>
                            <option value="9">Otros Propiedad</option>
                            <option value="10">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (8/11)-->
                <fieldset id="8">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (6/8)</h3>

                    <div class="select-container">
                        <label for="select_estacionamientos" class="label_titulo">Cantidad de Estacionamientos</label>
                        <select name="select_estacionamientos" id="select_estacionamientos" class="fs-nEst" onchange="estacionamientos()">
                            <option value="" disabled selected>Cantidad de estacionamientos</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>

                    <div class="form-group" id="estacionamientos">
                    </div>
                    <div class="select-container">
                        <label for="select_bodega" class="label_titulo">Cantidad de Bodegas</label>
                        <select name="select_bodega" id="select_bodega" class="fs-nEst" onchange="bodegas()">
                            <option value="" disabled selected>Cantidad de bodega(s)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>

                    <div class="form-group" id="div_bodega">

                    </div>


                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion8" class="paginacion" onchange="paginacion(8)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Propiedad</option>
                            <option value="4">Dirección</option>
                            <option value="5">Gasto común y otros</option>
                            <option value="6">Superficie y orientación</option>
                            <option value="7">Local Otro</option>
                            <option value="8" disabled>Estacionamiento y Bodegas</option>
                            <option value="9">Otros Propiedad</option>
                            <option value="10">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (10.2/11)-->
                <fieldset id="9">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (7/8) Otros propiedad</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row">

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="red_telefonia">
                                                            Red de Telefonía
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="red_telefonia" name="red_telefonia">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="red_computadores">
                                                            Red de Computadores
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="red_computadores" name="red_computadores">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="iluminacion_led">
                                                            Iluminación LED
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="iluminacion_led" name="iluminacion_led">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="red_incendio">
                                                            Red de Incendio
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="red_incendio" name="red_incendio">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="alarma">
                                                            Alarma
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="alarma" name="alarma">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="persiana_electrica">
                                                            Persiana Electrica
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="persiana_electrica" name="persiana_electrica">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="persiana_manual">
                                                            Persiana Manual
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="persiana_manual" name="persiana_manual">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="climatizacion_vrv">
                                                            Climatización VRV
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="climatizacion_vrv" name="climatizacion_vrv">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="aire">
                                                            Aire acondicionado
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="aire" name="aire">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="puerta_magnetica">
                                                            Puerta Magnetica
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="puerta_magnetica" name="puerta_magnetica">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="estClientesOf">
                                                            Estacionamiento Clientes
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="estClientesOf" name="estClientesOf">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="citofono">
                                                            Citófono
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="citofono" name="citofono">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="energia_trifasica">
                                                            Energía Trifásica
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="energia_trifasica" name="energia_trifasica">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="bicicletero">
                                                            Bicicletero
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="bicicletero" name="bicicletero">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="accesos_controlados">
                                                            Accesos Controlados
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="accesos_controlados" name="accesos_controlados">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="circuito">
                                                            CCTV
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="circuito" name="circuito">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="kitchenette">
                                                            Kitchenette
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="kitchenette" name="kitchenette">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocina">
                                                            Cocina
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="cocina" name="cocina">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion9" class="paginacion" onchange="paginacion(9)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Propiedad</option>
                            <option value="4">Dirección</option>
                            <option value="5">Gasto común y otros</option>
                            <option value="6">Superficie y orientación</option>
                            <option value="7">Local Otro</option>
                            <option value="8">Estacionamiento y Bodegas</option>
                            <option value="9" disabled>Otros Propiedad</option>
                            <option value="10">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (11/11)-->
                <fieldset id="10">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (8/8)</h3>

                    <label for="disponibilidad" class="label_titulo">Disponibilidad Visitas</label>
                    <input class="fs-contribucion" type="text" name="disponibilidad" name="disponibilidad" placeholder="Disponibilidad visitas [Ej. L-V 11:00-17:00]" />

                    <div class="select-container ">
                        <label for="info_propiedad" class="label_titulo">Disponibilidad de Entrega</label>
                        <select name="info_propiedad" id="info_propiedad" class="fs-nBan" onchange="disponibilidadF()">
                            <option value="" disabled selected>Disponibilidad de entrega</option>
                            <option value="Inmediata">Inmediata</option>
                            <option value="1 semana">1 semana</option>
                            <option value="2 semana">2 semanas</option>
                            <option value="3 semanas">3 semanas</option>
                            <option value="1 mes">1 mes</option>
                            <option value="+1 mes">+1 mes</option>
                            <option value="Otro">Otro</option>
                        </select>
                    </div>

                    <input class="fs-contribucion" type="date" style="display:none" id="fecha_entrega" name="fecha_entrega" />

                    <label for="nota" class="label_titulo">Notas</label>
                    <textarea name="nota" id="nota" cols="30" rows="4" placeholder="Notas" maxlength="1000"></textarea>

                    <label for="propDestacado" class="label_titulo">Destacado</label>
                    <textarea name="propDestacado" id="propDestacado" cols="30" rows="4" placeholder="¿Algo que quieras destacar de la propiedad?" maxlength="1000"></textarea>

                    <label for="url" class="label_titulo">URL</label>
                    <input class="fs-contribucion" type="text" id="url" name="url" placeholder="URL" />

                    <label for="tour3d" class="label_titulo">Tour Virtual</label>
                    <input class="fs-contribucion" type="text" id="tour3d" name="tour3d" placeholder="URL del Tour Virtual" max="100"/>

                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td">Mascotas</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                    <input type="checkbox" id="mascotas" name="mascotas">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td class="fs-td">Cartel</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                    <input type="checkbox" id="cartel" name="cartel">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td class="fs-td">Llave</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                    <input type="checkbox" id="llave" name="llave">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <label for="imagen">Imagen Propiedad:</label>
                        <input type="file" name="imagen" id="imagen"></div>

                    <input type="submit" name="save" class=" action-button" value="Guardar" />
                    <input type="button" name="quit" id="quit" class=" action-button" value="Finalizar" />
                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion10" class="paginacion" onchange="paginacion(10)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Propiedad</option>
                            <option value="4">Dirección</option>
                            <option value="5">Gasto común y otros</option>
                            <option value="6">Superficie y orientación</option>
                            <option value="7">Local Otro</option>
                            <option value="8">Estacionamiento y Bodegas</option>
                            <option value="9">Otros Propiedad</option>
                            <option value="10" disabled>Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="save previous btn-matchBlue action-button mr-0" value="Anterior" />
                </fieldset>

            </form>
        </div>
    </section>
    <?
    include '../pages/footer.php';
    ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>
        <script src="../js/toastr.js"></script>
        <script src="../js/plantillas/local/formularioLocalArr.js"></script>
        <script src="../js/validaciones/requerido.js"></script>


        <script>
            $( document ).ready(function() {
                cambioRegion();
            		});
        </script>
        </body>

        </html>