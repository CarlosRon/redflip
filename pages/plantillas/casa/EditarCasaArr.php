m
<?php
include '../conexion.php';
//datos del formulario creado
$idForm = $_GET["id"];
$prop = $_GET["prop"];
$op = $_GET["op"];


$sqlForm = "SELECT * FROM formulario WHERE id_formulario = $idForm";
$result = $conn->query($sqlForm);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $area_redflip = $row["area_redflip"];
        $fk_propietario = $row["fk_propietario"];
        $fk_operario = $row["fk_operario"];
        $operacion = $row["operacion"];
        $fk_tipo_propiedad = $row["fk_tipo_propiedad"];
        // $valor_uf = $row["valor_uf"];
        // $valor_clp = $row["valor_clp"];
        $Direccion_id = $row["Direccion_id"];
        $condominio = $row["condominio"];
        $exclusividad = $row["exclusividad"];
        $amoblado = $row["amoblado"];
        $anno = $row["anno"];
        $rol = $row["rol"];
        $contribuciones_trimestrales = $row["contribuciones_trimestrales"];
        $gastos_comunes = $row["gastos_comunes"];
        $nota_gastos_com = $row["nota_gastos_com"];
        $superficie_total = $row["superficie_total"];
        $superficie_util = $row["superficie_util"];
        $superficie_terraza = $row["superficie_terraza"];
        $superficie_terreno = $row["superficie_terreno"];
        // $orientacion = $row["orientacion"];
        $dormitorios = $row["dormitorios"];
        $estacionamiento = $row["estacionamiento"];
        $pisos = $row["pisos"];
        $piso_depto = $row["piso_depto"];
        $cant_deptos = $row["cant_deptos"];
        // $cantDpto = $row["cantDpto"];
        $ascensores = $row["ascensores"];
        $dptoPiso = $row["dptoPiso"];
        $bodega = $row["bodega"];
        $material_piso_comun = $row["material_piso_comun"];
        $material_piso_dorm = $row["material_piso_dorm"];
        $material_piso_banno = $row["material_piso_banno"];
        $tipo_calefaccion = utf8_encode($row["fk_tipo_calefaccion"]);
        $agua_caliente = $row["fk_agua_caliente"];
        $aire_acondicionado = $row["aire_acondicionado"];
        $ascensor_privado = $row["ascensor_privado"];
        $atico = $row["atico"];
        $cocina_ame = $row["cocina_ame"];
        $cocina_enci = $row["cocina_enci"];
        $cocina_isla = $row["cocina_isla"];
        $comedor_diario = $row["comedor_diario"];
        $cortina_hang = $row["cortina_hang"];
        $cortina_roller = $row["cortina_roller"];
        $cortina_elec = $row["cortina_elec"];
        $escritorio = $row["escritorio"];
        $horno_emp = $row["horno_emp"];
        $jacuzzi = $row["jacuzzi"];
        $logia = $row["logia"];
        $malla_proc_terr = $row["malla_proc_terr"];
        $riego_auto = $row["riego_auto"];
        $sala_estar = $row["sala_estar"];
        $alarma = $row["alarma"];
        $term_panel = $row["term_panel"];
        $lavavajillas = $row["lavavajillas"];
        $microondas = $row["microondas"];
        $refrigerador = $row["refrigerador"];
        $despensa = $row["despensa"];
        $hall = $row["hall"];
        $planchado = $row["planchado"];
        $cocina_amob = $row["cocina_amob"];
        $citofono = $row["citofono"];
        $cerco_elec = $row["cerco_elec"];
        $ascensor_edi = $row["ascensor_edi"];
        $conserje_24 = $row["conserje_24"];
        $est_visita = $row["est_visita"];
        $gim = $row["gim"];
        $juegos = $row["juegos"];
        $lavanderia = $row["lavanderia"];
        $piscina = $row["piscina"];
        $porton_elec = $row["porton_elec"];
        $quincho = $row["quincho"];
        $sala_juegos = $row["sala_juegos"];
        $sala_reuniones = $row["sala_reuniones"];
        $sala_multi_uso = $row["sala_multi_uso"];
        $sauna = $row["sauna"];
        $piscina_temp = $row["piscina_temp"];
        $gourmet_room = $row["gourmet_room"];
        $azotea = $row["azotea"];
        $cancha_tenis = $row["cancha_tenis"];
        $circ_tv = $row["circ_tv"];
        $area_verde = $row["area_verde"];
        $sala_cine = $row["sala_cine"];
        $patio_serv = $row["patio_serv"];
        $antejardin = $row["antejardin"];
        $disponibilidad_visita = utf8_encode($row["disponibilidad_visita"]);
        $disponibilidad_entrega = $row["disponibilidad_entrega"];
        $notas = utf8_encode($row["notas"]);
        $aprobado = $row["aprobado"];
        $cant_bannos = $row["cant_bannos"];
        $fk_corredor = $row["fk_corredor"];
        $mascotas = $row["mascotas"];
        $cartel = $row["cartel"];
        $material_pisos_cocina = $row["material_pisos_cocina"];
        $material_pisos_terraza = $row["material_pisos_terraza"];
        $encimeraGas = $row["encimeraGas"];
        $encimeraElect = $row["encimeraElect"];
        $cocinaIntegrada = $row["cocinaIntegrada"];
        $estractor = $row["estractor"];
        // $valor_venta = $row["valor_venta"];
        // $valor_arriendo = $row["valor_arriendo"];
        // $divisa_venta = $row["divisa_venta"];
        // $divisa_arriendo = $row["divisa_arriendo"];
        $cant_pisos = $row["cant_pisos"];
        $nombre_form = $row["nombre_form"];
        $tiempo = $row["fk_tiempo_publicacion"];
        $cantCorredor = $row["cantCorredor"];
        $adquisicion = $row["adquisicion"];
        $panelSolar = $row["panelSolar"];
        $infoPropVenta = $row["infoPropVenta"];
        $hipoteca = $row["hipoteca"];
        $propDestacado = $row["propDestacado"];
        $sup_patio = $row["sup_patio"];
        // $uso_goce = $row["uso_goce"];
        $fk_banco = $row["fk_banco"];
        $cant_pisos_casa = $row["cant_pisos_casa"];
        $monto1 = $row["monto1"];
        $monto2 = $row["monto2"];
        $divisa_monto1 = $row["divisa_monto1"];
        $divisa_monto2 = $row["divisa_monto2"];
        $llave = $row["llave"];
        $imagen = $row["imagen"];
        $url = $row["url"];
        $tour3d = $row["tour3d"];
        $fecha = $row["fecha"];
        $origen1 = $row["origen1"];
        $origen2 = $row["origen2"];
        $valorAmob = $row["valorAmob"];
        $cocinaIndep = $row["cocinaIndep"];
        // $encimeraemp = $row["encimeraemp"];
        $persiana_aluminio = $row["persiana_aluminio"];
        $bar = $row["bar"];
        $duplex = $row["duplex"];
        $triplex = $row["triplex"];
        $mariposa = $row["mariposa"];
        $penthouse = $row["penthouse"];
        $gimnasio = $row["gim"];
        $terraza_quincho = $row["terraza_quincho"];
        $sala_eventos = $row["sala_eventos"];
        $ascensor_comun = $row["ascensor_comun"];
        $bicicletero = $row["bicicletero"];
        $fk_tipo_gas = $row["fk_tipo_gas"];
        $loteo = $row["loteo"];
        $pareada = $row["pareada"];
        $persiana_madera = $row["persiana_madera"];
        $cava_vinos = $row["cava_vinos"];
        $accesos_controlados = $row["accesos_controlados"];
        $plaza = $row["plaza"];
        $cancha_futbol = $row["cancha_futbol"];
        $cancha_golf = $row["cancha_golf"];
        $sala_juegos_ext = $row["sala_juegos_ext"];
        $fecha_entrega = $row["fecha_entrega"];

    }
} else {
    echo "<script>console.log('0 Results en formulario')</script>";
}

$sql_direccion_condominio = "select * from Direccion_condominio where fk_formulario = $idForm";
$result = $conn->query($sql_direccion_condominio);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $calleCond = utf8_encode($row["calle"]);
        $numeroCond = $row["numero"];
        $letraCond = $row["letra"];
    }
}

$sqlProp = "SELECT * FROM Propietario WHERE id_propietario = $fk_propietario";
$result = $conn->query($sqlProp);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $fk_persona = $row["fk_persona"];
        $fk_origen = $row["fk_origen"];
    }
}
$sqlPersona = "SELECT * FROM Persona WHERE id_persona = $fk_persona";
$result = $conn->query($sqlPersona);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $idPer = $row["id_persona"];
        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $rut = $row["rut"];
        $telefono = $row["telefono"];
        $correo = $row["correo"];
    }
}

$sqlDireccion = "SELECT * FROM Direccion WHERE id = $Direccion_id";
$result = $conn->query($sqlDireccion);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $calle = $row["calle"];
        $numero = $row["numero"];
        $referencia = $row["referencia"];
        $letra = $row["letra"];
        $fk_comuna = $row["fk_comuna"];
    }
}

$sqlRegion = "SELECT Region.id_region FROM Region 
INNER JOIN Provincia ON Provincia.fk_region = Region.id_region
INNER JOIN Comuna ON Provincia.id_provincia = Comuna.fk_provincia
WHERE Comuna.id = $fk_comuna";

$result = $conn->query($sqlRegion);
if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
        $region = $row["id_region"];
    }
}

$sqlOrientacion = "SELECT * FROM Orientacion where fk_formulario = $idForm";
$result = $conn->query($sqlOrientacion);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $norte = $row["norte"];
        $sur = $row["sur"];
        $oriente = $row["oriente"];
        $poniente = $row["poniente"];
        $norOriente = $row["norOriente"];
        $norPoniente = $row["norPoniente"];
        $surOriente = $row["surOriente"];
        $surPoniente = $row["surPoniente"];

    }
}

$suites = array();
$walkings = array();
$closets = array();
$serv = array();
$sqlDormitorios = "SELECT * FROM Dormitorios where fk_formulario = $idForm";
$result = $conn->query($sqlDormitorios);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        array_push($suites, $row["suit"]);
        array_push($walkings, $row["walking"]);
        array_push($closets, $row["closet"]);
        array_push($serv, $row["servicio"]);

    }
}
$tipoBanno = array();
$sqlBannos = "SELECT * from Bannos where fk_formulario = $idForm";
$result = $conn->query($sqlBannos);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        array_push($tipoBanno, $row["fk_tipo_banno"]);
        

    }
}
$sqlEstacionamientos = "SELECT * FROM Estacionamientos where fk_formulario = $idForm";
$niveles = array();
$numeros = array();
$techados = array();
$roles_est = array();
$usogoce_est = array();
$otro_nivel = array();
$garage_cerrado = array();
$result = $conn->query($sqlEstacionamientos);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        array_push($niveles, $row["nivel"]);
        array_push($numeros, $row["numero"]);
        array_push($techados, $row["techado"]);
        array_push($roles_est, $row["rol"]);
        array_push($usogoce_est, $row["uso_goce"]);
        array_push($otro_nivel, $row["otro_nivel"]);
        array_push($garage_cerrado, $row["garage_cerrado"]);
    }
}

$sqlBodegas = "SELECT * FROM Bodegas WHERE fk_formulario = $idForm";
$nivelesBod = array();
$numerosBod = array();
$rol_bod = array();
$result = $conn->query($sqlBodegas);
$otro_bodega = array();
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        array_push($nivelesBod, $row["nivel"]);
        array_push($numerosBod, $row["num"]);     
        array_push($rol_bod, $row["rol"]);
        array_push($otro_bodega, $row["otro_bodega"]);
    }
}

$sqlpropiedad = "Select Direccion_id, id_propiedad from formulario, Propiedad where Propiedad.fk_formulario = formulario.id_formulario AND id_formulario = $idForm";
$result = $conn->query($sqlpropiedad);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $idPropiedad = $row["id_propiedad"];
        $idDirec = $row["Direccion_id"];
    }
}else{
    echo "<script>console.log('$sqlpropiedad')</script>";
}
?>

    <section>
        <div class="container">
            <form action="" id="formC">

                <input type="hidden" id="idPer" name="idPer" value="<?echo $idPer?>">
                <input type="hidden" id="idProp" name="idProp" value="<?echo $fk_propietario?>">
                <input type="hidden" id="idForm" name="idForm" value="<?echo $idForm?>">
                <input type="hidden" id="idPropiedad" name="idPropiedad" value="<?echo $idPropiedad?>">
                <input type="hidden" id="idDirec" name="idDirec" value="<?echo $idDirec?>">
                <input type="hidden" id="operacion" name="operacion" value="2">
                <input type="hidden" id="tipoProp" name="tipoProp" value="2">
                <input type="hidden" id="source" name="source" value="<?=$_GET[" source "]?>">


                <!-- progressbar -->
                <ul id="progressBar">
                    <li class="active">Corredor</li>
                    <li>Propietario</li>
                    <li>Propiedad</li>
                </ul>

                <div class="container">
                    <div class="row">
                        <div class="column col-12">

                        </div>
                    </div>
                </div>

                <!-- fieldset 1 -->
                <fieldset id="1">

                    <h2 class="fs-title">Información Corredor</h2>
                    <h3 class="fs-subtitle">Paso 1</h3>


                    <div class="select-container">
                        <label for="select_area" class="label_titulo">Área Redflip</label>
                        <select name="select_area" id="select_area" class="dropdown">
                        <option value="null" disabled>Área Redflip*</option>
                        <option value="Redflip Habitacional" <?=($area_redflip== "Redflip Habitacional")?'selected':''?>>Redflip Habitacional</option>
                        <option value="Redflip Comercial" <?=($area_redflip== "Redflip Comercial")?'selected':''?>>Redflip Comercial</option>
                        <option value="Redflip Luxury"<?=($area_redflip== "Redflip Luxury")?'selected':''?>>Redflip Luxury</option>
                    </select>
                    </div>

                    <div class="select-container">
                        <label for="corredor" class="label_titulo">Corredor</label>
                        <select name="corredor" id="corredor" onchange="selectMail()">
                        <option value="null" disabled>Nombre Corredor*</option>
                        <?
                        $sqlCorredor="Select Corredor.id_corredor, Persona.nombre, Persona.apellido, Persona.correo from Corredor, Persona where Corredor.fk_persona = Persona.id_persona AND eliminado != 1";
                        $result = $conn->query($sqlCorredor);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                if($fk_corredor == $row["id_corredor"]){
                                    echo "<option value=".$row["id_corredor"]." selected>".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                                    $mail = $row["correo"];
                                }else{
                                    echo "<option value=".$row["id_corredor"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                                }
                                
                            }
                        }
                        ?>
                    </select>
                    </div>

                    <input type="text" id="mail" value="" disabled>
                    <div class="select-container">
                        <label for="operario" class="label_titulo">Operario Cámara</label>
                        <select name="operario" id="operario">
                        <option value="null" disabled selected>Operario cámara*</option>
                        <?php
                        $sqlOperario = "Select Operario.id_operario, Persona.nombre, Persona.apellido from Operario, Persona where Operario.fk_persona = Persona.id_persona";
                        $result = $conn->query($sqlOperario);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                if($fk_operario == $row["id_operario"]){
                                    echo "<option value=".$row["id_operario"]." selected>".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                                }else{
                                    echo "<option value=".$row["id_operario"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                                }
                            
                            }
                        }
                        ?>
                    </select>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion1" class="paginacion paginacionIF" onchange="paginacion(1)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1" disabled>Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>

                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 2 -->
                <fieldset id="2">
                    <h2 class="fs-title">Información Propietario</h2>
                    <h3 class="fs-subtitle">Paso 2</h3>

                    <label for="nombre" class="label_titulo">Nombre</label>
                    <input class="fs-nom" type="text" name="nombre" placeholder="Nombre*" id="nombre" onfocusout="validarNombre()" value="<?php echo $nombre?>" />
                    <label for="apellido" class="label_titulo">Apellido</label>
                    <input class="fs-ape" type="text" name="apellido" placeholder="Apellido*" id="apellido" onfocusout="validarApellido()" value="<?php echo $apellido?>" />
                    <label for="rut" class="label_titulo">Rut</label>
                    <input class="fs-rut" type="text" name="rut" placeholder="Rut" id="rut" onfocusout="validarRut2()" value="<?php echo $rut?>" />
                    <div class="select-container">
                        <label for="origen" class="label_titulo">Origen</label>
                        <select name="origen" id="origen" class="fs-ori" onchange="cambioOrigen()">
                        <?php
                            if($fk_origen == "17"){
                                echo '  <option value="" disabled>Origen*</option>
                                        <option value="17" selected>Captado por Redflip</option>
                                        <option value="7">Referido Corredor</option>
                                        <option value="6">Referido Cliente</option>
                                        <option value="5">Referido Conserje</option>
                                        <option value="18">Partner Redflip</option>';
                            }else if($fk_origen == "7"){
                                echo '  <option value="" disabled>Origen*</option>
                                        <option value="17">Captado por Redflip</option>
                                        <option value="7" selected>Referido Corredor</option>
                                        <option value="6">Referido Cliente</option>
                                        <option value="5">Referido Conserje</option>
                                        <option value="18">Partner Redflip</option>';
                            }else if($fk_origen == "6"){
                                echo '  <option value="" disabled>Origen*</option>
                                        <option value="17">Captado por Redflip</option>
                                        <option value="7">Referido Corredor</option>
                                        <option value="6" selected>Referido Cliente</option>
                                        <option value="5">Referido Conserje</option>
                                        <option value="18">Partner Redflip</option>';
                            }else if($fk_origen == "5"){
                                echo '  <option value="" disabled>Origen*</option>
                                        <option value="17">Captado por Redflip</option>
                                        <option value="7">Referido Corredor</option>
                                        <option value="6">Referido Cliente</option>
                                        <option value="5" selected>Referido Conserje</option>
                                        <option value="18">Partner Redflip</option>';
                            }else if($fk_origen == "18"){
                                echo '  <option value="" disabled>Origen*</option>
                                        <option value="17">Captado por Redflip</option>
                                        <option value="7">Referido Corredor</option>
                                        <option value="6">Referido Cliente</option>
                                        <option value="5">Referido Conserje</option>
                                        <option value="18" selected>Partner Redflip</option>';
                            }else{
                                echo '  <option value=""disabled selected>Origen*</option>
                                        <option value="17">Captado por Redflip</option>
                                        <option value="7">Referido Corredor</option>
                                        <option value="6">Referido Cliente</option>
                                        <option value="5">Referido Conserje</option>
                                        <option value="18">Partner Redflip</option>';
                            }
                        ?>
                    </select>
                    </div>
                    <div class="select-container" id="div_captadaR" style="display:none">
                        <div class="select-container">
                            <label for="captadaR" class="label_titulo">Seleccione:</label>
                            <select name="captadaR" id="captadaR" class="fs-ori">
                            <option value="" disabled>Seleccione opción*</option>
                            <option value="9"<?=($origen2 == "9")?"selected":""?>>Base de Datos</option>
                            <option value="19"<?=($origen2 == "19")?"selected":""?>>Google</option>
                            <option value="1"<?=($origen2 == "1")?"selected":""?>>Instagram</option>
                            <option value="2"<?=($origen2 == "2")?"selected":""?>>Facebook</option>
                            <option value="23"<?=($origen2 == "23")?"selected":""?>>Desconocido</option>

                        </select>
                        </div>
                    </div>
                    <div class="select-container" id="div_partnerR" style="display:none">
                        <div class="select-container">
                            <label for="partnerR" class="label_titulo">Seleccione:</label>
                            <select name="partnerR" id="partnerR" class="fs-ori">
                            <option value="" disabled>Seleccione opción*</option>
                            <option value="11"<?=($origen2 == "11")?"selected":""?>>Doorlis</option>
                            <option value="20"<?=($origen2 == "20")?"selected":""?>>De Carlo</option>
                            <option value="12"<?=($origen2 == "12")?"selected":""?>>OpenCasa</option>
                            <option value="21"<?=($origen2 == "21")?"selected":""?>>UHomie</option>
                            <option value="13"<?=($origen2 == "13")?"selected":""?>>GI0</option>
                            <option value="22"<?=($origen2 == "22")?"selected":""?>>Stoffel & Valdivieso</option>
                            <option value="15"<?=($origen2 == "15")?"selected":""?>>Otro</option>
                            <option value="23"<?=($origen2 == "23")?"selected":""?>>Desconocido</option>

                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="selectDivisa-container">
                            <label for="telefono" class="label_titulo">Teléfono</label><br>
                            <select name="sub_fono" id="sub_fono" class="fs-suFono">
                        <?php
                            $pre = substr($telefono, 0, 4);
                            $tel = substr($telefono, 4);
                        ?>
                            <option value="" disabled>Prefijo*</option>
                            <option value="+569"<?=($pre == "+569")?"selected":""?>>+569</option>
                            <option value="+562"<?=($pre == "+562")?"selected":""?>>+562</option>
                            <option value="+568"<?=($pre == "568")?"selected":""?>>+568</option>
                            <option value="+56"<?=($pre == "+56")?"selected":""?>>+56</option>
                        </select>
                        </div>
                        <input class="fs-fono" type="tel" name="telefono" id="telefono" value="<?php echo $tel?>" placeholder="Teléfono*" onfocusout="validarTelefono()" />
                    </div>

                    <label for="correo" class="label_titulo">Correo</label>
                    <input type="mail" name="correo" id="correo" placeholder="Correo*" value="<?php echo $correo?>" onfocusout="validarCorreo()" />

                    <label class="txtProp">Fecha</label>
                    <input type="date" name="fecha" id="fecha" placeholder="Fecha" value="<?= $fecha?>" />

                    <hr>
                    <h3 class="fs-subtitle">Contactos Adicionales</h3>

                    <div class="divC">
                        <ol>
                            <div id="contactos">
                                <?php
                        $sqlContactos = "Select * from Contacto where fk_propietario = $fk_propietario";
                        $result = $conn->query($sqlContactos);
                        $cont = 1;
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                if($row["nombre"] != "NULL" && $row["telefono"] != "NULL" && $row["correo"] != "NULL"){
                                    echo '<li>
                                    <input class="fs-nomC" type="text" name="nombreC'.$cont.'" id="nombreC'.$cont.'" placeholder="Nombre*" value="'.utf8_encode($row["nombre"]).'"/>
                                    <input class="fs-fonoC" type="tel" name="telC'.$cont.'" id="telC'.$cont.'" placeholder="Teléfono*" value="'.$row["telefono"].'"/>
                                    <input class="fs-mailC" type="mail" name="correoC'.$cont.'" id="correoC'.$cont.'" placeholder="Correo*" value="'.$row["correo"].'" />
                                    </li>';
                                }
                                $cont ++;
                            }
                        } else {
                            echo "<script>console.log('0 Results en Contactos')</script>";
                        }
                        ?>
                            </div>
                        </ol>


                        <a class="addC" id="addC" href="#" onclick="agregarContacto(); return false;"> Agregar contacto <i
                            class="fas fa-plus-circle"></i></a>
                        <a class="delC" id="delC" href="#" onclick="eliminarContacto(); return false;"> Eliminar contacto <i
                            class="fas fa-minus-circle"></i></a>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion2" class="paginacion" onchange="paginacion(2)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2" disabled>Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (1/11)-->
                <fieldset id="3">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (1/11)</h3>

                    <div id="valor1" style="">
                        <label for="monto1" class="label_titulo">Monto</label><br>
                        <input class="fs-valor fs-tNumb" type="text" name="monto1" id="monto1" placeholder="Monto arriendo" style="width:75%!important" onfocusout="validarPrecio('monto1')" value="<?=$monto1?>" />
                        <select name="divisaMonto1" id="divisaMonto1" class="fs-divisa">
                        <option value="" disabled selected>Divisa</option>
                        <option value="1" <?=($divisa_monto1 == 1)?'Selected':''?>>UF</option>
                        <option value="2" <?=($divisa_monto1 == 2)?'Selected':''?>>CLP</option>
                    </select>
                    </div>
                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">Amoblado</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                <input type="checkbox" id="amoblado" name="amoblado" <?=($amoblado == "1")?"checked":""?> onchange="amobla2()">
                                <span class="slider round"></span>
                            </label>
                            </div>
                            <div id="div_valor_amoblado" class="fs-tipoOpe" style="display:none">
                                <label for="valorAmob" class="label_titulo">Valor diferencia</label>
                                <input class="fs-valor fs-tNumb" type="text" name="valorAmob" id="valorAmob" placeholder="Valor de diferencia amoblado" onfocusout="validarPrecio('valorAmob')" value="<?=$valorAmob?>" />
                            </div>
                        </div>
                    </div>

                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">No exclusivo</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                <input type="checkbox" id="exclusividad" name="exclusividad"
                                    onchange="noExclusividad()" <?=($exclusividad == "1")?"checked":""?>>
                                <span class="slider round"></span>
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="pregExclusividad">
                        <div id="div_preg_Exclusividad" style="display:none">
                            <div id="scCantCorredor" class="">
                                <label for="cantCorredor" class="label_titulo">Cantidad de Corredores</label>
                                <select name="cantCorredor" id="cantCorredor" class="fs-tipoOpe">
                                <option value="null" disabled selected>Cant. de corredores</option>
                                <?php
                                for($i = 1; $i<=6; $i++){
                                    if($i == $cantCorredor){
                                        echo "<option value='".$i."' selected>".$i."</option>";
                                    }else{
                                        echo "<option value='".$i."'>".$i."</option>";
                                    }
                                    
                                }
                                ?>
                            </select>
                            </div>

                            <div id="scTiempoPublicacion" class="">
                                <label for="select_tiempoPublicacion" class="label_titulo">Tiempo de publicación</label>
                                <select name="select_tiempoPublicacion" id="select_tiempoPublicacion" class="fs-tipoOpe">
                                <option value="null" disabled selected>Tiempo de publicación</option>
                                <?php
                                $sqlTiempo = "SELECT * FROM Tiempo_publicacion";
                                $result = $conn->query($sqlTiempo);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        if($tiempo == $row["id_tiempo_publicacion"]){
                                            echo "<option value='".$row["id_tiempo_publicacion"]."' selected>".$row["tiempo"]."</option>";
                                        }else{
                                            echo "<option value='".$row["id_tiempo_publicacion"]."'>".$row["tiempo"]."</option>";
                                        }
                                        
                                    }
                                } else {
                                    echo "<option>No hay datos</option>";
                                }
                                ?>
                            </select>
                            </div>
                        </div>
                    </div>

                    <label for="infoPropVenta" class="label_titulo">¿Por qué quieres arrendar?</label>
                    <textarea name="infoPropVenta" id="infoPropVenta" cols="30" rows="3" placeholder="¿Por qué quieres arrendar?" maxlength="1000"><?=utf8_encode($infoPropVenta)?></textarea>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion3" class="paginacion" onchange="paginacion(3)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3" disabled>Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (2/11)-->
                <fieldset id="4">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (2/11)</h3>
                    <div class="form-group">
                        <label for="calle" class="label_titulo">Dirección</label><br>
                        <input class="fs-calle" type="text" name="calle" placeholder="Calle*" id="calle" value="<?=utf8_encode($calle)?>" />
                        <input class="fs-numb fs-tNumb" type="number" name="numero" placeholder="Número*" id="numero" value="<?=$numero?>" />
                        <input class="fs-letra" type="text" name="letra" placeholder="Dpto*" id="letra" value="<?=$letra?>" />
                    </div>

                    <!-- <input class="fs-condominio" type="text" name="condominio" placeholder="Condominio" /> -->
                    <label for="region" class="label_titulo">Región</label><br>
                        <div class="selectCom-container">
                            <select name="region" id="region" class="fs-comuna" onchange="cambioRegion()">
                            <option value="null" disabled selected>Región*</option>
                            <?php
                            $sqlRegion = "Select * from Region";
                            $result = $conn->query($sqlRegion);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    //1-Arica 2-Tarapaca 3-Antofagasta 4-Atacama 5-Coquimbo
                                    //6-Valpo 7-RM 8-OHiggins 9-Maule 10-Ñuble 11-Biobio
                                    //12-Araucania 13-LosRios 14-LosLagos 15-Aysen 16-Magallanes&Antartica 17-SD
                                    if($row["id_region"] == 7 || $row["id_region"] == 11 || $row["id_region"] == 12){
                                        if($row["id_region"]==$region){
                                            echo "<option selected value=".$row["id_region"].">".utf8_encode($row["nombre_region"])."</option>";
                                        }else{
                                            echo "<option value=".$row["id_region"].">".utf8_encode($row["nombre_region"])."</option>";
                                        }
                                    }
                                }
                            } 
                            ?>
                            </select>
                        </div>

                    <!-- <input class="fs-condominio" type="text" name="condominio" placeholder="Condominio" /> -->
                    <label for="comuna" class="label_titulo">Comuna</label><br>
                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td3" colspan="6">
                            <div class="selectCom-container">
                                    <select name="comuna" id="comuna" class="fs-comuna">
                                    <option value="null" disabled selected>Comuna*</option>
                                    <?php
                                    $sqlComuna = "SELECT * FROM Comuna 
                                    INNER JOIN Provincia ON Provincia.id_provincia = Comuna.fk_provincia 
                                    INNER JOIN Region ON Region.id_region = Provincia.fk_region 
                                    WHERE Region.id_region = 7 AND Comuna.id NOT IN (53) ORDER BY Comuna.nombre ASC";
                                    $result = $conn->query($sqlComuna);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            if($row["id"] == $fk_comuna){
                                                echo "<option selected value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }else{
                                                echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }
                                            }
                                        } 
                                    ?>
                                    <option value="53">S/D</option>
                                    </select>
                                </div>

                                <div class="selectCom-container">
                                    <select name="comuna11" id="comuna11" class="fs-comuna">
                                    <option value="null" disabled selected>Comuna*</option>
                                    <?php
                                    $sqlComuna = "SELECT * FROM Comuna 
                                    INNER JOIN Provincia ON Provincia.id_provincia = Comuna.fk_provincia 
                                    INNER JOIN Region ON Region.id_region = Provincia.fk_region 
                                    WHERE Region.id_region = 11 AND Comuna.id NOT IN (53) ORDER BY Comuna.nombre ASC";
                                    $result = $conn->query($sqlComuna);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            if($row["id"] == $fk_comuna){
                                                echo "<option selected value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }else{
                                                echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }
                                            }
                                        } 
                                    ?>
                                    <option value="53">S/D</option>
                                    </select>
                                </div>

                                <div class="selectCom-container">
                                    <select name="comuna12" id="comuna12" class="fs-comuna">
                                    <option value="null" disabled selected>Comuna*</option>
                                    <?php
                                    $sqlComuna = "SELECT * FROM Comuna 
                                    INNER JOIN Provincia ON Provincia.id_provincia = Comuna.fk_provincia 
                                    INNER JOIN Region ON Region.id_region = Provincia.fk_region 
                                    WHERE Region.id_region = 12 AND Comuna.id NOT IN (53) ORDER BY Comuna.nombre ASC";
                                    $result = $conn->query($sqlComuna);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            if($row["id"] == $fk_comuna){
                                                echo "<option selected value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }else{
                                                echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }
                                            }
                                        } 
                                    ?>
                                    <option value="53">S/D</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr class="fs-tr">
                            <!-- <br> -->
                            <td class="fs-td2" id="switchCond1" style="">Condominio</td class="fs-td">
                            <td class="fs-td3" id="switchCond2" style="">
                                <label class="switch" id="switchCond3" style="">
                                <input type="checkbox" id="condominio" name="condominio" onchange="condominioF()" <?= ($condominio == "1")? "checked": "" ?>>
                                <span class="slider round" id="switchCond4" style=""></span>
                            </label>
                            </td>
                            <td class="fs-td2" id="switchLot1" style="">Loteo</td class="fs-td">
                            <td class="fs-td3" id="switchLot2" style="">
                                <label class="switch" id="switchLot3" style="">
                                <input type="checkbox" id="loteo" name="loteo" onchange="loteoF()" <?= ($loteo == "1")? "checked": "" ?>>
                                <span class="slider round" id="switchLot4" style=""></span>
                            </label>
                            </td>
                            <td class="fs-td2" id="switchPar1" style="display: ">Pareada</td class="fs-td">
                            <td class="fs-td3" id="switchPar2" style="display: ">
                                <label class="switch" id="switchPar3" style="display:">
                                <input type="checkbox" id="pareada" name="pareada" onchange="pareadaF()" <?= ($pareada == "1")? "checked": "" ?>>
                                <span class="slider round" id="switchPar4" style="display:"></span>
                            </label>
                            </td>
                        </tr>
                    </table>

                    <label for="referencia" class="label_titulo">Referencia</label>
                    <input class="fs-referencia" type="text" name="referencia" id="referencia" placeholder="Referencia" value="<?=utf8_encode($referencia)?>" />

                    <div class="form-group" style="display: none" id="divCondominio">
                        <label for="calleCond" class="label_titulo">Dirección Particular</label><br>
                        <input class="fs-calle" type="text" name="calleCond" placeholder="Calle" id="calleCond" value="<?=$calleCond?>" />
                        <input class="fs-numb fs-tNumb" type="number" name="numeroCond" placeholder="Número" id="numeroCond" value="<?=$numeroCond?>" />
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion4" class="paginacion" onchange="paginacion(4)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4" disabled>Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (5/11)-->
                <fieldset id="5">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (5/11)</h3>

                    <label for="construccion" class="label_titulo fs-construccion fs-tNumb">Año de Construcción</label>
                    <div class="form-group">
                        <input class="fs-construccion fs-tNumb" type="number" name="construccion" id="construccion" placeholder="Año construcción" onfocusout="validarAno('construccion')" value="<?= $anno ?>" />
                    </div>

                    <div id="div_gasto_comun">
                        <label for="gastoC" class="label_titulo">Valor Gasto Común</label>
                        <input class="fs-gastoC fs-tNumb" type="text" name="gastoC" id="gastoC" placeholder="Valor Gasto común" onfocusout="validarPrecio('gastoC')" value="<?= $gastos_comunes?>" />
                        <label for="gastos_comun" class="label_titulo">¿Qué incluyen los gastos comunes?</label>
                        <textarea name="gastos_comun" id="gastos_comun" cols="30" rows="3" placeholder="¿Qué Incluyen los Gastos Comunes?" maxlength="1000"><?= utf8_encode($nota_gastos_com)?></textarea>
                    </div>

                    <div class="select-container">
                        <label for="calefaccion" class="label_titulo">Calefacción</label>
                        <select name="calefaccion" id="calefaccion" class="fs-nDorm" onchange="fnCalefa()">
                        <option value="" disabled selected>Tipo de calefacción</option>
                        <?php
                            $sqlCalefa = "SELECT * FROM Tipo_calefaccion where id not in (6,5) order by tipo";
                            $result = $conn->query($sqlCalefa);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    if($tipo_calefaccion == $row["id"]){
                                        echo '<option value="'.$row["id"].'" selected>'.$row["tipo"].'</option>';
                                    }else{
                                        echo '<option value="'.$row["id"].'">'.$row["tipo"].'</option>';
                                    }
                                    
                                }
                            }
                        ?>
                    </select>
                        <input type="text" name="otroCalefa" id="otroCalefa" style="display:none" placeholder="Indique la calefacción">
                    </div>

                    <div class="select-container">
                        <label for="tipoGas" class="label_titulo">Tipo de Gas</label>
                        <select name="tipoGas" id="tipoGas" class="fs-nDorm">
                        <option value="" disabled selected>Tipo de gas</option>
                        <?php
                                    $sqlCalefa = "SELECT * FROM Tipo_gas where id_tipo_gas not in (3) order by tipo";
                                    $result = $conn->query($sqlCalefa);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            if($fk_tipo_gas == $row["id_tipo_gas"]){
                                                echo '<option value="'.$row["id_tipo_gas"].'" selected>'.utf8_encode($row["tipo"]).'</option>';
                                            }else{
                                                echo '<option value="'.$row["id_tipo_gas"].'">'.utf8_encode($row["tipo"]).'</option>';
                                            }
                                        }
                                    }
                                ?>
                    </select>
                    </div>

                    <div class="select-container">
                        <label for="aguaCaliente" class="label_titulo">Agua Caliente</label>
                        <select name="aguaCaliente" id="aguaCaliente" class="fs-nDorm" onchange="fnAguaCaliente()">
                        <option value="" disabled selected>Agua caliente</option>
                        <?php
                            $sql = "SELECT * from Agua_caliente order by tipo";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    if($agua_caliente == $row["id_agua_caliente"]){
                                        echo "<option value=".$row["id_agua_caliente"]." selected>".utf8_encode($row["tipo"])."</option>";
                                    }else{
                                        echo "<option value=".$row["id_agua_caliente"].">".utf8_encode($row["tipo"])."</option>";
                                    }                                                        
                                }
                            }
                            ?>
                    </select>
                        <input type="text" name="otroAguaCaliente" id="otroAguaCaliente" placeholder="Indique tipo de agua caliente" style="display:none">
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion5" class="paginacion" onchange="paginacion(5)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5" disabled>Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (6/11)-->
                <fieldset id="6">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (6/11)</h3>

                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td3">
                                <label for="supU" class="label_titulo">Superficie Útil</label>
                                <input class="fs-supU fs-tNumb" type="text" name="supU" id="supU" placeholder="m2 util" onfocusout="validarPrecio('supU')" value="<?= $superficie_util?>" />
                            </td>
                        </tr>

                        <tr class="fs-tr">
                            <td class="fs-td3" id="supTerrTd">
                                <label for="supTerr" class="label_titulo">Superficie Terreno</label>
                                <input class="fs-supTerr fs-tNumb" type="text" name="supTerr" id="supTerr" placeholder="m2 terreno" onfocusout="validarPrecio('supTerr')" value="<?= $superficie_terreno?>" />
                            </td>
                            <td class="fs-td3" id="supPatTd" style="">
                                <label for="supPatio" class="label_titulo">Superficie Patio</label>
                                <input class="fs-supTerr fs-tNumb" type="number" name="supPatio" id="supPatio" onfocusout="validarPrecio('supPatio')" placeholder="Superficie patio" value="<?= $sup_patio?>" />
                            </td>
                        </tr>
                    </table>
                    <!-- Si se escribe en sup. Patio, debe aparecer -->
                    <div class="column col-12" id="usoGoceDiv" style="display:none">
                        <div class="fluid-container">
                            <div class="row">
                                <div class="column col-6">
                                    <label for="usoGoce">
                                    Uso y goce
                                </label>
                                </div>
                                <div class="column col-6">
                                    <label class="switch">
                                    <input type="checkbox" id="usoGoce" name="usoGoce">
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin dinamico -->
                    <!-- <input class="fs-pDpto fs-tNumb" type="number" name="CantPisosCasa" id="CantPisosCasa" placeholder="Cant. de pisos casa" style="display:none"/> -->

                    <label for="CantPisosCasa" class="label_titulo">Cantidad de Pisos Casa</label>
                    <div class="form-group">
                        <div id="scCantPisosCasa" class="select-container">
                            <select name="CantPisosCasa" id="CantPisosCasa" class="fs-ori" style="">
                            <option value="" disabled>Cant. de pisos casa</option>
                            <option value="1" <?=($cant_pisos_casa=="1")?"selected":""?>>1</option>
                            <option value="2" <?=($cant_pisos_casa=="2")?"selected":""?>>2</option>
                            <option value="3" <?=($cant_pisos_casa=="3")?"selected":""?>>3</option>
                            <option value="4" <?=($cant_pisos_casa=="4")?"selected":""?>>4</option>
                        </select>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="column col-4">
                                <label class="txtProp">
                                    Orientación
                                </label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">N
                                    <input type="checkbox" id="norte" name="norte" onchange="" <?=($norte==1)?"checked":""?>>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">S
                                    <input type="checkbox" id="sur" name="sur" onchange="" <?=($sur==1)?"checked":""?>>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">O
                                    <input type="checkbox" id="oriente" name="oriente" onchange="" <?=($oriente==1)?"checked":""?>>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">P
                                    <input type="checkbox" id="poniente" name="poniente" onchange="" <?=($poniente==1)?"checked":""?>>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">NO
                                    <input type="checkbox" id="nOriente" name="nOriente" onchange=""<?=($norOriente==1)?"checked":""?>>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">NP
                                    <input type="checkbox" id="nPoniente" name="nPoniente" onchange=""<?=($norPoniente==1)?"checked":""?>>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">SO
                                    <input type="checkbox" id="sOriente" name="sOriente" onchange=""<?=($surOriente==1)?"checked":""?>>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">SP
                                    <input type="checkbox" id="sPoniente" name="sPoniente" onchange=""<?=($surPoniente==1)?"checked":""?>>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion6" class="paginacion" onchange="paginacion(6)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6" disabled>Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (7/11)-->
                <fieldset id="7">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (7/11)</h3>

                    <label for="num_dorm" class="label_titulo">Número de Dormitorios</label>
                    <div class="select-container">
                        <select name="num_dorm" id="num_dorm" class="fs-nDorm" onchange="nDorm()">
                                    <option value="" disabled selected>N° de dormitorios</option>
                                <?php
                                for($i = 0; $i < 10 ; $i++){
                                    if($i+1 == $dormitorios){
                                            echo '<option value ="'.($i+1).'" selected>'.($i+1).'</option>';
                                    }else{
                                            echo '<option value ="'.($i+1).'">'.($i+1).'</option>';
                                    }
                                }
                                
                                ?>
                    </select>
                    </div>

                    <table class="fs-table" id="dorm_aumentar">
                        <?php
                    for($i=0; $i< $dormitorios; $i++){

                        $imprimir .=   '<tr>
                        <td class="fs-td2" rowspan="2" style="text-align:center;">'.($i+1).'</td>
                        <td class="fs-td2">
                            <label class="fs-chkLbl">En suite
                                <input type="checkbox" id="suite'.($i+1).'" name="suite'.($i+1).'"';
                                
                        $imprimir .=(($suites[$i] == 1)? 'checked': '');
                        
                        $imprimir.='>
                                <span class="checkmark"></span>
                            </label>
                        </td>
                        <td class="fs-td2">
                            <label class="fs-chkLbl">WalkingC.
                                <input type="checkbox" id="walking'.($i+1).'" name="walking'.($i+1).'"';
                        
                        $imprimir .= (($walkings[$i] == 1)? 'checked': '');
                        $imprimir.='>
                                <span class="checkmark"></span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="fs-td2">
                            <label class="fs-chkLbl">ClosetN.
                                <input type="checkbox" id="closet'.($i+1).'" name="closet'.($i+1).'"';
                        
                        $imprimir.=(($closets[$i] == 1)? 'checked': '');
                        $imprimir.='>
                                <span class="checkmark"></span>
                            </label>
                        </td>
                        <td class="fs-td2">
                            <label class="fs-chkLbl">Servicio
                                <input type="checkbox" id="serv'.($i+1).'" name="serv'.($i+1).'"';
                        $imprimir .= (($serv[$i] == 1)? 'checked': '');        
                        $imprimir.='>
                                <span class="checkmark"></span>
                            </label>
                        </td>
                        
                    </tr>
                    <tr>
                        <td class="fs-td2" colspan="3">
                            <hr>
                        </td>
                    </tr>';
                    }
                    echo $imprimir;
                    ?>
                    </table>

                    <label for="cant_banos" class="label_titulo">Número de Baños</label>
                    <div class="select-container">
                        <select name="cant_banos" id="cant_banos" class="fs-nBan" onchange="bannos()">
                                <option value="0" disabled selected>N° de baños</option>
                                <?php
                            for($i = 0; $i < 10 ; $i++){
                                if($i+1 == $cant_bannos){
                                        echo '<option value ="'.($i+1).'" selected>'.($i+1).'</option>';
                                }else{
                                        echo '<option value ="'.($i+1).'">'.($i+1).'</option>';
                                }
                                
                            }
                            
                            
                            ?>
                            </select>
                    </div>

                    <table class="fs-table" width="90%" id="banos_aumentar">
                        <?php

                    for($i=0; $i< $cant_bannos; $i++){
                        if($tipoBanno[$i] == 1){
                            echo '<tr>
                            <td rowspan="2">
                                '.($i+1).'
                            </td>
                            <td>
                            <label for="completo'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="completo'.($i+1).'" value="3"  class="with-gap">
                            <span name="completo">Completo</span></label>
                            <label for="servicio'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="servicio'.($i+1).'" value="4"  class="with-gap">
                            <span name="servicio">Servicio</span></label>

                            </td>
                        </tr>
                        <tr>
                            <td>
                            <label for="visita'.($i+1).'"><input checked type="radio" name="tipo_banno'.($i+1).'" id="visita'.($i+1).'" value="1"  class="with-gap">
                            <span name="visita">Visita</span></label>

                            <label for="medio'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="medio'.($i+1).'" value="2"  class="with-gap">
                            <span name="medio">Medio Baño</span></label>

                            </td>
                                                                        
                        </tr>
                        <tr>
                            <td class="fs-td2" colspan="3">
                                <hr>
                            </td>
                        </tr>';
                        }else if($tipoBanno[$i] == 2){
                            echo '<tr>
                            <td rowspan="2">
                                '.($i+1).'
                            </td>
                            <td>
                            <label for="completo'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="completo'.($i+1).'" value="3"  class="with-gap">
                            <span name="completo">Completo</span></label>
                            <label for="servicio'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="servicio'.($i+1).'" value="4"  class="with-gap">
                            <span name="servicio">Servicio</span></label>

                            </td>
                        </tr>
                        <tr>
                            <td>
                            <label for="visita'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="visita'.($i+1).'" value="1"  class="with-gap">
                            <span name="visita">Visita</span></label>

                            <label for="medio'.($i+1).'"><input checked type="radio" name="tipo_banno'.($i+1).'" id="medio'.($i+1).'" value="2"  class="with-gap">
                            <span name="medio">Medio Baño</span></label>

                            </td>
                                                                        
                        </tr>
                        <tr>
                            <td class="fs-td2" colspan="3">
                                <hr>
                            </td>
                        </tr>';
                        }else if($tipoBanno[$i] == 3){
                            echo '<tr>
                            <td rowspan="2">
                                '.($i+1).'
                            </td>
                            <td>
                            <label for="completo'.($i+1).'"><input checked type="radio" name="tipo_banno'.($i+1).'" id="completo'.($i+1).'" value="3"  class="with-gap">
                            <span name="completo">Completo</span></label>
                            <label for="servicio'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="servicio'.($i+1).'" value="4"  class="with-gap">
                            <span name="servicio">Servicio</span></label>

                            </td>
                        </tr>
                        <tr>
                            <td>
                            <label for="visita'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="visita'.($i+1).'" value="1"  class="with-gap">
                            <span name="visita">Visita</span></label>

                            <label for="medio'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="medio'.($i+1).'" value="2"  class="with-gap">
                            <span name="medio">Medio Baño</span></label>

                            </td>
                                                                        
                        </tr>
                        <tr>
                            <td class="fs-td2" colspan="3">
                                <hr>
                            </td>
                        </tr>';
                        }else if($tipoBanno[$i] == 1){
                            echo '<tr>
                            <td rowspan="2">
                                '.($i+1).'
                            </td>
                            <td>
                            <label for="completo'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="completo'.($i+1).'" value="3"  class="with-gap">
                            <span name="completo">Completo</span></label>
                            <label for="servicio'.($i+1).'"><input checked type="radio" name="tipo_banno'.($i+1).'" id="servicio'.($i+1).'" value="4"  class="with-gap">
                            <span name="servicio">Servicio</span></label>

                            </td>
                        </tr>
                        <tr>
                            <td>
                            <label for="visita'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="visita'.($i+1).'" value="1"  class="with-gap">
                            <span name="visita">Visita</span></label>

                            <label for="medio'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="medio'.($i+1).'" value="2"  class="with-gap">
                            <span name="medio">Medio Baño</span></label>

                            </td>
                                                                        
                        </tr>
                        <tr>
                            <td class="fs-td2" colspan="3">
                                <hr>
                            </td>
                        </tr>';
                        }else{
                            echo '<tr>
                            <td rowspan="2">
                                '.($i+1).'
                            </td>
                            <td>
                            <label for="completo'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="completo'.($i+1).'" value="3"  class="with-gap">
                            <span name="completo">Completo</span></label>
                            <label for="servicio'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="servicio'.($i+1).'" value="4"  class="with-gap">
                            <span name="servicio">Servicio</span></label>

                            </td>
                        </tr>
                        <tr>
                            <td>
                            <label for="visita'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="visita'.($i+1).'" value="1"  class="with-gap">
                            <span name="visita">Visita</span></label>

                            <label for="medio'.($i+1).'"><input type="radio" name="tipo_banno'.($i+1).'" id="medio'.($i+1).'" value="2"  class="with-gap">
                            <span name="medio">Medio Baño</span></label>

                            </td>
                                                                        
                        </tr>
                        <tr>
                            <td class="fs-td2" colspan="3">
                                <hr>
                            </td>
                        </tr>';
                        }
                    }
                
                    ?>
                    </table>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion7" class="paginacion" onchange="paginacion(7)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7" disabled>Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (8/11)-->
                <fieldset id="8">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (8/11)</h3>

                    <label for="select_estacionamientos" class="label_titulo">Cantidad de Estacionamientos</label>
                    <div class="select-container">
                        <select name="select_estacionamientos" id="select_estacionamientos" class="fs-nEst" onchange="estacionamientos()">
                        <option value="" disabled selected>Cantidad de estacionamientos</option>
                        <?php
                            for($i = 0; $i<10;$i++){
                                if($i+1 == $estacionamiento){
                                    echo "<option value='".($i+1)."' selected>".($i+1)."</option>";
                                }else{
                                    echo "<option value='".($i+1)."'>".($i+1)."</option>";
                                }
                            }
                            ?>>
                    </select>
                    </div>

                    <div class="form-group" id="estacionamientos">
                        <?php
                    
                    for($i=0;$i<$estacionamiento; $i++){
                        $rol_res = explode("-", $roles_est[$i]);

                        $est .= '
                    <label for="">'.($i+1).'</label>
                    <div class="selectEst-container">
                    <select name="est_sub'.($i+1).'" id="est_sub'.($i+1).'" class="fs-estSub fs-alterSelect" onchange="nivelCalle('.($i+1).')">
                        <option value="" >Subterráneo</option>
                        <option value="Nivel Calle"';
                        
                        $est.=($niveles[$i] == "Nivel Calle")?'Selected':'';

                        $est.=' 
                        >Nivel calle</option>
                        <option value="-1"';
                        $est.=($niveles[$i] == "-1")?'Selected':'';
                        $est.='>-1</option>
                        <option value="-2"';
                        $est.=($niveles[$i] == "-2")?'Selected':'';
                        $est.='>-2</option>
                        <option value="-3"';
                        $est.=($niveles[$i] == "-3")?'Selected':'';
                        $est.='>-3</option>
                        <option value="-4"';
                        $est.=($niveles[$i] == "-4")?'Selected':'';
                        $est.='>-4</option>
                        <option value="-5"';
                        $est.=($niveles[$i] == "-5")?'Selected':'';
                        $est.='>-5</option>
                        <option value="-6"';
                        $est.=($niveles[$i] == "-6")?'Selected':'';
                        $est.='>-6</option>
                        <option value="otro"';
                        $est.=($niveles[$i] == "otro")?'Selected':'';
                        $est.='>Otro</option>
                    </select>
                    </div>
                    <input class="fs-numEstSub fs-alterSelect" type="text" name="num_est'.($i+1).'" id="num_est'.($i+1).'" placeholder="N°" maxlength="4" value="'.$numeros[$i].'">
                    <div style="'.(($niveles[$i] == "Nivel Calle")?'':'display:none').'" id="techado'.($i+1).'">      
                        Techado
                        <label class="switch">
                            <input type="checkbox" id="techado'.($i+1).'" name="techado'.($i+1).'" '.(($techados[$i] == "1")?'checked':'').'>
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <div style="" id="divGarageC'.($i+1).'">      
                    Garage Cerrado
                    <label class="switch">
                                                        
                        <input type="checkbox" id="garageC'.($i+1).'" name="garageC'.($i+1).'" '.(($garage_cerrado[$i] == "1")?'checked':'').'>
                        <span class="slider round"></span>
                    </label>
                    </div>
                    <div>
                        <input type="text" placeholder="Escriba aqui el nivel en que se encuentra el estacionamiento, Ej: 2" id="nivel'.($i+1).'" name="nivel'.($i+1).'" style="'.(($niveles[$i] == "otro")?'':'display:none').'" class="" value="'.$otro_nivel[$i].'">
                    </div>
                    <br>
                    <hr>';
                    }
                    
                    echo $est;
                    
                    ?>
                    </div>

                    <label for="select_bodega" class="label_titulo">Cantidad de Bodegas</label>
                    <div class="select-container">
                        <select name="select_bodega" id="select_bodega" class="fs-nEst" onchange="bodegas()">
                    <?php 
                        
                        if($bodega == "1"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "2"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" selected>2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "3"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "4"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" selected>4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "5"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" selected>5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "6"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                                <option value="6" selected>6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "7"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                                <option value="6" >6</option>
                                <option value="7" selected>7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "8"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                                <option value="6" >6</option>
                                <option value="7" >7</option>
                                <option value="8" selected>8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "9"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                                <option value="6" >6</option>
                                <option value="7" >7</option>
                                <option value="8" >8</option>
                                <option value="9" selected>9</option>
                                <option value="10">10</option>';
                        }else if($bodega == "10"){
                            echo '
                                <option value="" disabled>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                                <option value="6" >6</option>
                                <option value="7" >7</option>
                                <option value="8" >8</option>
                                <option value="9" >9</option>
                                <option value="10" selected>10</option>';
                        }else{
                            echo '
                                <option value="" disabled selected>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                                <option value="6" >6</option>
                                <option value="7" >7</option>
                                <option value="8" >8</option>
                                <option value="9" >9</option>
                                <option value="10" >10</option>';
                        }

                    ?>
                    </select>
                    </div>

                    <div class="form-group" id="div_bodega">
                        <?php
                    
                    for($i=0;$i<$bodega;$i++){
                        echo '<div class="form-group selectBod-container">  
                        <label for="">'.($i+1).'</label>
                        <select name="bodega'.($i+1).'" id="bodega'.($i+1).'" class="fs-bodSub fs-alterSelect" onchange="nivelBod('.($i+1).')">';

                            if($nivelesBod[$i] == "1"){
                            echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" selected>Nivel calle</option>
                                        <option value="2">-1</option>
                                        <option value="3">-2</option>
                                        <option value="4">-3</option>
                                        <option value="5">-4</option>
                                        <option value="6">-5</option>
                                        <option value="7">-6</option>
                                        <option value="8">No Tiene</option>								
                                        <option value="9">Otro</option>
                                    </select>'; 
                            }else if($nivelesBod[$i] == "2"){
                                echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" selected>-1</option>
                                        <option value="3">-2</option>
                                        <option value="4">-3</option>
                                        <option value="5">-4</option>
                                        <option value="6">-5</option>
                                        <option value="7">-6</option>
                                        <option value="8">No Tiene</option>								
                                        <option value="9">Otro</option>
                                    </select>'; 
                            }else if($nivelesBod[$i] == "3"){
                                echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" >-1</option>
                                        <option value="3" selected>-2</option>
                                        <option value="4">-3</option>
                                        <option value="5">-4</option>
                                        <option value="6">-5</option>
                                        <option value="7">-6</option>
                                        <option value="8">No Tiene</option>								
                                        <option value="9">Otro</option>
                                    </select>'; 
                            }else if($nivelesBod[$i] == "4"){
                                echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" >-1</option>
                                        <option value="3" >-2</option>
                                        <option value="4" selected>-3</option>
                                        <option value="5">-4</option>
                                        <option value="6">-5</option>
                                        <option value="7">-6</option>
                                        <option value="8">No Tiene</option>								
                                        <option value="9">Otro</option>
                                    </select>'; 
                            }else if($nivelesBod[$i] == "5"){
                                echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" >-1</option>
                                        <option value="3" >-2</option>
                                        <option value="4" >-3</option>
                                        <option value="5" selected>-4</option>
                                        <option value="6">-5</option>
                                        <option value="7">-6</option>
                                        <option value="8">No Tiene</option>								
                                        <option value="9">Otro</option>
                                    </select>'; 
                            }else if($nivelesBod[$i] == "6"){
                                echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" >-1</option>
                                        <option value="3" >-2</option>
                                        <option value="4" >-3</option>
                                        <option value="5" >-4</option>
                                        <option value="6" selected>-5</option>
                                        <option value="7">-6</option>
                                        <option value="8">No Tiene</option>								
                                        <option value="9">Otro</option>
                                    </select>'; 
                            }else if($nivelesBod[$i] == "7"){
                                echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" >-1</option>
                                        <option value="3" >-2</option>
                                        <option value="4" >-3</option>
                                        <option value="5" >-4</option>
                                        <option value="6" >-5</option>
                                        <option value="7" selected>-6</option>
                                        <option value="8">No Tiene</option>								
                                        <option value="9">Otro</option>
                                    </select>'; 
                            }else if($nivelesBod[$i] == "8"){
                                echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" >-1</option>
                                        <option value="3" >-2</option>
                                        <option value="4" >-3</option>
                                        <option value="5" >-4</option>
                                        <option value="6" >-5</option>
                                        <option value="7" >-6</option>
                                        <option value="8" selected>No Tiene</option>								
                                        <option value="9">Otro</option>
                                    </select>'; 
                            }else if($nivelesBod[$i] == "9"){
                                echo '<option value="" disabled="">Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" >-1</option>
                                        <option value="3" >-2</option>
                                        <option value="4" >-3</option>
                                        <option value="5" >-4</option>
                                        <option value="6" >-5</option>
                                        <option value="7" >-6</option>
                                        <option value="8" >No Tiene</option>								
                                        <option value="9" selected>Otro</option>
                                    </select>'; 
                            }else{
                                echo '<option value="" disabled="" selected>Bodega subterráneo</option>
                                        <option value="1" >Nivel calle</option>
                                        <option value="2" >-1</option>
                                        <option value="3" >-2</option>
                                        <option value="4" >-3</option>
                                        <option value="5" >-4</option>
                                        <option value="6" >-5</option>
                                        <option value="7" >-6</option>
                                        <option value="8" >No Tiene</option>								
                                        <option value="9" >Otro</option>
                                    </select>'; 
                            }
                        echo '<input class="fs-numBodSub fs-alterSelect" type="text" id="nBod'.($i+1).'" name="nBod'.($i+1).'" placeholder="N°" value="'.$numerosBod[$i].'">    
                        <input type="text" name="nivelBod'.($i+1).'" id="nivelBod'.($i+1).'" style="'.(($nivelesBod[$i] == "9")?'':'display:none').'" placeholder="Escriba el nivel de la bodega, Ej: 2" value="'.$otro_bodega[$i].'" >
                        
                        <hr>
                    </div>';
                    }
                    
                    ?>
                    </div>



                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion8" class="paginacion" onchange="paginacion(8)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8" disabled>Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (9/11)-->
                <fieldset id="9">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (9/11)</h3>

                    <label for="material_pisos_comunes" class="label_titulo">Material Pisos Espacio Comunes</label>
                    <div class="select-container">
                        <select name="material_pisos_comunes" id="material_pisos_comunes" class="fs-nDorm">
                        <option value="" disabled selected>Material Pisos Espacio Comunes</option>
                        <?php
                            $selectMaterial1 = "SELECT * FROM Materiales_pisos";
                            $result = $conn->query($selectMaterial1);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    if($material_piso_comun == $row["id_materiales_pisos"]){
                                        echo "<option value='".$row["id_materiales_pisos"]."' selected>".utf8_encode($row["material"])."</option>";  
                                    }else{
                                        echo "<option value='".$row["id_materiales_pisos"]."'>".utf8_encode($row["material"])."</option>";  
                                    }
                                    
                                }
                            } else {
                                
                            }
                        ?>
                    </select>
                    </div>

                    <label for="material_pisos_dorm" class="label_titulo">Material Pisos Dormitorios</label>
                    <div class="select-container">
                        <select name="material_pisos_dorm" id="material_pisos_dorm" class="fs-nDorm">
                        <option value="" disabled selected>Material Pisos Dormitorios</option>
                        <?php
                            $selectMaterial2 = "SELECT * FROM Materiales_pisos";
                            $result = $conn->query($selectMaterial2);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    if($material_piso_dorm == $row["id_materiales_pisos"]){
                                        echo "<option value='".$row["id_materiales_pisos"]."' selected>".utf8_encode($row["material"])."</option>";  
                                    }else{
                                        echo "<option value='".$row["id_materiales_pisos"]."'>".utf8_encode($row["material"])."</option>";  
                                    }
                                    
                                }
                            } else {
                                
                            }
                            ?>
                    </select>
                    </div>

                    <label for="material_pisos_bano" class="label_titulo">Material Piso Baños</label>
                    <div class="select-container">
                        <select name="material_pisos_bano" id="material_pisos_bano" class="fs-nDorm">
                        <option value="" disabled selected>Material Piso baños</option>
                        <?php
                            $selectMaterialBanno = "SELECT * FROM Material_piso_banno";
                            $result = $conn->query($selectMaterialBanno);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    if($material_piso_banno == $row["id_material_piso_banno"]){
                                        echo "<option value='".$row["id_material_piso_banno"]."' selected>".utf8_encode($row["material"])."</option>";  
                                    }else{
                                        echo "<option value='".$row["id_material_piso_banno"]."'>".utf8_encode($row["material"])."</option>";  
                                    }                                    
                                }
                            } else {
                                echo "<option>no hay datos</option>";
                            }
                            ?>
                    </select>
                    </div>

                    <label for="material_pisos_cocina" class="label_titulo">Material Piso Cocina</label>
                    <div class="select-container">
                        <select name="material_pisos_cocina" id="material_pisos_cocina" class="fs-nDorm">
                        <option value="" disabled selected>Material Piso Cocina</option>
                        <?php
                            $selectMaterial3 = "SELECT * FROM Materiales_pisos";
                            $result = $conn->query($selectMaterial3);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    if($material_pisos_cocina == $row["id_materiales_pisos"]){
                                        echo "<option value='".$row["id_materiales_pisos"]."' selected>".utf8_encode($row["material"])."</option>";  
                                    }else{
                                        echo "<option value='".$row["id_materiales_pisos"]."'>".utf8_encode($row["material"])."</option>";  
                                    }
                                    
                                }
                            } else {
                                
                            }
                            ?>
                    </select>
                    </div>

                    <label for="material_pisos_terraza" class="label_titulo">Material Piso Terraza</label>
                    <div class="select-container">
                        <select name="material_pisos_terraza" id="material_pisos_terraza" class="fs-nDorm">
                        <option value="" disabled selected>Material Piso Terraza</option>
                        <?php
                            $selectMaterial4 = "SELECT * FROM Materiales_pisos";
                            $result = $conn->query($selectMaterial4);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    if($material_pisos_terraza == $row["id_materiales_pisos"]){
                                        echo "<option value='".$row["id_materiales_pisos"]."' selected>".utf8_encode($row["material"])."</option>";  
                                    }else{
                                        echo "<option value='".$row["id_materiales_pisos"]."'>".utf8_encode($row["material"])."</option>";  
                                    }                                    
                                }
                            } else {
                                
                            }
                            ?>
                    </select>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion9" class="paginacion" onchange="paginacion(9)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9" disabled>Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 10 (10.1/11)-->
                <fieldset id="10">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (10.1/11) Cocina</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row">

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaIndep">
                                                    Cocina Independiente
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="cocinaIndep" name="cocinaIndep" <?= ($cocinaIndep == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaIntegrada">
                                                    Cocina integrada
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="cocinaIntegrada" name="cocinaIntegrada" <?= ($cocinaIntegrada == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaAme">
                                                    Cocina Americana
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="cocinaAme" name="cocinaAme" <?= ($cocina_ame == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaIsla">
                                                    Cocina Isla
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="cocinaIsla" name="cocinaIsla" <?= ($cocina_isla == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-12">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <hr class="hr-divisor-switch">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="encimeraElect">
                                                    Encimera Eléctrica
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="encimeraElect" name="encimeraElect" <?= ($encimeraElect == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="encimeraGas">
                                                    Encimera Gas
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="encimeraGas" name="encimeraGas" <?= ($encimeraGas == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="hornoEmp">
                                                    Horno Empotrado
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="hornoEmp" name="hornoEmp" <?= ($horno_emp == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="lavavajillas">
                                                    Lavavajillas empotrado
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="lavavajillas" name="lavavajillas" <?= ($lavavajillas == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="microondas">
                                                    Microondas empotrado
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="microondas" name="microondas" <?= ($microondas == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="refrigerador">
                                                    Refrigerador empotrado
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="refrigerador" name="refrigerador" <?= ($refrigerador == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-12">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <hr class="hr-divisor-switch">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="comedor">
                                                    Comedor de Diario
                                                </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                    <input type="checkbox" id="comedor" name="comedor" <?= ($comedor_diario == "1")? "checked": "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="patioServicio">
                                                        Patio de Servicio
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="patioServicio" name="patioServicio" <?= ($patio_serv == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="cocinaAmoblada">
                                                        Cocina amoblada
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="cocinaAmoblada" name="cocinaAmoblada">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="despensa">
                                                        Cuarto Despensa
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="despensa" name="despensa" <?= ($despensa == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="logia">
                                                        Logia
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="logia" name="logia" <?= ($logia == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion10" class="paginacion" onchange="paginacion(10)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10" disabled>Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (10.2/11)-->
                <fieldset id="11">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (10.2/11) Otros propiedad</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row">

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="roller">
                                                        Cortina Roller
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="roller" name="roller" <?= ($cortina_roller == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cortina_elec">
                                                        Cortinas Eléctricas
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="cortina_elec" name="cortina_elec" <?= ($cortina_elec == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="persiana_madera">
                                                        Persiana Madera
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="persiana_madera" name="persiana_madera" <?= ($persiana_madera == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="persiana_aluminio">
                                                        Persiana Aluminio
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="persiana_aluminio" name="persiana_aluminio" <?= ($persiana_aluminio == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-12">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <hr class="hr-divisor-switch">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="citofono">
                                                        Citófono
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="citofono" name="citofono" <?= ($citofono == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="alarma">
                                                        Alarma
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="alarma" name="alarma" <?= ($alarma == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="aire">
                                                        Aire acondicionado
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="aire" name="aire" <?= ($aire_acondicionado == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="termoPanel">
                                                        Termopanel
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="termoPanel" name="termoPanel" <?= ($term_panel == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-12">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <hr class="hr-divisor-switch">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="escritorio">
                                                        Escritorio
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="escritorio" name="escritorio" <?= ($escritorio == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_estar">
                                                        Sala de Estar
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="sala_estar" name="sala_estar" <?= ($sala_estar == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_juegos">
                                                        Sala de Juegos
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="sala_juegos" name="sala_juegos" <?= ($sala_juegos == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="planchado">
                                                        Sala de Planchado
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="planchado" name="planchado" <?= ($planchado == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="bar">
                                                        Bar
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="bar" name="bar" <?= ($bar == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cavaVinos">
                                                        Cava de Vinos
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="cavaVinos" name="cavaVinos" <?= ($cava_vinos == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-12">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <hr class="hr-divisor-switch">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sauna">
                                                        Sauna
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="sauna" name="sauna" <?= ($sauna == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="jacuzzi">
                                                        Jacuzzi
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="jacuzzi" name="jacuzzi" <?= ($jacuzzi == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 casa">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="atico">
                                                        Ático
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="atico" name="atico" <?= ($atico == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="hall">
                                                        Hall de acceso
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="hall" name="hall" <?= ($hall == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="hangaroa">
                                                        Cortina Hangaroa
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="hangaroa" name="hangaroa">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                        <!-- <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="terraza">
                                                        Malla Protección Terraza
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="terraza" name="terraza">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="cerco_electrico">
                                                        Cerco eléctrico
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="cerco_electrico" name="cerco_electrico">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                        <!-- <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="ascensor">
                                                        Ascensor privado
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="ascensor" name="ascensor">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="sala_cine">
                                                        Sala de cine
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="sala_cine" name="sala_cine">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion11" class="paginacion" onchange="paginacion(11)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11" disabled>Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (10.3/11)-->
                <fieldset id="12">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle" id="10.3">Paso 3 (10.3/11) Exterior</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row" id="exterior">

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="atejardin">
                                                        Antejardín
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="atejardin" name="atejardin" <?= ($antejardin == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 casa">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="riego">
                                                        Riego Automático
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="riego" name="riego" <?= ($riego_auto == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="piscina">
                                                        Piscina
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="piscina" name="piscina" <?= ($piscina == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="piscina_temp">
                                                        Piscina Climatizada
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="piscina_temp" name="piscina_temp" <?= ($piscina_temp == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="quincho">
                                                        Quincho
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="quincho" name="quincho" <?= ($quincho == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="panelSolar">
                                                        Panel Solar
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="panelSolar" name="panelSolar" <?= ($panelSolar == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="gimnasio">
                                                        Gimnasio
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="gimnasio" name="gimnasio">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="azotea">
                                                        Azotea habilitada
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="azotea" name="azotea">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="lavanderia">
                                                        Lavandería Edificio
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="lavanderia" name="lavanderia">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="gourmet">
                                                        Gourmet room
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="gourmet" name="gourmet">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion12" class="paginacion" onchange="paginacion(12)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor y Operario</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Pisos y ascensores</option>
                        <option value="6">Rol y Precios anexos</option>
                        <option value="7">Gasto común y otros</option>
                        <option value="8">Superficie y orientación</option>
                        <option value="9">Dormitorios y Baños</option>
                        <option value="10">Estacionamiento y Bodegas</option>
                        <option value="11">Material pisos</option>
                        <option value="12" disabled>Info Cocina</option>
                        <option value="13">Info Propiedad</option>
                        <option value="15">A. Comunes</option>
                        <option value="16">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (10.4/11)-->
                <fieldset id="13">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (10.4/11) Áreas comunes</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row" id="aComunes">

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="conserjeria">
                                                        Conserjería 24Hrs
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="conserjeria" name="conserjeria" <?= ($conserje_24 == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="accesos_controlados">
                                                        Accesos Controlados
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="accesos_controlados" name="accesos_controlados" <?= ($accesos_controlados == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="circuito">
                                                        Circuito Cerrado de TV
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="circuito" name="circuito" <?= ($circ_tv == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 ">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="est_visita">
                                                        Estac. de Visita
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="est_visita" name="est_visita" <?= ($est_visita == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-12">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <hr class="hr-divisor-switch">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 ">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="plaza">
                                                        Plaza
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="plaza" name="plaza" <?= ($plaza == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 ">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="juegos_infantiles">
                                                        Juegos Infantiles
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="juegos_infantiles" name="juegos_infantiles" <?= ($juegos == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 ">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cancha_futbol">
                                                        Cancha de Futbol
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="cancha_futbol" name="cancha_futbol" <?= ($cancha_futbol == "1")? "checked": "" ?>> 
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 ">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cancha_tenis">
                                                        Cancha de Tenis
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="cancha_tenis" name="cancha_tenis" <?= ($cancha_tenis == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 ">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cancha_golf">
                                                        Cancha de Golf
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="cancha_golf" name="cancha_golf" <?= ($cancha_golf == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 ">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_juegos_ext">
                                                        Sala de Juegos
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="sala_juegos_ext" name="sala_juegos_ext" <?= ($sala_juegos_ext == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="areas_verdes">
                                                        Áreas verdes
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="areas_verdes" name="areas_verdes" <?= ($area_verde == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_multiuso">
                                                        Sala Multiuso
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="sala_multiuso" name="sala_multiuso" <?= ($sala_multi_uso == "1")? "checked": "" ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="sala_reuniones">
                                                        Sala de reuniones
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="sala_reuniones" name="sala_reuniones">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="porton_elec">
                                                        Portón Eléctrico
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="porton_elec" name="porton_elec">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion13" class="paginacion" onchange="paginacion(13)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13" disabled>A. Comunes</option>
                        <option value="14">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (11/11)-->
                <fieldset id="14">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (11/11)</h3>

                    <label for="disponibilidad" class="label_titulo">Disponibilidad Visitas</label>
                    <input class="fs-contribucion" type="text" name="disponibilidad" name="disponibilidad" placeholder="Disponibilidad visitas [Ej. L-V 11:00-17:00]" value="<?= $disponibilidad_visita ?>" />

                    <label for="info_propiedad" class="label_titulo">Disponibilidad de Entrega</label>
                    <div class="select-container ">
                        <select name="info_propiedad" id="info_propiedad" class="fs-nBan" onchange="disponibilidadF()">
                        <option value="" disabled>Disponibilidad de entrega</option>
                        <option value="Inmediata"<?=($disponibilidad_entrega=="Inmediata")?'selected':''?>>Inmediata</option>
                        <option value="1 semana"<?=($disponibilidad_entrega=="1 semana")?'selected':''?>>1 semana</option>
                        <option value="2 semana"<?=($disponibilidad_entrega=="2 semana")?'selected':''?>>2 semanas</option>
                        <option value="3 semanas"<?=($disponibilidad_entrega=="3 semanas")?'selected':''?>>3 semanas</option>
                        <option value="1 mes"<?=($disponibilidad_entrega=="1 mes")?'selected':''?>>1 mes</option>
                        <option value="+1 mes"<?=($disponibilidad_entrega=="+1 mes")?'selected':''?>>+1 mes</option>
                        <option value="Otro"<?=($disponibilidad_entrega=="Otro")?'selected':''?>>Otro</option>
                    </select>
                    </div>
                    <input class="fs-contribucion" type="date" style="display:none" id="fecha_entrega" name="fecha_entrega" value="<?=$fecha_entrega?>" />

                    <label for="nota" class="label_titulo">Notas</label>
                    <textarea name="nota" id="nota" cols="30" rows="4" placeholder="Notas" maxlength="1000"><?= $notas ?></textarea>

                    <label for="propDestacado" class="label_titulo">Destacado</label>
                    <textarea name="propDestacado" id="propDestacado" cols="30" rows="4" placeholder="¿Algo que quieras destacar de la propiedad?" maxlength="1000"><?= $propDestacado ?></textarea>

                    <label for="url" class="label_titulo">URL</label>
                    <input class="fs-contribucion" type="text" id="url" name="url" placeholder="URL" value="<?= $url ?>" />

                    <label for="tour3d" class="label_titulo">Tour Virtual</label>
                    <input class="fs-contribucion" type="text" id="tour3d" name="tour3d" placeholder="URL del Tour Virtual" value="<?= $tour3d ?>" />
                    
                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td">Cartel</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                <input type="checkbox" id="cartel" name="cartel" <?= ($cartel == "1")? "checked": "" ?>>
                                <span class="slider round"></span>
                            </label>
                            </td>
                            <td class="fs-td">Llave</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                <input type="checkbox" id="llave" name="llave" <?= ($llave == "1")? "checked": "" ?>>
                                <span class="slider round"></span>
                            </label>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <label for="imagen">Imagen Propiedad:</label>
                        <input type="file" name="imagen" id="imagen"></div>
                        <input type="submit" name="save" class=" action-button" value="Guardar" />
                    <input type="button" name="quit" id="quit" class="action-button" value="Finalizar" />
                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion14" class="paginacion" onchange="paginacion(14)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Propiedad</option>
                        <option value="4">Dirección</option>
                        <option value="5">Gasto común y Calefacción</option>
                        <option value="6">Superficie y orientación</option>
                        <option value="7">Dormitorios y Baños</option>
                        <option value="8">Estacionamiento y Bodegas</option>
                        <option value="9">Material pisos</option>
                        <option value="10">Info Cocina</option>
                        <option value="11">Otros Propiedad</option>
                        <option value="12">Exterior</option>
                        <option value="13">A. Comunes</option>
                        <option value="14" disabled>Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="save previous btn-matchBlue action-button mr-0" value="Anterior" />

                </fieldset>

            </form>
        </div>
    </section>
    <?
    include '../pages/footer.php';
    ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>
        <script src="../js/toastr.js"></script>
        <script src="../js/plantillas/casa/formularioCasaArr.js"></script>
        <script src="../js/validaciones/requerido.js"></script>
        <script src="../js/autoguardado.js"></script>

        <script>
            // var area = document.getElementById("select_area");
            // if(area.value == "null"){
            //     alert("area vacía");
            //     area.focus();
            // }
        </script>
        <script>
            $(document).ready(function() {
                selectMail();
                cambioOrigen();
                amobla2();
                noExclusividad();
                disponibilidadF();
                condominioF();
                autoguardado();
                cambioRegion();
            });
        </script>
        </body>

        </html>