<?php
include '../conexion.php';
$idForm = $_GET["id"];
$idDirec = $_GET["idDirec"];
$idPer = $_GET["idPer"];
$idProp = $_GET["idProp"];
$idPropiedad = $_GET["idPropiedad"];
?>

    <section>
        <div class="container">
            <form action="" id="formC">

                <input type="hidden" id="idPer" name="idPer" value="<?echo $idPer?>">
                <input type="hidden" id="idProp" name="idProp" value="<?echo $idProp?>">
                <input type="hidden" id="idForm" name="idForm" value="<?echo $idForm?>">
                <input type="hidden" id="idPropiedad" name="idPropiedad" value="<?echo $idPropiedad?>">
                <input type="hidden" id="idDirec" name="idDirec" value="<?echo $idDirec?>">
                <input type="hidden" id="operacion" name="operacion" value="2">
                <input type="hidden" id="tipoProp" name="tipoProp" value="4">

                <!-- progressbar -->
                <ul id="progressBar">
                    <li class="active">Corredor</li>
                    <li>Propietario</li>
                    <li>Propiedad</li>
                </ul>

                <div class="container">
                    <div class="row">
                        <div class="column col-12">

                        </div>
                    </div>
                </div>
                <!-- fieldset 1 -->
                <fieldset id="1">

                    <h2 class="fs-title">Información Corredor</h2>
                    <h3 class="fs-subtitle">Paso 1</h3>


                    <div class="select-container">
                        <select name="select_area" id="select_area" class="dropdown">
                        <option value="null" disabled selected>Área Redflip*</option>
                        <option value="Redflip Habitacional">Redflip Habitacional</option>
                        <option value="Redflip Comercial">Redflip Comercial</option>
                        <option value="Redflip Luxury">Redflip Luxury</option>
                    </select>
                    </div>

                    <div class="select-container">
                        <select name="corredor" id="corredor" onchange="selectMail()">
                        <option value="null" disabled selected>Nombre Corredor*</option>
                        <?
                        $sqlCorredor="Select Corredor.id_corredor, Persona.nombre, Persona.apellido, Persona.correo from Corredor, Persona where Corredor.fk_persona = Persona.id_persona";
                        $result = $conn->query($sqlCorredor);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo "<option value=".$row["id_corredor"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                            }
                        }
                        ?>
                    </select>
                    </div>

                    <input type="text" id="mail" value="" disabled>
                    <div class="select-container">
                        <select name="operario" id="operario">
                        <option value="null" disabled selected>Operario cámara*</option>
                        <?php
                        $sqlOperario = "Select Operario.id_operario, Persona.nombre, Persona.apellido from Operario, Persona where Operario.fk_persona = Persona.id_persona order by id_operario";
                        $result = $conn->query($sqlOperario);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo "<option value=".$row["id_operario"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                            }
                        }
                        ?>
                    </select>
                    </div>


                    <input type="submit" name="save" class=" action-button" value="Guardar" />
                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion1" class="paginacion paginacionIF" onchange="paginacion(1)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1" disabled>Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>

                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 2 -->
                <fieldset id="2">
                    <h2 class="fs-title">Información Propietario</h2>
                    <h3 class="fs-subtitle">Paso 2</h3>

                    <input class="fs-nom" type="text" name="nombre" placeholder="Nombre*" id="nombre" onfocusout="validarNombre()" />
                    <input class="fs-ape" type="text" name="apellido" placeholder="Apellido*" id="apellido" onfocusout="validarApellido()" />
                    <input class="fs-rut" type="text" name="rut" placeholder="Rut" id="rut" onfocusout="validarRut2()" />
                    <div class="select-container">
                        <select name="origen" id="origen" class="fs-ori" onchange="cambioOrigen()">
                            <option value="" disabled selected>Origen*</option>
                            <option value="17" >Captado por Redflip</option>
                            <option value="7" >Referido Corredor</option>
                            <option value="6" >Referido Cliente</option>
                            <option value="5" >Referido Conserje</option>
                            <option value="18">Partner Redflip</option>                        
                    </select>
                    </div>
                    <div class="select-container">
                        <select name="captadaR" id="captadaR" class="fs-ori" style="display:none">
                            <option value="" disabled selected>Seleccione opción*</option>
                            <option value="9" >Base de Datos</option>
                            <option value="19" >Google</option>
                            <option value="1" >Instagram</option>
                            <option value="2" >Facebook</option>
                                                    
                    </select>
                    </div>
                    <div class="select-container">
                        <select name="partnerR" id="partnerR" class="fs-ori" style="display:none">
                            <option value="" disabled selected>Seleccione opción*</option>
                            <option value="11" >Doorlis</option>
                            <option value="20" >De Carlo</option>
                            <option value="12" >OpenCasa</option>
                            <option value="21" >UHomie</option>
                            <option value="13" >GI0</option>
                            <option value="2" >Stoffel & Valdivieso</option>
                            <option value="15" >Otro</option>
                                                    
                    </select>
                    </div>
                    <div class="form-group">
                        <div class="selectDivisa-container">
                            <select name="sub_fono" id="sub_fono" class="fs-suFono">
                            <option value="" disabled selected>Prefijo*</option>
                            <option value="+569">+569</option>
                            <option value="+562">+562</option>
                            <option value="+568">+568</option>
                            <option value="+56">+56</option>
                        </select>
                        </div>
                        <input class="fs-fono" type="tel" name="telefono" id="telefono" placeholder="Teléfono*" onfocusout="validarTelefono()" />
                    </div>

                    <input type="mail" name="correo" id="correo" placeholder="Correo*" onfocusout="validarCorreo()" />
                    <label class="txtProp">Fecha</label>
                    <input type="date" name="fecha" id="fecha" placeholder="Fecha" />

                    <hr>
                    <h3 class="fs-subtitle">Contactos Adicionales</h3>

                    <div class="divC">
                        <ol>
                            <div id="contactos">

                            </div>
                        </ol>


                        <a class="addC" id="addC" href="#" onclick="agregarContacto(); return false;"> Agregar contacto <i class="fas fa-plus-circle"></i></a>
                        <a class="delC" id="delC" href="#" onclick="eliminarContacto(); return false;"> Eliminar contacto <i class="fas fa-minus-circle"></i></a>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion2" class="paginacion" onchange="paginacion(2)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2" disabled>Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (1/9)-->
                <fieldset id="3">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (1/9)</h3>

                    <div id="valor1" style="">
                        <input class="fs-valor fs-tNumb" type="text" name="monto1" id="monto1" placeholder="Valor Arriendo" style="width:75%!important" onfocusout="validarPrecio('monto1')" />

                        <select name="divisaMonto1" id="divisaMonto1" class="fs-divisa">
                            <option value="" disabled selected>Divisa</option>
                            <option value="1" >UF</option>
                            <option value="2" >CLP</option>
                        </select>
                    </div>
                    <!-- <div id="valor2" style="display:none">
                    <input class="fs-valor fs-tNumb" type="text" name="monto2" id="monto2" placeholder="Monto2" style="width:75%!important" onfocusout="validarPrecio('monto2')"/>
                    <select name="divisaMonto2" id="divisaMonto2" class="fs-divisa">
                            <option value="" disabled selected>Divisa</option>
                            <option value="1" >UF</option>
                            <option value="2" >CLP</option>
                        </select>
                </div> -->

                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">Amoblado</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                <input type="checkbox" id="amoblado" name="amoblado" onchange="amobla2()">
                                <span class="slider round"></span>
                            </label>
                            </div>
                            <input class="fs-valor fs-tNumb" type="text" name="valorAmob" id="valorAmob" placeholder="Valor de diferencia amoblado" style="display:none" onfocusout="validarPrecio('valorAmob')" />
                        </div>
                    </div>

                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">No exclusivo</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                <input type="checkbox" id="exclusividad" name="exclusividad" onchange="noExclusividad()">
                                <span class="slider round"></span>
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="pregExclusividad">

                        <div id="scCantCorredor" class="">
                            <select name="cantCorredor" id="cantCorredor" class="fs-tipoOpe" style="display:none;">
                            <option value="null" disabled selected>Cant. de corredores</option>
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                            <option value="3" >3</option>
                            <option value="4" >4</option>
                            <option value="5" >5</option>
                            <option value="6" >6</option>
                        </select>
                        </div>

                        <div id="scTiempoPublicacion" class="">
                            <select name="select_tiempoPublicacion" id="select_tiempoPublicacion" class="fs-tipoOpe" style="display:none">
                            <option value="null" disabled selected>Tiempo de publicación</option>
                            <option value="1" >1 Semana</option>
                            <option value="2" >2 Semanas</option>
                            <option value="3" >3 Semanas</option>
                            <option value="4" >1 Mes</option>
                            <option value="5" >2 Meses</option>
                            <option value="6" >3 Meses</option>
                            <option value="7" >4 Meses</option>
                            <option value="8" >5 Meses</option>
                            <option value="9" >6 Meses</option>
                            <option value="10">+6 Meses</option>
                        </select>
                        </div>
                    </div>

                    <textarea name="infoPropVenta" id="infoPropVenta" cols="30" rows="3" placeholder="¿Por qué quieres vender o arrendar?" maxlength="1000"></textarea>
                    <!-- <div id="valorArr" style="display:none">
                
                    <input class="fs-valor fs-tNumb" type="text" name="valorCLP" id="valorCLP" placeholder="Valor CLP" style="width:100%!important"  onfocusout="validarPrecio('valorCLP')"/>
                    
                </div> -->

                    <!-- <div id="valorAmbos" style="display:none">
                    <input class="fs-valor fs-tNumb" type="text" name="precioVent" id="precioVent" placeholder="Valor Venta"  onfocusout="validarPrecio('precioVent')"/>
                    <div class="selectDivisa-container">
                        <select name="divisaVenta" id="divisaVenta" class="fs-divisa">
                            <option value="" disabled selected>Divisa</option>
                            <option value="1" >UF</option>
                            <option value="2" >CLP</option>
                        </select>
                    </div>

                    <input class="fs-valor fs-tNumb" type="text" name="precioArr" id="precioArr" placeholder="Valor Arriendo"  onfocusout="validarPrecio('precioArr')"/>
                    <div class="selectDivisa-container">
                        <select name="divisaArriendo" id="divisaArriendo" class="fs-divisa">
                            <option value="" disabled selected>Divisa</option>
                            <option value="1" >UF</option>
                            <option value="2" >CLP</option>
                        </select>
                    </div>
                </div> -->

                    <!-- <select name="divisa" id="divisa" class="fs-vaDivisa">
                            <option value="" disabled selected>Divisa</option>
                            <?
                            // $sqlDiv = "Select * from Divisa";
                            // $result = $conn->query($sqlDiv);
                            // if ($result->num_rows > 0) {
                            //     // output data of each row
                            //     while($row = $result->fetch_assoc()) {
                            //         echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                            //     }
                            // } 
                            ?>
                        </select> -->


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion3" class="paginacion" onchange="paginacion(3)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3" disabled>Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (2/9)-->
                <fieldset id="4">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (2/9)</h3>
                    <div class="form-group">
                        <input class="fs-calle" type="text" name="calle" placeholder="Calle*" id="calle" />
                        <input class="fs-numb fs-tNumb" type="number" name="numero" placeholder="Número*" id="numero" />
                        <input class="fs-letra" type="text" name="letra" placeholder="Of*" id="letra" />
                    </div>

                    <!-- <input class="fs-condominio" type="text" name="condominio" placeholder="Condominio" /> -->

                    <div class="selectCom-container">
                        <select name="comuna" id="comuna" class="fs-comuna">
                                <option value="null" disabled selected>Comuna*</option>
                                <?php
                                $sqlComuna = "Select * from Comuna order by nombre asc";
                                $result = $conn->query($sqlComuna);
                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                        }
                                    } 
                                ?>
                                </select>
                        <!-- <input type="text" style="width: 49%" name="numPiso" placeholder="Piso N°"> -->
                    </div>
                    <br>
                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td2" id="switchCond1" style="display: none">Condominio</td class="fs-td">
                            <td class="fs-td3" id="switchCond2" style="display: none">
                                <label class="switch" id="switchCond3" style="display: none">
                                        <input type="checkbox" id="condominio" name="condominio" onchange="condominioF()">
                                        <span class="slider round" id="switchCond4" style="display: none"></span>
                                    </label>
                            </td>
                            <td class="fs-td2" id="switchLot1" style="display: none">Loteo</td class="fs-td">
                            <td class="fs-td3" id="switchLot2" style="display: none">
                                <label class="switch" id="switchLot3" style="display: none">
                                        <input type="checkbox" id="loteo" name="loteo" onchange="loteoF()">
                                        <span class="slider round" id="switchLot4" style="display: none"></span>
                                    </label>
                            </td>


                        </tr>
                    </table>

                    <input class="fs-referencia" type="text" name="referencia" id="referencia" placeholder="Referencia" />

                    <div class="form-group" style="display: none" id="divCondominio">
                        <input class="fs-calle" type="text" name="calleCond" placeholder="Calle" id="calleCond" />
                        <input class="fs-numb fs-tNumb" type="number" name="numeroCond" placeholder="Número" id="numeroCond" />
                    </div>

                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion4" class="paginacion" onchange="paginacion(4)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4" disabled>Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (3/9)-->
                <fieldset id="5">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (3/9)</h3>

                    <div class="form-group">
                        <input class="fs-construccion fs-tNumb" type="number" name="construccion" id="construccion" placeholder="Año construcción" onfocusout="validarAno('construccion')" />
                    </div>

                    <input class="fs-gastoC fs-tNumb" type="text" name="gastoC" id="gastoC" placeholder="Valor Gasto común" onfocusout="validarPrecio('gastoC')" />
                    <textarea name="gastos_comun" id="gastos_comun" cols="30" rows="3" placeholder="¿Qué Incluyen los Gastos Comunes?" maxlength="1000"></textarea>
                    <select name="edificioClase" id="edificioClase">
                        <option value="" selected>Edificio Clase</option>
                        <option value="Clase A+">Clase A+</option>
                        <option value="Clase A">Clase A</option>
                        <option value="Clase B">Clase B</option>
                    </select>

                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion5" class="paginacion" onchange="paginacion(5)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5" disabled>Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>
                <!-- fieldset 3 (4/9)-->
                <fieldset id="6">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (4/9)</h3>
                    <!-- <div class="form-group">
                        <input class="fs-construccion fs-tNumb" type="number" name="construccion" id="construccion" placeholder="Año construcción" onfocusout="validarAno('construccion')"/>
                    </div> -->
                    <div class="select-container">
                        <select name="calefaccion" id="calefaccion" class="fs-nDorm" onchange="fnCalefa()">
                        <option value="" disabled selected>Tipo de calefacción</option>
                            <?php
                                $sqlCalefa = "SELECT * FROM Tipo_calefaccion where id not in (3,9,6,5) order by tipo";
                                $result = $conn->query($sqlCalefa);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo '<option value="'.$row["id"].'">'.utf8_encode($row["tipo"]).'</option>';
                                    }
                                }
                            ?>
                        </select>
                        <input type="text" name="otroCalefa" id="otroCalefa" style="display:none" placeholder="Indique la calefacción">
                    </div>

                    <div class="select-container">
                        <select name="tipoGas" id="tipoGas" class="fs-nDorm">
                            <option value="" disabled selected>Tipo de gas</option>
                            <?php
                                $sqlCalefa = "SELECT * FROM Tipo_gas where id_tipo_gas not in (3) order by tipo";
                                $result = $conn->query($sqlCalefa);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo '<option value="'.$row["id_tipo_gas"].'">'.utf8_encode($row["tipo"]).'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>

                    <div class="select-container">
                        <select name="aguaCaliente" id="aguaCaliente" class="fs-nDorm" onchange="fnAguaCaliente()">
                        <option value="" disabled selected>Agua caliente</option>
                        <?php
                        $sql = "SELECT * from Agua_caliente order by tipo";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo "<option value='".$row["id_agua_caliente"]."'>".$row["tipo"]."</option>";
                            }
                        }
                        
                        ?>
                    </select>
                        <input type="text" name="otroAguaCaliente" id="otroAguaCaliente" placeholder="Indique tipo de agua caliente" style="display:none">
                    </div>

                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion6" class="paginacion" onchange="paginacion(6)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6" disabled>Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (5/9)-->
                <fieldset id="7">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (5/9)</h3>

                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td3"><input class="fs-supT fs-tNumb" type="text" name="supT" id="supT" placeholder="m2 total" onfocusout="validarPrecio('supT')" /></td>
                            <td class="fs-td3"><input class="fs-supU fs-tNumb" type="text" name="supU" id="supU" placeholder="m2 útil" onfocusout="validarPrecio('supU')" /></td>
                        </tr>

                        <tr class="fs-tr">
                            <td class="fs-td3"><input class="fs-supTerz fs-tNumb" type="text" name="supTerz" id="supTerz" placeholder="m2 terraza" onfocusout="validarPrecio('supTerz')" /></td>
                            <!-- Dinámico -->
                            <!-- Cuando es dpto -->
                            <td class="fs-td3" id="supPatTd" style="display:none"><input class="fs-supTerr fs-tNumb" type="number" name="supPatio" id="supPatio" onfocusout="validarPrecio('supPatio')" placeholder="m2 patio" /></td>
                        </tr>
                    </table>
                    <!-- Si se escribe en sup. Patio, debe aparecer -->
                    <div class="column col-12" id="usoGoceDiv" style="display:none">
                        <div class="fluid-container">
                            <div class="row">
                                <div class="column col-6">
                                    <label for="usoGoce">
                                    Uso y goce
                                </label>
                                </div>
                                <div class="column col-6">
                                    <label class="switch">
                                    <input type="checkbox" id="usoGoce" name="usoGoce">
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin dinamico -->
                    <!-- <input class="fs-pDpto fs-tNumb" type="number" name="CantPisosCasa" id="CantPisosCasa" placeholder="Cant. de pisos casa" style="display:none"/> -->

                    <!-- <div class="form-group">
                        <div class="select-container">
                            <select name="orientacion" id="orientacion" class="fs-ori">
                                <option value="" disabled selected>Orientación</option>
                                <option value="Norte">Norte</option>
                                <option value="Sur">Sur</option>
                                <option value="Oriente">Oriente</option>
                                <option value="Poniente">Poniente</option>
                                <option value="N-O">N-O</option>
                                <option value="N-P">N-P</option>
                                <option value="S-O">S-O</option>
                                <option value="S-P">S-P</option>
                            </select>
                        </div>
                    </div> -->

                    <div class="container">
                        <div class="row">
                            <div class="column col-4">
                                <label class="txtProp">
                                    Orientación
                                </label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">N
                                    <input type="checkbox" id="norte" name="norte" onchange="">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">S
                                    <input type="checkbox" id="sur" name="sur" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">O
                                    <input type="checkbox" id="oriente" name="oriente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">P
                                    <input type="checkbox" id="poniente" name="poniente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">NO
                                    <input type="checkbox" id="nOriente" name="nOriente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">NP
                                    <input type="checkbox" id="nPoniente" name="nPoniente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column col-4">
                                <label for=""></label>
                            </div>

                            <div class="column col-4">
                                <label class="fs-chkLbl">SO
                                    <input type="checkbox" id="sOriente" name="sOriente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">SP
                                    <input type="checkbox" id="sPoniente" name="sPoniente" onchange="" >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion7" class="paginacion" onchange="paginacion(7)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7" disabled>Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (6/9) -->
                <fieldset id="8">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (6/9)</h3>

                    <div>
                        <select name="plantasLibres" id="plantasLibres">
                    <option value="" selected>Plantas Libres</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    </select>
                    </div>
                    <div>
                        <select name="privados" id="privados">
                    <option value="" selected>Privados</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    </select>
                    </div>
                    <div>
                        <select name="salaReu" id="salaReu">
                    <option value="" selected>Sala de Reuniones</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    </select>
                    </div>
                    <div>
                        <select name="bannos" id="bannos">
                    <option value="" selected>Baños</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    </select>
                    </div>
                    <input type="text" name="puestosTrab" id="puestosTrab" placeholder="Puestos de trabajo totales" onfocusout="validarPrecio('puestosTrab')">

                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion8" class="paginacion" onchange="paginacion(8)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8" disabled>Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (7/9)-->
                <fieldset id="9">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (7/9)</h3>
                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row">

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="recepcion">
                                                        Recepción
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="recepcion" name="recepcion">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaIndep">
                                                        <!-- espacio blanco, se usa para bajar los otros 2 switches-->
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <!-- <input type="checkbox" id="kitchenette" name="kitchenette"> -->
                                                        <!-- <span class="slider round"></span> -->
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="kitchenette">
                                                        Kitchenette
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="kitchenette" name="kitchenette">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocina">
                                                        Cocina
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="cocina" name="cocina">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="select-container">
                        <select name="select_estacionamientos" id="select_estacionamientos" class="fs-nEst" onchange="estacionamientos()">
                        <option value="" disabled selected>Cantidad de estacionamientos</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="0">No tiene</option>
                    </select>
                    </div>

                    <div class="form-group" id="estacionamientos">

                    </div>



                    <div class="select-container">
                        <select name="select_bodega" id="select_bodega" class="fs-nEst" onchange="bodegas()">
                        <option value="" disabled selected>Cantidad de bodega(s)</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="0">No tiene</option>
                    </select>
                    </div>

                    <div class="form-group" id="div_bodega">

                    </div>


                    <input type="submit" name="save" class=" action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion9" class="paginacion" onchange="paginacion(9)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9" disabled>Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (8.1/9)-->
                <fieldset id="10">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (8.1/9) Otros propiedad</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row">

                                        <!-- <div class="column col-6 casa" >
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="atico">
                                                        Ático
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="atico" name="atico">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="redTel">
                                                        Red de Telefonía
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="redTel" name="redTel">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="redCompu">
                                                        Red de Computadores
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="redCompu" name="redCompu">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="iluminacionLed">
                                                        Iluminación LED
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="iluminacionLed" name="iluminacionLed">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="redIncendio">
                                                        Red de Incendio
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="redIncendio" name="redIncendio">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="citofono">
                                                        Citófono
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="citofono" name="citofono">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="alarma">
                                                        Alarma
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="alarma" name="alarma">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="climaVRV">
                                                        Climatización VRV
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="climaVRV" name="climaVRV">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="aire">
                                                        Aire Acondicionado
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="aire" name="aire">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-12">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <!-- <hr class="hr-divisor-switch"> -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="puertaMag">
                                                        Puerta Magnetica
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="puertaMag" name="puertaMag">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="sala_estar">
                                                        Sala de Estar
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="sala_estar" name="sala_estar">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="despensa">
                                                        Cuarto Despensa
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="despensa" name="despensa">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="planchado">
                                                        Cuarto de planchado
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="planchado" name="planchado">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="terraza_quincho">
                                                        Terraza con Quincho
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="terraza_quincho" name="terraza_quincho">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="bar">
                                                        Bar
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="bar" name="bar">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="jacuzzi">
                                                        Jacuzzi
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="jacuzzi" name="jacuzzi">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="ascensor">
                                                        Ascensor privado
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="ascensor" name="ascensor">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-12">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <hr class="hr-divisor-switch">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="duplex">
                                                        Dúplex
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="duplex" name="duplex">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="triplex">
                                                        Tríplex
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="triplex" name="triplex">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="mariposa">
                                                        Mariposa
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="mariposa" name="mariposa">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="penthouse">
                                                        Penthouse
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="penthouse" name="penthouse">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                        <!-- <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="termoPanel">
                                                        Termo Panel
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="termoPanel" name="termoPanel">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    
                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="cerco_electrico">
                                                        Cerco eléctrico
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="cerco_electrico" name="cerco_electrico">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="hall">
                                                        Hall de acceso
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="hall" name="hall">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="sala_cine">
                                                        Sala de cine
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="sala_cine" name="sala_cine">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                        -->


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="submit" name="save" class="action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion10" class="paginacion" onchange="paginacion(10)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10" disabled>Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (8.2/9)-->
                <fieldset id="11">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle" id="8.2">Paso 3 (8.2/9) Áreas Comunes</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row" id="exterior">

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="accesoCont">
                                                        Accesos Controlados
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="accesoCont" name="accesoCont">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6 pl-0">
                                                        <label for="circuito">
                                                        CCTV
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="circuito" name="circuito">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Preguntar porque esta 2 veces -->

                                        <!-- <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="gimnasio">
                                                        Recepción
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="recepcion" name="recepcion">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6 pl-0">
                                                        <label for="estArriendo">
                                                        Estacionamientos Arriendo
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="estArriendo" name="estArriendo">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="estClientesOf">
                                                        Estacionamiento Clientes
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="estClientesOf" name="estClientesOf">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6 pl-0">
                                                        <label for="bicicletero">
                                                        Bicicletero
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="bicicletero" name="bicicletero">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="casino">
                                                        Casino
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="casino" name="casino">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6 pl-0">
                                                        <label for="cafeteria">
                                                        Cafeteria
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="cafeteria" name="cafeteria">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="maquinasDisp">
                                                        Maquinas Dispensadoras
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="maquinasDisp" name="maquinasDisp">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 dpto">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6 pl-0">
                                                        <label for="salasReuSwitch">
                                                        Salas de Reuniones
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="salasReu" name="salasReu">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 dpto">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="salaCapa">
                                                        Sala de Capacitación
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="salaCapa" name="salaCapa">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6 dpto">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6 pl-0">
                                                        <label for="auditorio">
                                                        Auditorio
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="auditorio" name="auditorio">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="gimnasio">
                                                        Gimnasio
                                                    </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                        <input type="checkbox" id="gimnasio" name="gimnasio">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="column col-12">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <hr class="hr-divisor-switch">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 dpto">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="ascensor_comun">
                                                        Ascensor
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="ascensor_comun" name="ascensor_comun">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="conserjeria">
                                                        Conserjería 24Hrs
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="conserjeria" name="conserjeria">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 ">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="est_visita">
                                                        Estac. de Visita
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="est_visita" name="est_visita">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6 ">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="bicicletero">
                                                        Bicicletero
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="bicicletero" name="bicicletero">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                        <!-- <div class="column col-6 casa">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="riego">
                                                        Riego Automático
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="riego" name="riego">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="atejardin">
                                                        Antejardín
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="atejardin" name="atejardin">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="column col-6">
                                        <div class="fluid-container">
                                            <div class="row">
                                                <div class="column col-6">
                                                    <label for="panelSolar">
                                                        Panel solar
                                                    </label>
                                                </div>
                                                <div class="column col-6">
                                                    <label class="switch">
                                                        <input type="checkbox" id="panelSolar" name="panelSolar">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <input type="submit" name="save" class=" action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion11" class="paginacion" onchange="paginacion(11)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11" disabled>A. Comunes</option>
                        <option value="12">Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (9/9)-->
                <fieldset id="12">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (9/9)</h3>

                    <input class="fs-contribucion" type="text" name="disponibilidad" name="disponibilidad" placeholder="Disponibilidad visitas [Ej. L-V 11:00-17:00]" />

                    <div class="select-container ">
                        <select name="info_propiedad" id="info_propiedad" class="fs-nBan" onchange="disponibilidadF()">
                            <option value="" disabled selected>Disponibilidad de entrega</option>
                            <option value="Inmediata">Inmediata</option>
                            <option value="1 semana">1 semana</option>
                            <option value="2 semana">2 semanas</option>
                            <option value="3 semanas">3 semanas</option>
                            <option value="1 mes">1 mes</option>
                            <option value="+1 mes">+1 mes</option>
                            <option value="Otro">Otro</option>
                        </select>
                    </div>
                    <input class="fs-contribucion" type="date" style="display:none" id="fecha_entrega" name="fecha_entrega" />

                    <textarea name="nota" id="nota" cols="30" rows="4" placeholder="Notas" maxlength="1000"></textarea>
                    <textarea name="propDestacado" id="propDestacado" cols="30" rows="4" placeholder="¿Algo que quieras destacar de la propiedad?" maxlength="1000"></textarea>
                    <input class="fs-contribucion" type="text" id="url" name="url" placeholder="URL" />

                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td">Mascotas</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                <input type="checkbox" id="mascotas" name="mascotas">
                                <span class="slider round"></span>
                            </label>
                            </td>
                            <td class="fs-td">Cartel</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                <input type="checkbox" id="cartel" name="cartel">
                                <span class="slider round"></span>
                            </label>
                            </td>
                            <td class="fs-td">Llave</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                <input type="checkbox" id="llave" name="llave">
                                <span class="slider round"></span>
                            </label>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <label for="imagen">Imagen Propiedad:</label>
                        <input type="file" name="imagen" id="imagen"></div>

                    <input type="button" name="quit" id="quit" class=" action-button" value="Finalizar" />
                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion12" class="paginacion paginacionIF" onchange="paginacion(12)">
                        <option value="0" disabled selected>Paginación</option>
                        <option value="1">Corredor</option>
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Rol y Precios anexos</option>
                        <option value="6">Calefacción</option>
                        <option value="7">Metraje y orientación</option>
                        <option value="8">Info. Oficina</option>
                        <option value="9">Estacionamientos y Bodegas</option>
                        <option value="10">Otros Propiedad</option>
                        <option value="11">A. Comunes</option>
                        <option value="12" disabled>Visita y entrega</option>
                    </select>
                    </div>
                    <input type="button" name="previous" class="save previous btn-matchBlue action-button" value="Anterior" />

                </fieldset>

            </form>
        </div>
    </section>
    <?
    include '../pages/footer.php';
    ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>
        <script src="../js/toastr.js"></script>
        <script src="../js/plantillas/oficina/formularioOficinaArr.js"></script>
        <script src="../js/validaciones/requerido.js"></script>

        <script>
            // var area = document.getElementById("select_area");
            // if(area.value == "null"){
            //     alert("area vacía");
            //     area.focus();
            // }
        </script>
        <script>
            $(document).ready(function() {
                // tipo_prop();

            });
        </script>
        </body>

        </html>