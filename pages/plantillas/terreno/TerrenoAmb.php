<?php
include '../conexion.php';
$idForm = $_GET["id"];
$idDirec = $_GET["idDirec"];
$idPer = $_GET["idPer"];
$idProp = $_GET["idProp"];
$idPropiedad = $_GET["idPropiedad"];
?>

    <section>
        <div class="container">
            <form action="" id="formC">

                <input type="hidden" id="idPer" name="idPer" value="<?echo $idPer?>">
                <input type="hidden" id="idProp" name="idProp" value="<?echo $idProp?>">
                <input type="hidden" id="idForm" name="idForm" value="<?echo $idForm?>">
                <input type="hidden" id="idPropiedad" name="idPropiedad" value="<?echo $idPropiedad?>">
                <input type="hidden" id="idDirec" name="idDirec" value="<?echo $idDirec?>">
                <input type="hidden" id="operacion" name="operacion" value="3">
                <input type="hidden" id="tipoProp" name="tipoProp" value="5">
                <!-- 14 es Casa terreno -->

                <!-- progressbar -->
                <ul id="progressBar">
                    <li class="active">Corredor</li>
                    <li>Propietario</li>
                    <li>Propiedad</li>
                </ul>

                <div class="container">
                    <div class="row">
                        <div class="column col-12">

                        </div>
                    </div>
                </div>
                <!-- fieldset 1 -->
                <fieldset id="1">

                    <h2 class="fs-title">Información Corredor</h2>
                    <h3 class="fs-subtitle">Paso 1</h3>


                    <div class="select-container">
                        <label for="select_area" class="label_titulo">Área Redflip</label>
                        <select name="select_area" id="select_area" class="dropdown">
                            <option value="null" disabled selected>Área Redflip*</option>
                            <option value="Redflip Habitacional">Redflip Habitacional</option>
                            <option value="Redflip Comercial">Redflip Comercial</option>
                            <option value="Redflip Luxury">Redflip Luxury</option>
                        </select>
                    </div>

                    <div class="select-container">
                        <label for="corredor" class="label_titulo">Corredor</label>
                        <select name="corredor" id="corredor" onchange="selectMail()">
                            <option value="null" disabled selected>Nombre Corredor*</option>
                            <?
                            $sqlCorredor="Select Corredor.id_corredor, Persona.nombre, Persona.apellido, Persona.correo from Corredor, Persona where Corredor.fk_persona = Persona.id_persona";
                            $result = $conn->query($sqlCorredor);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo "<option value=".$row["id_corredor"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <input type="text" id="mail" value="" disabled>
                    <div class="select-container">
                        <label for="operario" class="label_titulo">Operario Cámara</label>
                        <select name="operario" id="operario">
                            <option value="null" disabled selected>Operario cámara*</option>
                            <?php
                            $sqlOperario = "Select Operario.id_operario, Persona.nombre, Persona.apellido from Operario, Persona where Operario.fk_persona = Persona.id_persona order by id_operario";
                            $result = $conn->query($sqlOperario);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo "<option value=".$row["id_operario"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion1" class="paginacion paginacionIF" onchange="paginacion(1)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1" disabled>Corredor</option>  
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 2 Información Propietario-->
                <fieldset id="2">
                    <h2 class="fs-title">Información Propietario</h2>
                    <h3 class="fs-subtitle">Paso 2</h3>

                    <label for="nombre" class="label_titulo">Nombre</label>
                    <input class="fs-nom" type="text" name="nombre" placeholder="Nombre*" id="nombre" onfocusout="validarNombre()" />

                    <label for="apellido" class="label_titulo">Apellido</label>
                    <input class="fs-ape" type="text" name="apellido" placeholder="Apellido*" id="apellido" onfocusout="validarApellido()" />

                    <label for="rut" class="label_titulo">Rut</label>
                    <input class="fs-rut" type="text" name="rut" placeholder="Rut" id="rut" onfocusout="validarRut2()" />

                    <div class="select-container" id="">
                        <label for="origen" class="label_titulo">Origen</label>
                        <select name="origen" id="origen" class="fs-ori" onchange="cambioOrigen()">
                            <option value="" disabled selected>Origen*</option>
                            <option value="17" >Captado por Redflip</option>
                            <option value="7" >Referido Corredor</option>
                            <option value="6" >Referido Cliente</option>
                            <option value="5" >Referido Conserje</option>
                            <option value="18">Partner Redflip</option>                         
                        </select>
                    </div>

                    <div class="select-container" id="div_captadaR" style="display:none">
                        <label for="captadaR" class="label_titulo">Seleccione:</label>
                        <select name="captadaR" id="captadaR" class="fs-ori">
                                <option value="" disabled selected>Seleccione opción*</option>
                                <option value="9" >Base de Datos</option>
                                <option value="19" >Google</option>
                                <option value="1" >Instagram</option>
                                <option value="2" >Facebook</option>
                                <option value="23" >Desconocido</option>                   
                        </select>
                    </div>

                    <div class="select-container" id="div_partnerR" style="display:none">
                        <label for="partnerR" class="label_titulo">Seleccione:</label>
                        <select name="partnerR" id="partnerR" class="fs-ori">
                                <option value="" disabled selected>Seleccione opción*</option>
                                <option value="11" >Doorlis</option>
                                <option value="20" >De Carlo</option>
                                <option value="12" >OpenCasa</option>
                                <option value="21" >UHomie</option>
                                <option value="13" >GI0</option>
                                <option value="2" >Stoffel & Valdivieso</option>
                                <option value="15" >Otro</option>
                                <option value="23" >Desconocido</option>                 
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="selectDivisa-container">
                            <label for="telefono" class="label_titulo">Teléfono</label><br>
                            <select name="sub_fono" id="sub_fono" class="fs-suFono">
                                <option value="" disabled selected>Prefijo*</option>
                                <option value="+569">+569</option>
                                <option value="+562">+562</option>
                                <option value="+568">+568</option>
                                <option value="+56">+56</option>
                            </select>
                        </div>
                        <input class="fs-fono" type="tel" name="telefono" id="telefono" placeholder="Teléfono*" onfocusout="validarTelefono()" />
                    </div>

                    <label for="correo" class="label_titulo">Correo</label>
                    <input type="mail" name="correo" id="correo" placeholder="Correo*" onfocusout="validarCorreo()" />

                    <label for="fecha" class="label_titulo">Fecha</label>
                    <input type="date" name="fecha" id="fecha" placeholder="Fecha" />

                    <hr>
                    <h3 class="fs-subtitle">Contactos Adicionales</h3>

                    <div class="divC">
                        <ol>
                            <div id="contactos">

                            </div>
                        </ol>


                        <a class="addC" id="addC" href="#" onclick="agregarContacto(); return false;"> Agregar contacto <i class="fas fa-plus-circle"></i></a>
                        <a class="delC" id="delC" href="#" onclick="eliminarContacto(); return false;"> Eliminar contacto <i class="fas fa-minus-circle"></i></a>
                    </div>


                    <input type="submit" name="save" class="action-button" value="Guardar" />
                    <div class="selectPag-container">
                        <select name="" id="paginacion2" class="paginacion" onchange="paginacion(2)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>  
                            <option value="2" disabled>Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (1/11)-->
                <fieldset id="3">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (1/3)</h3>

                    <div id="valor1" style="">
                        <label for="monto1" class="label_titulo">Monto Venta</label><br>
                        <input class="fs-valor fs-tNumb" type="text" name="monto1" id="monto1" placeholder="Monto Venta" style="width:75%!important" onfocusout="validarPrecio('monto1')" />
                        <select name="divisaMonto1" id="divisaMonto1" class="fs-divisa">
                                <option value="" disabled selected>Divisa</option>
                                <option value="1" >UF</option>
                                <option value="2" >CLP</option>
                            </select>
                    </div>
                    <div id="valor2" style="">
                        <label for="monto2" class="label_titulo">Monto Arriendo</label><br>
                        <input class="fs-valor fs-tNumb" type="text" name="monto2" id="monto2" placeholder="Monto Arriendo" style="width:75%!important" onfocusout="validarPrecio('monto2')" />
                        <select name="divisaMonto2" id="divisaMonto2" class="fs-divisa">
                                <option value="" disabled selected>Divisa</option>
                                <option value="1" >UF</option>
                                <option value="2" >CLP</option>
                            </select>
                    </div>

                    <div class="container" style="margin-top:25px" id="switch_hipoteca">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">Hipoteca</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                    <input type="checkbox" id="hipoteca" name="hipoteca" onchange="hipotecaCheck()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div id="scBanco" class="pregHipoteca" style="display:none">
                        <label for="select_banco" class="label_titulo">Seleccione un banco:</label>
                        <div class="select-container">
                            <select name="select_banco" id="select_banco" class="fs-tipoOpe" onchange="" style="">
                                <option value="null" disabled selected>Seleccione un banco</option>
                                <?php
                                $sqlBancos = "SELECT * FROM Bancos order by banco asc";
                                $result = $conn->query($sqlBancos);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value='".$row["id_banco"]."'>".$row["banco"]."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <!-- <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">Amoblado</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                    <input type="checkbox" id="amoblado" name="amoblado" onchange="amobla2()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div id="div_valor_amoblado" class="fs-tipoOpe" style="display:none">
                                <label for="valorAmob" class="label_titulo">Valor diferencia venta</label>
                                <input class="fs-valor fs-tNumb"  type="text" name="valorAmob" id="valorAmob" placeholder="Valor de diferencia amoblado" onfocusout="validarPrecio('valorAmob')"/>
                                
                                <label for="valorAmobArr" class="label_titulo">Valor diferencia arriendo</label>
                                <input class="fs-valor fs-tNumb"  type="text" name="valorAmobArr" id="valorAmobArr" placeholder="Valor de diferencia amoblado" onfocusout="validarPrecio('valorAmobArr')"/>
                            </div>
                        </div>
                    </div> -->

                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">No exclusivo</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                    <input type="checkbox" id="exclusividad" name="exclusividad" onchange="noExclusividad()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="pregExclusividad">
                        <div id="div_preg_Exclusividad" style="display:none">
                            <label for="cantCorredor" class="label_titulo">Cantidad de Corredores</label>
                            <div id="scCantCorredor" class="">
                                <select name="cantCorredor" id="cantCorredor" class="fs-tipoOpe">
                                    <option value="null" disabled selected>Cant. de corredores</option>
                                    <option value="1" >1</option>
                                    <option value="2" >2</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                    <option value="5" >5</option>
                                    <option value="6" >6</option>
                                </select>
                            </div>

                            <div id="scTiempoPublicacion" class="">
                                <label for="select_tiempoPublicacion" class="label_titulo">Tiempo de publicación</label>
                                <select name="select_tiempoPublicacion" id="select_tiempoPublicacion" class="fs-tipoOpe">
                                    <option value="null" disabled selected>Tiempo de publicación</option>
                                    <option value="1" >1 Semana</option>
                                    <option value="2" >2 Semanas</option>
                                    <option value="3" >3 Semanas</option>
                                    <option value="4" >1 Mes</option>
                                    <option value="5" >2 Meses</option>
                                    <option value="6" >3 Meses</option>
                                    <option value="7" >4 Meses</option>
                                    <option value="8" >5 Meses</option>
                                    <option value="9" >6 Meses</option>
                                    <option value="10">+6 Meses</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <label for="infoPropVenta" class="label_titulo">¿Por qué quieres arrendar o vender?</label>
                    <textarea name="infoPropVenta" id="infoPropVenta" cols="30" rows="3" placeholder="¿Por qué quieres vender o arrendar?" maxlength="1000"></textarea>

                    <input type="submit" name="save" class="action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion3" class="paginacion" onchange="paginacion(3)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>  
                            <option value="2">Propietario</option>
                            <option value="3" disabled>Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (2/11)-->
                <fieldset id="4">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (2/3)</h3>
                    <div class="form-group">
                        <label for="calle" class="label_titulo">Dirección</label><br>
                        <input class="fs-calle" type="text" name="calle" placeholder="Calle*" id="calle" />
                        <input class="fs-numb fs-tNumb" type="number" name="numero" placeholder="Número*" id="numero" />
                        <input class="fs-letra" type="text" name="letra" placeholder="Lote" id="letra" />
                    </div>

                    <label for="comuna" class="label_titulo">Comuna</label><br>
                    <table class="fs-table">

                        <tr class="fs-tr">
                            <td class="fs-td3" colspan="6">
                                <div class="selectCom-container">
                                    <select name="comuna" id="comuna" class="fs-comuna">
                                    <option value="null" disabled selected>Comuna*</option>
                                    <?php
                                    $sqlComuna = "Select * from Comuna order by nombre asc";
                                    $result = $conn->query($sqlComuna);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                            }
                                        } 
                                    ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="fs-td3">
                                <label for="supT" class="label_titulo">Superficie Total</label>
                                <input class="fs-supT fs-tNumb" type="text" name="supT" id="supT" placeholder="m2 total" onfocusout="validarPrecio('supT')" />
                            </td>
                            <td class="fs-td2" id="switchAgricola1" style="">Agricola</td class="fs-td">
                            <td class="fs-td3" id="switchAgricola2" style="">
                                <label class="switch" id="switchAgricola3" style="">
                                        <input type="checkbox" id="agricola" name="agricola">
                                        <span class="slider round" id="switchAgricola4" style=""></span>
                                    </label>
                            </td>
                        </tr>
                        <tr class="fs-tr">
                            <!-- <br> -->
                            <td class="fs-td2" id="switchPasoServ1" style="">Paso Servidumbre</td class="fs-td">
                            <td class="fs-td3" id="switchPasoServ2" style="">
                                <label class="switch" id="switchPasoServ3" style="">
                                        <input type="checkbox" id="paso_servidumbre" name="paso_servidumbre">
                                        <span class="slider round" id="switchPasoServ4" style=""></span>
                                    </label>
                            </td>
                            <td class="fs-td2" id="switchUrbanizado1" style="">Urbanizado</td class="fs-td">
                            <td class="fs-td3" id="switchUrbanizado2" style="">
                                <label class="switch" id="switchUrbanizado3" style="">
                                        <input type="checkbox" id="urbanizado" name="urbanizado">
                                        <span class="slider round" id="switchUrbanizado4" style=""></span>
                                    </label>
                            </td>


                        </tr>
                    </table>
                    <label for="referencia" class="label_titulo">Referencia</label>
                    <input class="fs-referencia" type="text" name="referencia" id="referencia" placeholder="Referencia" />

                    <div class="form-group" style="display: none" id="divCondominio">
                        <label for="calleCond" class="label_titulo">Dirección Particular</label><br>
                        <input class="fs-calle" type="text" name="calleCond" placeholder="Calle" id="calleCond" />
                        <input class="fs-numb fs-tNumb" type="number" name="numeroCond" placeholder="Número" id="numeroCond" />
                    </div>

                    <input type="submit" name="save" class="action-button" value="Guardar" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion4" class="paginacion" onchange="paginacion(4)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>  
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4" disabled>Dirección</option>
                            <option value="5">Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="previous btn-matchBlue action-button" value="Anterior" />
                    <input type="button" name="next" class="save next btn-matchBlue action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 3 (11/11)-->
                <fieldset id="5">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (3/3)</h3>

                    <label for="disponibilidad" class="label_titulo">Disponibilidad Visitas</label>
                    <input class="fs-contribucion" type="text" name="disponibilidad" name="disponibilidad" placeholder="Disponibilidad visitas [Ej. L-V 11:00-17:00]" />

                    <label for="info_propiedad" class="label_titulo">Disponibilidad de Entrega</label>
                    <div class="select-container ">
                        <select name="info_propiedad" id="info_propiedad" class="fs-nBan" onchange="disponibilidadF()">
                            <option value="" disabled selected>Disponibilidad de entrega</option>
                            <option value="Inmediata">Inmediata</option>
                            <option value="1 semana">1 semana</option>
                            <option value="2 semana">2 semanas</option>
                            <option value="3 semanas">3 semanas</option>
                            <option value="1 mes">1 mes</option>
                            <option value="+1 mes">+1 mes</option>
                            <option value="Otro">Otro</option>
                        </select>
                    </div>

                    <input class="fs-contribucion" type="date" style="display:none" id="fecha_entrega" name="fecha_entrega" />

                    <label for="nota" class="label_titulo">Notas</label>
                    <textarea name="nota" id="nota" cols="30" rows="4" placeholder="Notas" maxlength="1000"></textarea>

                    <label for="propDestacado" class="label_titulo">Destacado</label>
                    <textarea name="propDestacado" id="propDestacado" cols="30" rows="4" placeholder="¿Algo que quieras destacar de la propiedad?" maxlength="1000"></textarea>

                    <label for="url" class="label_titulo">URL</label>
                    <input class="fs-contribucion" type="text" id="url" name="url" placeholder="URL" />

                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td">Cartel</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                    <input type="checkbox" id="cartel" name="cartel">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td class="fs-td">Llave</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                    <input type="checkbox" id="llave" name="llave">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <label for="imagen">Imagen Propiedad:</label>
                        <input type="file" name="imagen" id="imagen"></div>

                    <input type="button" name="quit" id="quit" class="action-button" value="Finalizar" />
                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion5" class="paginacion paginacionIF" onchange="paginacion(5)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor</option>  
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5" disabled>Visita y entrega</option>
                        </select>
                    </div>
                    <input type="button" name="previous" class="save previous btn-matchBlue action-button" value="Anterior" />

                </fieldset>

            </form>
        </div>
    </section>
    <?
    include '../pages/footer.php';
    ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>
        <script src="../js/toastr.js"></script>
        <script src="../js/plantillas/terreno/formularioTerrenoAmb.js"></script>
        <script src="../js/validaciones/requerido.js"></script>

        <script>
            // var area = document.getElementById("select_area");
            // if(area.value == "null"){
            //     alert("area vacía");
            //     area.focus();
            // }
        </script>
        <script>
            $(document).ready(function() {});
        </script>
        </body>

        </html>