<? include '../conexion.php';?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
/* .container {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
    text-align: center;
}

button {
    display: block;
    padding: 1em 2em;
    outline: none;
    font-weight: 600;
    border: none;
    color: #fff;
    background-color: #3498db;
    border: 1px solid #1f74ac;
    border-radius: 0.3em;
    margin-top: 4em;
    cursor: pointer;
}

button:hover {
    background-color: #2487c9;
} */

/* Notifications */

.notification {
    display: inline-block;
    position: relative;
    padding: 0.6em;
    background: #3498db;
    border-radius: 0.2em;
    font-size: 1.3em;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
}

.notification::before, 
.notification::after {
    color: #fff;
    text-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
}

.notification::before {
    display: block;
    content: "\f0f3";
    font-family: "FontAwesome";
    transform-origin: top center;
}

.notification::after {
    font-family: Arial;
    font-size: 0.7em;
    font-weight: 700;
    position: absolute;
    top: -15px;
    right: -15px;
    padding: 5px 8px;
    line-height: 100%;
    border: 2px #fff solid;
    border-radius: 60px;
    background: #3498db;
    opacity: 0;
    content: attr(data-count);
    opacity: 0;
    transform: scale(0.5);
    transition: transform, opacity;
    transition-duration: 0.3s;
    transition-timing-function: ease-out;
}

.notification.notify::before {
    animation: ring 1.5s ease;
}

.notification.show-count::after {
    transform: scale(1);
    opacity: 1;
}

@keyframes ring {
    0% {
        transform: rotate(35deg);
    }
    12.5% {
        transform: rotate(-30deg);
    }
    25% {
        transform: rotate(25deg);
    }
    37.5% {
        transform: rotate(-20deg);
    }
    50% {
        transform: rotate(15deg);
    }
    62.5% {
        transform: rotate(-10deg);
    }
    75% {
        transform: rotate(5deg);
    }
    100% {
        transform: rotate(0deg);
    }
}
/* asdas */
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: red;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1;}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
</head>
<body>

<ul>
  <li class="dropdown">
  <a onmouseover="ver()" href="" id="camp">
    <div class="notification"></div>
  </a>

    <div class="dropdown-content">
        <div>
            <?
            $sql = "Select * from Propietario, Propiedad, Persona where Propietario.cont >=20 and Propietario.cont <= 30 and Propiedad.fk_propietario = Propietario.id_propietario and Persona.id_persona = Propietario.fk_persona AND Propiedad.fk_estado_propiedad = 1" ;
            $result = $conn->query($sql);
            $cont =0;
            $importanciaMedia = 0;
            $importanciaAlta = 0;
            
            $sql2 = "Select * from Propietario, Propiedad, Persona where Propietario.cont >30 and Propiedad.fk_propietario = Propietario.id_propietario and Persona.id_persona = Propietario.fk_persona and Propiedad.fk_estado_propiedad = 1" ;
            $result2 = $conn->query($sql2);
            if ($result2->num_rows > 0) {
                while($row = $result2->fetch_assoc()) {
                    // echo "<a href='#'>".$row["nombre"]."</a>";
                    echo "<div style='background-color : red;' class = 'w3-panel w3-card-2'><a href='http://redflip.indev9.com/pages/editarPropiedad.php?id=".$row['id_propiedad']."'>
                    <h3>Notificación importancia Alta</h3>
                    Nombre: ".$row["nombre"]."</br>
                    Apellido: ".$row["apellido"]."</br>
                    Días sin contactar: ".$row["cont"]."
                    </a></div>";
                    $cont++;
                }
            } 
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    // echo "<a href='#'>".$row["nombre"]."</a>";
                    echo "<div style='background-color : yellow;' class = 'w3-panel w3-card-2'><a href='http://redflip.indev9.com/pages/editarPropiedad.php?id=".$row['id_propiedad']."'>
                    <h3>Notificación importancia media</h3>
                    Nombre: ".$row["nombre"]."</br>
                    Apellido: ".$row["apellido"]."</br>
                    Días sin contactar: ".$row["cont"]."
                    </a></div>";
                    $cont++;
                }
            } 
            $conn->close();
            $i=0;
            ?>
        </div>
      <input type="hidden" id = "count" value="<?echo $cont?>">
    </div>
  </li>
</ul>

</body>
</html>






<script>
var el = document.querySelector('.notification');
var cont = parseInt(document.getElementById("count").value);

    // var count = Number(el.getAttribute('data-count')) || 0;
    // el.setAttribute('data-count', count + 10);
    // el.classList.remove('notify');
    // el.offsetWidth = el.offsetWidth;
    // el.classList.add('notify');
    // if(count === 0){
    //     el.classList.add('show-count');
    // }

    el.setAttribute('data-count', cont);
    el.classList.add('show-count');
    el.setAttribute('show', false);
    


// document.querySelector('button').addEventListener('click', function(){
//     var count = Number(el.getAttribute('data-count')) || 0;
//     el.setAttribute('data-count', count + 1)
//     el.classList.remove('notify');
//     el.offsetWidth = el.offsetWidth;
//     el.classList.add('notify');
//     if(count === 0){
//         el.classList.add('show-count');
//     }
// }, false);

function ver (){



    // if(el.getAttribute("show") == true){
    //     el.setAttribute("show",false)
    // }else if(el.getAttribute("show") == false){
    //     el.setAttribute("show", true)
    // }   
    // console.log(el.getAttribute("show"))
    
    el.classList.remove('show-count');



}
</script>