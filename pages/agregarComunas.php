<?
    include 'sidebarMenu.php';
    include '../conexion.php';

    $region = "";
?>
<div class="container basic-margin-gabo animated fadeIn">
    <form>
    <div class="mb-3 form-check">
        <label for="exampleInputEmail1" class="form-label">Región</label>
        <select class="form-select" aria-label="" onchange="regionChange()">
            <option disabled selected>Seleccione</option>
            <?php
            $sql = "SELECT * from Region";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo "<option value='".$row["id_region"]."'>".utf8_encode($row["nombre_region"])."</option>";   
                }
            }
            $conn->close();
            ?>
        </select>
    </div>
    <div class="mb-3 form-check">
        <label for="exampleInputPassword1" class="form-label">Provincia</label>
        <select class="form-select" aria-label="Default select example">
            <option disabled selected>Seleccione</option>
            <?php
            $sql = "SELECT * from Provincia WHERE fk_region = $region";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                echo "<option value='".$row["id_provincia"]."'>".utf8_encode($row["nombre_provincia"])."</option>";
                }
            }
            $conn->close();
            ?>
            <option value = "0">Nueva Provincia</option>
        </select>
        <br>
        <label for="exampleInputPassword1" class="form-label">Nombre Provincia</label>
        <input type="text">
    </div>
    <div class="mb-3 form-check">
        <label for="exampleInputEmail1" class="form-label">Nombre comuna</label>
        <input type="text" class="form-control" id="nombre">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script src="../js/comunas.js"></script>
<?php
include_once 'footer.php';
?>
