<style>
    table tr th {
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    
    .sorting {
        background-color: #D4D4D4;
    }
    
    .asc:after {
        content: ' ↑';
    }
    
    .desc:after {
        content: " ↓";
    }
</style>
<?php

include 'sidebarMenu.php';
include '../conexion.php';
$sql2 = "Update usuario SET ultima_pag='http://indev9.com/redflip/pages/propVSpropiedad.php' where id_usuario =". $_SESSION['id'];
if ($conn->query($sql2) === TRUE) {
  echo "<script>console.log('última pagina guardada http://indev9.com/redflip/pages/propVSpropiedad.php')</script>";
} else {
  // echo "<script>console.log('no')</script>";
}

if(isset($_GET["filtro"])){

$filtro = $_GET["filtro"];

}else{

  $filtro = "";

}

?>


    
    <body>
        
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>




        <section>

            <div class="container container-prop col-11" style="padding-top:50px;">
                <div class="row">
                    <div class="col-12">
                        <h3 class="h3-prop mtb-50">Propietarios - Propiedades</h3>
                        <div>
                        </div>
                        <div class="row">
                            <!-- Filtro -->
                            <div class="column col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                <form id="filtro">
                                    <select name="tipo_operacion" id="tipo_operacion" class=" filtro filtroW4" style="display:none">
                                  <option id="" value= ""disabled selected> Operación</option>
                                  <option id="" value="">Todos</option>
                                  <option id="venta" value="venta">Venta</option>
                                  <option id="arriendo" value="arriendo">Arriendo</option>
                                </select>

                                    <select name="estado" id="estado" class=" filtro filtroW4" style="display:none">
                                  <option id="" value = "" disabled selected> Estado</option>
                                  <option id="" value="">Todos</option>  
                                  <option id="" value="prospecto">Prospecto</option>
                                  <option id="propietario" value="propietario">Propietario</option>
                                </select>

                                    <select name="fase" id="fase" class=" filtro filtroW4" style="display:none">
                                  <option id="" value = "" disabled selected> Fase</option>
                                  <option id=""  value="">Todos</option>
                                </select>

                                    <select name="fase" id="importancia" class=" filtro filtroW4" style="display:none">
                                  <option id="" value=""disabled selected> Importancia</option>
                                  <option id=""  value="">Todos</option>
                                  <option id=""  value="Alta">Alta</option>
                                  <option id=""  value="Media">Media</option>
                                  <option id=""  value="Baja">Baja</option>
                                </select>

                                    <button type="button" id="btn_venta" class="btn btn-filt bg-redflip-red" onclick="filtros()">Filtrar</button>
                                    <!-- <button type="button" id= "btn_venta" class="btn btn-filt bg-redflip-red" data-toggle="modal" data-target="#exampleModalCenter">Filtrar</button> -->
                                </form>
                            </div>

                            <!-- Filtro buscador -->
                            <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-4 v-align">
                                <input type="text" id=search placeholder="🔍 Buscar . . .">
                            </div>

                            <!-- Subida de archivos NUEVO -->

                            <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-4" style="text-align:right; width:100%;">
                                <form action="../controlador/subirArchivo.php?id=2" method="POST" enctype="multipart/form-data" target="POPUPW" onsubmit="POPUPW = window.open('about:blank','POPUPW','width=1400,height=600');">
                                    <button type="button" id="submitExport" class="btn btn-Imex bg-redflip-black pad-0" style="width:80%; padding:2px 5px; font-size:20px; margin:5px; float:right;" onclick="ExportExcel()">
                                      Exportar
                                  </button>
                                </form>
                            </div>
                            <!-- Subida de archivos antiguo -->
                            <!-- <div class="column col-xs-12 col-sm-12 col-md-2 col-lg-4">
                      <form action="../controlador/subirArchivo.php?id=2" style="float:right;" method="POST" enctype="multipart/form-data" target="POPUPW" onsubmit="POPUPW = window.open('about:blank','POPUPW','width=1400,height=600');">
                        <label style="float:right;"> 
                          <span style="margin:0;">
                            <input type="file" class="upArch" id="file" name="archivo" >
                            <label for="file" class="btn upLbl">Seleccione un archivo</label>
                              <button type="submit" id="subir" class="btn btn-Imex pad-0 bg-redflip-red">
                                Importar
                              </button>
                          </span>
                      </form>
                          <span>
                            <button type="button" id="submitExport" class="btn btn-Imex bg-redflip-black pad-0" onclick="ExportExcel()">
                              Exportar
                            </button>
                          </span>
                        </label>
                  </div> -->
                        </div>
                    </div>
                </div>
        </section>



        <div class="container-fluid" id="datos" onload="tabla()">

        </div>



        <form action="process.php" method="post" target="_blank" id="formExport">

            <input type="hidden" id="data_to_send" name="data_to_send" />

            <input type="hidden" id="nombre" name="nombre" value="Propietarios-Propiedades" />

        </form>



        <span>

    <input type="hidden" id="filtros_app" value="<?echo $filtro?>">

  </span>





        <?

include 'footer.php';

?>

            <script src="../js/dist/sweetalert2.all.min.js"></script>
            <script src="../js/buscar.js"></script>



            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

            <script src="../js/toastr.js"></script>

            <script src="../js/accionProp.js"></script>

            <script src="../js/selectFase.js"></script>

            <script src="../js/buscarPropietarios.js"></script>

            <script src="../js/duplicarFormulario.js"></script>

            <!-- <script src="../js/export.js"></script> -->

            <script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>


            <script type="text/javascript">
                function ExportExcel(type, fn, dl) {
                    var elt = document.getElementById('export_to_excel');
                    var wb = XLSX.utils.table_to_book(elt, {
                        sheet: "Sheet JS"
                    });
                    return dl ?
                        XLSX.write(wb, {
                            bookType: type,
                            bookSST: true,
                            type: 'base64'
                        }) :
                        XLSX.writeFile(wb, fn || ('Mix.' + (type || 'xlsx')));
                }
            </script>

            <script>
                function mandar() {

                    var popup = window.open('../services/insertarPropVSpropiedadExcel.php', 'popup', 'width=1300px,height=400px', true);

                    poup.focus();

                    document.getElementById("subArch").submit();

                }

                var filtro = document.getElementById("filtros_app").value;

                var importancia = document.getElementById("importancia");

                if (filtro == "alta") {



                    buscar_datos("", "", "", "Alta");

                } else if (filtro == "media") {

                    // console.log("media")



                    buscar_datos("", "", "", "Media");



                }

                function filtros() {
                    window.open("filtro.php", "filtros", "width=1000px, height=420px, top=30px, resizable=no, menubar=no");
                }

                function redirigir(id, origen) {
                    // window.open('fichaForm.php?id='+id, '');
                    window.open("fichaForm.php?id=" + id + "&origen=" + origen, 'name', 'height=800,width=700 center');
                }
            </script>