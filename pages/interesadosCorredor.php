<? 
include 'sidebarMenu.php';
include "../conexion.php";
include_once '../controlador/serv_matchInteresado.php';
$idPer = $_GET["id"];
$sql2 = "Update usuario SET ultima_pag='https://indev9.com/redflip/pages/interesadosCorredor.php?id=$idPer' where id_usuario =". $_SESSION['id'];
if(empty($idPer) || !isset($idPer)){
    header("location: interesadosCorredor.php?id=". $_SESSION["idPer"]);
}
if ($conn->query($sql2) === TRUE) {

}

?>

    <section>
        <div class="container col-11" style="padding-top:50px;">
            <div class="row">
                <div class="column col-12">
                    <h3 class="h3-prop mtb-50">Interesados</h3>
                </div>
            </div>

            <div class="row" style="margin:30px 0;">
                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-6">
                    <form id="filtro" action="buscarInteresadosGlobales.php" method="get">
                        <div class="column col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input type="text" id="search" name="search" placeholder="🔍 Buscar . . .">
                        </div>
                    </form>
                </div>

                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-6" style="text-align:right;">
                    <form action="../controlador/subirArchivo.php" method="POST" enctype="multipart/form-data">
                        <button type="button" id="submitExport" class="btn btn-Imex bg-redflip-black pad-0" style="width:30%; padding:5px;" onclick="ExportExcel('xlsx')">
                            Exportar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>



    <div id=datos>
        <table class='table table-responsive-xl table-hover table-striped stycky-uwu ' id='interesadosGlobales'>
            <thead class="">
                <tr class='thead-dark'>
                    <th class='' scope='col'> # </th>
                    <th class='' scope='col'> Nombre </th>
                    <th class='' scope='col'> Teléfono </th>
                    <th class='' scope='col'> Corredor </th>
                    <th class='' scope='col'> Correo </th>
                    <th class='' scope='col'> Acciones </th>
                </tr>
            </thead>
            <tbody>
                <?php
            $salida="";
            $sqlMain = "SELECT DISTINCT
            Persona.nombre
            , Persona.apellido
            , Persona.telefono
            , Persona.correo
            , Interesado.fk_corredor
            , Interesado.id_interesado
            , perf.comuna
            , perf.orientacion
            FROM Interesado
               , Persona
               , Perfil_busqueda perf
            WHERE Interesado.fk_persona = Persona.id_persona 
            AND perf.fk_interesado = Interesado.id_interesado 
            AND Interesado.fk_corredor = (SELECT id_corredor FROM Corredor WHERE fk_persona = ".$_SESSION["idPer"].")";
            $result = $conn->query($sqlMain);
            // echo $sqlMain;
            if ($result->num_rows > 0) {
                
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    $nomInt = $row["nombre"];
                    $apeInt = $row["apellido"];
                    $telInt = $row["telefono"];
                    $correoInt = $row["correo"];
                    $orientacion = $row["orientacion"];
                    $id = $row["id_interesado"];
                    $obtenerMatch = json_decode(( match($id) ));
                    $obtenerMatch= (count($obtenerMatch->array));
                    $corredor = $row["fk_corredor"];
                    $salida.= "<tr>
                    <td>".$id."</td>
                    <td>".$nomInt. " " .$apeInt . "</td>
                    <td>".$telInt."</td>";
                    
                    $sqlCorredor = "SELECT * from Corredor, Persona where Corredor.fk_persona = Persona.id_persona and Persona.id_persona = " . $_SESSION["idPer"];
                    $result2 = $conn->query($sqlCorredor);

                    if ($result2->num_rows > 0) {
                        while($row = $result2->fetch_assoc()) {
                        $salida.= "
                            <td>".$row["nombre"]. " ". $row["apellido"]."</td>";
                            
                        }
                    }else{
                        echo "no po";
                    }
                    $salida.= "<td>".$correoInt."</td>
                    <td class=''>
                    <div class='btn-group dropleft'>
                        <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            <i class='fas fa-cog'></i>
                        </button>
                        <div class='dropdown-menu'>
                            <a class='dropdown-item' href='editarInteresado.php?id=$id'><i class='fas fa-edit'></i> Modificar</a>
                            <a class='dropdown-item' href='https://api.whatsapp.com/send?phone=&text=Hola%20quiero%20info' target='_blank'><i class='fab fa-whatsapp'></i> Whatsapp</a>
                        </div>
                        <div style='margin-top: 11px;margin-left: 20px;'>
                        <a href='https://indev9.com/redflip/pages/match.php?id=$id'>
                            <span id='span' class='badge badge-".(($obtenerMatch > 0 )?'danger':'secondary')."'>".$obtenerMatch."</span>
                        </a>
                            </div>    
                        </td> 
                                </tr>";
                }
            } else {
                echo "no po 2";
            }
            // echo $sqlMain;
            echo $salida;
            ?>

    </div>



    <form action="process.php" method="post" target="_blank" id="formExport">

        <input type="hidden" id="data_to_send" name="data_to_send" />

        <input type="hidden" id="nombre" name="nombre" value="Propietarios Activos" />

    </form>



    <?

    if(isset($_GET["msg"])){

    ?>



        <input type="hidden" id="msg" value=< ?echo $_GET[ "msg"] ?> >



        <?  

        }else

        {

    ?>



            <input type="hidden" id="msg" value="">

            <?

    }

        if(isset($_GET["nom"])){

    ?>



                <input type="hidden" id="nom" value=< ?echo $_GET[ "nom"] ?> >

                <?   

    }

        if(isset($_GET["cont"])){

    ?>



                    <input type="hidden" id="cont" value=< ?echo $_GET[ "cont"] ?> >



                    <?

        }else{

    ?>



                        <input type="hidden" id="cont" value="">



                        <?

    }

    

    include 'footer.php';

    ?>

                            <script src="../js/dist/sweetalert2.all.min.js"></script>

                            <!-- <script src="../js/selectFase.js"></script> -->

                            <!-- <script src="../js/buscarInteresados.js"></script> -->

                            <script src="../js/toastr.js"></script>

                            <!-- <script src="../js/accionProp.js"></script> -->

                            <script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>

                            <script>
                                // document.getElementById('submitExport').addEventListener('click', function(e) {

                                //     e.preventDefault();

                                //     let export_to_excel = document.getElementById('export_to_excel');

                                //     let data_to_send = document.getElementById('data_to_send');

                                //     data_to_send.value = export_to_excel.outerHTML;

                                //     document.getElementById('formExport').submit();

                                // });



                                var msg = document.getElementById("msg").value;

                                var cont = document.getElementById("cont").value;

                                if (msg === "1") {

                                    var nom = document.getElementById("nom").value;

                                    alert("El archivo: " + nom + " se ha subido correctamnte")

                                } else if (msg === "2") {

                                    alert("El archivo no es un excel")

                                }

                                if (cont === "") {



                                } else {

                                    alert("se agregaron " + cont + " propietarios");

                                }
                            </script>



                            <script type="text/javascript">
                                function ExportExcel(type, fn, dl) {

                                    var elt = document.getElementById('export_to_excel');

                                    var wb = XLSX.utils.table_to_book(elt, {
                                        sheet: "Sheet JS"
                                    });

                                    return dl ?

                                        XLSX.write(wb, {
                                            bookType: type,
                                            bookSST: true,
                                            type: 'base64'
                                        }) :

                                        XLSX.writeFile(wb, fn || ('Propietarios.' + (type || 'xlsx')));

                                }
                            </script>

                            <SCRIPT>
                                $('tr').click(function(event) {

                                    $('tr').removeClass('tr-focus'); //Remove focus from other TDs

                                    $(this).addClass('tr-focus');

                                    console.log("asdasd");

                                });
                            </SCRIPT>



                            </body>

                            </html>