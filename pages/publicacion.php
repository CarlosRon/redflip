<?php
    include 'sidebarMenu.php';
    include '../conexion.php';

    $id = $_REQUEST["id"];
    $formulario = "";
    $sqlForm = "SELECT * FROM formulario where id_formulario = $id";
    $result = $conn->query($sqlForm);
    if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $formulario = $row;
    }
    }
    $sqlPub = "SELECT * from Publicacion where fk_formulario = $id and despublicada = 0";
    $result = $conn->query($sqlPub);
    if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $publicada = true;
        $orden=$row["orden_img"];
        $lat = $row["latitud"];
        $lng = $row["longitud"];
        $dir = $row["txtBusqueda"];
        $titulo = utf8_encode($row["titulo"]);
        $descripcion = utf8_encode($row["descripcion"]);
        $imagenes = explode(",", $orden);
        // echo $sqlPub;
        
    }
    } else {
        $publicada = false;
        $descripcion = "[Titulo de la descripción]
    Dormitorios:".$formulario["dormitorios"]." 
    Baños: ".$formulario["cant_bannos"]." 
    Metros útiles: ".$formulario["superficie_util"];
    }

?>
    <style>
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 100%;
        }
        
        #sortable li {
            margin: 9px;
            padding: 3px;
            float: left;
            width: 100px;
            height: 100px;
            border: 1px solid grey;
            border-radius: 5px;
            font-size: 4em;
            text-align: center;
        }
        
        #sortable li img {
            width: 100%;
            vertical-align: unset;
        }
        
        #publicacion {
            padding: 10px;
        }
        
        .pub-group {
            background: #fff;
            border-radius: 5px;
            padding: 10px;
            margin: 10px;
        }
        
        input[type="file"]#file {
            display: none;
        }
        
        .container-box {
            width: 100%;
            height: 365px;
            margin: 0 auto;
            border: 3px solid lightgrey;
            border-radius: 8px;
        }
        
        #sortable li > .quitar-img {
            cursor: pointer;
            z-index: 3!important;
            position: absolute!important;
            margin-left: 70px!important;
            background: rgb(6 6 6 / 30%)!important;
            width: 22px!important;
            height: 22px!important;
            font-size: 19px!important;
            text-align: center!important;
        }
        
        @media only screen and (min-width:1406px) and (max-width:1680px) {
            .container-box {
                height: 465px;
            }
            #sortable li {
                margin: 7px;
            }
        }
        
        @media only screen and (min-width:1331px)and (max-width:1405px) {
            .container-box {
                height: 435px;
            }
            #sortable li {
                margin: 3px;
            }
        }
        
        @media only screen and (min-width:1116px) and (max-width:1330px) {
            .container-box {
                height: 540px;
            }
            #sortable li {
                margin: 3px;
            }
        }
        
        @media only screen and (max-width:1115px) {
            .container-box {
                height: 750px;
            }
            #sortable li {
                margin: 3px;
            }
        }

        input[type="checkbox"]{
            display:none;
        }
        .chk-lbl {
            width: 120px;
            height: 120px;
            border-radius: 5px;
            border: 1px solid lightgrey;
        }

        :checked + label.chk-lbl  {
            border:2px solid #e8505e;
        }

        .chk-lbl>img {
            width:100%;
            transition-duration: 0.2s;
            transform: scale(0.9);
            
        }

        :checked + label.chk-lbl  img {
            transform: scale(0.8);
            z-index: -1;
        }

        .ul-portal{
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .ul-portal li {
            margin: 5px;
            float: left;
        }
        .bg-disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
    </style>

    <div class="container-fluid " style="padding-left: 4em; background:#f8f9fa!important;">
        <div class="row">
            <!-- <div class="col-12">
                <h3 class="h3-prop mtb-50">
                    Publicación
                </h3>
            </div> -->

            <!-- DATOS FORM -->
            <div class="col-6 pt-5">
                <form id="publicacion">

                    <input type="hidden" name="idForm" value="<?=$id?>">

                    <!-- Titulo publicacion-->
                    <div class="form-group pub-group">
                        <label for="titulo" class="h3-match">Título de la publicación</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" value="<?=($publicada)?$titulo:stripslashes(utf8_encode($formulario[" nombre_form "]))?>">
                    </div>

                    <!-- Descripción publicacion -->
                    <div class="form-group pub-group mt-2">
                        <label for="descripcion" class="h3-match">Descripción de la publicación</label>
                        <textarea class="form-control" id="descripcion" name="descripcion" rows="10"><?=$descripcion?></textarea>
                    </div>

                    <!-- Seleccion de fotografías -->
                    <div class="uploader pub-group mt-2">
                        <label for="" class="h3-match">Fotos</label><br>
                        <input type="file" name="images[]" id="file" multiple <?=(isset($orden)? '': 'required')?>>
                        <label for="file" id="file-drag">
                            <div id="start">
                                <span id="file-btn" class="btn btn-matchRed">Seleccione un archivo</span>
                            </div>
                        </label>

                        <div class="container-box">
                            <ul id="sortable">

                                <?php 
                                    foreach($imagenes as $imagen){
                                        echo '  <li id="'.trim($imagen).'" onchange="cambio() ">
                                                    <i class="fas fa-times fa-xs quitar-img" onclick="quitarImg(`'.trim($imagen).'`)"></i>
                                                    <img src="../publicacion/goPlaceIt/'.$id.'/'.trim($imagen).' " width="100 ">
                                                </li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <!-- Portales de publicación-->
                    <div class="form-group pub-group mt-2">
                        <label for="" class="h3-match">Portales</label><br>

                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <ul class="ul-portal">
                                        <li>
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" <?=($publicada)?'checked':''?> >
                                            <label class="form-check-label chk-lbl" for="defaultCheck1">
                                                <img src="../img/goplaceit.png" alt="">
                                            </label>
                                        </li>

                                        <li>
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" disabled>
                                            <label class="form-check-label chk-lbl bg-disabled" for="defaultCheck2">
                                                <img src="../img/Logo-Redflip-Sin-Borde.png" alt="" style="padding-top:35%;">
                                            </label>
                                        </li>

                                        <li>
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck3" disabled>
                                            <label class="form-check-label chk-lbl bg-disabled" for="defaultCheck3">
                                                <img src="../img/logo-toctoc-200x200.jpg" alt="">
                                            </label>
                                        </li>

                                        <li>
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck4" disabled>
                                            <label class="form-check-label chk-lbl bg-disabled" for="defaultCheck4" style="padding-top:30%;">
                                                <img src="../img/portal.png" alt="">
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Mapita mapita -->
                    <div class="form-group pub-group mt-2">
                        <label for="dir"> Ingrese una dirección</label><br>
                        <input type="text" name="dir" id="dir" value="<?=$dir?>">
                        <button type="button" class="btn btn-matchRed" onclick="buscar()">Buscar</button>

                        <div class="row" style="display:none">
                            <div class="col-6">
                                <input type="text" name="lat" id="lat">
                            </div>
                            <div class="col-6">
                                <input type="text" name="lng" id="lng">
                            </div>
                        </div>

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 mt-2 p-0 m-0">
                                    <div id="map" <?=($publicada)? 'onload="buscar()"': 'onload="initMap()"'?> style="height: 300px"></div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-12 mt-5">
                                    <?php
                                        $sql = "SELECT * from Publicacion where fk_formulario = $id and despublicada = 0";
                                        $result = $conn->query($sql);
                                        if ($result->num_rows > 0) {
                                            while($row = $result->fetch_assoc()) {
                                                echo '<button type="button" class="btn btn-matchRed float-right" id="btnDespublicar" style="font-size:20px;" onclick="despublicar('.$id.')">Despublicar</button>';
                                                echo '<button type="submit" class="btn btn-matchGreen float-right" id="btnPublicar" style="font-size:20px;">Modificar</button>';
                                            }
                                        } else {
                                            echo '<button type="submit" class="btn btn-matchRed float-right" id="btnPublicar" style="font-size:20px;">Publicar</button>';
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <!-- FICHA -->
            <div class="col-6 pt-5">
                <div class="pub-group mt-3" style="height: 91vh; overflow-x: overlay; position:fixed;">
                    <?php
                    include 'fichaFormPublicacion.php';
                ?>
                </div>

            </div>
        </div>

    </div>


    <script src="../js/publicacion.js"></script>
    <script src="https://unpkg.com/@google/markerclustererplus@5.1.0/dist/markerclustererplus.min.js"></script>
    <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNzSw5dqoHcZvquOW_wIti2qSkYQEyYBc&callback=initMap">
    </script>
    <script>
        <?php
    if($publicada){
    ?>
        setTimeout(() => {
            initMap2(<?=$lat .','. $lng?>);
        }, 1000);
        <?php
    }
    ?>
    </script>
    <?php
include 'footer.php';

?>