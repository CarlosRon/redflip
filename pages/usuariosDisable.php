<?php
    include '../conexion.php';

    $id = $_GET["id"];

    $sql = "UPDATE Corredor SET eliminado=1 WHERE id_corredor=$id";
    if ($conn->query($sql) === TRUE) {
        $resp ->ingresoBD = "Usuario corredor eliminado.";
        $resp ->respuesta = "2";
    } else {
        $resp->error = $conn->error;
        $resp ->respuesta = "1";
    }

    echo json_encode($resp);
?>