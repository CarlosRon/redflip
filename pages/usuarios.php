
<?php
include 'sidebarMenu.php';
include '../conexion.php';
?>

<!-- $conn->insert_id -->

<div class="container pt-3  animated fadeIn">
<form action="" method="post" id="ingresoUsuario" class="formulario-usuario">
  <input id="temp_pass" name="temp_pass" type="text" hidden>
  <div class="row  border pt-2">
    <div class="col">
      <!-- Level 1: .col-sm-9 -->
      <div class="row">
        <div class="col text-center">
            <h3>Crear Usuario</h3>
        </div>        
      </div>
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Nombre</span>
                </div>
                <input type="text" class="form-control" name="nombre" id="nombre" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Apellido</span>
                </div>
                <input type="text" class="form-control" name="apellido" id="apellido" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Rut</span>
                </div>
                <input type="text" class="form-control" name="rut" id="rut" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Fecha de Nacimiento</span>
                </div>
                <input type="date" class="form-control" name="fecha-nac" id="fecha-nac" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>

      
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Teléfono</span>
                </div>
                <input type="text" class="form-control" name="telefono" id="telefono" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Correo</span>
                </div>
                <input type="text" class="form-control" name="correo" id="correo" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Nombre de Usuario</span>
                </div>
                <input type="text" class="form-control" name="username" id="username" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Contraseña</span>
                </div>
                <input type="text" class="form-control" name="password" id="password" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                <label class="fs-chkLbl" style="margin-top: 13.5px; margin-left: 10px">Auto
                    <input type="checkbox" id="check" name="check" onchange="checkUsuario()">
                    <span class="checkmark"></span>
                </label>
            </div>
        </div>
        
      </div>
      <div class="row">
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Rol</label>
                </div>
                <select class="custom-select" id="rol"  name="rol">
                    <option value="" selected disabled>Seleccione una opción</option>
                    <?php
                    $sql = "SELECT * from Rol order by nom_rol";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                        echo "<option value='".$row["id_rol"]."'>".$row["nom_rol"]."</option>";
                      }
                    }
                    $conn->close();
                    ?>
                </select>
            </div>
        </div>
        <div class="col">
        <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect02">Estado</label>
                </div>
                <select class="custom-select" id="estado"  name="estado">
                    <option value="" selected disabled>Seleccione una opción</option>
                    <option value="1">Activo</option>
                    <option value="2">Desactivado</option>
                </select>
            </div>
        </div>
        
      </div>
      <div class="row">
      <div class="col text-center">
        <button type="submit" class="btn btn-outline-primary center" >Guardar</button>
      </div>
      </div>
    </div>
  </div>
  </form>
</div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script src="../js/usuarios.js"></script>
    <script src="../js/usuariosElim.js"></script>

<?php
include_once 'footer.php';
?>

