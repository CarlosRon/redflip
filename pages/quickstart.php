<?php
require '../vendor/autoload.php';

$client = new Google_Client();

$client->setAuthConfig('credentials.json');

$client->setRedirectUri('https://indev9.com/redflip/pages/almacenarApi.php');

// $client->setApprovalPrompt('force');
$client->setAccessType('offline');        // offline access
$client->setIncludeGrantedScopes(true);   // incremental auth
$client->addScope("https://mail.google.com/");
$client->addScope("https://www.googleapis.com/auth/gmail.compose");
$client->addScope("https://www.googleapis.com/auth/gmail.modify");
$client->addScope("https://www.googleapis.com/auth/gmail.readonly");
$auth_url = $client->createAuthUrl();
header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));

?>