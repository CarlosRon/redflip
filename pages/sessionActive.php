<?php

define('DURACION_SESION','7200'); //2 horas
ini_set("session.cookie_lifetime",'7200');
ini_set("session.gc_maxlifetime",'7200'); 
ini_set("session.save_path","/tmp");
session_cache_expire('7200');
session_start();
$res = null;

if(!isset($_SESSION['usuario'])){
    header("Location: ../index.php");
}


?>