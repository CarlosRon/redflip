<?php
include '../pages/sidebarMenu.php';
include '../conexion.php';
$idPer=$_GET["idPer"];
$idProp = $_GET["idProp"];
$idForm=$_GET["id"];
$idPropiedad=$_GET["idPropiedad"];
$idDirec=$_GET["idDirec"];
$sql2 = "Update usuario SET ultima_pag='https://indev9.com/redflip/pages/formulario.php?id=$idForm&idPer=$idPer&idProp=$idProp&idPropiedad=$idPropiedad&idDirec=$idDirec' where id_usuario =". $_SESSION['id'];
if ($conn->query($sql2) === TRUE) {
} else {
}
?>

<section>
    <div class="container">
        <form action="" id="formC">

        <input type="hidden" id="idPer" name="idPer" value="<?echo $idPer?>">
        <input type="hidden" id="idProp" name="idProp" value="<?echo $idProp?>">
        <input type="hidden" id="idForm" name="idForm" value="<?echo $idForm?>">
        <input type="hidden" id="idPropiedad" name="idPropiedad" value="<?echo $idPropiedad?>">
        <input type="hidden" id="idDirec" name="idDirec" value="<?echo $idDirec?>">

                <!-- progressbar -->
            <ul id="progressBar">
                <li class="active">Corredor y Operario</li>
                <li>Propietario</li>
                <li>Propiedad</li>
            </ul>

            <div class="container">
                <div class="row">
                    <div class="column col-12">
                        
                    </div>
                </div>
            </div>
                <!-- fieldset 1 -->
            <fieldset id="1">

                <h2 class="fs-title">Información Corredor y Operario</h2>
                <h3 class="fs-subtitle">Paso 1</h3>


                <div class="select-container">
                    <select name="select_area" id="select_area" class="dropdown">
                        <option value="null" disabled selected>Área Redflip*</option>
                        <option value="Redflip Habitacional">Redflip Habitacional</option>
                        <option value="Redflip Comercial">Redflip Comercial</option>
                        <option value="Redflip Luxury">Redflip Luxury</option>
                    </select>
                </div>
                
                <div class="select-container">
                    <select name="corredor" id="corredor" onchange="selectMail()" >
                        <option value="null" disabled selected>Nombre Corredor*</option>
                        <?
                        $sqlCorredor="Select Corredor.id_corredor, Persona.nombre, Persona.apellido, Persona.correo from Corredor, Persona where Corredor.fk_persona = Persona.id_persona";
                        $result = $conn->query($sqlCorredor);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo "<option value=".$row["id_corredor"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            
                <input type="text" id="mail" value="" disabled>
                <div class="select-container">
                    <select name="operario" id="operario" >
                        <option value="null" disabled selected>Operario cámara*</option>
                        <?php
                        $sqlOperario = "Select Operario.id_operario, Persona.nombre, Persona.apellido from Operario, Persona where Operario.fk_persona = Persona.id_persona order by id_operario";
                        $result = $conn->query($sqlOperario);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo "<option value=".$row["id_operario"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                
                <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                <div class="selectPag-containerIF">
                    <select name="" id="paginacion1" class="paginacion paginacionIF" onchange="paginacion(1)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="2">Propietario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Pisos y ascensores</option>
                        <option value="6">Rol y Precios anexos</option>
                        <option value="7">Gasto común y otros</option>
                        <option value="8">Superficie y orientación</option>
                        <option value="9">Dormitorios y Baños</option>
                        <option value="10">Estacionamiento y Bodegas</option>
                        <option value="11">Material pisos</option>
                        <option value="12">Info Cocina</option>
                        <option value="13">Info Propiedad</option>
                        <option value="14">Exterior</option>
                        <option value="15">A. Comunes</option>
                        <option value="16">Visita y entrega</option>
                    </select>
                </div>

                <!-- btn btn-outline-success -->
                <input type="submit" name="save" class="save action-button" value="Guardar" />
            </fieldset>

                <!-- fieldset 2 -->
              <fieldset id="2">
                <h2 class="fs-title">Información Propietario</h2>
                <h3 class="fs-subtitle">Paso 2</h3>
                
                <input class="fs-nom" type="text" name="nombre" placeholder="Nombre*" id="nombre" onfocusout="validarNombre()"/>
                <input class="fs-ape" type="text" name="apellido" placeholder="Apellido*" id="apellido" onfocusout="validarApellido()"/>
                <input class="fs-rut" type="text" name="rut" placeholder="Rut*" id="rut" onfocusout="validarRut2()"/>
                <div class="select-container">
                    <select name="origen" id="origen" class="fs-ori">
                            <option value="" disabled selected>Origen*</option>
                            <option value="5" >Referido Conserje</option>
                            <option value="6" >Referido Cliente</option>
                            <option value="7" >Referido Corredor</option>
                            <option value="15" >No Definido</option>                        
                    </select>
                </div>
                <div class="form-group">
                <div class="selectDivisa-container">
                    <select name="sub_fono" id="sub_fono" class="fs-suFono">
                        <option value="" disabled selected>Prefijo*</option>
                        <option value="+569">+569</option>
                        <option value="+562">+562</option>
                        <option value="+568">+568</option>
                        <option value="+56">+56</option>
                    </select>
                </div>
                    <input class="fs-fono" type="tel" name="telefono" id="telefono" placeholder="Teléfono*" onfocusout="validarTelefono()"/>
                </div>

                <input type="mail" name="correo" id="correo" placeholder="Correo*" onfocusout="validarCorreo()"/>
                <label class="txtProp">Fecha</label>
                <input type="date" name="fecha" id="fecha" placeholder="Fecha" />

                <textarea name="infoPropVenta" id="infoPropVenta" cols="30" rows="3" placeholder="¿Por qué quieres vender o arrendar?" maxlength="1000"></textarea>

                <hr>
                <h3 class="fs-subtitle">Contactos Adicionales</h3>
                
                <div class="divC">
                    <ol>
                        <div id="contactos">
                          
                        </div>
                    </ol>
                   
                    
                    <a class="addC" id="addC" href="#" onclick="agregarContacto(); return false;"> Agregar contacto <i class="fas fa-plus-circle"></i></a>
                    <a class="delC" id="delC" href="#" onclick="eliminarContacto(); return false;"> Eliminar contacto <i class="fas fa-minus-circle"></i></a>
                </div>

                <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                <div class="selectPag-container">
                    <select name="" id="paginacion2" class="paginacion" onchange="paginacion(2)">
                        <option value="0" disabled selected>Paginación</option>  
                        <option value="1">Corredor y Operario</option>
                        <option value="3">Tipo Prop. y operación</option>
                        <option value="4">Dirección</option>
                        <option value="5">Pisos y ascensores</option>
                        <option value="6">Rol y Precios anexos</option>
                        <option value="7">Gasto común y otros</option>
                        <option value="8">Superficie y orientación</option>
                        <option value="9">Dormitorios y Baños</option>
                        <option value="10">Estacionamiento y Bodegas</option>
                        <option value="11">Material pisos</option>
                        <option value="12">Info Cocina</option>
                        <option value="13">Info Propiedad</option>
                        <option value="14">Exterior</option>
                        <option value="15">A. Comunes</option>
                        <option value="16">Visita y entrega</option>
                    </select>
                </div>

                <input type="submit" name="save" class="save action-button" value="Guardar" />
              </fieldset>

                <!-- fieldset 3 (1/11)-->
                <fieldset id="3">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (1/11)</h3>


                    <div class="container">
                        <div class="row">
                        
                            <div class="column col-4">
                                <label class="txtProp">
                                    Tipo de propiedad
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">Casa
                                    <input type="checkbox" id="casa" name="casa" onchange="tipo_prop()">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">Depto.
                                    <input type="checkbox" id="depto" name="depto" onchange="check_depto()">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div class="column col-4">
                                <label></label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">Oficina
                                    <input type="checkbox" id="of" name="of" onchange="oficina()">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">Terreno
                                    <input type="checkbox" id="terreno" name="terreno" onchange="check_terreno()">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div class="column col-4">
                                <label></label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">Local
                                    <input type="checkbox" id="local" name="local" onchange="local1()">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">Bodega
                                    <input type="checkbox" id="bodegaTipoProp" name="bodegaTipoProp" onchange="bodega()">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div class="column col-4">
                                <label></label>
                            </div>
                            <div class="column col-4">
                                <label class="fs-chkLbl">Parcela
                                    <input type="checkbox" id="parcela" name="parcela" onchange="parcela1()">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="column col-4">
                            </div>
                            
                        </div>
                    </div>

                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">No exclusivo</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                    <input type="checkbox" id="exclusividad" name="exclusividad" onchange="noExclusividad()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="pregExclusividad">

                        <div id="scCantCorredor" class="">
                            <select name="cantCorredor" id="cantCorredor" class="fs-tipoOpe" style="display:none;">
                                <option value="null" disabled selected>Cant. de corredores</option>
                                <option value="1" >1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                                <option value="6" >6</option>
                            </select>
                        </div>
                        
                        <div id="scTiempoPublicacion" class="">
                            <select name="select_tiempoPublicacion" id="select_tiempoPublicacion" class="fs-tipoOpe" style="display:none">
                                <option value="null" disabled selected>Tiempo de publicación</option>
                                <option value="1" >1 Semana</option>
                                <option value="2" >2 Semanas</option>
                                <option value="3" >3 Semanas</option>
                                <option value="4" >1 Mes</option>
                                <option value="5" >2 Meses</option>
                                <option value="6" >3 Meses</option>
                                <option value="7" >4 Meses</option>
                                <option value="8" >5 Meses</option>
                                <option value="9" >6 Meses</option>
                                <option value="10" >+6 Meses</option>
                            </select>
                        </div>
                    </div>

                    <div class="container" style="margin-top:25px;">
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">Amoblado</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                    <input type="checkbox" id="amoblado" name="amoblado">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div id="scBanco" class="pregHipoteca" style="display:none">
                        <div class="select-container">
                            <select name="select_banco" id="select_banco" class="fs-tipoOpe" onchange="" style="">
                                <option value="null" disabled selected>Seleccione un banco</option>
                                <?php
                                $sqlBancos = "SELECT * FROM Bancos order by banco asc";
                                $result = $conn->query($sqlBancos);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value='".$row["id_banco"]."'>".$row["banco"]."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="select-container">
                        <select name="select_op" id="select_op" class="fs-tipoOpe" onchange="valores()">
                            <option value="null" disabled selected>Tipo de operación</option>
                            <option value="1" >Venta</option>
                            <option value="2" >Arriendo</option>
                            <option value="3" >Ambos</option>
                        </select>

                    </div>

                    <div id="valor1" style="display:none">
                        <input class="fs-valor fs-tNumb" type="text" name="monto1" id="monto1" placeholder="Monto1" style="width:75%!important" onfocusout="validarPrecio('monto1')"/>
                        <select name="divisaMonto1" id="divisaMonto1" class="fs-divisa">
                                <option value="" disabled selected>Divisa</option>
                                <option value="1" >UF</option>
                                <option value="2" >CLP</option>
                            </select>
                    </div>
                    <div id="valor2" style="display:none">
                        <input class="fs-valor fs-tNumb" type="text" name="monto2" id="monto2" placeholder="Monto2" style="width:75%!important" onfocusout="validarPrecio('monto2')"/>
                        <select name="divisaMonto2" id="divisaMonto2" class="fs-divisa">
                                <option value="" disabled selected>Divisa</option>
                                <option value="1" >UF</option>
                                <option value="2" >CLP</option>
                            </select>
                    </div>

                    <div class="container" style="margin-top:25px; display: none" id="switch_hipoteca" >
                        <div class="row">
                            <div class="col-8">
                                <label class="txtProp">Hipoteca</label>
                            </div>
                            <div class="col-4">
                                <label class="switch">
                                    <input type="checkbox" id="hipoteca" name="hipoteca" onchange="hipotecaCheck()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- <div id="valorArr" style="display:none">
                    
                        <input class="fs-valor fs-tNumb" type="text" name="valorCLP" id="valorCLP" placeholder="Valor CLP" style="width:100%!important"  onfocusout="validarPrecio('valorCLP')"/>
                       
                    </div> -->

                    <!-- <div id="valorAmbos" style="display:none">
                        <input class="fs-valor fs-tNumb" type="text" name="precioVent" id="precioVent" placeholder="Valor Venta"  onfocusout="validarPrecio('precioVent')"/>
                        <div class="selectDivisa-container">
                            <select name="divisaVenta" id="divisaVenta" class="fs-divisa">
                                <option value="" disabled selected>Divisa</option>
                                <option value="1" >UF</option>
                                <option value="2" >CLP</option>
                            </select>
                        </div>

                        <input class="fs-valor fs-tNumb" type="text" name="precioArr" id="precioArr" placeholder="Valor Arriendo"  onfocusout="validarPrecio('precioArr')"/>
                        <div class="selectDivisa-container">
                            <select name="divisaArriendo" id="divisaArriendo" class="fs-divisa">
                                <option value="" disabled selected>Divisa</option>
                                <option value="1" >UF</option>
                                <option value="2" >CLP</option>
                            </select>
                        </div>
                    </div> -->
                    
                            <!-- <select name="divisa" id="divisa" class="fs-vaDivisa">
                                <option value="" disabled selected>Divisa</option>
                                <?
                                // $sqlDiv = "Select * from Divisa";
                                // $result = $conn->query($sqlDiv);
                                // if ($result->num_rows > 0) {
                                //     // output data of each row
                                //     while($row = $result->fetch_assoc()) {
                                //         echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                //     }
                                // } 
                                ?>
                            </select> -->
                                    
                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion3" class="paginacion" onchange="paginacion(3)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>
                    
                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (2/11)-->
                <fieldset id="4">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (2/11)</h3>
                        <div class="form-group">
                            <input class="fs-calle" type="text" name="calle" placeholder="Calle*" id="calle"/>
                            <input class="fs-numb fs-tNumb" type="number" name="numero" placeholder="Número*" id="numero" />
                            <input class="fs-letra" type="text" name="letra" placeholder="Aa/Dpto*" id="letra"/>
                        </div>

                            <!-- <input class="fs-condominio" type="text" name="condominio" placeholder="Condominio" /> -->
                            <table class="fs-table">
                                <tr class="fs-tr">
                                    <td class="fs-td2" id="switchCond1" style="display: none">Condominio</td class="fs-td">
                                    <td class="fs-td3" id="switchCond2" style="display: none">
                                        <label class="switch" id="switchCond3" style="display: none">
                                            <input type="checkbox" id="condominio" name="condominio" onchange="condominioF()">
                                            <span class="slider round" id="switchCond4" style="display: none"></span>
                                        </label>
                                    </td>
                                    <td class="fs-td2" id="switchLot1" style="display: none">Loteo</td class="fs-td">
                                    <td class="fs-td3" id="switchLot2" style="display: none">
                                        <label class="switch" id="switchLot3" style="display: none">
                                            <input type="checkbox" id="loteo" name="loteo" onchange="loteoF()">
                                            <span class="slider round" id="switchLot4" style="display: none"></span>
                                        </label>
                                    </td>
                                    <td class="fs-td3">
                                        <div class="selectCom-container">
                                            <select name="comuna" id="comuna" class="fs-comuna">
                                                <option value="null" disabled selected>Comuna*</option>
                                                <?php
                                                $sqlComuna = "Select * from Comuna order by nombre asc";
                                                $result = $conn->query($sqlComuna);
                                                if ($result->num_rows > 0) {
                                                    // output data of each row
                                                    while($row = $result->fetch_assoc()) {
                                                        echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                                    }
                                                } 
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                        
                        </tr></table>

                        <input class="fs-referencia" type="text" name="referencia" id="referencia" placeholder="Referencia" />
                        
                        <div class="form-group" style= "display: none" id="divCondominio">
                            <input class="fs-calle" type="text" name="calleCond" placeholder="Calle*" id="calleCond"/>
                            <input class="fs-numb fs-tNumb" type="number" name="numeroCond" placeholder="Número*" id="numeroCond" />
                            <input class="fs-letra" type="text" name="letraCond" placeholder="Aa/Dpto*" id="letraCond"/>
                        </div>

                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion4" class="paginacion" onchange="paginacion(4)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (3/11)-->
                <fieldset id="5">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (3/11)</h3>

                    <input class="fs-pDpto fs-tNumb" type="number" name="CantPisosEdificio" id="CantPisosEdificio" placeholder="Cant. de pisos del edificio"/>
                    <input class="fs-cAscrs fs-tNumb" type="number" name="cantAscrs" id="cantAscrs" placeholder="Cant. de ascensores"/>
                    <input class="fs-pDpto fs-tNumb" type="number" name="dptoPiso" id="dptoPiso" placeholder="Cant. de Dptos. por piso"/>
                    <input class="fs-pDpto fs-tNumb" type="number" name="pisoDpto" id="pisoDpto" placeholder="Piso Dpto."/>
                    <input class="fs-cDpto fs-tNumb" type="number" name="cantDpto" id="cantDpto" placeholder="Cant. de pisos del dpto."/>
                    
                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion5" class="paginacion" onchange="paginacion(5)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (4/11)-->
                <fieldset id="6">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (4/11)</h3>
                        <table class="fs-table">
                            <tr class="fs-tr arriendo" id="rolProp">
                                <td class="fs-td">Rol</td class="fs-td">
                                <td class="fs-td"><input class="fs-rol1 fs-tNumb" type="number" name="rol1" id="rol1" placeholder="" /></td>
                                <td class="fs-td"> - </td>
                                <td class="fs-td"><input class="fs-rol2 fs-tNumb" type="number" name="rol2" id="rol2" placeholder="" /></td>
                            </tr>
                        </table>
                        <div class="form-group">
                            <input class="fs-construccion fs-tNumb" type="number" name="construccion" id="construccion" placeholder="Año construcción" onfocusout="validarAno('construccion')"/>
                            <input class="fs-construccion fs-tNumb" type="number" name="adquisicion" id="adquisicion" placeholder="Año adquisición" onfocusout="validarAno('adquisicion')"/>
                        </div>
                        
                        <input class="fs-contribucion fs-tNumb arriendo" type="text"  name="contri" id="contri" placeholder="Valor Contribución" onfocusout="validarPrecio('contri')"/>
                        
                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion6" class="paginacion" onchange="paginacion(6)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (5/11)-->
                <fieldset id="7">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (5/11)</h3>
                    
                    <input class="fs-gastoC fs-tNumb" type="text" name="gastoC" id="gastoC" placeholder="Valor Gasto común" onfocusout="validarPrecio('gastoC')" />
                        <textarea name="gastos_comun" id="gastos_comun" cols="30" rows="3" placeholder="¿Qué Incluyen los Gastos Comunes?" maxlength="1000"></textarea>

                        <div class="select-container">
                            <select name="calefaccion" id="calefaccion" class="fs-nDorm">
                            <option value="" disabled selected>Tipo de calefacción</option>
                                <?php
                                    $sqlCalefa = "SELECT * FROM Tipo_calefaccion";
                                    $result = $conn->query($sqlCalefa);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo '<option value="'.$row["id"].'">'.utf8_encode($row["tipo"]).'</option>';
                                        }
                                    }
                                ?>
                            </select> 
                        </div>

                        <div class="select-container">
                            <select name="tipoGas" id="tipoGas" class="fs-nDorm">
                                <option value="" disabled selected>Tipo de gas</option>
                                <option value="1"> Licuado </option>
                                <option value="2">Natural </option>
                            </select>
                        </div>

                    <div class="select-container">
                        <select name="aguaCaliente" id="aguaCaliente" class="fs-nDorm">
                            <option value="" disabled selected>Agua caliente</option>
                            <?php
                            $sql = "SELECT * from Agua_caliente";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo "<option value='".$row["id_agua_caliente"]."'>".$row["tipo"]."</option>";
                                }
                            }
                            
                            ?>
                        </select>
                    </div>
                    
                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion7" class="paginacion" onchange="paginacion(7)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (6/11)-->
                <fieldset id="8">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (6/11)</h3>
                        
                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td3"><input class="fs-supT fs-tNumb" type="text" name="supT" id="supT" placeholder="Superficie total" onfocusout="validarPrecio('supT')" /></td>
                            <td class="fs-td3"><input class="fs-supU fs-tNumb" type="text" name="supU" id="supU" placeholder="Superficie util" onfocusout="validarPrecio('supU')"/></td>
                        </tr >

                        <tr class="fs-tr">
                            <td class="fs-td3"><input class="fs-supTerz fs-tNumb" type="text" name="supTerz" id="supTerz" placeholder="Superficie terraza" onfocusout="validarPrecio('supTerz')"/></td>
                            <!-- Dinámico -->
                                <!-- Cuando es casa -->
                            <td class="fs-td3" id="supTerrTd"><input class="fs-supTerr fs-tNumb" type="text" name="supTerr" id="supTerr" placeholder="Superficie terreno" onfocusout="validarPrecio('supTerr')"/></td>
                                <!-- Cuando es dpto -->
                            <td class="fs-td3" id="supPatTd" style="display:none"><input class="fs-supTerr fs-tNumb" type="number" name="supPatio" id="supPatio" placeholder="Superficie patio" /></td>
                        </tr>
                    </table>
                    <!-- Si se escribe en sup. Patio, debe aparecer -->
                    <div class="column col-12" id="usoGoceDiv" style="display:none">
                        <div class="fluid-container">
                            <div class="row">
                                <div class="column col-6">
                                    <label for="usoGoce">
                                        Uso y goce
                                    </label>
                                </div>
                                <div class="column col-6">
                                    <label class="switch">
                                        <input type="checkbox" id="usoGoce" name="usoGoce">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin dinamico -->
                    <!-- <input class="fs-pDpto fs-tNumb" type="number" name="CantPisosCasa" id="CantPisosCasa" placeholder="Cant. de pisos casa" style="display:none"/> -->

                    <div  class="form-group">
                        <div id="scCantPisosCasa" class="select-container">
                            <select name="CantPisosCasa" id="CantPisosCasa" class="fs-ori" style="display:none">
                                <option value="" disabled selected>Cant. de pisos casa</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>

                        <!-- <div class="form-group">
                            <div class="select-container">
                                <select name="orientacion" id="orientacion" class="fs-ori">
                                    <option value="" disabled selected>Orientación</option>
                                    <option value="Norte">Norte</option>
                                    <option value="Sur">Sur</option>
                                    <option value="Oriente">Oriente</option>
                                    <option value="Poniente">Poniente</option>
                                    <option value="N-O">N-O</option>
                                    <option value="N-P">N-P</option>
                                    <option value="S-O">S-O</option>
                                    <option value="S-P">S-P</option>
                                </select>
                            </div>
                        </div> -->

                        <div class="container">
                            <div class="row">
                                <div class="column col-4">
                                    <label class="txtProp">
                                        Orientación
                                    </label>
                                </div>

                                <div class="column col-4">
                                    <label class="fs-chkLbl">N
                                        <input type="checkbox" id="norte" name="norte" onchange="">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="column col-4">
                                    <label class="fs-chkLbl">S
                                        <input type="checkbox" id="sur" name="sur" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="column col-4">
                                    <label for=""></label>
                                </div>

                                <div class="column col-4">
                                    <label class="fs-chkLbl">O
                                        <input type="checkbox" id="oriente" name="oriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="column col-4">
                                    <label class="fs-chkLbl">P
                                        <input type="checkbox" id="poniente" name="poniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="column col-4">
                                    <label for=""></label>
                                </div>

                                <div class="column col-4">
                                    <label class="fs-chkLbl">NO
                                        <input type="checkbox" id="nOriente" name="nOriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="column col-4">
                                    <label class="fs-chkLbl">NP
                                        <input type="checkbox" id="nPoniente" name="nPoniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="column col-4">
                                    <label for=""></label>
                                </div>

                                <div class="column col-4">
                                    <label class="fs-chkLbl">SO
                                        <input type="checkbox" id="sOriente" name="sOriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="column col-4">
                                    <label class="fs-chkLbl">SP
                                        <input type="checkbox" id="sPoniente" name="sPoniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion8" class="paginacion" onchange="paginacion(8)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (7/11)-->
                <fieldset id="9">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (7/11)</h3>

                    <div class="select-container">
                            <select name="num_dorm" id="num_dorm" class="fs-nDorm" onchange="nDorm()">
                                <option value="" disabled selected>N° de dormitorios</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>

                        <table class="fs-table" id="dorm_aumentar">
                      
                        </table>

                        <div class="select-container">
                            <select name="cant_banos" id="cant_banos" class="fs-nBan" onchange="bannos()">
                                <option value="0" disabled selected>N° de baños</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>

                        <table class="fs-table" id="banos_aumentar">
                            
                        </table>
                    
                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion9" class="paginacion" onchange="paginacion(9)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (8/11)-->
                <fieldset id="10">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (8/11)</h3>
                        
                    <div class="select-container">
                        <select name="select_estacionamientos" id="select_estacionamientos" class="fs-nEst" onchange="estacionamientos()">
                            <option value="" disabled selected>Cantidad de estacionamientos</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    
                    <div class="form-group" id="estacionamientos">
                        
			        </div>

                    
                    <div class="select-container">
                        <select name="select_bodega" id="select_bodega" class="fs-nEst" onchange="bodegas()">
                            <option value="" disabled selected>Cantidad de bodega(s)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>

                    <div class="form-group" id="div_bodega">
                        
                    </div>
                      

                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion10" class="paginacion" onchange="paginacion(10)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (9/11)-->
                <fieldset id="11">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (9/11)</h3>
                        
                    <div class="select-container">
                        <select name="material_pisos_comunes" id="material_pisos_comunes" class="fs-nDorm">
                            <option value="" disabled selected>Material Pisos Espacio Comunes</option>
                            <option value="1">Alfombra</option>
                            <option value="2">Cemento</option>
                            <option value="3">Cerámica</option>
                            <option value="4">Flotante</option>
                            <option value="5">Granito</option>
                            <option value="6">Hormigón</option>
                            <option value="7">Loseta</option>
                            <option value="8">Madera</option>
                            <option value="9">Mármol</option>
                            <option value="10">Mosaico</option>
                            <option value="11">Parquet</option>
                            <option value="12">Piedra</option>
                            <option value="13">Porcelanato</option>
                            <option value="14">Otro</option>
                        </select>
                    </div>

                    <div class="select-container">
                        <select name="material_pisos_dorm" id="material_pisos_dorm" class="fs-nDorm">
                            <option value="" disabled selected>Material Pisos Dormitorios</option>
                            <option value="1">Alfombra</option>
                            <option value="2">Cemento</option>
                            <option value="3">Cerámica</option>
                            <option value="4">Flotante</option>
                            <option value="5">Granito</option>
                            <option value="6">Hormigón</option>
                            <option value="7">Loseta</option>
                            <option value="8">Madera</option>
                            <option value="9">Mármol</option>
                            <option value="10">Mosaico</option>
                            <option value="11">Parquet</option>
                            <option value="12">Piedra</option>
                            <option value="13">Porcelanato</option>
                        </select>
                    </div>

                    <div class="select-container">
                        <select name="material_pisos_bano" id="material_pisos_bano" class="fs-nDorm">
                            <option value="" disabled selected>Material Piso baños</option>
                            <option value="1">Baldosas</option>
                            <option value="2">Baldosín cerámico</option>
                            <option value="3">Cerámica</option>
                            <option value="4">Flexit</option>
                            <option value="5">Madera</option>
                            <option value="6">Mármol</option>
                            <option value="7">Parquet</option>
                            <option value="8">Piso flotante</option>
                            <option value="9">Piso termolaminado</option>
                            <option value="10">Porcelanato</option>
                            <option value="11">Piso fotolaminado</option>
                        </select>
                    </div>

                    <div class="select-container">
                        <select name="material_pisos_cocina" id="material_pisos_cocina" class="fs-nDorm">
                            <option value="" disabled selected>Material Piso Cocina</option>
                            <option value="1">Alfombra</option>
                            <option value="2">Cemento</option>
                            <option value="3">Cerámica</option>
                            <option value="4">Flotante</option>
                            <option value="5">Granito</option>
                            <option value="6">Hormigón</option>
                            <option value="7">Loseta</option>
                            <option value="8">Madera</option>
                            <option value="9">Mármol</option>
                            <option value="10">Mosaico</option>
                            <option value="11">Parquet</option>
                            <option value="12">Piedra</option>
                            <option value="13">Porcelanato</option>
                            <option value="14">Otro</option>
                        </select>
                    </div>

                    <div class="select-container">
                        <select name="material_pisos_terraza" id="material_pisos_terraza" class="fs-nDorm">
                            <option value="" disabled selected>Material Piso Terraza</option>
                            <option value="1">Alfombra</option>
                            <option value="2">Cemento</option>
                            <option value="3">Cerámica</option>
                            <option value="4">Flotante</option>
                            <option value="5">Granito</option>
                            <option value="6">Hormigón</option>
                            <option value="7">Loseta</option>
                            <option value="8">Madera</option>
                            <option value="9">Mármol</option>
                            <option value="10">Mosaico</option>
                            <option value="11">Parquet</option>
                            <option value="12">Piedra</option>
                            <option value="13">Porcelanato</option>
                            <option value="14">Otro</option>
                        </select>
                    </div>

                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion11" class="paginacion" onchange="paginacion(11)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (10.1/11)-->
                <fieldset id="12">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (10.1/11) Cocina</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row">

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaAme">
                                                            Cocina Americana
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="cocinaAme" name="cocinaAme">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaIsla">
                                                            Cocina Isla
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="cocinaIsla" name="cocinaIsla">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="encimeraGas">
                                                            Encimera Gas
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="encimeraGas" name="encimeraGas">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="encimeraElect">
                                                            Encimera Eléctrica
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="encimeraElect" name="encimeraElect">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaIntegrada">
                                                            Cocina integrada
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="cocinaIntegrada" name="cocinaIntegrada">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="estractor">
                                                            Estractor
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="estractor" name="estractor">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="hornoEmp">
                                                            Horno Empotrado
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="hornoEmp" name="hornoEmp">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="comedor">
                                                            Comedor de Diario
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="comedor" name="comedor">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cocinaAmoblada">
                                                            Cocina amoblada
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="cocinaAmoblada" name="cocinaAmoblada">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="lavavajillas">
                                                            Lavavajillas empotrado
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="lavavajillas" name="lavavajillas">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="microondas">
                                                            Microondas empotrado
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="microondas" name="microondas">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="refrigerador">
                                                            Refrigerador empotrado
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="refrigerador" name="refrigerador">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="despensa">
                                                            Despensa
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="despensa" name="despensa">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="logia">
                                                            Logia
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch" >
                                                            <input type="checkbox" id="logia" name="logia">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion12" class="paginacion" onchange="paginacion(12)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (10.2/11)-->
                <fieldset id="13">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (10.2/11) Otros propiedad</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row">

                                        <div class="column col-6 casa" >
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="atico">
                                                            Ático
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="atico" name="atico">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="hangaroa">
                                                            Cortina Hangaroa
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="hangaroa" name="hangaroa">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="roller">
                                                            Cortina Roller
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="roller" name="roller">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cortina_elec">
                                                            Cortinas Eléctricas
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="cortina_elec" name="cortina_elec">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="termoPanel">
                                                            Termo Panel
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="termoPanel" name="termoPanel">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="jacuzzi">
                                                            Jacuzzi
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="jacuzzi" name="jacuzzi">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="planchado">
                                                            Pieza de planchado
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="planchado" name="planchado">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_estar">
                                                            Sala de Estar
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="sala_estar" name="sala_estar">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="aire">
                                                            Aire acondicionado
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="aire" name="aire">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="escritorio">
                                                            Escritorio
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="escritorio" name="escritorio">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="terraza">
                                                            Malla Protección Terraza
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="terraza" name="terraza">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="citofono">
                                                            Citófono
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="citofono" name="citofono">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cerco_electrico">
                                                            Cerco eléctrico
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="cerco_electrico" name="cerco_electrico">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="hall">
                                                            Hall de acceso
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="hall" name="hall">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="alarma">
                                                            Sistema de Alarma
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="alarma" name="alarma">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6 dpto">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="ascensor">
                                                            Ascensor privado
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="ascensor" name="ascensor">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sauna">
                                                            Sauna
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="sauna" name="sauna">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_cine">
                                                            Sala de cine
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="sala_cine" name="sala_cine">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_juegos">
                                                            Sala de Juegos
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="sala_juegos" name="sala_juegos">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion13" class="paginacion" onchange="paginacion(13)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (10.3/11)-->
                <fieldset id="14">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle" id="10.3">Paso 3 (10.3/11) Exterior</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row" id="exterior">

                                        <!-- <div class="column col-6 casa">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="riego">
                                                            Riego Automático
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="riego" name="riego">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6 dpto">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="azotea">
                                                            Azotea habilitada
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="azotea" name="azotea">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6 dpto">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="gimnasio">
                                                            Gimnasio
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="gimnasio" name="gimnasio">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="piscina">
                                                            Piscina
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="piscina" name="piscina">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="areas_verdes">
                                                            Áreas verdes
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="areas_verdes" name="areas_verdes">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6 dpto">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="lavanderia">
                                                            Lavandería Edificio
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="lavanderia" name="lavanderia">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="atejardin">
                                                            Antejardín
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="atejardin" name="atejardin">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="quincho">
                                                            Quincho
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="quincho" name="quincho">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="piscina_temp">
                                                            Piscina temperada
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="piscina_temp" name="piscina_temp">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6 dpto">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="gourmet">
                                                            Gourmet room
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="gourmet" name="gourmet">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_multiuso">
                                                            Sala Multiuso
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="sala_multiuso" name="sala_multiuso">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="juegos_infantiles">
                                                            Juegos Infantiles
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="juegos_infantiles" name="juegos_infantiles">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="cancha_tenis">
                                                            Cancha de tenis
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="cancha_tenis" name="cancha_tenis">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="panelSolar">
                                                            Panel solar
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="panelSolar" name="panelSolar">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion14" class="paginacion" onchange="paginacion(14)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="15">A. Comunes</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (10.4/11)-->
                <fieldset id="15">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (10.4/11) Áreas comunes</h3>

                    <div class="container fs-14">
                        <div class="row">
                            <div class="column col-12">
                                <div class="fluid-container">
                                    <div class="row" id="aComunes">

                                        <!-- <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="conserjeria">
                                                            Conserjería 24Hrs
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="conserjeria" name="conserjeria">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="circuito">
                                                            Circuito cerrado de TV
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="circuito" name="circuito">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="sala_reuniones">
                                                            Sala de reuniones
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="sala_reuniones" name="sala_reuniones">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6 ">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="est_visita">
                                                            Estac. de Visita
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="est_visita" name="est_visita">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column col-6">
                                            <div class="fluid-container">
                                                <div class="row">
                                                    <div class="column col-6">
                                                        <label for="porton_elec">
                                                            Portón Eléctrico
                                                        </label>
                                                    </div>
                                                    <div class="column col-6">
                                                        <label class="switch">
                                                            <input type="checkbox" id="porton_elec" name="porton_elec">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                    <div class="selectPag-container">
                        <select name="" id="paginacion15" class="paginacion" onchange="paginacion(15)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="16">Visita y entrega</option>
                        </select>
                    </div>

                    <input type="submit" name="save" class="save action-button" value="Guardar" />
                </fieldset>

                <!-- fieldset 3 (11/11)-->
                <fieldset id="16">
                    <h2 class="fs-title">Información Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 3 (11/11)</h3>

                    <input class="fs-contribucion" type="text" name="disponibilidad" name="disponibilidad" placeholder="Disponibilidad visitas [Ej. L-V 11:00-17:00]" />

                    <div class="select-container ">
                        <select name="info_propiedad" id="info_propiedad" class="fs-nBan">
                            <option value="" disabled selected>Disponibilidad de entrega</option>
                            <option value="Inmediata">Inmediata</option>
                            <option value="1 semana">1 semana</option>
                            <option value="2 semana">2 semanas</option>
                            <option value="3 semanas">3 semanas</option>
                            <option value="1 mes">1 mes</option>
                            <option value="+1 mes">+1 mes</option>
                        </select>
                    </div>

                    <textarea name="nota" id="nota" cols="30" rows="4" placeholder="Notas" maxlength="1000"></textarea>
                    <textarea name="propDestacado" id="propDestacado" cols="30" rows="4" placeholder="¿Algo que quieras destacar de la propiedad?" maxlength="1000"></textarea>
                    <input class="fs-contribucion" type="text" id="url" name="url" placeholder="URL" />
                    
                    <table class="fs-table">
                    <tr class="fs-tr">
                            <td class="fs-td">Mascotas</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                    <input type="checkbox" id="mascotas" name="mascotas">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td class="fs-td">Cartel</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                    <input type="checkbox" id="cartel" name="cartel">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td class="fs-td">Llave</td class="fs-td">
                            <td class="fs-td">
                                <label class="switch">
                                    <input type="checkbox" id="llave" name="llave">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                        </tr>
                    </table>
                            <div>
                            <label for="imagen" >Imagen Propiedad:</label>
                            <input type="file" name="imagen" id="imagen"></div>
                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />

                    <div class="selectPag-containerIF">
                        <select name="" id="paginacion16" class="paginacion paginacionIF" onchange="paginacion(16)">
                            <option value="0" disabled selected>Paginación</option>  
                            <option value="1">Corredor y Operario</option>
                            <option value="2">Propietario</option>
                            <option value="3">Tipo Prop. y operación</option>
                            <option value="4">Dirección</option>
                            <option value="5">Pisos y ascensores</option>
                            <option value="6">Rol y Precios anexos</option>
                            <option value="7">Gasto común y otros</option>
                            <option value="8">Superficie y orientación</option>
                            <option value="9">Dormitorios y Baños</option>
                            <option value="10">Estacionamiento y Bodegas</option>
                            <option value="11">Material pisos</option>
                            <option value="12">Info Cocina</option>
                            <option value="13">Info Propiedad</option>
                            <option value="14">Exterior</option>
                            <option value="15">A. Comunes</option>
                        </select>
                    </div>

                    <input type="button" name="quit" id="quit" class="save action-button" value="Finalizar" />
                </fieldset>

        </form>
    </div>
</section>
    <?
    include '../pages/footer.php';
    ?>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script src="../js/toastr.js"></script>
    <script src="../js/formulario.js"></script>
    
    <script>
    // var area = document.getElementById("select_area");
    // if(area.value == "null"){
    //     alert("area vacía");
    //     area.focus();
    // }
    </script>
    <script>
    
    $( document ).ready(function() {
				tipo_prop();
                
			});
    </script>
</body>
</html>