<?php
    include "sidebarMenu.php";
    include "../conexion.php";
    $id=$_GET["id"];
    //obtener el id del corredor
    $idPer = $_SESSION["idPer"];
    $corredor;
    $sqlCorredor = "SELECT * from Corredor, Persona where id_persona = fk_persona and id_persona = $idPer";
    $result = $conn->query($sqlCorredor);

    if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $corredor = $row;
    }
    }else{
    }

    $sqlPerf = "SELECT * from Perfil_busqueda where fk_interesado = $id";
    $result = $conn->query($sqlPerf);

    if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $dormDesde = $row["dormDesde"];
                $dormHasta = $row["dormHasta"];
                $Edorm = $row["Edorm"];
                $bannosDesde = $row["bannosDesde"];
                $bannosHasta = $row["bannosHasta"];
                $Ebanno = $row["Ebanno"];
                $m_utilesDesde = $row["m_utilesDesde"];
                $m_utilesHasta = $row["m_utilesHasta"];
                $EmUtiles = $row["EmUtiles"];
                $precio_min = $row["precio_min"];
                // $precio_min = str_replace('.', '', $precio_min);
                $precio_max = $row["precio_max"];
                // $precio_max = str_replace('.', '', $precio_max);
                $comuna = $row["comuna"];
                $orientacion = $row["orientacion"];
                $fk_tipo_propiedad = $row["fk_tipo_propiedad"];
                $m_terrazaDesde = $row["m_terrazaDesde"];
                $m_terrazaHasta = $row["m_terrazaHasta"];
                $Eterraza = $row["Eterraza"];
                $m_terrenoDesde = $row["m_terrenoDesde"];
                $m_terrenoHasta = $row["m_terrenoHasta"];
                $Eterreno = $row["Eterreno"];
                $bodega = $row["bodega"];
                $Ebod = $row["Ebod"];
                $estacionamientoDesde = $row["estacionamientoDesde"];
                $estacionamientoHasta = $row["estacionamientoHasta"];
                $Eestacionamiento = $row["Eestacionamiento"];
                $piso_depto = $row["piso_depto"];
                $piscina = $row["piscina"];
                $Episcina = $row["Episcina"];
                $cocina_americana = $row["cocina_americana"];
                $EcocinaAme = $row["EcocinaAme"];
                $cocina_isla = $row["cocina_isla"];
                $EcocinaIsla = $row["EcocinaIsla"];
                $comedor_diario = $row["comedor_diario"];
                $EcomedorDiario = $row["EcomedorDiario"];
                $termopanel = $row["termopanel"];
                $Etermopanel = $row["Etermopanel"];
                $amoblado = $row["amoblado"];
                $Eamoblado = $row["Eamoblado"];
                $mascotas = $row["mascotas"];
                $Emascotas = $row["Emascotas"];
                $gimnasio = $row["gimnasio"];
                $Egim = $row["Egim"];
                $dorm_servicio = $row["dorm_servicio"];
                $Edorm_servicio = $row["Edorm_servicio"];
                $quincho = $row["quincho"];
                $Equincho = $row["Equincho"];
                $tipo_operacion = $row["fk_tipo_operacion"];
                $fk_interesado = $row["fk_interesado"];
    }
    } else {
        echo "No hay perfil";
    }
    $contErrores = 0;
    $sqlMatch = "SELECT distinct  Perfil_busqueda.fk_interesado, formulario.id_formulario, formulario.dormitorios AS form_dorm, formulario.cant_bannos, formulario.superficie_util, monto1, monto2, formulario.superficie_terraza, superficie_terreno, formulario.bodega, estacionamiento, formulario.piso_depto, formulario.piscina FROM
    Perfil_busqueda , formulario,Direccion 
    WHERE
    formulario.Direccion_id = Direccion.id AND 
    ";
    $comunas = explode("-", $comuna);
    if($fk_tipo_propiedad != ""){
        $sqlMatch .= " (formulario.fk_tipo_propiedad LIKE '$fk_tipo_propiedad') AND";
    }else{
        $contErrores++;
    }

    if($tipo_operacion != ""){
        $sqlMatch.=" (formulario.operacion LIKE '$tipo_operacion') AND";
    }else{
        $contErrores++;
    }
    // test
    $sqlMatch.= "(";
    for($i = 0; $i<count($comunas); $i++){
        if($i == 0){
            $sqlMatch .= " Direccion.fk_comuna LIKE $comunas[$i] ";
        }else{
            $sqlMatch .= "OR Direccion.fk_comuna LIKE $comunas[$i] ";
        }

    }
    $sqlMatch .=") AND ";
    // test
    if($dormDesde != "" && $dormHasta != ""){
    $sqlMatch .=" (formulario.dormitorios BETWEEN '$dormDesde' AND '$dormHasta') AND";
    }else{
        $contErrores++;
    }
    if($bannosDesde != "" && $bannosHasta != ""){
    $sqlMatch .= "(formulario.cant_bannos BETWEEN '$bannosDesde' AND '$bannosHasta') AND";
    }else{
        $contErrores++;
    }

    if($m_utilesDesde != "" && $m_utilesHasta != ""){
    $sqlMatch .= "(formulario.superficie_util BETWEEN $m_utilesDesde AND $m_utilesHasta) AND";
    }else{
        $contErrores++;
    }

    if($precio_min != "" && $precio_max != ""){
    $sqlMatch .= "(formulario.monto1 BETWEEN $precio_min AND $precio_max OR
    formulario.monto2 BETWEEN $precio_min AND $precio_max) AND
    ";
    }else{
        $contErrores++;
    }

    if($m_terrazaDesde != "" && $m_terrazaHasta != ""){
    $sqlMatch .= "(formulario.superficie_terraza BETWEEN '$m_terrazaDesde' AND '$m_terrazaHasta') AND";
    }else{
        $contErrores++;
    }

    if($m_terrenoDesde != "" && $m_terrenoHasta != ""){
    $sqlMatch .= "(formulario.superficie_terreno BETWEEN '$m_terrenoDesde' AND '$m_terrenoHasta') AND";
    }else{
        $contErrores++;
    }

    if($bodega != ""){
    $sqlMatch .= "(formulario.bodega = '$bodega') AND";
    }else{
        $contErrores++;
    }

    if($estacionamientoDesde != "" && $estacionamientoHasta != ""){
    $sqlMatch .= "(formulario.estacionamiento BETWEEN '$estacionamientoDesde' AND '$estacionamientoHasta') AND";
    }else{
        $contErrores++;
    }

    if($piscina != ""){
        if($Episcina == "1"){
            $sqlMatch .= "(formulario.piscina = '$piscina') AND";
        }else{
            $sqlMatch .= "(formulario.piscina = '1' OR formulario.piscina = '0') AND";
        }

    }else{
        $contErrores++;
    }

    if($cocina_americana != ""){
        if($EcocinaAme == "1"){
            $sqlMatch .= "(formulario.cocina_ame = '$cocina_americana') AND";
        }else{
            $sqlMatch .= "(formulario.cocina_ame = '1' OR formulario.cocina_ame = '0') AND";
        }
        
    }else{
        $contErrores++;
    }

    if($cocina_isla != ""){
        if($EcocinaIsla == "1"){
            $sqlMatch .= "(formulario.cocina_isla = '$cocina_isla') AND";
        }else{
            $sqlMatch .= "(formulario.cocina_isla = '1' OR formulario.cocina_isla = 0) AND";
        }
        
    }else{
        $contErrores++;
    }

    if($comedor_diario != ""){
        if($EcomedorDiario == "1"){
            $sqlMatch .= "(formulario.comedor_diario = '$comedor_diario') AND";
        }else{
            $sqlMatch .= "(formulario.comedor_diario = '1' OR formulario.comedor_diario = 0) AND";
        }
        
    }else{
        $contErrores++;
    }

    if($termopanel != ""){
        if($Etermopanel == "1"){
            $sqlMatch .= "(formulario.term_panel = '$termopanel') AND";
        }else{
            $sqlMatch .= "(formulario.term_panel = '1' OR formulario.term_panel = 0) AND";
        }
        
    }else{
        $contErrores++;
    }

    if($amoblado != ""){
        if($Eamoblado == "1"){
            $sqlMatch .= "(formulario.amoblado = '$amoblado') AND";
        }else{
            $sqlMatch .= "(formulario.amoblado = '1' OR formulario.amoblado = 0) AND";
        }
        
    }else{
        $contErrores++;
    }

    if($mascotas != ""){
        if($Emascotas == "1"){
            $sqlMatch .= "(formulario.mascotas = '$mascotas') AND";
        }else{
            $sqlMatch .= "(formulario.mascotas = '1' OR formulario.mascotas = 0) AND";
        }
        
    }else{
        $contErrores++;
    }

    if($gimnasio != ""){
        if($gimnasio == "1"){
            $sqlMatch .= "(formulario.gim = '$gimnasio') AND";
        }else{
            $sqlMatch .= "(formulario.gim = '1' OR formulario.gim = 0) AND";
        }
        
    }else{
        $contErrores++;
    }

    if($quincho != ""){
        if($Equincho == "1"){
            $sqlMatch .= "(formulario.quincho = '$quincho') AND";
        }else{
            $sqlMatch .= "(formulario.quincho = '1' OR formulario.quincho = 0) AND";
        }
        
    }else{
        $contErrores++;
    }

    $sqlMatch .= " formulario.aprobado = 1 AND Perfil_busqueda.fk_interesado = $id";

    $result = $conn->query($sqlMatch);
    $idsForm = array();
    if ($result->num_rows > 0 && $contErrores != 20) {
        while($row = $result->fetch_assoc()) {
            // codigo que identifica si estan descartadas las propiedades
            $idForm = $row["id_formulario"];
            $aux = 0;
            // recorrer archivo
            $archivo = '../js/json/'.$idForm . '-'. $_GET['id'] .'.txt';
            if(!file_exists($archivo)){ //si no existe el archivo significa que no ha sido descartado por nadie
                array_push($idsForm, $idForm);
                echo "<script>console.log('archivo ".$archivo." no existe')</script>";
            }else{
                $contenido = file_get_contents($archivo); //se guarda el contenido del archivo
                $ids = explode('-',$contenido); //se crea un arreglo con todos los ids guardados en el archivo
                echo "<script>console.log('archivo ".$archivo." existe')</script>";
                foreach($ids as $id){
                    if($id == $corredor["id_corredor"]){
                        $aux++;
                        echo "<script>console.log('".$id." = ".$fk_corredor."')</script>";
                    }
                }
                if($aux == 0){
                    array_push($idsForm, $idForm);
                    echo "<script>console.log('no esta en el archivo')</script>";
                }else{
                    echo "<script>console.log('si esta en el archivo')</script>";
                }
            }
            
        }
        $sinMatch = 0;
    }else{

        if($contErrores == 20){
            echo "<center>no hay datos suficientes para el match</center>";
            $sinMatch = 1;
        }else{
            echo "<center>no hay match</center>";
            $sinMatch = 1;
        }
        
    }
    if(count($idsForm) == 0){
        echo "<center>no hay match</center>";
            $sinMatch = 1;
    }
    $sql2 = "SELECT * FROM formulario WHERE id_formulario = $idForm";

    $result = $conn->query($sql2);

    // if ($result->num_rows > 0) {
    //     // output data of each row
    //     while($row = $result->fetch_assoc()) {
    //         $m_utiles = $row["superficie_util"];
    //         $m_total = $row["superficie_total"];

    //         $Direccion_id = $row["Direccion_id"];

    //         $sqlDir = "SELECT * from Direccion where id = $Direccion_id";
    //         $result2 = $conn->query($sqlDir);
    //         if ($result2->num_rows > 0) {
    //             // output data of each row
    //             while($row2 = $result2->fetch_assoc()) {
    //                 $direccion = $row2["calle"]." ".$row2["numero"];
    //                 $fk_comuna = $row2["fk_comuna"];
    //             }
    //         }

    //         $sqlComuna = "SELECT * from Comuna where id = $fk_comuna";
    //         $result3 = $conn->query($sqlComuna);
    //         if ($result3->num_rows > 0) {
    //             // output data of each row
    //             while($row3 = $result3->fetch_assoc()) {
    //                 $direccion .=", ".$row3["nombre"] ;
    //             }
    //         }
    //         $imagen =$row["imagen"];
    //         if(!file_exists($imagen)){
    //            $imagen = "../imagenesPropiedades/sinImg/casa.jpg"; 
    //         }
    //        $imagenHTML="<center><img src='$imagen'style='width:350px' ></center>";
    //     }
    // } else {
    //     echo "no match";
    // }
    $count = 1;
    foreach($idsForm as $id){
        // echo "<center>$count:$id</center>";
        $count = $count+1;
    }
    $sqlPer = "SELECT Persona.*, Interesado.* FROM Persona, Interesado WHERE Interesado.fk_persona = Persona.id_persona AND Interesado.id_interesado = $fk_interesado";
    $result = $conn->query($sqlPer);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $nombre = $row["nombre"];
            $apellido = $row["apellido"];
            $rut = $row["rut"];
            $telefono = $row["telefono"];
            $correo = $row["correo"];
            $fk_corredor = $row["fk_corredor"];
        }
    } else {
        
    }
    $sqlCorredor = "SELECT * from Persona, Corredor WHERE Persona.id_persona = Corredor.fk_persona and Corredor.id_corredor = $fk_corredor";
    $result = $conn->query($sqlCorredor);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $nomCorredor = $row["nombre"];
            $apeCorredor = $row["apellido"];
            $nomCompleto = $nomCorredor." ".$apeCorredor;
        }
    } else {
        
    }
?>

    <section id="#match" class="bg-light pt-4 pb-4">
        <div class="container-lg" id="master" style="<?=($sinMatch == 1)?'display:none':''?>">
            <!-- Datos interesado -->

            <div class="row">
                <div class="col-12">
                    <div class="container-matchCol">
                        <h3 class="h3-match p-0 m-0">Interesado</h3>
                        <hr class="hr-match">
                        <div class="container-fluid">
                            <div class="row interesado-match">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="nombre">Nombre: <?=$nombre?> <?=$apellido?></label><br>
                                    <label for="correo" style="width: 100%; display:flex;">Correo: <input type="text" id="correo" value="<?=$correo?>"class= "match-input" disabled></label>
                                    <label for="telefono" style="width: 100%; display:flex;">Telefono: <input type="text" id="telefono" value="<?=$telefono?>"class= "match-input" disabled></label>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="corredor">Corredor: <?=$nomCompleto?></label>
                                    <label for="">mi id: <?=$corredor["id_corredor"]?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Match -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="container-matchCol">
                    <h3 class="h3-match p-0 m-0">Match</h3>
                    <hr class="hr-match">
                        <div class="container-match" style="height:auto;">
                            <!-- Intento n°1 de carrusel -->
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <!-- Intento n°1 de carrusel -->
                                    <?php
                                        $cont = 0;
                                            foreach ($idsForm as $value) {
                                                $sqlform = "SELECT * from formulario where id_formulario = $value";
                                                $result = $conn->query($sqlform);
                                                if ($result->num_rows > 0) {
                                                    $cantFilas = $result->num_rows;
                                                    // output data of each row
                                                    while($row = $result->fetch_assoc()) {
                                                        // Datos para la card
                                                        $m_utiles = $row["superficie_util"];
                                                        $m_total = $row["superficie_total"];
                                                        $dormitorios = $row["dormitorios"];
                                                        $cant_bannos = $row["cant_bannos"];
                                                        $monto1 = $row["monto1"];
                                                        $monto2 = $row["monto2"];
                                                        $divisa1 = $row["divisa_monto1"];
                                                        $estacionamiento = $row["estacionamiento"];
                                                        $bodega = $row["bodega"];
                                                        $fk_tipo_propiedad = $row["fk_tipo_propiedad"];
                                                        $superficie_terraza = $row["superficie_terraza"];
                                                        $fk_corredor = $row["fk_corredor"];

                                                        $formulario = $row;
                                                        echo "<script>console.log('".$formulario["id_formulario"]."')</script>";

                                                        $sqlCorredor = "SELECT nombre , apellido FROM Persona, Corredor WHERE Persona.id_persona = Corredor.fk_persona AND id_corredor =$fk_corredor";
                                                        $result3 = $conn->query($sqlCorredor);
                                                        if ($result2->num_rows > 0) {
                                                            // output data of each row
                                                            while($row3 = $result3->fetch_assoc()) {
                                                                $nombreC = $row3["nombre"];
                                                                $apellidoCorredor = $row3["apellido"];
                                                            }
                                                        }


                                                        $idDirec = $row["Direccion_id"];
                                                        $sqlDir = "SELECT * from Direccion where id = $idDirec";
                                                        $result2 = $conn->query($sqlDir);
                                                        if ($result2->num_rows > 0) {
                                                            // output data of each row
                                                            while($row2 = $result2->fetch_assoc()) {
                                                                $direccion = $row2["calle"]." ".$row2["numero"];
                                                                $fk_comuna = $row2["fk_comuna"];
                                                            }
                                                        }else{
                                                            echo $sqlDir;
                                                        }
                                                
                                                        $sqlComuna = "SELECT * from Comuna where id = $fk_comuna";
                                                        $result3 = $conn->query($sqlComuna);
                                                        if ($result3->num_rows > 0) {
                                                            // output data of each row
                                                            while($row3 = $result3->fetch_assoc()) {
                                                                $direccion .=", ".$row3["nombre"] ;
                                                            }
                                                        }
                                                        $imagen =$row["imagen"];
                                                        if(!file_exists($imagen)){
                                                            $imagen = "../imagenesPropiedades/sinImg/casa.jpg"; 
                                                        }
                                                        $imagenHTML="
                                                        <div style='padding:30px 0;'>
                                                            <div style='text-align:center;'>
                                                                <img src='$imagen'style='width:350px;'>
                                                            </div>
                                                        </div>
                                                        ";
                                                    }
                                                }else{
                                                    echo $sqlform;
                                                }
                                    ?>
                                        <!--Carousel del Charlie -->
                                        <div class="carousel-item <?=($cont == 0)?'active':''?>"> 
                                            <!-- <aside class="product-card"> -->
                                                <div class="container">
                                                    <div class="row justify-content-center">
                                                        <!-- IMÁGEN -->
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 p-0">
                                                            <div class="img-container" style="background-image: url('<?=$formulario["imagen"]?>');">
                                                            </div>
                                                        </div>
                                                        <!-- INFORMACIÓN -->
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 p-0">
                                                            <div class="container-info">
                                                                <div class="row product-info">
                                                                    <!-- TÍTULO -->
                                                                    <div class="col-12 pb-2 pt-4">
                                                                        <div class="product-content">
                                                                            <h3 class="h3-prop" id="titulo-<?=$value?>" style="margin: 0; padding: 0">
                                                                                <?=$direccion?>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- BADGE -->
                                                                <div class="row pb-2">
                                                                    <div class="col-8" style="left:5%;">
                                                                        <?php
                                                                            $venta = '<span class="badge badge-danger badge-venta ml-1 mr-1">Venta</span>';
                                                                            $arriendo = '<span class="badge badge-info badge-arriendo ml-1 mr-1">Arriendo</span>';
                                                                            switch ($formulario["operacion"]) {
                                                                                case '1':
                                                                                    echo $venta;
                                                                                    break;
                                                                                case '2':
                                                                                    echo $arriendo;
                                                                                    break;
                                                                                
                                                                                default:
                                                                                echo $venta,$arriendo;
                                                                                    break;
                                                                            }  
                                                                        ?>
                                                                    </div>
                                                                    
                                                                    <div class="col-4">
                                                                        <span class="badge badge-match" style="">
                                                                            <?=($cont+1)."/".count($idsForm)?>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <!-- LISTA INFORMACIÓN -->
                                                                <div class="row justify-content-between">
                                                                    <div class="col-11">
                                                                        <ul class="list-group list-group-flush ml-4 ul-match">
                                                                            <li class=" list-group-item tipo-prop">Tipo de Propiedad:
                                                                                <?php
                                                                                    $sqlPropiedad = "SELECT * from tipo_propiedad where id_tipo_propiedad = ".$formulario["fk_tipo_propiedad"];
                                                                                    $result = $conn->query($sqlPropiedad);

                                                                                    if ($result->num_rows > 0) {
                                                                                        while($row = $result->fetch_assoc()) {
                                                                                            echo $row["tipo"];
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </li>
                                                                            <li class="list-group-item">Metros útiles:
                                                                                <?=$formulario["superficie_util"]?> m2</li>
                                                                            <li class="list-group-item">Metros totales:
                                                                                <?=$formulario["superficie_total"]?> m2</li>
                                                                            <li class="list-group-item">Baños:
                                                                                <?=$formulario["cant_bannos"]?>
                                                                            </li>
                                                                            <li class="list-group-item">Dormitorios:
                                                                                <?=$formulario["dormitorios"]?>
                                                                            </li>
                                                                            <li class="list-group-item">Precio:
                                                                                <?=($formulario["divisa_monto1"] == 1)?'':'$'?>
                                                                                <?=($formulario["monto1"] == "")?$formulario["monto2"]:$formulario["monto1"]?>
                                                                                <?=($formulario["divisa_monto1"] == 1)?'UF':''?>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                

                                                                <div class="row">
                                                                    <div class="col-12 pl-5">
                                                                        
                                                                        <label class="fs-chkLbl" for='<?=$value?>'>Marcar para enviar
                                                                            <input type="checkbox" name='<?=$value?>' id='<?=$value?>' onclick='ids(<?=$value?>)'>
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-12 pr-3 pl-5 mt-3 d-flex">
                                                                        <button class="btn btn-matchGreen descartar-match" style="width:40%; margin-bottom:3px;" onclick="verFicha(<?=$value?>,<?=$corredor['id_corredor']?>, <?=$_GET['id']?>)">Ver ficha</button>
                                                                        <button class="btn btn-matchBlue descartar-match" style="width:40%; margin-bottom:3px;" onclick="descartar(<?=$value?>,<?=$corredor['id_corredor']?>, <?=$_GET['id']?>)">Descartar</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <!-- </aside> -->
                                        </div>
                                    <?php
                                            $cont ++;
                                        }
                                    ?>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="false"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Mensaje -->
            <div class="row">
                <div class="col-12">
                    <div class="container-matchCol">
                        <h3 class="h3-match p-0 m-0">Enviar</h3>
                        <hr class="hr-match">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 ">
                                    <textarea name="mensaje" id="mensaje" cols="" rows="5" class="textarea-match">
Estimado(a) <?=$nombre?>,
Adjunto propiedad(es) seleccionadas según sus preferencias.
                                    </textarea>
                                    <textarea cols="40" rows="2" id="claves" name="claves" style="display:none">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- LISTA -->
            <div class="row">
                <div class="col-12">
                    <div class="container-matchCol">
                        <h3 class="h3-match p-0 m-0">Haz seleccionado:</h3>
                        <hr class="hr-match">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <ol id="lista" class="match-lista">
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

                <!-- BTN -->
            <div class="row">
                <div class="col-12">
                    <div class="container-matchCol">
                        <h3 class="h3-match pb-4 m-0 center"> Elija la opción para envíar su match</h3>
                        <div class="container-fluid">
                            <div class="row justify-content-between">
                                <button class="btn-gmail" name="EnvCorreo" type="button" value="EnvCorreo" id="EnvCorreo" for="EnvCorreo">
                                    <img class="img-gmail" src="../img/Gmail.png">Enviar por Correo
                                </button>
                                <button class="btn-wtsp" type="button" value="EnvWahts" id="EnvWahts" for="EnvWahts"> 
                                    <img class="img-wtsp" src="../img/whatsapp.png">Enviar por Whatsapp
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
        include 'footer.php';
    ?>
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script src="../js/match.js"></script>
    <script>
        $('.carousel').carousel({
            interval: false
        });
    </script>

</body>
</html>
