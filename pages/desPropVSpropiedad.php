<?
  include 'sidebarMenu.php';
  include '../conexion.php';
  
  $sql2 = "Update usuario SET ultima_pag='http://indev9.com/redflip/pages/desPropVSpropiedad.php' where id_usuario =". $_SESSION['id'];
  if ($conn->query($sql2) === TRUE) {
  } else {}

  include '../conexion.php';
?>


    <section>
        <div class="container container-prop col-11" style="padding-top:50px;">
            <!-- Título -->
            <div class="row">
                <div class="column col-12">
                    <h3 class="h3-prop mtb-50">Propietarios - Propiedades Deshabilitadas</h3>
                </div>
            </div>
            <!-- Acciones -->
            <div class="row">
                <div class="column col-xs-12 col-sm-12 col-md-6 col-lg-4">
                    <!-- Filtro -->
                    <form id="filtro">
                        <select name="tipo_operacion" id="tipo_operacion" class="d-none filtro filtroW3">
                  <option id="" disabled selected> Operación</option>
                  <option id="" value="">Todos</option>
                  <option id="venta">Venta</option>
                  <option id="arriendo">Arriendo</option>
                </select>

                        <select name="estado" id="estado" class="d-none filtro filtroW3">
                  <option id="" disabled selected> Estado</option>
                  <option id="" value="">Todos</option>  
                  <option id="" value="prospecto">Prospecto</option>
                  <option id="propietario" value="propietario">Propietario</option>
                </select>

                        <select name="fase" id="fase" class="d-none filtro filtroW3">
                  <option id="" disabled selected> Fase</option>
                  <option id=""  value="">Todos</option>
                </select>

                        <button type="button" id="btn_venta" class="btn btn-filt bg-redflip-red" onclick="filtros()">Filtrar</button>
                    </form>
                </div>
                <!-- Filtro Buscador -->
                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-4 v-align">
                    <input type="text" id=search placeholder="🔍 Buscar . . .">
                </div>
                <!-- Btn Exportar -->
                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <button type="submit" id="submitExport" class="btn btn-Imex bg-redflip-black pad-0" style="width:80%; padding:2px 5px; font-size:20px; margin:5px; float:right;" onclick="fnExcelReport()">
                Exportar
            </button>
                </div>
            </div>
        </div>
    </section>



    <div id=datos>

    </div>



    <form action="process.php" method="post" target="_blank" id="formExport">

        <input type="hidden" id="data_to_send" name="data_to_send" />

        <input type="hidden" id="nombre" name="nombre" value="Propietarios y Propiedades Deshabilitadas" />

    </form>



    <?

include 'footer.php';

?>

        <script src="../js/toastr.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>
        <script src="../js/buscarPropVSpropiedadDes.js"></script>

        <script src="../js/stacktable.js"></script>

        <script src="../js/accionProp.js"></script>

        <script>
            document.getElementById('submitExport').addEventListener('click', function(e) {

                e.preventDefault();

                let export_to_excel = document.getElementById('export_to_excel');

                let data_to_send = document.getElementById('data_to_send');

                data_to_send.value = export_to_excel.outerHTML;

                document.getElementById('formExport').submit();

            });
        </script>

        <script>
            if (screen.width < 1200) {

                $('table').stacktable();

                document.getElementById("formExport").stacktable();

            }

            console.log(screen.width);
        </script>

        <script src="../js/selectFase.js"></script>

        </body>