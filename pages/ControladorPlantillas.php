<?php

include 'sidebarMenu.php';

$prop = $_GET["prop"]; //casa, casaof, casapa, casate
$prop = strtolower($prop);
$op = $_GET["op"]; //arr, ven, ambos
$op = strtolower($op);



if($prop == "casa"){
    switch ($op) {
        case 'arr':
            casaArr();
            break;
        case 'ven':
            casaVen();
            break;
        case 'amb':
            casaAmbos();
            break;
    }
}else if($prop == "casaof"){
    switch ($op) {
        case 'arr':
            casaOfArr();
            break;
        case 'ven':
            casaOfVen();
            break;
        case 'amb':
            casaOfAmbos();
            break;
    }
}else if($prop == "casapa"){
    switch ($op) {
        case 'arr':
            casaPaArr();
            break;
        case 'ven':
            casaPaVen();
            break;
        case 'amb':
            casaPaAmbos();
            break;
    }
}else if($prop == "casate"){
    switch ($op) {
        case 'arr':
            casaTeArr();
            break;
        case 'ven':
            casaTeVen();
            break;
        case 'amb':
            casaTeAmbos();
            break;
    }
}else if($prop == "depto"){
    switch ($op) {
        case 'arr':
            deptoArr();
            break;
        case 'ven':
            deptoVen();
            break;
        case 'amb':
            deptoAmbos();
            break;
    }
}else if($prop == "deptoof"){
    switch ($op) {
        case 'arr':
            deptoOfArr();
            break;
        case 'ven':
            deptoOfVen();
            break;
        case 'amb':
            deptoOfAmbos();
            break;
    }
}else if($prop == "ofi"){
    switch ($op) {
        case 'arr':
            ofiArr();
            break;
        case 'ven':
            ofiVen();
            break;
        case 'amb':
            ofiAmbos();
            break;
    }
}else if($prop == "local"){
    switch ($op) {
        case 'arr':
            localArr();
            break;
        case 'ven':
            localVen();
            break;
        case 'amb':
            localAmbos();
            break;
    }
}else if($prop == "terr"){
    switch ($op) {
        case 'arr':
            terrArr();
            break;
        case 'ven':
            terrVen();
            break;
        case 'amb':
            terrAmbos();
            break;
    }
}else if($prop == "casaterr"){
    switch ($op) {
        case 'arr':
            casaTeArr();
            break;
        case 'ven':
            casaTeVen();
            break;
        case 'amb':
            casaTeAmbos();
            break;
    }
}else if($prop == "parcela"){
    switch ($op) {
        case 'arr':
            parcelaArr();
            break;
        case 'ven':
            parcelaVen();
            break;
        case 'amb':
            parcelaAmbos();
            break;
    }
}


function casaArr(){
// codigo de formulario depto
    echo "<script>console.log('casa arr')</script>";
    include_once 'plantillas/casa/CasaArr.php';
}
function casaVen(){
    echo "<script>console.log('casa ven')</script>";
    include_once 'plantillas/casa/CasaVen.php';
}
function casaAmbos(){
// codigo de formulario depto
    echo "<script>console.log('casa amb')</script>";
    include_once 'plantillas/casa/CasaAmb.php';
}


function casaOfArr(){
    echo "<script>console.log('casa ofi-casa amb')</script>";
    include_once 'plantillas/ofi-casa/OficinaArr.php';
}
function casaOfVen(){
    echo "<script>console.log('casa ofi-casa amb')</script>";
    include_once 'plantillas/ofi-casa/OficinaVen.php';
}
function casaOfAmbos(){
    echo "<script>console.log('casa ofi-casa amb')</script>";
    include_once 'plantillas/ofi-casa/OficinaAmb.php';
}


function casaPaArr(){

}
function casaPaVen(){
    
}
function casaPaAmbos(){
    
}


function casaTeArr(){
    echo "<script>console.log('terreno-casa amb')</script>";
    include_once 'plantillas/terreno-casa/TerrenoAmb.php';
}
function casaTeVen(){
    echo "<script>console.log('terreno-casa ven')</script>";
    include_once 'plantillas/terreno-casa/TerrenoAmb.php';
}
function casaTeAmbos(){
    echo "<script>console.log('terreno-casa arr')</script>";
    include_once 'plantillas/terreno-casa/TerrenoAmb.php';
}

// DEPTO

function deptoArr(){
    // codigo de formulario depto
    echo "<script>console.log('depto arr')</script>";
    include_once 'plantillas/depto/DeptoArr.php';
}
function deptoVen(){
    // codigo de formulario depto
    echo "<script>console.log('depto ven')</script>";
    include_once 'plantillas/depto/DeptoVen.php';
}
function deptoAmbos(){
    // codigo de formulario depto
    echo "<script>console.log('depto amb')</script>";
    include_once 'plantillas/depto/DeptoAmb.php';
}
    
    
    
    
    function deptoOfArr(){
        echo "<script>console.log('depto of arr')</script>";
        include_once 'plantillas/ofi-edificio/OficinaArr.php';
    }
    function deptoOfVen(){
        echo "<script>console.log('depto of ven')</script>";
        include_once 'plantillas/ofi-edificio/OficinaVen.php';
    }
    function deptoOfAmbos(){
        echo "<script>console.log('depto of amb')</script>";
        include_once 'plantillas/ofi-edificio/OficinaAmb.php';
    }

    // OFICINA

    function ofiArr(){
        // codigo de formulario depto
        echo "<script>console.log('ofi arr')</script>";
        include_once 'plantillas/ofi/OficinaArr.php';
    }
    function ofiVen(){
        // codigo de formulario depto
        echo "<script>console.log('ofi ven')</script>";
        include_once 'plantillas/ofi/OficinaVen.php';
    }
    function ofiAmbos(){
        // codigo de formulario depto
        echo "<script>console.log('ofi amb')</script>";
        include_once 'plantillas/ofi/OficinaAmb.php';
    }
    
    // local
    function localArr(){
        echo "<script>console.log('Local arr')</script>";
        include 'plantillas/local/LocalArr.php';
    }
    function localVen(){
        echo "<script>console.log('Local ven')</script>";
        include 'plantillas/local/LocalVen.php';
    }
    function localAmbos(){
        echo "<script>console.log('Local amb')</script>";
        include 'plantillas/local/LocalAmb.php';
    }

    //Terreno
    function terrArr(){
        echo "<script>console.log('Terreno Arr')</script>";
        include 'plantillas/terreno/TerrenoArr.php';
    }
    function terrVen(){
        echo "<script>console.log('Terreno ven')</script>";
        include 'plantillas/terreno/TerrenoVen.php';
    }
    function terrAmbos(){
        echo "<script>console.log('Terreno Amb')</script>";
        include 'plantillas/terreno/TerrenoAmb.php';
    }

    //Parcela
    function parcelaArr(){
        echo "<script>console.log('Parecela Arr')</script>";
        include 'plantillas/parcela/ParcelaArr.php';
    }
    function parcelaVen(){
        echo "<script>console.log('Parecela Ven')</script>";
        include 'plantillas/parcela/ParcelaVen.php';
    }
    function parcelaAmbos(){
        echo "<script>console.log('Parecela Amb')</script>";
        include 'plantillas/parcela/ParcelaAmb.php';
    }
?>