<?php 
include 'valid_session.php';
include '../controlador/mcript.php'; 
?>

<!DOCTYPE html>
<html lang="es-cl">
<head>
    <!--=== Required meta tags ===-->
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <!--=== Title ===-->
     <title></title>
     <!--=== Favicon Icon ===-->
     <link rel="icon" href="../favicon.ico" type="image/x-icon" />
     <!--=== Bootstrap CSS ===-->
     <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
     <!--=== Font Awesome ===-->
     <link rel="stylesheet" type="text/css" href="../css/all.min.css">
     <link rel="" type="" href="fonts/FontAwesome.otf">
     <!--=== Style CSS ===-->
     <link rel="stylesheet" type="text/css" href="../css/menu.css">
     <link rel="stylesheet" type="text/css" href="../css/main.css">
     <link href="http://cdn.grapecity.com/spreadjs/hosted/css/gc.spread.sheets.excel2013white.10.1.0.css" rel="stylesheet" type="text/css" />  
    <script type="text/javascript" src="http://cdn.grapecity.com/spreadjs/hosted/scripts/gc.spread.sheets.all.10.1.0.min.js"></script>  
    <script type="text/javascript" src="http://cdn.grapecity.com/spreadjs/hosted/scripts/interop/gc.spread.excelio.10.1.0.min.js"></script>
</head>
<body>



<div id="menu">
		<div class="hamburger">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
		</div>
		<div class="menu-inner">
			
			<ul>
                <a href="index.php">
				<li>Índice</li>
                </a>
<!-- 
                <li class="dropdown-btn">
                    Title Subs
                </li>

                <ul class="dropdown-container">
                        <li>SUB 1</li>
                        <li>SUB 2</li>
                        <li>SUB 3</li>
                    </ul> -->

                <a href="AgregarPropietario.php">
				<li>Agregar Propietario</li>
                </a>
				<a href="PropietariosAct.php">
				<li>Propietarios Activos</li>
                </a>
				<a href="PropietariosDes.php">
				<li>Propietarios Deshabilitados</li>
                </a>
				<a href="AgregarPropiedades.php">
				<li>Agregar Propiedad</li>
                </a>
				<a href="propVSpropiedad.php">
				<li>Propiedades Activas</li>
                </a>
				<a href="PropietariosDes.php">
				<li>Propiedades Deshabilitadas</li>
                </a>
			</ul>
		</div>

		<svg version="1.1" id="blob"xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<path id="blob-path" d="M60,500H0V0h40c0,0,20,172,20,250S60,900,60,500z"/>
		</svg>
	</div>


    <!--=== JS ===-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/all.min.js"></script>


    <script src="../js/menu.js"></script>

    <script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>









    <!-- <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });

        function closeNav(){
            document.getElementById("sidebar").className="";
        }
    </script>

<script>
try{
    var pagina = document.getElementById("pagina").value;
    if(pagina === 'agregar'){
        document.getElementById("nav-1").className = "active";
    }else if(pagina === 'inactivo'){
        document.getElementById("nav-2").className = "active";
    }else if(pagina === 'activo'){
        document.getElementById("nav-3").className = "active";
    }
}catch(e){
    
}
</script> -->