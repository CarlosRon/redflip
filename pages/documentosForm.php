<?php
    include 'sidebarMenu.php';
    include '../conexion.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-12" style="padding-top:50px;">
                <h3 class="h3-prop mtb-50">Subir Archivos</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <form method="post" enctype="multipart/form-data" id="ingresoDoc">
            <label for="">Titulo:</label>
            <input type="text" name="tituloDoc" placeholder="" id="tituloDoc" require="">
            <br>

            <label for="">Categoría:</label>
            <select name="categoriaDoc" placeholder="" id="categoriaDoc" require="" form="ingresoDoc">
            <option value="" disabled selected>Seleccionar Categoría</option>
            <?php
                $sqlCategoriaDoc = "SELECT * from categoria_documento WHERE id NOT IN (0)";
                $result = $conn->query($sqlCategoriaDoc);
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                    echo '<option value="'.$row["id"].'">'.utf8_encode($row["nombre"]).'</option>';
                    }
                }
            ?>
        </select>
            <a href="#" onclick="crearCategoria()">Crear Categoría Nueva</a>
            <br>

            <label for="">Archivo:</label>
            <input type="file" value="Seleccionar" name="archivoDoc" id="archivoDoc">
            <br>
            <br>
            <button>Subir Archivo</button>
        </form>
    </div>
    <script src="../js/docs.js"></script>
    <script>
        function crearCategoria() {
            window.open("documentosCategoria.php", "Categoria Nueva", "width=500, height=600");
        }
    </script>
    <?php 
    include 'footer.php';
?>