<?php
include 'sidebarMenu.php';
include '../conexion.php';
?>
    <section>
        <div class="container" style="padding-top: 50px;">
            <div class="row">
                <div class="column col-12" style="padding-top:50px;">
                    <h3 class="h3-prop mtb-50">Mis formularios</h3>
                </div>
                <div class="column col-12">
                    <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <input type="text" id=search placeholder="🔍 Buscar . . .">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="column col-12">
                    <ol class="rectangle-list">
                        <!-- <li>
                        <label href="" class="listForm"><label class="overflow">List itemList item List item List item List item List item</label>
                            <button class="btn-acciones bg-redflip-red min-p fr-l" title="Eliminar"><i class="fas fa-times"></i></button>
                            <button class="btn-acciones bg-redflip-turquoise min-p fr-l" title="Modificar" ><i class="fas fa-edit"></i></button>
                            <button class="btn-acciones bg-redflip-form min-p fr-l" title="Ver formulario" ><i class="far fa-file-alt"></i></button>
                        </label> -->
                        <?php
                        $sql = "SELECT *, formulario.fk_tipo_propiedad as tipoProp from formulario, Propiedad where formulario.enviado = 0 and formulario.aprobado = 0 and usuario = ".$_SESSION['id']." AND formulario.id_formulario = Propiedad.fk_formulario AND formulario.usuario = ".$_SESSION['id']." order by id_formulario";
                        $result = $conn->query($sql);
                        // echo "$sql";
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                $nomform = $row["nombre_form"];
                                $nomform = utf8_encode($nomform);
                                $nomform = stripslashes($nomform);
                                $estado = $row["fk_estado"];
                                $cont = 1;
                                //para el editar
                                $prop = $row["tipoProp"];
                                switch ($prop) {
                                    case '1':
                                        $prop = "depto";
                                        break;
                                    case '2':
                                        $prop = "casa";
                                        break;
                                    case '3':
                                        $prop = "local";
                                        break;
                                    case '4':
                                        $prop = "ofi";
                                        break;
                                    case '5':
                                        $prop = "terr";
                                        break;
                                    default:
                                    $prop = "casa";
                                }

                                $op = $row["operacion"];
                                switch ($op) {
                                    case '1':
                                        $op = "ven";
                                        break;
                                    case '2':
                                        $op = "arr";
                                        break;
                                    case '3':
                                        $op = "amb";
                                        break;
                                }
                                
                                switch ($estado){
                                    case 1:
                                        $impr = '<span><i class="fas fa-circle ci-yellow ci-mob"></i><span class="statusFormList">Nuevo</span>';
                                        break;
                                    case 2:
                                        $impr = '<span><i class="fas fa-circle ci-green ci-mob"></i><span class="statusFormList">Aprobado</span>';
                                        break;
                                    case 3:
                                        $impr = '<span><i class="fas fa-circle ci-red2 ci-mob"></i><span class="statusFormList">Rechazado</span>';
                                        break;
                                    case 4:
                                        $impr = '<span><i class="fas fa-circle ci-cyan ci-mob"></i><span class="statusFormList">Modificado</span>';
                                        break;
                                    default:
                                        $impr='<span><i class="fas fa-circle ci-yellow ci-mob"></i><span class="statusFormList">Nuevo</span>';
                                        break;
                                }

                                echo '  <li>
                                            <label href="" onclick="return false;" class="listForm"><label class="overflow" title="'.$nomform.'">'.$nomform.'</label>
                                                '.$impr.'
                                                <button style="" class="btn-acciones bg-redflip-red min-p fr-l" title="Eliminar" onclick="eliminarForm('.$row["id_formulario"].')"><i class="fas fa-times"></i></button>
                                                <button class="btn-acciones bg-redflip-turquoise min-p fr-l" title="Modificar" onclick="editarForm('.$row["id_formulario"].', `'.$prop.'`, `'.$op.'`)"><i class="fas fa-edit"></i></button>
                                                <button class="btn-acciones bg-redflip-form min-p fr-l" title="Ver formulario" onclick="redirigir2('.$row["id_formulario"].')"><i class="far fa-file-alt"></i></button>
                                            </label>
                                        </li>';
                            }
                        } else {
                            echo "No tiene Formularios :(";
                        }
                        $conn->close();
                        ?>
                            <!-- </li>
                     <li>
                        <label href="" class="listForm"><label class="overflow">List itemList item List item List item List item List item</label>
                            <button class="btn-acciones bg-redflip-red min-p fr-l" title="Eliminar"><i class="fas fa-times"></i></button>
                            <button class="btn-acciones bg-redflip-turquoise min-p fr-l" title="Modificar" ><i class="fas fa-edit"></i></button>
                            <button class="btn-acciones bg-redflip-form min-p fr-l" title="Ver formulario" ><i class="far fa-file-alt"></i></button>
                        </label>
                    </li>
                     <li>
                        <label href="" class="listForm"><label class="overflow">List itemList item List item List item List item List item</label>
                            <button class="btn-acciones bg-redflip-red min-p fr-l" title="Eliminar"><i class="fas fa-times"></i></button>
                            <button class="btn-acciones bg-redflip-turquoise min-p fr-l" title="Modificar" ><i class="fas fa-edit"></i></button>
                            <button class="btn-acciones bg-redflip-form min-p fr-l" title="Ver formulario" ><i class="far fa-file-alt"></i></button>
                        </label> -->
                            </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <?php
include 'footer.php';
?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>
        <script>
            function editarForm(idForm, prop, op) {
                console.log(idForm);
                window.location = `ControladorPlantillasEditar.php?id=${idForm}&prop=${prop}&op=${op}`;
            }

            function eliminarForm(idForm) {
                Swal.fire({
                    title: '¿Estas seguro de eliminar este Formulario?',
                    text: "No se podrá recuperar",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.value) {
                        fetch(`../controlador/eliminarForm.php?idForm=${idForm}`)
                            .then(res => res.json())
                            .then(data => {
                                console.log(data)
                                location.reload();
                            })
                    } else {
                        console.log("cancelado")
                    }
                });

            }
            var statusFormList = document.getElementsByClassName("statusFormList");
            for (let i = 0; i < statusFormList.length; i++) {
                if (screen.width < 576) {
                    statusFormList[i].style.display = "none";
                } else {
                    statusFormList[i].style.display = "";
                }
            }
        </script>
        </body>

        </html>