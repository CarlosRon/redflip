<?php
include 'sidebarMenu.php';
$op = $_GET["op"];
?>

<div class="container">
<div class="row"> 
<div class="col-4 bSP1" onclick="" name="casa" id="casa"> <br>
<form action="ControladorCasa.php" method="GET" id="formCasa" >
<fieldset id="1" class="casa active" >
<label for="">Casa</label>
</fieldset >
<!-- Comienzo Radio buttons Casa -->
<fieldset id="2">
    
        <input type="hidden" name="op" value="<?=$op?>">
        <div id="GrupoC" style="" class="">
        <i class="fa fa-times btnCerrar" aria-hidden="true" ></i>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="prop" id="radioC1" value="casa" checked >
                <label class="form-check-label" for="radioC1">
                    Casa
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="prop" id="radioC2" value="option2">
                <label class="form-check-label" for="radioC2">
                    Casa Oficina
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="prop" id="radioC3" value="option3">
                <label class="form-check-label" for="radioC3">
                    Casa Parcela
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="prop" id="radioC4" value="option4">
                <label class="form-check-label" for="radioC4">
                    Casa Terreno
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
       
    </form>
</fieldset>

<!-- Fin Radio Button Casa -->
</div> 
<div class="col-4 bSP2" onclick="MostrarOcultar2()">Departamento <br>

<!-- Comienzo Radio Button Dpto -->
<form action="ControladorDepto.php" method="GET">
  <input type="hidden" name="op" value="<?=$op?>" >
<div id="GrupoD" style="display:none" class="animated fadeIn">
<div class="form-check">
  <input class="form-check-input" type="radio" name="prop" id="radioD1" value="depto" checked>
  <label class="form-check-label" for="radioD1">
    Departamento
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="radio" name="prop" id="radioD2" value="option2">
  <label class="form-check-label" for="radioD2">
    Departamento Oficina
  </label>
</div>
<button type="submit" class="btn btn-primary" >Enviar</button>
</div>
</form>
<!-- Fin Radio Button Dpto -->
</div>
<div class="col-4 bSP3" onclick="MostrarOcultar3()">Oficina <br>


<!-- Comienzo Radio Button Oficina -->
<div id="GrupoO" style="display:none" class="animated fadeIn">
<div class="form-check">
  <input class="form-check-input" type="radio" name="radioO" id="radioO1" value="option1" checked>
  <label class="form-check-label" for="radioO1">
    Oficina
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="radio" name="radioO" id="radioO2" value="option2">
  <label class="form-check-label" for="radioO2">
    Oficina Casa
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="radio" name="radioO" id="radioO3" value="option3">
  <label class="form-check-label" for="radioO3">
    Oficina Departamento
  </label>
</div>
</div>
<!-- Fin Radio Button Oficina -->
</div>
</div>
<div class="w-100"></div>
<div class="row">
  <div class="col-3 bSP4" onclick="MostrarOcultar4()" >Parcela <br>
 

<!-- Comienzo Radio Button Parcela -->
<div id="GrupoP" style="display:none" class="animated fadeIn">
<div class="form-check">
  <input class="form-check-input" type="radio" name="radioP" id="radioP1" value="option1" checked>
  <label class="form-check-label" for="radioP1">
    Parcela
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="radio" name="radioP" id="radioP2" value="option2">
  <label class="form-check-label" for="radioP2">
    Parcela Casa
  </label>
</div>
</div>
<!-- Fin Radio Button Parcela -->
</div>
  <div class="col-3 bSP5" onclick="MostrarOcultar5()">Terreno <br>
 
  
<!-- Comienzo Radio Button Terreno -->
<div id="GrupoT" style="display:none" class="animated fadeIn">
<div class="form-check">
  <input class="form-check-input" type="radio" name="radioT" id="radioT1" value="option1" checked>
  <label class="form-check-label" for="radioT1">
    Terreno
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="radio" name="radioT" id="radioT2" value="option2">
  <label class="form-check-label" for="radioT2">
    Terreno Casa
  </label>
</div>
</div>
<!-- Fin Radio Button Terreno -->
</div>
  <div class="col-3 bSP6">Local Comercial </div>
  <div class="col-3 bSP7">Bodega </div>
</div>
</div>

<script>
function MostrarOcultar1() {
  var x = document.getElementById("GrupoC");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function MostrarOcultar2() {
  var x = document.getElementById("GrupoD");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function MostrarOcultar3() {
  var x = document.getElementById("GrupoO");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function MostrarOcultar4() {
  var x = document.getElementById("GrupoP");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
 <script>
function MostrarOcultar5() {
  var x = document.getElementById("GrupoT");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<?php
 include 'footer.php';
?>
<script src="../js/testCubitos.js"></script>