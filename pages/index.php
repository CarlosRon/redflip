<?php
		include 'sidebarMenu.php';
		include '../conexion.php';
		
		$sql2 = "Update usuario SET ultima_pag='http://indev9.com/redflip/pages/index.php' where id_usuario =". $_SESSION['id'];
		if ($conn->query($sql2) === TRUE) {
		} else {
		}
		$sqlProp="SELECT DISTINCT count(id_propietario) as cont
		, Estado_prop.tipo from Persona INNER JOIN Propietario 
		, usuario, Estado_prop
		, Sub_estado, Origen where Persona.id_persona = usuario.fk_persona 
		AND usuario.fk_estado_us = 1 
		AND Propietario.fk_persona = Persona.id_persona 
		AND Estado_prop.id = Propietario.fk_estado_prop 
		AND Sub_estado.id = Estado_prop.fk_sub_estado
		AND Origen.id_origen = Propietario.fk_origen
		AND Propietario.aprobado = 1
		order by Propietario.id_propietario asc";
		$result = $conn->query($sqlProp);

		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$propietarios=$row["cont"];
			}
		} else {
			echo $sqlProp;
		}

		$sqlPropiedad = "SELECT distinct COUNT(id_formulario) as cont
		FROM Persona per
		INNER JOIN Propietario propietario
		, Estado_prop est
		, Sub_estado sub
		, Propiedad propiedad
		, Divisa divi, Direccion dir
		, Comuna com    
		, Venta_Arriendo vent
		, Origen    
		, Estatus 
		, formulario
		WHERE per.id_persona = propietario.fk_persona
		AND propietario.fk_estado_prop = est.id
		AND est.fk_sub_estado = sub.id
		AND propietario.id_propietario = propiedad.fk_propietario
		AND propiedad.divisa = divi.id
		AND propiedad.fk_direccion = dir.id
		and dir.fk_comuna = com.id
		AND propiedad.fk_venta_arriendo = vent.id
		AND propietario.fk_origen = Origen.id_origen
		AND propiedad.fk_status = Estatus.id_status
		AND propiedad.fk_estado_propiedad = 1
		AND propiedad.fk_formulario = formulario.id_formulario
		AND formulario.aprobado = 1
		order by propiedad.id_propiedad asc";
		$result = $conn->query($sqlPropiedad);

		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$propiedad=$row["cont"];
			}
		} else {
			echo $sqlPropiedad;
		}

		$categoriaDoc = array();
		$sql = "SELECT * FROM categoria_documento WHERE id NOT IN (0)";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				array_push($categoriaDoc, $row);
			}
		} else {
			echo $sql;
		}

		
?>


    <section class="pt-4 bg-light">
        <div class="container container-fluid-md container-fluid-sm container-fluid-xs">
            <div class="row">
                <!-- PHP ID Corredor -->
                <?php 
					$sqlCorredor = "SELECT id_corredor from Corredor where fk_persona =". $_SESSION["idPer"];
						$result = $conn->query($sqlCorredor);
						if ($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
							$idCorredor = $row["id_corredor"];
							
								echo "<input type='hidden' value='$idCorredor' name='idCorredor' id='idCorredor' />";
						}
						} else {
							// no hay resultados
							echo "<input type='hidden' value='0' name='idCorredor' id='idCorredor' />";
						}
				?>

                <!-- INICIO MATCH -->
                <div class="col-12 mt-5 mb-2 ">
                    <div class="container-matchCol">
                        <span class="d-flex align-items-center">
							<span>
								<img src="../img/match.svg" alt="" style="width:32px;">
							</span>
                        <h3 class="h3-match p-0 m-0 pl-2"> Match</h3>
                        </span>
                        <!-- ESTILO DEL MATCH -->
                        <table id="match" class="table-match">
                            <thead class="thead-match">
                                <tr class="tr-matchHead">
                                    <th>#</th>
                                    <th>Descripción</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- FIN MATCH -->

				<!-- INICIO PORTALINMOBILIARIO -->
				<div class="col-12 mt-5 mb-2 ">
                    <div class="container-matchCol">
                        <span class="d-flex align-items-center">
							<span>
								<img src="../img/match.svg" alt="" style="width:32px;">
							</span>
                        	<h3 class="h3-match p-0 m-0 pl-2"> Descarga Visitas PortalInmobiliario</h3>
                        </span>

						<select name="fecha" id="fecha">
							<option value="" disabled selected>Seleccione una fecha</option>
							<?php
								$fecha_actual = date("d-m-Y");
								for($i=1; $i< 8; $i++){
									echo "<option value='".date("Y-m-d", strtotime($fecha_actual."- ".$i." days"))."'>".date("d-m-Y", strtotime($fecha_actual."- ".$i." days"))."</option>";
								}
						
							?>
						</select>
            			<button id="exportar" class="btn-matchGreen" onclick="">exportar a excel</button>
                    </div>
                </div>
				
				<!-- FIN PORTALINMOBILIARIO -->

                <!-- INICIO GRÁFICOS -->
                <!-- Inicio Grafico de Torta -->
                <div class="col-md-6 col-lg-6 xl-6 mb-2">
                    <div class="container-matchCol">
                        <span class="d-flex align-items-center">
							<span>
								<img src="../img/movProp.svg" alt="" style="width:32px;">
							</span>
                        <h3 class="h3-match p-0 m-0 pl-2"> Prop. por comuna</h3>
                        </span>
                        <p class="p-subMatch">Propiedades / Comuna</p>
                        <div>
                            <div id="chartPreferences" class="ct-chart ct-perfect-fourth center">
                                <canvas id="graficoTorta" responsive="true" ></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Grafico Torta -->
                <!-- Inicio Grafico de Barra -->
                <div class="col-md-6 col-lg-6 xl-6 mb-2">
                    <div class="container-matchCol">
                        <span class="d-flex align-items-center">
							<span>
								<img src="../img/movProp.svg" alt="" style="width:32px;">
							</span>
                        <h3 class="h3-match p-0 m-0 pl-2"> Prop. por tipo</h3>
                        </span>
                        <p class="p-subMatch">T. Propiedad / T. Operación</p>
                        <div>
                            <div id="chartPreferences2" class="ct-chart ct-perfect-fourth center">
                                <canvas id="graficoBarras" responsive="true" width="" height=""></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Grafico de Barra -->
                <!-- FIN GRÁFICOS -->

                <!-- INICIO CONTENEDOR ARCHIVOS/TUTORIAL -->
                <!-- Descarga de archivos -->
                <div class="col-md-6 col-lg-6 mb-2">
                    <div class="container-matchCol">
                        <span class="d-flex align-items-center">
							<span>
								<img src="../img/descArr.svg" alt="" style="width:32px;">
							</span>
                        <h3 class="h3-match p-0 m-0 pl-2"> Descarga de archivos</h3>
                        </span>

                        <div class="list-group scroll-matchArchivos">
                            <?php
								$cont = 1;
								foreach($categoriaDoc as $valor){
							?>
                                <div class="accordion" id="accordion<?=$cont?>">
                                    <div id="heading<?=$cont?>">
                                        <button class="btn-matchArchivos collapsed" type="button" data-toggle="collapse" data-target="#collapse<?=$cont?>" aria-expanded="false" aria-controls="collapse<?=$cont?>">
											<?=utf8_encode($valor["nombre"])?>
											<?php
												$sql="SELECT COUNT(*) AS cont FROM documento WHERE eliminado=FALSE AND categoria=".$valor["id"];
												$result = $conn->query($sql);
												if ($result->num_rows > 0) {
													// output data of each row
													while($row = $result->fetch_assoc()) {
														$pillValor = $row["cont"];
													}
												}
											?>
											<span class="badge badge-danger badge-pill float-right mt-1 mb-1"><?=$pillValor?></span>
										</button>
                                    </div>

                                    <div id="collapse<?=$cont?>" class="collapse" aria-labelledby="heading<?=$cont?>" data-parent="#accordion<?=$cont?>">
                                        <div>
                                            <?php
													$sql = "SELECT * FROM documento WHERE eliminado=FALSE AND categoria=".$valor["id"];
													$result = $conn->query($sql);
													if ($result->num_rows > 0) {
														// output data of each row
														while($row = $result->fetch_assoc()) {
															echo ' <a href="https://indev9.com/redflip/doc/'.$valor["id"].'/'.$row["nombre"].'"
															download="'.$row["titulo"].".".$row["extension"].'"
															class="acordeon-matchArchivos d-flex justify-content-between align-items-center">
															'.$row["titulo"];
															echo '<span class="badge badge-info badge-pill">'.strtoupper($row["extension"]).'</span></a>';
														}
													} else {
														echo '<label class="label-match">Ups! Aquí no hay archivos...</label>';
													}
												?>
                                        </div>
                                    </div>
                                </div>

                                <?php
								$cont++;
								}
							?>
                        </div>
                    </div>
                </div>
                <!-- Fin Descarga de archivos -->

                <!-- Enlaces a tutoriales -->
                <div class="col-md-6 col-lg-6mb-2">
                    <div class="container-matchCol">
                        <span class="d-flex align-items-center">
							<span>
								<img src="../img/enTuto.svg" alt="" style="width:32px;">
							</span>
                        <h3 class="h3-match p-0 m-0 pl-2"> Enlaces a tutoriales</h3>
                        </span>
                        <div class="body">
                            <iframe width="100%" height="300" src="https://www.youtube.com/embed/8fYagh-rA7A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" style="padding-top: 25px;"></iframe>
                        </div>
                    </div>
                </div>
                <!-- Fin enlaces a tutorial -->
                <!-- FIN CONTENEDOR ARCHIVOS/TUTORIAL -->
            </div>
        </div>
    </section>
    <?
		include 'footer.php';
		$conn->close();
	?>
        <script src="../js/peticionMatch.js"></script>
        <script src="../js/mercadoLibre/exportExcel.js"></script>

        </body>

        </html>