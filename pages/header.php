<?php 
  // include 'valid_session.php';
  include '../controlador/mcript.php';
  include '../conexion.php';

  $sql = "Select * from Persona where id_persona = " . $_SESSION['idPer'];

  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $nombrePer =utf8_encode($row["nombre"]);
        $apePer = utf8_encode($row["apellido"]);
      }
  } else {
  }
  $conn->close();
  $ini_nom = substr($nombrePer, 0,1);
  $ini_ape = substr($apePer, 0, 1);
?>

<!DOCTYPE html>
<html lang="es-cl">
<head>
    <!--=== Required meta tags ===-->
    <meta charset="UTF-8">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
     <!--=== Title ===-->
     <title></title>
     <!--=== Favicon Icon ===-->
     <link rel="icon" href="../favicon.ico" type="image/x-icon" />
     <!--=== Bootstrap CSS ===-->
     <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
     <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
     <!--=== Font Awesome ===-->
     <link rel="stylesheet" type="text/css" href="../css/all.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <!--=== Style CSS ===-->
     <link rel="stylesheet" type="text/css" href="../css/menu.css">
     <link rel="stylesheet" type="text/css" href="../css/main.css">
     <!-- <link href="http://cdn.grapecity.com/spreadjs/hosted/css/gc.spread.sheets.excel2013white.10.1.0.css" rel="stylesheet" type="text/css" />   -->
      <link href="../css/toastr.css" rel="stylesheet"/>
</head>
<body onresize="header()">
  <div id="headerDesk" style="display:none">
    <nav id="" class="navbar navbar-expand-lg navbar-light bg-redflip-red static-top">
      
      <a class="navbar-brand nav-redflip-brand mr-auto" href="index.php">
          <img src="../img/Logo-Redflip-Sin-Borde-blanco.png" class="logoNav" alt="Logo-Redflip-Sin-Borde">
      </a>

      <div class="dropdown show">
        <a class="" href="#" role="button" id="camp" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title='Notificaciones' >
          <span class="notification" id="btnNoti"></span>
        </a>
        
          <!-- <a class="dropdown-item notFormDdI" href="#">
            <div class="notForm">
              <div class="container">
                  <div class="row">
                      <div class="column notFormC"></div>
                      <div class="column col-11">
                          <h5 class="notFormH5">Notificación</h5>
                          <p>Some text</p>
                          <div class="notFormBtn">
                              <input class="notFormBtnM btn-info" type="button" value="Ver">
                              <input class="notFormBtnM btn-success" type="button" value="Aceptar">
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </a>

          <a class="dropdown-item notFormDdI" href="#">
            <div class="notForm">
              <div class="container">
                  <div class="row">
                      <div class="column notFormC"></div>
                      <div class="column col-11">
                          <h5 class="notFormH5">Notificación 2</h5>
                          <p>Some text</p>
                          <div class="notFormBtn">
                              <input class="notFormBtnM btn-info" type="button" value="Ver">
                              <input class="notFormBtnM btn-success" type="button" value="Aceptar">
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </a> -->

          <!-- <a class="dropdown-item notFormDdI" href="#">
            <div class="notForm">
              <div class="container">
                  <div class="row">
                      <div class="column notFormC"></div>
                      <div class="column col-11">
                          <h5 class="notFormH5">Notificación3</h5>
                          <p>Some text</p>
                          <div class="notFormBtn">
                              <input class="notFormBtnM btn-info" type="button" value="Ver">
                              <input class="notFormBtnM btn-success" type="button" value="Aceptar">
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </a> -->
          <?php
          if($_SESSION['rol'] == 1){?>

              <!-- <a class="dropdown-item notFormDdI" href="#">
              <div class="notForm">
                <div class="container">
                    <div class="row">
                        <div class="column notFormC"></div>
                        <div class="column col-11">
                            <h5 class="notFormH5">Notificación3</h5>
                            <p>Some text</p>
                            <div class="notFormBtn">
                                <input class="notFormBtnM btn-info" type="button" value="Ver">
                                <input class="notFormBtnM btn-success" type="button" value="Aceptar">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </a> -->
            <div class="dropdown-menu dropdown-menu-right notification-content" aria-labelledby="camp" id="notify">
            </div>
          <?php 
          }else{
          ?>
            <div class="dropdown-menu dropdown-menu-right notification-content" aria-labelledby="camp"></div>

         <?php
          }
          ?>


        
      </div>

      <div style="margin: 0 15px;">
        <label class="welcome-usr dropdown"> <?echo $nombrePer . " " . $apePer?></label>
      </div>

        <div class="dropdown show" style="margin-right: 30px;">
          <a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="circulo">
              <p><?
              echo $ini_nom . $ini_ape;
              ?></p>
            </div>
          </a>
        
          <div class="dropdown-menu dropdown-menu-right" style="margin-top:12px;" aria-labelledby="dropdownMenuLink">
            <!-- <a class="dropdown-item" href="#">No</a> -->
            <a class="dropdown-item r-usser" href="#"><?echo $nombrePer . " " . $apePer?></a>
            <a class="dropdown-item" href="cerrar_sesion.php">Cerrar Sesión</a>
          </div>
        </div>        
    </nav>

<?php
include 'sidebar.php';
?>
</div>
<div id="headerMobile" style="display:none">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand nav-redflip-brand mr-auto" href="index.php">
            <img src="../img/Logo-Redflip-Sin-Borde.png" class="logoNav" alt="Logo-Redflip-Sin-Borde" style="">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
    
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="mobMenu">
                <a href="index.php">
                    <li>Índice</li>
                </a>
                <?php if($_SESSION['rol'] == 1){?>
                <a href="AgregarPropietario.php">
                    <li>Agregar Propietario</li>
                </a>
                <?php } 
                if($_SESSION['rol'] == 1){?>
                <a href="PropietariosAct.php">
                    <li>Propietarios Activos</li>
                </a>
                <?php } if($_SESSION['rol'] == 1){?> 
                <a href="PropietariosDes.php">
                    <li>Propietarios Deshabilitados</li>
                </a>
                <?php } if($_SESSION['rol'] == 1){?> 
                <a href="AgregarPropiedades.php">
                    <li>Agregar Propietario - Propiedad</li>
                </a>
                <?php } if($_SESSION['rol'] == 1){?> 
                <a href="propVSpropiedad.php">
                    <li>Propiedades Activas</li>
                </a>
                <?php } if($_SESSION['rol'] == 1){?> 
                <a href="desPropVSpropiedad.php">
                    <li>Propiedades Deshabilitadas</li>
                </a>
                <?php }?> 
                <a href="../controlador/CrearFormularioVacio.php">
                    <li>Nuevo Formulario</li>
                </a>
                <a href="../pages/misFormularios.php">
                    <li>Mis Formularios</li>
                </a>

                <hr>
                <label for="" class="nomb"> Super Admin</label>
                
                <a href="../controlador/CrearFormularioVacio.php">
                    <li>Notificaciones</li>
                </a>
                <a href="../controlador/CrearFormularioVacio.php">
                    <li class="logOut">Cerrar sesión</li>
                </a>
            </ul>
        </div>
    </nav>
</div>
