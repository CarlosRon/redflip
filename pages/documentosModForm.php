<?php
    include 'sidebarMenu.php';
    include '../conexion.php';

    $id = $_GET["id"];

    $sqlDocumento = "SELECT * FROM documento WHERE id=".$id;
    $result = $conn->query($sqlDocumento);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $documento = $row;
        }
    }
 
?>

<body>
    <h1>Modificar Archivos</h1>
    <div class="container">
        <form method="post" enctype="multipart/form-data" id="modificaDoc">
            <input type="hidden" value="<?=$id?>" name="id" id="id">
            <label for="">Titulo:</label>
            <input type="text" name="tituloDoc" placeholder="" id="tituloDoc" require="" value="<?=$documento["titulo"]?>">
            <br>

            <label for="">Categoría:</label>
            <select name="categoriaDoc" placeholder="" id="categoriaDoc" require="" form="modificaDoc">
                <option value="" disabled>Seleccionar Categoría</option>
                <?php
                    $sqlCategoriaDoc = "SELECT * from categoria_documento WHERE id NOT IN (0)";
                    $result = $conn->query($sqlCategoriaDoc);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            if($documento["categoria"]==$row["id"]){
                                echo '<option selected value="'.$row["id"].'">'.utf8_encode($row["nombre"]).'</option>';
                            }else{
                                echo '<option value="'.$row["id"].'">'.utf8_encode($row["nombre"]).'</option>';
                            }
                        }
                    }
                ?>
            </select>
            <br>
            <br>
            <button>Modificar Archivo</button>
        </form>
    </div>
    <script src="../js/docsMod.js"></script>
    <?php 
        include 'footer.php';
    ?>
</body>