<?php 
include_once 'sessionActive.php';
define('DURACION_SESION','7200'); //2 horas
ini_set("session.cookie_lifetime",'7200');
ini_set("session.gc_maxlifetime",'7200'); 
ini_set("session.save_path","/tmp");
session_cache_expire('7200');
session_start();

  include '../controlador/mcript.php';

  include '../conexion.php';

 
  $sql = "Select * from Persona where id_persona = " . $_SESSION['idPer'];



  $result = $conn->query($sql);



  if ($result->num_rows > 0) {

      // output data of each row

      while($row = $result->fetch_assoc()) {

        $nombrePer =utf8_encode($row["nombre"]);

        $apePer = utf8_encode($row["apellido"]);

      }

  } else {

  }

  $conn->close();

  $ini_nom = substr($nombrePer, 0,1);

  $ini_ape = substr($apePer, 0, 1);
//   define si es mobile o desktop

$dispositivo="";
$tablet_browser = 0;
$mobile_browser = 0;
$body_class = 'desktop';
 
if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $tablet_browser++;
    $body_class = "tablet";
}
 
if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $mobile_browser++;
    $body_class = "mobile";
}
 
if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
    $mobile_browser++;
    $body_class = "mobile";
}
 
$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
$mobile_agents = array(
    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
    'newt','noki','palm','pana','pant','phil','play','port','prox',
    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
    'wapr','webc','winw','winw','xda ','xda-');
 
if (in_array($mobile_ua,$mobile_agents)) {
    $mobile_browser++;
}
 
if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
    $mobile_browser++;
    //Check for tablets on opera mini alternative headers
    $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
      $tablet_browser++;
    }
}
if ($tablet_browser > 0) {
// Si es tablet has lo que necesites
   $dispositivo = "desk";
}
else if ($mobile_browser > 0) {
// Si es dispositivo mobil has lo que necesites
$dispositivo = "mobil";
}
else {
// Si es ordenador de escritorio has lo que necesites
$dispositivo = "desk";
}  

?>

<!DOCTYPE html>

<html lang="es-CL">

<head>

    <!--=== Required meta tags ===-->

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta http-equiv="Expires" content="0">

    <meta http-equiv="Last-Modified" content="0">

    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">

    <meta http-equiv="Pragma" content="no-cache">

    <!--=== Title ===-->

    <title></title>

    <!--=== Favicon Icon ===-->

    <link rel="icon" href="../favicon.ico" type="image/x-icon" />

    <!--=== Bootstrap CSS ===-->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!--=== Font Awesome ===-->

    <!-- <link rel="stylesheet" type="text/css" href="../css/all.min.css"> -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css">

    <!--=== Style CSS ===-->

    <!-- <link rel="stylesheet" type="text/css" href="../css/menu.css"> -->

    <link rel="stylesheet" type="text/css" href="../css/main.css">

    <link rel="stylesheet" type="text/css" href="../css/sideMenu.css">
    
    <link rel="stylesheet" type="text/css" href="../css/loader.css">

    <link href="../css/toastr.css" rel="stylesheet"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.compat.css"/>
    <!-- para el dragable de publicacion -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
  </script>
    <!-- Scripts Asincronos -->
    <script src="../js/load.js" type="text/javascript" async="async"></script>
    <script src="../js/push.js" type="text/javascript" async="async"></script>
    

	      <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->
<!-- Smartsupp Live Chat script -->
<script type="text/javascript">
var _smartsupp = _smartsupp || {};
_smartsupp.key = 'eb16114f301228359f6ca8270cb945cc88d1d1f9';
window.smartsupp||(function(d) {
  var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
  s=d.getElementsByTagName('script')[0];c=d.createElement('script');
  c.type='text/javascript';c.charset='utf-8';c.async=true;
  c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
</script>
<script>smartsupp('group', 'FnRdUTtLBs'); //group Soporte Redflip 
</script>

</head>



<body>

<div class="primary-nav" id="headerDesk2" style="<?=($dispositivo != "desk")?'display:none;':''?>">

<nav class="navbar navbar-expand-xl navbar-light bg-light" style="padding-left:80px; height:74px;">
        <a class="navbar-brand" style="color: white;">
            <img src="../img/Logo-Redflip-Sin-Borde.png" class="logoNav">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#">
                    <?=$_SESSION["access_token"]["access_token"]?><span class="sr-only">(current)</span></a>
                <!-- <a class="nav-item nav-link" href="#">Features</a>
					<a class="nav-item nav-link" href="#">Pricing</a> -->
            </div>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="no-icon">Configuración</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="no-icon">Mi Cuenta</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cerrar_sesion.php">
                        <span class="no-icon">Cerrar Sesión</span>
                    </a>
                </li>
            </ul>

        </div>
    </nav>

    <button href="#" class="hamburger open-panel nav-toggle" style="outline: none;">

        <span class="screen-reader-text">Menu</span>

    </button>



	<nav role="navigation" class="menu">

		<a href="index.php" class="logotype">

            <img src="../img/Logo-Redflip-Sin-Borde.png" class="logoNav" alt="Logo-Redflip-Sin-Borde" style="margin-top: -8px;">

        </a>

		<div class="overflow-container">

            <label for="" class="lblUsr">

                <?php

                    echo $desencriptar($_SESSION["usuario"]);

                ?>

            </label>

			<ul class="menu-dropdown">

                

                <li class="menu-hasdropdown" style="<?=($_SESSION["rol"] == 2)?'display:none':''?>">

                    <a href="index.php">Mi Dashboard</a>

                    <span class="icon">

                        <i class="fas fa-star"></i>

                    </span>

                </li>

                    <?php if($_SESSION['rol'] == 1){?>

                    <li>

                        <a href="#">Propietarios</a>

                        <span class="icon">

                            <i class="fa fa-user"></i>

                        </span>

                        <label title="toggle menu" for="menuPrp">

                                <span class="downarrow">

                                    <i class="fa fa-caret-down"></i>

                                </span>

                            </label>

                        <input type="checkbox" class="sub-menu-checkbox" id="menuPrp" onchange="showHide('menuPrp')"/>

                        <ul class="sub-menu-dropdown" style="<?=($_SESSION["rol"] == 2)?'display:none':''?>">

                            <li><a href="AgregarPropietario.php">Agregar propietario</a></li>

                            <li><a href="PropietariosAct.php">Ver propietarios</a></li>

                            <li><a href="PropietariosDes.php">Propietarios deshabilitados</a></li>

                        </ul>

                    </li>

                
                    <?php } ?>
                    <li class="menu-hasdropdown" style="<?=($_SESSION["rol"] == 2)?'display:none':''?>">

                        <a href="#">Interesados</a>

                            <span class="icon">

                                <i class="fa fa-users"></i>

                            </span>

                            <label title="toggle menu" for="menuInt">

                                <span class="downarrow">

                                    <i class="fa fa-caret-down"></i>

                                </span>

                            </label>

                        <input type="checkbox" class="sub-menu-checkbox" id="menuInt" onchange="showHide('menuInt')"/>

                        <ul class="sub-menu-dropdown">

                            <li><a href="AgregarInteresado.php">Agregar interesado</a></li>

                            <li><a href="<?=($_SESSION["rol"] == 1)?"interesadosGlobales.php":"interesadosCorredor.php"?>">Ver interesados</a></li>

                            <!-- <li><a href="#3.php">Interesados deshabilitados</a></li> -->

                        </ul>

                    </li>

                    

                    <!-- <li class="menu-hasdropdown">

                        <a href="tablaCargaMasiva.php">Carga masiva</a>

                            <span class="icon">

                                <i class="fa fa-coins"></i>

                            </span>

                    </li> -->

                

                    <li>

                        <a href="#">Propiedades</a>

                        <span class="icon">

                            <i class="fa fa-home"></i>

                        </span>

                        <label title="toggle menu" for="menuPropi">

                            <span class="downarrow">

                                <i class="fa fa-caret-down"></i>

                            </span>

                        </label>

                        <input type="checkbox" class="sub-menu-checkbox" id="menuPropi" onchange="showHide('menuPropi')"/>

                        <ul class="sub-menu-dropdown">

                            <li style="<?=($_SESSION["rol"] != 1)?'display:none':''?>" ><a href="AgregarPropiedades.php">Agregar propiedad</a></li>

                            <li style="<?=($_SESSION["rol"] != 1)?'display:none':''?>" ><a href="propVSpropiedad.php">Ver propiedades</a></li>

                            <li style="<?=($_SESSION["rol"] != 1)?'display:none':''?>" ><a href="desPropVSpropiedad.php">Propiedades deshabilitados</a></li>

                            <li style="<?=($_SESSION["rol"] == 2)?'display:none':''?>"><a href="carteraPropiedades.php">Cartera de Propiedades</a></li>

                            <li style="<?=($_SESSION["rol"] != 2)?'display:none':''?>"><a href="misPropiedades.php">Mis Propiedades</a></li>
                        
                            <li><a href="evaluarToken.php">Mercado Libre</a></li>
                        </ul>

                    </li>

                



				<li style="<?=($_SESSION["rol"] == 2)?'display:none':''?>">

                    <a href="#">Formularios</a>

                    <span class="icon">

                        <i class="fa fa-id-card"></i>

                    </span>

                    <label title="toggle menu" for="menuForm">

                        <span class="downarrow">

                            <i class="fa fa-caret-down"></i>

                        </span>

                    </label>

					<input type="checkbox" class="sub-menu-checkbox" id="menuForm" onchange="showHide('menuForm')"/>

					<ul class="sub-menu-dropdown">

						<li><a href="../controlador/CrearFormularioVacio.php">Nuevo formulario</a></li>

                        <li><a href="../pages/misFormularios.php">Formularios por enviar</a></li>

                        <li><a href="../pages/formulariosEnviados.php">Formularios enviados</a></li>

                        <li><a href="../pages/seleccionarOperacion.php">Plantillas</a></li>

					</ul>

                </li>

                <li style="<?=($_SESSION["rol"] != 1)?'display:none':''?>">

                    <a href="#">Documentos</a>

                    <span class="icon">

                        <i class="fas fa-file"></i>

                    </span>

                    <label title="toggle menu" for="menuForm">

                        <span class="downarrow">

                            <i class="fa fa-caret-down"></i>

                        </span>

                    </label>

					<input type="checkbox" class="sub-menu-checkbox" id="menuForm" onchange="showHide('menuForm')"/>

					<ul class="sub-menu-dropdown">

						<li><a href="documentosVer.php">Ver Documentos</a></li>

                        <li><a href="documentosForm.php">Subir Documentos</a></li>

                        <li><a href="documentosPapelera.php">Papelera de Reciclaje</a></li>
                        
                        <!-- 
                        <li><a href="../pages/formulariosEnviados.php">Formularios enviados</a></li>

                        <li><a href="../pages/seleccionarOperacion.php">Plantillas</a></li> -->

					</ul>

                </li>

                <li style="<?=($_SESSION["rol"] != 1)?'display:none':''?>">

                    <a href="#">Usuarios</a>

                    <span class="icon">

                        <i class="fas fa-users-cog"></i>

                    </span>

                    <label title="toggle menu" for="menuForm">

                        <span class="downarrow">

                            <i class="fa fa-caret-down"></i>

                        </span>

                    </label>

					<input type="checkbox" class="sub-menu-checkbox" id="menuForm" onchange="showHide('menuForm')"/>

					<ul class="sub-menu-dropdown">

						<li><a href="usuariosVer.php">Ver Usuarios</a></li>

                        <li><a href="usuarios.php">Crear Usuario</a></li>

                        <li><a href="usuariosPapelera.php">Usuarios Desactivados</a></li>
                        
                        <!-- 
                        <li><a href="../pages/formulariosEnviados.php">Formularios enviados</a></li>

                        <li><a href="../pages/seleccionarOperacion.php">Plantillas</a></li> -->

					</ul>

                </li>

                <hr>

                <li id="btnNoti" class="nBell" onclick="<?php echo ($_SESSION["rol"] == 1)? "notiAdmin()" : "NotiCorredor(".$_SESSION["id"].")" ?>">

                <input type="hidden" id="rol" value="<?php echo $_SESSION["rol"]?>">

                <input type="hidden" id="id" value="<?php echo $_SESSION["id"]?>">

                    <a href="#">Notificaciones</a>

                    <span class="icon">

                        <i class="fa fa-bell"></i>

                        <div class="notiCont" id="notiCont" style="display:none"></div>

                    </span>

                    <label title="toggle menu" for="menuNoti">

                        <span class="downarrow">

                            <i class="fa fa-caret-down"></i>

                        </span>

                    </label>

                    <input type="checkbox" class="sub-menu-checkbox" id="menuNoti" onchange="showHide('menuNoti')"/>

                    

					<ul class="sub-menu-dropdown noty-scroll" id="notify">

                        <!-- <li class="noty-li">

                            <div class="noty-container">

                                <h5 class="noty-h5">Notificación</h5>

                                <p class="noty-p">[Nombre] ha creado un nuevo formulario.</p>

                                <div class="notFormBtn">

                                    <input class="notFormBtnM noty-btn btn-info" type="button" value="Ver">

                                    <input class="notFormBtnM noty-btn btn-success" type="button" value="Aceptar">

                                </div>

                            </div>

                        </li> -->

					</ul>

                </li>

                

                <li class="lOut">

                    <a href="cerrar_sesion.php">Cerrar sesión</a>

                    <span class="icon">

                        <i class="fa fa-sign-out-alt"></i>

                    </span>

                </li>

            </ul>

            <div class="" style="margin-top:10%;">

                <div class="hoyDivisa">

                    <div class="hoyDivContDato" onclick="UF()">

                        <p class="hoyDivisaTitulo">UF</p>

                        <label for="" class="hoyDivisaValor" id="uf"></label>

                    </div>

                    <!-- <div class="hoyDivContDato" onclick="dolar()">

                        <p class="hoyDivisaTitulo">Dolar</p>

                        <label for="" class="hoyDivisaValor" id="dolar"></label>

                    </div> -->

                </div>

                <div class="" style="">

                <div class="hoyDivisa">

                    <!-- <div class="hoyDivContDato" onclick="UF()">

                        <p class="hoyDivisaTitulo">UF</p>

                        <label for="" class="hoyDivisaValor" id="uf"></label>

                    </div> -->

                    <div class="hoyDivContDato" onclick="dolar()">

                        <p class="hoyDivisaTitulo">Dolar</p>

                        <label for="" class="hoyDivisaValor" id="dolar"></label>

                    </div>

                </div>



                <div style="text-align:center; width:92%; bottom:20px; padding:0 30px;">

                    <p style="font-size:14px;">

                        ©2020 <span class="c-redflip-red" style="font-weight:600;">Redflip</span> Inc.<br> Todos los derechos reservados.

                    </p>

                </div>

            </div>



        </div>

    </nav>

</div>

    

<div id="headerMobile2" class="menu" style="<?=($dispositivo != "mobil")?'display:none;':''?>">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

        <a class="navbar-brand nav-redflip-brand mr-auto" href="index.php">

            <img src="../img/Logo-Redflip-Sin-Borde.png" class="logoNav" alt="Logo-Redflip-Sin-Borde" style="">

        </a>



        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

        <span class="navbar-toggler-icon"></span>

        </button>

    

        <div class="collapse navbar-collapse scrollMm" id="navbarSupportedContent">

        <ul class="menu-dropdown">



            <label for="" class="lblUsr">

                <?php

                    echo $desencriptar($_SESSION["usuario"]);

                ?>

            </label>

            <hr class="hrUsr">

                

                <li class="menu-hasdropdown">

                    <a href="index.php">Mi Dashboard</a>

                    <span class="icon">

                        <i class="fas fa-star"></i>

                    </span>

                </li>

                    <?php if($_SESSION['rol'] == 1){?>

                    <li>

                        <a href="#">Propietarios</a>

                        <span class="icon">

                            <i class="fa fa-user"></i>

                        </span>

                        <label title="toggle menu" for="menuPrpM">

                                <span class="downarrow">

                                    <i class="fa fa-caret-down"></i>

                                </span>

                            </label>

                        <input type="checkbox" class="sub-menu-checkbox" id="menuPrpM" onchange="showHideM('menuPrpM')"/>

                        <ul class="sub-menu-dropdown">

                            <li><a href="AgregarPropietario.php">Agregar propietario</a></li>

                            <li><a href="PropietariosAct.php">Ver propietarios</a></li>

                            <li><a href="PropietariosDes.php">Propietarios deshabilitados</a></li>

                        </ul>

                    </li>

                
                    <?php } ?>
                    <!-- <li class="menu-hasdropdown">

                        <a href="#">Interesados</a>

                            <span class="icon">

                                <i class="fa fa-users"></i>

                            </span>

                            <label title="toggle menu" for="menuIntM">

                                <span class="downarrow">

                                    <i class="fa fa-caret-down"></i>

                                </span>

                            </label>

                        <input type="checkbox" class="sub-menu-checkbox" id="menuIntM" onchange="showHideM('menuIntM')"/>

                        <ul class="sub-menu-dropdown">

                            <li><a href="#1.php">Agregar interesado</a></li>

                            <li><a href="#2.php">Ver interesados</a></li>

                            <li><a href="#3.php">Interesados deshabilitados</a></li>

                        </ul>

                    </li> -->



                    <!-- <li class="menu-hasdropdown">

                        <a href="tablaCargaMasiva.php">Carga masiva</a>

                            <span class="icon">

                                <i class="fa fa-coins"></i>

                            </span>

                    </li> -->

                

                    <li>

                        <a href="#">Propiedades</a>

                        <span class="icon">

                            <i class="fa fa-home"></i>

                        </span>

                        <label title="toggle menu" for="menuPropiM">

                            <span class="downarrow">

                                <i class="fa fa-caret-down"></i>

                            </span>

                        </label>

                        <input type="checkbox" class="sub-menu-checkbox" id="menuPropiM" onchange="showHideM('menuPropiM')"/>

                        <ul class="sub-menu-dropdown">

                            <li style="<?=($_SESSION["rol"] != 1)? 'display:none' : '' ?>"><a href="AgregarPropiedades.php">Agregar propiedad</a></li>

                            <li style="<?=($_SESSION["rol"] != 1)? 'display:none' : '' ?>"><a href="propVSpropiedad.php">Ver propiedades</a></li>

                            <li style="<?=($_SESSION["rol"] != 1)? 'display:none' : '' ?>"><a href="desPropVSpropiedad.php">Propiedades deshabilitados</a></li>

                            <li><a href="carteraPropiedades.php">Cartera de Propiedades</a></li>

                            
                        </ul>

                    </li>

                



				<li class="">

                    <a href="#">Formularios</a>

                    <span class="icon">

                        <i class="fa fa-id-card"></i>

                    </span>

                    <label title="toggle menu" for="menuFormM" onchange="showHideM('menuFormM')">

                        <span class="downarrow">

                            <i class="fa fa-caret-down"></i>

                        </span>

                    </label>

					<input type="checkbox" class="sub-menu-checkbox"   id="menuFormM" />

					<ul class="sub-menu-dropdown">

						<li><a href="../controlador/CrearFormularioVacio.php">Nuevo formulario</a></li>

						<li><a href="../pages/misFormularios.php">Formularios por enviar</a></li>

						<li><a href="../pages/formulariosEnviados.php">Formularios enviados</a></li>

					</ul>

                </li>



                <hr>



                <li id="btnNoti" class="nBell" onclick="<?php echo ($_SESSION["rol"] == 1)? "notiAdmin()" : "NotiCorredor(".$_SESSION["id"].")" ?>">

                    <input type="hidden" id="rol" value="<?php echo $_SESSION["rol"]?>">

                    <input type="hidden" id="id" value="<?php echo $_SESSION["id"]?>">

                    <a href="#">Notificaciones</a>

                    <span class="icon">

                        <i class="fa fa-bell"></i>

                        <div class="notiCont" id="notiContM" style="display:none"></div>

                    </span>

                    <label title="toggle menu" for="menuNotiM">

                        <span class="downarrow">

                            <i class="fa fa-caret-down"></i>

                        </span>

                    </label>

                    <input type="checkbox" class="sub-menu-checkbox" id="menuNotiM" onchange="showHideM('menuNotiM')"/>

                    

					<ul class="sub-menu-dropdown noty-scroll" id="notifyM">

                        <!-- <li class="noty-li">

                            <div class="noty-container">

                                <h5 class="noty-h5">Notificación</h5>

                                <p class="noty-p">[Nombre] ha creado un nuevo formulario.</p>

                                <div class="notFormBtn">

                                    <input class="notFormBtnM noty-btn btn-info" type="button" value="Ver">

                                    <input class="notFormBtnM noty-btn btn-success" type="button" value="Aceptar">

                                </div>

                            </div>

                        </li> -->

					</ul>

                </li>

                

                <li class="lOut">

                    <a href="cerrar_sesion.php">Cerrar sesión</a>

                    <span class="icon">

                        <i class="fa fa-sign-out-alt"></i>

                    </span>

                </li>

            </ul>

            <div class="" style="margin-top:10%;">

                <div class="hoyDivisa">

                    <div class="hoyDivContDato" onclick="UF()">

                        <p class="hoyDivisaTitulo">UF</p>

                        <label for="" class="hoyDivisaValor" id="ufM"></label>

                    </div>

                    <!-- <div class="hoyDivContDato" onclick="dolar()">

                        <p class="hoyDivisaTitulo">Dolar</p>

                        <label for="" class="hoyDivisaValor" id="dolar"></label>

                    </div> -->

                </div>

                <div class="" style="">

                <div class="hoyDivisa">

                    <!-- <div class="hoyDivContDato" onclick="UF()">

                        <p class="hoyDivisaTitulo">UF</p>

                        <label for="" class="hoyDivisaValor" id="ufM"></label>

                    </div> -->

                    <div class="hoyDivContDato" onclick="dolar()">

                        <p class="hoyDivisaTitulo">Dolar</p>

                        <label for="" class="hoyDivisaValor" id="dolarM"></label>

                    </div>

                </div>



                <div style="text-align:center; width:92%; bottom:20px; padding:0 30px;">

                    <p style="font-size:14px;">

                        ©2020 <span class="c-redflip-red" style="font-weight:600;">Redflip</span> Inc.<br> Todos los derechos reservados.

                    </p>

                </div>

            </div>

        </div>

    </nav>

    

</div>