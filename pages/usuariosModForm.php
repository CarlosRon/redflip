<?php
    include 'sidebarMenu.php';
    include '../conexion.php';
    include "../controlador/mcript.php";

    $idPer = $_GET["idPer"];

    $sql = $sql = "SELECT Persona.id_persona, usuario.fk_persona, Corredor.fk_persona, Persona.nombre, Persona.apellido, Persona.rut, Persona.fec_nac, Persona.telefono, Persona.correo, usuario.nom_us, usuario.fk_rol, usuario.fk_estado_us, usuario.pass, usuario.id_usuario, Corredor.id_corredor, Corredor.eliminado, Corredor.imagen FROM ((Persona INNER JOIN usuario ON Persona.id_persona = usuario.fk_persona) INNER JOIN Corredor ON Persona.id_persona = Corredor.fk_persona) WHERE Persona.id_persona = $idPer";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $nombre = $row["nombre"];
            $apellido = $row["apellido"];
            $rut = $row["rut"];
            $fecha = $row["fec_nac"];
            $telefono = $row["telefono"];
            $correo = $row["correo"];
            $username = $row["nom_us"];
            $pass = $row["pass"];
            $rol = $row["fk_rol"];
            $estado = $row["fk_estado_us"];
        }
    }

    
?>


<div class="container pt-3  animated fadeIn">
<form action="" method="post" id="modificaUsuario" class="formulario-usuario">
<input type="hidden" value="<?=$idPer?>" id="idPer">
  <input id="temp_pass" name="temp_pass" type="text" hidden>
  <input value="<?=$idPer?>" id="id_persona" name="id_persona" type="text" hidden>
  <div class="row  border pt-2">
    <div class="col">
      <!-- Level 1: .col-sm-9 -->
      <div class="row">
        <div class="col text-center">
            <h3>Modificar Usuario</h3>
        </div>        
      </div>
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Nombre</span>
                </div>
                <input value="<?=$nombre?>" type="text" class="form-control" name="nombre" id="nombre" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Apellido</span>
                </div>
                <input value="<?=$apellido?>" type="text" class="form-control" name="apellido" id="apellido" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Rut</span>
                </div>
                <input value="<?=$rut?>" type="text" class="form-control" name="rut" id="rut" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Fecha de Nacimiento</span>
                </div>
                <input value="<?=$fecha?>" type="date" class="form-control" name="fecha-nac" id="fecha-nac" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>

      
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Teléfono</span>
                </div>
                <input value="<?=$telefono?>" type="text" class="form-control" name="telefono" id="telefono" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Correo</span>
                </div>
                <input value="<?=$correo?>" type="text" class="form-control" name="correo" id="correo" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Rol</label>
                </div>
                <select class="custom-select" id="rol"  name="rol">
                    <option value="" disabled>Seleccione una opción</option>
                    <?php
                    $sql = "SELECT * from Rol order by nom_rol";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                          if($row["id_rol"] == $rol){
                            echo "<option selected value='".$row["id_rol"]."'>".$row["nom_rol"]."</option>";
                          }else{
                            echo "<option value='".$row["id_rol"]."'>".$row["nom_rol"]."</option>";
                          }
                        
                      }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col">
        <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect02">Estado</label>
                </div>
                <select class="custom-select" id="estado"  name="estado">
                    <option value="" selected disabled>Seleccione una opción</option>
                    <?php
                        $sql = "SELECT * FROM estado_us ORDER BY estado_us.nom_estado_us";
                        $result = $conn->query($sql);
                        
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                              
                                if($row["id_estado_us"] == $estado){
                                  echo "<option selected value='".$row["id_estado_us"]."'>".$row["nom_estado_us"]."</option>";
                                }else{
                                  echo "<option value='".$row["id_estado_us"]."'>".$row["nom_estado_us"]."</option>";
                                }
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        
      </div>
      <div class="row">
      <div class="col text-center">
        <button type="submit" class="btn btn-outline-primary center" >Guardar</button>
      </div>
      </div>
    </div>
  </div>
  </form>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script src="../js/usuariosMod.js"></script>
    <script src="../js/usuariosElim.js"></script>

<?php
include_once 'footer.php';
?>