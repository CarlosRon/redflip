<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" type="text/css" href="../css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>notificacion</title>

        <style>
            .notForm{
                width:300px;
                height:auto;
                bottom:0;
                right: 0;
                position: absolute;
                margin: 10px;
                border: 1px solid rgb(219, 219, 219);
                background-color: rgb(240, 239, 239);
                border-radius:3px;
                margin-bottom: 9px;
                border-top: 3px solid #d4d4d4;
            }

            .notFormC{
                background-color: #e9b059; 
                width:10px;
            }

            .notFormBtn{
                float:right;
            }
            .notFormBtnM{
                background-color: #27AE60;
                color: #fff;
                border:none;
                margin:0 0 10px 10px;
                width:100px;
            }
        </style>
    </head>
    <body>
        <section class="notForm">
            <div class="container">
                <div class="row">
                    <div class="column notFormC"></div>
                    <div class="column col-11">
                        <h5>Notificación</h5>
                        <p>Some text</p>
                        <div class="notFormBtn">
                            <input class="notFormBtnM" type="button" value="Ver">
                            <input class="notFormBtnM" type="button" value="Aceptar">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>