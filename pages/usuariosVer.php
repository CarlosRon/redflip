<?php
    include 'sidebarMenu.php';
    include '../conexion.php';
    $sql = "SELECT Persona.id_persona, usuario.fk_persona, Corredor.fk_persona, Persona.nombre, Persona.apellido, Persona.rut, Persona.fec_nac, Persona.telefono, Persona.correo, usuario.nom_us, usuario.fk_rol, usuario.fk_estado_us, usuario.id_usuario, Corredor.id_corredor, Corredor.eliminado, Corredor.imagen FROM Persona, Corredor, usuario WHERE Persona.id_persona = Corredor.fk_persona AND Persona.id_persona = usuario.fk_persona AND Corredor.eliminado = 0";
    $result = $conn->query($sql);
    $eliminado = "";
?>

<div class="container-fluid" style="padding-top: 100px; padding-left: 100px">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Datos</th>
                <th scope="col">ID Persona</th>
                <th scope="col">Nombre de Usuario</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">RUT</th>
                <th scope="col">Fecha de Nacimiento</th>
                <th scope="col">Telefono</th>
                <th scope="col">Correo</th>
                <th scope="col">Rol</th>
                <th scope="col">Estado</th>
                <th scope="col">ID Usuario</th>
                <th scope="col">ID Corredor</th>
                <th scope="col">Imagen</th>  
                <th scope="col">Acciones</th>        
            </tr>
        </thead>
        <tbody>
        <?php
            if($result->num_rows > 0){
                while($row = $result->fetch_assoc()){
                    $decript_username = $desencriptar($row["nom_us"]);
                    if($row["eliminado"] == '1'){
                        $eliminado = "Inactivo";
                    }else if($row["eliminado"] == '0'){
                        $eliminado = "Activo";
                    }else{
                        $eliminado = "S/D";
                    }

                    $rol_display = $row["fk_rol"];
                    $estado_display = $row["fk_estado_us"];

                    $sql2 = "SELECT Rol.nom_rol FROM Rol where Rol.id_rol = $rol_display";
                    $result2 = $conn->query($sql2);
                    if($result2->num_rows > 0){
                        while($row2 = $result2->fetch_assoc()){
                            $rol_display=$row2["nom_rol"];
                        }
                    }

                    $sql2 = "SELECT estado_us.nom_estado_us FROM estado_us where estado_us.id_estado_us = $estado_display";
                    $result2 = $conn->query($sql2);
                    if($result2->num_rows > 0){
                        while($row2 = $result2->fetch_assoc()){
                            $estado_display=$row2["nom_estado_us"];
                        }
                    }

                    
                    echo '
                    <tr>
                        <td> <a href="#" userModal="'.$row["id_persona"].'" class="btn btn-matchBlue view_user"><i class="far fa-eye"></i>Aber la info</a> </td>
                        <td>'.$row["id_persona"].'</td>
                        <td>'.$decript_username.'</td>
                        <td>'.utf8_encode($row["nombre"]).'</td>
                        <td>'.utf8_encode($row["apellido"]).'</td>
                        <td>'.$row["rut"].'</td>
                        <td>'.$row["fec_nac"].'</td>
                        <td>'.$row["telefono"].'</td>
                        <td>'.$row["correo"].'</td>
                        <td>'.$rol_display.'</td>
                        <td>'.$estado_display.'</td>
                        <td>'.$row["id_usuario"].'</td>
                        <td>'.$row["id_corredor"].'</td>
                        <td>'.$row["imagen"].'</td> 
                        <td> <a href="usuariosModForm.php?idPer='.$row["id_persona"].'" class="btn btn-matchGreen"><i class="fas fa-edit"></i> Modificar</a>
                        <a href="" onclick="eliminarU('.$row["id_corredor"].'); return false" class="btn btn-matchRed"><i class="fas fa-trash-alt"></i> Eliminar</a></td>
                    </tr>';
                    
                }
            }else{
                echo $sql;
            }
            
        ?>
        </tbody>
    </table>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/dist/sweetalert2.all.min.js"></script>
<script src="../js/usuariosElim.js"></script>