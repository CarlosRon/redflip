<?php
    include 'sidebarMenu.php';
    include '../conexion.php';
    
?>

    <div class="container-fluid bg-light">
        <form action="" id="formC" style="width: 80%;">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Corredor</h2>
                                    <h3 class="fs-subtitle">Paso 1</h3>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="operario" class="label_titulo" style="margin-bottom: 10px;">Operario Cámara</label>
                                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla ligula urna. In eleifend sem" style="font-size: 15px !important;">
                                <button class="btn btn-primary" style="pointer-events: none;" type="button" disabled>?</button>
                            </span>
                                    <select name="operario" id="operario">
                                <option value="null" disabled selected>Operario cámara*</option>
                                <?php
                                $sqlOperario = "Select Operario.id_operario, Persona.nombre, Persona.apellido from Operario, Persona where Operario.fk_persona = Persona.id_persona order by id_operario";
                                $result = $conn->query($sqlOperario);
                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value=".$row["id_operario"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                                    }
                                }
                                ?>
                            </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="select_area" class="label_titulo" style="margin-bottom: 10px;">Área Redflip</label>
                                    <select name="select_area" id="select_area" class="dropdown">
                                <option value="null" disabled selected>Área Redflip*</option>
                                <option value="Redflip Habitacional">Redflip Habitacional</option>
                                <option value="Redflip Comercial">Redflip Comercial</option>
                                <option value="Redflip Luxury">Redflip Luxury</option>
                            </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="corredor" class="label_titulo" style="margin-bottom: 10px;">Corredor</label>
                                    <select name="corredor" id="corredor" onchange="selectMail()">
                                <option value="null" disabled selected>Nombre Corredor*</option>
                                <?
                                    $sqlCorredor="Select Corredor.id_corredor, Persona.nombre, Persona.apellido, Persona.correo from Corredor, Persona where Corredor.fk_persona = Persona.id_persona";
                                    $result = $conn->query($sqlCorredor);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value=".$row["id_corredor"].">".utf8_encode($row["nombre"]). " " . utf8_encode($row["apellido"])."</option>";
                                        }
                                    }
                                ?>
                            </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="mail" class="label_titulo" style="margin-bottom: 10px;">Mail</label>
                                    <input type="text" id="mail" value="" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propietario</h2>
                                    <h3 class="fs-subtitle">Paso 2</h3>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="nombre" class="label_titulo">Nombre</label>
                                    <input class="fs-nom" type="text" name="nombre" placeholder="Nombre*" id="nombre" onfocusout="validarNombre()" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="apellido" class="label_titulo">Apellido</label>
                                    <input class="fs-ape" type="text" name="apellido" placeholder="Apellido*" id="apellido" onfocusout="validarApellido()" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="rut" class="label_titulo">Rut</label>
                                    <input class="fs-rut" type="text" name="rut" placeholder="Rut" id="rut" onfocusout="validarRut2()" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="correo" class="label_titulo">Correo</label>
                                    <input type="mail" name="correo" id="correo" placeholder="Correo*" onfocusout="validarCorreo()" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="telefono" class="label_titulo">Teléfono</label><br>
                                    <select name="sub_fono" id="sub_fono" class="fs-suFono fono-float-none">
                                <option value="" disabled selected>Prefijo*</option>
                                <option value="+569">+569</option>
                                <option value="+562">+562</option>
                                <option value="+568">+568</option>
                                <option value="+56">+56</option>
                            </select>
                                    <input class="fs-fono fono-float-right" type="tel" name="telefono" id="telefono" placeholder="Teléfono*" onfocusout="validarTelefono()" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label class="label_titulo">Fecha</label>
                                    <input type="date" name="fecha" id="fecha" placeholder="Fecha" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3"> <label for="origen" class="label_titulo">Origen</label>
                                    <select name="origen" id="origen" class="fs-ori" onchange="cambioOrigen()">
                                <option value="" disabled selected>Origen*</option>
                                <option value="17" >Captado por Redflip</option>
                                <option value="7" >Referido Corredor</option>
                                <option value="6" >Referido Cliente</option>
                                <option value="5" >Referido Conserje</option>
                                <option value="18">Partner Redflip</option>                         
                            </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                                    <label for="captadaR" class="label_titulo">Seleccione:</label>
                                    <select name="captadaR" id="captadaR" class="fs-ori">
                                    <option value="" disabled selected>Seleccione opción*</option>
                                    <option value="9" >Base de Datos</option>
                                    <option value="19" >Google</option>
                                    <option value="1" >Instagram</option>
                                    <option value="2" >Facebook</option>
                                    <option value="23" >Desconocido</option>    
                            </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna A1 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                    <h3 class="fs-subtitle">Paso 3</h3>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                                    <!-- Quizas cambiar monto por precio -->
                                    <label for="monto1" class="label_titulo">Monto</label><br>
                                    <select name="divisaMonto1" id="divisaMonto1" class="fs-divisa">
                                            <option value="" disabled selected>Divisa</option>
                                            <option value="1" >UF</option>
                                            <option value="2" >CLP</option>
                                        </select>
                                    <input class="fs-valor fs-tNumb" type="text" name="monto1" id="monto1" placeholder="Monto" style="width:60%!important; margin-left: 10px;" onfocusout="validarPrecio('monto1')" />


                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 pt-3 d-flex align-items-end">
                                    <div class="container-fluid d-flex justify-content-between pb-5px">
                                        <label class="txtProp">
                                        Hipoteca
                                    </label>
                                        <label class="switch" style="float: right;">
                                        <input type="checkbox" id="hipoteca" name="hipoteca" onchange="hipotecaCheck()">
                                        <span class="slider round"></span>
                                    </label>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                                    <label for="select_banco" class="label_titulo">Seleccione un banco:</label>
                                    <select name="select_banco" id="select_banco" class="fs-tipoOpe" onchange="" style="">
                                    <option value="null" disabled selected>Seleccione un banco</option>
                                    <?php
                                    $sqlBancos = "SELECT * FROM Bancos order by banco asc";
                                    $result = $conn->query($sqlBancos);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value='".$row["id_banco"]."'>".$row["banco"]."</option>";
                                        }
                                    }
                                    ?>
                                </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 d-flex align-items-end">
                                    <div class="container-fluid d-flex justify-content-between pb-5px">
                                        <label class="txtProp">
                                        Amoblado
                                    </label>
                                        <label class="switch" style="float: right;">
                                        <input type="checkbox" id="amoblado" name="amoblado" onchange="amobla2()">
                                        <span class="slider round"></span>
                                    </label>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                                    <label for="valorAmob" class="label_titulo">Valor diferencia</label>
                                    <input class="fs-valor fs-tNumb" type="text" name="valorAmob" id="valorAmob" placeholder="Valor de diferencia amoblado" onfocusout="validarPrecio('valorAmob')" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 d-flex align-items-end">
                                    <div class="container-fluid d-flex justify-content-between pb-5px">
                                        <label class="txtProp">
                                        No exclusivo
                                    </label>
                                        <label class="switch" style="float: right;">
                                        <input type="checkbox" id="exclusividad" name="exclusividad" onchange="noExclusividad()">
                                        <span class="slider round"></span>
                                    </label>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="select_tiempoPublicacion" class="label_titulo">Cantidad de Corredores</label>
                                    <select name="cantCorredor" id="cantCorredor" class="fs-tipoOpe">
                                    <label for="cantCorredor" class="label_titulo">Cantidad de Corredores</label>
                                    <option value="null" disabled selected>Cant. de corredores</option>
                                    <option value="1" >1</option>
                                    <option value="2" >2</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                    <option value="5" >5</option>
                                    <option value="6" >6</option>
                                </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="select_tiempoPublicacion" class="label_titulo">Tiempo de publicación</label>
                                    <select name="select_tiempoPublicacion" id="select_tiempoPublicacion" class="fs-tipoOpe">
                                    <option value="null" disabled selected>Tiempo de publicación</option>
                                    <option value="1" >1 Semana</option>
                                    <option value="2" >2 Semanas</option>
                                    <option value="3" >3 Semanas</option>
                                    <option value="4" >1 Mes</option>
                                    <option value="5" >2 Meses</option>
                                    <option value="6" >3 Meses</option>
                                    <option value="7" >4 Meses</option>
                                    <option value="8" >5 Meses</option>
                                    <option value="9" >6 Meses</option>
                                    <option value="10">+6 Meses</option>
                                </select>
                                </div>
                                <div class="col-12">
                                    <label for="infoPropVenta" class="label_titulo">¿Por qué quieres vender?</label>
                                    <textarea name="infoPropVenta" id="infoPropVenta" cols="30" rows="3" placeholder="¿Por qué quieres vender?" maxlength="1000"></textarea>
                                </div>
                                <div class="col-12">
                                    <label for="contactos" class="label_titulo">Contactos Adicionales</label>
                                    <!--  -->
                                    <div class="row">
                                        <div class="divC">
                                            <ol>
                                                <div id="contactos">
                                                </div>
                                            </ol>
                                            <a class="addC" id="addC" href="#" style="margin-right: 10px;" onclick="agregarContacto(); return false;"> Agregar contacto <i class="fas fa-plus-circle"></i></a>
                                            <a class="delC" id="delC" style="padding-top:7px;" href="#" onclick="eliminarContacto(); return false;"> Eliminar contacto <i class="fas fa-minus-circle"></i></a>
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna B1 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                    <h3 class="fs-subtitle">Paso 4</h3>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                    <label for="comuna" class="label_titulo">Comuna</label><br>
                                    <select name="comuna" id="comuna" class="fs-comuna">
                                <option value="null" disabled selected>Comuna*</option>
                                <?php
                                $sqlComuna = "Select * from Comuna order by nombre asc";
                                $result = $conn->query($sqlComuna);
                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value=".$row["id"].">".utf8_encode($row["nombre"])."</option>";
                                        }
                                    } 
                                ?>
                            </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                                    <label for="calle" class="label_titulo">Dirección</label><br>
                                    <input class="" style="width: 50%;" type="text" name="calle" placeholder="Calle*" id="calle" />
                                    <input class="fs-numb fs-tNumb" type="number" name="numero" placeholder="Número*" id="numero" />
                                    <input class="fs-letra" type="text" name="letra" placeholder="Casa" id="letra" />
                                </div>


                                <!-- <div class="col-12">
                                    <input class="fs-numb fs-tNumb" type="number" name="numero" placeholder="Número*" id="numero" />
                                    <input class="fs-letra" type="text" name="letra" placeholder="Casa" id="letra" />
                                </div> -->
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" style="margin-top: 5px; margin-bottom: 5px;">
                                    <label class="txtProp">
                                Condominio
                                <label class="switch" style="float: left;">
                                    <input type="checkbox" id="condominio" name="condominio" onchange="condominioF()">
                                    <span class="slider round"></span>
                                </label>
                                    </label>

                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                                    <label for="calleCond" class="label_titulo">Dirección Particular</label><br>
                                    <input class="fs-calle" type="text" name="calleCond" placeholder="Calle" id="calleCond" /> -
                                    <input class="fs-numb fs-tNumb" type="number" name="numeroCond" placeholder="Número" id="numeroCond" />
                                </div>
                                <div class="col-12" style="margin-top: 10px; margin-bottom: 10px;">
                                    <label class="txtProp">
                                Loteo
                                <label class="switch" style="float: left;">
                                    <input type="checkbox" id="loteo" name="loteo" onchange="loteoF()">                                    
                                    <span class="slider round"></span>
                                </label>
                                    </label>
                                </div>
                                <!-- <div class="col-6">
                                    <input class="fs-calle" type="text" name="calleCond" placeholder="Calle" id="calleCond" />
                                </div> -->
                                <div class="col-12">
                                    <label class="txtProp">
                                Pareada
                                <label class="switch" style="float: left;">
                                    <input type="checkbox" id="pareada" name="pareada" onchange="pareadaF()">                                   
                                    <span class="slider round"></span>
                                </label>
                                    </label>

                                </div>
                                <!-- <div class="col-6">
                                    <input class="fs-numb fs-tNumb" type="number" name="numeroCond" placeholder="Número" id="numeroCond" />
                                </div> -->
                                <div class="col-12">
                                    <label for="referencia" class="label_titulo">Referencia</label>
                                    <input class="fs-referencia" type="text" name="referencia" id="referencia" placeholder="Referencia" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna C1 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                </div>
                                <div class="col-12">
                                    <h3 class="fs-subtitle">Paso 5</h3>
                                </div>
                                <div class="col-12">
                                    <label class="label_titulo">Rol</label>
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="text">
                                        </div>
                                        -
                                        <!-- <div class="col-2" style="text-align:center; padding-top: 15px;">
                                            <label ">-</label>
                                </div> -->
                                        <div class="col-4 ">
                                            <input type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <label for="construccion " class="label_titulo fs-tNumb ">Año de Construcción</label><br>
                                    <input class="fs-contribucion fs-tNumb " type="number" name="construccion " id="construccion " placeholder="YYYY" onfocusout="validarAno( 'construccion') " />
                                </div>
                                <div class="col-12 ">
                                    <label for="adquisicion " class="label_titulo  fs-tNumb ">Año de Adquisición</label><br>
                                    <input class="fs-contribucion fs-tNumb " type="number " name="adquisicion " id="adquisicion " placeholder="YYYY" onfocusout="validarAno( 'adquisicion') " />
                                </div>
                                <div class="col-12 ">
                                    <label for="contri " class="label_titulo fs-tNumb ">Valor Contribución</label><br>
                                    <input class="fs-contribucion fs-tNumb " type="text " name="contri " id="contri " placeholder="$ " onfocusout="validarPrecio( 'contri') " />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna A2 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <div class="container-fluid ">
                            <div class="row container-matchCol ">
                                <div class="col-12 ">
                                    <h2 class="fs-title ">Información Propiedad</h2>
                                    <h3 class="fs-subtitle ">Paso 6</h3>
                                </div>
                                <div class="col-12 ">
                                    <label for="gastoC " class="label_titulo ">Valor Gasto Común</label>
                                    <input class="fs-contribucion fs-tNumb " style="width: 100% !important;" type="text " name="gastoC " id="gastoC " placeholder="Valor Gasto común " onfocusout="validarPrecio( 'gastoC') " />
                                </div>
                                <div class="col-12 ">
                                    <label for="gastos_comun " class="label_titulo ">¿Qué incluyen los gastos comunes?</label>
                                    <textarea name="gastos_comun " id="gastos_comun " cols="30 " rows="3 " placeholder="¿Qué Incluyen los Gastos Comunes? " maxlength="1000 "></textarea>
                                </div>
                                <div class="col-12 ">
                                    <label for="tipoGas " class="label_titulo ">Tipo de Gas</label>
                                    <select name="tipoGas " id="tipoGas " class="fs-nDorm ">
                                <option value=" " disabled selected>Tipo de gas</option>
                                <?php
                                    $sql = "SELECT * from Tipo_gas where id_tipo_gas NOT IN (3) order by tipo ";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value='".$row["id_tipo_gas"]."'>".$row["tipo"]."</option>"; } } ?>
                                            </select>
                                </div>
                                <div class="col-12">
                                    <label for="calefaccion" class="label_titulo">Calefacción</label>
                                    <select name="calefaccion" id="calefaccion" class="fs-nDorm" onchange="fnCalefa()">
                            <option value="" disabled selected>Tipo de calefacción</option>
                                <?php
                                    $sqlCalefa = "SELECT * FROM Tipo_calefaccion where id NOT IN (5,6) order by tipo";
                                    $result = $conn->query($sqlCalefa);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo '<option value="'.$row["id"].'">'.utf8_encode($row["tipo"]).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                                </div>
                                <div class="col-12">
                                    <label for="aguaCaliente" class="label_titulo">Agua Caliente</label>
                                    <select name="aguaCaliente" id="aguaCaliente" class="fs-nDorm" onchange="fnAguaCaliente()">
                                <option value="" disabled selected>Agua caliente</option>
                                <?php
                                $sql = "SELECT * from Agua_caliente";
                                $result = $conn->query($sql);
                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value='".$row["id_agua_caliente"]."'>".$row["tipo"]."</option>";
                                    }
                                }
                                
                                ?>
                            </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna B2 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                    <h3 class="fs-subtitle">Paso 3 (5/10)</h3>
                                </div>
                                <div class="col-6">
                                    <label for="supU" class="label_titulo" style="margin-top: 20px;">Superficie Útil</label>
                                    <input class="fs-supU fs-tNumb" type="text" name="supU" id="supU" placeholder="m2 util" onfocusout="validarPrecio('supU')" />
                                </div>
                                <div class="col-6">
                                    <label for="supTerr" class="label_titulo">Superficie Terreno</label>
                                    <input class="fs-supTerr fs-tNumb" type="text" name="supTerr" id="supTerr" placeholder="m2 terreno" onfocusout="validarPrecio('supTerr')" />
                                </div>
                                <div class="col-6">
                                    <label for="supPatio" class="label_titulo">Superficie Patio</label>
                                    <input class="fs-supTerr fs-tNumb" type="number" name="supPatio" id="supPatio" onfocusout="validarPrecio('supPatio')" placeholder="Superficie patio" />
                                </div>
                                <div class="col-6">
                                    <div class="row">
                                        <div class="column col-4">
                                            <label class="txtProp">
                                        Orientación
                                    </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">N
                                        <input type="checkbox" id="norte" name="norte" onchange="">
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">S
                                        <input type="checkbox" id="sur" name="sur" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">O
                                        <input type="checkbox" id="oriente" name="oriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">P
                                        <input type="checkbox" id="poniente" name="poniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">NO
                                        <input type="checkbox" id="nOriente" name="nOriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">NP
                                        <input type="checkbox" id="nPoniente" name="nPoniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">SO
                                        <input type="checkbox" id="sOriente" name="sOriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">SP
                                        <input type="checkbox" id="sPoniente" name="sPoniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6" style="margin-top: -55px;">
                                    <label for="CantPisosCasa" class="label_titulo">Cantidad de Pisos Casa</label>
                                    <select name="CantPisosCasa" id="CantPisosCasa" class="fs-ori" style="">
                                    <option value="" disabled selected>Cant. de pisos casa</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                </div>
                                <div class="col-6">
                                    <!-- <div class="row">
                                        <div class="column col-4">
                                            <label class="txtProp">
                                        Orientación
                                    </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">N
                                        <input type="checkbox" id="norte" name="norte" onchange="">
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">S
                                        <input type="checkbox" id="sur" name="sur" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">O
                                        <input type="checkbox" id="oriente" name="oriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">P
                                        <input type="checkbox" id="poniente" name="poniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">NO
                                        <input type="checkbox" id="nOriente" name="nOriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">NP
                                        <input type="checkbox" id="nPoniente" name="nPoniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">SO
                                        <input type="checkbox" id="sOriente" name="sOriente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                        <div class="column col-5">
                                            <label class="fs-chkLbl">SP
                                        <input type="checkbox" id="sPoniente" name="sPoniente" onchange="" >
                                        <span class="checkmark"></span>
                                    </label>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna C2 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                    <h3 class="fs-subtitle">Paso 3 (6/10)</h3>
                                </div>
                                <div class="col-6">
                                    <label for="num_dorm" class="label_titulo">Número de Dormitorios</label>
                                    <select name="num_dorm" id="num_dorm" class="fs-nDorm" onchange="nDorm()">
                                <option value="" disabled selected>N° de dormitorios</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                                </div>
                                <div class="col-6">
                                    <label for="cant_banos" class="label_titulo">Número de Baños</label>
                                    <select name="cant_banos" id="cant_banos" class="fs-nBan" onchange="bannos()">
                                <option value="0" disabled selected>N° de baños</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna A3 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                    <h3 class="fs-subtitle">Paso 3 (7/10)</h3>
                                </div>
                                <div class="col-6">
                                    <label for="select_estacionamientos" class="label_titulo">Cantidad de Estacionamientos</label>
                                    <select name="select_estacionamientos" id="select_estacionamientos" class="fs-nEst" onchange="estacionamientos()">
                                <option value="0" disabled selected>Cantidad de estacionamientos</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                                </div>
                                <div class="col-6">
                                    <label for="select_bodega" class="label_titulo">Cantidad de Bodegas</label>
                                    <select name="select_bodega" id="select_bodega" class="fs-nEst">
                                <option value="" disabled selected>Cantidad de bodega(s)</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="0">No tiene</option>
                            </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna B3 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                    <h3 class="fs-subtitle">Paso 3 (8/10)</h3>
                                </div>
                                <div class="col-6">
                                    <label for="material_pisos_comunes" class="label_titulo">Material Pisos Espacio Comunes</label>
                                    <select name="material_pisos_comunes" id="material_pisos_comunes" class="fs-nDorm">
                                <option value="" disabled selected>Material Pisos Espacio Comunes</option>
                                <?php
                                $sqlMateriales = "SELECT * from Materiales_pisos";
                                $result = $conn->query($sqlMateriales);

                                if ($result->num_rows > 0) {
                                // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                    echo '<option value="'.$row["id_materiales_pisos"].'">'.utf8_encode($row["material"]).'</option>';
                                    }
                                }
                                ?>
                            </select>
                                </div>
                                <div class="col-6">
                                    <label for="material_pisos_dorm" class="label_titulo">Material Pisos Dormitorios</label>
                                    <select name="material_pisos_dorm" id="material_pisos_dorm" class="fs-nDorm">
                                <option value="" disabled selected>Material Pisos Dormitorios</option>
                                <?php
                                $sqlMateriales = "SELECT * from Materiales_pisos";
                                $result = $conn->query($sqlMateriales);

                                if ($result->num_rows > 0) {
                                // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                    echo '<option value="'.$row["id_materiales_pisos"].'">'.utf8_encode($row["material"]).'</option>';
                                    }
                                }
                                ?>
                            </select>
                                </div>
                                <div class="col-6">
                                    <label for="material_pisos_bano" class="label_titulo">Material Piso Baños</label>
                                    <select name="material_pisos_bano" id="material_pisos_bano" class="fs-nDorm">
                                <option value="" disabled selected>Material Piso baños</option>
                                <option value="1">Baldosas</option>
                                <option value="2">Baldosín cerámico</option>
                                <option value="3">Cerámica</option>
                                <option value="4">Flexit</option>
                                <option value="5">Madera</option>
                                <option value="6">Mármol</option>
                                <option value="7">Parquet</option>
                                <option value="8">Piso flotante</option>
                                <option value="9">Piso termolaminado</option>
                                <option value="10">Porcelanato</option>
                                <option value="11">Piso fotolaminado</option>
                            </select>
                                </div>
                                <div class="col-6">
                                    <label for="material_pisos_cocina" class="label_titulo">Material Piso Cocina</label>
                                    <select name="material_pisos_cocina" id="material_pisos_cocina" class="fs-nDorm">
                                <option value="" disabled selected>Material Piso Cocina</option>
                                <?php
                                $sqlMateriales = "SELECT * from Materiales_pisos";
                                $result = $conn->query($sqlMateriales);

                                if ($result->num_rows > 0) {
                                // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                    echo '<option value="'.$row["id_materiales_pisos"].'">'.utf8_encode($row["material"]).'</option>';
                                    }
                                }
                                ?>
                            </select>
                                </div>
                                <div class="col-6">
                                    <label for="material_pisos_terraza" class="label_titulo">Material Piso Terraza</label>
                                    <select name="material_pisos_terraza" id="material_pisos_terraza" class="fs-nDorm">
                                <option value="" disabled selected>Material Piso Terraza</option>
                                <?php
                                $sqlMateriales = "SELECT * from Materiales_pisos";
                                $result = $conn->query($sqlMateriales);

                                if ($result->num_rows > 0) {
                                // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                    echo '<option value="'.$row["id_materiales_pisos"].'">'.utf8_encode($row["material"]).'</option>';
                                    }
                                }
                                ?>
                            </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- columna C3 -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                    <h3 class="fs-subtitle">Paso 3 (9/10)</h3>
                                </div>

                                <style>
                                    /* CHECKS */
                                    
                                    .form-check {
                                        padding-left: 0!important;
                                        padding-bottom: 5px;
                                    }
                                    
                                    [type="checkbox"]:checked,
                                    [type="checkbox"]:not(:checked) {
                                        position: absolute;
                                        width: 0;
                                    }
                                    
                                    [type="checkbox"]:checked+label,
                                    [type="checkbox"]:not(:checked)+label {
                                        position: relative;
                                        padding-left: 25px;
                                        cursor: pointer;
                                    }
                                    
                                    [type="checkbox"]:checked+label:before,
                                    [type="checkbox"]:not(:checked)+label:before {
                                        content: '';
                                        position: absolute;
                                        left: 0;
                                        top: 0;
                                        width: 20px;
                                        height: 20px;
                                        border: 1px solid #ddd;
                                        background: #fff;
                                    }
                                    
                                    [type="checkbox"]:checked+label:after,
                                    [type="checkbox"]:not(:checked)+label:after {
                                        content: '';
                                        width: 14px;
                                        height: 14px;
                                        background: #e8505b;
                                        position: absolute;
                                        top: 3px;
                                        left: 3px;
                                        -webkit-transition: all 0.2s ease;
                                        transition: all 0.2s ease;
                                    }
                                    
                                    [type="checkbox"]:not(:checked)+label:after {
                                        opacity: 0;
                                        -webkit-transform: scale(0);
                                        transform: scale(0);
                                    }
                                    
                                    [type="checkbox"]:checked+label:after {
                                        opacity: 1;
                                        -webkit-transform: scale(1);
                                        transform: scale(1);
                                    }
                                    
                                    [type="checkbox"]+label:hover {
                                        background: #ddd;
                                    }
                                </style>

                                <!-- Aqui van los ddl uwu -->
                                <div class="col-12">
                                    <label class="label_titulo" style="margin-bottom: 10px;">Tipo Transacción</label>
                                    <div class="dropdown">
                                        <button class="btn btn-dropdown dropdown-toggle" style="margin-bottom: 10px;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Tipo de transaccion 
                            </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="venta">
                                                <label class="form-check-label w-100" for="venta">
                                    Venta
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="arriendo">
                                                <label class="form-check-label w-100" for="arriendo">
                                    Arriendo
                                    </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="label_titulo" style="margin-bottom: 10px;">Tipo Cocina</label>
                                    <div class="dropdown">
                                        <button class="btn btn-dropdown dropdown-toggle" style="margin-bottom: 10px;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Cocina
                            </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="cocinaIndep" name="cocinaIndep">
                                                <label class="form-check-label w-100" for="cocinaIndep">
                                        Cocina Independiente
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="cocinaIntegrada" name="cocinaIntegrada">
                                                <label class="form-check-label w-100" for="cocinaIntegrada">
                                        Cocina Integrada
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="cocinaAme" name="cocinaAme">
                                                <label class="form-check-label w-100" for="cocinaAme">
                                        Cocina Americana
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="cocinaIsla" name="cocinaIsla">
                                                <label class="form-check-label w-100" for="cocinaIsla">
                                        Cocina Isla
                                    </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <label class="label_titulo" style="margin-bottom: 10px;">Tipo accesorio cocina</label>
                                    <div class="dropdown">
                                        <button class="btn btn-dropdown dropdown-toggle" style="margin-bottom: 10px;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Accesorios y Espacios Cocina
                            </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="encimeraElect" name="encimeraElect">
                                                <label class="form-check-label w-100" for="encimeraElect">
                                        Encimera Eléctrica
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="encimeraGas" name="encimeraGas">
                                                <label class="form-check-label w-100" for="encimeraGas">
                                        Encimera Gas
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="hornoEmp" name="hornoEmp">
                                                <label class="form-check-label w-100" for="hornoEmp">
                                        Horno Empotrado
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="refrigerador" name="refrigerador">
                                                <label class="form-check-label w-100" for="refrigerador">
                                        Refrigerador Empotrado
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="comedor" name="comedor">
                                                <label class="form-check-label w-100" for="comedor">
                                        Comedor de Diario
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="patioServicio" name="patioServicio">
                                                <label class="form-check-label w-100" for="patioServicio">
                                        Patio de Servicio
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="despensa" name="despensa">
                                                <label class="form-check-label w-100" for="despensa">
                                        Cuarto Despensa
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="logia" name="logia">
                                                <label class="form-check-label w-100" for="logia">
                                        Logia
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="roller" name="roller">
                                                <label class="form-check-label w-100" for="roller">
                                        Cortina Roller
                                    </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="cortina_elec" name="cortina_elec">
                                                <label class="form-check-label w-100" for="cortina_elec">
                                        Cortinas Eléctricas
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="persiana_madera" name="persiana_madera">
                                                <label class="form-check-label w-100" for="persiana_madera">
                                        Persiana Madera
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="persiana_aluminio" name="persiana_aluminio">
                                                <label class="form-check-label w-100" for="persiana_aluminio">
                                    Persiana Aluminio
                                    </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <label class="label_titulo" style="margin-bottom: 10px;">Seguridad</label>
                                    <div class="dropdown">
                                        <button class="btn btn-dropdown dropdown-toggle" style="margin-bottom: 10px;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Seguridad y Temperatura
                            </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="citofono" name="citofono">
                                                <label class="form-check-label w-100" for="citofono">
                                        Citófono
                                    </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="alarma" name="alarma">
                                                <label class="form-check-label w-100" for="alarma">
                                        Alarma
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="aire" name="aire">
                                                <label class="form-check-label w-100" for="aire">
                                        Aire Acondicionado
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="termoPanel" name="termoPanel">
                                                <label class="form-check-label w-100" for="termoPanel">
                                        Termopanel
                                    </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="label_titulo" style="margin-bottom: 10px;">Tipo de Espacios</label>
                                    <div class="dropdown">
                                        <button class="btn btn-dropdown dropdown-toggle" style="margin-bottom: 10px;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Otros Espacios
                            </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="escritorio" name="escritorio">
                                                <label class="form-check-label w-100" for="escritorio">
                                        Escritorio
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="sala_estar" name="sala_estar">
                                                <label class="form-check-label w-100" for="sala_estar">
                                        Sala de Estar
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="sala_juegos" name="sala_juegos">
                                                <label class="form-check-label w-100" for="sala_juegos">
                                        Sala de Juegos
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="planchado" name="planchado">
                                                <label class="form-check-label w-100" for="planchado">
                                        Sala de Planchado
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="bar" name="bar">
                                                <label class="form-check-label w-100" for="bar">
                                        Bar
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="cavaVinos" name="cavaVinos">
                                                <label class="form-check-label w-100" for="cavaVinos">
                                        Cava de Vinos
                                    </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="label_titulo" style="margin-bottom: 10px;">Tipo de Recreacion</label>
                                    <div class="dropdown">
                                        <button class="btn btn-dropdown dropdown-toggle" style="margin-bottom: 10px;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Recreación y Otros
                            </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="sauna" name="sauna">
                                                <label class="form-check-label w-100" for="sauna">
                                        Sauna
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="jacuzzi" name="jacuzzi">
                                                <label class="form-check-label w-100" for="jacuzzi">
                                        Jacuzzi
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="atico" name="atico">
                                                <label class="form-check-label w-100" for="atico">
                                        Ático
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="hall" name="hall">
                                                <label class="form-check-label w-100" for="hall">
                                        Hall de acceso
                                    </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="label_titulo" style="margin-bottom: 10px;">Tipo de Exteriores</label>
                                    <div class="dropdown">
                                        <button class="btn btn-dropdown dropdown-toggle" style="margin-bottom: 10px;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Exterior
                            </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="atejardin" name="atejardin">
                                                <label class="form-check-label w-100" for="atejardin">
                                        Antejardín
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="riego" name="riego">
                                                <label class="form-check-label w-100" for="riego">
                                        Riego Automático
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="piscina" name="piscina">
                                                <label class="form-check-label w-100" for="piscina">
                                        Piscina
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="piscina_temp" name="piscina_temp">
                                                <label class="form-check-label w-100" for="piscina_temp">
                                        Piscina Climatizada
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="quincho" name="quincho">
                                                <label class="form-check-label w-100" for="quincho">
                                        Quincho
                                    </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="panelSolar" name="panelSolar">
                                                <label class="form-check-label w-100" for="panelSolar">
                                        Panel Solar
                                    </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                    <!-- columna D1 -->
                    <div class="col-12">
                        <div class="container-fluid">
                            <div class="row container-matchCol">
                                <div class="col-12">
                                    <h2 class="fs-title">Información Propiedad</h2>
                                    <h3 class="fs-subtitle">Paso 3 (10/10)</h3>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="disponibilidad" class="label_titulo">Disponibilidad Visitas</label>
                                    <input class="fs-contribucion" type="text" name="disponibilidad" name="disponibilidad" placeholder="Disponibilidad visitas [Ej. L-V 11:00-17:00]" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="url" class="label_titulo">URL</label>
                                    <input class="fs-contribucion" type="text" id="url" name="url" placeholder="URL" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="propDestacado" class="label_titulo">Destacado</label>
                                    <textarea name="propDestacado" id="propDestacado" cols="30" rows="4" placeholder="¿Algo que quieras destacar de la propiedad?" maxlength="1000"></textarea>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="nota" class="label_titulo">Notas</label>
                                    <textarea name="nota" id="nota" cols="30" rows="4" placeholder="Notas" maxlength="1000"></textarea>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                    <label for="cartel">
                                Cartel
                            </label>
                                    <label class="switch" style="float: right;">
                                <input type="checkbox" id="cartel" name="cartel">
                                <span class="slider round"></span>
                            </label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                    <label for="llave">
                                Llave
                            </label>
                                    <label class="switch" style="float: right;">
                                <input type="checkbox" id="llave" name="llave">
                                <span class="slider round"></span>
                            </label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                    <label for="mascotas">
                                Mascotas
                            </label>
                                    <label class="switch" style="float: right;">
                                <input type="checkbox" id="mascotas" name="mascotas">
                                <span class="slider round"></span>
                            </label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label for="imagen">Imagen Propiedad</label>
                                    <input type="file" name="imagen" id="imagen"></div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <input type="submit" name="save" class=" action-button" value="Guardar" style="" />
                                <input type="button" name="quit" id="quit" class=" action-button" value="Finalizar" style="" />
                            </div>
                            <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <input type="button" name="quit" id="quit" class=" action-button" value="Finalizar" style="float: right;" />
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <?
    include '../pages/footer.php';
?>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>
        <script src="../js/toastr.js"></script>