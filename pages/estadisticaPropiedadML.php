<?php

include 'sidebarMenu.php';
include '..conexion.php';

// echo $_REQUEST["id"];


?>
    <div class="container-fluid" style="padding-top: 80px; padding-left: 70px;">
        <div class="row" style="border:solid grey 1px">
            <div class="col-6">
                <h2 id="titulo"></h2>
            </div>
            <div class="col-2">
                <label for="" id="codigo"></label>
            </div>
            <div class="col-2">
                <label for="" id="tipo"></label>
            </div>
            <div class="col-2">
                <label for="" id="operacion"></label>
            </div>
        </div>
        <div class="row" style="border-bottom: solid grey 1px;">
            <div class="col-4" style="text-align: center;">
                <div class="row">
                    <div class="col-12">
                        <h2>Datos</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12" style="text-align: center;">
                        <div class="card" style="width: 18rem;margin: auto;">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" id="carousel-inner">
                                    
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <!-- <img src="C:\Users\Carlos\Pictures\casa.jpeg" class="card-img-top" alt="..."> -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6" style="text-align: start;">
                                        Visitas:
                                    </div>
                                    <div class="col-6">
                                        40
                                    </div>
                                    <div class="col-6" style="text-align: start">
                                        Contactos:
                                    </div>
                                    <div class="col-6">
                                        2
                                    </div>
                                    <div class="col-6" style="text-align: start">
                                        Precio:
                                    </div>
                                    <div class="col-6">
                                        <label for="" id="precio">2</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-8" style="border-left: solid grey 1px; text-align: center">
                <div class="row">
                    <div class="col-12">
                        <h2>Estadisticas</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <canvas id="visitasGraf" width="400" height="100"></canvas>
                        <!-- <img src="C:\Users\Carlos\Pictures\grafico.jpg" alt="" width="500px"> -->
                    </div>
                </div>
            </div>

        </div>
        <div class="row" style="">
            <div class="col-12" style="text-align: center;">
                <h2>Comentarios</h2>
            </div>
        </div>
        <div class="row">
            <div class="container" id="comentarios">
                
            </div>
        </div>
    </div>


<script>
    
token();
var access_token = "";
var refresh_token = "";
function token(){
    // comprobar si hay token activo
    fetch('../controlador/comprobarTokenMercadoLibre.php',{
        method: 'GET',
    })
    .then( res=> res.json())
    .then( comprobacion => {
        if(comprobacion.message === "yes"){
            console.log("hay token activo");
            console.log(comprobacion);
            console.log(comprobacion.access_token);
            console.log(comprobacion.refresh_token);
            obtenerDatosPropiedad('<?=$_REQUEST["id"]?>');
            cargarComentarios(comprobacion.access_token);
        }else{
            console.log("token malformed " + comprobacion.access_token);
        }
    })
    //fin comprobar si hay token activo
}

function obtenerDatosPropiedad(idPropiedad){
    fetch('https://api.mercadolibre.com/items/'+idPropiedad,{
        method: 'GET'
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        let titulo = document.getElementById("titulo");
        let precio = document.getElementById("precio");
        let carousel = document.getElementById("carousel-inner");
        let codigo = document.getElementById("codigo");
        let tipoHTML = document.getElementById("tipo");
        let operacionHTML = document.getElementById("operacion");
        let tipo = "";
        let operacion = "";

        codigo.innerHTML = data.id;
        titulo.innerHTML = data.title;

        if (data.domain_id === "MLC-APARTMENTS_FOR_RENT") {
            tipo = "Depto";
            operacion = "Arriendo";
        } else if (data.domain_id === "MLC-INDIVIDUAL_APARTMENTS_FOR_SALE") {
            tipo = "Depto";
            operacion = "Venta";
        } else if (data.domain_id === "MLC-OFFICES_FOR_RENT") {
            tipo = "Oficina";
            operacion = "Arriendo";
        } else if (data.domain_id === "MLC-INDIVIDUAL_OFFICES_FOR_SALE") {
            tipo = "Oficina";
            operacion = "Venta";
        } else if (data.domain_id === "MLC-INDIVIDUAL_HOUSES_FOR_SALE") {
            tipo = "Casa";
            operacion = "Venta";
        } else if (data.domain_id === "MLC-RETAIL_SPACE_FOR_RENT") {
            tipo = "Local";
            operacion = "Arriendo";
        } else if (data.domain_id === "MLC-RETAIL_SPACE_FOR_SALE") {
            tipo = "Local";
            operacion = "Venta";
        }

        tipoHTML.innerHTML = tipo;
        operacionHTML.innerHTML = operacion;

        if(data.currency_id === "CLP"){
            precio.innerHTML = "$" + data.price;
        }else{
            precio.innerHTML = "UF " + data.price;
        }
        let cont = 0;
        data.pictures.forEach(imagen => {
            if(cont === 0){
                carousel.innerHTML += `<div class="carousel-item active">
                        <img class="d-block w-100" src="${imagen.url}" alt="First slide">
                    </div>`;
            }else{
                carousel.innerHTML += `<div class="carousel-item">
                        <img class="d-block w-100" src="${imagen.url}" alt="First slide">
                    </div>`;
            }
            cont++;
            
        });
        carousel.innerHTML += '';

        fetch('https://api.mercadolibre.com/visits/items?ids='+idPropiedad,{
            method: 'GET'
        })
        .then(res=>res.json())
        .then(data=>{
            // let visitas = document.getElementById("visitas");
            // visitas.innerHTML = "Visitas:" + data[idPropiedad];
        })

        
    })
}

</script>
<?php
include 'footer.php';
?>
<script>
    let visitasGraf = document.getElementById("visitasGraf");
    fetch('https://api.mercadolibre.com/items/<?=$_GET["id"]?>/visits/time_window?last=7&unit=day&ending=2020-10-21',{
        method: 'GET'
    })
    .then(res=> res.json())
    .then(data => {
        console.log(data);
        var data = {
            labels: [data.results[0].date.split("T")[0],data.results[1].date.split("T")[0],data.results[2].date.split("T")[0],data.results[3].date.split("T")[0],data.results[4].date.split("T")[0],data.results[5].date.split("T")[0],data.results[6].date.split("T")[0]],
            datasets: [
                {
                    label: "Últimos 7 días",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(232, 80, 90,0.4)",
                    borderColor: "rgba(232, 80, 90 ,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(238, 23, 30,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(238, 23, 30,1)",
                    pointHoverBorderColor: "rgba(238, 23, 30,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 3,
                    pointHitRadius: 10,
                    data: [data.results[0].total, data.results[1].total, data.results[2].total, data.results[3].total, data.results[4].total,data.results[5].total,data.results[6].total],
                    spanGaps: false,
                }
            ]
        }
        var stackedLine = new Chart(visitasGraf, {
            type: 'line',
            data: data,
            options: {
            }
        });
    });
function cargarComentarios(access_token){
    fetch('https://api.mercadolibre.com/questions/search?item=<?=$_GET["id"]?>&access_token='+access_token+'&api_version=2',{
        method: 'GET'
    })
    .then(res=> res.json())
    .then(data => {
        console.log(data);
        if(data.message == "Invalid token"){
            console.log("token invalido");
            window.location = "http://auth.mercadolibre.com.ar/authorization?response_type=code&amp;client_id=2868867605753550&amp;redirect_uri=https://indev9.com/redflip/pages/mercadoLibre.php";
        }
        let comentarios = document.getElementById("comentarios");
        comentarios.innerHTML = "";
        data.questions.forEach(pregunta => {
            fetch(`https://api.mercadolibre.com/questions/${pregunta.id}?access_token=${access_token}`,{
                method : 'GET'
            })
            .then(res => res.json())
            .then(data2 =>{
                comentarios.innerHTML += `
                <div class="row" style="border-bottom:solid grey 1px">
                    <div class="col-4 pt-5">
                        ${data2.from.first_name} ${data2.from.last_name}
                    </div>
                    <div class="col-8">

                    </div>
                    <div class="col-4 pt-2">
                        ${data2.from.email}
                    </div>
                    <div class="col-8 pt-2" style="text-align: end;">
                        ${data2.date_created.split("T")[0]}
                    </div>
                    <div class="col-12 pb-2">
                        <textarea name="" id="" cols="30" rows="5" style="width:100%" disabled>${data2.text}</textarea>
                </div>`;
            })
            
        });
    })
    .catch(error=> {
        console.error("error en peticion",error);
    });
}
    
</script>