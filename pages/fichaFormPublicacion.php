<?php

include '../conexion.php';
include "../services/funciones.php";
$id = $_GET["id"];

$sql = "SELECT DISTINCT
        per.nombre,
        per.rut,
        per.apellido,
        per.telefono,
        per.correo,
        per.id_persona AS idPer,
        dir.calle,
        dir.numero,
        dir.referencia,
        dir.id AS idDir,
        com.nombre AS Comuna,
        com.id AS idCom,
        vent.descripcion V_A,
        vent.id AS idVent,
        divi.nombre AS divisa,
        divi.id AS idDiv,
        propiedad.id_propiedad AS idPropiedad,
        propiedad.fk_propietario,
        propiedad.nota,
        propiedad.monto,
        est.tipo,
        est.id AS idEst,
        sub.fase,
        sub.id AS idSub,
        propietario.cont,
        propietario.id_propietario AS idPropietario,
        propietario.contactos,
        Origen.nom_origen AS ori,
        Origen.id_origen AS id_or,
        formulario.*
        FROM
        Persona per
        INNER JOIN Propietario propietario,
        Estado_prop est,
        Sub_estado sub,
        Propiedad propiedad,
        Divisa divi,
        Direccion dir,
        Comuna com,
        Venta_Arriendo vent,
        Origen,
        formulario
        WHERE
        per.id_persona = propietario.fk_persona AND propietario.fk_estado_prop = est.id AND est.fk_sub_estado = sub.id AND propietario.id_propietario = propiedad.fk_propietario AND propiedad.divisa = divi.id AND propiedad.fk_direccion = dir.id AND dir.fk_comuna = com.id AND propiedad.fk_venta_arriendo = vent.id AND propietario.fk_origen = Origen.id_origen AND propiedad.fk_estado_propiedad = 1 AND propiedad.fk_formulario = formulario.id_formulario AND formulario.id_formulario =$id";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        // info propietario
        $nombre = utf8_encode($row["nombre"]);
        $apellido = utf8_encode($row["apellido"]);
        $rut = $row["rut"];
        $telefono = $row["telefono"];
        $correo = $row["correo"];
        $fecha = $row["fecha"];

        // info corredor y operario
        $area = $row["area_redflip"];
        $nom_corredor = $row["fk_corredor"];
        $nom_operario = $row["fk_operario"];

        // info propiedad
        $operacion = $row["operacion"];
        $tipo_prop = $row["fk_tipo_propiedad"];
        $valorCLP = $row["valor_clp"];
        $valorUF = $row["valor_uf"];
        $monto1 = $row["monto1"];
        $divisa_monto1 = $row["divisa_monto1"];
        $monto2 = $row["monto2"];
        $divisa_monto2 = $row["divisa_monto2"];
        if($divisa_monto1 == 1){
            $divisa_monto1 = "UF";
        }else if($divisa_monto1 == 2){
            $divisa_monto1 = "CLP";
        }else{
            $divisa_monto1 = "";
        }
        if($divisa_monto2 == 1){
            $divisa_monto2 = "UF";
        }else if($divisa_monto2 == 2){
            $divisa_monto2 = "CLP";
        }else{
            $divisa_monto2 = "";
        }
        $direccion = $row["Direccion_id"];
        $condominio = $row["condominio"];
        $ref = $row["referencia"];
        $exclusividad = $row["exclusividad"];
        $amoblado = $row["amoblado"];
        $anno_construccion = $row["anno"];
        $rolPropNuevo = $row["rol"];
        $contribucion = $row["contribuciones_trimestrales"];
        $gasto_comun = $row["gastos_comunes"];
        $nota_gastos = $row["nota_gastos_com"];
        $sup_total = $row["superficie_total"];
        $sup_util = $row["superficie_util"];
        $sup_terraza = $row["superficie_terraza"];
        $sup_terreno = $row["superficie_terreno"];
        $cant_pisos_casa = $row["cant_pisos_casa"];
        $orientacion = $row["orientacion"];
        $dormitorios = $row["dormitorios"];
        $dorm_suite = $row["dormitorios_suite"];
        $suite_walk_closet = $row["Suit_Walking_Closet"];
        $bannos = $row["cant_bannos"];
        $estacionamiento = $row["estacionamiento"];
        $techado = $row["techado"];
        $ubicacion = $row["ubicacion_est"];
        $piso_depto = $row["piso_depto"];
        $cant_deptos = $row["cant_deptos"];
        $cant_ascensores = $row["ascensores"];
        $fin_propiedad = $row["fin_propiedad"];
        $bodega = $row["bodega"];
        $material_comun = $row["material_piso_comun"];
        $material_dorm = $row["material_piso_dorm"];
        $material_banno = $row["material_piso_banno"];
        $material_pisos_cocina = $row["material_pisos_cocina"];
        $material_pisos_terraza = $row["material_pisos_terraza"];
        $calefaccion = $row["tipo_calefaccion"];
        $agua_caliente = $row["fk_agua_caliente"];
        $casa_liv_separados = $row["casa_liv_separados"];
        $cant_pisos = $row["cant_pisos"];
        $dptoPiso = $row["dptoPiso"];
        $adquisicion = $row["adquisicion"];
        // otros sums
        $internet = $row["internet"];
        $wifi = $row["wifi"];
        $tel = $row["tel"];
        $satelite =$row["satelite"];
        $cable =$row["cable"];
        // fin otros sums
        $aire = $row["aire_acondicionado"];
        $ascensor_privado = $row["ascensor_privado"];
        $atico = $row["atico"];
        $bodega_esp = $row["bodega_esp"];
        $cocina_ame = $row["cocina_ame"];
        $cocina_enci = $row["cocina_enci"];
        $cocina_isla = $row["cocina_isla"];
        $encimeraGas = $row["encimeraGas"];
        $encimeraElect = $row["encimeraElect"];
        $cocinaIntegrada = $row["cocinaIntegrada"];
        $estractor = $row["estractor"];
        $comedor_diario = $row["comedor_diario"];
        $cortina_hanga = $row["cortina_hang"];
        $cortina_roller = $row["cortina_roller"];
        $cortina_elec = $row["cortina_elec"];
        $dorm_serv = $row["dorm_serv"];
        $escritorio = $row["escritorio"];
        $horno_emp = $row["horno_emp"];
        $jacuzzi = $row["jacuzzi"];
        $logia = $row["logia"];
        $malla_proc_terraza = $row["malla_proc_terr"];
        $riego = $row["riego_auto"];
        $sala_estar = $row["sala_estar"];
        $alarma = $row["alarma"];
        $termo_panel = $row["term_panel"];
        $lavavajilla_emp = $row["lavavajillas"];
        $microondas = $row["microondas"];
        $refrigerador = $row["refrigerador"];
        $despensa = $row["despensa"];
        $hall = $row["hall"];
        $pieza_planchado = $row["planchado"];
        $cocina_amob = $row["cocina_amob"];
        $citofono = $row["citofono"];
        $mansarda = $row["mansarda"];
        $walking_closet = $row["walking_closet"];
        $cerco_elec = $row["cerco_elec"];
        $ascensor_comun = $row["ascensor_comun"];
        $conserjeria = $row["conserje_24"];
        $est_visita = $row["est_visita"];
        $gimnasio = $row["gim"];
        $juegos_inf = $row["juegos"];
        $lavanderia = $row["lavanderia"];
        $piscina = $row["piscina"];
        $porton_elec = $row["porton_elec"];
        $quincho = $row["quincho"];
        $terraza_quincho = $row["terraza_quincho"];
        $sala_juegos = $row["sala_juegos"];
        $sala_reuniones = $row["sala_reuniones"];
        $sala_multi_uso = $row["sala_multi_uso"];
        $sauna = $row["sauna"];
        $piscina_temp = $row["piscina_temp"];
        $gourmet_room = $row["gourmet_room"];
        $azotea = $row["azotea"];
        $cancha_tenis = $row["cancha_tenis"];
        $panelSolar = $row["panelSolar"];
        $circ_tv = $row["circ_tv"];
        $area_verde = $row["area_verde"];
        $sala_cine = $row["sala_cine"];
        $patio_serv = $row["patio_serv"];
        $antejardin = $row["antejardin"];
        $disponibilidad_visita = utf8_encode($row["disponibilidad_visita"]);
        $disponibilidad_entrega = $row["disponibilidad_entrega"];
        $notas = $row["notas"];
        $mascotas = $row["mascotas"];
        $cartel = $row["cartel"];
        $fk_tipo_gas = $row["fk_tipo_gas"];
        $infoPropVenta = $row["infoPropVenta"];
        $fk_propietario = $row["fk_propietario"];
        $cantCorredor = $row["cantCorredor"];
        $fk_tiempo_publicacion = $row["fk_tiempo_publicacion"];
        $hipoteca = $row["hipoteca"];
        $fk_banco = $row["fk_banco"];
        $fk_tipo_calefaccion = $row["fk_tipo_calefaccion"];
        $sup_patio = $row["sup_patio"];
        $uso_goce = $row["uso_goce"];
        $propDestacado = $row["propDestacado"];
        $loteo = $row["loteo"];
        $pareada = $row["pareada"];
        $cocinaIndep = $row["cocinaIndep"];
        $persiana_madera = $row["persiana_madera"];
        $persiana_aluminio = $row["persiana_aluminio"];
        $bar = $row["bar"];
        $cava_vinos = $row["cava_vinos"];
        $accesos_controlados = $row["accesos_controlados"];
        $plaza = $row["plaza"];
        $cancha_futbol = $row["cancha_futbol"];
        $cancha_golf = $row["cancha_golf"];
        $sala_juegos_ext = $row["sala_juegos_ext"];
        $fecha_entrega = $row["fecha_entrega"];
        $url = $row["url"];
        $llave = $row["llave"];
        $duplex = $row["duplex"];
        $triplex = $row["triplex"];
        $mariposa = $row["mariposa"];
        $penthouse =$row["penthouse"];
        $sala_eventos =$row["sala_eventos"];
        $bicicletero =$row["bicicletero"];
        $url = $row["url"];
        if(!file_exists($url)){
            $url = "https://eu.dlink.com/es/es/-/media/landing-pages/2018/mydlink/smart-home.png?h=519&la=es-ES&w=807";
        }
    }
} else {
    // echo $sql;
}

?>

<style>
    .table{
        font-size:16px;
    }
    textarea{
        width:100%;
        height:80px;
        background:#fff;
        border:none;
    }
    .tr-center{
        text-align:center;
        width:15%;
    }
    .td-vcenter{
        padding-top:80px!important;
    }
    .center{
        text-align:center;
    }
    th{
        background:#ee171eb3;
    }
</style>


<input type="hidden" id="idForm" value="<?php echo $id ?>">
    <section class="p-0 m-o">
        <div class="container">
            <div class="row">
                <div class="col-12">
                <!-- INICIO TABLA -->
                    <table class="table">
                        <tr>
                            <td  colspan="5">
                                <!-- <img src="https://eu.dlink.com/es/es/-/media/landing-pages/2018/mydlink/smart-home.png?h=519&la=es-ES&w=807" alt="" style="width:100%"> -->
                                <img src="<?=$url?>" alt="" style="width:100%">
                            </td>
                        </tr>
                    <!-- INICIO INFO PROPIETARIO -->
                        <tr>
                            <th colspan="5">
                                Información de propietario
                            </th>
                        </tr>
                        <tr>
                            <!-- Nombre -->
                            <td>Nombre</td>
                            <td colspan="4">
                                <?php echo utf8_encode($nombre)." ".utf8_encode($apellido)?>
                            </td>
                        </tr>
                        <tr>
                            <!-- Rut -->
                            <td>Rut</td>
                            <td colspan="4">
                                <?php echo $rut?>
                            </td>
                        </tr>
                        <tr>
                            <!-- Teléfono -->
                            <td>Teléfono</td>
                            <td colspan="4">
                                <?php echo $telefono?>
                            </td>
                        </tr>
                        <tr>
                            <!-- Correo -->
                            <td>Correo</td>
                            <td colspan="4">
                                <?php echo $correo?>
                            </td>
                        </tr>
                        <tr>
                            <!-- Fecha Nacimiento -->
                            <td>Fecha Nac.</td>
                            <td colspan="4">
                                <?php
                                    $date = date_create($fecha);
                                    echo date_format($date, 'd/m/Y')
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <!-- Razón de venta o arriendo -->
                            <td>Razón de venta o arriendo</td>
                            <td colspan="4">
                                <textarea name="" id="" cols="" rows="" disabled><?=(utf8_encode(ucfirst($infoPropVenta)))?></textarea>
                            </td>
                        </tr>
                    <!-- FIN INFO PROPIETARIO - INICIO CONTACTOS ADICIONALES-->
                        <tr >
                            <th colspan="5">Información Contacto</th>
                        </tr>
                            <?php
                                $sqlContactos = "SELECT * from Contacto where fk_propietario = $fk_propietario";
                                $result = $conn->query($sqlContactos);
                                $cont = 0;
                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        $cont++;
                                        echo '
                                        <tr>
                                            <td colspan="5">'.$cont.'</td>
                                        </tr>
                                        <tr>
                                            <td>Nombre</td>
                                            <td colspan="4">
                                                '.$row["nombre"].'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Teléfono</td>
                                            <td colspan="4">
                                                '.$row["telefono"].'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Correo</td>
                                            <td colspan="4">
                                                '.$row["correo"].'
                                            </td>
                                        </tr>';
                                    }
                                }
                            ?>
                    <!-- FIN CONTACTOS ADICIONALES  - INICIO INFO CORREDOR Y OPERARIO-->
                        <tr>
                            <th colspan="5">Información corredor y operario</th>
                        </tr>
                        <tr>
                            <td>Área Redflip</td>
                            <td colspan="4">
                                <?php echo $area?>
                            </td>
                        </tr>
                        <tr>
                            <td>Nombre Corredor</td>
                            <td colspan="4">
                                <?php
                                    $sqlCorredor = "select * from Persona, Corredor where Corredor.id_corredor = $nom_corredor and Corredor.fk_persona = Persona.id_persona";
                                    $result = $conn->query($sqlCorredor);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo utf8_encode($row["nombre"]);
                                            echo " ";
                                            echo utf8_encode($row["apellido"]);
                                            $correo_corredor = $row["correo"];
                                        }
                                    } else {
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Correo Corredor</td>
                            <td colspan="4">
                                <?php echo $correo_corredor?>
                            </td>
                        </tr>
                        <tr>
                            <td>Operario</td>
                            <td colspan="4">
                                <?php
                                    $sqlOperario = "select * from Persona, Operario where Operario.id_operario = $nom_operario and Operario.fk_persona = Persona.id_persona";
                                    $result = $conn->query($sqlOperario);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo $row["nombre"] ." " .$row["apellido"];
                                        }
                                    } else {
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>
                    <!-- FIN INFO CORREDOR Y OPERARIO - INICIO TIPO DE PROPIEDAD Y OPERACIÓN-->
                        <tr>
                            <th colspan="5">Tipo de propiedad y operación</th>
                        </tr>
                        <tr>
                            <td>Tipo de propiedad</td>
                            <td colspan="4">
                                <?php 
                                    $sqlTipo_prop = "select tipo from tipo_propiedad where id_tipo_propiedad = $tipo_prop";
                                    $result = $conn->query($sqlTipo_prop);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo $row["tipo"];
                                        }
                                    } else {
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Tipo de operación</td>
                            <td colspan="4">
                                <?php 
                                    $sqlOp = "SELECT * FROM Venta_Arriendo where id = $operacion";
                                    $result = $conn->query($sqlOp);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo $row["descripcion"];
                                        }
                                    } else {
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr style="<?php echo ($operacion == 2 || $operacion == 1)? "":"display:none"?>">
                            <td>Precio</td>
                            <td colspan="4">
                                <?php echo $divisa_monto1." ".moneda_chilena_sin_peso($monto1)?>
                            </td>
                        </tr>
                        <tr style="<?php echo ($operacion != 3)? "display:none":""?>">
                            <td>Precio Venta</td>
                            <td colspan="4">
                                <?php
                                    if($divisa_monto1 == "UF"){
                                        if($monto1 == ""){
                                            echo "S/D";
                                        }else{
                                            echo $divisa_monto1." ".moneda_chilena_sin_peso($monto1);
                                        }
                                        
                                    }else{
                                        if($monto1 == ""){
                                            echo "S/D";
                                        }else{
                                            echo $divisa_monto1." ".moneda_chilena_sin_peso($monto1);
                                        }
                                        
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr style="<?php echo ($operacion != 3)? "display:none":""?>">
                            <td>Precio Arriendo</td>
                            <td colspan="4">
                                <?php
                                    if($divisa_monto2 == "UF"){
                                        if($monto2 == ""){
                                            echo "S/D";
                                        }else{
                                            echo $divisa_monto2." ".moneda_chilena_sin_peso($monto2);
                                        }
                                        
                                    }else{
                                        if($monto2 == ""){
                                            echo "S/D";
                                        }else{
                                            echo $divisa_monto2." ".moneda_chilena_sin_peso($monto2);
                                        }
                                        
                                    }
                                ?>
                            </td>
                        </tr>           
                        <tr style="<?php echo ($operacion == 4)? "":"display:none"?>">
                            <td>Valor CLP</td>
                            <td colspan="4">
                                S/D
                            </td>
                        </tr>
                        <tr>
                            <td>Hipoteca</td>
                            <td colspan="4">
                                <?php
                                    if($hipoteca == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                             </td>
                        </tr>
                        <tr style="<?=($hipoteca == 0)?'display:none':''?>">
                            <td>Banco</td>
                            <td colspan="4">
                                <?php
                                    $sqlBanco = "SELECT * FROM Bancos where id_banco = $fk_banco";
                                    $result = $conn->query($sqlBanco);

                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            while($row = $result->fetch_assoc()) {
                                                echo $row["banco"];
                                            }
                                        } else {
                                            echo "S/D";
                                        }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Amoblado</td>
                            <td colspan="4">
                                <?php
                                    if($amoblado == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                             </td>
                        </tr>
                        <tr>
                            <td>Exclusividad</td>
                            <td colspan="4">
                                <?php
                                    if($exclusividad==0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr style="<?php echo ($exclusividad == 0)? 'display:none' : ''?>">
                            <td>Cantidad de corredores</td>
                            <td xolspan="4">
                                <?=$cantCorredor?>
                            </td>
                        </tr>
                        <tr style="<?php echo ($exclusividad == 0)? 'display:none' : ''?>">
                            <td>Tiempo de publicación</td>
                            <td colspan="4">
                                <?php
                                    $sqlPublicacion = "SELECT * FROM Tiempo_publicacion where id_tiempo_publicacion = $fk_tiempo_publicacion";
                                    $result = $conn->query($sqlPublicacion);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo $row["tiempo"];
                                        }
                                    } else {
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>
                    <!-- FIN TIPO DE PROPIEDAD Y OPERACIÓN - DIRECCIÓN-->
                        <!-- PHP QUE TRAE DATOS DE LA DIRECCION -->
                        <?php 
                            $sqlDirec = "select * from Direccion, Comuna where Direccion.id = $direccion and Direccion.fk_comuna = Comuna.id";
                            $result = $conn->query($sqlDirec);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    $calle = $row["calle"];
                                    $numero = $row["numero"];
                                    $comuna = $row["nombre"];
                                    $letra = $row["letra"];
                                }
                            } else {
                            }
                        ?>

                        <tr>
                            <th colspan="5">Dirección</th>
                        </tr>
                        <tr>
                            <td>Calle</td>
                            <td colspan="4">
                                <?=utf8_encode($calle)?>
                            </td>
                        </tr>
                        <tr>
                            <td>Número</td>
                            <td colspan="4">
                                <?=$numero?>
                            </td>
                        </tr>
                        <tr>
                            <td>Letra / N° Dpto	</td>
                            <td colspan="4">
                                <?=utf8_encode($letra)?>
                            </td>
                        </tr>
                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?"":"display: none"?>">
                            <td>Piso</td>
                            <td colspan="4">
                                <?php echo $piso_depto?>
                            </td>
                        </tr>
                        <tr>
                            <td>Comuna</td>
                            <td colspan="4">
                                <?=utf8_encode($comuna)?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?"":"display: none"?>">
                            <td>Condominio</td>
                            <td colspan="4">
                                <?php
                                    if($condominio == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?"":"display: none"?>">
                            <td>Loteo</td>
                            <td colspan="4">
                                <?php
                                    if($loteo == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr style="<?php echo ($condominio == 0 && $loteo == 0)? 'display:none' : ''?>" >
                            <td>Sub Dirección</td>
                            <td colspan="4">
                                <?php
                                    $sql ="SELECT * FROM Direccion_condominio WHERE fk_formulario =".$id;
                                    $result = $conn->query($sql);

                                    if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        $calle_cond = utf8_encode($row["calle"]);
                                        $numero_cond = $row["numero"];
                                        if($calle_cond == ""){
                                            echo "S/D";
                                        }else{
                                            echo $calle_cond. " ".$numero_cond;
                                        }
                                    }
                                    } else {
                                    echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?"":"display: none"?>">
                            <td>Pareada</td>
                            <td colspan="4">
                                <?php
                                    if($pareada == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Referencia</td>
                            <td colspan="4">
                                <?php
                                    echo utf8_encode($ref);
                                ?>
                            </td>
                        </tr>
                    <!-- FIN DIRECCIÓN - INICIOS PISOS Y ASCENSORES -->
                        <tr>
                            <th colspan="5">Pisos y ascensores</th>
                        </tr>
                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)? '':'display:none'?>">
                            <td>Cantidad de pisos del edificio	</td>
                            <td colspan="4">
                                <?php echo $cant_pisos?>
                            </td>
                        </tr>
                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)? '':'display:none'?>">
                            <td>Cantidad de ascensores	</td>
                            <td colspan="4">
                                <?php echo $cant_ascensores?>
                            </td>
                        </tr>
                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)? '':'display:none'?>">
                            <td>Cantidad de Dptos. por piso	</td>
                            <td colspan="4">
                                <?php echo $dptoPiso?>
                            </td>
                        </tr>
                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)? '':'display:none'?>">
                            <td>Cantidad de pisos del Dpto.	</td>
                            <td colspan="4">
                                <?php echo $cant_deptos?>
                            </td>
                        </tr>
                    <!-- FIN PISOS Y ASCENSORES - INICIO ROL Y PRECIOS ANEXOS-->
                        <tr style="<?=($operacion == 2)? 'display:none':''?>">
                            <th colspan="5">ROL Y PRECIOS ANEXOS</th>
                        </tr>
                        <tr style="<?=($operacion == 2)? 'display:none':''?>">
                            <td>Rol</td>
                            <td colspan="4">
                                <?php
                                    if($rolPropNuevo == "-"){
                                        echo "S/D";
                                    }else{
                                        echo $rolPropNuevo;
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr style="<?=($operacion == 2)? 'display:none':''?>">
                            <td>Año de construcción</td>
                            <td colspan="4">
                                <?php
                                    if($anno_construccion == ""){
                                        echo "S/D";
                                    }else{
                                    echo $anno_construccion;  
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr style="<?=($operacion == 2)? 'display:none':''?>">
                            <td>Año de adquisición</td>
                            <td colspan="4">
                                <?php
                                    if($adquisicion == ""){
                                        echo "S/D";
                                    }else{
                                        echo $adquisicion;
                                    }
                                    
                                ?>
                            </td>
                        </tr>
                        <tr style="<?=($operacion == 2)? 'display:none':''?>">
                            <td>Contribución</td>
                            <td colspan="4">
                                <?php
                                    if($adquisicion == ""){
                                        echo "S/D";
                                    }else{
                                        echo "$".$contribucion;
                                    }
                                    
                                ?>
                            </td>
                        </tr>
                    <!-- FIN ROL Y PRECIOS ANEXOS - INICIO GASTO COMÚN Y OTROS-->
                        <tr>
                            <th colspan="5"><?=($tipo_prop == 1 || $tipo_prop == 13 || ($tipo_prop==2 && $condominio == 1))? 'Gastos comunes y ': ''?>otros</th>
                        </tr>
                        
                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13 || ($tipo_prop==2 && $condominio == 1))? '': 'display:none'?>">
                            <td>Gasto común</td>
                            <td colspan="4">
                                <?php
                                    if($gasto_comun == ""){
                                        echo "S/D";
                                    }else{
                                        echo "$".$gasto_comun;
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13 || ($tipo_prop==2 && $condominio == 1))? '': 'display:none'?>">
                            <td>¿Qué incluyen?</td>
                            <td colspan="4">
                                <textarea name="" id="" cols="" rows="" disabled><?php echo utf8_encode(ucfirst($nota_gastos));?></textarea>
                            </td>
                        </tr>

                        <tr>
                            <td>Tipo de calefacción</td>
                            <td colspan="4">
                                <?php 
                                    $sqlCalefa = "SELECT * from Tipo_calefaccion where id = $fk_tipo_calefaccion";
                                    $result = $conn->query($sqlCalefa);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo $row["tipo"];
                                        }
                                    } else {
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Tipo de gas</td>
                            <td colspan="4">
                                <?php 
                                    $sqlGas = "SELECT * FROM Tipo_gas WHERE id_tipo_gas = $fk_tipo_gas";
                                    $result = $conn->query($sqlGas);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo $row["tipo"];
                                        }
                                    } else {
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Agua caliente</td>
                            <td colspan="4">
                                <?php 
                                    $sqlAguaCaliente = "SELECT * FROM Agua_caliente WHERE id_agua_caliente = $agua_caliente";
                                    $result = $conn->query($sqlAguaCaliente);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo utf8_encode($row["tipo"]);
                                        }
                                    } else {
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($operacion == 2)? '':'display:none'?>">
                            <td>Año de Construcción</td>
                            <td colspan="4">
                                <?php echo $anno_construccion?>
                            </td>
                        </tr>
                    <!-- FIN GASTO COMÚN Y OTROS - INICIO SUPERFICIE Y ORIENTACIÓN -->
                        <tr>
                            <th colspan="5">Superficie y orientación</th>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?"":"display:none"?>">
                            <td>m<sup>2</sup> Total</td>
                            <td colspan="4">
                                <?php 
                                    if($sup_total != ""){
                                    echo $sup_total . "". " m<sup>2</sup>";
                                    }else{
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>m<sup>2</sup> Útil</td>
                            <td colspan="4">
                                <?php 
                                    if($sup_util != ""){
                                        echo $sup_util . " " . "m<sup>2<sup>";
                                    }else{
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?"":"display:none"?>">
                            <td>m<sup>2</sup> Terraza</td>
                            <td colspan="4">
                                <?php 
                                    if($sup_terraza != ""){
                                        echo $sup_terraza . " " . "m<sup>2<sup>";
                                    }else{
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?"display:none":""?>">
                            <td>m<sup>2</sup> Terreno</td>
                            <td colspan="4">
                                <?php
                                    if($sup_terreno != ""){
                                        echo $sup_terreno . " " . "m<sup>2<sup>";
                                    }else{
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>

                        <!-- NUEVOS CAMPOS (2) -->
                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 12 || $tipo_prop == 14 || $tipo_prop == 15)?"":"display: none"?>">
                            <td>m<sup>2</sup> Patio</td>
                            <td colspan="4">
                                <?php
                                    if($sup_patio != ""){
                                        echo $sup_patio . " " . "m<sup>2<sup>";
                                    }else{
                                        echo "S/D";
                                    }
                                ?>
                            </td>
                        </tr>

                        <?php
                            $sqlOrientacion = "SELECT * from Orientacion WHERE fk_formulario = $id"; 
                            $result = $conn->query($sqlOrientacion);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    $norte = $row["norte"];
                                    $sur =$row["sur"] ;
                                    $oriente =$row["oriente"];
                                    $poniente = $row["poniente"];
                                    $norOriente = $row["norOriente"];
                                    $norPoniente = $row["norPoniente"];
                                    $surOriente = $row["surOriente"];
                                    $surPoniente = $row["surPoniente"];
                                }
                            }
                        ?>
                        <tr class="tr-center">
                            <td style="text-align:justify;" rowspan="4">Orientación</td>
                            <td>N</td>
                            <td>S</td>
                            <td>O</td>
                            <td>P</td>
                        </tr>
                        <tr class="tr-center">
                            <td><?php echo ($norte == 1) ? '<i class="fas fa-check"></i>' : "" ?></td>
                            <td><?php echo ($sur == 1) ? '<i class="fas fa-check"></i>' : "" ?></td>
                            <td><?php echo ($oriente == 1) ? '<i class="fas fa-check"></i>' : "" ?></td>
                            <td><?php echo ($poniente == 1) ? '<i class="fas fa-check"></i>' : "" ?></td>
                        </tr>
                        
                        <tr class="tr-center">
                            <td>NO</td>
                            <td>NP</td>
                            <td>SO</td>
                            <td>SP</td>
                        </tr>
                        <tr class="tr-center">
                            <td><?php echo ($norOriente == 1) ? '<i class="fas fa-check"></i>' : "" ?></td>
                            <td><?php echo ($norPoniente == 1) ? '<i class="fas fa-check"></i>' : "" ?></td>
                            <td><?php echo ($surOriente == 1) ? '<i class="fas fa-check"></i>' : "" ?></td>
                            <td><?php echo ($surPoniente == 1) ? '<i class="fas fa-check"></i>' : "" ?></td>
                        </tr>
                    <!-- FIN SUPERFICIE Y ORIENTACIÓN - INICIO DORMITORIOS -->
                            <tr>
                                <th colspan="5">Dormitorios</th>
                            </tr>

                            <tr>
                                <td rowspan="<?=(4*$dormitorios) + 1?>">Dormitorios</td>
                                <td colspan="5" class="center" >Cant. de Dormitorios: <?=$dormitorios?></td>
                            </tr>
                            <?php
                                $sqlDorm = "SELECT * FROM Dormitorios where fk_formulario = $id";
                                $result = $conn->query($sqlDorm);
                                $suite = array();
                                $walking = array();
                                $closet = array();
                                $servicio = array();
                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        array_push($suite,$row["suit"]);
                                        array_push($walking,$row["walking"]);
                                        array_push($closet,$row["closet"]);
                                        array_push($servicio,$row["servicio"]);
                                    }
                                }
                                echo "<script>console.log(".$suite.")</script>";

                                for($i = 0; $i < $dormitorios; $i++){
                                    echo '
                                <tr>
                                    <td class="center td-vcenter" rowspan="4">'.($i+1).'</td>
                                    <td colspan="2">Suite</td>
                                    <td colspan="1">';
                                    echo ($suite[$i] == 1)? '<i class="fas fa-check"></i>': '';
                                    echo '</td>
                                </tr>
                                <tr>
                                    <td colspan="2">WalkingC</td>
                                    <td colspan="1">';
                                    echo ($walking[$i] == 1)? '<i class="fas fa-check"></i>':''; 
                                    echo '</td>
                                </tr>
                                <tr>
                                    <td colspan="2">ClosetN</td>
                                    <td colspan="1">';
                                    echo ($closet[$i] == 1)?'<i class="fas fa-check"></i>':'';
                                    echo '</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Servicio</td>
                                    <td colspan="1">';
                                    echo ($servicio[$i] == 1)? '<i class="fas fa-check"></i>':'';
                                    echo '</td>
                                </tr>';
                                }
                            ?>
                    <!-- FIN DORMITORIOS - INICIO BAÑOS -->
                        <tr>
                            <th colspan="5">Baños</th>
                        </tr>

                        <tr>
                            <td rowspan="3">Baños</td>
                            <td class="center" colspan="5">Cantidad Baños: <?php echo $bannos?></td>
                        </tr>

                        <tr>
                            <td>Visita</td>
                            <td>Medio Baño</td>
                            <td>Completo</td>
                            <td>Servicio</td>
                        </tr>

                        <?php
                            $sqlBannos = "SELECT * FROM Bannos WHERE fk_formulario = $id";
                            $tipo_bannos = array();
                            $result = $conn->query($sqlBannos);
                            $visita = 0;
                            $medio = 0;
                            $completo = 0;
                            $servicio_bano = 0;
                            
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    if($row["fk_tipo_banno"] == 1){
                                        $visita++;
                                    }else if($row["fk_tipo_banno"] == 2){
                                        $medio ++;
                                    }else if($row["fk_tipo_banno"] == 3){
                                        $completo++;
                                    }else if($row["fk_tipo_banno"] == 4){
                                        $servicio_bano++;
                                    }
                                }
                            }
                        ?>

                        <tr>
                            <td><?php echo $visita?></td>
                            <td><?php echo $medio?></td>
                            <td><?php echo $completo?></td>
                            <td><?php echo $servicio_bano?></td>
                        </tr>
                    <!-- FIN BAÑOS - INICIO ESTACIONAMIENTO -->
                        <tr>
                            <th colspan="5">Estacioamientos</th>
                        </tr>

                        <tr>
                            <td rowspan="<?php echo ($estacionamiento*5)+1?>">Estacionamientos</td>
                            <td class="center" colspan="4" >Cantidad de estacionamientos: <?php echo ($estacionamiento == "")?'S/D':$estacionamiento?></td>
                        </tr>
                        <?php
                            $nivel = array();
                            $numero = array();
                            $techado = array();
                            $rol = array();
                            $garage = array();
                            $otro_nivel_est = array();
                            $sqlEstacionamientos = "SELECT * FROM Estacionamientos WHERE fk_formulario = $id";
                            $result = $conn->query($sqlEstacionamientos);

                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                array_push($nivel,$row["nivel"]);  
                                array_push($numero,$row["numero"]);  
                                array_push($techado,$row["techado"]);  
                                array_push($rol,$row["rol"]);
                                array_push($garage,$row["garage_cerrado"]);
                                array_push($otro_nivel_est,$row["otro_nivel"]);
                                }
                            }else{
                                // echo "<script>console.log('$sqlEstacionamientos')</script>";
                            }
                            for($i=0;$i<$estacionamiento;$i++){
                                if($nivel[$i] == ""){
                                    $nivel[$i] = "S/D";
                                }
                                if($numero[$i] == ""){
                                    $numero[$i] = "S/D";
                                }
                            
                                echo '
                                    <tr>
                                        <td rowspan="5">'.($i+1).'</td>
                                        <td>Nivel</td>
                                        <td colspan="2">'.(($nivel[$i]=="otro")?$otro_nivel_est[$i]:$nivel[$i]).' </td>
                                    </tr>
                                    <tr>
                                        <td>N°</td>
                                        <td colspan="2">'.(strtoupper($numero[$i])).'</td>
                                    </tr>
                                    <tr>
                                        <td>Techado</td>
                                        <td colspan="2">'.(($techado[$i] == 0)? 'No' : 'Si').'</td>
                                    </tr>
                                    <tr>
                                        <td>Garage Cerrado</td>
                                        <td colspan="2">'.(($garage[$i] == 0)? 'No' : 'Si').'</td>
                                    </tr>
                                    <tr>
                                        <td>Rol</td>
                                        <td colspan="2">'.(($rol[$i]=="-")?'S/D': $rol[$i]).'</td>
                                    </tr>';
                            }
                        ?>
                    <!-- FIN ESTACIONAMIENTO - INICIO BODEGA -->
                        <tr>
                            <th colspan="5">BODEGAS</th>
                        </tr>

                        <tr>
                            <td rowspan="<?php echo ($bodega*3)+1?>">Bodegas</td>
                            <td class="center" colspan="4">Cantidad de bodegas: <?php echo $bodega?></td>
                        </tr>

                        <?php
                            $sqlBodegas = "SELECT * FROM Bodegas WHERE fk_formulario = $id";
                            $result = $conn->query($sqlBodegas);
                            $num = array();
                            $nivel = array();
                            $rol = array();
                            $otro_nivel_bod = array();
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                array_push($num,$row["num"]);
                                array_push($nivel,$row["nivel"]);
                                array_push($rol,$row["rol"]);
                                array_push($otro_nivel_bod,$row["otro_bodega"]);
                                }
                            }else{
                                echo "<script>console.log('$sqlEstacionamientos')</script>";
                            }
                            for($i=0;$i<$bodega;$i++){
                                echo '
                                    <tr>
                                        <td rowspan="3">'.($i+1).'</td>
                                        <td>N°</td>
                                        <td colspan="2">'.(($num[$i] == "")?'S/D':$num[$i]).'</td>
                                    </tr>
                                    <tr>
                                        <td>Nivel</td>
                                        <td colspan="2">';
                                        if($nivel[$i]=="9"){
                                            echo $otro_nivel_bod[$i];
                                        }else{
                                            if($nivel[$i]==""){
                                                echo 'S/D';
                                            }else{
                                            echo $nivel[$i];
                                            }
                                        }
                                    echo '</td>
                                    </tr>
                                    <tr>
                                        <td>Rol</td>
                                        <td colspan="2">'.(($rol[$i] == "-")?'S/D':$rol[$i]).'</td>
                                    </tr>';
                            }
                        ?>
                    <!-- FIN BODEGA - INICIO MATERIAL PISOS-->
                        <tr>
                            <th colspan="5">Material pisos</th>
                        </tr>

                        <tr>
                            <td>Material piso Espacios comunes</td>
                            <td colspan="4">
                                <?php
                                    $sqlMaterialPisos = "SELECT * FROM Materiales_pisos WHERE id_materiales_pisos = $material_comun";
                                    $result = $conn->query($sqlMaterialPisos);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            $material_comun = $row["material"];
                                        }
                                    }else{
                                        $material_comun = "S/D";
                                    }
                                    echo utf8_encode($material_comun);
                                ?>
                            </td>
                        </tr>
                    
                        <tr>
                            <td>Material piso dormitorios</td>
                            <td colspan="4">
                                <?php
                                    $sqlMaterialPisos = "SELECT * FROM Materiales_pisos WHERE id_materiales_pisos = $material_dorm";
                                    $result = $conn->query($sqlMaterialPisos);
                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            while($row = $result->fetch_assoc()) {
                                                $material_dorm = $row["material"];
                                            }
                                        }else{
                                            echo "S/D";
                                        }
                                        echo utf8_encode($material_dorm)
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Material piso baños</td>
                            <td colspan="4">
                                <?php
                                    $sqlMaterialPisosBano = "SELECT * FROM Material_piso_banno WHERE id_material_piso_banno = $material_banno";
                                    $result = $conn->query($sqlMaterialPisosBano);
                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            while($row = $result->fetch_assoc()) {
                                                $material_banno2 = $row["material"];
                                            }
                                        }else{
                                            echo "S/D";
                                        }
                                        echo utf8_encode($material_banno2)
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Material piso cocina</td>
                            <td colspan="4">
                                <?php
                                    $sqlMaterialPisos = "SELECT * FROM Materiales_pisos WHERE id_materiales_pisos = $material_pisos_cocina";
                                    $result = $conn->query($sqlMaterialPisos);
                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            while($row = $result->fetch_assoc()) {
                                                $material_pisos_cocina = $row["material"];
                                            }
                                        }else{
                                            echo "S/D";
                                        }
                                        echo utf8_encode($material_pisos_cocina)
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Material piso terraza</td>
                            <td colspan="4">
                                <?php
                                    $sqlMaterialPisos = "SELECT * FROM Materiales_pisos WHERE id_materiales_pisos = $material_pisos_terraza";
                                    $result = $conn->query($sqlMaterialPisos);

                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            while($row = $result->fetch_assoc()) {
                                                $material_pisos_terraza = $row["material"];
                                            }
                                        }else{
                                            echo "S/D";
                                        }
                                        echo utf8_encode($material_pisos_terraza)
                                ?>
                            </td>
                        </tr>
                    <!-- FIN MATERIAL PISOS - INICIO COCINA-->
                        <tr>
                            <th colspan="5">Cocina</th>
                        </tr>

                        <tr>
                            <td>Cocina independiente</td>
                            <td colspan="4">
                                <?php
                                    if($cocinaIndep == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                }?>
                            </td>
                        </tr>

                        <tr>
                            <td>Cocina integrada</td>
                            <td colspan="4">
                                <?php
                                    if($cocinaIntegrada == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Cocina americana</td>
                            <td colspan="4">
                                <?php
                                    if($cocina_ame == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Cocina isla</td>
                            <td colspan="4">
                                <?php
                                    if($cocina_isla == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Encimera electrica</td>
                            <td colspan="4">
                                <?php
                                    if($encimeraElect == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Encimera gas</td>
                            <td colspan="4">
                                <?php
                                    if($encimeraGas == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Horno Empotrado</td>
                            <td colspan="4">
                                <?php
                                    if($horno_emp == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Lavavajillas empotrado</td>
                            <td colspan="4">
                                <?php
                                    if($lavavajilla_emp == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Microondas empotrado</td>
                            <td colspan="4">
                                <?php
                                    if($microondas == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Refrigerador empotrado</td>
                            <td colspan="4">
                                <?php
                                    if($refrigerador == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>Comedor de Diario</td>
                            <td colspan="4">
                                <?php
                                    if($comedor_diario == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'display: none':''?>">
                            <td>Patio de Servicio</td>
                            <td colspan="4">
                                <?php
                                    if($patio_serv == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Cuarto Despensa</td>
                            <td colspan="4">
                                <?php
                                    if($despensa == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Logia</td>
                            <td colspan="4">
                                <?php
                                    if($logia == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                    <!-- FIN COCINA - INICIO OTROS PROPIEDADES -->
                        <tr>
                            <th class="thFichaForm tdTitulo" colspan="5">Otros propiedades</th>
                        </tr>

                        <tr>
                            <td>Cortina Roller</td>
                            <td colspan="4">
                                    <?php
                                    if($cortina_roller == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                    
                                    ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Cortinas Eléctricas</td>
                            <td colspan="4">
                                <?php
                                    if($cortina_elec == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Cortinas Hangaroa</td>
                            <td colspan="4">
                                <?php
                                    if($cortina_hanga == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Persiana Aluminio</td>
                            <td colspan="4">
                                <?php
                                    if($persiana_aluminio == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Malla Protección</td>
                            <td colspan="4">
                                <?php
                                    if($malla_proc_terraza == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>Aire acondicionado</td>
                            <td colspan="4">
                                <?php
                                    if($aire == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Persiana Madera</td>
                            <td colspan="4">
                                <?php
                                    if($persiana_madera == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Citófono</td>
                            <td colspan="4">
                                <?php
                                    if($citofono == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Sistema de alarma</td>
                            <td colspan="4">
                                <?php
                                    if($alarma == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Termo Panel</td>
                            <td colspan="4">
                                <?php
                                    if($termo_panel == 0){  
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Escritorio</td>
                            <td colspan="4">
                                <?php
                                    if($escritorio == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Sala de Estar</td>
                            <td colspan="4">
                                <?php
                                    if($sala_estar == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Cuarto Despensa</td>
                            <td colspan="4">
                                <?php
                                    if($despensa == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Sala de Juegos</td>
                            <td colspan="4">
                                <?php
                                    if($sala_juegos == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Sala de planchado</td>
                            <td colspan="4">
                                <?php
                                    if($pieza_planchado == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Terraza con Quincho</td>
                            <td colspan="4">
                                <?php
                                    if($terraza_quincho == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Bar</td>
                            <td colspan="4">
                                <?php
                                    if($bar == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Cava de Vinos</td>
                            <td colspan="4">
                                <?php
                                    if($cava_vinos == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Sauna</td>
                            <td colspan="4">
                                <?php
                                    if($sauna == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Jacuzzi</td>
                            <td colspan="4">
                                <?php
                                    if($jacuzzi == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Ascensor privado</td>
                            <td colspan="4">
                                <?php
                                    if($ascensor_privado == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Dúplex</td>
                            <td colspan="4">
                                <?php
                                    if($duplex == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Tríplex</td>
                            <td colspan="4">
                                <?php
                                    if($triplex == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Mariposa</td>
                            <td colspan="4">
                                <?php
                                    if($mariposa == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Penthouse</td>
                            <td colspan="4">
                                <?php
                                    if($penthouse == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Ático</td>
                            <td colspan="4">
                                <?php
                                    if($atico == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Hall de acceso</td>
                            <td colspan="4">
                                <?php
                                    if($hall == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                    <!-- FIN OTROS PROPIEDADES- INICIO EXTERIOR -->
                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <th class="thFichaForm tdTitulo" colspan="5">Exterior</th>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Antejardín</td>
                            <td colspan="4">
                                <?php
                                    if($antejardin == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Riego Automático</td>
                            <td colspan="4">
                                <?php
                                    if($riego == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Piscina</td>
                            <td colspan="4">
                                <?php
                                    if($piscina == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Piscina Climatizada</td>
                            <td colspan="4">
                                <?php
                                    if($piscina_temp == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Quincho</td>
                            <td colspan="4">
                                <?php
                                    if($quincho == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Panel Solar</td>
                            <td colspan="4">
                                <?php
                                    if($panelSolar == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                    <!-- FIN EXTERIOR - INICIO ÁREAS COMUNES -->
                        <tr>
                            <th class="thFichaForm tdTitulo" colspan="5">ÁREAS COMUNES</th>
                        </tr>
                        
                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Piscina</td>
                            <td colspan="4">
                                <?php
                                    if($piscina == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Piscina Climatizada</td>
                            <td colspan="4">
                                <?php
                                    if($piscina_temp == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Gimnasio</td>
                            <td colspan="4">
                                <?php
                                    if($gimnasio == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Sauna</td>
                            <td colspan="4">
                                <?php
                                    if($sauna == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Quincho</td>
                            <td colspan="4">
                                <?php
                                    if($quincho == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Sala de Eventos</td>
                            <td colspan="4">
                                <?php
                                    if($sala_eventos == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Gourmet Room</td>
                            <td colspan="4">
                                <?php
                                    if($gourmet_room == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Lavanderia Edificio</td>
                            <td colspan="4">
                                <?php
                                    if($lavanderia == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Azotea Habilitada</td>
                            <td colspan="4">
                                <?php
                                    if($azotea == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Ascensor</td>
                            <td colspan="4">
                                <?php
                                    if($ascensor_comun == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 1 || $tipo_prop == 13)?'':'display: none'?>">
                            <td>Bicicletero</td>
                            <td colspan="4">
                                <?php
                                    if($bicicletero == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Conserjería 24Hrs</td>
                            <td colspan="4">
                                <?php
                                    if($conserjeria == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Accesos Controlados</td>
                            <td colspan="4">
                                <?php
                                    if($accesos_controlados == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Circuito cerrado de TV</td>
                            <td colspan="4">
                                <?php
                                    if($circ_tv == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Estacionamiento de Visita</td>
                            <td colspan="4">
                                <?php
                                    if($est_visita == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Plaza</td>
                            <td colspan="4">
                                <?php
                                    if($plaza == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Juegos Infantiles</td>
                            <td colspan="4">
                                <?php
                                    if($juegos_inf == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Cancha de Fútbol</td>
                            <td colspan="4">
                                <?php
                                    if($cancha_futbol == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Cancha de Tenis</td>
                            <td colspan="4">
                                <?php
                                    if($cancha_tenis == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr style="<?=($tipo_prop == 2 || $tipo_prop == 14 || $tipo_prop == 15)?'':'display: none'?>">
                            <td>Cancha de Golf</td>
                            <td colspan="4">
                                <?php
                                    if($cancha_golf == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Sala de Juegos (Área Común)</td>
                            <td colspan="4">
                                <?php
                                    if($sala_juegos_ext == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Áreas verdes</td>
                            <td colspan="4">
                                <label class="fichaFormLbl" for="">
                                <?php
                                    if($area_verde == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                    ?>
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td>Sala Multiuso</td>
                            <td colspan="4">
                                <?php
                                    if($sala_multi_uso == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                    <!-- FIN ÁREAS COMUNES - INICIO VISITA Y ENTREGA-->
                        <tr>
                            <th class="thFichaForm tdTitulo" colspan="5">Visita y entrega</th>
                        </tr>
                        <tr>
                            <td>Disponibilidad visitas</td>
                            <td colspan="4">
                                <?php echo ($disponibilidad_visita == "")?'S/D':$disponibilidad_visita?>
                            </td>
                        </tr>
                        <tr>
                            <td>Disponibilidad de entrega</td>
                            <td colspan="4">
                                <?php 
                                    if($disponibilidad_entrega == "Otro"){
                                        echo date("d-m-Y", strtotime($fecha_entrega));
                                    }else if($disponibilidad_entrega == ""){
                                        echo 'S/D';
                                    }else{
                                        echo $disponibilidad_entrega;
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Nota</td>
                            <td colspan="4">
                                <textarea name="" id="" cols="" rows="" disabled><?php echo utf8_encode(ucfirst($notas))?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>Destacado</td>
                            <td colspan="4">
                                <textarea name="" id="" cols="" rows=""  disabled><?=utf8_encode(ucfirst($propDestacado))?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>URL</td>
                            <td colspan="4">
                                <?php echo $url?>
                            </td>
                        </tr>

                        <tr class="trFichaForm" style="<?=($tipo_prop == 1)?"":"display: none"?>">
                            <td>Mascotas</td>
                            <td colspan="4">
                                <?php
                                    if($mascotas == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Cartel</td>
                            <td colspan="4">
                                <?php
                                    if($cartel == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Llave</td>
                            <td colspan="4">
                                <?php
                                    if($llave == 0){
                                        echo "No";
                                    }else{
                                        echo "Si";
                                    }
                                ?>
                            </td>
                        </tr>
                    <!-- FIN ÁREAS COMUNES -->
                    </table>
                <!-- FIN TABLA -->
                <!-- INICIO BOTON -->
                    
                <!-- FIN BOTON -->
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script>
        function aprobarPorForm(id){
            fetch('../controlador/aprobarForm.php?idForm='+id)
            .then(res=> res.json())
            .then(data=> {
                // console.log(data);
                window.close();
                window.opener.location.reload();
                
            });
        }

        function rechazar(id){
            fetch('../controlador/rechazarForm.php?idForm='+id)
            .then(res=> res.json())
            .then(data=>{
                // console.log(data)
                window.opener.location.reload();
                window.close(); 
                
                
            })
        }

        function enviar(){  
            
                let idForm = document.getElementById("idForm").value;
                console.log(idForm);
                fetch('../controlador/enviarNotificacion.php?idForm=' + idForm)
            .then(res2=> res2.json())
            .then(data2 => {
                if(data2 == "yes"){
                    Swal.fire({
                        title: 'Datos guardados',
                        text: "Se enviará una notificación para su aprobación",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK!'
                    }).then((result) => {
                        if (result.value) {
                            // console.log(result.value);
                            window.opener.location.reload();
                            window.close();
                        }
                    });
                }else{
                    // console.log("no pasa na hermano")
                }
            });
        }
    </script>