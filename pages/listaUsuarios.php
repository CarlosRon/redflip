<?php
if(!$_GET || $_GET["pagina"]<1){
    echo "<script>location.href='listaUsuarios.php?pagina=1';</script>";
}
include_once 'sidebarMenu.php';
include_once '../controlador/mcript.php';
include '../conexion.php';

?>
<div class="row">
    <div class="col text-center pt-5">
    <h1>Lista de usuarios</h1>

        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre de usuario</th>
            <th scope="col">Password</th>
            <th scope="col">Estado</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $iniciar = ($_GET["pagina"] - 1)*20;
        $sqlCantUsuarios = "SELECT * from usuario WHERE usuario.fk_rol NOT IN(4) order by id_usuario";
        $resultAyuda = $conn->query($sqlCantUsuarios);

            if ($resultAyuda->num_rows > 0) {
            // output data of each row
            while($row = $resultAyuda->fetch_assoc()) {
            }
        }

        $sqlUsuarios = "SELECT * from usuario WHERE usuario.fk_rol NOT IN(4) order by id_usuario LIMIT $iniciar,20 ";
        $result = $conn->query($sqlUsuarios);

            if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                
                echo '<tr>
            <th scope="row">'.$row["id_usuario"].'</th>
            <td>'.$desencriptar($row["nom_us"]).'</td>
            <td>'.$desencriptar($row["pass"]).'</td>
            <td>'.(($row["fk_estado_us"] == 1)?'Activo':'Inactivo').'</td>
            </tr>';
            }
            } else {
            echo $conn->connect_error;
            }
            $filas = $resultAyuda->num_rows;
            $paginas =ceil($filas/20);
            echo "<script>console.log('$paginas')</script>";
            if($_GET["pagina"] > $paginas){
                echo "<script>location.href='listaUsuarios.php?pagina=$paginas';</script>";
            }
            echo $salida;
            $pagina = $_GET["pagina"];
            // calculamos la primera y última página a mostrar
            $primera = $pagina - ($pagina % 10) + 1;
            // echo $primera;
            if ($primera > $pagina) { $primera = $primera - 10; }
            $ultima = $primera + 10 > $paginas ? $paginas : $primera + 10;
            // echo $ultima;
        ?>
            
        </tbody>
        </table>

        </div>
    </div>
<div class="row row-cols-3">
    <div class="col ">
            
        
    </div>
    <div class="col">
    <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?=($_GET["pagina"]<=1)? "disabled":""?>"  ><a class="page-link" href="listaUsuarios.php?pagina=<?=1?>"><<</a></li>
                    <li class="page-item <?=($_GET["pagina"]<=1)? "disabled":""?>"  ><a class="page-link" href="listaUsuarios.php?pagina=<?=$_GET["pagina"] - 1?>"><</a></li>
                    <?php for($i=$primera; $i<=$ultima; $i++):?>
                    
                    <li class="page-item"><a class="page-link" href="listaUsuarios.php?pagina=<?=$i?>"><?=$i?></a></li>
                    
                    
                
                    <?php endfor?>
                    <li class="page-item <?=($_GET["pagina"]>=$paginas)? "disabled":""?>"><a class="page-link" href="listaUsuarios.php?pagina=<?=$_GET["pagina"] + 1?>">></a></li>
                    <li class="page-item <?=($_GET["pagina"]>=$paginas)? "disabled":""?>"><a class="page-link" href="listaUsuarios.php?pagina=<?=$paginas?>">>></a></li>
                </ul>
            </nav>
    </div>
    <div class="col"></div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/dist/sweetalert2.all.min.js"></script>


<?php
include_once 'footer.php';

?>