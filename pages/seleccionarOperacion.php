<?php
include '../pages/sidebarMenu.php';
include '../conexion.php';
?>

    <section>
        <div class="container">
            <form action="" id="formC">

                <!-- progressbar -->
                <ul id="progressBar">
                    <li class="active">Seleccionar Operación</li>
                    <li>Seleccionar Propiedad</li>

                </ul>

                <!-- <div class="container">
                <div class="row">
                    <div class="column col-12"></div>
                </div>
            </div> -->
                <!-- fieldset 1 -->
                <fieldset id="1">

                    <h2 class="fs-title">Seleccionar Operación</h2>
                    <h3 class="fs-subtitle">Paso 1</h3>

                    <div class="col-12">
                        <p>Quiero...</p>
                        <div class="row radio-group">
                            <div class="col-12">
                                <label><input type="radio" name="ptradio" id="ven" value="ven"  class="with-gap" checked><span>Vender una propiedad</span></label>
                            </div>
                            <div class="col-12">
                                <label><input type="radio" name="ptradio" id="arr"value="arr"  class="with-gap"> <span>Poner en arriendo una propiedad</span> </label>
                            </div>
                            <div class="col-12">
                                <label> <input type="radio" name="ptradio" id="amb" value="amb" class="with-gap"> <span>Ambos</span> </label>
                            </div>
                        </div>
                    </div>

                    <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

                </fieldset>

                <!-- fieldset 2 -->
                <fieldset id="2">
                    <h2 class="fs-title">Seleccionar Propiedad</h2>
                    <h3 class="fs-subtitle">Paso 2</h3>

                    <div class="accordion" id="accordionExample">
                        <div class="col-12">
                            <div class="row radio-group">
                                <div class="col-6">
                                    <label><input type="radio" name="prop" value="casa" id="cas" class="with-gap" checked><span >Casa</span></label>
                                </div>
                                <div class="col-6">
                                    <label>
                                    <input type="radio" name="prop" value="depto" id="dep"  class="with-gap" >
                                    <span>Departamento</span>
                                </label>
                                </div>
                                <div class="col-6">
                                    <label>
                                    <input type="radio" name="prop" id="ofiC" class="with-gap" value="casaof">
                                    <span >Oficina Casa</span>
                                </label>
                                </div>
                                <div class="col-6">
                                    <label>
                                    <input type="radio" name="prop" id="ofiD" class="with-gap" value="deptoof">
                                    <span>Oficina Departamento</span>
                                </label>
                                </div>
                                <div class="col-6">
                                    <label><input type="radio" name="prop" id="terr" class="with-gap" value="terr"><span >Terreno</span></label>
                                </div>
                                <div class="col-6">
                                    <label><input type="radio" name="prop" id="casaterr" class="with-gap"> <span >Terreno Casa</span> </label>
                                </div>
                                <div class="col-6">
                                    <label><input type="radio" name="prop" id="loc" value="local" class="with-gap"><span >Local Comercial</span></label>
                                </div>

                            </div>
                        </div>
                    </div>

                    <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
                    <input type="button" onclick="enviarForm()" name="quit" id="quit" class="save action-button" value="Finalizar" />
                </fieldset>
            </form>
        </div>
    </section>
    <?
    include '../pages/footer.php';
    ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>
        <script src="../js/toastr.js"></script>
        <script src="../js/TestPlantilla.js"></script>

        <script>
        </script>