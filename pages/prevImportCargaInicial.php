
<!DOCTYPE html>
<html lang="es-cl">
    <head>
        <!--=== Required meta tags ===-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <!--=== Title ===-->
        <title></title>
        <!--=== Favicon Icon ===-->
        <link rel="icon" href="../favicon.ico" type="image/x-icon" />
        <!--=== Bootstrap CSS ===-->
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
        <!--=== Font Awesome ===-->
        <link rel="stylesheet" type="text/css" href="../css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!--=== Style CSS ===-->
        <link rel="stylesheet" type="text/css" href="../css/menu.css">
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <!-- <link href="http://cdn.grapecity.com/spreadjs/hosted/css/gc.spread.sheets.excel2013white.10.1.0.css" rel="stylesheet" type="text/css" />   -->
        <link href="../css/toastr.css" rel="stylesheet"/>
        
    </head>
    <body>
    <?php 
            // include 'header.php';
            include '../conexion.php';
            require '../services/Classes/PHPExcel/IOFactory.php';
            include '../controlador/mcript.php';
            include 'valid_session.php';
           
                
        ?>
        <div>
        <?php
        if(isset($_GET["msg"])){
            $msg = $_GET["msg"];
        ?>
        
            <input type="hidden"id="msg"value= <?echo $msg  ?>>
            <?php
            }
            $cont=0;
            $nombre = $_GET["nom"];
            $nombreArchivo = "../uploadFiles/" . $nombre;
            $objPHPExcel = PHPEXCEL_IOFactory::load($nombreArchivo);
            $objPHPExcel -> setActiveSheetIndex(0);
            $form2 = array();
            $formulario = array();
            

                //     echo '<div class="progress" style="height : 16px">
                //     <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
                //   </div>';
            echo "
                <table border=1 class='' style='margin-top: 50px; margin-bottom:50px!important;'>
                <thead>
                    <tr class='thead-dark'>
                    <th scope='col'>cont</th>
                    <th scope='col'>Fecha de ingreso</th>
                    <th scope='col'>Area Redflip</th>                  
                    <th scope='col'>Nombre Corredor</th>                  
                    <th scope='col'>Mail corredor</th>                 
                    <th scope='col'>Nombre Operario</th>                  
                    <th scope='col'>Nombre Completo</th>                  
                    <th scope='col'>Rut</th>                  
                    <th scope='col'>Teléfono</th>                  
                    <th scope='col'>Correo</th>                  
                    <th scope='col'>Operación</th>                  
                    <th scope='col'>Tipo propiedad</th>                  
                    <th scope='col'>Valor UF</th>                    
                    <th scope='col'>Valor CLP</th>                  
                    <th scope='col'>Calle</th>
                    <th scope='col'>Número</th>
                    <th scope='col'>Depto/letra</th>
                    <th scope='col'>Condominio</th>
                    <th scope='col'>Comuna</th>
                    <th scope='col'>Exclusividad</th>
                    <th scope='col'>Amoblado</th>
                    <th scope='col'>Año de Construcción</th>
                    <th scope='col'>Rol</th>
                    <th scope='col'>Contribuciones trimestrales</th>
                    <th scope='col'>Gastos Comunes</th>
                    <th scope='col'>¿Qué incluyen?</th>
                    <th scope='col'>Superficie Útil</th>
                    <th scope='col'>Superficie Terraza</th>
                    <th scope='col'>Superficie Total</th>
                    <th scope='col'>SuperFicie Terreno</th>
                    <th scope='col'>Orientación</th>
                    <th scope='col'>Nº Dormitorios</th>
                    <th scope='col'>Nº Baños</th>
                    <th scope='col'>Cantidad Estacionamientos</th>
                    <th scope='col'>Nº Estacionamiento(s)</th>
                    <th scope='col'>Ubicación estacionamiento</th>
                    <th scope='col'>Techado (si es nivel calle)</th>
                    <th scope='col'>Cantidad de pisos Casa/depto</th>
                    <th scope='col'>Piso Depto.</th>
                    <th scope='col'>Cantidad de deptos x piso</th>
                    <th scope='col'>Cantidad de ascensores x piso</th>
                    <th scope='col'>Fin de la propiedad</th>
                    <th scope='col'>Cantidad Bodega(s)</th>
                    <th scope='col'>Nº de la Bodega(s)</th>
                    <th scope='col'>Ubicación bodega</th>
                    <th scope='col'>Material Pisos Espacio Comunes</th>
                    <th scope='col'>Material Pisos Dormitorios</th>
                    <th scope='col'>Material Piso Baños</th>
                    <th scope='col'>Tipo de calefacción</th>
                    <th scope='col'>Agua caliente</th>
                    <th scope='col'>Casa / Living-comedor separado</th>
                    <th scope='col'>Aire Acondicionado</th>
                    <th scope='col'>Ascensor privado</th>
                    <th scope='col'>Ático</th>
                    <th scope='col'>Bodega</th>
                    <th scope='col'>Cocina Americana</th>
                    <th scope='col'>Cocina Encimera</th>
                    <th scope='col'>Cocina Isla</th>
                    <th scope='col'>Comedor de Diario</th>
                    <th scope='col'>Cortina Hangaroa</th>
                    <th scope='col'>Cortina Roller</th>
                    <th scope='col'>Cortinas Eléctricas</th>
                    <th scope='col'>Dorm. Servicio</th>
                    <th scope='col'>Escritorio</th>
                    <th scope='col'>Horno Empotrado</th>
                    <th scope='col'>Jacuzzi</th>
                    <th scope='col'>Logia</th>
                    <th scope='col'>Malla Protección Terraza</th>
                    <th scope='col'>Riego Automático</th>
                    <th scope='col'>Sala de Estar</th>
                    <th scope='col'>Sistema de Alarma</th>
                    <th scope='col'>Termo Panel</th>
                    <th scope='col'>Lavavajillas empotrado</th>
                    <th scope='col'>Microondas empotrado</th>
                    <th scope='col'>Refrigerador empotrado</th>
                    <th scope='col'>Despensa</th>
                    <th scope='col'>Hall de acceso</th>
                    <th scope='col'>Pieza de planchado</th>
                    <th scope='col'>Cocina amoblada</th>
                    <th scope='col'>Citófono</th>
                    <th scope='col'>Mansarda</th>
                    <th scope='col'>Walking closet</th>
                    <th scope='col'>Cerco eléctrico</th>
                    <th scope='col'>Ascensor Edificio</th>
                    <th scope='col'>Conserjería 24Hrs</th>
                    <th scope='col'>Estacionamiento de Visita</th>
                    <th scope='col'>Gimnasio edificio</th>
                    <th scope='col'>Juegos Infantiles</th>
                    <th scope='col'> Lavandería Edificio</th>
                    <th scope='col'>Piscina</th>
                    <th scope='col'>Portón Eléctrico</th>
                    <th scope='col'>Quincho</th>
                    <th scope='col'>Sala de Juegos</th>
                    <th scope='col'>Sala de reuniones</th>
                    <th scope='col'>Sala Multiuso</th>
                    <th scope='col'>Sauna</th>
                    <th scope='col'>Piscina temperada</th>
                    <th scope='col'>Gourmet room</th>
                    <th scope='col'>Azotea habilitada</th>
                    <th scope='col'>cancha de tenis</th>
                    <th scope='col'>Circuito cerrado de TV</th>
                    <th scope='col'>Areas verdes</th>
                    <th scope='col'>Sala de cine</th>
                    <th scope='col'>Patio de servicio</th>
                    <th scope='col'>Antejardín</th>
                    <th scope='col'>Piscina temperada</th>
                    <th scope='col'>Disponibilidad visitas</th>
                    <th scope='col'>Disponibilidad de entrega</th>
                    <th scope='col'>Notas</th>
                  
                        
                    </tr>
                </thead>
                <tbody>";
            
                $numRows = $objPHPExcel -> setActiveSheetIndex(0) -> getHighestRow();
                    //recorre el archivo .xlsx y saca los datos desde la fila numero 2 en adelante
                    for($i = 2; $i <= $numRows; $i++){
<<<<<<< HEAD
                        $fecha = $objPHPExcel-> getActiveSheet() ->getCell('B'.$i)-> getCalculatedValue();
=======
                        $fecha = $objPHPExcel-> getActiveSheet() ->getCell('K'.$i)-> getCalculatedValue();
>>>>>>> 2f0c59f8be3308efd7bea9264a73e5cbb95f943f
                        $area = $objPHPExcel-> getActiveSheet() ->getCell('C'.$i)-> getCalculatedValue();                    
                        $corredor = $objPHPExcel-> getActiveSheet() ->getCell('D'.$i)-> getCalculatedValue();                    
                        $mailCorredor = $objPHPExcel-> getActiveSheet() ->getCell('E'.$i)-> getCalculatedValue();                    
                        $operario = $objPHPExcel-> getActiveSheet() ->getCell('F'.$i)-> getCalculatedValue();                    
                        $propietario = $objPHPExcel-> getActiveSheet() ->getCell('G'.$i)-> getCalculatedValue();                    
                        $rut = $objPHPExcel-> getActiveSheet() ->getCell('H'.$i)-> getCalculatedValue();                    
                        $tel = $objPHPExcel-> getActiveSheet() ->getCell('I'.$i)-> getCalculatedValue();                    
                        $mailProp = $objPHPExcel-> getActiveSheet() ->getCell('J'.$i)-> getCalculatedValue();                    
                        $operacion = $objPHPExcel-> getActiveSheet() ->getCell('L'.$i)-> getCalculatedValue();                    
                        $tipoProp = $objPHPExcel-> getActiveSheet() ->getCell('M'.$i)-> getCalculatedValue();                    
                        $valorUF = $objPHPExcel-> getActiveSheet() ->getCell('N'.$i)-> getCalculatedValue();                    
                        $valorCLP = $objPHPExcel-> getActiveSheet() ->getCell('O'.$i)-> getCalculatedValue();
                        $calle = $objPHPExcel-> getActiveSheet() ->getCell('P'.$i)-> getCalculatedValue();
                        $num = $objPHPExcel-> getActiveSheet() ->getCell('Q'.$i)-> getCalculatedValue();
                        $letra = $objPHPExcel-> getActiveSheet() ->getCell('R'.$i)-> getCalculatedValue();
                        $condominio = $objPHPExcel-> getActiveSheet() ->getCell('S'.$i)-> getCalculatedValue();
                        $comuna = $objPHPExcel-> getActiveSheet() ->getCell('T'.$i)-> getCalculatedValue();
                        $exclusividad = $objPHPExcel-> getActiveSheet() ->getCell('BJ'.$i)-> getCalculatedValue();
                        $amoblado = $objPHPExcel-> getActiveSheet() ->getCell('BK'.$i)-> getCalculatedValue();
                        $anoConstruccion = $objPHPExcel-> getActiveSheet() ->getCell('BL'.$i)-> getCalculatedValue();
                        $rol = $objPHPExcel-> getActiveSheet() ->getCell('BM'.$i)-> getCalculatedValue();
                        $contribucion = $objPHPExcel-> getActiveSheet() ->getCell('BN'.$i)-> getCalculatedValue();
                        $gastosComunes = $objPHPExcel-> getActiveSheet() ->getCell('BO'.$i)-> getCalculatedValue();
                        $queIncluyen = $objPHPExcel-> getActiveSheet() ->getCell('BP'.$i)-> getCalculatedValue();
                        $supUtil = $objPHPExcel-> getActiveSheet() ->getCell('BQ'.$i)-> getCalculatedValue();
                        $supTerraza = $objPHPExcel-> getActiveSheet() ->getCell('BR'.$i)-> getCalculatedValue();
                        $supTotal = $objPHPExcel-> getActiveSheet() ->getCell('BS'.$i)-> getCalculatedValue();
                        $supTerreno = $objPHPExcel-> getActiveSheet() ->getCell('BT'.$i)-> getCalculatedValue();
                        $orientacion = $objPHPExcel-> getActiveSheet() ->getCell('BU'.$i)-> getCalculatedValue();
                        $dorm = $objPHPExcel-> getActiveSheet() ->getCell('BV'.$i)-> getCalculatedValue();
                        $bannos = $objPHPExcel-> getActiveSheet() ->getCell('BY'.$i)-> getCalculatedValue();
                        $estacionamiento = $objPHPExcel-> getActiveSheet() ->getCell('CH'.$i)-> getCalculatedValue();
                        $numEst = $objPHPExcel-> getActiveSheet() ->getCell('CI'.$i)-> getCalculatedValue();
                        $ubicacionEst = $objPHPExcel-> getActiveSheet() ->getCell('CJ'.$i)-> getCalculatedValue();
                        $techado = $objPHPExcel-> getActiveSheet() ->getCell('CK'.$i)-> getCalculatedValue();
                        $cantPisos = $objPHPExcel-> getActiveSheet() ->getCell('CS'.$i)-> getCalculatedValue();
                        $pisoDepto = $objPHPExcel-> getActiveSheet() ->getCell('CT'.$i)-> getCalculatedValue();
                        $cantPisosPorDepto = $objPHPExcel-> getActiveSheet() ->getCell('CU'.$i)-> getCalculatedValue();
                        $cantAscensores = $objPHPExcel-> getActiveSheet() ->getCell('CV'.$i)-> getCalculatedValue();
                        $finPropiedad = $objPHPExcel-> getActiveSheet() ->getCell('CW'.$i)-> getCalculatedValue();
                        $cantBodegas = $objPHPExcel-> getActiveSheet() ->getCell('CX'.$i)-> getCalculatedValue();
                        $numBodegas = $objPHPExcel-> getActiveSheet() ->getCell('CY'.$i)-> getCalculatedValue();
                        $ubicacionBodega = $objPHPExcel-> getActiveSheet() ->getCell('CZ'.$i)-> getCalculatedValue();
                        $MaterialEspComunes = $objPHPExcel-> getActiveSheet() ->getCell('DE'.$i)-> getCalculatedValue();
                        $MaterialpisosDorm = $objPHPExcel-> getActiveSheet() ->getCell('DF'.$i)-> getCalculatedValue();
                        $MaterialpisoBano = $objPHPExcel-> getActiveSheet() ->getCell('DG'.$i)-> getCalculatedValue();
                        $calefaccion = $objPHPExcel-> getActiveSheet() ->getCell('DH'.$i)-> getCalculatedValue();
                        $aguaCaliente = $objPHPExcel-> getActiveSheet() ->getCell('DI'.$i)-> getCalculatedValue();
                        $separada = $objPHPExcel-> getActiveSheet() ->getCell('DJ'.$i)-> getCalculatedValue();
                        $aire = $objPHPExcel-> getActiveSheet() ->getCell('DL'.$i)-> getCalculatedValue();
                        $ascensorPriv = $objPHPExcel-> getActiveSheet() ->getCell('DM'.$i)-> getCalculatedValue();
                        $atico = $objPHPExcel-> getActiveSheet() ->getCell('DN'.$i)-> getCalculatedValue();
                        $bodega = $objPHPExcel-> getActiveSheet() ->getCell('DO'.$i)-> getCalculatedValue();
                        $cocinaAme = $objPHPExcel-> getActiveSheet() ->getCell('DP'.$i)-> getCalculatedValue();
                        $cocinaEnci = $objPHPExcel-> getActiveSheet() ->getCell('DQ'.$i)-> getCalculatedValue();
                        $cocinaIsla = $objPHPExcel-> getActiveSheet() ->getCell('DR'.$i)-> getCalculatedValue();
                        $comedorDiario = $objPHPExcel-> getActiveSheet() ->getCell('DS'.$i)-> getCalculatedValue();
                        $cortinaHang = $objPHPExcel-> getActiveSheet() ->getCell('DT'.$i)-> getCalculatedValue();
                        $cortinaRoller = $objPHPExcel-> getActiveSheet() ->getCell('DU'.$i)-> getCalculatedValue();
                        $cortinaElec = $objPHPExcel-> getActiveSheet() ->getCell('DV'.$i)-> getCalculatedValue();
                        $dormServ = $objPHPExcel-> getActiveSheet() ->getCell('DW'.$i)-> getCalculatedValue();
                        $escritorio = $objPHPExcel-> getActiveSheet() ->getCell('DX'.$i)-> getCalculatedValue();
                        $hornoEmp = $objPHPExcel-> getActiveSheet() ->getCell('DY'.$i)-> getCalculatedValue();
                        $jacuzzi = $objPHPExcel-> getActiveSheet() ->getCell('DZ'.$i)-> getCalculatedValue();
                        $logia = $objPHPExcel-> getActiveSheet() ->getCell('EA'.$i)-> getCalculatedValue();
                        $mallaProcTerr = $objPHPExcel-> getActiveSheet() ->getCell('EB'.$i)-> getCalculatedValue();
                        $riego = $objPHPExcel-> getActiveSheet() ->getCell('EC'.$i)-> getCalculatedValue();
                        $salaEstar = $objPHPExcel-> getActiveSheet() ->getCell('ED'.$i)-> getCalculatedValue();
                        $alarma = $objPHPExcel-> getActiveSheet() ->getCell('EE'.$i)-> getCalculatedValue();
                        $termoPanel = $objPHPExcel-> getActiveSheet() ->getCell('EF'.$i)-> getCalculatedValue();
                        $lavavajillasEmp = $objPHPExcel-> getActiveSheet() ->getCell('EG'.$i)-> getCalculatedValue();
                        $microondasEmp = $objPHPExcel-> getActiveSheet() ->getCell('EH'.$i)-> getCalculatedValue();
                        $refriEmp = $objPHPExcel-> getActiveSheet() ->getCell('EI'.$i)-> getCalculatedValue();
                        $despensa = $objPHPExcel-> getActiveSheet() ->getCell('EJ'.$i)-> getCalculatedValue();
                        $hall = $objPHPExcel-> getActiveSheet() ->getCell('EK'.$i)-> getCalculatedValue();
                        $piezaPlan = $objPHPExcel-> getActiveSheet() ->getCell('EL'.$i)-> getCalculatedValue();
                        $cocinaAmob = $objPHPExcel-> getActiveSheet() ->getCell('EM'.$i)-> getCalculatedValue();
                        $citofono = $objPHPExcel-> getActiveSheet() ->getCell('EN'.$i)-> getCalculatedValue();
                        $mansarda = $objPHPExcel-> getActiveSheet() ->getCell('EO'.$i)-> getCalculatedValue();
                        $walking = $objPHPExcel-> getActiveSheet() ->getCell('EP'.$i)-> getCalculatedValue();
                        $cercoElec = $objPHPExcel-> getActiveSheet() ->getCell('EQ'.$i)-> getCalculatedValue();
                        $ascensorEd = $objPHPExcel-> getActiveSheet() ->getCell('ER'.$i)-> getCalculatedValue();
                        $conserjeria = $objPHPExcel-> getActiveSheet() ->getCell('ES'.$i)-> getCalculatedValue();
                        $estVisita = $objPHPExcel-> getActiveSheet() ->getCell('ET'.$i)-> getCalculatedValue();
                        $gim = $objPHPExcel-> getActiveSheet() ->getCell('EU'.$i)-> getCalculatedValue();
                        $juegosInf = $objPHPExcel-> getActiveSheet() ->getCell('EV'.$i)-> getCalculatedValue();
                        $lavanderia = $objPHPExcel-> getActiveSheet() ->getCell('EW'.$i)-> getCalculatedValue();
                        $piscina = $objPHPExcel-> getActiveSheet() ->getCell('EX'.$i)-> getCalculatedValue();
                        $portonElec = $objPHPExcel-> getActiveSheet() ->getCell('EY'.$i)-> getCalculatedValue();
                        $quincho = $objPHPExcel-> getActiveSheet() ->getCell('EZ'.$i)-> getCalculatedValue();
                        $salaJuego = $objPHPExcel-> getActiveSheet() ->getCell('FA'.$i)-> getCalculatedValue();
                        $salaReuniones = $objPHPExcel-> getActiveSheet() ->getCell('FB'.$i)-> getCalculatedValue();
                        $salaMulti = $objPHPExcel-> getActiveSheet() ->getCell('FC'.$i)-> getCalculatedValue();
                        $sauna = $objPHPExcel-> getActiveSheet() ->getCell('FD'.$i)-> getCalculatedValue();
                        $piscinaTemp = $objPHPExcel-> getActiveSheet() ->getCell('FE'.$i)-> getCalculatedValue();
                        $gourmet = $objPHPExcel-> getActiveSheet() ->getCell('FF'.$i)-> getCalculatedValue();
                        $azotea = $objPHPExcel-> getActiveSheet() ->getCell('FG'.$i)-> getCalculatedValue();
                        $tenis = $objPHPExcel-> getActiveSheet() ->getCell('FH'.$i)-> getCalculatedValue();
                        $TV = $objPHPExcel-> getActiveSheet() ->getCell('FI'.$i)-> getCalculatedValue();
                        $areasVerdes = $objPHPExcel-> getActiveSheet() ->getCell('FJ'.$i)-> getCalculatedValue();
                        $salaCine = $objPHPExcel-> getActiveSheet() ->getCell('FK'.$i)-> getCalculatedValue();
                        $patioServ = $objPHPExcel-> getActiveSheet() ->getCell('FL'.$i)-> getCalculatedValue();
                        $antejardin = $objPHPExcel-> getActiveSheet() ->getCell('FM'.$i)-> getCalculatedValue();
                        $piscinaTemp = $objPHPExcel-> getActiveSheet() ->getCell('FN'.$i)-> getCalculatedValue();
                        $dispVisitas = $objPHPExcel-> getActiveSheet() ->getCell('FO'.$i)-> getCalculatedValue();
                        $dispEntrega = $objPHPExcel-> getActiveSheet() ->getCell('FP'.$i)-> getCalculatedValue();
                        $notas = $objPHPExcel-> getActiveSheet() ->getCell('FQ'.$i)-> getCalculatedValue();
                        
                        echo "<tr>";

    echo "<td>".($cont + 1)."</td>";
    echo "<td>$fecha</td>";
    echo "<td>$area</td>";
    echo "<td>$corredor</td>";
    echo "<td>$mailCorredor</td>";
    echo "<td>$operario</td>";
    echo "<td>$propietario</td>";
    echo "<td>$rut</td>";
    echo "<td>$tel</td>";
    echo "<td>$mailProp</td>";
    echo "<td>$operacion</td>";
    echo "<td>$tipoProp</td>";
    echo "<td>$valorUF</td>";
    echo "<td>$valorCLP</td>";
    echo "<td>$calle</td>";
    echo "<td>$num</td>";
    echo "<td>$letra</td>";
    echo "<td>$condominio</td>";
    echo "<td>$comuna</td>";
    echo "<td>$exclusividad</td>";
    echo "<td>$amoblado</td>";
    echo "<td>$anoConstruccion</td>";
    echo "<td>$rol</td>";
    echo "<td>$contribucion</td>";
    echo "<td>$gastosComunes</td>";
    echo "<td>$queIncluyen</td>";
    echo "<td>$supUtil</td>";
<<<<<<< HEAD
    echo "<td>$supTerraza</td>";
=======
    echo "<td>$supTerreno</td>";
>>>>>>> 2f0c59f8be3308efd7bea9264a73e5cbb95f943f
    echo "<td>$supTotal</td>";
    echo "<td>$supTerreno</td>";
    echo "<td>$orientacion</td>";
    echo "<td>$dorm</td>";
    echo "<td>$bannos</td>";
    echo "<td>$estacionamiento</td>";
    echo "<td>$numEst</td>";
    echo "<td>$ubicacionEst</td>";
    echo "<td>$techado</td>";
    echo "<td>$cantPisos</td>";
    echo "<td>$pisoDepto</td>";
    echo "<td>$cantPisosPorDepto</td>";
    echo "<td>$cantAscensores</td>";
    echo "<td>$finPropiedad</td>";
    echo "<td>$cantBodegas</td>";
    echo "<td>$numBodegas</td>";
    echo "<td>$ubicacionBodega</td>";
    echo "<td>$MaterialEspComunes</td>";
    echo "<td>$MaterialpisosDorm</td>";
    echo "<td>$MaterialpisoBano</td>";
    echo "<td>$calefaccion</td>";
    echo "<td>$aguaCaliente</td>";
    echo "<td>$separada</td>";
    echo "<td>$aire</td>";
    echo "<td>$ascensorPriv</td>";
    echo "<td>$atico</td>";
    echo "<td>$bodega</td>";
    echo "<td>$cocinaAme</td>";
    echo "<td>$cocinaEnci</td>";
    echo "<td>$cocinaIsla</td>";
    echo "<td>$comedorDiario</td>";
    echo "<td>$cortinaHang</td>";
    echo "<td>$cortinaRoller</td>";
    echo "<td>$cortinaElec</td>";
    echo "<td>$dormServ</td>";
    echo "<td>$escritorio</td>";
    echo "<td>$hornoEmp</td>";
    echo "<td>$jacuzzi</td>";
    echo "<td>$logia</td>";
    echo "<td>$mallaProcTerr</td>";
    echo "<td>$riego</td>";
    echo "<td>$salaEstar</td>";
    echo "<td>$alarma</td>";
    echo "<td>$termoPanel</td>";
    echo "<td>$lavavajillasEmp</td>";
    echo "<td>$microondasEmp</td>";
    echo "<td>$refriEmp</td>";
    echo "<td>$despensa</td>";
    echo "<td>$hall</td>";
    echo "<td>$piezaPlan</td>";
    echo "<td>$cocinaAmob</td>";
    echo "<td>$citofono</td>";
    echo "<td>$mansarda</td>";
    echo "<td>$walking</td>";
    echo "<td>$cercoElec</td>";
    echo "<td>$ascensorEd</td>";
    echo "<td>$conserjeria</td>";
    echo "<td>$estVisita</td>";
    echo "<td>$gim</td>";
    echo "<td>$juegosInf</td>";
    echo "<td>$lavanderia</td>";
    echo "<td>$piscina</td>";
    echo "<td>$portonElec</td>";
    echo "<td>$quincho</td>";
    echo "<td>$salaJuego</td>";
    echo "<td>$salaReuniones</td>";
    echo "<td>$salaMulti</td>";
    echo "<td>$sauna</td>";
    echo "<td>$piscinaTemp</td>";
    echo "<td>$gourmet</td>";
    echo "<td>$azotea</td>";
    echo "<td>$tenis</td>";
    echo "<td>$TV</td>";
    echo "<td>$areasVerdes</td>";
    echo "<td>$salaCine</td>";
    echo "<td>$patioServ</td>";
    echo "<td>$antejardin</td>";
    echo "<td>$piscinaTemp</td>";
    echo "<td>$dispVisitas</td>";
    echo "<td>$dispEntrega</td>";
    echo "<td>$notas</td>";
    echo "</tr>";

    array_push($form2, ["fecha" => $fecha,
                         "area" => $area,
                         "nomCorredor" => $corredor,
                         "mailCorredor" => $mailCorredor,
                         "operario" => $operario,
                         "nomProp" => $propietario,
                         "rut" => $rut,
                         "tel" => $tel,
                         "mailProp" => $mailProp,
                         "operacion" => $operacion,
                         "tipoProp" => $tipoProp,
                         "valorUF" => $valorUF,
                         "valorCLP" => $valorCLP,
                         "calle" => $calle,
                         "num" => $num,
                         "letra" => $letra,
                         "condominio" => $condominio,
                         "comuna" => $comuna,
                         "exclusividad" => $exclusividad,
                         "amoblado" => $amoblado,
                         "anoConstruccion" => $anoConstruccion,
                         "rol" => $rol,
                         "contribuciones" => $contribucion,
                         "gastosComunes" => $gastosComunes,
                         "queIncluyen" => $queIncluyen,
                         "supUtil" => $supUtil,
                         "supTerraza" => $supTerraza,
                         "supTotal" => $supTotal,
                         "supTerreno" => $supTerreno,
                         "orientacion" => $orientacion,
                         "dorm" => $dorm,
                         "banos" => $bannos,
                         "estacionamientos" => $estacionamiento,
                         "numEst" => $numEst,
                         "ubicacionEst" => $ubicacionEst,
                         "techado" => $techado,
                         "cantPisos" => $cantPisos,
                         "pisoDepto" => $pisoDepto,
                         "cantDeptoPorPiso" => $cantPisosPorDepto,
                         "cantAscensores" => $cantAscensores,
                         "finProp" => $finPropiedad,
                         "cantBodegas" => $cantBodegas,
                         "numBodega" => $numBodegas,
                         "ubicacionBod" => $ubicacionBodega,
                         "materialPisosComunes" => $MaterialEspComunes,
                         "materialPisoDorm" => $MaterialpisosDorm,
                         "materialPisoBano" => $MaterialpisoBano,
                         "calefaccion" => $calefaccion,
                         "aguaCaliente" => $aguaCaliente,
                         "separada" => $separada,
                         "aire" => $aire,
                         "ascensorPriv" => $ascensorPriv,
                         "atico" => $atico,
                         "bodega" => $bodega,
                         "cocinaAme" => $cocinaAme,
                         "cocinaEnci" => $cocinaEnci,
                         "cocinaIsla" => $cocinaIsla,
                         "comedorDiario" => $comedorDiario,
                         "cortinaHang" => $cortinaHang,
                         "cortinaRoller" => $cortinaRoller,
                         "cortinaElec" => $cortinaElec,
                         "dormServ" => $dormServ,
                         "escritorio" => $escritorio,
                         "hornoEmp" => $hornoEmp,
                         "jacuzzi" => $jacuzzi,
                         "logia" => $logia,
                         "mallaProcTerr" => $mallaProcTerr,
                         "riego" => $riego,
                         "salaEstar" => $salaEstar,
                         "alarma" => $alarma,
                         "termoPanel" => $termoPanel,
                         "lavavajillas" => $lavavajillasEmp,
                         "microondas" => $microondasEmp,
                         "refrigerador" => $refriEmp,
                         "despensa" => $despensa,
                         "hall" => $hall,
                         "piezaPlan" => $piezaPlan,
                         "cocinaAmob" => $cocinaAmob,
                         "citofono" => $citofono,
                         "mansarda" => $mansarda,
                         "walkingcloset" => $walking,
                         "cercoElec" => $cercoElec,
                         "ascensorEd" => $ascensorEd,
                         "conserje" => $conserjeria,
                         "estVisita" => $estVisita,
                         "gim" => $gim,
                         "juegosInf" => $juegosInf,
                         "lavanderia" => $lavanderia,
                         "piscina" => $piscina,
                         "portonElec" => $portonElec,
                         "quincho" => $quincho,
                         "salaJuegos" => $salaJuego,
                         "salaReu" => $salaReuniones,
                         "salaMulti" => $salaMulti,
                         "sauna" => $sauna,
                         "piscinaTemp" => $piscinaTemp,
                         "gourmet" => $gourmet,
                         "azotea" => $azotea,
                         "tenis" => $tenis,
                         "TV" => $TV,
                         "areaVerde" =>$areasVerdes,
                         "salaCine" => $salaCine,
                         "patioServ" => $patioServ,
                         "antejardin" => $antejardin,
                         "piscinaTemp" => $piscinaTemp,
                         "dispVisitas" => $dispVisitas,
                         "dispEntrega" => $dispEntrega,
                         "notas" => $notas
                         ]);
    $cont++;
    
    }
    echo "</tbody>
    </table>
    ";
                
            ?>
        </div>

        <div>
            <form id="form" style="text-align:center;">
                <input type="hidden" value= '<?echo $nombre;?>' name="nom">

                <input type="hidden" value= '<?echo base64_encode(serialize($form2));?>' name="formulario">

                <button type="submit" class="btn btn-action bg-redflip-green">Aceptar</button>
                <button type="button" class="btn btn-action bg-redflip-red" onclick="cerrarPopup()">cerrar</button>
            </form>
        </div>

        <!-- JS - Prev Import -->
        <script>
            function cerrarPopup(){
                window.close();
            }

            var msg = document.getElementById("msg").value;
            console.log(msg);

            if(msg == '2'){
                alert("El archivo no corresponde con el formato");
                window.close();
            }  
        
        </script>
        <script src="../js/importarCargaInicial.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>

    </body>
</html>