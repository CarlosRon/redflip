<?php
        include "../conexion.php";
        $nombre = $_FILES['archivoDoc']['name'];
        $guardado = $_FILES['archivoDoc']['tmp_name'];
        $tamano = $_FILES['archivoDoc']['size'];
        $titulo = $_POST['tituloDoc'];
        $categoria = $_POST['categoriaDoc'];

        $extArray = explode(".", $nombre);
        $extension = end($extArray);
        
        if($guardado != ""){
            if($nombre != "" && $categoria != ""){
                if (!file_exists('../doc/'.$categoria)){
                    mkdir('../doc/'.$categoria, 0777, true);
                    if (file_exists('../doc/'.$categoria)){
                        if(!file_exists('../doc/'.$categoria.'/'.$nombre)){
                            if(move_uploaded_file($guardado, '../doc/'.$categoria.'/'.$nombre)){
                                $sql = "INSERT into documento (nombre, titulo, categoria, tamano, extension) values ('$nombre', '$titulo', '$categoria', '$tamano', '$extension')";
                                if ($conn->query($sql) === TRUE) {
                                    // echo "Ingresado a la BD <br>";
                                    $resp ->ingresoBD = "Tu archivo se ha subido correctamente.";
                                    $resp ->respuesta = "2";
                                } else {
                                    // echo "Error de BD: " . $sql . "<br>" . $conn->error . "<br>";
                                    $resp->error = $conn->error;
                                    $resp ->respuesta = "1";
                                }
                                $conn->close(); 
        
                            }else{
                                // echo "Archivo no se pudo guardar";
                                $resp->error = "El archivo no se ha podido subir.";
                                $resp ->respuesta = "1";
                            }
                        }else{
                            // echo "El Archivo ya existe";
                            $resp->error = "El archivo que intentaste subir ya existe, prueba cambiando el nombre antes de subirlo.";
                            $resp ->respuesta = "1";
                        }
                    }else{
                        $resp->error = "Error al crear directorio.";
                        $resp ->respuesta = "1";
                    }
                }else{
                    if(!file_exists('../doc/'.$categoria.'/'.$nombre)){
                        if(move_uploaded_file($guardado, '../doc/'.$categoria.'/'.$nombre)){
                            $sql = "INSERT into documento (nombre, titulo, categoria, tamano, extension) values ('$nombre', '$titulo', '$categoria', '$tamano', '$extension')";
                            if ($conn->query($sql) === TRUE) {
                                // echo "New record created successfully";
                                $resp ->ingresoBD = "Tu archivo se ha subido correctamente.";
                                $resp ->respuesta = "2";
                            } else {
                                // echo "Error: " . $sql . "<br>" . $conn->error;
                                $resp->error = $conn->error;
                                $resp->error = "Ha ocurrido un error de sistema, se ha enviado una notificación al soporte.";
                                $resp ->respuesta = "1";
                                mail('soporte@redflip.net', 'Error de Base de Datos SUBIR ARCHIVO', $sql, null);
                            }
                            $conn->close(); 
                        }else{
                            // echo "Archivo no se pudo guardar";
                            $resp->error = "El archivo no se ha podido subir.";
                            $resp ->respuesta = "1";
                        }
                    }else{
                        // echo "El Archivo ya existe";
                        $resp->error = "El archivo que intentaste subir ya existe, prueba cambiando el nombre antes de subirlo.";
                        $resp ->respuesta = "3";
                    }
                }
            }else{
                $resp->error = "Debes ingresar un nombre y una categoria para poder subir archivos.";
                $resp ->respuesta = "3";
            }
        }else{
            $resp->error = "Debes seleccionar un archivo.";
            $resp ->respuesta = "3";
        }

        
        echo json_encode($resp);
?>