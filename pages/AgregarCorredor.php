<?
    include 'sidebarMenu.php';
    include '../conexion.php';
    $sql2 = "Update usuario SET ultima_pag='http://indev9.com/redflip/pages/AgregarCorredor.php' where id_usuario =". $_SESSION['id'];
    if ($conn->query($sql2) === TRUE) {
    } else {
    }
    // include '../services/Serv_agregarProp.php';
?>

<section>
  <div class="container ">
    <div class="row">
      <div class="colum col-lg-6">
        <div class="imgFondo"></div>

      </div>
      <div class="colum col-xs-12 col-sm-12 col-md-6 col-lg-6 containerPropietario">
        <form id="formCorredor" style="margin-bottom:150px;">
            <h3 class="h3-prop mb-50">Ingresar Corredor</h3>

            <div class="form-group row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-">
                <label class="input-form" for="exampleInputEmail1">Nombre*</label>
                <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="Nombre" placeholder="Nombre" value="">
                <div id="err-nom"></div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-">
                <label class="input-form" for="exampleInputPassword1">Apellido*</label>
                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" value="">
                <div id="err-ape"></div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-">
                <label class="input-form" for="exampleInputPassword1">Teléfono</label>
                <input type="text" class="form-control" id="tel" name="tel" placeholder="+56 9 1234 5678" value="">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-">
                <label class="input-form" for="exampleInputPassword1">Correo</label>
                <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo" value="">
                </div>
            </div>

            

            <button type="submit" id="ingresar" class="mt-30 btn-def btn-login bg-redflip-red">Guardar</button>

        </form>
      </div>
    </div>
  </div>
</section>

<?php
    include 'footer.php';
?>
<script>

var formCorredor = document.getElementById("formCorredor");


formCorredor.addEventListener('submit', function(e){

    e.preventDefault();

    var datos = new FormData(formCorredor);

    fetch('../controlador/agregarCorredor.php',{

        method: 'POST',

        body: datos

    }).then(res => res.json())
    .then(datos => {
        console.log(datos)
    })
})
</script>