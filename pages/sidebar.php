<div id="menu">
    <div class="hamburger">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
    <div class="menu-inner">
        <ul>
            <a href="index.php">
                <li>Índice</li>
            </a>
            <?php if($_SESSION['rol'] == 1){?>
            <a href="AgregarPropietario.php">
                <li>Agregar Propietario</li>
            </a>
            <?php } 
				if($_SESSION['rol'] == 1){?>
            <a href="PropietariosAct.php">
                <li>Propietarios Activos</li>
            </a>
            <?php } if($_SESSION['rol'] == 1){?>
            <a href="PropietariosDes.php">
                <li>Propietarios Deshabilitados</li>
            </a>
            <?php } if($_SESSION['rol'] == 1){?>
            <a href="AgregarPropiedades.php">
                <li>Agregar Propietario - Propiedad</li>
            </a>
            <?php } if($_SESSION['rol'] == 1){?>
            <a href="propVSpropiedad.php">
                <li>Propiedades Activas</li>
            </a>
            <?php } if($_SESSION['rol'] == 1){?>
            <a href="desPropVSpropiedad.php">
                <li>Propiedades Deshabilitadas</li>
            </a>
            <?php }?>
            <a href="../controlador/CrearFormularioVacio.php">
                <li>Nuevo Formulario</li>
            </a>
            <a href="../pages/misFormularios.php">
                <li>Mis Formularios</li>
            </a>
        </ul>
    </div>

    <svg version="1.1" id="blob" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<path id="blob-path" d="M60,500H0V0h40c0,0,20,172,20,250S60,900,60,500z"/>
		</svg>
</div>