<?php
include "sidebarMenu.php";
include "../conexion.php";
$corredor;
$sqlCorredor = "SELECT * from Corredor where fk_persona=" . $_SESSION["idPer"];
$result = $conn->query($sqlCorredor);

if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
      $corredor = $row;
  }
}
$id=$_GET["id"];
$sqlPerf = "SELECT * from Perfil_busqueda where fk_interesado = $id";
$result = $conn->query($sqlPerf);

if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    $dormDesde = $row["dormDesde"];
            $dormHasta = $row["dormHasta"];
            $Edorm = $row["Edorm"];
            $bannosDesde = $row["bannosDesde"];
            $bannosHasta = $row["bannosHasta"];
            $Ebanno = $row["Ebanno"];
            $m_utilesDesde = $row["m_utilesDesde"];
            $m_utilesHasta = $row["m_utilesHasta"];
            $EmUtiles = $row["EmUtiles"];
            $precio_min = $row["precio_min"];
            // $precio_min = str_replace('.', '', $precio_min);
            $precio_max = $row["precio_max"];
            // $precio_max = str_replace('.', '', $precio_max);
            $comuna = $row["comuna"];
            $orientacion = $row["orientacion"];
            $fk_tipo_propiedad = $row["fk_tipo_propiedad"];
            $m_terrazaDesde = $row["m_terrazaDesde"];
            $m_terrazaHasta = $row["m_terrazaHasta"];
            $Eterraza = $row["Eterraza"];
            $m_terrenoDesde = $row["m_terrenoDesde"];
            $m_terrenoHasta = $row["m_terrenoHasta"];
            $Eterreno = $row["Eterreno"];
            $bodega = $row["bodega"];
            $Ebod = $row["Ebod"];
            $estacionamientoDesde = $row["estacionamientoDesde"];
            $estacionamientoHasta = $row["estacionamientoHasta"];
            $Eestacionamiento = $row["Eestacionamiento"];
            $piso_depto = $row["piso_depto"];
            $piscina = $row["piscina"];
            $Episcina = $row["Episcina"];
            $cocina_americana = $row["cocina_americana"];
            $EcocinaAme = $row["EcocinaAme"];
            $cocina_isla = $row["cocina_isla"];
            $EcocinaIsla = $row["EcocinaIsla"];
            $comedor_diario = $row["comedor_diario"];
            $EcomedorDiario = $row["EcomedorDiario"];
            $termopanel = $row["termopanel"];
            $Etermopanel = $row["Etermopanel"];
            $amoblado = $row["amoblado"];
            $Eamoblado = $row["Eamoblado"];
            $mascotas = $row["mascotas"];
            $Emascotas = $row["Emascotas"];
            $gimnasio = $row["gimnasio"];
            $Egim = $row["Egim"];
            $dorm_servicio = $row["dorm_servicio"];
            $Edorm_servicio = $row["Edorm_servicio"];
            $quincho = $row["quincho"];
            $Equincho = $row["Equincho"];
            $tipo_operacion = $row["fk_tipo_operacion"];
            $fk_interesado = $row["fk_interesado"];
  }
} else {
    echo "No hay perfil";
}

$contErrores = 0;
$sqlMatch = "SELECT distinct  Perfil_busqueda.fk_interesado, formulario.id_formulario, formulario.dormitorios AS form_dorm,
 formulario.cant_bannos, formulario.superficie_util, monto1, monto2, formulario.superficie_terraza, superficie_terreno, formulario.bodega, estacionamiento, formulario.piso_depto, formulario.piscina FROM
Perfil_busqueda , formulario,Direccion 
WHERE
formulario.Direccion_id = Direccion.id AND 
";
$comunas = explode("-", $comuna);
if($fk_tipo_propiedad != ""){
    $sqlMatch .= " (formulario.fk_tipo_propiedad LIKE '$fk_tipo_propiedad') AND";
}else{
    $contErrores++;
}

if($tipo_operacion != ""){
    $sqlMatch.=" (formulario.operacion LIKE '$tipo_operacion') AND";
}else{
    $contErrores++;
}
// test
$sqlMatch.= "(";
for($i = 0; $i<count($comunas); $i++){
    if($i == 0){
        $sqlMatch .= " Direccion.fk_comuna LIKE $comunas[$i] ";
    }else{
        $sqlMatch .= "OR Direccion.fk_comuna LIKE $comunas[$i] ";
    }

}
$sqlMatch .=") AND ";
// test
if($dormDesde != "" && $dormHasta != ""){
$sqlMatch .=" (formulario.dormitorios BETWEEN '$dormDesde' AND '$dormHasta') AND";
}else{
    $contErrores++;
}
if($bannosDesde != "" && $bannosHasta != ""){
$sqlMatch .= "(formulario.cant_bannos BETWEEN '$bannosDesde' AND '$bannosHasta') AND";
}else{
    $contErrores++;
}

if($m_utilesDesde != "" && $m_utilesHasta != ""){
$sqlMatch .= "(formulario.superficie_util BETWEEN $m_utilesDesde AND $m_utilesHasta) AND";
}else{
    $contErrores++;
}

if($precio_min != "" && $precio_max != ""){
$sqlMatch .= "(formulario.monto1 BETWEEN $precio_min AND $precio_max OR
formulario.monto2 BETWEEN $precio_min AND $precio_max) AND
";
}else{
    $contErrores++;
}

if($m_terrazaDesde != "" && $m_terrazaHasta != ""){
$sqlMatch .= "(formulario.superficie_terraza BETWEEN '$m_terrazaDesde' AND '$m_terrazaHasta') AND";
}else{
    $contErrores++;
}

if($m_terrenoDesde != "" && $m_terrenoHasta != ""){
$sqlMatch .= "(formulario.superficie_terreno BETWEEN '$m_terrenoDesde' AND '$m_terrenoHasta') AND";
}else{
    $contErrores++;
}

if($bodega != ""){
$sqlMatch .= "(formulario.bodega = '$bodega') AND";
}else{
    $contErrores++;
}

if($estacionamientoDesde != "" && $estacionamientoHasta != ""){
$sqlMatch .= "(formulario.estacionamiento BETWEEN '$estacionamientoDesde' AND '$estacionamientoHasta') AND";
}else{
    $contErrores++;
}

if($piscina != ""){
    if($Episcina == "1"){
        $sqlMatch .= "(formulario.piscina = '$piscina') AND";
    }else{
        $sqlMatch .= "(formulario.piscina = '1' OR formulario.piscina = '0') AND";
    }

}else{
    $contErrores++;
}

if($cocina_americana != ""){
    if($EcocinaAme == "1"){
        $sqlMatch .= "(formulario.cocina_ame = '$cocina_americana') AND";
    }else{
        $sqlMatch .= "(formulario.cocina_ame = '1' OR formulario.cocina_ame = '0') AND";
    }
    
}else{
    $contErrores++;
}

if($cocina_isla != ""){
    if($EcocinaIsla == "1"){
        $sqlMatch .= "(formulario.cocina_isla = '$cocina_isla') AND";
    }else{
         $sqlMatch .= "(formulario.cocina_isla = '1' OR formulario.cocina_isla = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($comedor_diario != ""){
    if($EcomedorDiario == "1"){
        $sqlMatch .= "(formulario.comedor_diario = '$comedor_diario') AND";
    }else{
         $sqlMatch .= "(formulario.comedor_diario = '1' OR formulario.comedor_diario = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($termopanel != ""){
    if($Etermopanel == "1"){
        $sqlMatch .= "(formulario.term_panel = '$termopanel') AND";
    }else{
         $sqlMatch .= "(formulario.term_panel = '1' OR formulario.term_panel = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($amoblado != ""){
    if($Eamoblado == "1"){
        $sqlMatch .= "(formulario.amoblado = '$amoblado') AND";
    }else{
         $sqlMatch .= "(formulario.amoblado = '1' OR formulario.amoblado = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($mascotas != ""){
    if($Emascotas == "1"){
        $sqlMatch .= "(formulario.mascotas = '$mascotas') AND";
    }else{
         $sqlMatch .= "(formulario.mascotas = '1' OR formulario.mascotas = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($gimnasio != ""){
    if($gimnasio == "1"){
        $sqlMatch .= "(formulario.gim = '$gimnasio') AND";
    }else{
         $sqlMatch .= "(formulario.gim = '1' OR formulario.gim = 0) AND";
    }
    
}else{
    $contErrores++;
}


if($quincho != ""){
    if($Equincho == "1"){
        $sqlMatch .= "(formulario.quincho = '$quincho') AND";
    }else{
         $sqlMatch .= "(formulario.quincho = '1' OR formulario.quincho = 0) AND";
    }
    
}else{
    $contErrores++;
}

$sqlMatch .= " formulario.aprobado = 1 AND Perfil_busqueda.fk_interesado = $id AND fk_corredor = " . $corredor["id_corredor"];

// echo "<center>$sqlMatch </center>";
// echo "<center>$sqlMatch</center>";
$result = $conn->query($sqlMatch);
$idsForm = array();
if ($result->num_rows > 0 && $contErrores != 20) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        // echo "<center>match con el formulario: ".$row["id_formulario"]."</center>";
        $idForm = $row["id_formulario"];
        array_push($idsForm, $idForm);
    }
    $sinMatch = 0;
}else{
    if($contErrores == 20){
        echo "<center>no hay datos suficientes para el match</center>";
        $sinMatch = 1;
    }else{
        echo "<center>no hay match</center>";
        $sinMatch = 1;
    }
    
}
$sql2 = "SELECT * FROM formulario WHERE id_formulario = $idForm";

$result = $conn->query($sql2);

// if ($result->num_rows > 0) {
//     // output data of each row
//     while($row = $result->fetch_assoc()) {
//         $m_utiles = $row["superficie_util"];
//         $m_total = $row["superficie_total"];

//         $Direccion_id = $row["Direccion_id"];

//         $sqlDir = "SELECT * from Direccion where id = $Direccion_id";
//         $result2 = $conn->query($sqlDir);
//         if ($result2->num_rows > 0) {
//             // output data of each row
//             while($row2 = $result2->fetch_assoc()) {
//                 $direccion = $row2["calle"]." ".$row2["numero"];
//                 $fk_comuna = $row2["fk_comuna"];
//             }
//         }

//         $sqlComuna = "SELECT * from Comuna where id = $fk_comuna";
//         $result3 = $conn->query($sqlComuna);
//         if ($result3->num_rows > 0) {
//             // output data of each row
//             while($row3 = $result3->fetch_assoc()) {
//                 $direccion .=", ".$row3["nombre"] ;
//             }
//         }
//         $imagen =$row["imagen"];
//         if(!file_exists($imagen)){
//            $imagen = "../imagenesPropiedades/sinImg/casa.jpg"; 
//         }
//        $imagenHTML="<center><img src='$imagen'style='width:350px' ></center>";
//     }
// } else {
//     echo "no match";
// }
$count = 1;
foreach($idsForm as $id){
    // echo "<center>$count:$id</center>";
    $count = $count+1;
}
$sqlPer = "SELECT Persona.*, Interesado.* FROM Persona, Interesado WHERE Interesado.fk_persona = Persona.id_persona AND Interesado.id_interesado = $fk_interesado";
$result = $conn->query($sqlPer);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $nombre = $row["nombre"];
        $apellido = $row["apellido"];
        $rut = $row["rut"];
        $telefono = $row["telefono"];
        $correo = $row["correo"];
        $fk_corredor = $row["fk_corredor"];
    }
} else {
    
}
$sqlCorredor = "SELECT * from Persona, Corredor WHERE Persona.id_persona = Corredor.fk_persona and Corredor.id_corredor = $fk_corredor";
$result = $conn->query($sqlCorredor);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $nomCorredor = $row["nombre"];
        $apeCorredor = $row["apellido"];
        $nomCompleto = $nomCorredor." ".$apeCorredor;
    }
} else {
    
}
?>

<div id="master" class="container" style="<?=($sinMatch == 1)?'display:none':''?>">
<!-- Datos interesado -->
    <div class="row" style="background:#e0e0e0;">
        <div class="col-12">
            <h4 class="match-title">Interesado</h4>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <label for="nombre">Nombre: <?=$nombre?></label><br>
            <label for="apellido">Apellido: <?=$apellido?></label><br>
            <label for="corredor">Corredor: <?=$nomCompleto?></label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
            <label for="correo" style="width: 100%;">Correo: <input type="text" id="correo" value="<?=$correo?>"class= "match-input" disabled></label><br>
            <label for="telefono" style="width: 100%;">Telefono: <input type="text" id="telefono" value="<?=$telefono?>"class= "match-input" disabled></label>
        </div>
    </div>
    <!-- Match -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <h4 class="match-title">Match</h4>
                <div class="container-match">
                    <?php
                        foreach ($idsForm as $value) {
                            $sqlform = "SELECT * from formulario where id_formulario = $value";
                            $result = $conn->query($sqlform);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    // Datos para la card
                                    $m_utiles = $row["superficie_util"];
                                    $m_total = $row["superficie_total"];
                                    $dormitorios = $row["dormitorios"];
                                    $cant_bannos = $row["cant_bannos"];
                                    $monto1 = $row["monto1"];
                                    $monto2 = $row["monto2"];
                                    $divisa1 = $row["divisa_monto1"];
                                    $estacionamiento = $row["estacionamiento"];
                                    $bodega = $row["bodega"];
                                    $fk_tipo_propiedad = $row["fk_tipo_propiedad"];
                                    $superficie_terraza = $row["superficie_terraza"];
                                    $fk_corredor = $row["fk_corredor"];

                                    $sqlCorredor = "SELECT nombre , apellido FROM Persona, Corredor WHERE Persona.id_persona = Corredor.fk_persona AND id_corredor =$fk_corredor";
                                    $result3 = $conn->query($sqlCorredor);
                                    if ($result2->num_rows > 0) {
                                        // output data of each row
                                        while($row3 = $result3->fetch_assoc()) {
                                            $nombreC = $row3["nombre"];
                                            $apellidoCorredor = $row3["apellido"];
                                        }
                                    }


                                    $idDirec = $row["Direccion_id"];
                                    $sqlDir = "SELECT * from Direccion where id = $idDirec";
                                    $result2 = $conn->query($sqlDir);
                                    if ($result2->num_rows > 0) {
                                        // output data of each row
                                        while($row2 = $result2->fetch_assoc()) {
                                            $direccion = $row2["calle"]." ".$row2["numero"];
                                            $fk_comuna = $row2["fk_comuna"];
                                        }
                                    }else{
                                        echo $sqlDir;
                                    }
                            
                                    $sqlComuna = "SELECT * from Comuna where id = $fk_comuna";
                                    $result3 = $conn->query($sqlComuna);
                                    if ($result3->num_rows > 0) {
                                        // output data of each row
                                        while($row3 = $result3->fetch_assoc()) {
                                            $direccion .=", ".$row3["nombre"] ;
                                        }
                                    }
                                    $imagen =$row["imagen"];
                                    if(!file_exists($imagen)){
                                    $imagen = "../imagenesPropiedades/sinImg/casa.jpg"; 
                                    }
                                $imagenHTML="
                                <div style='padding:30px 0;'>
                                    <div style='text-align:center;'>
                                        <img src='$imagen'style='width:350px;'>
                                    </div>
                                ";
                                }
                            }else{
                                echo $sqlform;
                            }
                            ?>
                            <?=$imagenHTML?>
                            <br>
                            <div class="container-fluid">
                                <div class="row">
                                    <!-- Dir -->
                                    <div class="col-5">
                                        <label for="direccion"><b>Direccion:</b> </label>
                                    </div>
                                    <div class="col-7">
                                        <?=$direccion?>
                                    </div>
                                    <?php if($fk_tipo_propiedad == 2){?>
                                    <!-- M.Util -->
                                    <div class="col-5">
                                        <label for="metrosU"><b>Metros Utiles:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$m_utiles?> Mts.
                                    </div>

                                    <!-- M.Totales -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Metros Totales:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$m_total?> Mts.
                                    </div>
                                    <?php }else{?>
                                         <!-- M.Util -->
                                    <div class="col-5">
                                        <label for="metrosU"><b>Metros Utiles:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$m_utiles?> Mts.
                                    </div>

                                    <!-- M. Terraza -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Metros Terraza:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$superficie_terraza?> Mts.
                                    </div>
                                    <?php }?>
                                    <!-- Dormitorios -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Dormitorios:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$dormitorios?>
                                    </div>
                                    <!-- Baños -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Baños:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$cant_bannos?>
                                    </div>
                                    <!-- Precio -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Precio:</b></label>
                                    </div>
                                    <div class="col-7">
                                    <?=($divisa1 == 1)?'':'$'?><?=($monto1 == "")?$monto2:$monto1?> <?=($divisa1 == 1)?'UF':''?>
                                    </div>
                                    <!-- estacionamiento -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Estacionamientos:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$estacionamiento?>
                                    </div>
                                    <!-- bodegas -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>Bodegas:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$bodega?>
                                    </div>
                                    <!-- id -->
                                    <div class="col-5">
                                        <label for="metrosT"><b>id:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$value?>
                                    </div>
                                    <div class="col-5">
                                        <label for="metrosT"><b>Corredor:</b></label>
                                    </div>
                                    <div class="col-7">
                                        <?=$fk_corredor?>
                                    </div>
                                        
                                    <div style='text-align:right; margin:0 20px; width:100%;'>
                                    <label for='<?=$value?>'>Marcar para enviar</label>
                                        <input type='checkbox' name='<?=$value?>' id='<?=$value?>' onclick='ids(<?=$value?>)'>
                                    </div>
                                    <!-- <a href="" style="float: right; margin: 0 50px; text-align: right; width: 100%;">Ver más</a> -->
                                </div>
                            </div>
                        </div>
                        <hr>
                    <?php }?>
                </div>
        </div>

        <!-- Mensaje -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <h4 class="match-title">Enviar</h4>
            <textarea name="mensaje" id="mensaje" cols="" rows="5" style="width:100%;">
Estimado(a) <?=$nombre?>,
Adjunto propiedad(es) seleccionadas según sus preferencias.</textarea><br>
            <textarea cols="40" rows="2" id="claves" name="claves" style="display:none"></textarea>
            <div class="match-list">
                <ul>
                    <li>List 1</li>
                    <li>List 2</li>
                    <li>List 3</li>
                </ul>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6">
                        <input type="checkbox" name="EnvCorreo" id="EnvCorreo">
                        <label for="EnvCorreo">Enviar por correo</label>
                    </div>
                    
                    <div class="col-6">
                        <input type="checkbox" name="EnvWahts" id="EnvWahts">
                        <label for="EnvWahts">Enviar por Whastapp</label>
                    </div>
                </div>
            </div>
            <br>
            <input class="btn btn-def bg-redflip-red" type="submit" value="Enviar" onclick="mail()">
        </div>
    </div>
</div>
<script src="../js/dist/sweetalert2.all.min.js"></script>
 <script src="../js/match.js"></script>