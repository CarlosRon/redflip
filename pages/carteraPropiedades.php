<?php
include 'sidebarMenu.php';
include "../conexion.php";
require "../services/funciones.php";
if(!$_GET || $_GET["pagina"]<1){
    echo "<script>location.href='carteraPropiedades.php?pagina=1';</script>";
}
$sql2 = "Update usuario SET ultima_pag='https://indev9.com/redflip/pages/carteraPropiedades.php' where id_usuario =". $_SESSION['id'];

if ($conn->query($sql2) === TRUE) {

}
if($_SESSION["idPer"] == 1){
    $idSession = "";
}else{
    $idSession = "AND formulario.fk_corredor = (SELECT id_corredor FROM Corredor, Persona WHERE Corredor.fk_persona = Persona.id_persona AND Persona.id_persona =".$_SESSION["idPer"].")";
}

?>



    <section>
        <div class="container col-11" style="padding-top:50px;">
            <!-- Título -->
            <div class="row">
                <div class="column col-12">
                    <h3 class="h3-prop mtb-50">Cartera de Propiedades</h3>
                </div>
            </div>

            <!-- Acciones -->
            <div class="row" style="margin:30px;">
                <!-- Filtro Buscador -->
                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-6">
                    <form id="filtro" action="buscarCarteraPropiedades.php" method="get">
                        <div class="column col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input type="text" id="search" name="search" placeholder="🔍 Buscar . . .">
                        </div>
                    </form>
                </div>

                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-6" style="text-align:right;">
                    <form action="../controlador/subirArchivo.php" method="POST" enctype="multipart/form-data">
                        <button type="button" id="submitExport" class="btn btn-Imex bg-redflip-black pad-0" style="width:30%; padding:5px;" onclick="ExportExcel('xlsx')">
                            Exportar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <input type="hidden" id="idPer" name="idPer" value="<?=$_SESSION[" idPer "]?>">
    <div id=datos>
        <table class='table table-responsive-md table-hover table-striped' id='interesadosGlobales'>
            <thead>
                <tr class='thead-dark'>
                    <th class='' scope='col'> # </th>
                    <th class='' scope='col'> Nombre </th>
                    <th class='' scope='col'> Teléfono </th>
                    <th class='' scope='col'> Correo </th>
                    <th class='' scope='col'> Direccion </th>
                    <th class='' scope='col'> Comuna </th>
                    <th class='' scope='col'> Valor UF </th>
                    <th class='' scope='col'> Valor CLP </th>
                    <th class='' scope='col'> Fase </th>
                    <th class=' center-tab' scope='col'> Acciones </th>
                    <tr>
            </thead>
            <tbody>
                <?php
                    $iniciar = ($_GET["pagina"] - 1)*10;

                    $salida="";
                    $sqlHalph = "SELECT DISTINCT * FROM 
                    Direccion,formulario, Propietario,
                    Estado_prop, Sub_estado, Persona
                    WHERE formulario.fk_propietario = Propietario.id_propietario
                    AND Propietario.fk_persona = Persona.id_persona
                    AND formulario.aprobado = 1
                    AND Direccion.id = formulario.Direccion_id
                    $idSession
                    AND Propietario.fk_estado_prop = Estado_prop.id
                    AND Estado_prop.fk_sub_estado = Sub_estado.id";
                    $resultHalph = $conn->query($sqlHalph);

                    if ($resultHalph->num_rows > 0) {
                        // output data of each row
                        while($rowHalph = $result->fetch_assoc()) {
                        }
                    }else{
                        echo "<script>console.log('$sqlHalph')</script>";
                    }
                    $sqlMain = "SELECT DISTINCT * FROM 
                    Direccion,formulario, Propietario,
                    Estado_prop, Sub_estado, Persona
                    WHERE formulario.fk_propietario = Propietario.id_propietario
                    AND Propietario.fk_persona = Persona.id_persona
                    AND formulario.aprobado = 1
                    AND Direccion.id = formulario.Direccion_id
                    $idSession
                    AND Propietario.fk_estado_prop = Estado_prop.id
                    AND Estado_prop.fk_sub_estado = Sub_estado.id order by id_formulario desc
                    LIMIT $iniciar,10  ";
                    // echo $sqlMain;
                    $result = $conn->query($sqlMain);

                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            $nomInt = utf8_encode($row["nombre"]);
                            $apeInt = utf8_encode($row["apellido"]);
                            $telInt = trim($row["telefono"]);
                            $telInt = formato_telefono($telInt);
                            $calle = utf8_encode($row["calle"]);
                            $numero = $row["numero"];
                            $correoInt = $row["correo"];
                            $comunaId = $row["fk_comuna"];
                            $monto1 = $row["monto1"];
                            $monto2 = $row["monto2"];
                            
                            if($monto1 == ""){
                                $monto1 = "S/D";
                            }else{
                            $monto1 = moneda_chilena_sin_peso($monto1); 
                            }
                            if($monto2 == ""){
                                $monto2 = "S/D";
                            }else{
                            $monto2 = moneda_chilena_sin_peso($monto2); 
                            }
                            
                            $fase = $row["fase"];
                        

                            $sqlComuna = "SELECT * from Comuna where id = $comunaId";
                            $result3 = $conn->query($sqlComuna);
                            if ($result3->num_rows > 0) {
                                // output data of each row
                                while($row2 = $result3->fetch_assoc()) {
                                    $comuna = utf8_encode($row2["nombre"]);
                                }
                            }

                            
                            $id = $row["id_formulario"];
                            $idPer = $row["id_persona"];
                            $corredor = $row["fk_corredor"];

                            $salida.= "<tr>
                                        <td>".$id."</td>
                                        <td>".$nomInt. " " .$apeInt . "</td>
                                        <td>".trim($telInt)."</td>
                                        <td>".$correoInt."</td>
                                        <td>".$calle." ".$numero."</td>";
                            
                            $sqlCorredor = "SELECT * from Corredor, Persona where Corredor.fk_persona = Persona.id_persona and Corredor.id_corredor = " . $corredor;
                            $result2 = $conn->query($sqlCorredor);

                            //codigo para ver los datos del corredor
                            // if ($result2->num_rows > 0) {
                            //     while($row = $result2->fetch_assoc()) {
                            //     // $salida.= "
                            //     //     <td>".utf8_encode($row["nombre"]). " ". utf8_encode($row["apellido"])."</td>";
                                    
                            //     }
                            // }else{
                            //     echo "<script>console.log('no po')</script>"; 
                            // }
                            $salida.= "
                                        <td>".$comuna."</td>
                                        <td>$monto1</td>
                                        <td>$monto2</td>
                                        <td>$fase</td>
                                        <td class=''>
                                            <div class='btn-group dropleft'>
                                                <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                    <i class='fas fa-cog'></i>
                                                </button>
                                                <div class='dropdown-menu'>";
                                            
                                                    if($_SESSION["rol"] == 1){
                                                        $salida .= " <a class='dropdown-item' href='editarFormulario.php?id=$id&source=2'><i class='fas fa-edit'></i> Modificar</a>";
                                                    }
                                                    
                                                    $salida .= "
                                                    <a class='dropdown-item' href='' onclick='redirigir($id, `2`); return false'><i class='far fa-file-alt'></i></i> Ver Ficha</a>
                                                    <a class='dropdown-item' href='https://api.whatsapp.com/send?phone=$telInt&text=Hola%20$nomInt,' target='_blank'><i class='fab fa-whatsapp'></i> Whatsapp</a>
                                                </div>
                                            </div>
                                        </td> 
                                    </tr>";
                        }
                    }else{
                        $salida .= "<td class= 'propTabN center-tab' colspan=8 > No posee una cartera de propiedades</td>
                                </tr>";
                    }
                    // echo $sqlMain;
                    
                    $filas = $resultHalph->num_rows;
                    $paginas =ceil($filas/10);
                    if($_GET["pagina"] > $paginas){
                        echo "<script>location.href='carteraPropiedades.php?pagina=$paginas';</script>";
                    }
                    echo $salida;
                    $pagina = $_GET["pagina"];
                    // calculamos la primera y última página a mostrar
                    $primera = $pagina - ($pagina % 5) + 1;
                    // echo $primera;
                    if ($primera > $pagina) { $primera = $primera - 4; }
                    $ultima = $primera + 4 > $paginas ? $paginas : $primera + 4;
                    // echo $ultima;
                ?>
            </tbody>
        </table>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item <?=($_GET[" pagina "]<=1)? "disabled ":" "?>">
                            <a class="page-link" href="carteraPropiedades.php?pagina=<?=$_GET[" pagina "] - 1?>">
                                <</a>
                        </li>
                        <?php for($i=$primera; $i<=$ultima; $i++):?>

                        <li class="page-item">
                            <a class="page-link" href="carteraPropiedades.php?pagina=<?=$i?>">
                                <?=$i?>
                            </a>
                        </li>
                        <?php endfor?>
                        <li class="page-item <?=($_GET[" pagina "]>=$paginas)? "disabled ":" "?>"><a class="page-link" href="carteraPropiedades.php?pagina=<?=$_GET[" pagina "] + 1?>">></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>


    <form action="process.php" method="post" target="_blank" id="formExport">

        <input type="hidden" id="data_to_send" name="data_to_send" />

        <input type="hidden" id="nombre" name="nombre" value="Propietarios Activos" />

    </form>



    <?

    if(isset($_GET["msg"])){

    ?>



        <input type="hidden" id="msg" value=< ?echo $_GET[ "msg"] ?> >



        <?  

        }else

        {

    ?>



            <input type="hidden" id="msg" value="">

            <?

    }

        if(isset($_GET["nom"])){

    ?>



                <input type="hidden" id="nom" value=< ?echo $_GET[ "nom"] ?> >

                <?   

    }

        if(isset($_GET["cont"])){

    ?>



                    <input type="hidden" id="cont" value=< ?echo $_GET[ "cont"] ?> >



                    <?

        }else{

    ?>



                        <input type="hidden" id="cont" value="">



                        <?

    }

    

    include 'footer.php';

    ?>

                            <script src="../js/dist/sweetalert2.all.min.js"></script>

                            <!-- <script src="../js/selectFase.js"></script> -->

                            <!-- <script src="../js/buscarInteresados.js"></script> -->

                            <script src="../js/toastr.js"></script>

                            <!-- <script src="../js/accionProp.js"></script> -->
                            <script src="../js/interesados.js"></script>
                            <script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>

                            <script>
                                // document.getElementById('submitExport').addEventListener('click', function(e) {

                                //     e.preventDefault();

                                //     let export_to_excel = document.getElementById('export_to_excel');

                                //     let data_to_send = document.getElementById('data_to_send');

                                //     data_to_send.value = export_to_excel.outerHTML;

                                //     document.getElementById('formExport').submit();

                                // });



                                var msg = document.getElementById("msg").value;

                                var cont = document.getElementById("cont").value;

                                if (msg === "1") {

                                    var nom = document.getElementById("nom").value;

                                    alert("El archivo: " + nom + " se ha subido correctamnte")

                                } else if (msg === "2") {

                                    alert("El archivo no es un excel")

                                }

                                if (cont === "") {



                                } else {

                                    alert("se agregaron " + cont + " propietarios");

                                }

                                function redirigir(id, origen) {
                                    // window.open('fichaForm.php?id='+id, '');
                                    window.open("fichaForm.php?id=" + id + "&origen=" + origen, 'name', 'height=800,width=700 center');
                                }
                            </script>



                            <script type="text/javascript">
                                function ExportExcel(type, fn, dl) {

                                    var elt = document.getElementById('interesadosGlobales');

                                    var wb = XLSX.utils.table_to_book(elt, {
                                        sheet: "Sheet JS"
                                    });

                                    return dl ?

                                        XLSX.write(wb, {
                                            bookType: type,
                                            bookSST: true,
                                            type: 'base64'
                                        }) :

                                        XLSX.writeFile(wb, fn || ('Propietarios.' + (type || 'xlsx')));

                                }
                            </script>




                            </body>

                            </html>