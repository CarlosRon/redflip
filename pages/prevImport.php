
<!DOCTYPE html>
<html lang="es-cl">
    <head>
        <!--=== Required meta tags ===-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <!--=== Title ===-->
        <title></title>
        <!--=== Favicon Icon ===-->
        <link rel="icon" href="../favicon.ico" type="image/x-icon" />
        <!--=== Bootstrap CSS ===-->
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
        <!--=== Font Awesome ===-->
        <link rel="stylesheet" type="text/css" href="../css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!--=== Style CSS ===-->
        <link rel="stylesheet" type="text/css" href="../css/menu.css">
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <!-- <link href="http://cdn.grapecity.com/spreadjs/hosted/css/gc.spread.sheets.excel2013white.10.1.0.css" rel="stylesheet" type="text/css" />   -->
        <link href="../css/toastr.css" rel="stylesheet"/>
        
    </head>
    <body>
    <?php 
            // include 'header.php';
            include '../conexion.php';
            require '../services/Classes/PHPExcel/IOFactory.php';
            include '../controlador/mcript.php';
            include 'valid_session.php';
           
                
        ?>
        <div>
        <?php
        if(isset($_GET["msg"])){
            $msg = $_GET["msg"];
        ?>
        
            <input type="hidden"id="msg"value= <?echo $msg  ?>>
            <?php
            }
            $cont=0;
            $nombre = $_GET["nom"];
            $nombreArchivo = "../uploadFiles/" . $nombre;
            $objPHPExcel = PHPEXCEL_IOFactory::load($nombreArchivo);
            $objPHPExcel -> setActiveSheetIndex(0);

            $nomArray = array();
            $apeArray = array();
            $telArray = array();
            $calleArray = array();
            $comunaArray = array();
            $tipoPropArray = array();
            $operacionArray = array();
            $m2Array = array();
            $m2UtilArray = array();
            $domrArray = array();
            $bannosArray = array();
            $bodegasArray = array();
            $persona = array();

                //     echo '<div class="progress" style="height : 16px">
                //     <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
                //   </div>';
            echo "
                <table border=1 class='table table-striped' style='margin-top: 50px; margin-bottom:50px!important;'>
                <thead>
                    <tr class='thead-dark'>
                        <th scope='col'>Nombre</th>
                        <th scope='col'>Apellido</th>
                        <th scope='col'>Telefono</th>
                        <th scope='col'>Calle</th>
                        <th scope='col'>Comuna</th>
                        <th scope='col'>Tipo Propiedad</th>
                        <th scope='col'>Operación</th>
                        <th scope='col'>Metros Totales</th>
                        <th scope='col'>Metros útiles</th>
                        <th scope='col'>Dormitorios</th>
                        <th scope='col'>Baños</th>
                        <th scope='col'>Bodegas</th>
                        
                    </tr>
                </thead>
                <tbody>";
            
                $numRows = $objPHPExcel -> setActiveSheetIndex(0) -> getHighestRow();
                    //recorre el archivo .xlsx y saca los datos desde la fila numero 2 en adelante
                    for($i = 2; $i <= $numRows; $i++){
                        // $hidden = '<input type="hidden" id="hidden" value="'.$i.'">';
                        // echo $hidden;
                        // $ori = $objPHPExcel-> getActiveSheet() ->getCell('A'.$i)-> getCalculatedValue();
                        $nom = $objPHPExcel-> getActiveSheet() ->getCell('B'.$i)-> getCalculatedValue();
                        // $ape = $objPHPExcel-> getActiveSheet() ->getCell('C'.$i)-> getCalculatedValue();
                        $nombreCompleto = explode(" ",$nom);
                        if(count($nombreCompleto)>2){
                            $nom = $nombreCompleto[0];
                            $ape=$nombreCompleto[2];
                        }else if(count($nombreCompleto) == 2){
                            $nom = $nombreCompleto[0];
                            $ape = $nombreCompleto[1];
                            
                        }else{
                            $nom = $nombreCompleto[0];
                            $ape = "";
                        }
                        //gestion de telefono si es casa pasa a la siguiente columna
                        $tel = $objPHPExcel-> getActiveSheet() ->getCell('E'.$i)-> getCalculatedValue();
                        if(substr($tel, 0, 3) == "562"){      
                            $tel = $objPHPExcel-> getActiveSheet() ->getCell('F'.$i)-> getCalculatedValue();                  
                            if(substr($tel, 0, 3) == "562"){
                                $tel = "S/D";
                            }else if(substr($tel, 0, 3) == "569" || substr($tel, 0, 3) == "568" || substr($tel, 0, 3) == "567"){
                                $tel = "+".$tel;
                            }else if(substr($tel, 0, 1) == "9" && strlen($tel) == 8){
                                $tel = "+569" . $tel;
                            }else if(strlen($tel) == 9){
                                $tel = "+56" . $tel;
                            }else if(strlen($tel) == 8){
                                $tel = "+569" . $tel;
                            }else{
                                $tel = "S/D";
                            }
                        }else if(substr($tel, 0, 3) == "569" || substr($tel, 0, 3) == "568" || substr($tel, 0, 3) == "567"){
                            
                            $tel = "+".$tel;

                        }else if(substr($tel, 0, 1) == "9" && strlen($tel) == 9){
                            $tel = "+56" . $tel;
                        }else if(strlen($tel) == 8){
                            $tel = "+569" . $tel;
                        }else{
                            $tel = "S/D";
                        }

                        // $correo = $objPHPExcel-> getActiveSheet() ->getCell('E'.$i)-> getCalculatedValue();


                        $calle = $objPHPExcel-> getActiveSheet() ->getCell('D'.$i)-> getCalculatedValue();
                        
                        $direccionCompleta = explode(" ", $calle);
                        $calle = $direccionCompleta[0] . " " . $direccionCompleta[1];
                        $comuna = $objPHPExcel-> getActiveSheet() ->getCell('T'.$i)-> getCalculatedValue();
                        if($comuna == "" || $comuna == null){
                            $comuna = "S/D";
                        }
                        $operacion = $objPHPExcel-> getActiveSheet() ->getCell('O'.$i)-> getCalculatedValue();
                        if($operacion == ""){
                            $operacion = "S/D";
                        }
                        $tipoProp = $objPHPExcel-> getActiveSheet() ->getCell('P'.$i)-> getCalculatedValue();
                        if($tipoProp == "" || $tipoProp == null){
                            $tipoProp = "S/D";
                        }
                        if($tipoProp == "sitio"){
                            $tipoProp = "terreno";
                        }else if($tipoProp == "local"){
                            $tipoProp = "local comercial";
                        }
                        $m2 = $objPHPExcel-> getActiveSheet() ->getCell('J'.$i)-> getCalculatedValue();
                        $m2Mod = explode(" ", $m2);
                        if($m2Mod[0] == "" || $m2Mod == null){
                            $m2Mod[0] = "S/D";
                        }
                        $m2Util = $objPHPExcel-> getActiveSheet() ->getCell('K'.$i)-> getCalculatedValue();
                        $m2UtilMod = explode(" ", $m2Util);
                        if($m2UtilMod[0] == "" || $m2UtilMod == null){
                            $m2UtilMod[0] = "S/D";
                        }
                        $dormitorios = $objPHPExcel-> getActiveSheet() ->getCell('L'.$i)-> getCalculatedValue();
                        if($dormitorios == "" || $dormitorios == null){
                            $dormitorios = "0";
                        }
                        $bannos = $objPHPExcel-> getActiveSheet() ->getCell('M'.$i)-> getCalculatedValue();
                        if(strlen($bannos) || $bannos == null){
                            $bannos = 0;
                        }
                        $bodegas = $objPHPExcel-> getActiveSheet() ->getCell('N'.$i)-> getCalculatedValue();
                        if(strlen($bodegas) > 2){
                            $bodegas = 0;
                        }
                        if($bodegas == ""){
                            $bodegas = 0;
                        }

                            
                                echo "<tr class=' tb-data-single'>";
                                echo "<td class='table-item'>$nom</td>";
                                echo "<td class='table-item'>$ape</td>";
                                echo "<td class='table-item'>$tel</td>";
                                echo "<td class='table-item'>$calle</td>";
                                echo "<td class='table-item'>$comuna</td>";
                                echo "<td class='table-item'>$tipoProp</td>";
                                echo "<td class='table-item'>$operacion</td>";
                                echo "<td class='table-item'>".$m2Mod[0]."</td>";
                                echo "<td class='table-item'>".$m2Util[0]."</td>";
                                echo "<td class='table-item'>$dormitorios</td>";
                                echo "<td class='table-item'>$bannos</td>";
                                echo "<td class='table-item'>$bodegas</td>";
                                echo "</tr>";
                                
                                // array_push($nomArray, $nom);
                                
                                array_push($persona, ["nombre" => $nom, "apellido" => $ape, "tel" => $tel , "calle" => $calle, "comuna" => $comuna, "tipoProp" => $tipoProp, 
                                                        "operacion" => $operacion, "m2" => $m2Mod[0], "m2Util" => $m2Util[0], "dormitorios" => $dormitorios, "bannos" => $bannos, "bodegas" => $bodegas]);
                                // array_push($apeArray, $ape);
                                // array_push($telArray, $tel);
                                // array_push($calleArray, $calle);
                                // array_push($comunaArray, $comuna);
                                // array_push($tipoPropArray, $tipoPropArray);
                                // array_push($operacionArray, $operacion);
                                // array_push($m2Array, $m2Mod);
                                // array_push($m2UtilArray, $m2Util);
                                // array_push($domrArray, $dormitorios);
                                // array_push($bannosArray, $bannos);
                                // array_push($bodegasArray, $bodegas);
                    }
                echo "</tbody>
                </table>
                ";
                
            ?>
        </div>

        <div>
            <form id="formArrays" style="text-align:center;">
                <input type="hidden" value= '<?echo $nombre;?>' name="nom">

                <input type="hidden" value= '<?echo serialize($persona);?>' name="persona">

                <button type="submit" class="btn btn-action bg-redflip-green">Aceptar</button>
                <button type="button" class="btn btn-action bg-redflip-red" onclick="cerrarPopup()">cerrar</button>
            </form>
        </div>

        <!-- JS - Prev Import -->
        <script>
            function cerrarPopup(){
                window.close();
            }

            var msg = document.getElementById("msg").value;
            console.log(msg);

            if(msg == '2'){
                alert("El archivo no corresponde con el formato");
                window.close();
            }  
        
        </script>
        <script src="../js/prevImport.js"></script>
        <script src="../js/dist/sweetalert2.all.min.js"></script>

    </body>
</html>