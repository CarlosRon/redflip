<?
    include 'sidebarMenu.php';
    include '../conexion.php';
    $sql2 = "Update usuario SET ultima_pag='http://indev9.com/redflip/pages/AgregarInteresado.php' where id_usuario =". $_SESSION['id'];
    if ($conn->query($sql2) === TRUE) {
    } else {
    }
    // include '../services/Serv_agregarProp.php';
?>


<section>
    <div class="container">
        <form action="" id="formC">

                <!-- progressbar -->
            <ul id="progressBar">
                <li class="active">informacion del Interesado</li>
                <li>Perfil de Busqueda</li>
                <li>Seleccionar Caracteristicas</li>
              
            </ul>

            <!-- <div class="container">
                <div class="row">
                    <div class="column col-12"></div>
                </div>
            </div> -->
                <!-- fieldset 1 -->
            <fieldset id="1">

                <h2 class="fs-title">Información del Interesado</h2>
                <h3 class="fs-subtitle">Paso 1</h3>
            <div>
              <h2></h2>
                <div>
                 
                  <input type="text" name="nom" id="nom" placeholder="Nombre" required>
                 
                  <input type="text" name="ape" id="ape" placeholder="Apellido" required>
                  
                  <select name="ciudad" id="ciudad">
                    <option value="" disabled selected>Ciudad</option>
                    <?php
                    $sqlCiudad = "SELECT * from Ciudad ORDER BY nombre ASC";
                    $result = $conn->query($sqlCiudad);

                  if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                        echo "<option value=".$row["id_ciudad"].">".utf8_encode($row["nombre"])."</option>";
                      }
                    }
                    
                    ?>
                  </select>
                </div>
            <div>
                
                <input type="text" name="rut" id="rut" placeholder="Rut">
               
                <input type="tel" name="tel" id="tel" style="width: 255px;height: 56px;" placeholder="Teléfono" required>
                <input type="mail" name="correo" id="correo" style="width: 260px;height: 56px;" placeholder="Mail" required>

              </div>
              <div>
                
                <select name="corredor" id="corredor" required <?= ($_SESSION["rol"] != 1)? "disabled" : "" ?>>
                <option value="" disabled selected>Seleccione un corredor</option>
                <?php
                  $sqlCorredor = "SELECT * FROM Corredor, Persona WHERE Corredor.fk_persona = Persona.id_persona";
                  if($_SESSION["rol"] != 1){
                    $sqlCorredor .= " AND Persona.id_persona = " . $_SESSION["idPer"];
                  }
                  $result = $conn->query($sqlCorredor);

                  if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                          if($_SESSION["rol"] != 1){
                            echo "<option value='".$row["id_corredor"]."' selected>".utf8_encode($row["nombre"])." ".utf8_encode($row["apellido"])."</option>";
                          }else{
                            echo "<option value='".$row["id_corredor"]."'>".utf8_encode($row["nombre"])." ".utf8_encode($row["apellido"])."</option>";
                          }
                        }
                  } else {
                      echo "<option disabled style='color:red'>Sin Datos</option>";
                  }
                ?>
                </select>
              </div>
          </div>
                      
                <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

            </fieldset>

                <!-- fieldset 2 -->
              <fieldset id="2">
                <h2 class="fs-title">Perfil de Busqueda</h2>
                <h3 class="fs-subtitle">Paso 2.1 </h3>
                
                <div>
              
                <select name="tipoProp" id="tipoProp" required>
                <option value="" disabled selected>Tipo Propiedad</option>
                <?php
                
                $sqlTipoProp = "SELECT * FROM tipo_propiedad WHERE id_tipo_propiedad NOT IN (9) order by tipo asc";
                $result = $conn->query($sqlTipoProp);

                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        echo "<option value='".$row["id_tipo_propiedad"]."'>".$row["tipo"]."</option>";
                    }
                } else {
                }
                
                ?>
              
              </select>
              </div>
              <div>
                <select name="tipo_operacion" id="tipo_operacion">
                <option value=""disabled selected>Tipo Operación</option>
                <?php
                $sqlTipoOp = "SELECT * from Venta_Arriendo where id not in (4)";
                $result = $conn->query($sqlTipoOp);

                if ($result->num_rows > 0) {
                  // output data of each row
                  while($row = $result->fetch_assoc()) {
                    echo "<option value=".$row["id"].">".utf8_encode($row["descripcion"])."</option>";
                  }
                }
                ?>
              </select>
            </div>
              <br>
           <div class="container fs-11">
             <div class="row">
            <div class="column col-12" >
              <div class="fluid-container">
                <div class="row">
                 <div class="column col-4">
                   
                   <label for="dormitorios">
                    Dormitorios
                   </label>
                  </div>
                    <div class="column col-2">
                      <!-- <label class="switch">
                      <input type="checkbox" id="dormitorios" name="dormitorios">
                      <span class="slider round"></span>   
                      </label> -->
                    </div>
                     <div class="colum col-3">
                     <input type="number" name="dormDesde" id="dormDesde" min="0" style="width: 40px;" > - <input type="number" name="dormHasta" id="dormHasta" min="0" style="width: 40px;" > 
                     </div>                                                              
                  </div>
                </div>
               </div>
               
            <div class="column col-12" >
              <div class="fluid-container">
                <div class="row">
                 <div class="column col-4">
                   <label for="bannos">
                    Baños
                   </label>
                  </div>
                    <div class="column col-2">
                      <!-- <label class="switch">
                      <input type="checkbox" id="Ebanno" name="Ebanno">
                      <span class="slider round"></span>   
                      </label> -->
                    </div>
                     <div class="colum col-3">
                     <input type="number" name="bannosDesde" id="bannosDesde" min="0" style="width: 40px;" > - <input type="number" name="bannosHasta" id="bannosHasta" min="0" style="width: 40px;" > 
                     </div>                                                              
                  </div>
                </div>
              </div>

              <div class="column col-12" >
              <div class="fluid-container">
                <div class="row">
                 <div class="column col-4">
                   <label for="mutiles">
                   Metros útiles
                   </label>
                  </div>
                    <div class="column col-2">
                      <!-- <label class="switch">
                      <input type="checkbox" id="EmUtiles" name="EmUtiles">
                      <span class="slider round"></span>   
                      </label> -->
                    </div>
                     <div class="colum col-3">
                     <input type="number" name="mutilesDesde" id="mutilesDesde" min="0" style="width: 40px;" > - <input type="number" name="mutilesHasta" id="mutilesHasta" min="0" style="width: 40px;" > 
                     </div>                                                              
                  </div>
                </div>
              </div>

              <div class="column col-12" >
              <div class="fluid-container">
                <div class="row">
                 <div class="column col-4">
                   <label for="mTerraza">
                   Metros Terraza
                   </label>
                  </div>
                    <div class="column col-2">
                      <!-- <label class="switch">
                      <input type="checkbox" id="EmTerraza" name="EmTerraza">
                      <span class="slider round"></span>   
                      </label> -->
                    </div>
                     <div class="colum col-3">
                     <input type="number" name="mTerrazaDesde" id="mTerrazaDesde" min="0" style="width: 40px;" > - <input type="number" name="mTerrazaHasta" id="mTerrazaHasta" min="0" style="width: 40px;" > 
                     </div>                                                              
                  </div>
                </div>
              </div>

              <div class="column col-12" >
              <div class="fluid-container">
                <div class="row">
                 <div class="column col-4">
                   <label for="mTerrenoDesde">
                   Metros Terreno (solo casa)
                   </label>
                  </div>
                    <div class="column col-2">
                      <!-- <label class="switch">
                      <input type="checkbox" id="EmTerraza" name="EmTerraza">
                      <span class="slider round"></span>   
                      </label> -->
                    </div>
                     <div class="colum col-3">
                     <input type="number" name="mTerrenoDesde" id="mTerrenoDesde" min="0" style="width: 40px;" > - <input type="number" name="mTerrenoHasta" id="mTerrenoHasta" min="0" style="width: 40px;" > 
                     </div>                                                              
                  </div>
                </div>
              </div>
               
              <div class="column col-12">
                <div class="fluid-container">
                  <div class="row">
                    <div class="column col-6">
                      <label for="valorMin">Valor Minimo </label>
                    </div>
                    <input type="number" name="valorMin" id="valorMin" style="margin-left: 15px;width: 190px;">
                  </div>
                  <br>
                </div>
              </div>
              
              <div class="colum col-12">
                <div class="fluid-container">
                  <div class="row">
                    <div class="column col-6">
                      <label for="valorMin">Valor Maximo </label>
                    </div>
                    <input type="number" name="valorMax" id="valorMax" style="margin-left: 15px;width: 190px;">
                  </div>
                  <br>
                </div>
              </div>
                
             

              <div class="column col-12" >
              <div class="fluid-container">
                <div class="row">
                 <div class="column col-4">
                   <label for="bodega">
                    Bodega
                   </label>
                  </div>
                    <div class="column col-2"></div>
                     <div class="colum col-3">
                     <input type="number" name="bodega" id="bodega" min="0" style="width: 40px;">
                     </div>
                     <div class="colum col-2">
                     <label class="switch">
                      <input type="checkbox" id="Ebod" name="Ebod">
                      <span class="slider round"></span>   
                      </label>

                     </div>                                                              
                  </div>
                </div>
              </div>

              <div class="column col-12" >
              <div class="fluid-container">
                <div class="row">
                 <div class="column col-4">
                   <label for="mTerraza">
                   Estacionamiento
                   </label>
                  </div>
                    <div class="column col-2">
                      <!-- <label class="switch">
                      <input type="checkbox" id="Eestacionamiento" name="Eestacionamiento">
                      <span class="slider round"></span>   
                      </label> -->
                    </div>
                     <div class="colum col-3">
                     <input type="number" name="estacionamientosDesde" id="estacionamientosDesde" min="0" style="width: 40px;" > - <input type="number" name="estacionamientosHasta" id="estacionamientosHasta" min="0" style="width: 40px;" > 
                     </div>                                                              
                  </div>
                </div>
              </div>

              <div class="column col-12">
               <div class="fluid-container">
                <div class="row">
                <div class="column col-4">
                   <label for="mTerraza">
                   Piso Depto
                   </label>
                  </div>
                  <div class="column col-2"></div>
                  <div class="column col-3">
                  <input type="number" name="pisoDeptoMin" id="pisoDeptoMin" style="width: 40px;"> - <input type="number" name="pisoDepto" id="pisoDepto" style="width: 40px;">
                  </div>
                </div>
               </div>
              </div>
             </div>
           </div>
                <!-- Todos los switch con estilo son indicadores de Excluyentes o No excluyentes -->
                <!-- <label class="switch">
                  <input type="checkbox" id="Edorm" name="Edorm">
                  <span class="slider round"></span>
                </label>
            </div> -->

            <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
            <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

              </fieldset>
               <!-- fieldset 2.2 -->
              <fieldset id="3">
              <h2 class="fs-title">Orientación</h2>
              <h3 class="fs-subtitle">Paso 2.2</h3>


              

                <table>
                <tr>
                  <td><label class="fs-chkLbl" for="todos">Todos 
                  <input type="checkbox" id="todos" name="todos">
                  <span class="checkmark"></span></label></td>
                  <td><label class="fs-chkLbl" name="norte" for="norte">Norte 
                  <input type="checkbox" id="norte" name="norte">
                  <span class="checkmark" name="norte"></span></label></td>
                </tr>
                <tr>
                  <td><label class="fs-chkLbl" name="sur" for="sur">Sur
                  <input type="checkbox" id="sur" name="sur">
                  <span class="checkmark" name="sur"></span></label></td>
                  <td><label class="fs-chkLbl" name="oriente" for="oriente">Oriente 
                  <input type="checkbox" id="oriente" name="oriente">
                  <span class="checkmark" name="oriente"></span></label></td>
                </tr>
                <tr>
                  <td><label class="fs-chkLbl" name="poniente" for="poniente">Poniente 
                  <input type="checkbox" id="poniente" name="poniente">
                  <span class="checkmark" name="poniente"></span></label></td>
                  <td><label class="fs-chkLbl" name="surOriente" for="surOriente">Sur-Oriente 
                  <input type="checkbox" id="surOriente" name="surOriente">
                  <span class="checkmark" name="surOriente"></span></label></td>
                </tr>
                <tr>
                  <td><label class="fs-chkLbl" name="surPoniente" for="surPoniente">Sur-Poniente
                  <input type="checkbox" id="surPoniente" name="surPoniente">
                  <span class="checkmark" name="surPoniente"></span></label></td>
                  <td><label class="fs-chkLbl" name="norOriente" for="norOriente">Nor-Oriente 
                  <input type="checkbox" id="norOriente" name="norOriente">
                  <span class="checkmark" name="norOriente"></span></label></td>
                </tr>
                <tr>
                  <td><label class="fs-chkLbl" name="norPoniente" for="norPoniente">Nor-Poniente
                  <input type="checkbox" id="norPoniente" name="norPoniente">
                  <span class="checkmark" name="norPoniente"></span></label></td>
                </tr>
              </table>
              <br>

              <select name="selectComuna" id="selectComuna">
                <option value="" disabled selected>Seleccionar Comuna</option>
                <?php
                $sqlComuna = "SELECT * FROM Comuna WHERE id NOT IN (53) ORDER BY nombre ASC ";
                $result = $conn->query($sqlComuna);

                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        echo "<option value='".$row["id"]."'>".utf8_encode($row["nombre"])."</option>";
                    }
                }
                
                ?>
              </select>
            
          <div id="container" style="border:1px solid lightgray;width: 100%;margin-right: 2%;height: 280px;"></div>
          
          <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />     
          <input type="button" name="next" class="next bg-secondary action-button" value="Siguiente" />

              </fieldset>

              <!-- Fieldset 3 -->
              <fieldset id="4">
              <h2 class="fs-title">Características</h2>
                <h3 class="fs-subtitle">Paso 3 </h3>
                <br>
                
                <div class="container">
                  <div class="row">
                    <div class="col-sm"></div>
                    <div class="col-sm">
                <h4 class="fs-subtitle" style="float:center;font-size:small">Opcional </h4>
                     </div>
                    <div class="col-sm">
                <h4 class="fs-subtitle" style="float:right;font-size:small">Excluyente</h4>
                    </div>
                  </div>
                </div>


                <div class="container">
                  <div class="row">
                    <div class="col-sm">
                    <br>
                    <label for="piscina">Piscina </label>
                    </div>
                     <div class="col-sm">
                    <br>
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="piscina" name="piscina">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                          <br>
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="Episcina" name="Episcina">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>

                 <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="cocinaAme">Cocina Americana</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="cocinaAme" name="cocinaAme">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="EcocinaAme" name="EcocinaAme">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>
            
                <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="cocinaIsla">Cocina Isla</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="cocinaIsla" name="cocinaIsla">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="EcocinaIsla" name="EcocinaIsla">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>

                <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="comedorDiario">Comedor Diario</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="comedorDiario" name="comedorDiario">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="EcomedorDiario" name="EcomedorDiario">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>

                     <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="termoPanel">Termopanel</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="termoPanel" name="termoPanel">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="EtermoPanel" name="EtermoPanel">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>

                     <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="amoblado">Amoblado</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="amoblado" name="amoblado">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="Eamoblado" name="Eamoblado">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>
                     
                     <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="mascotas">Mascotas</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="mascotas" name="mascotas">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="Emascotas" name="Emascotas">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>

                     <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="gim">Gimnasio</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="gim" name="gim">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="Egim" name="Egim">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>
                     
                     <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="dormServicio">Dormitorio de servicio</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="dormServicio" name="dormServicio">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="EdormServicio" name="EdormServicio">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>

                     <div class="container">
                  <div class="row">
                    <div class="col-sm">             
                    <label for="quincho">Quincho</label>
                    </div>
                     <div class="col-sm">
                      <label class="switch" style="margin-left: 45px;">
                      <input type="checkbox" id="quincho" name="quincho">
                      <span class="slider round"></span>
                      </label>
                      </div>
                        <div class="col-sm">
                         <label class="switch" style="margin-left: 90px;">
                         <input type="checkbox" id="Equincho" name="Equincho">
                         <span class="slider round"></span>
                         </label>
                        </div>
                       </div>
                     </div>

        <input type="button" name="previous" class="previous bg-secondary action-button" value="Anterior" />
        <input type="button" onclick="" name="quit" id="quit" class="save action-button" value="Finalizar"/>

              </fieldset>
        </form>
    </div>
</section>

      <textarea name="comunas" id="comunas" cols="30" rows="10" style="display:none"></textarea>
 


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/dist/sweetalert2.all.min.js"></script>
<script src="../js/toastr.js"></script>
<script src="../js/agregarInteresado.js"></script>

<?php
    include 'footer.php';
?>