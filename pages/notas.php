<?php
  include 'header.php';
  include '../conexion.php';
  $id = $_GET["id"];
  $sql2 = "Update usuario SET ultima_pag='indev9.com/redflip/pages/notas.php?id=$id' where id_usuario =". $_SESSION['id'];
  if ($conn->query($sql2) === TRUE) {
  } else {
  }

          
?>

<link href="https://unpkg.com/@fullcalendar/core@4.3.1/main.min.css" rel="stylesheet">
<link href="https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.css" rel="stylesheet">
<link href="https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.css" rel="stylesheet">

    <section>
      <div class="container-fluid" style="height:auto;margin-bottom:100px;">
        <div class="row">
          <div class="column col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="container-fluid">
              <div class="row">
                  <!-- [CALENDARIO] -->

                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6 no-ta">
                  <h3 class="h3-prop mtb-50"></h3>
                      <div id='calendar'></div>
                        <div>
                        <form id = "formEvent" >
                          <div class="form-group row">
                            <div class="col-lg-3">
                              <label >Título</label>
                              <input class="form-control" type="text" id = "title" name="title" placeholder="Ej. Primera visita" required>
                            </div>
                            <div class="col-lg-3">
                              <label >Fecha inico</label>
                              <input class="form-control" type="date" id = "start" name="start" required>
                            </div>
                            <div class="col-lg-3">
                            <label >Fecha fin</label>
                              <input class="form-control" type="date" id = "end" name="end" required>
                              <input type="hidden" id="id" name="id" value="<?echo $id?>">
                            </div>
                            <div class="col-lg-3">
                            <label >Hora</label>
                              <input class="form-control" type="time" id = "time" name="time" value="00:00"required>
                            </div>
                            <div class="col-lg-3 mt-40">
                            <button class="btn-action bg-redflip-red" type="submit">Guardar</button>
                            </div>
                          </div>
                        </form>
                        <input type="hidden" id="resolution"value="">
                        </div>
                </div>

                <!-- [NOTAS] -->

                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-6">
                  <h3 class="h3-prop mtb-50">Notas</h3>
                    <div>
                      <form id="formNotas">
                        <input type="hidden" id="idProp" name="idProp" value="<?echo $id?>">
                        <textarea class="" id="notas" name="notas" class="notas" maxlength="1000"></textarea><br>
                        <button class="btn-def bg-redflip-red" type="submit" value="Guardar">Guardar</button>
                    </div>
                      </form>
                </div>
              </div>
          </div>
      </div>
      <iframe src="https://calendar.google.com/calendar/embed?src=daryldaza98%40gmail.com&ctz=America%2FSantiago" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
    </section>

    <?
    include 'footer.php';
    ?>

    <!-- JS - NOTAS -->
    <script src="../js/dist/sweetalert2.all.min.js"></script>
    <script src="../js/mostrarNotas.js"></script>
    <script src="https://unpkg.com/@fullcalendar/core@4.3.1/main.min.js"></script>
    <script src="https://unpkg.com/@fullcalendar/interaction@4.3.0/main.min.js"></script>
    <script src="https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.js"></script>
    <script src="https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.js"></script>
    <script>
    
    var id = document.getElementById("idProp").value;
    document.getElementById("resolution").value=screen.width;
    
    
    document.addEventListener('DOMContentLoaded', function() {
      if(screen.width <= 576){
      var screen2 = 'dayGridDay';
    }else{
      var screen2 = 'dayGridMonth';
    }
      var calendarEl = document.getElementById('calendar');

      var calendar = new FullCalendar.Calendar(calendarEl, {
              eventClick: function(info) {
              // alert('Event: ' + info.event.start);
              // alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
              // alert('View: ' + info.view.type);
              Swal.fire({
              title: '¿Estas seguro?',
              text: "Este evento no volverá",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, Eliminalo!'
            }).then((result) => {
              if (result.value) {
                  fetch('../controlador/eliminarEventos.php?id=' + info.event.id,{
                  })

                  .then(res=>res.text())

                  .then(data => {
                      console.log(data);
                      if(data == 2){
                        // location.reload();
                      }else{
                        console.log(data);
                      }
                      
                  })
                  

                  Swal.fire(
                    'Eliminado!',
                    'El evento fue eliminado',
                    'success'
                  )
                  .then((result) => {
                    location.reload()
                  })
              }
            })
             

    // change the border color just for fun
    info.el.style.borderColor = 'red';
  },
        locale: 'Es',
        plugins: [ 'interactionPlugin','interaction', 'dayGrid', 'timeGrid' ],
        defaultView: screen2,
        selectable: true,
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth , dayGridDay'
        },
        buttonText: {
          today: 'Hoy',
          month: 'Mes',
          day: 'Día'
        },
        events: [
        
        <?php
        $sql = "Select * from Calendar where fk_propiedad = $id";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            // echo ""
            while($row = $result->fetch_assoc()) {
              $title = $row["title"];
              $start = $row["start"];
              $end = $row["end"];
              $time = $row["time"];
              $id = $row["id_calendar"];
              echo "{'title' : '$title'";
                echo ", 'start' :  '".$start."T".$time."'";
                echo ", 'end' : '$end'";
                echo ", 'color': '#404040'";
                echo ", 'id': '$id'";
                echo ",'textColor':'white'";
                echo "},";
               
            }
          }else{
           
          }
        
        $conn->close();
          
                
                  ?>
      
      ],
      
      height: 550,
      contentHeight: 500
        
      });
      calendar.on('dateClick', function(info) {
          // console.log('clicked on ' + info.dateStr);
          // console.log(info.dateStr)
            Swal.fire({
            title: 'Agregar Evento',
            input: 'text',
            placeholder: 'Título',
            showCancelButton: true,
            inputValidator: (value) => {
              console.log(value);
              if (!value) {
                return 'You need to write something!'
              }else{
                var data = new FormData();
                data.append("title",value);
                data.append("start", info.dateStr);
                data.append("end", info.dateStr);
                data.append("time", "00:00");

                fetch('../controlador/guardarEventos.php?id=' + <?echo $_GET["id"] ?>,{
                  method: 'POST',
                  body: data
                })
                .then(res=>res.json())
                .then(datos=>{
                  location.reload();
                })

              }
            }
          })
      }).updateSize();
      calendar.render();
    });
    
      </script>
      <script>
      // fetch('../controlador/obtenerEvento.php')
      //   .then(res => res.json())
      //   .then(data => { 
      //     $('#calendar').fullCalendar({
      //       events: [
      //         {
      //           tittle: data.title,
      //           start: data.start
      //         }
      //       ]
      //     })

      //   })

      if(document.getElementById("time") == ""){
        document.getElementById("time").focus;
    }
      </script>

  </body>
</html>