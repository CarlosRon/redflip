    <?php
        include 'sidebarMenu.php';
    ?>

    <h3 class="h3-prop" style="padding:30px 0;">Propietarios</h3>

    <div class="container col-10"  style="padding-bottom:30px;">
        <div class="row">
            <div class="col-sm">
                <input type="text" id=search placeholder="🔍 Buscar . . .">
            </div>
            <div class="col-sm">
                <button type="submit" id= "btn_venta" class="btn btn-filt bg-redflip-red">Filtrar</button>
            </div>
            <div class="col-sm">
                <form action="../controlador/subirArchivo.php" method="POST" enctype="multipart/form-data">
                    <button type="button" id="submitExport" class="btn btn-filt bg-redflip-black" onclick="ExportExcel('xlsx')">
                        Exportar
                    </button>
                </form>
            </div>
        </div>
    </div>

    <section>
        <table class="table table-responsive table-hover table-striped">
            <thead class="thead-dark tab-center">
                <tr>
                    <th scope="col">Origen</th>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Estado</th>
                    <th scope="col" class="fa-1600">Fase</th>
                    <th scope="col">Rut</th>
                    <th scope="col">Fono</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Días</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tab-center">
                        <i class="far fa-file-alt" title="Formulario"></i>
                    </td>
                    <td>
                        <pre class="tabPre"><i class="fas fa-circle fa-xs ci-gre"></i><span>1233</span></pre>
                    </td>
                    <td>Jacqueline</td>
                    <td>Delgado</td>
                    <td>Prospecto</td>
                    <td>
                        <select class="btn btn-r dropdown-toggle" name="" id="">
                            <option value="1" selected="">No Contactado</option>
                            <option value="3">Contacto</option>
                            <option value="4">Primera Visita</option>
                            <option value="2">Tour Virtual</option>
                            <option value="6">Publicado</option>
                            <option value="5">Despublicado</option>
                            <option value="7">Finalizado</option>
                            <option value="8">Cancelado</option>
                        </select>
                    </td>
                    <td><pre class="tabPre">14.465.985-6</pre></td>
                    <td>+56992235580</td>
                    <td>j.delgadofonseca@gmail.com </td>
                    <td class="tab-center">0</td>
                    <td>
                        <!-- Default dropleft button -->
                        <div class="btn-group dropleft">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cog"></i>
                            </button>
                        <div class="dropdown-menu">
                            <!-- Dropdown menu links -->
                            <a class="dropdown-item" href="#"><i class="fas fa-edit"></i> Modificar</a>
                            <a class="dropdown-item" href="#"><i class="fas fa-user-slash"></i> Deshabilitar</a>
                            <a class="dropdown-item" href="#"><i class="fab fa-whatsapp"></i> WhatsApp</a>
                        </div>
                    </div>
                    </td>
                </tr>

                <tr class="tr-style">
                    <td class="tab-center"><i class="fas fa-ghost" title="Sin definir"></i></td>
                    <td><pre class="tabPre"><i class="fas fa-circle fa-xs ci-gre"></i> V1233-692</pre></td>
                    <td>Jacqueline</td>
                    <td>Delgado</td>
                    <td>Prospecto</td>
                    <td class="contacto">
                        <select class="btn btn-r dropdown-toggle" name="" id="">
                            <option value="1">No Contactado</option>
                            <option value="3" selected="">Contacto</option>
                            <option value="4">Primera Visita</option>
                            <option value="2">Tour Virtual</option>
                            <option value="6">Publicado</option>
                            <option value="5">Despublicado</option>
                            <option value="7">Finalizado</option>
                            <option value="8">Cancelado</option>
                        </select>
                        <input type="number" name="" id ="" class="btn-border">
                    </td>
                    <td><pre class="tabPre">20.052.158-7</pre></td>
                    <td>+56912345678</td>
                    <td>fuinimuneno@dokokawo.fumi</td>
                    <td class="tab-center">0</td>
                    <td>
                        <!-- Default dropleft button -->
                        <div class="btn-group dropleft">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cog"></i>
                            </button>
                            <div class="dropdown-menu">
                                <!-- Dropdown menu links -->
                                <a class="dropdown-item" href="#"><i class="fas fa-edit"></i> Modificar</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-user-slash"></i> Deshabilitar</a>
                                <a class="dropdown-item" href="#"><i class="fab fa-whatsapp"></i> WhatsApp</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
    <?php
        include 'footer.php';
    ?>
    </body>
</html>