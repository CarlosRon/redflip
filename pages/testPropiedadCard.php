<!doctype html>
<html ⚡4email>
 <head><meta charset="utf-8"><style amp4email-boilerplate>body{visibility:hidden}</style><script async src="https://cdn.ampproject.org/v0.js"></script> 
   
  <script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script><style amp-custom>
@media only screen and (max-width:600px) {.st-br { padding-left:10px; padding-right:10px } p, ul li, ol li, a { font-size:16px; line-height:150% } h1 { font-size:30px; text-align:center; line-height:120% } h2 { font-size:26px; text-align:center; line-height:120% } h3 { font-size:20px; text-align:center; line-height:120% } h1 a { font-size:30px; text-align:center } h2 a { font-size:26px; text-align:center } h3 a { font-size:20px; text-align:center } .es-menu td a { font-size:14px } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px } *[class="gmail-fix"] { display:none } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left } .es-m-txt-r amp-img { float:right } .es-m-txt-c amp-img { margin:0 auto } .es-m-txt-l amp-img { float:left } .es-button-border { display:block } a.es-button { font-size:16px; display:block; border-left-width:0px; border-right-width:0px } .es-btn-fw { border-width:10px 0px; text-align:center } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100% } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%; max-width:600px } .es-adapt-td { display:block; width:100% } .adapt-img { width:100%; height:auto } .es-m-p0 { padding:0px } .es-m-p0r { padding-right:0px } .es-m-p0l { padding-left:0px } .es-m-p0t { padding-top:0px } .es-m-p0b { padding-bottom:0 } .es-m-p20b { padding-bottom:20px } .es-mobile-hidden, .es-hidden { display:none } .es-desk-hidden { display:table-row; width:auto; overflow:visible; float:none; max-height:inherit; line-height:inherit } .es-desk-menu-hidden { display:table-cell } table.es-table-not-adapt, .esd-block-html table { width:auto } table.es-social { display:inline-block } table.es-social td { display:inline-block } }
.section-title {
	padding:5px 10px;
	background-color:#F6F6F6;
	border:1px solid #DFDFDF;
	outline:0;
}
a[x-apple-data-detectors] {
	color:inherit;
	text-decoration:none;
	font-size:inherit;
	font-family:inherit;
	font-weight:inherit;
	line-height:inherit;
}
.es-desk-hidden {
	display:none;
	float:left;
	overflow:hidden;
	width:0;
	max-height:0;
	line-height:0;
}
.es-button-border:hover {
	border-style:solid solid solid solid;
	background:#D6A700;
	border-color:#42D159 #42D159 #42D159 #42D159;
}
.es-button-border:hover a.es-button {
	background:#D6A700;
	border-color:#D6A700;
}
s {
	text-decoration:line-through;
}
body {
	width:100%;
	font-family:roboto, "helvetica neue", helvetica, arial, sans-serif;
}
table {
	border-collapse:collapse;
	border-spacing:0px;
}
table td, html, body, .es-wrapper {
	padding:0;
	Margin:0;
}
.es-content, .es-header, .es-footer {
	table-layout:fixed;
	width:100%;
}
p, hr {
	Margin:0;
}
h1, h2, h3, h4, h5 {
	Margin:0;
	line-height:120%;
	font-family:tahoma, verdana, segoe, sans-serif;
}
.es-left {
	float:left;
}
.es-right {
	float:right;
}
.es-p5 {
	padding:5px;
}
.es-p5t {
	padding-top:5px;
}
.es-p5b {
	padding-bottom:5px;
}
.es-p5l {
	padding-left:5px;
}
.es-p5r {
	padding-right:5px;
}
.es-p10 {
	padding:10px;
}
.es-p10t {
	padding-top:10px;
}
.es-p10b {
	padding-bottom:10px;
}
.es-p10l {
	padding-left:10px;
}
.es-p10r {
	padding-right:10px;
}
.es-p15 {
	padding:15px;
}
.es-p15t {
	padding-top:15px;
}
.es-p15b {
	padding-bottom:15px;
}
.es-p15l {
	padding-left:15px;
}
.es-p15r {
	padding-right:15px;
}
.es-p20 {
	padding:20px;
}
.es-p20t {
	padding-top:20px;
}
.es-p20b {
	padding-bottom:20px;
}
.es-p20l {
	padding-left:20px;
}
.es-p20r {
	padding-right:20px;
}
.es-p25 {
	padding:25px;
}
.es-p25t {
	padding-top:25px;
}
.es-p25b {
	padding-bottom:25px;
}
.es-p25l {
	padding-left:25px;
}
.es-p25r {
	padding-right:25px;
}
.es-p30 {
	padding:30px;
}
.es-p30t {
	padding-top:30px;
}
.es-p30b {
	padding-bottom:30px;
}
.es-p30l {
	padding-left:30px;
}
.es-p30r {
	padding-right:30px;
}
.es-p35 {
	padding:35px;
}
.es-p35t {
	padding-top:35px;
}
.es-p35b {
	padding-bottom:35px;
}
.es-p35l {
	padding-left:35px;
}
.es-p35r {
	padding-right:35px;
}
.es-p40 {
	padding:40px;
}
.es-p40t {
	padding-top:40px;
}
.es-p40b {
	padding-bottom:40px;
}
.es-p40l {
	padding-left:40px;
}
.es-p40r {
	padding-right:40px;
}
.es-menu td {
	border:0;
}
a {
	font-family:roboto, "helvetica neue", helvetica, arial, sans-serif;
	font-size:16px;
	text-decoration:underline;
}
h1 {
	font-size:30px;
	font-style:normal;
	font-weight:bold;
	color:#212121;
}
h1 a {
	font-size:30px;
}
h2 {
	font-size:24px;
	font-style:normal;
	font-weight:bold;
	color:#212121;
}
h2 a {
	font-size:24px;
}
h3 {
	font-size:20px;
	font-style:normal;
	font-weight:normal;
	color:#212121;
}
h3 a {
	font-size:20px;
}
p, ul li, ol li {
	font-size:16px;
	font-family:roboto, "helvetica neue", helvetica, arial, sans-serif;
	line-height:150%;
}
ul li, ol li {
	Margin-bottom:15px;
}
.es-menu td a {
	text-decoration:none;
	display:block;
}
.es-wrapper {
	width:100%;
	height:100%;
}
.es-wrapper-color {
	background-color:#F6F6F6;
}
.es-content-body {
	background-color:transparent;
}
.es-content-body p, .es-content-body ul li, .es-content-body ol li {
	color:#131313;
}
.es-content-body a {
	color:#2CB543;
}
.es-header {
	background-color:transparent;
}
.es-header-body {
	background-color:#FFFFFF;
}
.es-header-body p, .es-header-body ul li, .es-header-body ol li {
	color:#333333;
	font-size:14px;
}
.es-header-body a {
	color:#1376C8;
	font-size:14px;
}
.es-footer {
	background-color:#F6F6F6;
}
.es-footer-body {
	background-color:transparent;
}
.es-footer-body p, .es-footer-body ul li, .es-footer-body ol li {
	color:#131313;
	font-size:16px;
}
.es-footer-body a {
	color:#FFFFFF;
	font-size:16px;
}
.es-infoblock, .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li {
	line-height:120%;
	font-size:12px;
	color:#FFFFFF;
}
.es-infoblock a {
	font-size:12px;
	color:#FFFFFF;
}
a.es-button {
	border-style:solid;
	border-color:#FFC80A;
	border-width:10px 20px 10px 20px;
	display:inline-block;
	background:#FFC80A;
	border-radius:3px;
	font-size:18px;
	font-family:roboto, "helvetica neue", helvetica, arial, sans-serif;
	font-weight:normal;
	font-style:normal;
	line-height:120%;
	color:#FFFFFF;
	text-decoration:none;
	width:auto;
	text-align:center;
}
.es-button-border {
	border-style:solid solid solid solid;
	border-color:#2CB543 #2CB543 #2CB543 #2CB543;
	background:#FFC80A;
	border-width:0px 0px 0px 0px;
	display:inline-block;
	border-radius:3px;
	width:auto;
}
.es-p-default {
	padding-top:20px;
	padding-right:30px;
	padding-bottom:0px;
	padding-left:30px;
}
.es-p-all-default {
	padding:0px;
}
</style> 
 </head> 
 <body> 
  <div class="es-wrapper-color"> 
   <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0"> 
     <tr> 
      <td class="st-br" valign="top"> 
       <table cellpadding="0" cellspacing="0" class="es-header" align="center"> 
         <tr> 
          <td align="center" style="background-image:url(https://iiewdo.stripocdn.email/content/guids/CABINET_fc24a0f4d263de11268f11e745f048ef/images/20841560930387653.jpg);background-color: transparent; background-position: center bottom; background-repeat: no-repeat;" bgcolor="transparent"> 
           <div> 
            <table bgcolor="transparent" class="es-header-body" align="center" cellpadding="0" cellspacing="0" width="600" style="background-color: transparent;"> 
              <tr> 
               <td class="es-p20t es-p20r es-p20l" align="left"> 
                <table cellpadding="0" cellspacing="0" width="100%"> 
                  <tr> 
                   <td width="560" align="center" valign="top"> 
                    <table cellpadding="0" cellspacing="0" width="100%" role="presentation"> 
                      <tr> 
                       <td align="center" style="font-size: 0px;"><a target="_blank" href="https://redflip.com" class="rollover"><amp-img src="https://iiewdo.stripocdn.email/content/guids/CABINET_bab7ae6ccf468def71591e5440eaa069/images/44861590428129965.png" alt style="display: block; width: 246px; height: 78px;" class="rollover-first" width="246" height="78"></amp-img> 
                         <div style=""> 
                          <amp-img class="rollover-second" style="max-height: 0px; display: none;" src="https://iiewdo.stripocdn.email/content/guids/CABINET_bab7ae6ccf468def71591e5440eaa069/images/44861590428129965.png" width="246" height="78"></amp-img> 
                         </div></a></td> 
                      </tr> 
                      <tr> 
                       <td align="center" height="71"></td> 
                      </tr> 
                    </table></td> 
                  </tr> 
                </table></td> 
              </tr> 
            </table> 
           </div></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center"> 
         <tr> 
          <td align="center"> 
           <table bgcolor="transparent" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="background-color: transparent;"> 
             <tr> 
              <td class="es-p20r es-p20l" align="left" style="background-position: left bottom;"> 
               <table cellpadding="0" cellspacing="0" width="100%"> 
                 <tr> 
                  <td width="560" align="center" valign="top"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation"> 
                     <tr> 
                      <td align="center" height="40"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr> 
              <td align="left"> 
               <table cellpadding="0" cellspacing="0" width="100%"> 
                 <tr> 
                  <td width="600" align="center" valign="top"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation"> 
                     <tr> 
                      <td align="center" height="20" bgcolor="#fafafa"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr> 
              <td class="es-p20t es-p20b es-p30r es-p30l" align="left" style="background-position: left bottom; background-color: #fafafa;" bgcolor="#fafafa"> 
               <table cellpadding="0" cellspacing="0" width="100%"> 
                 <tr> 
                  <td width="540" align="center" valign="top"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation"> 
                     <tr> 
                      <td align="left" class="es-m-txt-c"><h2 style="text-align: center;">¡Crea un banner directamente en el email!</h2></td> 
                     </tr> 
                     <tr> 
                      <td align="center" class="es-m-txt-c"><p>Solo añade una imagen, texto, y el banner estará listo</p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr> 
              <td class="es-p20t es-p20b es-p30r es-p30l" align="left" style="background-position: left bottom; background-color: #fafafa;" bgcolor="#fafafa"> 
               <table cellpadding="0" cellspacing="0" width="100%"> 
                 <tr> 
                  <td width="540" align="left"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation"> 
                     <tr> 
                      <td align="center" style="position: relative;"><amp-img class="adapt-img" src="https://iiewdo.stripocdn.email/content/guids/bannerImgGuid/images/21561563981682563.png" alt title width="540" style="display: block;" height="540" layout="responsive"></amp-img></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr> 
              <td class="es-p20r es-p20l" align="left" style="background-color: #fafafa;" bgcolor="#fafafa"> 
               <table cellpadding="0" cellspacing="0" width="100%"> 
                 <tr> 
                  <td width="560" align="center" valign="top"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation"> 
                     <tr> 
                      <td align="center" class="es-p20" style="font-size:0"> 
                       <table border="0" width="100%" cellpadding="0" cellspacing="0" role="presentation"> 
                         <tr> 
                          <td style="border-bottom: 1px solid #cccccc; background:none; height:1px; width:100%; margin:0px 0px 0px 0px;"></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr class="es-visible-amp-html-only"> 
                      <td align="left"> 
                       <amp-accordion animate> 
                        <section> 
                         <h3 class="section-title">Sección 1</h3> 
                         <div> 
                          <table width="100%" cellspacing="0" cellpadding="0" role="presentation"> 
                           <tbody class="accordion"> 
                            <tr> 
                             <td align="left"><p>Texto</p></td> 
                            </tr> 
                            <tr> 
                             <td align="left"><p>Texto</p></td> 
                            </tr> 
                            <tr> 
                             <td align="left"><p>Texto</p></td> 
                            </tr> 
                          </table> 
                         </div> 
                        </section> 
                       </amp-accordion></td> 
                     </tr> 
                     <tr> 
                      <td align="center" class="es-p10"><span class="es-button-border"><a href="https://my.stripo.email/cabinet/" class="es-button" target="_blank">PRUÉBALO</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr> 
              <td class="es-p20r es-p20l" align="left" style="background-position: left bottom;"> 
               <table cellpadding="0" cellspacing="0" width="100%"> 
                 <tr> 
                  <td width="560" align="center" valign="top"> 
                   <table cellpadding="0" cellspacing="0" width="100%" style="background-position: left bottom;" role="presentation"> 
                     <tr> 
                      <td align="center" height="40"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center"> 
         <tr> 
          <td align="center" style="background-image:url(https://iiewdo.stripocdn.email/content/guids/CABINET_fc24a0f4d263de11268f11e745f048ef/images/31751560930679125.jpg);background-position: left bottom; background-repeat: no-repeat;"> 
           <table bgcolor="#31cb4b" class="es-footer-body" align="center" cellpadding="0" cellspacing="0" width="600"> 
             <tr> 
              <td class="es-p20t es-p20b es-p30r es-p30l" align="left" style="background-position: left top; background-color: #fafafa; border-radius: 10px 10px 0px 0px;" bgcolor="#fafafa"> 
               <table cellpadding="0" cellspacing="0" width="100%"> 
                 <tr> 
                  <td width="540" align="center" valign="top"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation"> 
                     <tr> 
                      <td align="left"><h2 style="text-align: center; line-height: 150%;">Recuerda crear un pie de página con tu información de contacto para mantenerte en contacto con tus suscriptores</h2></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr> 
              <td class="es-p10t es-p20b es-p30r es-p30l" style="background-position: left top; background-color: #fafafa;" align="left" bgcolor="#fafafa"> 
               <table width="100%" cellspacing="0" cellpadding="0"> 
                 <tr> 
                  <td width="540" valign="top" align="center"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation"> 
                     <tr> 
                      <td align="left" class="es-p10b"><h2>Redes sociales</h2></td> 
                     </tr> 
                     <tr> 
                      <td align="left" class="es-p5b"><p>Coloca íconos de redes sociales en el pie de página de tus emails para que los suscriptores puedan contactarte como sea más conveniente para ellos y para que te sigan en las redes sociales.</p></td> 
                     </tr> 
                     <tr> 
                      <td align="left"><p>Además, el marketing con email y redes sociales aumenta significativamente el CTR.</p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr> 
              <td class="es-p30t es-p30b es-p30r es-p30l" style="border-radius: 0px 0px 10px 10px; background-position: left top; background-color: #efefef;" align="left" bgcolor="#efefef"> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left"> 
                 <tr> 
                  <td class="es-m-p0r es-m-p20b" width="166" align="center"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation"> 
                     <tr> 
                      <td class="es-m-txt-c" align="left" style="font-size:0"> 
                       <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" role="presentation"> 
                         <tr> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Facebook" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/circle-colored/facebook-circle-colored.png" alt="Fb" width="32" height="32"></amp-img></td> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Twitter" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/circle-colored/twitter-circle-colored.png" alt="Tw" width="32" height="32"></amp-img></td> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Instagram" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/circle-colored/instagram-circle-colored.png" alt="Inst" width="32" height="32"></amp-img></td> 
                          <td valign="top" align="center"><amp-img title="Youtube" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/circle-colored/youtube-circle-colored.png" alt="Yt" width="32" height="32"></amp-img></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                  <td class="es-hidden" width="20"></td> 
                 </tr> 
               </table> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left"> 
                 <tr> 
                  <td class="es-m-p20b" width="165" align="center"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation"> 
                     <tr> 
                      <td class="es-m-txt-c" align="left" style="font-size:0"> 
                       <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" role="presentation"> 
                         <tr> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Facebook" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/rounded-gray-bordered/facebook-rounded-gray-bordered.png" alt="Fb" width="32" height="32"></amp-img></td> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Twitter" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/rounded-gray-bordered/twitter-rounded-gray-bordered.png" alt="Tw" width="32" height="32"></amp-img></td> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Instagram" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/rounded-gray-bordered/instagram-rounded-gray-bordered.png" alt="Inst" width="32" height="32"></amp-img></td> 
                          <td valign="top" align="center"><amp-img title="Youtube" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/rounded-gray-bordered/youtube-rounded-gray-bordered.png" alt="Yt" width="32" height="32"></amp-img></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <table class="es-right" cellspacing="0" cellpadding="0" align="right"> 
                 <tr> 
                  <td width="169" align="center"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation"> 
                     <tr> 
                      <td class="es-m-txt-c" align="left" style="font-size:0"> 
                       <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" role="presentation"> 
                         <tr> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Facebook" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/logo-black/facebook-logo-black.png" alt="Fb" width="32" height="32"></amp-img></td> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Twitter" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/logo-black/twitter-logo-black.png" alt="Tw" width="32" height="32"></amp-img></td> 
                          <td class="es-p10r" valign="top" align="center"><amp-img title="Instagram" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/logo-black/instagram-logo-black.png" alt="Inst" width="32" height="32"></amp-img></td> 
                          <td valign="top" align="center"><amp-img title="Youtube" src="https://iiewdo.stripocdn.email/content/assets/img/social-icons/logo-black/youtube-logo-black.png" alt="Yt" width="32" height="32"></amp-img></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr> 
              <td align="left" style="background-position: left top;"> 
               <table cellpadding="0" cellspacing="0" width="100%"> 
                 <tr> 
                  <td width="600" align="center" valign="top"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation"> 
                     <tr> 
                      <td align="center" height="40"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table></td> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>