
<?php
include '../sidebarMenu.php';
?>
<div class="container pt-3  animated fadeIn">
<form action="../../controlador/agregarUsuario.php" method="post">

  <div class="row  border pt-2">
    <div class="col">
      <!-- Level 1: .col-sm-9 -->
      <div class="row">
        <div class="col text-center">
            <h3>Creación de usuarios</h3>
        </div>        
      </div>
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Nombre</span>
                </div>
                <input type="text" class="form-control" name="nombre" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Apellido</span>
                </div>
                <input type="text" class="form-control" name="apellido" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg">
        <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Rut</span>
                </div>
                <input type="text" class="form-control" name="rut" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-8 col-sm-6 ">
          <!-- Level 2: .col-8 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Teléfono</span>
                </div>
                <input type="text" class="form-control" name="telefono" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
        <div class="col-4 col-sm-6">
          <!-- Level 2: .col-4 .col-sm-6 -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Correo</span>
                </div>
                <input type="text" class="form-control" name="correo" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Rol</label>
                </div>
                <select class="custom-select" id="inputGroupSelect01" name="rol">
                    <option value="" selected disabled>Seleccione una opción</option>
                    <option value="1">Corredor</option>
                    <option value="2">Administrador</option>
                </select>
            </div>
        </div>
        <div class="col">
        <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Estado</label>
                </div>
                <select class="custom-select" id="inputGroupSelect01" name="estado">
                    <option value="" selected disabled>Seleccione una opción</option>
                    <option value="1">Activo</option>
                    <option value="2">Desactivado</option>
                </select>
            </div>
        </div>
        
      </div>
      <div class="row">
      <div class="col text-center">
        <button type="submit" class="btn btn-outline-primary center" >Guardar</button>
      </div>
      </div>
    </div>
  </div>
  </form>
</div>