<? 
    $busqueda =strtolower($_REQUEST["search"]);
    echo "<script>console.log(".utf8_encode($busqueda).")</script>";
    if(empty($busqueda)){
        header("location: interesadosGlobales.php");
    }else{
        $sqlInt = "SELECT distinct
        Persona.nombre,
        Persona.apellido,
        Persona.telefono,
        Persona.correo,
        Interesado.fk_corredor,
        Perfil_busqueda.orientacion
    FROM
        Persona,
        usuario,
        Interesado,
        Corredor,
        Perfil_busqueda
    WHERE
        usuario.fk_persona = Persona.id_persona 
        AND Interesado.fk_persona = Persona.id_persona 
        AND Interesado.fk_corredor = Corredor.id_corredor 
        AND Perfil_busqueda.fk_interesado = Interesado.id_interesado 
        AND usuario.fk_rol = 4
        AND ( Interesado.fk_corredor = (SELECT Corredor.id_corredor FROM Corredor, Persona
                                                    WHERE Persona.id_persona = Corredor.fk_persona 
                                                    AND (Persona.nombre LIKE '$busqueda%' OR Persona.apellido LIKE '$busqueda%'))
                OR (	Persona.nombre LIKE '$busqueda%' OR
                Persona.apellido LIKE '$busqueda%' OR
                Persona.telefono LIKE '$busqueda%' OR
                Persona.correo LIKE '$busqueda%'))";
    }

$busquedaArray =explode(" " , $busqueda);
include 'sidebarMenu.php';
include "../conexion.php";
$sql2 = "Update usuario SET ultima_pag='https://indev9.com/redflip/pages/interesadosGlobales.php' where id_usuario =". $_SESSION['id'];

if ($conn->query($sql2) === TRUE) {

}


?>

    <section>
        <div class="container col-11">
            <div class="row">
                <div class="column col-12">
                    <h3 class="h3-prop mtb-50">Interesados Globales</h3>
                </div>
            </div>

            <div class="row" style="margin:30px 0;">
                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-6">
                    <form id="filtro" action="buscarInteresadosGlobales.php" method="get">
                        <div class="column col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input type="text" id="search" name="search" placeholder="🔍 Buscar . . .">
                        </div>
                    </form>
                </div>

                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-6" style="text-align:right;">
                    <form action="../controlador/subirArchivo.php" method="POST" enctype="multipart/form-data">
                        <button type="button" id="submitExport" class="btn btn-Imex bg-redflip-black pad-0" style="width:30%; padding:5px;" onclick="ExportExcel('xlsx')">
                            Exportar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    
    <div id="">
    <table class='table table-responsive-md table-hover table-striped' id='interesadosGlobales'>
        <thead class='thead-dark'>
            <tr>
                <th class='' scope='col'> # </th>
                <th class='' scope='col'> Nombre </th>
                <th class='' scope='col'> Teléfono </th>
                <th class='' scope='col'> Corredor </th>
                <th class='' scope='col'> Correo </th>
                <th class='' scope='col'> Acciones </th>	
            <tr>
        </thead>
        <tbody>
            <?php
                $result = $conn->query($sqlInt);

                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            $nomInt = $row["nombre"];
                            $apeInt = $row["apellido"];
                            $telInt = $row["telefono"];
                            $correoInt = $row["correo"];
                            $comuna = $row["comuna"];
                            $orientacion = $row["orientacion"];
                            $id = $row["id_interesado"];
                            
                            $sqlCorredor = "SELECT * from Corredor, Persona where Corredor.fk_persona = Persona.id_persona and Corredor.id_corredor = " . $row["fk_corredor"];
                            $result2 = $conn->query($sqlCorredor);

                            if ($result2->num_rows > 0) {
                                while($row = $result2->fetch_assoc()) {
                                echo "<tr>
                                        <td>".$id."</td>
                                        <td>".$nomInt. " " .$apeInt . "</td>
                                        <td>".$telInt."</td>
                                        <td>".$row["nombre"]. " ". $row["apellido"]."</td>
                                        <td>".$correoInt."</td>
                                        <td class=''>
                                            <div class='btn-group dropleft'>
                                                <button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                    <i class='fas fa-cog'></i>
                                                </button>
                                                <div class='dropdown-menu'>
                                                    <a class='dropdown-item' href='editarProp.php?id='><i class='fas fa-edit'></i> Modificar</a>
                                                    <a class='dropdown-item' href='../controlador/eliminarInt' ><i class='fas fa-user-slash'></i> Eliminar</a>
                                                    <a class='dropdown-item' href='https://api.whatsapp.com/send?phone=$telInt&text=Hola%20quiero%20info' target='_blank'><i class='fab fa-whatsapp'></i> Whatsapp</a>
                                                </div>
                                            </div>
                                        </td> 
                                    </tr>";
                                }
                            }
                        
                        }
                    } else {
                        echo "<tr>
                                <td class= 'tab-center' colspan=8 > no hay datos</td>
                            </tr>";
                    }
            ?>
            </tbody>
        </table>
    </div>
    </section>


    <form action="process.php" method="post" target="_blank" id="formExport">

        <input type="hidden" id="data_to_send" name="data_to_send" />

        <input type="hidden" id="nombre" name="nombre" value="Propietarios Activos" />

    </form>

    <?
    if(isset($_GET["msg"])){
    ?>

    <input type="hidden" id = "msg" value= <?echo $_GET["msg"] ?>  >

    <?  
        }else
        {
    ?>

    <input type="hidden" id = "msg" value=""  >

    <?
    }
        if(isset($_GET["nom"])){
    ?>

     <input type="hidden" id = "nom" value= <?echo $_GET["nom"] ?>  >

    <?   
    }
        if(isset($_GET["cont"])){
    ?>

     <input type="hidden" id = "cont" value= <?echo $_GET["cont"] ?>  >   

    <?
        }else{
    ?>

    <input type="hidden" id = "cont" value= ""  > 

    <?
    }
    include 'footer.php';
    ?>

    <script src="../js/dist/sweetalert2.all.min.js"></script>
<!-- <script src="../js/selectFase.js"></script> -->
<!-- <script src="../js/buscarInteresados.js"></script> -->
<script src="../js/toastr.js"></script>
<!-- <script src="../js/accionProp.js"></script> -->
<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>

<script>
    // document.getElementById('submitExport').addEventListener('click', function(e) {
    //     e.preventDefault();
    //     let export_to_excel = document.getElementById('export_to_excel');
    //     let data_to_send = document.getElementById('data_to_send');
    //     data_to_send.value = export_to_excel.outerHTML;
    //     document.getElementById('formExport').submit();
    // });
    
    var msg = document.getElementById("msg").value;
        var cont = document.getElementById("cont").value;
        if(msg === "1"){
        var nom = document.getElementById("nom").value;
            alert("El archivo: " + nom + " se ha subido correctamnte")
        }else if (msg === "2"){
            alert("El archivo no es un excel")
        }
        if(cont === ""){
            
        }else{
            alert("se agregaron " + cont + " propietarios");
        }
</script>



<script type="text/javascript">
    function ExportExcel(type, fn, dl) {
    var elt = document.getElementById('export_to_excel');
    var wb = XLSX.utils.table_to_book(elt, {sheet:"Sheet JS"});
    return dl ?
        XLSX.write(wb, {bookType:type, bookSST:true, type: 'base64'}) :
        XLSX.writeFile(wb, fn || ('Propietarios.' + (type || 'xlsx')));
    }
</script>

<script>
    $('tr').click(function (event) {
        $('tr').removeClass('tr-focus'); //Remove focus from other TDs
    $(this).addClass('tr-focus');
    console.log("asdasd");
    });
</script>

</body>
</html>