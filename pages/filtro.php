<?php

include '../conexion.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="../css/all.min.css">
    <title>filtro</title>

    <style>
        .content-filtro{
            background-color: rgb(245, 245, 245);
            width:100%;
            margin:0 auto;
            padding: 20px 0;
            text-align: center;
            transform: translateY(10%);
            margin-top: -40px;
        }
        select, input{
            width:24%;
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 3px;
            margin-bottom: 10px;
            box-sizing: border-box;
            color: #2C3E50;
            font-size: 16px;
            font-family: 'futura-pt',sans-serif;
        }
        input[type="number"]{
            -webkit-appearance: none;
        }
        select.mr-7{
            margin-right:73%;
        }
        .mlFiltro{
            background-color: #E8505B; 
            color:#fff;
            float: right;
            margin-right: 13px;
        }

        .mrLimpiar{
            background-color: #27AE60; 
            color:#fff;
            color: #fff;
            float: left;
            margin-left: 13px;
        }

        .group{
            height:50px;
        }
        .fa-times{
            color:#999999;
            float: right;
            margin-right: 15px;
        }
        @media (max-width:768px){
            .content-filtro{
                width:95%;
            }
            select, input{
                width:30%;
            }
            select.mr-7{
                margin-right:61%;
            }
        }

        @media (max-width:576px){
            select, input{
                font-size: 10px;
            }
        }
    </style>
</head>
<body>
    <section class="content-filtro">
    <!-- <i class="fas fa-times fa-lg"></i> -->

        <h2 style="font-family: sans-serif;" >Filtros</h2>
        <div class="container">
        <div class="row">
            <div class="column">

                <select name="operacion" id="operacion">
                    <option value="" selected>Tipo de operación</option>
                    <option value="">Todos</option>
                    <?php
                    $sqlOp = "select * from Venta_Arriendo where id < 4";
                    $result = $conn->query($sqlOp);

                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            echo "<option value=".$row['descripcion'].">".utf8_encode($row["descripcion"])."</option>";
                        }
                    } else {
                        echo "<script>console.log('$sqlCom')</script>";
                    }
                    ?>
                </select>

                <select name="tipo_prop" id="tipo_prop">
                    <option value=""  selected>Tipo de propiedad</option>
                    <?php
                    $sqlTipoProp = "Select * from tipo_propiedad";
                    $result = $conn->query($sqlTipoProp);

                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            echo "<option value=".$row['id_tipo_propiedad'].">".utf8_encode($row["tipo"])."</option>";
                        }
                    } else {
                        echo "<script>console.log('$sqlCom')</script>";
                    }
                    ?>
                </select>

                <select name="comuna" id="comuna">
                    <option value = "" selected>Comuna</option>
                    <?php
                    $sqlCom = "Select * from Comuna where id not in (53)";
                    $result = $conn->query($sqlCom);

                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            echo "<option value=".$row['id'].">".utf8_encode($row["nombre"])."</option>";
                        }
                    } else {
                        echo "<script>console.log('$sqlCom')</script>";
                    }
                    ?>
                </select>

                <select name="origen" id="origen">
                    <option value="" selected >Origen</option>
                    <?php
                        $sqlOri = "Select * from Origen";
                        $result = $conn->query($sqlOri);

                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            echo "<option value=".$row['id_origen'].">".utf8_encode($row["nom_origen"])."</option>";
                        }
                    } else {
                       
                    }
                    ?>
                </select>

                <input type="number" name="habitacionesMin" id="habitacionesMin" placeholder="Habitaciones mínimas">
                <input type="number" name="habitacionesMax" id="habitacionesMax" placeholder="Habitaciones máximas">
                
                <input type="number" name="bannosMin" id="bannosMin" placeholder="Baños mínimo">
                <input type="number" name="bannosMax" id="bannosMax" placeholder="Baños máximo">

                <input type="number" name="precioMin" id="precioMin" placeholder="Precio mínimo">
                <input type="number" name="precioMax" id="precioMax" placeholder="Precio máximo">

                <input type="number" name="sUtilMin" id="sUtilMin" placeholder="Superficie útil mínima">
                <input type="number" name="sUtilMax" id="sUtilMax" placeholder="Superficie útil máxima">

                <select name="divisa" id="divisa">
                    <option value="" selected disabled>Divisa</option>
                    <option value="1">UF</option>
                    <option value="2">CLP</option>
                </select>

                <select name="orden" id="orden">
                    <option value=""  selected>Orden</option>
                    <!-- <option value="">Recientes</option>
                    <option value="">Antiguos</option> -->
                    <option value="1">precio mayor a menor CLP</option>
                    <option value="2">precio mayor a menor UF</option>
                    <option value="3">precio menor a mayor CLP</option>
                    <option value="4">precio menor a mayor UF</option>
                    <option value="5">Habitaciones mayor a menor</option>
                    <option value="6">Habitaciones menor a mayor</option>
                    <option value="7">Baños mayor a menor</option>
                    <option value="8">Baños menor a mayor</option>
                </select>

                <select name="estado" id="estado">
                    <option value="" selected >Estado</option>
                    <option value="" >Todos</option>
                    <option value="prospecto" >Prospecto</option>
                    <option value="propietario" >Propietario</option>
                    
                </select>
                <select name="fase" id="fase">
                    <option value="">Fase</option>
                </select>
                <select name="importancia" id="importancia">
                    <option value="" selected>Importancia</option>
                    <option id=""  value="">Todos</option>
                    <option id=""  value="Alta">Alta</option>
                    <option id=""  value="Media">Media</option>
                    <option id=""  value="Baja">Baja</option>
                </select>
            </div>
                    <div id="datos2"></div>
            <div class="group row">
            <!-- <input type="button" class="mrLimpiar" value="Limpiar"> -->
            <input type="button" class="mlFiltro" value="Filtrar" onclick="filtrar()">
            </div>
        </div>
        </div>
    </section>
    <script>
        function filtrar(){
            var datos = window.opener.document.getElementById("datos"); //esto funciona!
            var operacion = document.getElementById("operacion").value;
            var estado = document.getElementById("estado").value;
            var fase = document.getElementById("fase").value;
            var importancia = document.getElementById("importancia").value;
            var comuna = document.getElementById("comuna").value;
            var tipo_prop = document.getElementById("tipo_prop").value;
            var habitacionesMin = document.getElementById("habitacionesMin").value;
            var habitacionesMax = document.getElementById("habitacionesMax").value;
            var bannosMax = document.getElementById("bannosMax").value;
            var bannosMin = document.getElementById("bannosMin").value;
            var precioMin = document.getElementById("precioMin").value;
            var precioMax = document.getElementById("precioMax").value;
            var orden = document.getElementById("orden").value;
            var origen = document.getElementById("origen").value;
            var sUtilMin = document.getElementById("sUtilMin").value;
            var sUtilMax = document.getElementById("sUtilMax").value;
            var divisa = document.getElementById("divisa").value;

            buscar_filtros(operacion,estado,fase,importancia,comuna,tipo_prop, sUtilMax, sUtilMin, habitacionesMin, habitacionesMax, bannosMax, bannosMin,precioMin,precioMax , orden,  origen, status, divisa, "popup");
        }
        function buscar_filtros(operacion,estado, fase, importancia, comuna,tipo_prop, sUtilMax, sUtilMin, habitacionesMin, habitacionesMax, bannosMax, bannosMin,precioMin,precioMax, orden, origen, status, divisa, filtro){
            var datos = window.opener.document.getElementById("datos")
            $.ajax({
                url: '../services/buscar.php',
                type: 'POST',
                dataType: 'html',
                data: {operacion:operacion, estado:estado, fase:fase, importancia:importancia,
                     comuna:comuna, tipo_prop:tipo_prop, sUtilMin:sUtilMin, sUtilMax:sUtilMax, habitacionesMin:habitacionesMin, habitacionesMax:habitacionesMax, bannosMin:bannosMin, bannosMax:bannosMax, precioMin:precioMin, precioMax:precioMax, orden:orden, origen:origen, status:status, divisa:divisa, filtro:filtro}
            })
            .done(function(respuesta){
                datos.innerHTML=respuesta;
                opener.tabla();
                // return respuesta;
                window.close();
            })
            .fail(function(){
                console.log("error");
            });
            // return "oli";
        }
    
    </script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
    <script src="../js/all.min.js"></script>
    <!-- <script src="../js/menu.js"></script> -->
    <!-- <script src="../js/notificacion.js"></script> -->
    <script src="../js/selectFase.js"></script>
    <!-- <script src="../js/buscar.js"></script> -->
    <script>
    function tabla() {
        console.log("asd");
        window.opener.document.getElementsByTagName('th').addEventListener('onclick', function(e){
        console.log("object");
        var table = $(this).parents('table').eq(0)
        var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
        this.asc = !this.asc
        if (!this.asc) {
            rows = rows.reverse()
        }
        for (var i = 0; i < rows.length; i++) {
            table.append(rows[i])
        }
        setIcon($(this), this.asc);
    })


    function comparer(index) {
        return function(a, b) {
            var valA = getCellValue(a, index),
                valB = getCellValue(b, index)
            return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
        }
    }

    function getCellValue(row, index) {
        return $(row).children('td').eq(index).html()
    }

    function setIcon(element, asc) {
        $("th").each(function(index) {
            $(this).removeClass("sorting");
            $(this).removeClass("asc");
            $(this).removeClass("desc");
        });
        element.addClass("sorting");
        if (asc) element.addClass("asc");
        else element.addClass("desc");
    }

}
    </script>
</body>
</html>