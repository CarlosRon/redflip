<?php
    include 'sidebarMenu.php';
    include '../conexion.php';
    include '../pages/valid_session.php';
?>
<section>
    <div class="container">
        <div class="row">
            <div class="column col-12">
                <h3 class="h3-prop mtb-50">Formulario Enviados</h3>
            </div>
            <div class="column col-12">
                <div class="column col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <input type="text" id=search placeholder="🔍 Buscar . . .">
                </div>
            </div>
        </div>
    </div>
        
    <div class="container">
        <div class="row">
            <div class="column col-12">
                <ol class="rectangle-list" >
                        <?php
                        $sql = "SELECT * from formulario, Propiedad where formulario.enviado = 1 and formulario.aprobado = 0 and usuario = ".$_SESSION['id']." AND formulario.id_formulario = Propiedad.fk_formulario AND formulario.usuario = ".$_SESSION['id']." order by id_formulario";
                        $result = $conn->query($sql);
                        
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                $nombreForm = utf8_encode($row["nombre_form"]);
                                $nombreForm = stripslashes($nombreForm);
                                echo '  <li>
                                            <label  class="listForm" onclick="return false;"><label disabled class="overflow" title="'.$nombreForm.'">'.$nombreForm.'</label>';                                               
                                               echo '<button class="btn-acciones bg-redflip-form min-p fr-l" title="Ver formulario" onclick="redirigir('.$row["id_formulario"].')"><i class="far fa-file-alt"></i></button>
                                            </label>
                                        </li>';
                            }
                        } else {
                            echo "No tiene Formularios :(";
                            // echo $sql;
                        }
                        $conn->close();
                        ?>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</section>

<?php
    include 'footer.php';
?>

<script>
    function editarForm (idForm){
        console.log(idForm);
        window.location=`editarFormulario.php?id=${idForm}`;
    }
    function eliminarForm(idForm){
        fetch(`../controlador/eliminarForm.php?idForm=${idForm}`)
        .then(res=>res.json())
        .then(data=>{
            console.log(data)
            location.reload();
        })
    }
    function redirigir(id){
        // window.open('fichaForm.php?id='+id, '');
        window.open("fichaForm.php?id="+id+"&origen=2",'name','height=800,width=700 center');
    }
</script>
</body>
</html>