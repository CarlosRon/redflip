Dada Mail Installation/Configuration - Command Line Interface
    Dada Mail is installed/configured using the included installer, either
    using the installer's web-based interface, or the command line
    interface.

    The web-based interface works great if you are setting up and running
    Dada Mail in a CGI environment. Otherwise, you probably will want to use
    the command line interface.

Installing Dada Mail via the CLI
  Download Dada Mail
    The latest version of Dada Mail will be listed on:

    <http://dadamailproject.com/download>

    if you are a Pro Dada subscription, you may download the most recent
    version of Pro Dada at:

    <http://dadamailproject.com/pro_dada>

    Copy the distribution to the directory you would like to install Dada
    Mail under.

  Uncompress the Distribution
    Uncompress Dada Mail - this will create a new directory named "dada".
    Change into this directory:

        gunzip dada-11_4_4.tar.gz
        tar -xvf dada-11_4_4.tar
            cd dada

    In the "dada" directory, rename the, "installer-disabled" directory to
    simply, "install" and change into this directory. Once in the
    "installer" directory, change the permissions of the "install.cgi"
    script to, 755:

        mv installer-disabled installer
        cd installer
        chmod 755 install.cgi

    You're now ready to run the "install.cgi" install/configuration script.

    To make sure permissions are set correctly, we suggest running the
    "install.cgi" script under the same owner as will run Dada Mail (or
    change the ownership of the files at a later on - just don't forget!)

Introduction to the Installation/Configuration Process
    The "install.cgi" script takes care of the following jobs:

    *   Creates the .dada_files directory structure

        Dada Mail uses this directory structure to save it's configuration
        file, and keep its temporary files, among other things. If you use
        SQLite for your backend, the SQLite database will be saved here, as
        well.

    *   Writes the .dada_config configuration file.

        Dada Mail's global configuration file is called, ".dada_config" and
        is itself written in Perl. Any global changes you would like to make
        to Dada Mail should be made in this file. The global defaults are
        located in "dada/DADA/Config.pm", it is not suggested that you make
        changes to this file.

    *   Copies over static files

        Dada Mail has a collection of static files that need to be copied
        over.

        Static files include images, stylesheets and javascript files - some
        of these need extra configuration. Dada Mail keeps all of these
        files in a directory called "dada_mail_support_files". The
        collection of these files will be copied from the, "dada/static"
        directory of the distribution.

    *   Verifies configuration information

        The installer will try to make sure that the information you submit
        is correct - files can be written where you ask them to be written,
        the database connection works, etc.

    The "install.cgi" script does not take care of any web server
    configuration. If you run Dada Mail under PSGI/Plack, you will need to
    configure your webserver (nginx, most likely) to run Dada Mail, once you
    start the PSGI process.

    More on running Dada Mail under PSGI/Plack:

    <http://dadamailproject.com/d/features-psgi_support.pod.html>

Required Paramaters
    The following parameters are required, if you're installing Dada Mail.
    If you are upgrading Dada Mail, there are separate options to make
    things easier:

  --dada_files_loc
    Dada Mail needs a certain directory structure to keep its configuration
    files, temporary files, etc in. The top directory will be called,
    ".dada_files" and will need to be readable/writable to whatever process
    runs Dada Mail.

    Set "--dada_files_loc" to the parent directory you would like the
    .dada_files directory to be created in.

  --program_url
    The URL that will be used to access Dada Mail.

  --dada_root_pass
    The Dada Mail Root Password you'd like to use

  --support_files_dir_path --support_files_dir_url
    Dada Mail keeps its static files in a directory called,
    "dada_mail_support_files" (images/javascripts,etc). Set the parent
    directory this should be created in using these two parameters.

    "--support_files_dir_path" should contain the absolute server path to
    this directory

    "--support_files_dir_url" should contain the URL that maps to this
    directory

  --backend
    The backend you'd like to use. Can be set to use any of the following:

    *   mysql

        Uses a MySQL database

    *   Pg

        Uses a PostgreSQL database

    *   SQLite

        Uses a SQLite database - good for testing/development, but most
        likely not something you want to use for production use.

    If you set, "--backend" to either, "mysql", or, "Pg", you will also have
    to pass the following parameters:

    *   --sql_server

        The SQL server

    *   --sql_port

        SQL port - can be set to, "auto" to choose the default port for
        mysql and Pg

    *   --sql_database

        The name of your SQL database

    *   --sql_username

        Your SQL username

    *   --sql_password

        Your SQL password

Optional Paramaters
  --deployment_running_under
    Dada Mail defaults to, "CGI". Supported are the following:

    *   CGI

        When running Dada Mail as a CGI scripts using something like Apache
        and mod_cgi, most likely in a shared hosting environment. This is
        the default configuration, so you will not need to pass this
        parameter if you would like to run under CGI.

    *   FastCGI

        This will set up Dada Mail to run as a FastCGI script using
        CGI::Fast. This would be useful if you're running under Apache, but
        would like to run Dada Mail as a FastCGI script using mod_fastcgi or
        mod_fcgi.

        If you want to run Dada Mail under a reverse proxy, we'd suggest
        instead using the PSGI deployment option instead

    *   PSGI

        Use this option to run Dada Mail under PSGI/Plack. Doing so will set
        up Dada Mail's own "app.psgi" script inside its "dada" directory,
        which you can then start up and utilize however you would like using
        the wonders of PSGI:

            sudo plackup --port 9090 --host 127.0.0.1

  --install_plugins
    Takes a comma separated (no space between the comma) list of
    plugin/extension names.

    The following plugins/extensions are supported:

    *   mailing_monitor

    *   change_root_password

    *   screen_cache

    *   log_viewer

    *   tracker

    *   bridge

    *   bounce_handler

    *   change_list_shortname

    *   password_protect_directories

    *   multiple_subscribe

    *   blog_index

    Example:

            --install_plugins mailing_monitor,change_root_password,screen_cache

    This also works:

            --install_plugins mailing_monitor --install_plugins change_root_password --install_plugins screen_cache

  --install_wysiwyg_editors
    You may pass one of the following (or both), to setup and install the
    following WYSIWYG editors:

    *   ckeditor

    *   tiny_mce

    Example:

        --install_wysiwyg_editors ckeditor,tiny_mce

  --install_file_browser
    pass either, "kcfinder" to install the KCFinder File Browser, or pass,
    "core5_filemanager" to install the Core5 Filemanager.

    Unlike the, "--install_wysiwyg_editors" option, "--install_file_browser"
    (and Dada Mail) only supports installing one file browser/manager at a
    time.

  Amazon SES
    Dada Mail supports Amazon SES to send messages out, sending verification
    email messages and to find out your sending stats and limitations. More
    information on Amazon SES support:

    <http://dadamailproject.com/d/features-amazon_ses_support.pod.html>

    To enable Amazon SES support you will need to pass your "AWSAccessKeyId"
    and "AWSSecretKey".

    *   --amazon_ses_AWSAccessKeyId

        Your AWSAccessKeyId

    *   --amazon_ses_AWSSecretKey

        Your AWSSecretKey

    *   --amazon_ses_AWS_endpoint

        Optionally, you may set the AWS Endpoint. Available endpoints are
        listed here:

        <http://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html>

        "us-east" is used by default - use the full URL listed when setting
        the region

    *   --amazon_ses_Allowed_Sending_Quota_Percentage

        Dada Mail can be configured to use all, or only part of your total
        Amazon SES quota. Setting this paramater will allow you to set that
        amount. For example, to allow Dada Mail to use only 50% of your
        total SES sending quota, set this paramater to the following:

            --amazon_ses_Allowed_Sending_Quota_Percentage 50

Examples
    An example of invoking the installer with the above parameters:

        ./install.cgi --program_url http://example.com/cgi-bin/dada/mail.cgi \
        --dada_root_pass secret \
        --dada_files_loc /home/example \
        --support_files_dir_path /home/example/public_html \
        --support_files_dir_url http://example.com \
        --backend mysql \
        --sql_server localhost \
        --sql_port 3306 \
        --sql_database dada_mail \
        --sql_username username \
        --sql_password password \
        --install_plugins mailing_monitor \
        --install_plugins change_root_password \
        --install_plugins screen_cache \
        --install_plugins log_viewer \
        --install_plugins tracker \
        --install_plugins multiple_subscribe \
        --install_plugins blog_index \
        --install_plugins change_list_shortname \
        --install_wysiwyg_editors ckeditor \
        --wysiwyg_editor_install_ckeditor \
        --wysiwyg_editor_install_tiny_mce \
        --file_browser kcfinder

    Since the list of options is so long, it may make sense to put the
    entire command w/options in its own file, so that you can resuse your
    own recipe for installing Dada Mail in multiple locations.

Upgrading
    If you are using the installer to upgrade from an install of dada mail
    you've installed beforeusing the installer, you may simply pass the,

            --dada_files_loc

    flag, as well as the,

            --upgrading

    parameter, and the installer will install Dada Mail, based on the
    previous values in your global configuration.

    You may then pass *additional* parameters, to augment the previous
    configuration.

    For example:

            ./install.cgi --dada_files_loc /home/account --upgrading

    Will upgrade Dada Mail, pulling the previous global config values,

            ./install.cgi --dada_files_loc /home/account --upgrading \
        --install_wysiwyg_editors ckeditor,tiny_mce \
            --install_plugins mailing_monitor,change_root_password,screen_cache

    Will upgrade Dada Mail, pulling the previous global config values, as
    well as making sure CKeditor and TinyMCE is installed, and the three
    plugins listed are also installed.

    You CANNOT use the, "--dada_files_dir_setup" parameter with the,
    "--upgrading" parameter. You must explicitly set the,
    "--dada_files_loc".

    If performing an partial upgrade or doing some of your installation
    manually, you can tell Dada Mail to skip some of the tests it would
    usually preform, before configuring Dada Mail:

    *   --dada_files_dir_setup

        If you pass this parameter with a value of, "auto", you do not need
        to pass the, "--dada_files_loc" parameter provided that you're
        certain that the directory it automatically discovers to create the,
        ".dada_files" directory structure will be correct. The installer
        usually guesses the home directory.

    *   --skip_configure_SQL

        By default, the installer will make sure the database you pass in,
        "--sql_database" doesn't already have the tables required for a Dada
        Mail install. If you've already set up these tables (for example, in
        a previous version of Dada Mail) pass this parameter with a value
        of, "1" to simply use the sql_* parameters you're passing, without
        attempting to set up the database, itself.

    *   --if_dada_files_already_exists

        If you pass, "keep_dir_create_new_config", the installer will
        install Dada Mail, even if a already-existing, ".dada_files"
        directory is found.

        If you pass this parameter, "skip_configure_dada_files", NO other
        parameter is really required, except, "--dada_files_loc", provided
        that the directory you pass in that parameter has an already-set up
        ".dada_files" directory, including a valid ".dada_config" file. When
        passing this parameter, this script will simply change the,
        "dada/DADA/Config.pm" file to point to the already-created
        ".dada_files" directory.

Other Paramaters
  --help
    Displays this help doc. Use:

        ./install.cgi | less

    (or something similar) so that the script doesn't zoom to the end.

Bugs
    Please file any problems at:

    <https://github.com/justingit/dada-mail/issues>

