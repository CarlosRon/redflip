<?php


$rol = $_POST["rol"];
$nombre = $_POST["nombre"];
$apellido = $_POST["apellido"];
$rut = $_POST["rut"];
$tel = $_POST["telefono"];
$correo = $_POST["correo"];
$estado = $_POST["estado"];

switch ($rol) {
    case '1':
        $resp = agregarAdmin($nombre, $apellido, $rut, $tel, $correo, $estado);
        break;
    case '2':
        $resp = agregarPropietario($nombre, $apellido, $rut, $tel, $correo, $estado);
        echo json_encode($resp);
        break;
    case '3':
        $resp = agregarCorredor($nombre, $apellido, $rut, $tel, $correo, $estado);
        echo json_encode($resp);
        break;
    case '4':
        $resp = agregarInteresado($nombre, $apellido, $rut, $tel, $correo, $estado);
        break;
    
    default:
        return;
        break;
}

function agregarCorredor($nombre, $apellido, $rut, $tel, $correo, $estado){
    $res->error = "";
    $res->nom_us = "";
    $res->clave = "";
    $res->estado = "";
    include_once '../conexion.php';
    include_once 'mcript.php';

    $sqlComprobar = "SELECT * from Persona where correo = '$correo'";
    $result = $conn->query($sqlComprobar);

    if ($result->num_rows > 0) {
    // output data of each row
    $res->error = "Correo ya existe";
    return $res;
    }
    
    $sqlPersona = "INSERT INTO Persona (nombre, apellido, telefono, rut, correo) values ('$nombre', '$apellido', '$tel', '$rut', '$correo')";
    if ($conn->query($sqlPersona) === TRUE) {
        $error = 0;
        $idPer = $conn->insert_id;
    } else {
        $error = $sqlPersona;
    }
    $nom_us = $encriptar(strtolower($correo));
    $pass="";
    $pass = strtoupper($nombre[0]) . strtolower($apellido[0])."Redflip";
    $pass_encript = $encriptar($pass);
    $sqlusuario = "INSERT INTO usuario (nom_us, pass, fk_rol, fk_estado_us, fk_persona) values ('$nom_us', '$pass_encript', 3, $estado ,$idPer)";
    if ($conn->query($sqlusuario) === TRUE) {
        $error = 0;
    } else {
        $error = $sqlusuario;
        return $error;
    }
    
    $sqlCorredor = "INSERT INTO Corredor (fk_persona) values ($idPer)";
    if ($conn->query($sqlCorredor) === TRUE) {
        $error = 0;
    } else {
        $error = $sqlCorredor;
        return $error;
    }
    $res->nom_us    = strtolower($correo);
    $res->clave     = $pass;
    $res->error     = $error;
    $res->estado    = ($estado == 1)?'activo':'inactivo';
    return $res;
    
}

function agregarAdmin(){

}

function agregarPropietario($nombre, $apellido, $rut, $tel, $correo, $estado){
    $res->error = "";
    $res->nom_us = "";
    $res->clave = "";
    $res->estado = "";
    $res->error = "";
    include_once '../conexion.php';
    include_once 'mcript.php';
    $sqlComprobar = "SELECT * from Persona where correo = '$correo'";
    $result = $conn->query($sqlComprobar);

    if ($result->num_rows > 0) {
    // output data of each row
    $res->error = "Correo ya existe";
    return $res;
    }
    //agrega a la persona a la tabla persona
    $sqlPersona = "INSERT INTO Persona (nombre, apellido, telefono, rut, correo) values ('$nombre', '$apellido', '$tel', '$rut', '$correo')";
    if ($conn->query($sqlPersona) === TRUE) {
        $error = 0;
        $idPer = $conn->insert_id;
    } else {
        $error = $sqlPersona;
    }
    $nom_us = $encriptar(strtolower($correo));
    $pass="";
    $pass = strtoupper($nombre[0]) . strtolower($apellido[0])."Redflip";
    $pass_encript = $encriptar($pass);

    //genera registro en tabla usuario
    $sqlusuario = "INSERT INTO usuario (nom_us, pass, fk_rol, fk_estado_us, fk_persona) values ('$nom_us', '$pass_encript', 3, $estado ,$idPer)";
    if ($conn->query($sqlusuario) === TRUE) {
        $error = 0;
    } else {
        $error = $sqlusuario;
        return $error;
    }
    //genera propietario en tabla propietario
    $sqlProp = "insert into Propietario (fk_persona, cont, fk_estado_prop, eliminado, contactos, aprobado) values ($idPer,0,1,false,0,0)";
    if ($conn->query($sqlProp) === TRUE) {
        $success = "2";
    } else {
        $error = $sqlProp;
        return $error;
    }
    $res->nom_us    = strtolower($correo);
    $res->clave     = $pass;
    $res->error     = $error;
    $res->estado    = ($estado == 1)?'activo':'inactivo';
    return $res;
}

function agregarInteresado($nombre, $apellido, $rut, $tel, $correo, $estado){
    $res->error = "";
    $res->nom_us = "";
    $res->clave = "";
    $res->estado = "";
    include_once '../conexion.php';
    include_once 'mcript.php';
    $sqlComprobar = "SELECT * from Persona where correo = '$correo'";
    $result = $conn->query($sqlComprobar);

    if ($result->num_rows > 0) {
    // output data of each row
    $res->error = "Correo ya existe";
    return $res;
    }
    //agrega a la persona a la tabla persona
    $sqlPersona = "INSERT INTO Persona (nombre, apellido, telefono, rut, correo) values ('$nombre', '$apellido', '$tel', '$rut', '$correo')";
    if ($conn->query($sqlPersona) === TRUE) {
        $error = 0;
        $idPer = $conn->insert_id;
    } else {
        $error = $sqlPersona;
    }
    $nom_us = $encriptar(strtolower($correo));
    $pass="";
    $pass = strtoupper($nombre[0]) . strtolower($apellido[0])."Redflip";
    $pass_encript = $encriptar($pass);

    //genera registro en tabla usuario
    $sqlusuario = "INSERT INTO usuario (nom_us, pass, fk_rol, fk_estado_us, fk_persona) values ('$nom_us', '$pass_encript', 3, $estado ,$idPer)";
    if ($conn->query($sqlusuario) === TRUE) {
        $error = 0;
    } else {
        $error = $sqlusuario;
        return $error;
    }


}

?>