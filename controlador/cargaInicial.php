<?php
include '../conexion.php';
include 'mcript.php';
$form = unserialize(base64_decode($_POST["formulario"]));
$cont = 0;
foreach($form as $datos){

    
        $nombrecompleto = explode(" ",utf8_decode($datos["nomProp"]));
        $nom = "";
        $ape = "";
        if(count($nombrecompleto) == 2){
            $nom = $nombrecompleto[0];
            $ape = $nombrecompleto[1];
        }else if(count($nombrecompleto) < 2){
            $nom = $nombrecompleto[0];
            $ape = "";
        }else if(count($nombrecompleto) == 4){
            $nom = $nombrecompleto[0];
            $ape = $nombrecompleto[2];
        }else{
            //si no existe ninguna de las opciones anterioes se guarda el nombre completo en la variable nom
            $nom = utf8_decode($datos["nomProp"]);
            $ape = "";
            
        }
        $sql="insert into Persona (nombre, apellido, rut, fec_nac, telefono, correo) VALUES ('$nom' , '$ape', '',null, '".$datos['tel']."', '".$datos["mailProp"]."')";
        if ($conn->query($sql) === TRUE) {
            $success = "2";
        } else {
            $error = "3.0";
        }

        $sqlPer="select MAX(id_persona) as id from Persona";
        $result = $conn->query($sqlPer);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $idPer=$row["id"];
                $success = "2";
            }
        } else {
            $error = "3.1";
        }

        $nomUs = $encriptar($datos["mailProp"]);
        $pass = strtoupper($nom[0]) . strtolower($ape[0])."Redflip";
        $pass = $encriptar($pass);
        $sqlUs = "INSERT INTO usuario (nom_us, pass, fk_rol, fk_estado_us, fk_persona) VALUES ('$nomUs', '$pass' ,2, 1, $idPer)";
        if ($conn->query($sqlUs) === TRUE) {
            $cont =2;
        } else {
            $error = $conn->error;
        }
        $sqlComuna = "SELECT * from Comuna where nombre = '".$datos["comuna"]."'";
        $result = $conn->query($sqlComuna);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $comuna = $row["id"];
            }
        } else {
            $comuna = 53;
        }

        if(strlen($datos["letra"]) > 5){
            $letra = "";
        }else{
            $letra = $datos["letra"];
        }

        $sqlDireccion = "insert into Direccion (calle, numero, referencia, letra, fk_comuna) values ('".utf8_decode($datos["calle"])."', '".$datos["num"]."' , '' ,'$letra',$comuna)";
        if ($conn->query($sqlDireccion) === TRUE) {
            $success = "2";
        } else {
            $error = $sqlDireccion;
        }

        $sqlSelectDirec = "Select max(id) as id from Direccion";
        $result = $conn->query($sqlSelectDirec);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $idDirec=$row["id"];
                $success = "2";
            }
        } else {
            $error = "3.3";
        }

        $sqlProp = "insert into Propietario (fk_persona, cont, fk_estado_prop, eliminado, contactos,fk_direccion, aprobado, fk_origen) values ($idPer,0,1,false,0,$idDirec, 1, 15)";
        if ($conn->query($sqlProp) === TRUE) {
            $success = "2";
        } else {
            $error = "3.4";
        }

        $sqlSelectProp = "Select max(id_propietario) as id from Propietario";
        $result = $conn->query($sqlSelectProp);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $idProp=$row["id"];
                $success = "2";
            }
        } else {
            $error = "3.5";
        }
        if($datos["operacion"] == "Venta"){
            $operacion = 1;
        }else{
            $operacion = 2;
        }
        $tipoProp = explode(";", $datos["tipoProp"]);
        $sqlTipoProp = "SELECT * from tipo_propiedad where tipo ='".$tipoProp[0]."'";
        $result = $conn->query($sqlTipoProp);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $fk_tipoProp=$row["id_tipo_propiedad"];
                $success = "2";
            }
        } else {
            $fk_tipoProp = "NULL";
        }

        if($datos["condominio"] == "Si"){
            $condominio = 1;
        }else{
            $condominio = 0;
        }
        if($datos["exclusividad"] == "Si"){
            $exclusividad = 1;
        }else{
            $exclusividad = 0;
        }
        if($datos["amoblado"] == "Si"){
            $amoblado = 1;
        }else{
            $amoblado = 0;
        }
        $rol = $datos["rol"];
        $rol = substr($rol,1);
        $rol = str_replace(")", "-",$rol);
        $rol = str_replace(" ","",$rol);

        $calefa = $datos["calefaccion"];
        $sqlCalefa = "SELECT * FROM Tipo_calefaccion where tipo = '$calefa'";
        $result = $conn->query($sqlCalefa);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $fk_calefa=$row["id"];
                $success = "2";
            }
        } else {
            $fk_calefa="NULL";
        }
        if($datos["separada"] == "Si"){
            $separada = 1;
        }else{
            $separada = 0;
        }
        if($datos["aire"] == "Si"){
            $aire = 1;
        }else{
            $aire = 0;
        }
        if($datos["ascensorPriv"] == "Si"){
            $ascensorPriv = 1;
        }else{
            $ascensorPriv = 0;
        }
        if($datos["atico"] == "Si"){
            $atico = 1;
        }else{
            $atico = 0;
        }
        if($datos["bodega"] == "Si"){
            $bodega = 1;
        }else{
            $bodega = 0;
        }
        if($datos["cocinaAme"] == "Si"){
            $cocinaAme = 1;
        }else{
            $cocinaAme = 0;
        }
        if($datos["cocinaEnci"] == "Si"){
            $cocinaEnci = 1;
        }else{
            $cocinaEnci = 0;
        }
        if($cocinaIsla == "Si"){
            $cocinaIsla = 1;
        }else{
            $cocinaIsla = 0;
        }
        if($comedorDiario == "Si"){
            $comedorDiario = 1;
        }else{
            $comedorDiario = 0;
        }
        if($datos["cortinaHang"] == "Si"){
            $cortinaHang = 1;
        }else{
            $cortinaHang = 0;
        }
        if($datos["cortinaRoller"] == "Si"){
            $cortinaRoller = 1;
        }else{
            $cortinaRoller = 0;
        }
        if($datos["cortinaElec"] == "Si"){
            $cortinaElec = 1;
        }else{
            $cortinaElec = 0;
        }
        if($datos["dormServ"] == "Si"){
            $dormServ = 1;
        }else{
            $dormServ = 0;
        }
        if($datos["escritorio"] == "Si"){
            $escritorio = 1;
        }else{
            $escritorio = 0;
        }
        if($datos["hornoEmp"] == "Si"){
            $hornoEmp = 1;
        }else{
            $hornoEmp = 0;
        }
        if($datos["jacuzzi"] == "Si"){
            $jacuzzi = 1;
        }else{
            $jacuzzi = 0;
        }
        if($datos["logia"] == "Si"){
            $logia = 1;
        }else{
            $logia = 0;
        }
        if($datos["mallaProcTerr"] == "Si"){
            $mallaProcTerr = 1;
        }else{
            $mallaProcTerr = 0;
        }
        if($datos["riego"] == "Si"){
            $riego = 1;
        }else{
            $riego = 0;
        }
        if($datos["salaEstar"] == "Si"){
            $salaEstar = 1;
        }else{
            $salaEstar = 0;
        }
        if($datos["alarma"]){
            $alarma = 1;
        }else{
            $alarma = 0;
        }
        if($datos["termoPanel"] == "Si"){
            $termoPanel = 1;
        }else{
            $termoPanel = 0;
        }
        if($datos["lavavajillas"] == "Si"){
            $lavavajillas = 1;
        }else{
            $lavavajillas = 0;
        }
        if($datos["microondas"] == "Si"){
            $microondas = 1;
        }else{
            $microondas = 0;
        }
        if($datos["refrigerador"] == "Si"){
            $refrigerador = 1;
        }else{
            $refrigerador = 0;
        }
        if($datos["despensa"] == "Si"){
            $despensa = 1;
        }else{
            $despensa = 0;
        }
        if($datos["hall"] == "Si"){
            $hall = 1;
        }else{
            $hall = 0;
        }
        if($datos["piezaPlan"] == "Si"){
            $piezaPlan = 1;
        }else{
            $piezaPlan = 0;
        }
        if($datos["cocinaAmob"] == "Si"){
            $cocinaAmob = 1;
        }else{
            $cocinaAmob = 0;
        }
        if($datos["cocinaAmob"] == "Si"){
            $cocinaAmob = 1;
        }else{
            $cocinaAmob = 0;
        }
        if($datos["citofono"] == "Si"){
            $citofono = 1;
        }else{
            $citofono = 0;
        }
        if($datos["mansarda"] == "Si"){
            $mansarda = 1;
        }else{
            $mansarda = 0;
        }
        if($datos["walkingcloset"] == "Si"){
            $walkingcloset = 1;
        }else{
            $walkingcloset = 0;
        }
        if($datos["cercoElec"] == "Si"){
            $cercoElec = 1;
        }else{
            $cercoElec = 0;
        }
        if($datos["ascensorEd"] == "Si"){
            $ascensorEd = 1;
        }else{
            $ascensorEd = 0;
        }
        if($datos["conserje"] == "Si"){
            $conserje = 1;
        }else{
            $conserje = 0;
        }
        if($datos["estVisita"] == "Si"){
            $estVisita = 1;
        }else{
            $estVisita = 0;
        }
        if($datos["gim"] == "Si"){
            $gim = 1;
        }else{
            $gim = 0;
        }
        if($datos["juegosInf"] == "Si"){
            $juegosInf = 1;
        }else{
            $juegosInf = 0;
        }
        if($datos["lavanderia"] == "Si"){
            $lavanderia = 1;
        }else{
            $lavanderia = 0;
        }
        if($datos["piscina"] == "Si"){
            $piscina = 1;
        }else{
            $piscina = 0;
        }
        if($datos["portonElec"] == "Si"){
            $portonElec = 1;
        }else{
            $portonElec = 0;
        }
        if($datos["quincho"] == "Si"){
            $quincho = 1;
        }else{
            $quincho = 0;
        }
        if($datos["salaJuegos"] == "Si"){
            $salaJuegos = 1;
        }else{
            $salaJuegos = 0;
        }
        if($datos["salaReu"] == "Si"){
            $salaReu = 1;
        }else{
            $salaReu = 0;
        }
        if($datos["salaMulti"] == "Si"){
            $salaMulti = 1;
        }else{
            $salaMulti = 0;
        }
        if($datos["sauna"] == "Si"){
            $sauna = 1;
        }else{
            $sauna = 0;
        }
        if($datos["piscinaTemp"] == "Si"){
            $piscinaTemp = 1;
        }else{
            $piscinaTemp = 0;
        }
        if($datos["gourmet"] == "Si"){
            $gourmet = 1;
        }else{
            $gourmet = 0;
        }
        if($datos["azotea"] == "Si"){
            $azotea = 1;
        }else{
            $azotea = 0;
        }
        if($datos["tenis"] == "Si"){
            $tenis = 1;
        }else{
            $tenis = 0;
        }
        if($datos["TV"] == "Si"){
            $TV = 1;
        }else{
            $TV = 0;
        }
        if($datos["areaVerde"] == "Si"){
            $areaVerde = 1;
        }else{
            $areaVerde = 0;
        }
        if($datos["salaCine"] == "Si"){
            $salaCine = 1;
        }else{
            $salaCine = 0;
        }
        if($datos["patioServ"] == "Si"){
            $patioServ = 1;
        }else{
            $patioServ = 0;
        }
        if($datos["antejardin"] == "Si"){
            $antejardin = 1;
        }else{
            $antejardin = 0;
        }
        
        $nomCorredor = explode(" ", $datos["nomCorredor"]);
        $sqlCorredor = "SELECT * FROM `Corredor`, Persona WHERE Persona.nombre ='".$nomCorredor[0]."'  and Persona.id_persona = Corredor.fk_persona";
        $result = $conn->query($sqlCorredor);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $fk_corredor=$row["id_corredor"];
            }
        } else {
            $fk_corredor="NULL";
        }

        $sqlAguaCaliente = "SELECT * from Agua_caliente where tipo = '".$datos["aguaCaliente"]."'";
        $result = $conn->query($sqlAguaCaliente);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $fkAguaCaliente=$row["id_agua_caliente"];
            }
        } else {
            $fkAguaCaliente = 7;
        }

        $sqlpisoDorm = "SELECT * FROM Materiales_pisos where material = '".$datos["materialPisoDorm"]."'";
        $result = $conn->query($sqlpisoDorm);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $matPisoDorm=$row["id_materiales_pisos"];
            }
        } else {
            $matPisoDorm = "NULL";
        }

        $sqlpisoBano = "SELECT * FROM Materiales_pisos_banno where material = '".$datos["materialPisoBano"]."'";
        $result = $conn->query($sqlpisoBano);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $matPisoBanno=$row["id_materiales_pisos"];
            }
        } else {
            $matPisoBanno = "NULL";
        }

        $sqlpisoComun = "SELECT * FROM Materiales_pisos where material = '".$datos["materialPisosComunes"]."'";
        $result = $conn->query($sqlpisoComun);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $matPisoComun=$row["id_materiales_pisos"];
            }
        } else {
            $matPisoComun = "NULL";
        }
        if($datos["banos"] == ""){
            $bannos = "NULL";
        }else{
            $bannos = $datos["banos"];
        }
        if(strlen($datos["dispVisitas"]) > 45){
            $dispVisitas = "";
        }else{
            $dispVisitas = $datos["dispVisitas"];
        }
        $sqlForm = "INSERT INTO `formulario`(
            `area_redflip`,
            `fk_propietario`,
            `fk_operario`,
            `fecha`,
            `operacion`,
            `fk_tipo_propiedad`,
            `valor_uf`,
            `valor_clp`,
            `Direccion_id`,
            `condominio`,
            `exclusividad`,
            `amoblado`,
            `anno`,
            `rol`,
            `contribuciones_trimestrales`,
            `gastos_comunes`,
            `superficie_total`,
            `superficie_util`,
            `superficie_terraza`,
            `superficie_terreno`,
            `orientacion`,
            `dormitorios`,
            `estacionamiento`,
            `pisos`,
            `piso_depto`,
            `cant_deptos`,
            `ascensores`,
            `fk_tipo_calefaccion`,
            `casa_liv_separados`,
            `aire_acondicionado`,
            `ascensor_privado`,
            `atico`,
            `bodega_esp`,
            `cocina_ame`,
            `cocina_enci`,
            `cocina_isla`,
            `comedor_diario`,
            `cortina_hang`,
            `cortina_roller`,
            `cortina_elec`,
            `dorm_serv`,
            `escritorio`,
            `horno_emp`,
            `jacuzzi`,
            `logia`,
            `malla_proc_terr`,
            `riego_auto`,
            `sala_estar`,
            `alarma`,
            `term_panel`,
            `lavavajillas`,
            `microondas`,
            `refrigerador`,
            `despensa`,
            `hall`,
            `planchado`,
            `cocina_amob`,
            `citofono`,
            `mansarda`,
            `walking_closet`,
            `cerco_elec`,
            `ascensor_edi`,
            `conserje_24`,
            `est_visita`,
            `gim`,
            `juegos`,
            `lavanderia`,
            `piscina`,
            `porton_elec`,
            `quincho`,
            `sala_juegos`,
            `sala_reuniones`,
            `sala_multi_uso`,
            `sauna`,
            `piscina_temp`,
            `gourmet_room`,
            `azotea`,
            `cancha_tenis`,
            `circ_tv`,
            `area_verde`,
            `sala_cine`,
            `patio_serv`,
            `antejardin`,
            `disponibilidad_visita`,
            `disponibilidad_entrega`,
            `notas`,
            `aprobado`,
            `cant_bannos`,
            `fin_propiedad`,
            `Aprobado_por`,
            `fk_corredor`,
            `cant_pisos`,
            `dptoPiso`,
            `fk_agua_caliente`,
            `material_piso_dorm`,
            `material_piso_comun`,
            `material_piso_banno`,
            `fk_estado`
        )
        VALUES(
            '".$datos["area"]."',
            $idProp,
            1,
            '".$datos["fecha"]."',
            $operacion,
            $fk_tipoProp,
            '".$datos["valorUF"]."',
            '".$datos["valorCLP"]."',
            $idDirec,
            $condominio,
            $exclusividad,
            $amoblado,
            '".$datos["anoConstruccion"]."',
            '$rol',
            '".$datos["contribuciones"]."',
            '".$datos["gastosComunes"]."',
            '".$datos["supTotal"]."',
            '".$datos["supUtil"]."',
            '".$datos["supTerraza"]."',
            '".$datos["supTerreno"]."',
            '".$datos["orientacion"]."',
            '".$datos["dorm"]."',
            '".$datos["estacionamientos"]."',
            '".$datos["cantPisos"]."',
            '".$datos["pisoDepto"]."',
            '".$datos["cantDeptoPorPiso"]."',
            '".$datos["cantAscensores"]."',
            $fk_calefa,
            $separada,
            $aire,
            $ascensorPriv,
            $atico,
            $bodega,
            $cocinaAme,
            $cocinaEnci,
            $cocinaIsla,
            $comedorDiario,
            $cortinaHang,
            $cortinaRoller,
            $cortinaElec,
            $dormServ,
            $escritorio,
            $hornoEmp,
            $jacuzzi,
            $logia,
            $mallaProcTerr,
            $riego,
            $salaEstar,
            $alarma,
            $termoPanel,
            $lavavajillas,
            $microondas,
            $refrigerador,
            $despensa,
            $hall,
            $piezaPlan,
            $cocinaAmob,
            $citofono,
            $mansarda,
            $walkingcloset,
            $cercoElec,
            $ascensorEd,
            $conserje,
            $estVisita,
            $gim,
            $juegosInf,
            $lavanderia,
            $piscina,
            $portonElec,
            $quincho,
            $salaJuegos,
            $salaReu,
            $salaMulti,
            $sauna,
            $piscinaTemp,
            $gourmet,
            $azotea,
            $tenis,
            $TV,
            $areaVerde,
            $salaCine,
            $patioServ,
            $antejardin,
            '$dispVisitas',
            '".$datos["dispEntrega"]."',
            '".$datos["notas"]."',
            1,
            $bannos,
            '".$datos["finProp"]."',
            1,
            $fk_corredor,
            '".$datos["cantPisos"]."',
            '".$datos["pisoDepto"]."',
            $fkAguaCaliente,
            $matPisoDorm,
            $matPisoComun,
            $matPisoBanno,
            1
        );
        ";
        if ($conn->query(utf8_decode($sqlForm)) === TRUE) {
            $success = "2";
        } else {
            $error =  $sqlForm;
        }

        $sqlSelectForm = "Select MAX(id_formulario) as id from formulario";
        $result = $conn->query($sqlSelectForm);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $idForm=$row["id"];
                $success = "2";
            }
        } else {
            $error = "3.7";
        }
        if($datos["operacion"] == "Venta"){
            $divisa = 1;
        }else{
            $divisa = 2;
        }
        $sqlPropiedad = "INSERT INTO `Propiedad`(
            `id_propiedad`,
            `dormitorios`,
            `bannos`,
            `estacionamiento`,
            `metros_totales`,
            `metros_utiles`,
            `bodega`,
            `monto`,
            `fk_propietario`,
            `fk_estado_propiedad`,
            `fk_corredor`,
            `fk_lista_administrativo`,
            `fk_lista_interesado`,
            `fk_lista_servicio`,
            `fk_direccion`,
            `divisa`,
            `fk_venta_arriendo`,
            `nota`,
            `fk_evento`,
            `fk_status`,
            `fk_operario`,
            `fk_tipo_propiedad`,
            `id_externo`,
            `fk_formulario`,
            `carga`
        )
        VALUES(
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            $idProp,
            1,
            NULL,
            NULL,
            NULL,
            NULL,
            $idDirec,
            $divisa,
            $divisa,
            NULL,
            NULL,
            1,
            NULL,
            NULL,
            NULL,
            $idForm,
            0
        );";

        if ($conn->query(utf8_decode($sqlPropiedad)) === TRUE) {
            $success = "2";
        } else {
            $error =  "3.10";
            // $error =  $sqlPropiedad;
        }

        $sqlSelectPropiedad = "Select MAX(id_propiedad) as id from Propiedad";
        $result = $conn->query($sqlSelectPropiedad);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $idPropiedad=$row["id"];
                $success = "2";
            }
        } else {
            $error = "3.7";
        }
    $cont ++;
    if($cont >=155){
        $resp->success = $success;
        $resp->error = $error;
        echo json_encode($cont);        
        die;
    }
}


?>