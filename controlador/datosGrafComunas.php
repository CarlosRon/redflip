<?php
include '../conexion.php';
function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}
$idCorredor = $_REQUEST["idCorredor"];

$sql = "SELECT Distinct Comuna.nombre from formulario, Direccion, Comuna where fk_corredor = $idCorredor and aprobado = 1 AND Direccion_id = Direccion.id AND Comuna.id = Direccion.fk_comuna AND nombre NOT IN ('S/D')";
$result = $conn->query($sql);
$comunas = array();
if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
      array_push($comunas,$row["nombre"]);
  }
} else {
}
$conteo = array();
$colores = array();
foreach ($comunas as $comuna) {
    $sql2 = "SELECT  COUNT(Comuna.id) as cont from formulario, Direccion, Comuna where fk_corredor = $idCorredor and aprobado = 1 AND Direccion_id = Direccion.id AND Comuna.id = Direccion.fk_comuna AND nombre NOT IN ('S/D') AND Comuna.nombre = '$comuna'";
    $result = $conn->query($sql2);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row2 = $result->fetch_assoc()) {
            array_push($conteo,$row2["cont"]);
            array_push($colores, '#'.random_color());
        }
    } 
}
$res->comunas =$comunas;
$res->conteo = $conteo;
$res->colores = $colores;
echo json_encode($res);
?>