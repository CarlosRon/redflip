<?php
    include '../conexion.php';
    // include '../pages/valid_session.php';
    include 'mcript.php';

    //Desencriptar usuario
    $usEncript = $_SESSION['usuario'];
    $usuario = $desencriptar($usEncript);

    //Obtener notificaciones del usaurio que está logueado
    $sql="select count(*) as cont from Notificacion where visto = 0";
    $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // Obtiene las filas del result
            while($row = $result->fetch_assoc()) {
                //Obtiene primera linea del result
                $cont = $row["cont"];
            }
        } else {
            $error=$sql;
        }
        $conn->close();

        // Crea un arreglo con los resultados
        $result->cont=$cont;
        $result->error=$error;
        
        // Imprime la variable "cont" en formato Json
        echo json_encode($result);
?>