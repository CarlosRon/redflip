<?php
// $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
// $propiedades = json_decode(file_get_contents('https://api.mercadolibre.com/users/422248515/items/search?search_type=scan&status=active&orders=start_time_desc&limit=100&access_token=APP_USR-2868867605753550-101313-ca4357f0e721db73a301fefb7f45cc85-422248515',false,$context));
$url = 'https://api.mercadolibre.com/users/422248515/items/search?search_type=scan&status=active&orders=start_time_desc&limit=100&access_token=';
$access_token = 'APP_USR-2868867605753550-101313-ca4357f0e721db73a301fefb7f45cc85-422248515';
$request_url = $url . $access_token;
$curl = curl_init($request_url);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, [
  'Content-Type: application/json'
]);
$propiedades = json_decode(curl_exec($curl));
curl_close($curl);

$array = array();

foreach ($propiedades->results as $id_prop) {
    $url2 = "https://api.mercadolibre.com/items/".$id_prop;
    $curl2 = curl_init($url2);
    curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl2, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json'
    ]);
    $datosProp = json_decode(curl_exec($curl2));

    $titulo = $datosProp->title;
    $id = $id_prop;
    $fechaPublicacion = explode('T',$datosProp->start_time);
    $fechaPublicacion = $fechaPublicacion[0];
    if ($datosProp->domain_id == "MLC-APARTMENTS_FOR_RENT") {
        $tipo = "Depto";
        $operacion = "Arriendo";
    } else if ($datosProp->domain_id == "MLC-INDIVIDUAL_APARTMENTS_FOR_SALE") {
        $tipo = "Depto";
        $operacion = "Venta";
    } else if ($datosProp->domain_id == "MLC-OFFICES_FOR_RENT") {
        $tipo = "Oficina";
        $operacion = "Arriendo";
    } else if ($datosProp->domain_id == "MLC-INDIVIDUAL_OFFICES_FOR_SALE") {
        $tipo = "Oficina";
        $operacion = "Venta";
    } else if ($datosProp->domain_id == "MLC-INDIVIDUAL_HOUSES_FOR_SALE") {
        $tipo = "Casa";
        $operacion = "Venta";
    } else if ($datosProp->domain_id == "MLC-RETAIL_SPACE_FOR_RENT") {
        $tipo = "Local";
        $operacion = "Arriendo";
    } else if ($datosProp->domain_id == "MLC-RETAIL_SPACE_FOR_SALE") {
        $tipo = "Local";
        $operacion = "Venta";
    }
    if ($datosProp->currency_id == "CLP") {
        $precio = "$ " . $datosProp->price;

    } else {
        $precio = "UF " . $datosProp->price;
    }
    $hoy = date('yy-m-d'); 
    $ayer = date("yy-m-d",strtotime($hoy."- 1 days"));

    $urlVisitas = "https://api.mercadolibre.com/items/visits?ids=".$id_prop.'&date_from='.$ayer.'&date_to='.$hoy;
    $curl3 = curl_init($urlVisitas);
    curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl3, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json'
    ]);
    $visitas = $datosProp = json_decode(curl_exec($curl3));
    // $visitas = json_decode(file_get_contents('https://api.mercadolibre.com/items/visits?ids='.$id_prop.'&date_from='.$ayer.'&date_to='.$hoy,false,$context));
    $visitasHoy = $visitas[0]->total_visits;
    $propiedad = array( 'id' => $id_prop,
                        'titulo' => $titulo,
                        'tipo' => $tipo,
                        'operacion' => $operacion,
                        'precio' => $precio,
                        'visitas_hoy' => $visitasHoy,
                        'fecha_publicacion' => $fechaPublicacion);
    array_push($array, $propiedad);

}

$total = $propiedades->paging->total;
$limit = $propiedades->paging->limit;
// $scroll = $propiedades->scroll_id;
// $arrayScroll = array();

// for($i = 0; $i< round($total/$limit); $i++){
    // $propiedades2 = json_decode(file_get_contents('https://api.mercadolibre.com/users/422248515/items/search?search_type=scan&status=active&orders=start_time_desc&limit=100&access_token=APP_USR-2868867605753550-101313-ca4357f0e721db73a301fefb7f45cc85-422248515&scroll_id='.$scroll,false,$context));
    // foreach ($propiedades2->results as $id_prop2) {
    //     $datosProp = json_decode(file_get_contents('https://api.mercadolibre.com/items/'.$id_prop2,false,$context));
    //     $titulo = $datosProp->title;
    //     $id = $id_prop2;
    //     $fechaPublicacion = explode('T',$datosProp->start_time);
    //     $fechaPublicacion = $fechaPublicacion[0];
    //     if ($datosProp->domain_id == "MLC-APARTMENTS_FOR_RENT") {
    //         $tipo = "Depto";
    //         $operacion = "Arriendo";
    //     } else if ($datosProp->domain_id == "MLC-INDIVIDUAL_APARTMENTS_FOR_SALE") {
    //         $tipo = "Depto";
    //         $operacion = "Venta";
    //     } else if ($datosProp->domain_id == "MLC-OFFICES_FOR_RENT") {
    //         $tipo = "Oficina";
    //         $operacion = "Arriendo";
    //     } else if ($datosProp->domain_id == "MLC-INDIVIDUAL_OFFICES_FOR_SALE") {
    //         $tipo = "Oficina";
    //         $operacion = "Venta";
    //     } else if ($datosProp->domain_id == "MLC-INDIVIDUAL_HOUSES_FOR_SALE") {
    //         $tipo = "Casa";
    //         $operacion = "Venta";
    //     } else if ($datosProp->domain_id == "MLC-RETAIL_SPACE_FOR_RENT") {
    //         $tipo = "Local";
    //         $operacion = "Arriendo";
    //     } else if ($datosProp->domain_id == "MLC-RETAIL_SPACE_FOR_SALE") {
    //         $tipo = "Local";
    //         $operacion = "Venta";
    //     }
    //     if ($datosProp->currency_id == "CLP") {
    //         $precio = "$ " . $datosProp->price;
    
    //     } else {
    //         $precio = "UF " . $datosProp->price;
    //     }
    //     $hoy = date('yy-m-d'); 
    //     $ayer = date("yy-m-d",strtotime($hoy."- 1 days"));
    //     $visitas = json_decode(file_get_contents('https://api.mercadolibre.com/items/visits?ids='.$id_prop2.'&date_from='.$ayer.'&date_to='.$hoy,false,$context));
    //     $visitasHoy = $visitas[0]->total_visits;
    //     $propiedad2 = array( 'id' => $id_prop2,
    //                         'titulo' => $titulo,
    //                         'tipo' => $tipo,
    //                         'operacion' => $operacion,
    //                         'precio' => $precio,
    //                         'visitas_hoy' => $visitasHoy,
    //                         'fecha_publicacion' => $fechaPublicacion);
    //     array_push($arrayScroll, $propiedad2);
    // }
// }
$res->total = $total;
$res->limit = $limit;
$res->results = $array;
// $res->results2 = $arrayScroll;
// $res->scroll_id = $scroll;
echo json_encode($res);
?>