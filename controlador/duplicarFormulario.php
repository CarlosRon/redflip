<?php
    include '../conexion.php';

    $idForm = $_REQUEST["idForm"];
    $operacion = $_REQUEST["tipo_op_select"];
    $select_area = $_REQUEST["select_area"];
    $corredor = $_REQUEST["corredor"];
    $operario = $_REQUEST["operario"];
    $nombre = $_REQUEST["nombre"];
    $apellido = $_REQUEST["apellido"];
    $rut = $_REQUEST["rut"];
    $origen1 = $_REQUEST["origen"];
    $captadaR = $_REQUEST["captadaR"];
    $partnerR = $_REQUEST["partnerR"];
    if($origen1 == 17){
        $origen2 = $captadaR;
    }else if($origen1 == 18){
        $origen2 = $partnerR;
    }else{
        $origen2 = null;
    }
    $telefono = $_REQUEST["sub_fono"].$_REQUEST["telefono"];
    $correo = $_REQUEST["correo"];
    $fecha = $_REQUEST["fecha"];

    $sql = "SELECT *, formulario.fk_corredor as corredor,
    formulario.fk_operario as oper,
    formulario.bodega as bode,
    formulario.dormitorios as dorm,
    formulario.estacionamiento as estac,
    formulario.fk_tipo_propiedad as fk_tipo_prop, Direccion.calle as dir_calle, Direccion.numero as dir_num, Direccion.referencia as dir_ref, Direccion.fk_comuna as dir_com, Direccion.letra as dir_letra from
    formulario,
    Propiedad,
    Direccion,
    Propietario,
    Persona
    WHERE
    formulario.id_formulario = $idForm and
    formulario.id_formulario = Propiedad.fk_formulario
    AND Direccion.id = formulario.Direccion_id
    AND Propietario.id_propietario = formulario.fk_propietario
    AND Persona.id_persona = Propietario.fk_persona";

    $result = $conn->query($sql);

    if ($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            //formulario
            $fk_propietario = $row["fk_propietario"];
            $fk_tipo_propiedad = $row["fk_tipo_prop"];
            $Direccion_id = $row["Direccion_id"];
            $fk_tipo_calefaccion = $row["fk_tipo_calefaccion"];
            if($fk_tipo_calefaccion == ""){
                $fk_tipo_calefaccion = "null";
            }
            $banno2 = $row["banno2"];
            $Aprobado_por = $row["Aprobado_por"];
            $usuario = $row["usuario"];
            $divisa_monto2 = $row["divisa_monto2"];
            if($divisa_monto2 == ""){
                $divisa_monto2 = "null";
            }
            $divisa_monto1 = $row["divisa_monto1"];
            if($divisa_monto1 == ""){
                $divisa_monto1 = "null";
            }
            $fk_tiempo_publicacion = $row["fk_tiempo_publicacion"];
            if($fk_tiempo_publicacion == ""){
                $fk_tiempo_publicacion = "null";
            }
            $material_pisos_terraza = $row["material_pisos_terraza"];
            if($material_pisos_terraza == 0){
                $material_pisos_terraza = "null";
            }
            $fk_agua_caliente = $row["fk_agua_caliente"];
            if($fk_agua_caliente == 0){
                $fk_agua_caliente = "null";
            }
            $material_piso_dorm = $row["material_piso_dorm"];
            if($material_piso_dorm == 0){
                $material_piso_dorm = "null";
            }
            $material_piso_comun = $row["material_piso_comun"];
            if($material_piso_comun == 0){
                $material_piso_comun = "null";
            }
            $material_pisos_cocina = $row["material_pisos_cocina"];
            if($material_pisos_cocina == 0){
                $material_pisos_cocina = "null";
            }
            $material_piso_banno = $row["material_piso_banno"];
            if($material_piso_banno == 0){
                $material_piso_banno = "null";
            }
            $fk_tipo_gas = $row["fk_tipo_gas"];
            if($fk_tipo_gas == 0){
                $fk_tipo_gas = "null";
            }
            $fk_banco = $row["fk_banco"];
            if($fk_banco == 0){
                $fk_banco = "null";
            }
            $fk_estado = $row["fk_estado"];
            if($fk_estado == 0){
                $fk_estado = "null";
            }
            $condominio = $row["condominio"];
            $exclusividad = $row["exclusividad"];
            $amoblado = $row["amoblado"];
            $anno = $row["anno"];
            $contribuciones_trimestrales = $row["contribuciones_trimestrales"];
            $gastos_comunes = $row["gastos_comunes"];
            $nota_gastos_com = $row["nota_gastos_com"];
            $superficie_total = $row["superficie_total"];
            $superficie_util = $row["superficie_util"];
            $superficie_terraza = $row["superficie_terraza"];
            $superficie_terreno = $row["superficie_terreno"];
            $dormitorios = $row["dormitorios"];
            $dormitorios_suite = $row["dormitorios_suite"];
            $Suit_Walking_Closet = $row["Suit_Walking_Closet"];
            if($Suit_Walking_Closet == 0){
                $Suit_Walking_Closet = "null";
            }
            $desc_baños = $row["desc_baños"];
            $estacionamiento = $row["estacionamiento"];
            $num_estacionamiento = $row["num_estacionamiento"];
            $ubicacion_est = $row["ubicacion_est"];
            $est_subterraneo = $row["est_subterraneo"];
            $pisos = $row["pisos"];
            $piso_depto = $row["piso_depto"];
            $cant_deptos = $row["cant_deptos"];
            $ascensores = $row["ascensores"];
            $bodega = $row["bodega"];
            $num_bodega = $row["num_bodega"];
            $casa_liv_separados = $row["casa_liv_separados"];
            $otros_sum = $row["otros_sum"];
            $aire_acondicionado = $row["aire_acondicionado"];
            $ascensor_privado = $row["ascensor_privado"];
            $duplex = $row["duplex"];
            $triplex = $row["triplex"];
            $mariposa = $row["mariposa"];
            $penthouse = $row["penthouse"];
            $atico = $row["atico"];
            $bodega_esp = $row["bodega_esp"];
            if($bodega_esp == 0){
                $bodega_esp = "null";
            }
            $cocina_ame = $row["cocina_ame"];
            $cocina_enci = $row["cocina_enci"];
            $cocina_isla = $row["cocina_isla"];
            $cocinaIndep = $row["cocinaIndep"];
            $comedor_diario = $row["comedor_diario"];
            $cortina_hang = $row["cortina_hang"];
            $persiana_aluminio = $row["persiana_aluminio"];
            $cortina_roller = $row["cortina_roller"];
            $cortina_elec = $row["cortina_elec"];
            $dorm_serv = $row["dorm_serv"];
            if($dorm_serv == 0){
                $dorm_serv = "null";
            }
            $escritorio = $row["escritorio"];
            $horno_emp = $row["horno_emp"];
            $jacuzzi = $row["jacuzzi"];
            $logia = $row["logia"];
            $malla_proc_terr = $row["malla_proc_terr"];
            $riego_auto = $row["riego_auto"];
            $sala_estar = $row["sala_estar"];
            $alarma = $row["alarma"];
            $term_panel = $row["term_panel"];
            $lavavajillas = $row["lavavajillas"];
            $microondas = $row["microondas"];
            $refrigerador = $row["refrigerador"];
            $despensa = $row["despensa"];
            $hall = $row["hall"];
            $planchado = $row["planchado"];
            $cocina_amob = $row["cocina_amob"];
            $citofono = $row["citofono"];
            $mansarda = $row["mansarda"];
            if($mansarda == 0){
                $mansarda = "null";
            }
            $walking_closet = $row["walking_closet"];
            if($walking_closet == 0){
                $walking_closet = "null";
            }
            $cerco_elec = $row["cerco_elec"];
            $ascensor_edi = $row["ascensor_edi"];
            if($ascensor_edi == 0){
                $ascensor_edi = "null";
            }
            $conserje_24 = $row["conserje_24"];
            $est_visita = $row["est_visita"];
            $gim = $row["gim"];
            $juegos = $row["juegos"];
            $lavanderia = $row["lavanderia"];
            $piscina = $row["piscina"];
            $porton_elec = $row["porton_elec"];
            $quincho = $row["quincho"];
            $terraza_quincho = $row["terraza_quincho"];
            $bar = $row["bar"];
            $sala_juegos = $row["sala_juegos"];
            $sala_reuniones_of = $row["sala_reuniones_of"];
            $sala_multi_uso = $row["sala_multi_uso"];
            $sala_eventos = $row["sala_eventos"];
            $sauna = $row["sauna"];
            $piscina_temp = $row["piscina_temp"];
            $gourmet_room = $row["gourmet_room"];
            $azotea = $row["azotea"];
            $ascensor_comun = $row["ascensor_comun"];
            $cancha_tenis = $row["cancha_tenis"];
            $circ_tv = $row["circ_tv"];
            $area_verde = $row["area_verde"];
            $sala_cine = $row["sala_cine"];
            $patio_serv = $row["patio_serv"];
            $antejardin = $row["antejardin"];
            $disponibilidad_visita = $row["disponibilidad_visita"];
            $disponibilidad_entrega = $row["disponibilidad_entrega"];
            $notas = $row["notas"];
            $aprobado = $row["aprobado"];
            $cant_bannos = $row["cant_bannos"];
            $fin_propiedad = $row["fin_propiedad"];
            $mascotas = $row["mascotas"];
            $cartel = $row["cartel"];
            $encimeraGas = $row["encimeraGas"];
            $encimeraElect = $row["encimeraElect"];
            $estractor = $row["estractor"];
            $cant_pisos = $row["cant_pisos"];
            $nombre_form = $row["nombre_form"];
            $dptoPiso = $row["dptoPiso"];
            $cantCorredor = $row["cantCorredor"];
            $adquisicion = $row["adquisicion"];
            $cocinaIntegrada = $row["cocinaIntegrada"];
            $panelSolar = $row["panelSolar"];
            $cant_pisos_casa = $row["cant_pisos_casa"];
            $infoPropVenta = $row["infoPropVenta"];
            $hipoteca = $row["hipoteca"];
            $propDestacado = $row["propDestacado"];
            $sup_patio = $row["sup_patio"];
            $enviado = $row["enviado"];
            $loteo = $row["loteo"];
            $monto1 = $row["monto1"];
            $monto2 = $row["monto2"];
            $valorAmob = $row["valorAmob"];
            $valorAmobArr = $row["valorAmobArr"];
            $imagen = $row["imagen"];
            $llave = $row["llave"];
            $bicicletero = $row["bicicletero"];
            $url = $row["url"];
            $rol = $row["rol"];
            $fecha_entrega = $row["fecha_entrega"];
            if($fecha_entrega == 0){
                $fecha_entrega = "null";
            }
            $pareada = $row["pareada"];
            $persiana_madera = $row["persiana_madera"];
            $cava_vinos = $row["cava_vinos"];
            $accesos_controlados = $row["accesos_controlados"];
            $plaza = $row["plaza"];
            $cancha_futbol = $row["cancha_futbol"];
            $cancha_golf = $row["cancha_golf"];
            $sala_juegos_ext = $row["sala_juegos_ext"];
            $plantas_libres = $row["plantas_libres"];
            $privado = $row["privado"];
            $bannos_ctes = $row["bannos_ctes"];
            $kitchenette = $row["kitchenette"];
            $cocina = $row["cocina"];
            $red_telefonia = $row["red_telefonia"];
            $red_computadores = $row["red_computadores"];
            $iluminacion_led = $row["iluminacion_led"];
            $red_incendio = $row["red_incendio"];
            $climatizacion_vrv = $row["climatizacion_vrv"];
            $puerta_magnetica = $row["puerta_magnetica"];
            $persiana_electrica = $row["persiana_electrica"];
            $edificio_clase = $row["edificio_clase"];
            $sala_reuniones = $row["sala_reuniones"];
            $puestos_trabajo = $row["puestos_trabajo"];
            $recepcion = $row["recepcion"];
            $estacionamientos_ctes = $row["estacionamientos_ctes"];
            $est_clientes_of = $row["est_clientes_of"];
            $casino = $row["casino"];
            $cafeteria = $row["cafeteria"];
            $maquina_disp = $row["maquina_disp"];
            $sala_capacitacion = $row["sala_capacitacion"];
            $auditorio = $row["auditorio"];
            $estacionamientos_arr = $row["estacionamientos_arr"];
            $salas_reu_switch = $row["salas_reu_switch"];
            $energia_trifasica = $row["energia_trifasica"];
            if($energia_trifasica == 0){
                $energia_trifasica = "null";
            }
            $recepcion_edificio = $row["recepcion_edificio"];
            $persiana_manual = $row["persiana_manual"];
            if($persiana_manual == 0){
                $persiana_manual = "null";
            }
            $piso_of = $row["piso_of"];
            if($piso_of == 0){
                $piso_of = "null";
            }
            $urbanizado = $row["urbanizado"];
            $agricola = $row["agricola"];
            $paso_servidumbre = $row["paso_servidumbre"];
            $tour3d = $row["tour3d"];
            $area_redflip = $row["area_redflip"];

            //persona (5 royale UWU)
            $id_persona = $row["id_persona"];
            $fec_nac = $row["fec_nac"];

            //usuario
            $id_usuario = $row["id_usuario"];
            $nom_us = $row["nom_us"];
            $pass = $row["pass"];
            $fk_rol = $row["fk_rol"];
            $fk_estado_us = $row["fk_estado_us"];
            $fk_persona = $row["fk_persona"];
            $ultima_pag = $row["ultima_pag"];

            //propietario
            $id_propietario = $row["id_propietario"];
            $fk_persona = $row["fk_persona"];
            $cont = $row["cont"];
            $fk_estado_prop = $row["fk_estado_prop"];
            $eliminado = $row["eliminado"];
            $fec_creacion = $row["fec_creacion"];
            $fk_direccion = $row["fk_direccion"];
            $contactos = $row["contactos"];
            $fk_origen = $row["fk_origen"];
            $aprobado = $row["aprobado"];
            $fk_contacto = $row["fk_contacto"];

            //direccion
            $id = $row["id"];
            $calle = $row["dir_calle"];
            $numero = $row["dir_num"];
            $referencia = $row["dir_ref"];
            $fk_comuna = $row["dir_com"];
            $letra = $row["dir_letra"];

            //propiedad
            $id_propiedad = $row["id_propiedad"];
            $dormitorios = $row["dormitorios"];
            $bannos = $row["bannos"];
            $estacionamiento = $row["estacionamiento"];
            $metros_totales = $row["metros_totales"];
            $metros_utiles = $row["metros_utiles"];
            $bodega = $row["bodega"];
            $monto = $row["monto"];
            $fk_propietario = $row["fk_propietario"];
            $fk_estado_propiedad = $row["fk_estado_propiedad"];
            $fk_lista_administrativo = $row["fk_lista_administrativo"];
            $fk_lista_servicio = $row["fk_lista_servicio"];
            $fk_direccion = $row["fk_direccion"];
            $divisa = $row["divisa"];
            $fk_venta_arriendo = $row["fk_venta_arriendo"];
            $nota = $row["nota"];
            $fk_evento = $row["fk_evento"];
            $fk_status = $row["fk_status"];
            $id_externo = $row["id_externo"];
            $fk_formulario = $row["fk_formulario"];
            $carga = $row["carga"];
            
        }
    }else{
        echo json_encode($sql);
        die;
    }

    //pass
    //corre = nom us

    $idPersona = "";
    $idUsuario = "";
    $idPropietario = "";
    $idDireccion = "";
    $idPropiedad = "";
    $idFormularioDuplicado = "";


    $sqlPer = "INSERT INTO Persona (nombre, apellido, rut, telefono, correo) VALUES ('$nombre', '$apellido', '$rut', '$telefono', '$correo')";

    if ($conn->query($sqlPer) === TRUE) {
        $idPersona = $conn->insert_id;
    } else {
        echo json_encode($sqlPer);
        die;
    }

    $sqlDirec = "INSERT INTO Direccion (calle, numero, referencia, fk_comuna, letra) VALUES ('$calle', '$numero', '$referencia', '$fk_comuna', '$letra')";
    
    if ($conn->query($sqlDirec) === TRUE) {
        $idDireccion = $conn->insert_id;
    } else {
        echo json_encode($sqlDirec);
        die;
    }

    $sqlProp = "INSERT INTO Propietario (fk_persona, cont, fk_estado_prop, eliminado, fec_creacion, 
    fk_direccion, contactos, fk_origen, aprobado) 
    VALUES ('$idPersona', '$cont', '$fk_estado_prop', '$eliminado', '$fec_creacion', '$idDireccion', '$contactos', '$origen1', '$aprobado')";

    if ($conn->query($sqlProp) === TRUE) {
        $idPropietario = $conn->insert_id;
    } else {
        echo json_encode($sqlProp); 
        die;
    }
    $sqlForm1 = "INSERT INTO formulario (fk_propietario) VALUES ($idPropietario)";
    if ($conn->query($sqlForm1) === TRUE) {
        $idFormularioDuplicado = $conn->insert_id;
    } else {
        $res->error = $sqlForm1;
        echo json_encode("Error: " . $sqlForm1 );
        die;
    }

    $sqlForm2 = "UPDATE formulario SET
    fk_operario = $operario,
    operacion = $operacion,
    fk_tipo_propiedad = $fk_tipo_propiedad,
    Direccion_id = $fk_direccion,
    fk_tipo_calefaccion = $fk_tipo_calefaccion,
    fk_corredor = $corredor,
    divisa_monto2 = $divisa_monto2,
    divisa_monto1 = $divisa_monto1,
    fk_tiempo_publicacion = $fk_tiempo_publicacion,
    material_pisos_terraza = $material_pisos_terraza,
    fk_agua_caliente = $fk_agua_caliente,
    material_piso_dorm = $material_piso_dorm,
    material_piso_comun = $material_piso_comun, 
    material_pisos_cocina = $material_pisos_cocina, 
    material_piso_banno = $material_piso_banno, 
    fk_tipo_gas = $fk_tipo_gas, 
    fk_banco = $fk_banco, 
    fk_estado = $fk_estado, 
    origen1 = $origen1, 
    origen2 = $origen2, 
    area_redflip = '$area_redflip', 
    fecha = '$fecha', 
    condominio = $condominio, 
    exclusividad = $exclusividad, 
    amoblado = $amoblado, 
    anno = '$anno', 
    contribuciones_trimestrales = '$contribuciones_trimestrales', 
    gastos_comunes = '$gastos_comunes', 
    nota_gastos_com = '$nota_gastos_com', 
    superficie_total = '$superficie_total'
    WHERE id_formulario = '$idFormularioDuplicado'
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    superficie_util = '$superficie_util', 
    superficie_terraza = '$superficie_terraza',
    superficie_terreno = '$superficie_terreno',
    dormitorios_suite = '$dormitorios_suite',
    Suit_Walking_Closet = $Suit_Walking_Closet,
    estacionamiento = '$estacionamiento',
    num_estacionamiento = '$num_estacionamiento',
    ubicacion_est = '$ubicacion_est',
    est_subterraneo = '$est_subterraneo'
    WHERE id_formulario = '$idFormularioDuplicado'
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    pisos = '$pisos',
    piso_depto = '$piso_depto',
    cant_deptos = '$cant_deptos',
    ascensores = '$ascensores',
    bodega = '$bodega',
    num_bodega = '$num_bodega',
    casa_liv_separados = '$casa_liv_separados',
    otros_sum = '$otros_sum',
    aire_acondicionado = $aire_acondicionado,
    ascensor_privado = $ascensor_privado
    WHERE id_formulario = '$idFormularioDuplicado'
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    duplex = $duplex,
    triplex = $triplex,
    mariposa = $mariposa,
    penthouse = $penthouse,
    atico = $atico,
    bodega_Esp = $bodega_esp,
    cocina_ame = $cocina_ame,
    cocina_isla = $cocina_isla,
    cocinaIndep = $cocinaIndep,
    comedor_diario = $comedor_diario,
    cortina_hang = $cortina_hang
    WHERE id_formulario = $idFormularioDuplicado
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    persiana_aluminio = $persiana_aluminio,
    cortina_roller = $cortina_roller,
    cortina_elec = $cortina_elec,
    dorm_serv = $dorm_serv,
    escritorio = $escritorio,
    horno_emp = $horno_emp,
    jacuzzi = $jacuzzi,
    logia = $logia,
    malla_proc_terr = $malla_proc_terr,
    riego_auto = $riego_auto,
    sala_estar = $sala_estar,
    alarma = $alarma,
    term_panel = $term_panel,
    lavavajillas = $lavavajillas,
    microondas = $microondas,
    refrigerador = $refrigerador
    WHERE id_formulario = '$idFormularioDuplicado'
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    despensa = $despensa,
    hall = $hall,
    planchado = $planchado,
    cocina_amob = $cocina_amob,
    mansarda = $mansarda,
    walking_closet = $walking_closet,
    cerco_elec = $cerco_elec,
    ascensor_edi = $ascensor_edi,
    conserje_24 = $conserje_24,
    est_visita = $est_visita,
    gim = $gim,
    juegos = $juegos,
    lavanderia = $lavanderia,
    piscina = $piscina,
    porton_elec = $porton_elec
    WHERE id_formulario = $idFormularioDuplicado
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    quincho = $quincho,
    terraza_quincho = $terraza_quincho,
    bar = $bar,
    sala_juegos = $sala_juegos,
    sala_reuniones = $sala_reuniones,
    sala_multi_uso = $sala_multi_uso,
    sala_eventos = $sala_eventos,
    sauna = $sauna,
    piscina_temp = $piscina_temp,
    gourmet_room = $gourmet_room,
    azotea = $azotea,
    ascensor_comun = $ascensor_comun,
    cancha_tenis = $cancha_tenis
    WHERE id_formulario = '$idFormularioDuplicado'
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    circ_tv = $circ_tv,
    area_verde = $area_verde,
    sala_cine = $sala_cine,
    patio_serv = $patio_serv,
    antejardin = $antejardin,
    disponibilidad_visita = '$disponibilidad_visita',
    disponibilidad_entrega = '$disponibilidad_entrega',
    notas = '$notas',
    aprobado = 1,
    cant_bannos = $cant_bannos,
    fin_propiedad = '$fin_propiedad',
    mascotas = $mascotas,
    cartel = $cartel,
    encimeraGas = $encimeraGas,
    encimeraElect = $encimeraElect,
    estractor = $estractor,
    cant_pisos = '$cant_pisos'
    WHERE id_formulario = $idFormularioDuplicado
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    nombre_form = '$nombre_form',
    dptoPiso = '$dptoPiso',
    cantCorredor = '$cantCorredor',
    adquisicion = '$adquisicion',
    cocinaIntegrada = $cocinaIntegrada,
    panelSolar = $panelSolar,
    cant_pisos_casa = '$cant_pisos_casa',
    infoPropVenta = '$infoPropVenta',
    hipoteca = $hipoteca,
    propDestacado = '$propDestacado',
    sup_patio = '$sup_patio',
    enviado = $enviado,
    loteo = $loteo,
    monto1 = '$monto1',
    monto2 = '$monto2'
    WHERE id_formulario = '$idFormularioDuplicado'
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    valorAmob = '$valorAmob',
    valorAmobArr = '$valorAmobArr',
    imagen = '$imagen',
    llave = $llave,
    bicicletero = $bicicletero,
    url = '$url',
    rol = '$rol',
    fecha_entrega = $fecha_entrega,
    pareada = $pareada,
    persiana_madera = $persiana_madera,
    cava_vinos = $cava_vinos,
    accesos_controlados = $accesos_controlados,
    plaza = $plaza,
    cancha_futbol = $cancha_futbol,
    sala_juegos_ext = $sala_juegos_ext
    WHERE id_formulario = $idFormularioDuplicado
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    plantas_libres = '$plantas_libres',
    privado = '$privado',
    bannos_ctes = '$bannos_ctes',
    kitchenette = $kitchenette,
    cocina = $cocina,
    red_telefonia = $red_telefonia,
    red_computadores = $red_computadores,
    iluminacion_led = $iluminacion_led,
    red_incendio = $red_incendio
    WHERE id_formulario = '$idFormularioDuplicado'
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      } 

    $sqlForm2 = "UPDATE formulario SET
    climatizacion_vrv = $climatizacion_vrv,
    puerta_magnetica = $puerta_magnetica,
    persiana_electrica = $persiana_electrica,
    edificio_clase = '$edificio_clase',
    sala_reuniones_of = '$sala_reuniones_of',
    puestos_trabajo = '$puestos_trabajo',
    recepcion = $recepcion,
    estacionamientos_ctes = '$estacionamientos_ctes',
    est_clientes_of = $est_clientes_of,
    casino = $casino,
    maquina_disp = $maquina_disp,
    sala_capacitacion = $sala_capacitacion,
    auditorio = $auditorio,
    estacionamientos_arr = $estacionamientos_arr,
    salas_reu_switch = $salas_reu_switch,
    energia_trifasica = $energia_trifasica,
    persiana_manual = $persiana_manual,
    piso_of = $piso_of,
    urbanizado = $urbanizado,
    agricola = $agricola,
    paso_servidumbre = $paso_servidumbre,
    tour3d = '$tour3d'

    WHERE id_formulario = $idFormularioDuplicado
    ";
    if ($conn->query($sqlForm2) === TRUE) {

      } else {
        echo json_encode("Error: " . $sqlForm2 );
        die;
      }
    // $sqlForm = "INSERT INTO formulario (fk_propietario,fk_operario,operacion,fk_tipo_propiedad,Direccion_id,fk_tipo_calefaccion,banno2,fk_corredor,divisa_monto2,divisa_monto1, fk_tiempo_publicacion,material_pisos_terraza, fk_agua_caliente,material_piso_dorm, material_piso_comun,material_pisos_cocina, material_piso_banno,fk_tipo_gas, fk_banco,fk_estado, origen1,origen2, area_redflip,fecha,condominio,exclusividad, amoblado,anno,contribuciones_trimestrales, gastos_comunes,nota_gastos_com,superficie_total,superficie_util,superficie_terraza,superficie_terreno,dormitorios_suite,Suit_Walking_Closet,desc_baños,estacionamiento,num_estacionamiento,ubicacion_est,est_subterraneo,pisos,piso_depto,cant_deptos,ascensores,bodega,num_bodega,casa_liv_separados,otros_sum,aire_acondicionado,ascensor_privado,duplex,triplex,mariposa,penthouse,atico,bodega_Esp,cocina_ame,cocina_isla,cocinaIndep,comedor_diario,cortina_hang,persiana_aluminio,cortina_roller,cortina_elec,dorm_serv,escritorio,horno_emp,jacuzzi,logia,malla_proc_terr,riego_auto,sala_estar,alarma,term_panel,lavavajillas,microondas,refrigerador,despensa,hall,planchado,cocina_amob,mansarda,walking_closet,cerco_elec,ascensor_edi,conserje_24,est_visita,gim,juegos,lavanderia,piscina,porton_elec,quincho,terraza_quincho,bar,sala_juegos,sala_reuniones,sala_multi_uso,sala_eventos,sauna,piscina_temp,gourmet_room,azotea,ascensor_comun,cancha_tenis,circ_tv,area_verde,sala_cine,patio_serv,antejardin,disponibilidad_visita,disponibilidad_entrega,notas,aprobado,cant_bannos,fin_propiedad,mascotas,cartel,encimeraGas,encimeraElect,estractor,cant_pisos,nombre_form,dptoPiso,cantCorredor,adquisicion,cocinaIntegrada,panelSolar,cant_pisos_casa,infoPropVenta,hipoteca,propDestacado,sup_patio,enviado,loteo,monto1,monto2,valorAmob,valorAmobArr,imagen,llave,bicicletero,url,rol,fecha_entrega,pareada,persiana_madera,cava_vinos,accesos_controlados,plaza,cancha_futbol,sala_juegos_ext,plantas_libres,privado,bannos_ctes,kitchenette,cocina,red_telefonia,red_computadores,iluminacion_led,red_incendio,climatizacion_vrv,puerta_magnetica,persiana_electrica,edificio_clase,sala_reuniones_of,puestos_trabajo,recepcion,estacionamientos_ctes,est_clientes_of,casino,maquina_disp,sala_capacitacion,auditorio,estacionamientos_arr,salas_reu_switch,energia_trifasica,persiana_manual,piso_of,urbanizado,agricola,paso_servidumbre,tour3d)
    // VALUES ('$idPropietario','$operario','$operacion','$fk_tipo_propiedad','$idDireccion',$fk_tipo_calefaccion,'$banno2','$corredor',$divisa_monto2,$divisa_monto1,$fk_tiempo_publicacion,$material_pisos_terraza,$fk_agua_caliente,$material_piso_dorm,$material_piso_comun,$material_pisos_cocina,$material_piso_banno,$fk_tipo_gas,$fk_banco,$fk_estado,'$origen1',$origen2,'$area_redflip','$fecha','$condominio','$exclusividad','$amoblado','$anno','$contribuciones_trimestrales','$gastos_comunes','$nota_gastos_com','$superficie_total','$superficie_util','$superficie_terraza','$superficie_terreno','$dormitorios_suite',$Suit_Walking_Closet,'$desc_baños','$estacionamiento','$num_estacionamiento','$ubicacion_est','$est_subterraneo','$pisos','$piso_depto','$cant_deptos','$ascensores','$bodega','$num_bodega','$casa_liv_separados','$otros_sum','$aire_acondicionado','$ascensor_privado','$duplex','$triplex','$mariposa','$penthouse','$atico',$bodega_esp,'$cocina_ame','$cocina_isla','$cocinaIndep','$comedor_diario','$cortina_hang','$persiana_aluminio','$cortina_roller','$cortina_elec',$dorm_serv,'$escritorio','$horno_emp','$jacuzzi','$logia','$malla_proc_terr','$riego_auto','$sala_estar','$alarma','$term_panel','$lavavajillas','$microondas','$refrigerador','$despensa','$hall','$planchado','$cocina_amob',$mansarda,$walking_closet,'$cerco_elec',$ascensor_edi,'$conserje_24','$est_visita','$gim','$juegos','$lavanderia','$piscina','$porton_elec','$quincho','$terraza_quincho','$bar','$sala_juegos','$sala_reuniones','$sala_multi_uso','$sala_eventos','$sauna','$piscina_temp','$gourmet_room','$azotea','$ascensor_comun','$cancha_tenis','$circ_tv','$area_verde','$sala_cine','$patio_serv','$antejardin','$disponibilidad_visita','$disponibilidad_entrega','$notas',1,'$cant_bannos','$fin_propiedad','$mascotas','$cartel','$encimeraGas','$encimeraElect','$estractor','$cant_pisos','$nombre_form','$dptoPiso','$cantCorredor','$adquisicion','$cocinaIntegrada','$panelSolar','$cant_pisos_casa','$infoPropVenta','$hipoteca','$propDestacado','$sup_patio','$enviado','$loteo','$monto1','$monto2','$valorAmob','$valorAmobArr','$imagen','$llave','$bicicletero','$url','$rol',$fecha_entrega,'$pareada','$persiana_madera','$cava_vinos','$accesos_controlados','$plaza','$cancha_futbol','$sala_juegos_ext','$plantas_libres','$privado','$bannos_ctes','$kitchenette','$cocina','$red_telefonia','$red_computadores','$iluminacion_led','$red_incendio','$climatizacion_vrv','$puerta_magnetica','$persiana_electrica','$edificio_clase','$sala_reuniones_of','$puestos_trabajo','$recepcion','$estacionamientos_ctes','$est_clientes_of','$casino','$maquina_disp','$sala_capacitacion','$auditorio','$estacionamientos_arr','$salas_reu_switch',$energia_trifasica,$persiana_manual,$piso_of,'$urbanizado','$agricola','$paso_servidumbre','$tour3d')";

    // if ($conn->query($sqlForm) === TRUE) {
    //     $idFormularioDuplicado = $conn->insert_id;
    // } else {
    //     $res->error = $sqlForm;
    //     echo json_encode("Error: " . $sqlForm );
    //     die;
    // }
    
    $sqlPropiedad = "INSERT INTO Propiedad (fk_propietario, 
    fk_estado_propiedad, 
    fk_status,
    id_externo, 
    fk_formulario, 
    carga,
    divisa,
    fk_venta_arriendo) 
    VALUES ('$idPropietario', 
    '$fk_estado_propiedad', 
    '$fk_status', 
    '$id_externo', 
    '$idFormularioDuplicado', 
    '$carga',
    $divisa,
    $fk_venta_arriendo)";

    if ($conn->query($sqlPropiedad) === TRUE) {
        $idPropiedad = $conn->insert_id;
    } else {
        echo json_encode($sqlPropiedad);
        die;
    }
$resp ->select_area = $select_area;
$resp ->tipo_op_select = $tipo_op_select;
$resp ->corredor = $corredor;
$resp ->operario = $operario;
$resp ->nombre = $nombre;
$resp ->apellido = $apellido;
$resp ->rut = $rut;
$resp ->origen = $origen1;
$resp ->captadaR = $captadaR;
$resp ->partnerR = $partnerR;
$resp ->sub_fono = $sub_fono;
$resp ->telefono = $telefono;
$resp ->correo = $correo;
$resp ->fecha = $fecha;
$resp ->form_mostrar = $idFormularioDuplicado;

echo json_encode($resp);
?>