<?php
$xmlstr2 .= "
<!--Ambos-->
";
$res = new \stdClass();

$sqlAmb = "SELECT  Publicacion.* , formulario.* from formulario, Propietario, Estado_prop, Sub_estado, Publicacion WHERE Sub_estado.id = 5
        AND formulario.fk_propietario = Propietario.id_propietario
        AND Propietario.fk_estado_prop = Estado_prop.id
        AND Estado_prop.fk_sub_estado = Sub_estado.id
        AND Publicacion.fk_formulario = formulario.id_formulario
        and despublicada = 0
        AND formulario.operacion = 3";

$result = $conn->query($sqlAmb);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        
            $fecha = date("d-m-Y", strtotime($row["fecha"]));
            $fecha = str_replace("-","/",$fecha);
            $tipo_propiedad = $row["fk_tipo_propiedad"];
            $operacion = $row["operacion"];
            $divisa = $row["divisa_monto1"];
            $Direccion_id = $row["Direccion_id"];
            $sqlDireccion = "SELECT * from Direccion WHERE id = $Direccion_id";
            $latitud = $row["latitud"];
            $longitud = $row["longitud"];
            $titulo = utf8_encode($row["titulo"]);
            $descripcion = utf8_encode($row["descripcion"]);
            $datos1 = $row;
            switch ($tipo_propiedad) {
                case '1': //depto
                    $tipo_prop = "Apartment";
                    break;
                case '2': //casa
                    $tipo_prop = "House";
                    break;
                case '3': //local
                    $tipo_prop = "Office";
                    break;
                case '4': //Oficina
                    $tipo_prop = "Office";
                    break;
                case '5': //terreno
                    $tipo_prop = "Office";
                    break;
                case '6': //Parcela
                    $tipo_prop = "Office";
                    break;
                case '7': //Bodega
                    $tipo_prop = "Office";
                    break;
                case '8': //Galpon
                    $tipo_prop = "Office";
                    break;
                case '9': //No Definida
                    $tipo_prop = "No";
                    break;
                case '12': //Casa Oficina
                    $tipo_prop = "Office";
                    break;
                case '13': //Departamento Oficina
                    $tipo_prop = "Office";
                    break;
                case '14': //Casa - Terreno
                    $tipo_prop = "House";
                    break;
                case '15': //Casa - Parcela
                    $tipo_prop = "House";
                    break;
                
                // default:
                //     $tipo_prop = "No";
                //     break;
            }
            $result2 = $conn->query($sqlDireccion);

            if ($result2->num_rows > 0) {
                while($row2 = $result2->fetch_assoc()) {
                        //Direccion
                        $calle = utf8_encode($row2["calle"]);
                        $numero = $row2["numero"];
                        $fk_comuna = $row2["fk_comuna"];
                }
            }else{
                $res->errorDir="error dir";
            }
            $sqlComuna = "SELECT * FROM Comuna WHERE id = $fk_comuna";

                
            $result3 = $conn->query($sqlComuna);

            if ($result3->num_rows > 0) {
                while($row3 = $result3->fetch_assoc()) {
                    $comuna = $row3["nombre"];
                }
            }else{
                $res->errorComuna = "Error en comuna";
            }

            $orientacion = "";
            $sqlOrientacion = "SELECT * from Orientacion WHERE fk_formulario = " . $row["id_formulario"];
            $result4 = $conn->query($sqlOrientacion);

            if ($result4->num_rows > 0) {
                while($row4 = $result4->fetch_assoc()) {
                    if($row4["norte"] == 1){
                        $orientacion = "Norte";
                    }
                    if($row4["sur"] == 1){
                        $orientacion = "Sur";
                    }
                    if($row4["oriente"] == 1){
                        $orientacion = "Oriente";
                    }
                    if($row4["poniente"] == 1){
                        $orientacion = "Poniente";
                    }
                    if($row4["norOriente"] == 1){
                        $orientacion = "Nor-Oriente";
                    }
                    if($row4["oriente"] == 1){
                        $orientacion = "Oriente";
                    }
                    if($row4["oriente"] == 1){
                        $orientacion = "Oriente";
                    }
                    if($row4["oriente"] == 1){
                        $orientacion = "Oriente";
                    }
                }
            }else{
                $res->errorOrientacion = "Error en orientacion";
            }


        for ($i1=0; $i1 < 2; $i1++) {
            $div1 = $row["divisa_monto1"];
            $div2 = $row["divisa_monto2"];
            switch ($div1) {
                case '1':
                    $div1 = "UF";
                    break;
                case '2':
                    $div1 = "CLP";
                    break;
                case '3':
                    $div1 = "USD";
                    break;
                
                default:
                    $div1 = "No";
                    break;
            }
            switch ($div2) {
                case '1':
                    $div2 = "UF";
                    break;
                case '2':
                    $div2 = "CLP";
                    break;
                case '3':
                    $div2 = "USD";
                    break;
                
                default:
                    $div2 = "No";
                    break;
            }
            $sqlBannoServ = "SELECT * FROM Bannos where Bannos.fk_tipo_banno = 4 AND fk_formulario = ". $row["id_formulario"];
            $result5 = $conn->query($sqlBannoServ);

            if ($result5->num_rows > 0) {
                while($row5 = $result5->fetch_assoc()) {
                    $bann_serv = true;
                }
            }else{
                $bann_serv = false;
            }
            $sqlHabServ = "SELECT * FROM Dormitorios where fk_formulario = ".$row["id_formulario"]." AND Dormitorios.servicio = 1";
            $result6 = $conn->query($sqlHabServ);

            if ($result6->num_rows > 0) {
                while($row6 = $result6->fetch_assoc()) {
                    $hab_serv = true;
                }
            }else{
                $hab_serv = false;
            }
            if($div1 != "No" && $div2 != "No"){
                //obtener orden de imagenes
                $orden_img = explode(',',$datos1["orden_img"]);

                $xmlstr2.= "
                <ad>
                <id><![CDATA[ ".$row['id_formulario']." ]]></id>
                <mls_database><![CDATA[ 102158561 ]]></mls_database>
                <url><![CDATA[".$row["url"]."]]></url>
                <title><![CDATA[".$titulo."]]></title>
                <publication_date><![CDATA[ ".$fecha." ]]></publication_date>
                <update_date><![CDATA[ ".getdate()["mday"]."/" .getdate()["mon"] ."/".getdate()["year"] ." ]]></update_date>
                <properties>

                    <propertytype><![CDATA[ ".$tipo_prop." ]]></propertytype>
                    <transactiontype><![CDATA[ ".(($i1==0)?'For Sale':'For Rent')." ]]></transactiontype>
                    <content><![CDATA[".($descripcion)."]]>
                    </content>
                    <price><![CDATA[ ".(($i1==0)?$row["monto1"]:$row["monto2"])." ]]></price>
                    <currency><![CDATA[ ".(($i1==0)?$div1:$div2)." ]]></currency>
                    <rooms><![CDATA[ ".$row["dormitorios"]." ]]></rooms>
                    <bathrooms><![CDATA[ ".$row["cant_bannos"]." ]]></bathrooms>
                    <floor_area><![CDATA[ ".$row["superficie_util"]." ]]></floor_area>
                    <plot_area><![CDATA[ ".$row["superficie_total"]." ]]></plot_area>
                    <year><![CDATA[".$row["anno"]."]]></year>
                    <is_new><![CDATA[ false ]]></is_new>
                    <rol><![CDATA[ ".$row["rol"]." ]]></rol>
                    <common_expenses><![CDATA[ ".$row["gastos_comunes"]." ]]></common_expenses>
                    <currency_expenses><![CDATA[ CLP ]]></currency_expenses>
                    <floor_number><![CDATA[ ".$row["piso_depto"]." ]]></floor_number>
                    <orientation><![CDATA[ Sur-Oriente ]]></orientation>
                    <parking><![CDATA[ ".$row["estacionamiento"]." ]]></parking>
                    <cellar><![CDATA[ ".$row["bodega"]." ]]></cellar>
                    <is_furnished><![CDATA[ ".(($row["amoblado"] == 0)?'false':'true')." ]]></is_furnished>
                    <daily><![CDATA[ false ]]></daily>
                    <video><![CDATA[ ]]></video>
                    <blueprint><![CDATA[  ]]></blueprint>

                </properties>

                <location>

                    <address><![CDATA[  $calle $numero ]]></address>
                    <neighborhood><![CDATA[  ]]></neighborhood>
                    <city_area><![CDATA[ $comuna ]]></city_area>
                    <city><![CDATA[ Santiago ]]></city>
                    <state><![CDATA[ Metropolitana ]]></state>
                    <country><![CDATA[ Chile ]]> </country>
                    <latitude> <![CDATA[ $latitud ]]> </latitude>
                    <longitude> <![CDATA[ $longitud ]]> </longitude>

                </location>

                <agency>

                    <by_owner> <![CDATA[ false ]]> </by_owner>
                    <agency_name> <![CDATA[ Redflip ]]> </agency_name>
                    <agency_office> <![CDATA[ Redflip ]]> </agency_office>
                    <agency_id> <![CDATA[ 1 ]]>  </agency_id>
                    <agency_logo> <![CDATA[  https://indev9.com/contenido-redflip/assets/img/Logo-Redflip-Sin-Borde.png  ]]> </agency_logo>
                    <agency_address> <![CDATA[  Badajoz 100, Las Condes, Santiago, Chile  ]]> </agency_address>
                    <contact_name> <![CDATA[ Maria Jose ]]> </contact_name>
                    <contact_phones> <![CDATA[  +569 9879 4479 ]]> </contact_phones>
                    <contact_email> <![CDATA[ mjadauy@redflip.com ]]> </contact_email>

                </agency>

                <features>";

                if($row["sup_patio"] > 0 ){ //patio
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 1 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }

                if($row["superficie_terraza"] > 0 ){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 2 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 2 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                // no tenemos opcion de lucarna  / claraboya (3)
                // no tenemos opcion de chimenea (4)

                if($row["term_panel"] == 1){ //termo panel
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 5 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 5 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                if($bann_serv){ // baño de servicio "result5"
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 6 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 6 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                if($hab_serv){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 7 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 7 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                //No hay opcion de calefaccion central (8)
                //No hay opcion cerca a via principal (9)
                //No hay opcion transporte urbano cercano (10)

                if($row["ascensor_edi"] == 1){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 11 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{

                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 11 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                //No hay opcion cerca de colegio (12)
                //No hay opcion cerca de comercio (13)
                //No hay opcion cerca a areas verdes (14)

                if($row["alarma"] == 1){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 15 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 15 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                if($row["sala_multi_uso"] == 1){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 16 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 16 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                if($row["piscina"] == 1){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 17 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 17 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                if($row["sauna"] == 1){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 18 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 18 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                if($row["jacuzzi"] == 1){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 19 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 19 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                // No hay opcion mesa de pool (20)

                if($row["terraza_quincho"] == 1){ //terraza
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 21 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 21 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                if($row["gim"] == 1){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 22 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 22 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                //No hay opcion seguridad 24hrs (23)

                if($row["condominio"] == 1){
                    $xmlstr2.="
                    <feature>
                        <id> <![CDATA[ 24 ]]> </id>
                        <value> <![CDATA[ true ]]> </value>
                    </feature>";
                }else{
                    // $xmlstr2.="
                    // <feature>
                    //     <id> <![CDATA[ 24 ]]> </id>
                    //     <value> <![CDATA[ false ]]> </value>
                    // </feature>";
                }

                $xmlstr2.="</features>
                ";

                $xmlstr2.="<pictures>
                ";

                for ($i=0; $i <count($orden_img) ; $i++) { 
                
                    $xmlstr2.="
                    <picture>
                        <picture_url_large><![CDATA[http://indev9.com/redflip/publicacion/goPlaceIt/".$datos1["id_formulario"]."/".$orden_img[$i]."]]></picture_url_large>
                        <picture_title></picture_title>
                    </picture>";
                }
                $xmlstr2.= "
                </pictures>
                </ad>"
                ;
            }else{
                echo "no";
            }
        }
    }
}

// echo($xmlstr2);
$xmlstr2 .= <<<XML

</goplaceit>
XML;
$xmlstr2 = $xmlstr . $xmlstr2;
$nombre = "../publicacion/goPlaceIt/goplaceit.xml";
unlink($nombre);
$archivo = fopen($nombre, "a+");

fwrite($archivo,$xmlstr2);
fclose($archivo);
?>