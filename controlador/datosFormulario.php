<?php
    include '../conexion.php';
    $idForm = $_REQUEST["idForm"];

    $sql = "SELECT *, formulario.fk_corredor as corredor,
    formulario.fk_operario as oper,
    formulario.bodega as bode,
    formulario.dormitorios as dorm,
    formulario.estacionamiento as estac,
    formulario.fk_tipo_propiedad as fk_tipo_prop, Direccion.calle as dir_calle, Direccion.numero as dir_num, Direccion.referencia as dir_ref, Direccion.fk_comuna as dir_com, Direccion.letra as dir_letra from
    formulario,
    Propiedad,
    Direccion,
    Propietario,
    Persona
    WHERE
    formulario.id_formulario = $idForm and
    formulario.id_formulario = Propiedad.fk_formulario
    AND Direccion.id = formulario.Direccion_id
    AND Propietario.id_propietario = formulario.fk_propietario
    AND Persona.id_persona = Propietario.fk_persona";

    $result = $conn->query($sql);

    if ($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            //formulario
            $fk_propietario = $row["fk_propietario"];
            $fk_operario = $row["oper"];
            $operacion = $row["operacion"];
            $fk_tipo_propiedad = $row["fk_tipo_prop"];
            $Direccion_id = $row["Direccion_id"];
            $fk_tipo_calefaccion = $row["fk_tipo_calefaccion"];
            $banno2 = $row["banno2"];
            $Aprobado_por = $row["Aprobado_por"];
            $fk_corredor = $row["corredor"];
            $usuario = $row["usuario"];
            $divisa_monto2 = $row["divisa_monto2"];
            $divisa_monto1 = $row["divisa_monto1"];
            $fk_tiempo_publicacion = $row["fk_tiempo_publicacion"];
            $material_pisos_terraza = $row["material_pisos_terraza"];
            $fk_agua_caliente = $row["fk_agua_caliente"];
            $material_piso_dorm = $row["material_piso_dorm"];
            $material_piso_comun = $row["material_piso_comun"];
            $material_pisos_cocina = $row["material_pisos_cocina"];
            $material_piso_banno = $row["material_piso_banno"];
            $fk_tipo_gas = $row["fk_tipo_gas"];
            $fk_banco = $row["fk_banco"];
            $fk_estado = $row["fk_estado"];
            $origen1 = $row["origen1"];
            $origen2 = $row["origen2"];
            $area_redflip = $row["area_redflip"];
            $fecha = $row["fecha"];
            $condominio = $row["condominio"];
            $exclusividad = $row["exclusividad"];
            $amoblado = $row["amoblado"];
            $anno = $row["anno"];
            $contribuciones_trimestrales = $row["contribuciones_trimestrales"];
            $gastos_comunes = $row["gastos_comunes"];
            $nota_gastos_com = $row["nota_gastos_com"];
            $superficie_total = $row["superficie_total"];
            $superficie_util = $row["superficie_util"];
            $superficie_terraza = $row["superficie_terraza"];
            $superficie_terreno = $row["superficie_terreno"];
            $dormitorios = $row["dorm"];
            $dormitorios_suite = $row["dormitorios_suite"];
            $Suit_Walking_Closet = $row["Suit_Walking_Closet"];
            $desc_baños = $row["desc_baños"];
            $estacionamiento = $row["estac"];
            $num_estacionamiento = $row["num_estacionamiento"];
            $ubicacion_est = $row["ubicacion_est"];
            $est_subterraneo = $row["est_subterraneo"];
            $pisos = $row["pisos"];
            $piso_depto = $row["piso_depto"];
            $cant_deptos = $row["cant_deptos"];
            $ascensores = $row["ascensores"];
            $bodega = $row["bode"];
            $num_bodega = $row["num_bodega"];
            $casa_liv_separados = $row["casa_liv_separados"];
            $otros_sum = $row["otros_sum"];
            $aire_acondicionado = $row["aire_acondicionado"];
            $ascensor_privado = $row["ascensor_privado"];
            $duplex = $row["duplex"];
            $triplex = $row["triplex"];
            $mariposa = $row["mariposa"];
            $penthouse = $row["penthouse"];
            $atico = $row["atico"];
            $bodega_esp = $row["bodega_esp"];
            $cocina_ame = $row["cocina_ame"];
            $cocina_enci = $row["cocina_enci"];
            $cocina_isla = $row["cocina_isla"];
            $cocinaIndep = $row["cocinaIndep"];
            $comedor_diario = $row["comedor_diario"];
            $cortina_hang = $row["cortina_hang"];
            $persiana_aluminio = $row["persiana_aluminio"];
            $cortina_roller = $row["cortina_roller"];
            $cortina_elec = $row["cortina_elec"];
            $dorm_serv = $row["dorm_serv"];
            $escritorio = $row["escritorio"];
            $horno_emp = $row["horno_emp"];
            $jacuzzi = $row["jacuzzi"];
            $logia = $row["logia"];
            $malla_proc_terr = $row["malla_proc_terr"];
            $riego_auto = $row["riego_auto"];
            $sala_estar = $row["sala_estar"];
            $alarma = $row["alarma"];
            $term_panel = $row["term_panel"];
            $lavavajillas = $row["lavavajillas"];
            $microondas = $row["microondas"];
            $refrigerador = $row["refrigerador"];
            $despensa = $row["despensa"];
            $hall = $row["hall"];
            $planchado = $row["planchado"];
            $cocina_amob = $row["cocina_amob"];
            $citofono = $row["citofono"];
            $mansarda = $row["mansarda"];
            $walking_closet = $row["walking_closet"];
            $cerco_elec = $row["cerco_elec"];
            $ascensor_edi = $row["ascensor_edi"];
            $conserje_24 = $row["conserje_24"];
            $est_visita = $row["est_visita"];
            $gim = $row["gim"];
            $juegos = $row["juegos"];
            $lavanderia = $row["lavanderia"];
            $piscina = $row["piscina"];
            $porton_elec = $row["porton_elec"];
            $quincho = $row["quincho"];
            $terraza_quincho = $row["terraza_quincho"];
            $bar = $row["bar"];
            $sala_juegos = $row["sala_juegos"];
            $sala_reuniones = $row["sala_reuniones"];
            $sala_multi_uso = $row["sala_multi_uso"];
            $sala_eventos = $row["sala_eventos"];
            $sauna = $row["sauna"];
            $piscina_temp = $row["piscina_temp"];
            $gourmet_room = $row["gourmet_room"];
            $azotea = $row["azotea"];
            $ascensor_comun = $row["ascensor_comun"];
            $cancha_tenis = $row["cancha_tenis"];
            $circ_tv = $row["circ_tv"];
            $area_verde = $row["area_verde"];
            $sala_cine = $row["sala_cine"];
            $patio_serv = $row["patio_serv"];
            $antejardin = $row["antejardin"];
            $disponibilidad_visita = $row["disponibilidad_visita"];
            $disponibilidad_entrega = $row["disponibilidad_entrega"];
            $notas = $row["notas"];
            $aprobado = $row["aprobado"];
            $cant_bannos = $row["cant_bannos"];
            $fin_propiedad = $row["fin_propiedad"];
            $mascotas = $row["mascotas"];
            $cartel = $row["cartel"];
            $encimeraGas = $row["encimeraGas"];
            $encimeraElect = $row["encimeraElect"];
            $estractor = $row["estractor"];
            $cant_pisos = $row["cant_pisos"];
            $nombre_form = $row["nombre_form"];
            $dptoPiso = $row["dptoPiso"];
            $cantCorredor = $row["cantCorredor"];
            $adquisicion = $row["adquisicion"];
            $cocinaIntegrada = $row["cocinaIntegrada"];
            $panelSolar = $row["panelSolar"];
            $cant_pisos_casa = $row["cant_pisos_casa"];
            $infoPropVenta = $row["infoPropVenta"];
            $hipoteca = $row["hipoteca"];
            $propDestacado = $row["propDestacado"];
            $sup_patio = $row["sup_patio"];
            $enviado = $row["enviado"];
            $loteo = $row["loteo"];
            $monto1 = $row["monto1"];
            $monto2 = $row["monto2"];
            $valorAmob = $row["valorAmob"];
            $valorAmobArr = $row["valorAmobArr"];
            $imagen = $row["imagen"];
            $llave = $row["llave"];
            $bicicletero = $row["bicicletero"];
            $url = $row["url"];
            $rol = $row["rol"];
            $fecha_entrega = $row["fecha_entrega"];
            $pareada = $row["pareada"];
            $persiana_madera = $row["persiana_madera"];
            $cava_vinos = $row["cava_vinos"];
            $accesos_controlados = $row["accesos_controlados"];
            $plaza = $row["plaza"];
            $cancha_futbol = $row["cancha_futbol"];
            $cancha_golf = $row["cancha_golf"];
            $sala_juegos_ext = $row["sala_juegos_ext"];
            $plantas_libres = $row["plantas_libres"];
            $privado = $row["privado"];
            $bannos_ctes = $row["bannos_ctes"];
            $kitchenette = $row["kitchenette"];
            $cocina = $row["cocina"];
            $red_telefonia = $row["red_telefonia"];
            $red_computadores = $row["red_computadores"];
            $iluminacion_led = $row["iluminacion_led"];
            $red_incendio = $row["red_incendio"];
            $climatizacion_vrv = $row["climatizacion_vrv"];
            $puerta_magnetica = $row["puerta_magnetica"];
            $persiana_electrica = $row["persiana_electrica"];
            $edificio_clase = $row["edificio_clase"];
            $sala_reuniones = $row["sala_reuniones"];
            $puestos_trabajo = $row["puestos_trabajo"];
            $recepcion = $row["recepcion"];
            $estacionamientos_ctes = $row["estacionamientos_ctes"];
            $est_clientes_of = $row["est_clientes_of"];
            $casino = $row["casino"];
            $cafeteria = $row["cafeteria"];
            $maquina_disp = $row["maquina_disp"];
            $sala_capacitacion = $row["sala_capacitacion"];
            $auditorio = $row["auditorio"];
            $estacionamientos_arr = $row["estacionamientos_arr"];
            $salas_reu_switch = $row["salas_reu_switch"];
            $energia_trifasica = $row["energia_trifasica"];
            $recepcion_edificio = $row["recepcion_edificio"];
            $persiana_manual = $row["persiana_manual"];
            $piso_of = $row["piso_of"];
            $urbanizado = $row["urbanizado"];
            $agricola = $row["agricola"];
            $paso_servidumbre = $row["paso_servidumbre"];
            $tour3d = $row["tour3d"];

            //persona (5 royale UWU)
            $id_persona = $row["id_persona"];
            $nombre = $row["nombre"];
            $apellido = $row["apellido"];
            $rut = $row["rut"];
            $fec_nac = $row["fec_nac"];
            $telefono = $row["telefono"];
            $correo = $row["correo"];

            //usuario
            $id_usuario = $row["id_usuario"];
            $nom_us = $row["nom_us"];
            $pass = $row["pass"];
            $fk_rol = $row["fk_rol"];
            $fk_estado_us = $row["fk_estado_us"];
            $fk_persona = $row["fk_persona"];
            $ultima_pag = $row["ultima_pag"];

            //propietario
            $id_propietario = $row["id_propietario"];
            $fk_persona = $row["fk_persona"];
            $cont = $row["cont"];
            $fk_estado_prop = $row["fk_estado_prop"];
            $eliminado = $row["eliminado"];
            $fec_creacion = $row["fec_creacion"];
            $fk_direccion = $row["fk_direccion"];
            $contactos = $row["contactos"];
            $fk_origen = $row["fk_origen"];
            $aprobado = $row["aprobado"];
            $fk_contacto = $row["fk_contacto"];

            //direccion
            $id = $row["id"];
            $calle = $row["calle"];
            $numero = $row["numero"];
            $referencia = $row["referencia"];
            $fk_comuna = $row["fk_comuna"];
            $letra = $row["letra"];

            //propiedad
            $id_propiedad = $row["id_propiedad"];
            $bannos = $row["bannos"];
            $metros_totales = $row["metros_totales"];
            $metros_utiles = $row["metros_utiles"];
            $monto = $row["monto"];
            $fk_propietario = $row["fk_propietario"];
            $fk_estado_propiedad = $row["fk_estado_propiedad"];
            $fk_lista_administrativo = $row["fk_lista_administrativo"];
            $fk_lista_servicio = $row["fk_lista_servicio"];
            $fk_direccion = $row["fk_direccion"];
            $divisa = $row["divisa"];
            $fk_venta_arriendo = $row["fk_venta_arriendo"];
            $nota = $row["nota"];
            $fk_evento = $row["fk_evento"];
            $fk_status = $row["fk_status"];
            $id_externo = $row["id_externo"];
            $fk_formulario = $row["fk_formulario"];
            $carga = $row["carga"];
        }
    }else{
        
    }

    $sqlCorredor="Select Corredor.id_corredor, Persona.nombre, Persona.apellido, Persona.correo from Corredor, Persona where Corredor.fk_persona = Persona.id_persona AND eliminado != 1";
    $corredoresArr = array();
    $result = $conn->query($sqlCorredor);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()){
            array_push($corredoresArr, array(id => $row["id_corredor"],
                                             nombre => utf8_encode($row["nombre"]) . " " . utf8_encode($row["apellido"]),
                                             correo => $row["correo"]
        ));
        }
    }

    $sqlOperario = "Select Operario.id_operario, Persona.nombre, Persona.apellido from Operario, Persona where Operario.fk_persona = Persona.id_persona";
    $operariosArr = array();
    $result = $conn->query($sqlOperario);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()){
            array_push($operariosArr, array(id => $row["id_operario"],
                                             nombre => utf8_encode($row["nombre"]) . " " . utf8_encode($row["apellido"])
        ));
        }
    }

    $res->fk_propietario = $fk_propietario;
    $res->fk_operario = $fk_operario;
    $res->operacion = $operacion;
    $res->fk_tipo_propiedad = $fk_tipo_propiedad;
    $res->Direccion_id = $Direccion_id;
    $res->fk_tipo_calefaccion = $fk_tipo_calefaccion;
    $res->banno2 = $banno2;
    $res->Aprobado_por = $Aprobado_por;
    $res->fk_corredor = $fk_corredor;
    $res->usuario = $usuario;
    $res->divisa_monto2 = $divisa_monto2;
    $res->divisa_monto1 = $divisa_monto1;
    $res->fk_tiempo_publicacion = $fk_tiempo_publicacion;
    $res->material_pisos_terraza = $material_pisos_terraza;
    $res->fk_agua_caliente = $fk_agua_caliente;
    $res->material_piso_dorm = $material_piso_dorm;
    $res->material_piso_comun = $material_piso_comun;
    $res->material_pisos_cocina = $material_pisos_cocina;
    $res->material_piso_banno = $material_piso_banno;
    $res->fk_tipo_gas = $fk_tipo_gas;
    $res->fk_banco = $fk_banco;
    $res->fk_estado = $fk_estado;
    $res->origen1 = $origen1;
    $res->origen2 = $origen2;
    $res->area_redflip = $area_redflip;
    $res->fecha = $fecha;
    $res->condominio = $condominio;
    $res->exclusividad = $exclusividad;
    $res->amoblado = $amoblado;
    $res->anno = $anno;
    $res->contribuciones_trimestrales = $contribuciones_trimestrales;
    $res->gastos_comunes = $gastos_comunes;
    $res->nota_gastos_com = $nota_gastos_com;
    $res->superficie_total = $superficie_total;
    $res->superficie_util = $superficie_util;
    $res->superficie_terraza = $superficie_terraza;
    $res->superficie_terreno = $superficie_terreno;
    $res->dormitorios = $dormitorios;
    $res->dormitorios_suite = $dormitorios_suite;
    $res->Suit_Walking_Closet = $Suit_Walking_Closet;
    $res->desc_baños = $desc_baños;
    $res->estacionamiento = $estacionamiento;
    $res->num_estacionamiento = $num_estacionamiento;
    $res->ubicacion_est = $ubicacion_est;
    $res->est_subterraneo = $est_subterraneo;
    $res->pisos = $pisos;
    $res->piso_depto = $piso_depto;
    $res->cant_deptos = $cant_deptos;
    $res->ascensores = $ascensores;
    $res->bodega = $bodega;
    $res->num_bodega = $num_bodega;
    $res->casa_liv_separados = $casa_liv_separados;
    $res->otros_sum = $otros_sum;
    $res->aire_acondicionado = $aire_acondicionado;
    $res->ascensor_privado = $ascensor_privado;
    $res->duplex = $duplex;
    $res->triplex = $triplex;
    $res->mariposa = $mariposa;
    $res->penthouse = $penthouse;
    $res->atico = $atico;
    $res->bodega_esp = $bodega_esp;
    $res->cocina_ame = $cocina_ame;
    $res->cocina_enci = $cocina_enci;
    $res->cocina_isla = $cocina_isla;
    $res->cocinaIndep = $cocinaIndep;
    $res->comedor_diario = $comedor_diario;
    $res->cortina_hang = $cortina_hang;
    $res->persiana_aluminio = $persiana_aluminio;
    $res->cortina_roller = $cortina_roller;
    $res->cortina_elec = $cortina_elec;
    $res->dorm_serv = $dorm_serv;
    $res->escritorio = $escritorio;
    $res->horno_emp = $horno_emp;
    $res->jacuzzi = $jacuzzi;
    $res->logia = $logia;
    $res->malla_proc_terr = $malla_proc_terr;
    $res->riego_auto = $riego_auto;
    $res->sala_estar = $sala_estar;
    $res->alarma = $alarma;
    $res->term_panel = $term_panel;
    $res->lavavajillas = $lavavajillas;
    $res->microondas = $microondas;
    $res->refrigerador = $refrigerador;
    $res->despensa = $despensa;
    $res->hall = $hall;
    $res->planchado = $planchado;
    $res->cocina_amob = $cocina_amob;
    $res->citofono = $citofono;
    $res->mansarda = $mansarda;
    $res->walking_closet = $walking_closet;
    $res->cerco_elec = $cerco_elec;
    $res->ascensor_edi = $ascensor_edi;
    $res->conserje_24 = $conserje_24;
    $res->est_visita = $est_visita;
    $res->gim = $gim;
    $res->juegos = $juegos;
    $res->lavanderia = $lavanderia;
    $res->piscina = $piscina;
    $res->porton_elec = $porton_elec;
    $res->quincho = $quincho;
    $res->terraza_quincho = $terraza_quincho;
    $res->bar = $bar;
    $res->sala_juegos = $sala_juegos;
    $res->sala_reuniones = $sala_reuniones;
    $res->sala_multi_uso = $sala_multi_uso;
    $res->sala_eventos = $sala_eventos;
    $res->sauna = $sauna;
    $res->piscina_temp = $piscina_temp;
    $res->gourmet_room = $gourmet_room;
    $res->azotea = $azotea;
    $res->ascensor_comun = $ascensor_comun;
    $res->cancha_tenis = $cancha_tenis;
    $res->circ_tv = $circ_tv;
    $res->area_verde = $area_verde;
    $res->sala_cine = $sala_cine;
    $res->patio_serv = $patio_serv;
    $res->antejardin = $antejardin;
    $res->disponibilidad_visita = $disponibilidad_visita;
    $res->disponibilidad_entrega = $disponibilidad_entrega;
    $res->notas = $notas;
    $res->aprobado = $aprobado;
    $res->cant_bannos = $cant_bannos;
    $res->fin_propiedad = $fin_propiedad;
    $res->mascotas = $mascotas;
    $res->cartel = $cartel;
    $res->encimeraGas = $encimeraGas;
    $res->encimeraElect = $encimeraElect;
    $res->estractor = $estractor;
    $res->cant_pisos = $cant_pisos;
    $res->nombre_form = $nombre_form;
    $res->dptoPiso = $dptoPiso;
    $res->cantCorredor = $cantCorredor;
    $res->adquisicion = $adquisicion;
    $res->cocinaIntegrada = $cocinaIntegrada;
    $res->panelSolar = $panelSolar;
    $res->cant_pisos_casa = $cant_pisos_casa;
    $res->infoPropVenta = $infoPropVenta;
    $res->hipoteca = $hipoteca;
    $res->propDestacado = $propDestacado;
    $res->sup_patio = $sup_patio;
    $res->enviado = $enviado;
    $res->loteo = $loteo;
    $res->monto1 = $monto1;
    $res->monto2 = $monto2;
    $res->valorAmob = $valorAmob;
    $res->valorAmobArr = $valorAmobArr;
    $res->imagen = $imagen;
    $res->llave = $llave;
    $res->bicicletero = $bicicletero;
    $res->url = $url;
    $res->rol = $rol;
    $res->fecha_entrega = $fecha_entrega;
    $res->pareada = $pareada;
    $res->persiana_madera = $persiana_madera;
    $res->cava_vinos = $cava_vinos;
    $res->accesos_controlados = $accesos_controlados;
    $res->plaza = $plaza;
    $res->cancha_futbol = $cancha_futbol;
    $res->cancha_golf = $cancha_golf;
    $res->sala_juegos_ext = $sala_juegos_ext;
    $res->plantas_libres = $plantas_libres;
    $res->privado = $privado;
    $res->bannos_ctes = $bannos_ctes;
    $res->kitchenette = $kitchenette;
    $res->cocina = $cocina;
    $res->red_telefonia = $red_telefonia;
    $res->red_computadores = $red_computadores;
    $res->iluminacion_led = $iluminacion_led;
    $res->red_incendio = $red_incendio;
    $res->climatizacion_vrv = $climatizacion_vrv;
    $res->puerta_magnetica = $puerta_magnetica;
    $res->persiana_electrica = $persiana_electrica;
    $res->edificio_clase = $edificio_clase;
    $res->sala_reuniones = $sala_reuniones;
    $res->puestos_trabajo = $puestos_trabajo;
    $res->recepcion = $recepcion;
    $res->estacionamientos_ctes = $estacionamientos_ctes;
    $res->est_clientes_of = $est_clientes_of;
    $res->casino = $casino;
    $res->cafeteria = $cafeteria;
    $res->maquina_disp = $maquina_disp;
    $res->sala_capacitacion = $sala_capacitacion;
    $res->auditorio = $auditorio;
    $res->estacionamientos_arr = $estacionamientos_arr;
    $res->salas_reu_switch = $salas_reu_switch;
    $res->energia_trifasica = $energia_trifasica;
    $res->recepcion_edificio = $recepcion_edificio;
    $res->persiana_manual = $persiana_manual;
    $res->piso_of = $piso_of;
    $res->urbanizado = $urbanizado;
    $res->agricola = $agricola;
    $res->paso_servidumbre = $paso_servidumbre;
    $res->tour3d = $tour3d;

    //persona (5 royale UWU)
    $res->id_persona = $id_persona;
    $res->nombre = $nombre;
    $res->apellido = $apellido;
    $res->rut = $rut;
    $res->fec_nac = $fec_nac;
    $res->telefono = $telefono;
    $res->correo = $correo;

    //usuario
    $res->id_usuario = $id_usuario;
    $res->nom_us = $nom_us;
    $res->pass = $pass;
    $res->fk_rol = $fk_rol;
    $res->fk_estado_us = $fk_estado_us;
    $res->fk_persona = $fk_persona;
    $res->ultima_pag = $ultima_pag;

    //propietario
    $res->id_propietario = $id_propietario;
    $res->fk_persona = $fk_persona;
    $res->cont = $cont;
    $res->fk_estado_prop = $fk_estado_prop;
    $res->eliminado = $eliminado;
    $res->fec_creacion = $fec_creacion;
    $res->fk_direccion = $fk_direccion;
    $res->contactos = $contactos;
    $res->fk_origen = $fk_origen;
    $res->aprobado = $aprobado;
    $res->fk_contacto = $fk_contacto;

    //direccion
    $res->id = $id;
    $res->calle = $calle;
    $res->numero = $numero;
    $res->referencia = $referencia;
    $res->fk_comuna = $fk_comuna;
    $res->letra = $letra;

    //propiedad
    $res->id_propiedad  = $id_propiedad;
    $res->dormitorios  = $dormitorios;
    $res->bannos  = $bannos;
    $res->estacionamiento  = $estacionamiento;
    $res->metros_totales  = $metros_totales;
    $res->metros_utiles  = $metros_utiles;
    $res->bodega  = $bodega;
    $res->monto  = $monto;
    $res->fk_propietario  = $fk_propietario;
    $res->fk_estado_propiedad  = $fk_estado_propiedad;
    $res->fk_lista_administrativo  = $fk_lista_administrativo;
    $res->fk_lista_servicio  = $fk_lista_servicio;
    $res->fk_direccion  = $fk_direccion;
    $res->divisa  = $divisa;
    $res->fk_venta_arriendo  = $fk_venta_arriendo;
    $res->nota = $nota;
    $res->fk_evento  = $fk_evento;
    $res->fk_status  = $fk_status;
    $res->fk_operario  = $fk_operario;
    $res->fk_tipo_propiedad  = $fk_tipo_propiedad;
    $res->id_externo  = $id_externo;
    $res->fk_formulario  = $fk_formulario;
    $res->carga  = $carga;

    //corredores
    $res->corredores = $corredoresArr;

    //operarios
    $res->operarios= $operariosArr;
    
    $conn->close();
    
    echo json_encode($res);
?>