<?php
include "../conexion.php";
$idCorredor=$_GET["id"];
$InteresadosArray = array();
$resp = array();
$cont = 0;
$error = "";


$sqlCorredor = 'SELECT Interesado.id_interesado FROM Interesado, Corredor WHERE Interesado.fk_corredor = Corredor.id_corredor AND Corredor.id_corredor ='. $idCorredor;
$result = $conn->query($sqlCorredor);
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
      array_push($InteresadosArray, $row["id_interesado"]);
  }
}else{
    $error = $sqlCorredor;
}
$contadorInteresado = 0;
foreach ($InteresadosArray as $id) {


$sqlPerf = "SELECT Perfil_busqueda.* from Perfil_busqueda, Interesado where fk_interesado = $id";
$result = $conn->query($sqlPerf);

if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
      $contadorInteresado++;
    $dormDesde = $row["dormDesde"];
            $dormHasta = $row["dormHasta"];
            $Edorm = $row["Edorm"];
            $bannosDesde = $row["bannosDesde"];
            $bannosHasta = $row["bannosHasta"];
            $Ebanno = $row["Ebanno"];
            $m_utilesDesde = $row["m_utilesDesde"];
            $m_utilesHasta = $row["m_utilesHasta"];
            $EmUtiles = $row["EmUtiles"];
            $precio_min = $row["precio_min"];
            // $precio_min = str_replace('.', '', $precio_min);
            $precio_max = $row["precio_max"];
            // $precio_max = str_replace('.', '', $precio_max);
            $comuna = $row["comuna"];
            $orientacion = $row["orientacion"];
            $fk_tipo_propiedad = $row["fk_tipo_propiedad"];
            $m_terrazaDesde = $row["m_terrazaDesde"];
            $m_terrazaHasta = $row["m_terrazaHasta"];
            $Eterraza = $row["Eterraza"];
            $m_terrenoDesde = $row["m_terrenoDesde"];
            $m_terrenoHasta = $row["m_terrenoHasta"];
            $Eterreno = $row["Eterreno"];
            $bodega = $row["bodega"];
            $Ebod = $row["Ebod"];
            $estacionamientoDesde = $row["estacionamientoDesde"];
            $estacionamientoHasta = $row["estacionamientoHasta"];
            $Eestacionamiento = $row["Eestacionamiento"];
            $piso_depto = $row["piso_depto"];
            $piscina = $row["piscina"];
            $Episcina = $row["Episcina"];
            $cocina_americana = $row["cocina_americana"];
            $EcocinaAme = $row["EcocinaAme"];
            $cocina_isla = $row["cocina_isla"];
            $EcocinaIsla = $row["EcocinaIsla"];
            $comedor_diario = $row["comedor_diario"];
            $EcomedorDiario = $row["EcomedorDiario"];
            $termopanel = $row["termopanel"];
            $Etermopanel = $row["Etermopanel"];
            $amoblado = $row["amoblado"];
            $Eamoblado = $row["Eamoblado"];
            $mascotas = $row["mascotas"];
            $Emascotas = $row["Emascotas"];
            $gimnasio = $row["gimnasio"];
            $Egim = $row["Egim"];
            $dorm_servicio = $row["dorm_servicio"];
            $Edorm_servicio = $row["Edorm_servicio"];
            $quincho = $row["quincho"];
            $Equincho = $row["Equincho"];
            $tipo_operacion = $row["fk_tipo_operacion"];
            $fk_interesado = $row["fk_interesado"];
  }
} else {
    // echo $sqlPerf;
}
$contErrores = 0;
$sqlMatch = "SELECT distinct  Perfil_busqueda.fk_interesado, formulario.id_formulario, formulario.dormitorios AS form_dorm, formulario.cant_bannos, formulario.superficie_util, monto1, monto2, formulario.superficie_terraza, superficie_terreno, formulario.bodega, estacionamiento, formulario.piso_depto, formulario.piscina FROM
Perfil_busqueda , formulario,Direccion 
WHERE
formulario.fk_corredor = $idCorredor and
formulario.Direccion_id = Direccion.id AND 
";
$comunas = explode("-", $comuna);
if($fk_tipo_propiedad != ""){
    $sqlMatch .= " (formulario.fk_tipo_propiedad LIKE '$fk_tipo_propiedad') AND";
}else{
    $contErrores++;
}

if($tipo_operacion != ""){
    $sqlMatch.=" (formulario.operacion LIKE '$tipo_operacion') AND";
}else{
    $contErrores++;
}
// test
$sqlMatch.= "(";
for($i = 0; $i<count($comunas); $i++){
    if($i == 0){
        $sqlMatch .= " Direccion.fk_comuna LIKE $comunas[$i] ";
    }else{
        $sqlMatch .= "OR Direccion.fk_comuna LIKE $comunas[$i] ";
    }

}
$sqlMatch .=") AND ";
// test
if($dormDesde != "" && $dormHasta != ""){
$sqlMatch .=" (formulario.dormitorios BETWEEN '$dormDesde' AND '$dormHasta') AND";
}else{
    $contErrores++;
}
if($bannosDesde != "" && $bannosHasta != ""){
$sqlMatch .= "(formulario.cant_bannos BETWEEN '$bannosDesde' AND '$bannosHasta') AND";
}else{
    $contErrores++;
}

if($m_utilesDesde != "" && $m_utilesHasta != ""){
$sqlMatch .= "(formulario.superficie_util BETWEEN $m_utilesDesde AND $m_utilesHasta) AND";
}else{
    $contErrores++;
}

if($precio_min != "" && $precio_max != ""){
$sqlMatch .= "(formulario.monto1 BETWEEN $precio_min AND $precio_max OR
formulario.monto2 BETWEEN $precio_min AND $precio_max) AND
";
}else{
    $contErrores++;
}

if($m_terrazaDesde != "" && $m_terrazaHasta != ""){
$sqlMatch .= "(formulario.superficie_terraza BETWEEN '$m_terrazaDesde' AND '$m_terrazaHasta') AND";
}else{
    $contErrores++;
}

if($m_terrenoDesde != "" && $m_terrenoHasta != ""){
$sqlMatch .= "(formulario.superficie_terreno BETWEEN '$m_terrenoDesde' AND '$m_terrenoHasta') AND";
}else{
    $contErrores++;
}

if($bodega != ""){
$sqlMatch .= "(formulario.bodega = '$bodega') AND";
}else{
    $contErrores++;
}

if($estacionamientoDesde != "" && $estacionamientoHasta != ""){
$sqlMatch .= "(formulario.estacionamiento BETWEEN '$estacionamientoDesde' AND '$estacionamientoHasta') AND";
}else{
    $contErrores++;
}

if($piscina != ""){
    if($Episcina == "1"){
        $sqlMatch .= "(formulario.piscina = '$piscina') AND";
    }else{
        $sqlMatch .= "(formulario.piscina = '1' OR formulario.piscina = '0') AND";
    }

}else{
    $contErrores++;
}

if($cocina_americana != ""){
    if($EcocinaAme == "1"){
        $sqlMatch .= "(formulario.cocina_ame = '$cocina_americana') AND";
    }else{
        $sqlMatch .= "(formulario.cocina_ame = '1' OR formulario.cocina_ame = '0') AND";
    }
    
}else{
    $contErrores++;
}

if($cocina_isla != ""){
    if($EcocinaIsla == "1"){
        $sqlMatch .= "(formulario.cocina_isla = '$cocina_isla') AND";
    }else{
         $sqlMatch .= "(formulario.cocina_isla = '1' OR formulario.cocina_isla = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($comedor_diario != ""){
    if($EcomedorDiario == "1"){
        $sqlMatch .= "(formulario.comedor_diario = '$comedor_diario') AND";
    }else{
         $sqlMatch .= "(formulario.comedor_diario = '1' OR formulario.comedor_diario = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($termopanel != ""){
    if($Etermopanel == "1"){
        $sqlMatch .= "(formulario.term_panel = '$termopanel') AND";
    }else{
         $sqlMatch .= "(formulario.term_panel = '1' OR formulario.term_panel = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($amoblado != ""){
    if($Eamoblado == "1"){
        $sqlMatch .= "(formulario.amoblado = '$amoblado') AND";
    }else{
         $sqlMatch .= "(formulario.amoblado = '1' OR formulario.amoblado = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($mascotas != ""){
    if($Emascotas == "1"){
        $sqlMatch .= "(formulario.mascotas = '$mascotas') AND";
    }else{
         $sqlMatch .= "(formulario.mascotas = '1' OR formulario.mascotas = 0) AND";
    }
    
}else{
    $contErrores++;
}

if($gimnasio != ""){
    if($gimnasio == "1"){
        $sqlMatch .= "(formulario.gim = '$gimnasio') AND";
    }else{
         $sqlMatch .= "(formulario.gim = '1' OR formulario.gim = 0) AND";
    }
    
}else{
    $contErrores++;
}

// if($dorm_servicio != ""){
//     if($Edorm_servicio == "1"){
//         $sqlMatch .= "(formulario.dorm_serv = '$dorm_servicio') AND";
//     }else{
//          $sqlMatch .= "(formulario.dorm_serv = '1' OR formulario.dorm_serv = 0) AND";
//     }
    
// }else{
//     $contErrores++;
// }

if($quincho != ""){
    if($Equincho == "1"){
        $sqlMatch .= "(formulario.quincho = '$quincho') AND";
    }else{
         $sqlMatch .= "(formulario.quincho = '1' OR formulario.quincho = 0) AND";
    }
    
}else{
    $contErrores++;
}

$sqlMatch .= " formulario.aprobado = 1 AND Perfil_busqueda.fk_interesado = $id";

// echo "<center>$sqlMatch </center>";
// echo "<center>$sqlMatch</center>";
$result = $conn->query($sqlMatch);
$idsForm = array();

if ($result->num_rows > 0 && $contErrores != 20) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        // echo "<center>match con el formulario: ".$row["id_formulario"]."</center>";
        $idForm = $row["id_formulario"];
        array_push($idsForm, $idForm);
       $cont ++;
    }
    array_push($resp, ['interesado' => $id,'forms' => $idsForm, 'URL' =>  'https://indev9.com/redflip/pages/match.php?id='.$id]);
    $sinMatch = 0;
}else{

    if($contErrores == 20){
        // echo "<center>no hay datos suficientes para el match</center>";
        $sinMatch = 1;
    }else{
        // echo "<center>no hay match</center>";
        $sinMatch = 1;
    }
    
}
}
//  if($cont > 0){
//      $res -> array = $idsForm;
//      $res -> success = 2;
//      $res -> resp = $resp;
//     echo json_encode($res);
//  }else{
//     $res -> array = $idsForm;
//     $res -> success = 1;
//     echo json_encode($res);
//  }
     $res -> array = $idsForm;
     $res -> success = 2;
     $res -> resp = $resp;
     $res -> error = $error;
    echo json_encode($res);

?>