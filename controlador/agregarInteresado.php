<?php
include "../conexion.php";
include_once "mcript.php";
// include_once "../pages/valid_session.php";
$nom = $_POST["nom"];
$ape = $_POST["ape"];
$correo = $_POST["correo"];
$tel = $_POST["tel"];
$corredor = $_POST["corredor"];
$rut = $_POST["rut"];
$fec_nac = $_POST["fec_nac"];
$ciudad = $_POST["ciudad"];
// perfil Busqueda
$tipo_prop = $_POST["tipoProp"];
$tipo_operacion = $_POST["tipo_operacion"];

$dormDesde = $_POST["dormDesde"];
$dormHasta = $_POST["dormHasta"];
$Edorm = $_POST["Edorm"];
if($Edorm == "on"){
    $Edorm = 1;
}else{
    $Edorm = 0;
}

$bannosDesde = $_POST["bannosDesde"];
$bannosHasta = $_POST["bannosHasta"];
$Ebanno = $_POST["Ebanno"];
if($Ebanno == "on"){
    $Ebanno = 1;
}else{
    $Ebanno = 0;
}

$mutilesDesde = $_POST["mutilesDesde"];
$mutilesHasta = $_POST["mutilesHasta"];
$EmUtiles = $_POST["EmUtiles"];
if($EmUtiles == "on"){
    $EmUtiles = 1;
}else{
    $EmUtiles = 0;
}

$mTerrazaDesde = $_POST["mTerrazaDesde"];
$mTerrazaHasta = $_POST["mTerrazaHasta"];
$EmTerraza = $_POST["EmTerraza"];
if($EmTerraza == "on"){
    $EmTerraza = 1;
}else{
    $EmTerraza = 0;
}

$mTerrenoDesde = $_POST["mTerrenoDesde"];
$mTerrenoHasta = $_POST["mTerrenoHasta"];
$EmTerreno = $_POST["EmTerreno"];
if($EmTerreno == "on"){
    $EmTerreno = 1;
}else{
    $EmTerreno = 0;
}

$valorMin = $_POST["valorMin"];
$valorMax = $_POST["valorMax"];

$bodega = $_POST["bodega"];
$Ebod = $_POST["Ebod"];
if($Ebod == "on"){
    $Ebod = 1;
}else{
    $Ebod = 0;
}

$estacionamientosDesde = $_POST["estacionamientosDesde"];
$estacionamientosHasta = $_POST["estacionamientosHasta"];
$Eestacionamiento = $_POST["Eestacionamiento"];
if($Eestacionamiento == "on"){
    $Eestacionamiento = 1;
}else{
    $Eestacionamiento = 0;
}

$pisoDepto = $_POST["pisoDepto"];
$pisoDeptoMin = $_POST["pisoDeptoMin"];

$success = array();
$res = $_POST["res"];
$norte = $_POST["norte"];
$sur = $_POST["sur"];
$oriente = $_POST["oriente"];
$poniente = $_POST["poniente"];
$surOriente = $_POST["surOriente"];
$surPoniente = $_POST["surPoniente"];
$norOriente = $_POST["norOriente"];
$norPoniente = $_POST["norPoniente"];

$orientacion = array();

//CARACTERISTICAS

$piscina = $_POST["piscina"];
if($piscina == "on"){
    $piscina = 1;
}else{
    $piscina = 0;
}
$ePiscina = $_POST["Episcina"];
if($ePiscina == "on"){
    $ePiscina = 1;
}else{
    $ePiscina = 0;
}

$cocinaAme = $_POST["cocinaAme"];
if($cocinaAme == "on"){
    $cocinaAme = 1;
}else{
    $cocinaAme = 0;
}
$EcocinaAme = $_POST["EcocinaAme"];
if($EcocinaAme == "on"){
    $EcocinaAme = 1;
}else{
    $EcocinaAme = 0;
}

$cocinaIsla = $_POST["cocinaIsla"];
if($cocinaIsla == "on"){
    $cocinaIsla = 1;
}else{
    $cocinaIsla = 0;
}
$EcocinaIsla = $_POST["EcocinaIsla"];
if($EcocinaIsla == "on"){
    $EcocinaIsla = 1;
}else{
    $EcocinaIsla = 0;
}

$comedorDiario = $_POST["comedorDiario"];
if($comedorDiario == "on"){
    $comedorDiario = 1;
}else{
    $comedorDiario = 0;
}
$EcomedorDiario = $_POST["EcomedorDiario"];
if($EcomedorDiario == "on"){
    $EcomedorDiario = 1;
}else{
    $EcomedorDiario = 0;
}

$termoPanel = $_POST["termoPanel"];
if($termoPanel == "on"){
    $termoPanel = 1;
}else{
    $termoPanel = 0;
}
$EtermoPanel = $_POST["EtermoPanel"];
if($EtermoPanel == "on"){
    $EtermoPanel = 1;
}else{
    $EtermoPanel = 0;
}

$amoblado = $_POST["amoblado"];
if($amoblado == "on"){
    $amoblado = 1;
}else{
    $amoblado = 0;
}
$Eamoblado = $_POST["Eamoblado"];
if($Eamoblado == "on"){
    $Eamoblado = 1;
}else{
    $Eamoblado = 0;
}

$mascotas = $_POST["mascotas"];
if($mascotas == "on"){
    $mascotas = 1;
}else{
    $mascotas = 0;
}
$Emascotas = $_POST["Emascotas"];
if($Emascotas == "on"){
    $Emascotas = 1;
}else{
    $Emascotas = 0;
}

$gim = $_POST["gim"];
if($gim == "on"){
    $gim = 1;
}else{
    $gim = 0;
}
$Egim = $_POST["Egim"];
if($Egim == "on"){
    $Egim = 1;
}else{
    $Egim = 0;
}

$dormServicio = $_POST["dormServicio"];
if($dormServicio == "on"){
    $dormServicio = 1;
}else{
    $dormServicio = 0;
}
$EdormServicio = $_POST["EdormServicio"];
if($EdormServicio == "on"){
    $EdormServicio = 1;
}else{
    $EdormServicio = 0;
}

$quincho = $_POST["quincho"];
if($quincho == "on"){
    $quincho = 1;
}else{
    $quincho = 0;
}
$Equincho = $_POST["Equincho"];
if($Equincho == "on"){
    $Equincho = 1;
}else{
    $Equincho = 0;
}
//orientacion
if($norte == "on"){
    array_push($orientacion, "N");
}
if($sur == "on"){
    array_push($orientacion, "S");
}
if($oriente == "on"){
    array_push($orientacion, "O");
}
if($poniente == "on"){
    array_push($orientacion, "P");
}
if($surOriente == "on"){
    array_push($surOriente, "SO");
}
if($surPoniente == "on"){
    array_push($orientacion, "SP");
}
if($norOriente == "on"){
    array_push($orientacion, "NO");
}
if($norPoniente == "on"){
    array_push($orientacion, "NP");
}
$textOrien = "";
foreach($orientacion as $element){
    $textOrien .= $element . "-";
}
$textOrien = substr($textOrien, 0, (strlen($textOrien)-1));
$sqlPer = "INSERT INTO Persona
            (nombre, apellido,rut, telefono, correo)
             values ('$nom'
             , '$ape'
             , '$rut'
             , '$tel' 
             , '$correo')";

if ($conn->query($sqlPer) === TRUE) {
    $idPer = $conn->insert_id;
    $error = 2;
} else {
    $error = $sqlPer;
}
$nomUs = $encriptar($correo);
$pass = strtoupper($nom[0]) . strtolower($ape[0])."Redflip";
$pass = $encriptar($pass);
$sqlUs = "INSERT INTO usuario (nom_us, pass, fk_rol, fk_estado_us, fk_persona) values ('$nomUs', '$pass', 4, 2, $idPer)";
if ($conn->query($sqlUs) === TRUE) {
    $error = 2;
} else {
    $error = $sqlUs;
}

$sqlInt = "INSERT INTO Interesado (fk_persona, fk_corredor, fk_ciudad) VALUES ($idPer, $corredor, $ciudad)";
if ($conn->query($sqlInt) === TRUE) {
    $idInt = $conn->insert_id;
    $error = 2;
} else {
    $error = $sqlInt;
}

$sqlPerfBusq = "INSERT INTO Perfil_busqueda ( 
fk_interesado,
dormDesde,
dormHasta,
Edorm, 
bannosDesde, 
bannosHasta,
Ebanno, 
m_utilesDesde, 
m_utilesHasta, 
EmUtiles, 
precio_min, 
precio_max, 
comuna, 
orientacion, 
fk_tipo_propiedad, 
m_terrazaDesde, 
m_terrazaHasta, 
Eterraza, 
m_terrenoDesde, 
m_terrenoHasta,
Eterreno,
bodega,
Ebod,
estacionamientoDesde,
estacionamientoHasta,
Eestacionamiento,
piso_depto,
piso_depto_min,
piscina,
Episcina,
cocina_americana,
EcocinaAme,
cocina_isla,
EcocinaIsla,
comedor_diario,
EcomedorDiario,
termopanel,
Etermopanel,
amoblado,
Eamoblado,
mascotas,
Emascotas,
gimnasio,
Egim,
dorm_servicio,
Edorm_servicio,
quincho,
Equincho,
fk_tipo_operacion
 ) VALUES ( 
$idInt, 
'$dormDesde',
'$dormHasta',
'$Edorm',
'$bannosDesde',
'$bannosHasta',
'$Ebanno',
'$mutilesDesde',
'$mutilesHasta',
'$EmUtiles',
'$valorMin',
'$valorMax',
'$res',
'$textOrien',
'$tipo_prop',
'$mTerrazaDesde',
'$mTerrazaHasta',
'$EmTerraza',
'$mTerrenoDesde',
'$mTerrenoHasta',
'$EmTerreno',
'$bodega',
'$Ebod',
'$estacionamientosDesde',
'$estacionamientosHasta',
'$Eestacionamiento',
'$pisoDepto',
'$pisoDeptoMin',
'$piscina',
'$ePiscina',
'$cocinaAme',
'$EcocinaAme',
'$cocinaIsla',
'$EcocinaIsla',
'$comedorDiario',
'$EcomedorDiario',
'$termoPanel',
'$EtermoPanel',
'$amoblado',
'$Eamoblado',
'$mascotas',
'$Emascotas',
'$gim',
'$Egim',
'$dormServicio',
'$EdormServicio',
'$quincho',
'$Equincho',
'$tipo_operacion'
)";
if ($conn->query($sqlPerfBusq) === TRUE) {
    $error = 2;
} else {
    $error = $sqlPerfBusq;
}

$conn->close();
$resp->error = $error;
$resp->rol = $_SESSION["rol"];
$resp->idInt = $idInt;
echo json_encode($resp);

?>