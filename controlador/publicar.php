<?php
include '../conexion.php';
$idForm = $_REQUEST["idForm"];
$titulo = utf8_decode($_REQUEST["titulo"]);
$descripcion = utf8_decode($_REQUEST["descripcion"]);
$dir = $_REQUEST["dir"];
$imagenes = count($_FILES['images']['name']);
$orden = $_REQUEST["orden"];
$respOrden = $_REQUEST["orden"];
$ordenArray = explode(',', $orden);
$latitud = $_REQUEST["lat"];
$longitud = $_REQUEST["lng"];
$entra = "";

$error = "";
$res = new \stdClass();


//validar si se puede publicar
$sql = "SELECT  Sub_estado.id as subEstado, formulario.* from formulario, Propietario, Estado_prop, Sub_estado 
WHERE formulario.id_formulario = $idForm
AND formulario.fk_propietario = Propietario.id_propietario
AND Propietario.fk_estado_prop = Estado_prop.id
AND Estado_prop.fk_sub_estado = Sub_estado.id";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $Sub_estado = $row["subEstado"];
        $divisa = $row["divisa_monto1"];
        $operacion = $row["operacion"];
        $tipo_propiedad = $row["fk_tipo_propiedad"];
        $orientacion = "";
        $sqlOrientacion = "SELECT * from Orientacion WHERE fk_formulario = " . $row["id_formulario"];
        $result4 = $conn->query($sqlOrientacion);
        
        if ($result4->num_rows > 0) { //orientacion
            while($row4 = $result4->fetch_assoc()) {
                if($row4["norte"] == 1){
                    $orientacion = "Norte";
                }
                if($row4["sur"] == 1){
                    $orientacion = "Sur";
                }
                if($row4["oriente"] == 1){
                    $orientacion = "Oriente";
                }
                if($row4["poniente"] == 1){
                    $orientacion = "Poniente";
                }
                if($row4["norOriente"] == 1){
                    $orientacion = "Nor-Oriente";
                }
                if($row4["oriente"] == 1){
                    $orientacion = "Oriente";
                }
                if($row4["oriente"] == 1){
                    $orientacion = "Oriente";
                }
                if($row4["oriente"] == 1){
                    $orientacion = "Oriente";
                }
            }
        }else{
            $res->txtError = "Error en orientacion";
            echo json_encode($res);
            die;
        }
        
        //falta validar si es nueva, por defecto false
        switch ($tipo_propiedad) {
            case '1': //depto
                $tipo_prop = "Apartment";
                break;
            case '2': //casa
                $tipo_prop = "House";
                break;
            case '3': //local
                $tipo_prop = "Office";
                break;
            case '4': //Oficina
                $tipo_prop = "Office";
                break;
            case '5': //terreno
                $tipo_prop = "Office";
                break;
            case '6': //Parcela
                $tipo_prop = "Office";
                break;
            case '7': //Bodega
                $tipo_prop = "Office";
                break;
            case '8': //Galpon
                $tipo_prop = "Office";
                break;
            case '9': //No Definida
                $tipo_prop = "No";
                break;
            case '12': //Casa Oficina
                $tipo_prop = "Office";
                break;
            case '13': //Departamento Oficina
                $tipo_prop = "Office";
                break;
            case '14': //Casa - Terreno
                $tipo_prop = "House";
                break;
            case '15': //Casa - Parcela
                $tipo_prop = "House";
                break;
            
            // default:
            //     $tipo_prop = "No";
            //     break;
        }
        switch ($operacion) {
            case '1':
                $valor_operacion = "For Sale";
                break;
            case '2':
                $valor_operacion = "For Rent";
                break;
            
            case '3':
                $valor_operacion = "amb";
                break;
            default:
                $valor_operacion = "No";
                break;
        }
        switch ($divisa) {
            case '1':
                $valor_divisa = "UF";
                break;
            case '2':
                $valor_divisa = "CLP";
                break;
            case '3':
                $valor_divisa = "USD";
                break;
            
            default:
                $valor_divisa = "No";
                break;
        }


        if($valor_operacion != "No" && $valor_divisa != "No" && $Sub_estado == 5){
            
        }else{
            if($Sub_estado != 5){
                $res->crearXml = "4"; //error subEstado
                $res->txtError = "la fase no es publicado";
                $res->id = $idForm;
                echo json_encode($res);
                die;
            }
            $res->crearXml = "3";
            $cont = 0;
            $txtError = "Error en Formulario: ";
            if($tipo_prop == "No"){
                $txtError .= "Tipo de propiedad no definida";
                $cont++;
            }
            if($valor_divisa == "No"){
                if($cont > 0){
                    $txtError .= ", Operacion ambos o sin dato";
                }else{
                    $txtError .= "Operacion ambos o sin dato";
                }
                $cont++;
            }
            if($valor_divisa == "No"){
                if($cont > 0){
                    $txtError .= ", Divisa no seleccionada";
                }else{
                    $txtError .= "Divisa no seleccionada";
                }
                
            }
            $res->txtError = $txtError;
            $res->idForm = $idForm;
            $res->divisa = $valor_divisa;
            echo json_encode($res);
            die;
        }
    }
}else{
echo json_encode($sql);
die;
}


//fin validacion


    
    
    //guardar datos en bd
    
    //consultar si hay publicaciones anteriores
    $sqlConsultarPub = "SELECT * from Publicacion where fk_formulario = $idForm and despublicada = 0";
    $result = $conn->query($sqlConsultarPub);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $publicaciones = true; //si tiene publicaciones se eliminan los archivos antiguos para se reemplazados por los nuevos
        $sqlGetOrden = "SELECT * from Publicacion where fk_formulario = $idForm";
        $result = $conn->query($sqlGetOrden);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $prevOrden = $row["orden_img"];
            }
        }
        if(!isset($prevOrden)){
            $carpeta = '../publicacion/goPlaceIt/'.$idForm;
            foreach(glob($carpeta . "/*") as $archivos_carpeta){             
            if (is_dir($archivos_carpeta)){
              rmDir_rf($archivos_carpeta);
            } else {
                unlink($archivos_carpeta);
            }
          }
          rmdir($carpeta);
        }
        
      }
    } else {
      $publicaciones = false;
    }
    
    if(!$publicaciones){ //si no hay publicaciones las crea
        $sqlPublicacion = "insert into Publicacion (titulo, descripcion, orden_img, longitud, latitud, txtBusqueda, despublicada, fk_formulario) values ('$titulo', '$descripcion', '$orden', '$longitud' ,'$latitud', '$dir',0, $idForm)";
        if ($conn->query($sqlPublicacion) === TRUE) {
            // echo "New record created successfully";
        } else {
            $error =$sqlPublicacion;
        }
    }else{
        $sqlGetOrden = "SELECT * from Publicacion where fk_formulario = $idForm";
        $result = $conn->query($sqlGetOrden);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $prevOrden = $row["orden_img"];
            }
        }
        //si no hay publicaciones las edita
        if($respOrden != $prevOrden){ //si el orden previo es diferente al orden nuevo
            $orden = $respOrden;
            $entra = "entra";
        }
        $sqlUpdatePublicacion = "UPDATE Publicacion set titulo = '$titulo', descripcion = '$descripcion',
                                 orden_img = '$orden', longitud = '$longitud', latitud = '$latitud', txtBusqueda = '$dir'  where fk_formulario = $idForm and despublicada = 0";
    
        if ($conn->query($sqlUpdatePublicacion) === TRUE) {
            $error = "no hay error";
        } else {
            $error = $sqlUpdatePublicacion;
        }
    
    }
    

    if(isset($orden)){ //solo sucede si orden está setiado
        $ordenArray = explode(',',$orden);
        for($x = 0; $x<count($ordenArray); $x++){
            for ($i=0; $i < $imagenes ; $i++) { 
                if(trim($ordenArray[$x]) == $_FILES['images']['name'][$i]){
                    if(!file_exists("../publicacion/goPlaceIt/".$idForm)){
                        mkdir("../publicacion/goPlaceIt/".$idForm);
                    }
                    $ruta_nueva = "../publicacion/goPlaceIt/".$idForm."/".$_FILES['images']['name'][$i];
                    if(!file_exists($ruta_nueva)){
                        $ruta_temporal = $_FILES['images']['tmp_name'][$i];
                        move_uploaded_file($ruta_temporal, $ruta_nueva);
                        $error = "no hay error";
                    }else{
                        $error = "Error";
                    }
                }
            }
        }
        
    }

    
    //creacion de XML
    include 'crearXML.php';
    include 'crearXMLAmb.php';
    $archivoNoEncontrado = "";
    $archivosEncontrados = "";
    $archivos = scandir("../publicacion/goPlaceIt/".$idForm,1);
    foreach ($archivos as $archivo) {
        if($archivo != "." && $archivo != ".."){
            $coincidencia = strpos($respOrden, $archivo);
            if ($coincidencia === false) {
                    unlink("../publicacion/goPlaceIt/".$idForm."/".$archivo);
                } else {
                    $archivosEncontrados = "todos los archivos estan";
                }
        }
    }
    
    
    
    
    // $res->titulo = $titulo;
    // $res->descripcion = $descripcion;
    // $res->imagenes = $imagenes;
    // $res->orden = $ordenArray;
    // $res->lat = $latitud;
    // $res->lng = $longitud;
    $res->error = $error;
    $res->subEstado = $Sub_estado;
    $res->orden = $respOrden;
    $res->prevOrden =$prevOrden;
    $res->entra = $entra;
    $res->archivoNoEncontrado = $archivoNoEncontrado;
    $res->archivosEncontrados = $archivosEncontrados;
    
    echo json_encode($res);




?>