function datosPropietario(e, idPropietario) {
    e.preventDefault();
    let data = new FormData();
    data.append('idPropietario', idPropietario);
    fetch('../controlador/datosPropietario.php', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(datos => {
            console.log(datos);
            let contenido = `
            <tr>
                <td>Nombre</td>
                <td>${datos.nombre+' '+datos.apellido}</td>
            </tr>
            <tr>
                <td>RUT</td>
                <td>`;
            if (datos.rut != "") {
                contenido += `${datos.rut}`;
            } else {
                contenido += `S/D`;
            }
            contenido += `</td>
            </tr>
            <tr>
                <td>Telefono</td>
                <td>`;
            if (datos.tel != "") {
                contenido += `${datos.tel}`;
            } else {
                contenido += `S/D`;
            }
            contenido += `</td>
            </tr>
            <tr>
                <td>Correo</td>
                <td>`;
            if (datos.correo != "") {
                contenido += `${datos.correo}`;
            } else {
                contenido += `S/D`;
            }
            contenido += `</td>
            </tr>
            `;
            botonWsp = ``;

            if (datos.tel.length != 12) {
                botonWsp += `<a class='btn btn-secondary boton-wsp'><i class='fab fa-whatsapp'></i> Número incorrecto</a>`;

            } else {
                botonWsp += `<a href='https://api.whatsapp.com/send?phone=${datos.tel}&text=Hola%20quiero%20info' class='btn btn-success boton-wsp' target='_blank'><i class='fab fa-whatsapp'></i> Contactar</a>`;
            }
            $('.tablaModal').html(contenido)
            $('.botonWsp').html(botonWsp)
            $('#modal1').fadeIn();
        })
        .catch(error => alert.log(error));

}

function closeModal() {
    $('#modal1').fadeOut();
}