// propietario
function EliminarPropietario(id) {

    Swal.fire({
        title: '¿Estas seguro?',
        text: "Esto no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminalo!'
    }).then((result) => {
        if (result.value) {
            fetch('../controlador/EliminarPropietario.php?id=' + id, {
                    method: 'POST',
                })
                .then(buscar_datos());

            Swal.fire(
                'Eliminado!',
                'Propietario eliminado',
                'success'
            )
        }
    })
}

function EditarPropietario(id) {
    location.href = "../pages/editarProp.php?id=" + id;
}

function desPropietario(id) {

    var valor5 = document.getElementById("search").value;
    fetch('../controlador/desPropietario.php?id=' + id, {
            method: 'POST',
        })
        .then(res => res.json())
        .then(datos => {
            Swal.fire({
                title: 'Correcto',
                icon: 'success',

            });
            buscar_datos();

        })
}

function EditarPropietario(id) {
    location.href = "../pages/editarProp.php?id=" + id;
}

function habilitarPropietario(id) {

    Swal.fire({
        title: 'Correcto',
        icon: 'success',

    })
    fetch('../controlador/habPropietario.php?id=' + id, {
            method: 'POST',
        })
        .then(buscar_datos());
}

function EditarPropietario(id) {
    location.href = "../pages/editarProp.php?id=" + id;
}

// Propiedad


function EditarPropiedadR(id) {
    location.href = "../pages/editarPropiedad.php?id=" + id;
}


function verNotas(id) {
    location.href = `../pages/notas.php?id=${id}`;
}

function desPropiedad(id) {

    var valor1 = document.getElementById("tipo_operacion").value;
    var valor2 = document.getElementById("estado").value;
    var valor3 = document.getElementById("fase").value;
    var valor4 = document.getElementById("importancia").value;
    var valor5 = document.getElementById("search").value;

    (async() => {

        const { value: fruit } = await Swal.fire({
            title: 'Selecciona un motivo',
            icon: 'question',
            input: 'select',
            inputOptions: {
                V_O: 'Vendida con otro',
                A_O: 'Arrendada con otro',
                P_F: 'Proceso de Firma',
                A: 'Arrepentimiento',
                otro: 'Otro'
            },
            inputPlaceholder: 'Selecciona un motivo',
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value === 'otro') {
                        //         empieza el input
                        const { value: motivo } = Swal.fire({
                                title: 'Ingrese el motivo para deshabilitar esta propiedad',
                                input: 'text',
                                showCancelButton: true
                            })
                            .then(

                                // se envia el input
                                Swal.fire({
                                    title: 'Ingrese el motivo para deshabilitar esta propiedad',
                                    input: 'text',
                                    icon: 'question',
                                    inputAttributes: {
                                        autocapitalize: 'off'
                                    },
                                    cancelButtonText: 'Cancelar',
                                    showCancelButton: true,
                                    reverseButtons: true,
                                    confirmButtonText: 'Guardar',
                                    showLoaderOnConfirm: true,
                                    preConfirm: (motivo) => {
                                        return fetch(`../controlador/guardarEvento.php?id=${id}&motivo=${motivo}`)
                                            .then(response => {
                                                if (response.ok == "false") {
                                                    console.log(response);
                                                    throw new Error(response.statusText);
                                                }
                                                console.log(response.ok);
                                                return response.json();




                                            })
                                            .catch(error => {
                                                Swal.showValidationMessage(
                                                    `Request failed: ${error}`
                                                );
                                            });
                                    },
                                    allowOutsideClick: () => !Swal.isLoading()
                                }).then((result) => {
                                    if (result.value) {
                                        fetch('../controlador/desPropiedad.php?id=' + id, {
                                                method: 'POST',
                                            })
                                            .then(function() {

                                                console.log(valor1);
                                                console.log(valor2);
                                                console.log(valor3);
                                                console.log(valor4);
                                                console.log(valor5);

                                                if (valor1 == "" && valor2 == "" && valor3 == "" && valor4 == "") {}
                                                buscar_datos(valor5);

                                                Swal.fire({
                                                    title: 'Correcto',
                                                    icon: 'success',

                                                });
                                            });
                                    }
                                })





                            );


                        //         termina el input
                    } else {
                        // aqui va a guardar lo del select


                        // formateo el motivo
                        var motiv = "";
                        if (value == "V_O") {
                            motiv = "Vendida con otro";
                        } else if (value == "A_O") {
                            motiv = "Arrendada con otro";
                        } else if (value == "P_F") {
                            motiv = "Proceso de firma";
                        } else if (value == "A") {
                            motiv = "Arrepentimiento";
                        }
                        //fin
                        fetch(`../controlador/guardarEvento.php?id=${id}&motivo=${motiv}`)
                            .then(response => {
                                console.log("ahora aqui");
                                if (response.ok == "false") {
                                    console.log(response);
                                    throw new Error(response.statusText);

                                }
                                console.log(response.ok);
                                return response.json();
                            })

                        .then(

                            fetch('../controlador/desPropiedad.php?id=' + id, {
                                method: 'POST',
                            })
                            .then(function() {

                                console.log(valor1);
                                console.log(valor2);
                                console.log(valor3);
                                console.log(valor4);
                                console.log(valor5);

                                if (valor1 == "" && valor2 == "" && valor3 == "" && valor4 == "") {}
                                buscar_datos(valor5);

                                Swal.fire({
                                    title: 'Correcto',
                                    icon: 'success',

                                });
                            })

                        );

                    }
                })
            }
        })

    })()





}













function habilitarPropiedad(id) {

    Swal.fire({
        title: 'Correcto',
        icon: 'success',

    })


    fetch('../controlador/habPropiedad.php?id=' + id, {
            method: 'POST',
        })
        .then(buscar_datos());
}

function EliminarPropiedad(id) {

    Swal.fire({
        title: '¿Estas seguro?',
        text: "Esto no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Elimínalo!'
    }).then((result) => {
        if (result.value) {
            var valor1 = document.getElementById("tipo_operacion").value;
            var valor2 = document.getElementById("estado").value;
            var valor3 = document.getElementById("fase").value;
            var valor5 = document.getElementById("search").value;
            fetch('../controlador/EliminarPropiedad.php?id=' + id, {
                    method: 'GET'
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if (data.error === "null") {
                        Swal.fire(
                            'Eliminado!',
                            'Propiedad Eliminada',
                            'success'
                        )
                    } else {
                        Swal.fire(
                            'Error',
                            `${data.error}`,
                            'error'
                        )
                    }
                    buscar_datos(valor5)

                });
        }
    })
}

// cambia la fase desde el listado principal
function cambiar(id, idprop) {
    var ide = document.getElementById(id).value;


    fetch('../controlador/cambiarFase.php?consulta1=' + ide + '&consulta2=' + idprop, {

        })
        .then(
            fetch('../jobs/resetContPropietario.php?idProp=' + idprop, {

            }).then(res => res.json())

        )
        .then(res => res.json())
        .then(data => {
            if (ide == 5) {

                Swal.fire({
                    title: '¿Desea deshabilitar la propiedad?',
                    text: "Podrás revertirlo desde el listado de deshabilitadas",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, deshabilitar!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.value) {
                        desPropiedad(id);

                    }
                });



            } else {

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "100",
                    "hideDuration": "100",
                    "timeOut": "1000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success('Hecho');
                var valor = document.getElementById("search").value;


                var valor2 = document.getElementById("estado").value;

                var valor3 = document.getElementById("fase").value;

                var valor4 = document.getElementById("importancia").value;

                let search = document.getElementById("search").value;

                if (search.length > 0) {
                    buscar_datos(search);
                } else {
                    buscar_datos();
                }




            }

        });
}

// cambia la fase desde el listado de propietarios
function cambiar2(idPer, idprop) {
    // valores de los filtros
    var valor2 = document.getElementById("estado").value;
    var valor3 = document.getElementById("fase").value;
    var valor4 = document.getElementById("importancia").value;
    var valor5 = document.getElementById("search").value;
    //nombre del select
    var ide = document.getElementById(idPer).value;
    var ide2 = document.getElementById(idPer);




    fetch('../controlador/cambiarFase.php?consulta1=' + ide + '&consulta2=' + idprop, {

        })
        .then(res => res.json())
        .then(
            fetch('../jobs/resetContPropietario.php?idProp=' + idprop, {

            }).then(res => res.json())

        )
        .then(data => {

            if (ide == 5) {

                Swal.fire({
                    title: '¿Desea deshabilitar la propiedad?',
                    text: "Podrás revertirlo desde el listado de deshabilitadas",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, deshabilitar!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.value) {
                        desPropiedad(id);
                        buscar_datos(valor2, valor3, valor4, valor5, 5);

                    } else {
                        buscar_datos();
                    }
                });




            } else {

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "100",
                    "hideDuration": "100",
                    "timeOut": "1000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.success('Correcto');
                var valor = document.getElementById("search").value;


                var valor2 = document.getElementById("estado").value;

                var valor3 = document.getElementById("fase").value;

                var valor4 = document.getElementById("importancia").value;

                var valor5 = document.getElementById("origen").value;

                buscar_datos_all(valor, valor2, valor3, valor4, valor5, 5);


            }

        });
}




function insertarContactos(idProp) {
    var nom = "input_" + idProp;
    var cant = document.getElementById(nom).value;
    fetch('../controlador/insertarContactos.php?idProp=' + idProp + '&cantidad=' + cant, {

        }).then(res => res.json())
        .then(data => {
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "100",
                "timeOut": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.success('Correcto');
            // buscar_datos();
        });
}

function whatsapp(phone) {
    window.open(`https://api.whatsapp.com/send?phone=${phone}&text=Hola%20quiero%20info`, '_blank');
}

function cambiarEstatus(idForm) {
    Swal.fire({
            icon: 'question',
            title: 'Seleccione una Opción',
            input: 'select',
            inputOptions: {
                disponible: 'Disponible',
                v_redflip: 'Vendida por Redflip',
                v_terceros: 'Vendida por terceros',
                a_redflip: 'Arrendada por Redflip',
                a_terceros: 'Arrendada por Terceros',
                p_tv: 'Pendiente de Tour Virtual'
            },
            inputPlaceholder: 'Seleccione una opción'
        })
        .then(value => {
            if (value.value != "") {
                data = new FormData();
                data.append('valor', value.value);
                data.append('idForm', idForm);
                fetch('../controlador/cambiarEstatus.php', {
                        method: 'POST',
                        body: data
                    })
                    .then(res => res.json())
                    .then(datos => {
                        if (datos === 2) {
                            let search = document.getElementById("search").value;
                            buscar_datos(search);
                        }
                    });
            } else {
                console.log("no seleccionaste nada");
            }


        })
}