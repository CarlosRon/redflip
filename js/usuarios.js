let form = document.getElementById("ingresoUsuario");

form.addEventListener('submit', function(e) {
    e.preventDefault();
    let data = new FormData(form);

    fetch('usuarioSubir.php', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(datos => {
            console.log(datos);
            if (datos.respuesta === "2") {
                console.log("la respuesta es positiva");
                Swal.fire({
                        icon: 'success',
                        title: 'Excelente',
                        text: `${datos.error}`
                    })
                    .then((result) => {
                        if (result.value) {
                            window.location = "usuariosVer.php";
                        }
                    });
            } else if (datos.respuesta === "1") {
                console.log("la respuesta es negativa");
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: `${datos.error}`
                })
            } else {
                console.log("la respuesta es negativa");
                Swal.fire({
                    icon: 'warning',
                    title: 'Oops...',
                    text: `${datos.error}`
                })
            }
        })
        // .catch(error => console.log('Error: ' + error));
});

function checkUsuario() {
    let campo = document.getElementById("password");
    let temp = document.getElementById("temp_pass");
    if (campo.disabled == false) {
        campo.disabled = true;
        temp.value = campo.value;
        campo.value = "";
    } else {
        campo.disabled = false;
        campo.value = temp.value;
    }
    console.log("check usuario")
}