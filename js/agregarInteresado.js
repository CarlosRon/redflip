/*jshint esversion: 6 */
var form = document.getElementById("formC");
form.addEventListener("submit", function(e) {
    e.preventDefault();
    let container = document.getElementById("container");
    let res = "";
    // obtener los datos de las comunas
    cont = $('#container').children().length;
    for (let i = 0; i < cont; i++) {
        res += $('#container').children()[i].id + "-";
    }
    res = res.substr(0, (res.length - 1));
    var datos = new FormData(form);
    console.log(datos.get["corredor"]);
    datos.append("res", res);
    // datos de las comunas guardados en variable "res"
    fetch("../controlador/agregarInteresado.php", {
            method: 'POST',
            body: datos
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.error)
                // ----------------- aqui van las comunas


            // fetch(`../controlador/agregrarComunasPerfil.php?id=${data.idInt}&res=${res}`,{
            //     method: "POST",
            //     body: res
            // })
            // .then(res2 => res2.json())
            // .then(data => {
            //     console.log("La respuesta de la comuna es: " + data)
            // })
            // --------------------------------------

            Swal.fire({
                title: 'Listo',
                text: "Interesado agregado",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok'
            }).then((result) => {
                if (result.value) {

                    console.log(data);
                    // if (data.rol == "1") {
                    //     windows.location.replace('http://redflip.net/pages/InteresadosGlobales.php');
                    //     // console.log("aqui");
                    // } else {
                    //     windows.location.replace("http://redflip.net/pages/interesadosCorredor.php");
                    //     // console.log("o aqui");
                    // }

                }
            });
        });
});

var selectComuna = document.getElementById("selectComuna");

selectComuna.addEventListener("change", function() {
    let comunas = document.getElementById("comunas");
    let comuna = selectComuna.value;
    let container = document.getElementById("container");
    let div = document.createElement("div");
    let input = document.createElement("input");
    div.id = comuna;
    input.type = "text";
    input.disabled = true;
    let cont = "";
    let aux = 0;
    fetch("../controlador/selectComuna.php?id=" + comuna)
        .then(res => res.json())
        .then(data => {
            cont = $('#container').children().length;
            for (let i = 0; i < cont; i++) {
                let lastId = $('#container').children()[i].id;
                if (lastId == comuna) {
                    aux++;
                }
            }
            if (aux == 0) {
                input.id = comuna;
                div.onclick = function() {
                    clickDiv(comuna);
                };
                input.value = data;
                // comunas.innerHTML+=","+comuna;

                let arr = comunas.value.split(",");
                arr.filter(Boolean);
                // comunas.innerHTML = arr;
                div.appendChild(input);
                container.appendChild(div);

                console.log(cont);


            } else {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'warning',
                    title: 'Comuna Repetida'
                })
            }

        })



})

function clickDiv(id) {
    let div = document.getElementById(id);
    div.remove();
}

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    // next_fs = document.getElementById("ste");
    // console.log($(this).parent());
    //activate next step on progressbar using the index of next_fs
    $("#progressBar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    current_fs.removeClass("active");
    next_fs.addClass("active");
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.hide(),
                animating = false,
                current_fs.css({
                    'transform': 'scale()',
                    'position': 'absolute',

                }); // jshint ignore:line
            next_fs.css({ 'left': left, 'opacity': opacity });
        },
        duration: 800,
        complete: function() {

        },
        //this comes from the custom easing plugin
        // easing: 'easeInOutBack'
    });
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

});

$(".previous").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();
    // if(document.getElementById("casa").checked && !document.getElementById("condominio").checked){
    // 	if(previous_fs[0].id == "10.3"){
    // 		previous_fs = previous_fs.prev();
    // 	}
    // }

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    previous_fs.addClass("active");
    current_fs.removeClass("active");
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            // scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            // left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({ 'left': left });
            previous_fs.css({ 'transform': 'scale()', 'opacity': opacity });
            current_fs.hide();
            animating = false;
        },
        duration: 800,
        complete: function() {
            // current_fs.hide();
            // animating = false;
        },
        //this comes from the custom easing plugin
        // easing: 'easeInOutBack'
    });
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});

$(".submit").click(function() {
    return false;
});