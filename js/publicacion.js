var orden = [];

function filePreview(input) {
    var li = document.getElementById("sortable").innerHtml = "";

    console.log(input.files.length);
    if (input.files.length != 0) {
        for (let i = 0; i < input.files.length; i++) {
            if (input.files && input.files[i]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#sortable + img').remove();
                    var li = document.createElement("li");
                    li.id = input.files[i].name;
                    li.setAttribute("onchange", `cambio()`);
                    document.getElementById("sortable").appendChild(li);
                    var iElement = document.createElement("i");
                    iElement.className = "fas fa-times fa-xs quitar-img";
                    iElement.setAttribute("onclick", 'quitarImg("' + input.files[i].name + '")');
                    li.appendChild(iElement);
                    var img = document.createElement("img");
                    img.src = e.target.result;
                    img.width = 100;
                    li.appendChild(img);
                }
                reader.readAsDataURL(input.files[i]);
                console.log(input.files[i].name);
                // orden[i] = input.file[i].name;
                orden.push(input.files[i].name);
            }
        }
    } else {
        document.getElementById("sortable").innerHTML = "";
    }

}

$("#file").change(function() {
    document.getElementById("sortable").innerHTML = "";
    filePreview(this);
    console.log(orden);
});

var form = document.getElementById('publicacion');

form.addEventListener('submit', function(e) {
    e.preventDefault();
    publicar();

});

function publicar() {
    Swal.fire({
        title: 'Subiendo Archivos',
        icon: 'warning',
        html: '<i class="fas fa-spinner fa-spin fa-2x"></i>',
        showConfirmButton: false,
        allowOutsideClick: false

    })
    var btn = document.getElementById("btnPublicar");
    var orden = getOrdenImg();
    console.log(orden);
    var data = new FormData(form);
    data.append("orden", orden);
    fetch('../controlador/publicar.php', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(datos => {
            console.log("dsadsad");
            console.log(datos);
            if (datos.crearXml == 3) {
                Swal.close();
                Swal.fire({
                    icon: 'error',
                    title: 'Error en formulario',
                    text: datos.txtError
                });
            } else if (datos.crearXml == 4) {
                Swal.close();
                Swal.fire({
                    title: 'La propiedad no está en fase "publicado"',
                    text: "¿Quieres cambiarlo ahora?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No',
                    allowOutsideClick: false
                }).then((result) => {
                    // console.log(result);
                    if (result.value) {
                        cambiarPublicado(datos.id);
                        publicar();
                    } else if (result.dismiss === "cancel") {
                        Swal.fire(
                            'Propiedad no publicada',
                            'La propiedad no fue publicada',
                            'info'
                        );
                    }
                })
            } else {
                console.log(data);
                Swal.close();
                Swal.fire(
                    'Hecho',
                    'Propiedad publicada',
                    'success'
                );
            }

        })
        .catch((error) => {
            console.warn(error);
        })
}

function cambiarPublicado(id) {

    fetch('../controlador/cambiarPublicado.php?id=' + id, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if (data == 2) {
                return true;
            } else {
                return false;
            }
        });
}

function cambio() {
    console.log("cambio");
}

$("#sortable").sortable({
    items: "> li",

});
var items = $(".selector").sortable("option", "items");
console.log(items);

function getOrdenImg() {
    var itemsId = $("#sortable")[0].children;
    orden = [];
    for (let i = 0; i < itemsId.length; i++) {

        if (itemsId[i].id != "") {
            orden.push(itemsId[i].id);
        }
    }
    return orden;
}

//eliminar una imagen
function quitarImg(id) {
    let input = document.getElementById("file");
    let li = document.getElementById(id);
    let ul = li.parentNode;
    ul.removeChild(li);
    if (ul.children.length === 0) {
        input.value = "";
    }
    for (let i = 0; i < orden.length; i++) {
        if (orden[i] == id) {
            orden.splice(i, 1);
        }
    }
    console.log(orden);

}




var map, infoWindow;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 6
    });
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Estas aquí.');
            infoWindow.open(map);
            map.setCenter(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

// function initMap() {

//     var map = new google.maps.Map(document.getElementById('map'), {
//         zoom: 3,
//         center: { lat: -28.024, lng: 140.887 }
//     });

//     // Create an array of alphabetical characters used to label the markers.
//     var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

//     // Add some markers to the map.
//     // Note: The code uses the JavaScript Array.prototype.map() method to
//     // create an array of markers based on a given "locations" array.
//     // The map() method here has nothing to do with the Google Maps API.
//     var markers = locations.map(function(location, i) {
//         return new google.maps.Marker({
//             position: location,
//             label: labels[i % labels.length]
//         });
//     });

//     // Add a marker clusterer to manage the markers.
//     var markerCluster = new MarkerClusterer(map, markers, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
// }
// var locations = [
//     { lat: -33.3211035, lng: -70.7536919 }
// ]

var marker;

// function initMap(lat = -33.3211035, lng = -70.7536919) {
//     var map = new google.maps.Map(document.getElementById('map'), {
//         zoom: 13,
//         center: { lat: -33.3211035, lng: -70.7536919 }
//     });

//     marker = new google.maps.Marker({
//         map: map,
//         draggable: true,
//         // animation: google.maps.Animation.DROP,
//         position: { lat: lat, lng: lng }
//     });
//     marker.addListener('click', toggleBounce);
// }

// function toggleBounce() {
//     if (marker.getAnimation() !== null) {
//         marker.setAnimation(null);
//     } else {
//         marker.setAnimation(google.maps.Animation.BOUNCE);
//     }
// }
// lat = document.getElementById("lat");
// lng = document.getElementById("lng");
// lat.addEventListener("change", function(e) {
//     console.log("dasdsa");
//     initMap(lat.value, lng.value);

// })

function initMap2(lat, lng) {
    document.getElementById("lat").value = lat;
    document.getElementById("lng").value = lng;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng(lat, lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // creates a draggable marker to the given coords
    var vMarker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        draggable: true
    });

    // adds a listener to the marker
    // gets the coords when drag event ends
    // then updates the input with the new coords
    google.maps.event.addListener(vMarker, 'dragend', function(evt) {
        document.getElementById("lat").value = evt.latLng.lat().toFixed(6);
        document.getElementById("lng").value = evt.latLng.lng().toFixed(6);
        $("#lat").val(evt.latLng.lat().toFixed(6));
        $("#lng").val(evt.latLng.lng().toFixed(6));

        map.panTo(evt.latLng);
    });

    // centers the map on markers coords
    map.setCenter(vMarker.position);

    // adds the marker on the map
    vMarker.setMap(map);
}


function buscar() {
    var dir = document.getElementById("dir").value;
    fetch("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCNzSw5dqoHcZvquOW_wIti2qSkYQEyYBc&address=" + dir, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            initMap2(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng);

        })
}

function despublicar(id) {
    fetch('../controlador/despublicar.php?id=' + id, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if (data == 2) {
                Swal.fire(
                    'Propiedad despublicada',
                    'La propiedad fue despublicada',
                    'info'
                );
            } else {
                Swal.fire(
                    'Error',
                    data,
                    'error'
                );
            }
        })
}