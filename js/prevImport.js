let formArrays = document.getElementById("formArrays");
    document.addEventListener("submit", function(e){
        e.preventDefault();
        var datos = new FormData(formArrays);

        fetch('../controlador/cargaMasiva.php',{

            method: 'POST',
    
            body: datos
    
        })
        .then(res=>res.json())
        .then(data =>{
            console.log(data);
            if(data.error == null){
                Swal.fire(
                    'Bien hecho!',
                    'Datos Guardados!',
                    'success'
                  ).then((result) =>{
                    if(result.value){
                        window.close();
                    }
                  })
            }
        })
    })
    