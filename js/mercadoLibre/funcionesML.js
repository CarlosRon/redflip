/*jshint esversion: 6 */
token();
var access_token = "";
var refresh_token = "";
let ids = [];

function token() {
    let code = document.getElementById("code");
    let body = new FormData();
    body.append("grant_type", "authorization_code");
    body.append("client_id", "2868867605753550");
    body.append("client_secret", "AGxwnEuWoY2OHmlaE28rKWgjLLGWFNNK");
    body.append("code", code);
    body.append("redirect_uri", "https://indev9.com/redflip/pages/mercadoLibre.php");


    // comprobar si hay token activo
    fetch('../controlador/comprobarTokenMercadoLibre.php', {
            method: 'GET',
        })
        .then(res => res.json())
        .then(comprobacion => {
            if (comprobacion.message === "yes") {
                console.log("hay token activo");
                // console.log(comprobacion);
                // console.log(comprobacion.access_token);
                // console.log(comprobacion.refresh_token);
                cargarPaquetes(comprobacion.access_token);
                cargarPropiedades(comprobacion.access_token);
                document.getElementById("access_token").value = comprobacion.access_token;
            } else {
                window.location = "http://auth.mercadolibre.com.ar/authorization?response_type=code&amp;client_id=2868867605753550&amp;redirect_uri=https://indev9.com/redflip/pages/mercadoLibre.php";
                // console.log(comprobacion);
            }
        });
    //fin comprobar si hay token activo
}

function cargarPaquetes(access_token) {
    fetch('https://api.mercadolibre.com/users/422248515/classifieds_promotion_packs?package_content=ALL&access_token=' + access_token, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            var tabla = document.getElementById("paquetes");
            tabla.innerHTML = `<tr>
            <th>descripcion</th>
            <th>tipo</th>
            <th>disponibles</th>
            <th>usados</th>

            </tr>`;
            data.forEach((paquete) => {
                tabla.innerHTML += `<tr>
            <td>${paquete.description}</td>
            <td>${paquete.listing_details[0].listing_type_id}</td>
            <td>${paquete.listing_details[0].available_listings}</td>
            <td>${paquete.listing_details[0].used_listings}</td>

            </tr>`;
            });
            // console.log(data);
            // console.log(data[0].listing_details[0].listing_type_id);
        });

}
// script para las propiedades

function cargarPropiedades(access_token, scroll_id) {
    let url = "";
    if (scroll_id == undefined) {
        url = 'https://api.mercadolibre.com/users/422248515/items/search?search_type=scan&status=active&orders=start_time_desc&limit=100&access_token=' + access_token;
    } else {
        url = 'https://api.mercadolibre.com/users/422248515/items/search?search_type=scan&status=active&orders=start_time_desc&limit=100&access_token=' + access_token + '&scroll_id=' + scroll_id;
    }
    fetch(url, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            console.log("url: " + url);
            let scroll_Data = data.scroll_id;
            // console.log(data);
            if (data.message === "Invalid token") {
                console.log("token invalido");
            }
            let cards = document.getElementById("cards");
            data.results.forEach(propiedadId => {
                // console.log(cargarDatosPropiedad(propiedadID))
                fetch('https://api.mercadolibre.com/items/' + propiedadId, {
                        method: 'GET'
                    })
                    .then(res => res.json())
                    .then(data2 => {

                        let tipo = "";
                        let operacion = "";
                        let precio = "";
                        let start_time = data2.start_time;
                        start_time = start_time.split('T');
                        start_time = start_time[0];
                        let stop_time = data2.stop_time;
                        stop_time = stop_time.split('T');
                        stop_time = stop_time[0];
                        let paquete = data2.listing_type_id;
                        if (data2.domain_id === "MLC-APARTMENTS_FOR_RENT") {
                            tipo = "Depto";
                            operacion = "Arriendo";
                        } else if (data2.domain_id === "MLC-INDIVIDUAL_APARTMENTS_FOR_SALE") {
                            tipo = "Depto";
                            operacion = "Venta";
                        } else if (data2.domain_id === "MLC-OFFICES_FOR_RENT") {
                            tipo = "Oficina";
                            operacion = "Arriendo";
                        } else if (data2.domain_id === "MLC-INDIVIDUAL_OFFICES_FOR_SALE") {
                            tipo = "Oficina";
                            operacion = "Venta";
                        } else if (data2.domain_id === "MLC-INDIVIDUAL_HOUSES_FOR_SALE") {
                            tipo = "Casa";
                            operacion = "Venta";
                        } else if (data2.domain_id === "MLC-RETAIL_SPACE_FOR_RENT") {
                            tipo = "Local";
                            operacion = "Arriendo";
                        } else if (data2.domain_id === "MLC-RETAIL_SPACE_FOR_SALE") {
                            tipo = "Local";
                            operacion = "Venta";
                        }
                        if (data2.currency_id == "CLP") {
                            precio = "$ " + data2.price;

                        } else {
                            precio = "UF " + data2.price;
                        }
                        if (data2.status !== "closed") {
                            let idSinMLC = data2.id.replace("MLC", "#");
                            let contenido = "";
                            ids.push(idSinMLC);
                            //         contenido += `<div class="card w-75" style="
                            //     height: 200px;
                            //     border: solid 1px;
                            //     width: 100% !important;
                            //     ">
                            // <div class="card-body">
                            //         <div class="row pt-2">
                            //             <div class="col-3 "><a href="estadisticaPropiedadML.php?id=${data2.id}">${data2.title}</a></div>
                            //             <div class="col-3"><label>${idSinMLC}</label></div>
                            //             <div class="col-3"><label>${tipo}</label></div>
                            //             <div class="col-3"><label>${operacion}</label></div>
                            //         </div>
                            //         <div class="row pt-4">
                            //             <div class="col-3">${precio}</div>`;
                            let urlImg = data2.pictures[0].url;
                            let urlImgArray = urlImg.split(':');
                            urlImg = urlImgArray[0] + 's:' + urlImgArray[1];
                            contenido += `<div class="row mt-5">
                                <div class="col-4 m-0 p-0">
                                    <img src="${urlImg}" alt="" width="100%">
                                </div>
                                <div class="col-8">
                                    <div class="container-fluid m-0 p-0">
                                        <div class="row" style="">
                                            <div class="col-12">
                                                <!-- titulo -->
                                                <h2>${data2.title}</h2>

                                            </div>
                                            <div class="col-12">
                                                <!-- id -->
                                                ${idSinMLC}
                                            </div>
                                            <div class="col-6">
                                                <!-- tipo prop -->
                                                ${tipo}
                                            </div>
                                            <div class="col-6">
                                                <!-- tipo operacion -->
                                                ${operacion}
                                            </div>
                                        `;


                            let f = new Date();
                            let mes = "";
                            if (f.getMonth() <= 9) {
                                mes = "0" + f.getMonth();
                            } else {
                                mes = f.getMonth();
                            }
                            let hoy = f.getFullYear() + '-' + mes + '-' + f.getDate();
                            let ayer = f.getFullYear() + '-' + mes + '-' + (f.getDate() - 1);
                            fetch('https://api.mercadolibre.com/items/visits?ids=' + propiedadId + '&date_from=' + ayer + '&date_to=' + hoy, {
                                    method: 'GET'
                                })
                                .then(res => res.json())
                                .then(data3 => {
                                    let visitas = data3[0].total_visits;

                                    let preguntas = "";
                                    let urlPreguntas = "https://api.mercadolibre.com/questions/search?item=" + propiedadId + "&access_token=" + access_token;
                                    fetch(urlPreguntas, {
                                            method: 'GET'
                                        })
                                        .then(res => res.json())
                                        .then(data4 => {
                                            preguntas = data4.total;
                                            //     contenido += `

                                            // <div class="col-3">Visitas de hoy ${visitas}</div>
                                            //     <div class="col-3">Contactos: ${preguntas}</div>
                                            //         <div class="col-3">${start_time}</div>
                                            //         </div>
                                            //     </div>
                                            // </div>
                                            // `;
                                            contenido += `
                                            
                                        <div class="col-6">
                                        Visitas de hoy ${visitas}
                                    </div>
                                    <div class="col-6">
                                        Contactos: ${preguntas}
                                    </div>
                                    <div class="col-6">
                                        Fecha publicación: ${start_time}
                                    </div>
                                    <div class="col-6">
                                        Fecha de termino: ${stop_time}
                                    </div>
                                    <div class="col-6">
                                        Paquete: ${paquete}
                                    </div>
                                    <div class="col-6">
                                        <!-- "Precio" -->
                                        <span>${precio}</span>
                                    </div>
                                    <div class="col-12 pt-5 float-right">
                                        <a href="estadisticaPropiedadML.php?id=${data2.id}"class="btn btn-matchRed">Ver Propiedad</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                                        `
                                            cards.innerHTML += contenido;
                                        });
                                });
                        }
                    });
            });
            // aqui va la llamada con scroll
            // console.log(scroll_id);
            if (scroll_id == undefined) {
                console.log("entra en scroll");
                cargarPropiedades(access_token, scroll_Data);
                console.log(ids);
            }

        });

}

function cargarDatosPropiedad(propiedadId) {
    var datos = "";
    fetch('https://api.mercadolibre.com/items/' + propiedadId, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            // var cards = document.getElmentById("cards");


        });
}

function buscar() {
    let busqueda = document.getElementById("search").value;
    let access_token = document.getElementById("access_token").value;
    if (busqueda === "") {
        location.reload();
    } else {


        ids.forEach(id => {
            if (id.indexOf(busqueda) != -1) {
                let idProp = id.split("#");
                let resultId = 'MLC' + idProp[1];
                let cards = document.getElementById("cards");
                cards.innerHTML = "";
                fetch('https://api.mercadolibre.com/items/' + resultId, {
                        method: 'GET'
                    })
                    .then(res => res.json())
                    .then(data2 => {

                        let tipo = "";
                        let operacion = "";
                        let precio = "";
                        let start_time = data2.start_time;
                        start_time = start_time.split('T');
                        start_time = start_time[0];
                        if (data2.domain_id === "MLC-APARTMENTS_FOR_RENT") {
                            tipo = "Depto";
                            operacion = "Arriendo";
                        } else if (data2.domain_id === "MLC-INDIVIDUAL_APARTMENTS_FOR_SALE") {
                            tipo = "Depto";
                            operacion = "Venta";
                        } else if (data2.domain_id === "MLC-OFFICES_FOR_RENT") {
                            tipo = "Oficina";
                            operacion = "Arriendo";
                        } else if (data2.domain_id === "MLC-INDIVIDUAL_OFFICES_FOR_SALE") {
                            tipo = "Oficina";
                            operacion = "Venta";
                        } else if (data2.domain_id === "MLC-INDIVIDUAL_HOUSES_FOR_SALE") {
                            tipo = "Casa";
                            operacion = "Venta";
                        } else if (data2.domain_id === "MLC-RETAIL_SPACE_FOR_RENT") {
                            tipo = "Local";
                            operacion = "Arriendo";
                        } else if (data2.domain_id === "MLC-RETAIL_SPACE_FOR_SALE") {
                            tipo = "Local";
                            operacion = "Venta";
                        }
                        if (data2.currency_id == "CLP") {
                            precio = "$ " + data2.price;

                        } else {
                            precio = "UF " + data2.price;
                        }
                        if (data2.status !== "closed") {
                            let idSinMLC = data2.id.replace("MLC", "#");
                            let contenido = "";
                            ids.push(idSinMLC);
                            //         contenido += `<div class="card w-75" style="
                            //     height: 200px;
                            //     border: solid 1px;
                            //     width: 100% !important;
                            //     ">
                            // <div class="card-body">
                            //         <div class="row pt-2">
                            //             <div class="col-3 "><a href="estadisticaPropiedadML.php?id=${data2.id}">${data2.title}</a></div>
                            //             <div class="col-3"><label>${idSinMLC}</label></div>
                            //             <div class="col-3"><label>${tipo}</label></div>
                            //             <div class="col-3"><label>${operacion}</label></div>
                            //         </div>
                            //         <div class="row pt-4">
                            //             <div class="col-3">${precio}</div>`;
                            let urlImg = data2.pictures[0].url;
                            let urlImgArray = urlImg.split(':');
                            urlImg = urlImgArray[0] + 's:' + urlImgArray[1];
                            contenido += `<div class="row mt-5">
                                <div class="col-4 m-0 p-0">
                                    <img src="${urlImg}" alt="" width="100%">
                                </div>
                                <div class="col-8">
                                    <div class="container-fluid m-0 p-0">
                                        <div class="row" style="">
                                            <div class="col-12">
                                                <!-- titulo -->
                                                <h2>${data2.title}</h2>

                                            </div>
                                            <div class="col-12">
                                                <!-- id -->
                                                ${idSinMLC}
                                            </div>
                                            <div class="col-6">
                                                <!-- tipo prop -->
                                                ${tipo}
                                            </div>
                                            <div class="col-6">
                                                <!-- tipo operacion -->
                                                ${operacion}
                                            </div>
                                        `;


                            let f = new Date();
                            let mes = "";
                            if (f.getMonth() <= 9) {
                                mes = "0" + f.getMonth();
                            } else {
                                mes = f.getMonth();
                            }
                            let hoy = f.getFullYear() + '-' + mes + '-' + f.getDate();
                            let ayer = f.getFullYear() + '-' + mes + '-' + (f.getDate() - 1);
                            fetch('https://api.mercadolibre.com/items/visits?ids=' + resultId + '&date_from=' + ayer + '&date_to=' + hoy, {
                                    method: 'GET'
                                })
                                .then(res => res.json())
                                .then(data3 => {
                                    let visitas = data3[0].total_visits;

                                    let preguntas = "";
                                    let urlPreguntas = "https://api.mercadolibre.com/questions/search?item=" + resultId + "&access_token=" + access_token;
                                    fetch(urlPreguntas, {
                                            method: 'GET'
                                        })
                                        .then(res => res.json())
                                        .then(data4 => {
                                            preguntas = data4.total;
                                            //     contenido += `

                                            // <div class="col-3">Visitas de hoy ${visitas}</div>
                                            //     <div class="col-3">Contactos: ${preguntas}</div>
                                            //         <div class="col-3">${start_time}</div>
                                            //         </div>
                                            //     </div>
                                            // </div>
                                            // `;
                                            contenido += `
                                            
                                                                <div class="col-6">
                                                                Visitas de hoy ${visitas}
                                                            </div>
                                                            <div class="col-6">
                                                                Contactos: ${preguntas}
                                                            </div>
                                                            <div class="col-6">
                                                                Fecha publicación: ${start_time}
                                                            </div>
                                                            <div class="col-6">
                                                                <!-- "baños" -->
                                                                cosa
                                                            </div>
                                                            <div class="col-6"></div>
                                                            <div class="col-6">
                                                                <!-- "Precio" -->
                                                                <span>${precio}</span>
                                                            </div>
                                                            <div class="col-12 pt-5 float-right">
                                                                <a href="estadisticaPropiedadML.php?id=${data2.id}"class="btn btn-matchRed">Ver Propiedad</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        `
                                            cards.innerHTML += contenido;
                                        });
                                });
                        }
                    });

            }
        });
    }

}