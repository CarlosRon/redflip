let formulario = document.getElementById("formC");

function validarRequeridos() {
    let area = document.getElementById("select_area");
    let corredor = document.getElementById("corredor");
    let operario = document.getElementById("operario");
    let nombre = document.getElementById("nombre");
    let apellido = document.getElementById("apellido");
    let origen = document.getElementById("origen");
    let captadaR = document.getElementById("captadaR");
    let partnerR = document.getElementById("partnerR");
    let prefijo = document.getElementById("sub_fono");
    let telefono = document.getElementById("telefono");
    let correo = document.getElementById("correo");
    let fecha = document.getElementById("fecha");
    let calle = document.getElementById("calle");
    let numero = document.getElementById("numero");
    let comuna = document.getElementById("comuna");
    let comuna11 = document.getElementById("comuna11");
    let comuna12 = document.getElementById("comuna12");

    if (area.value == "null") {
        area.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione un Area',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (corredor.value == "null") {
        corredor.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione un Corredor',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (operario.value == "null") {
        operario.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione un Operario',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (nombre.value == "") {
        nombre.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo "Nombre" está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (apellido.value == "") {
        apellido.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo "Apellido" está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    }
    // else if (rut.value == "") {
    //     rut.focus();
    //     Swal.fire({
    //         position: 'bottom-end',
    //         icon: 'warning',
    //         title: 'Campo "Rut" está vacío',
    //         showConfirmButton: false,
    //         toast: true,
    //         timer: 1000
    //     });
    // } 
    else if (origen.value == "") {
        origen.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione un Origen',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (origen.value == "17" && captadaR.value == "") {
        console.log("if 17");
        captadaR.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione una opción para "Captado por Redflip"',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (origen.value == "18" && partnerR.value == "") {
        partnerR.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione un "Partner Redflip"',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });

    } else if (prefijo.value == "") {
        prefijo.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione un Prefijo',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (telefono.value == "") {
        telefono.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo "Teléfono" está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (correo.value == "") {
        correo.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo "Correo" está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (fecha.value == "") {
        fecha.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione una Fecha',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (calle.value == "") {
        calle.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo "Calle" está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (numero.value == "") {
        numero.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo "Número" de la dirección está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (comuna.value == "null" && comuna11.value == "null" && comuna12.value == "null") {
        comuna.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Por favor seleccione una comuna',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else {
        return true;
    }
    return false;
}

document.getElementById("quit").addEventListener('click', function(e) {
    e.preventDefault();
    cookie();

    let area = document.getElementById("select_area");
    let corredor = document.getElementById("corredor");
    let operario = document.getElementById("operario");
    let nombre = document.getElementById("nombre");
    let apellido = document.getElementById("apellido");
    let rut = document.getElementById("rut");
    let origen = document.getElementById("origen");
    let captadaR = document.getElementById("captadaR");
    let partnerR = document.getElementById("partnerR");
    let prefijo = document.getElementById("sub_fono");
    let telefono = document.getElementById("telefono");
    let correo = document.getElementById("correo");
    let fecha = document.getElementById("fecha");
    let numero = document.getElementById("numero");
    let comuna = document.getElementById("comuna");
    let comuna11 = document.getElementById("comuna11");
    let comuna12 = document.getElementById("comuna12");

    if (validarRequeridos()) {
        var datos = new FormData(formulario);
        fetch('../controlador/enviarFormPlantillas.php?guardar=4', {
                method: 'POST',
                body: datos
            })
            .then(res => res.json())
            .then(data => {
                if (data.error == null) {
                    Swal.fire({
                        title: '¿Estas Seguro?',
                        text: "Puedes guardarlo y enviarlo después si te falta información, o enviarlo ahora",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#28a745',
                        confirmButtonText: 'Enviar',
                        cancelButtonText: 'Guardar',
                        showCloseButton: true,
                        reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            fetch('../controlador/enviarNotificacion.php?idForm=' + data.idForm)
                                .then(res2 => res2.json())
                                .then(data2 => {
                                    if (data2 == "yes") {
                                        Swal.fire({
                                            title: 'Datos enviados',
                                            text: "Se enviará una notificación para su aprobación",
                                            icon: 'success',
                                            showCancelButton: false,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'OK!'
                                        }).then((result) => {
                                            if (result.value) {
                                                location.href = "../pages/index.php";
                                            }
                                        });
                                    } else {
                                        Swal.fire({
                                            title: 'Esta notificación ya se envió',
                                            text: "Espere mientras los administadores aprueban su formulario",
                                            icon: 'warning',
                                            showCancelButton: false,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'OK!'
                                        }).then((result) => {
                                            if (result.value) {
                                                location.href = "https://redflip.net/pages/formulariosEnviados.php";
                                            }
                                        });
                                    }
                                });

                        } else {
                            Swal.fire({
                                title: 'Datos guardados',
                                text: "Formulario almacenado",
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'OK!'
                            }).then((result) => {
                                if (result.value) {
                                    // console.log(result.value);
                                    // location.href ="https://redflip.net/pages/misFormularios.php";
                                }
                            });
                        }
                    });
                }
            });
    } else {}

});

formulario.addEventListener("submit", function(e) {
    e.preventDefault();
    cookie();

    if (validarRequeridos()) {
        var datos = new FormData(formulario);

        fetch('../controlador/enviarFormPlantillas.php?guardar=1', {
                method: 'POST',
                body: datos
            }).then(res => res.json())
            .then(data => {
                console.log(data.calle);
                console.log("Error:" + data.error);
                if (data.error == null) {
                    Swal.fire({
                        position: 'bottom-end',
                        icon: 'success',
                        title: 'Los datos han sido guardados',
                        showConfirmButton: false,
                        toast: true,
                        timer: 1000
                    });

                    // fetch('../controlador/enviarNotificacion.php?idForm=' + data.idForm).then(res2=> res2.json())
                    // .then(data2 => console.log(data2))
                }
            });
    } else {}
});