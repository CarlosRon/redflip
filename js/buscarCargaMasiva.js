
$(buscar_datos());


function buscar_datos(consulta1,consulta2,consulta3, consulta4, consulta5){
    $.ajax({
        url: '../services/buscarCargaMasiva.php',
        type: 'POST',
        dataType: 'html',
        data: {consulta1: consulta1, consulta2: consulta2, consulta3:consulta3, consulta4:consulta4,consulta5: consulta5}
    })
    .done(function(respuesta){
        $("#datos").html(respuesta);
    })
    .fail(function(){
        console.log("error");
    });
}

function filtrar(consulta1){
    $.ajax({
        url: '../services/buscarCargaMasiva.php',
        type: 'POST',
        dataType: 'html',
        data: {consulta1: consulta1}
    })
    
    .done(function(respuesta){
        $("#datos").html(respuesta);
    })
    .fail(function(){
        console.log("error");
    });
}


var formulario = document.getElementById("filtro");

formulario.addEventListener('submit', function(e){
e.preventDefault();
var valor1 = document.getElementById("tipo_operacion").value;
var valor2 = document.getElementById("estado").value;
var valor3 = document.getElementById("fase").value;
var valor4 = document.getElementById("importancia").value;
buscar_datos(valor1,valor2,valor3, valor4);
// console.log(valor1);
// console.log(valor2);
// console.log(valor3);
// console.log(valor4);
});

//busqueda de enter en search
$(document).on('keyup', '#search', function(event){
    if (event.keyCode === 13){
            var valor = $(this).val();
    
        
            var valor2 = document.getElementById("estado").value;
            var valor3 = document.getElementById("fase").value;
            var valor4 = document.getElementById("importancia").value;
            buscar_datos(valor, valor2, valor3, valor4);        
    }
// q1 = consulta1 -> search
// q2 = consulta2 -> estado
// q3 = consulta3 -> fase
// q4 = consulta4 -> 

    

});