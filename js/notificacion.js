/*jshint esversion: 6 */

var btnNoti = document.getElementById("btnNoti");
var loader = document.getElementById("loader");
var notify = document.getElementById("notify");
var notifyM = document.getElementById("notifyM");

function notiAdmin() {
    // e.preventDefault();
    var timeout = null;
    notify.innerHTML = "<img src='../img/loading.gif' style='width:100%; height:180px;'>";
    notifyM.innerHTML = "";
    clearTimeout(timeout);
    timeout = setTimeout(function() {
        mostrarNotificaiones();
        btnNoti.style.disabled = true;
    }, 1500);
}
var idUs;

function NotiCorredor(id) {
    // var timeout = null;
    // notify.innerHTML = "<img src='../img/loading.gif' style='width:100%; height:180px;'>";
    // clearTimeout(timeout);
    // timeout = setTimeout(function() {
    //     notificacionCorredor(id);
    //     btnNoti.style.disabled = true;
    // }, 1500);
    notificacionCorredor(id);
    idUs = id;
}

function mostrarNotificaiones() {


    notify.innerHTML = "";
    fetch('../controlador/obtenerNotifiaciones.php', {
            method: 'GET'
        }).then(res => res.json())
        .then(datos => {
            // console.log(datos);
            // console.log(Object.keys(datos).length);
            if (datos.error != "1") {
                Push.create("Notificacion de formulario");
                notify.innerHTML = "";
                for (var i = 0; i < Object.keys(datos).length; i++) {
                    var i2 = "datos.notificacion" + i.toString() + ".responsable";
                    notify.innerHTML += `
            <li class="noty-li">
                <div class="noty-container">
                    <h5 class="noty-h5">Notificación</h5>
                    <p class="noty-p">${datos[i].responsable} ha creado un nuevo formulario.</p>
                    <div class="notFormBtn">
                        <input class="notFormBtnM noty-btn btn-info" type="button" value="Ver" onclick="redirigirCorredor(${datos[i].fk_formulario})">
                        <input class="notFormBtnM noty-btn btn-success" type="button" value="Aprobar" onclick="aprobar(${datos[i].fk_formulario})">
                    </div>
                </div>
            </li>
        `;
                    notifyM.innerHTML += `
            <li class="noty-li">
                <div class="noty-container">
                    <h5 class="noty-h5">Notificación</h5>
                    <p class="noty-p">${datos[i].responsable} ha creado un nuevo formulario.</p>
                    <div class="notFormBtn">
                        <input class="notFormBtnM noty-btn btn-info" type="button" value="Ver" onclick="redirigirCorredor(${datos[i].fk_formulario})">
                        <input class="notFormBtnM noty-btn btn-success" type="button" value="Aprobar" onclick="aprobar(${datos[i].fk_formulario})">
                    </div>
                </div>
            </li>
        `;
                }
            } else {
                notify.innerHTML = `<p>
        Sin datos
      </p>`;
                notifyM.innerHTML = `<p>
        Sin datos
      </p>`;
            }
            btnNoti.style.disabled = false;
        });
}

function redirigir2(id) {
    // window.open('fichaForm.php?id='+id, '');
    window.open("fichaForm.php?id=" + id + "&origen=1", 'name', 'height=800,width=700 center');
}

function aprobar(id) {
    fetch('../controlador/aprobarForm.php?idForm=' + id)
        .then(res => res.json())
        .then(data => {
            // mostrarNotificaiones();
            // console.log(data)
            notificaciones();
        });
}

function redirigirCorredor(id) {
    // window.open('fichaForm.php?id='+id, '');
    window.open("fichaForm.php?id=" + id + "&origen=1", 'name', 'height=800,width=700 center');
}

function notificacionCorredor(id) {
    fetch('../controlador/obtenerNotificacionesCorredor.php?id=' + id)
        .then(res => res.json())
        .then(datos => {
            let notify = document.getElementById("notify");
            let notifyM = document.getElementById("notifyM");
            console.log(datos);
            console.log(Object.keys(datos).length);
            let cont = Object.keys(datos).length;
            if (datos[cont].error == 2) {

                console.log("entra")
                notify.innerHTML = "";
                notifyM.innerHTML = "";
                for (var i = 0; i < Object.keys(datos).length - 1; i++) {
                    if (datos[i].estado == 2 || datos[i].estado == 1) {

                        console.log("entra al 2")
                        notify.innerHTML += `
                <li class="noty-li">
                    <div class="noty-container">
                        <h5 class="noty-h5">Formulario aprobado</h5>
                        <p class="noty-p">${datos[i].nombre_form} ha sido aprobado.</p>
                        <div class="notFormBtn">
                            <input class="notFormBtnM noty-btn btn-info" type="button" value="Ver" onclick="redirigirCorredor(${(datos[i].idForm)})">
                            <input class="notFormBtnM noty-btn btn-success" type="button" value="Aceptar" onclick="aceptar(${datos[i].id}, ${id})">
                        </div>
                    </div>
                </li>
            `;
                        notifyM.innerHTML += `
                <li class="noty-li">
                    <div class="noty-container">
                        <h5 class="noty-h5">Formulario aprobado</h5>
                        <p class="noty-p">${datos[i].nombre_form} ha sido aprobado.</p>
                        <div class="notFormBtn">
                            <input class="notFormBtnM noty-btn btn-info" type="button" value="Ver" onclick="redirigirCorredor(${(datos[i].idForm)})">
                            <input class="notFormBtnM noty-btn btn-success" type="button" value="Aceptar" onclick="aceptar(${datos[i].id}, ${id})">
                        </div>
                    </div>
                </li>
            `;
                    } else if (datos[i].estado == 3) {
                        notify.innerHTML += `
            <li class="noty-li">
                <div class="noty-container">
                    <h5 class="noty-h5">Formulario Rechazado</h5>
                    <p class="noty-p">${datos[i].nombre_form} ha sido rechazado.</p>
                    <div class="notFormBtn">
                        <input class="notFormBtnM noty-btn btn-info" type="button" value="Ver" onclick="redirigirCorredor(${(datos[i].idForm)})">
                        <input class="notFormBtnM noty-btn btn-success" type="button" value="Aceptar" onclick="aceptar(${datos[i].id}, ${id})">
                    </div>
                </div>
            </li>
        `;
                        notifyM.innerHTML += `
            <li class="noty-li">
                <div class="noty-container">
                    <h5 class="noty-h5">Formulario Rechazado</h5>
                    <p class="noty-p">${datos[i].nombre_form} ha sido rechazado.</p>
                    <div class="notFormBtn">
                        <input class="notFormBtnM noty-btn btn-info" type="button" value="Ver" onclick="redirigirCorredor(${(datos[i].idForm)})">
                        <input class="notFormBtnM noty-btn btn-success" type="button" value="Aceptar" onclick="aceptar(${datos[i].id}, ${id})">
                    </div>
                </div>
            </li>
        `;
                    }
                }
            } else {
                // console.log("no")
                notify.innerHTML = `<p>
            Sin datos
        </p>`;
                notifyM.innerHTML = `<p>
            Sin datos
        </p>`;
            }
            btnNoti.style.disabled = false;

        });
}

function aceptar(idNoti, id) {
    fetch("../controlador/vistoNotificacionCorredor.php?id=" + idNoti)
        .then(res => res.json())
        .then(datos => {
            // let notify=document.getElementById("notify");
            console.log("aceptar");
            notificacionCorredor(id);
            notificaciones();
        });
}