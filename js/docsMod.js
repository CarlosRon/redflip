let formMod = document.getElementById("modificaDoc");

formMod.addEventListener('submit', function(e) {
    e.preventDefault();
    let data = new FormData(formMod);

    fetch('documentosMod.php', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(datos => {
            console.log(datos);
            if (datos.respuesta === "2") {
                console.log("la respuesta es positiva");
                Swal.fire({
                        icon: 'success',
                        title: 'Excelente',
                        text: `${datos.ingresoBD}`
                    })
                    .then((result) => {
                        if (result.value) {
                            window.location = "documentosVer.php";
                        }
                    });
            } else {
                console.log("la respuesta es negativa");
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: `${datos.error}`
                })
            }
        })
        // .catch(error => console.log('Error: ' + error));
});