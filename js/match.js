/*jshint esversion: 6 */
var btn = document.getElementById("demo");
var lista = document.getElementById("lista");

function ids(id) {
    let input = document.getElementById(id);
    let textArea = document.getElementById("claves");
    let titulo = document.getElementById("titulo-" + id);

    if (input.checked) {
        let li = document.createElement("li");
        li.innerHTML = titulo.textContent;
        li.id = 'li-' + id;
        lista.appendChild(li);
        console.log(textArea.value.length);
        if (textArea.value.length == 0) {
            textArea.value = id;
        } else {
            textArea.value += "-" + id;
        }

    } else if (!input.checked || textArea.value.charAt(textArea.length - 1) == "-") {
        let li = document.getElementById('li-' + id);
        let padre = li.parentNode;
        padre.removeChild(li);
        textArea.value = textArea.value.replace(id, "");

        if (textArea.value.charAt(0) == "-") {
            textArea.value = textArea.value.substring(1);
        }
    }
    let array = textArea.value.split("-");
    let cleanArray = new Array();
    for (let i = 0; i < array.length; i++) {
        if (array[i]) {
            cleanArray.push(array[i]);
        }
    }
    console.log(cleanArray);

}

function mail() {

    let textArea = document.getElementById("claves");
    let checkcorreo = document.getElementById("EnvCorreo");
    let EnvWahts = document.getElementById("EnvWahts");
    let array = textArea.value.split("-");
    // let cleanArray = new FormData();
    // for (let i = 0; i < array.length; i++) {
    //     if (array[i]) {
    //         cleanArray.append(`${i}`, array[i]);
    //     }
    // }
    let cleanArray = new Array();
    for (let i = 0; i < array.length; i++) {
        if (array[i]) {
            cleanArray.push(array[i]);
        }
    }
    let string = "";
    for (let e = 0; e < cleanArray.length; e++) {
        if (e == 0) {
            string = cleanArray[e];
        } else {
            string += "-" + cleanArray[e];
        }
    }
    console.log("el string es: " + string)
    if (string == "") {
        Swal.fire({
            icon: 'warning',
            title: 'Selecciona una propiedad',
            text: 'No se ha seleccionado ninguna propiedad'
        })
    } else {
        if (checkcorreo.checked) {
            if (EnvWahts.checked) {
                // correo y whatsapp
                console.log("se chequeó correo");
                let mensaje = document.getElementById("mensaje").value;
                let correo = document.getElementById("correo").value;
                let telefono = document.getElementById("telefono").value;
                let datos = new FormData();
                datos.append("ids", string);
                datos.append("mensaje", mensaje);
                datos.append("correo", correo);
                datos.append("telefono", telefono);
                fetch("enviarMailInteresado.php", {
                        method: "POST",
                        body: datos
                    }).then(res => res.json())
                    .then(data => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Correcto',
                            text: 'Se envió el Correo'
                        });
                        console.log(data);
                    });
                // para whatsapp
                fetch("enviarWhatsapp.php", {
                        method: "POST",
                        body: datos
                    })
                    .then(res => res.json())
                    .then(data => {
                        console.log("El mensaje es: " + data);
                        window.open("https://api.whatsapp.com/send?phone=" + telefono + "&text=" + data);
                    });

            } else {

                // solo correo
                console.log("se chequeó correo");
                let mensaje = document.getElementById("mensaje").value;
                let correo = document.getElementById("correo").value;
                let telefono = document.getElementById("telefono").value;
                let datos = new FormData();
                datos.append("ids", string);
                datos.append("mensaje", mensaje);
                datos.append("correo", correo);
                datos.append("telefono", telefono);
                fetch("enviarMailInteresado.php", {
                        method: "POST",
                        body: datos
                    }).then(res => res.json())
                    .then(data => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Correcto',
                            text: 'Se envió el Correo'
                        });
                        console.log(data);
                    });
            }
        } else if (EnvWahts.checked && !checkcorreo.checked) {
            // solo whatsapp
            let mensaje = document.getElementById("mensaje").value;
            let correo = document.getElementById("correo").value;
            let telefono = document.getElementById("telefono").value;
            let datos = new FormData();
            datos.append("ids", string);
            datos.append("mensaje", mensaje);
            datos.append("correo", correo);
            datos.append("telefono", telefono);
            fetch("enviarWhatsapp.php", {
                    method: "POST",
                    body: datos
                })
                .then(res => res.json())
                .then(data => {
                    console.log("El mensaje es: " + data);
                    window.open("https://api.whatsapp.com/send?phone=" + telefono + "&text=" + data);
                });
        } else {
            //ninguna
            Swal.fire({
                icon: 'warning',
                title: 'Selecciona una opcion de envío',
                text: 'No se ha seleccionado ninguna opción de envío'
            })
        }
    }
}

function verMas() {
    console.log(btn.classList[1]);
    var button = document.getElementById("verMas");
    if (btn.classList[1] == "show") {
        console.log("menos");
        button.textContent = "Ver más";
    } else {
        console.log("mas");
        button.textContent = "Ver menos";
    }

}

function descartar(id, corredor, interesado) {
    Swal.fire({
        title: 'Estas seguro?',
        text: "no podrás ver mas match de esta propiedad",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, descartalo!'
    }).then((result) => {
        if (result.value) {
            // fetch
            let datos = new FormData();
            datos.append('id', id);
            datos.append('corredor', corredor);
            datos.append('interesado', interesado);
            fetch('../controlador/descartarForm.php', {
                    method: 'POST',
                    body: datos
                })
                .then(resp => resp.text())
                .then(data => {
                    console.log(data);
                    Swal.fire(
                            'Descartado!',
                            'La propiedad ha sido descartada',
                            'success'
                        )
                        .then(res => location.reload());

                });
            // fin fetch

        }
    });

}