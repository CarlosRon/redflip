let formArrays = document.getElementById("form");
    document.addEventListener("submit", function(e){
        e.preventDefault();
        var datos = new FormData(formArrays);

        fetch('../controlador/cargaInicial.php',{

            method: 'POST',
    
            body: datos
    
        })
        .then(res=>res.json())
        .then(data =>{
            console.log(data);
            if(data.error == null){
                Swal.fire(
                    'Bien hecho!',
                    'Datos Guardados!',
                    'success'
                  ).then((result) =>{
                    if(result.value){
                        window.close();
                    }
                  })
            }
        })
    })
    