function eliminarU(id) {
    Swal.fire({
        title: '¿Deseas eliminar este usuario?',
        text: "Esta acción no se puede revertir.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, borrar.',
        cancelButtonText: 'Cancelar.'
    }).then((result) => {
        if (!result.dismiss) {
            fetch('usuariosDisable.php?id=' + id, {
                    method: 'GET'
                })
                .then(res => res.json())
                .then(datos => {
                    console.log(datos);
                    if (datos.respuesta === "2") {
                        console.log("la respuesta es positiva");
                        Swal.fire({
                                icon: 'success',
                                title: 'Excelente',
                                text: `${datos.ingresoBD}`
                            })
                            .then((result) => {
                                if (result.value) {
                                    window.location = "usuariosVer.php";
                                }
                            });
                    } else {
                        console.log("la respuesta es negativa");
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: `${datos.error}`
                        })
                    }
                })
                .catch(error => console.log('Error: ' + error));
        } else {
            console.log("no confirmado");
        }
    })

}

function activarU(id) {
    fetch('usuariosEnable.php?id=' + id, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(datos => {
            console.log(datos);
            if (datos.respuesta === "2") {
                console.log("la respuesta es positiva");
                Swal.fire({
                        icon: 'success',
                        title: 'Excelente',
                        text: `${datos.ingresoBD}`
                    })
                    .then((result) => {
                        if (result.value) {
                            // window.location = "documentosVer.php";
                        }
                    });
            } else {
                console.log("la respuesta es negativa");
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: `${datos.error}`
                })
            }
        })
        .catch(error => console.log('Error: ' + error));
}

$('.view_user').click(function(e) {
    e.preventDefault();
    let user = $(this).attr('userModal');
    data.append('idUsuario', idUsuario);
    fetch('../controlador/datosUsuario.php', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(datos => {
            console.log(datos);
            let contenido = `
            <tr>
                <td>Nombre</td>
                <td>${datos.nombre+' '+datos.apellido}</td>
            </tr>
            `;
            $('.usuarioModal').html(contenido)
            $('.modal').fadeIn();
        })

});

function closeModal() {
    $('.modal').fadeOut();
}