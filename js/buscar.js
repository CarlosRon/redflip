$(buscar_datos());


function buscar_datos(consulta1, consulta2, consulta3, consulta4, consulta5) {
    var tablaLoader = `
    <div class="row">
        <div class="col-12 center">
        <i class='fas fa-sync-alt fa-spin fa-2x'></i>
        </div>
    </div>
`;
    var tablaLoader2 = `
    <div class="preloader">
        <div class="loading">
            <h2 style="padding-bottom: 0px!important">
                Cargando...
            </h2>
            <span class="progress"></span>
        </div>
    </div>
`;

    $("#datos").html(tablaLoader2);
    $.ajax({
            url: '../services/buscar.php',
            type: 'POST',
            dataType: 'html',
            data: { consulta1: consulta1, consulta2: consulta2, consulta3: consulta3, consulta4: consulta4, consulta5: consulta5 }
        })
        .done(function(respuesta) {
            $("#datos").html(respuesta);
            tabla();
        })
        .fail(function() {
            console.log("error");
        });
}

function filtrar(consulta1) {
    $.ajax({
        url: '../services/buscar.php',
        type: 'POST',
        dataType: 'html',
        data: { consulta1: consulta1 }
    })

    .done(function(respuesta) {
            $("#datos").html(respuesta);
            tabla();
        })
        .fail(function() {
            console.log("error");
        });
}


var formulario = document.getElementById("filtro");

formulario.addEventListener('submit', function(e) {
    e.preventDefault();
    var valor1 = document.getElementById("tipo_operacion").value;
    var valor2 = document.getElementById("estado").value;
    var valor3 = document.getElementById("fase").value;
    var valor4 = document.getElementById("importancia").value;
    buscar_datos(valor1, valor2, valor3, valor4);
    // console.log(valor1);
    // console.log(valor2);
    // console.log(valor3);
    // console.log(valor4);
});

//busqueda de enter en search
$(document).on('keyup', '#search', function(event) {
    if (event.keyCode === 13) {
        var valor = $(this).val();


        var valor2 = document.getElementById("estado").value;
        var valor3 = document.getElementById("fase").value;
        var valor4 = document.getElementById("importancia").value;
        buscar_datos(valor, valor2, valor3);
    }
    // q1 = consulta1 -> search
    // q2 = consulta2 -> estado
    // q3 = consulta3 -> fase
    // q4 = consulta4 -> 



});

function tabla() {
    $('th').click(function() {
        console.log("object");
        var table = $(this).parents('table').eq(0)
        var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
        this.asc = !this.asc
        if (!this.asc) {
            rows = rows.reverse()
        }
        for (var i = 0; i < rows.length; i++) {
            table.append(rows[i])
        }
        setIcon($(this), this.asc);
    })

    function comparer(index) {
        return function(a, b) {
            var valA = getCellValue(a, index),
                valB = getCellValue(b, index)
            return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
        }
    }

    function getCellValue(row, index) {
        return $(row).children('td').eq(index).html()
    }

    function setIcon(element, asc) {
        $("th").each(function(index) {
            $(this).removeClass("sorting");
            $(this).removeClass("asc");
            $(this).removeClass("desc");
        });
        element.addClass("sorting");
        if (asc) element.addClass("asc");
        else element.addClass("desc");
    }

}