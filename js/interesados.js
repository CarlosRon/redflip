function eliminarInt(id, idPer) {

    Swal.fire({
        title: '¿Estas seguro?',
        text: "El interesado no podrá ser recuperado",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borralo!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {

            Swal.fire(
                'Eliminado!',
                'El registro ha sido eliminado',
                'success'
            ).then((result2) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "../controlador/eliminarInteresado.php",
                        data: { id: id, idPer: idPer },
                        success: function(data) {
                            console.log(data);
                            location.reload();
                        }
                    });
                }
            });
        }
    });




}