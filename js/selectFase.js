var estado = document.getElementById('estado');
var fase = document.getElementById('fase');


estado.addEventListener('change', function(){
    fase.remove("Finalizado");
    fase.remove("Cancelado");
    fase.remove("Publicado");

    fase.remove("Contacto");
    fase.remove("Primera Visita");
    fase.remove("Tour Virtual");
    fase.remove("Todos");
    
if(estado.value === "propietario"){
    
    var option4 = document.createElement('option');
    option4.text = 'Todos';
    option4.value = '';
    var option = document.createElement('option');
    option.text = 'Finalizado';
    option.value = 'Finalizado';
    var option3 = document.createElement('option');
    option3.text = 'Publicado';
    option3.value = 'Publicado';    
    var option5 = document.createElement('option');
    option5.text = 'Despublicado';
    option5.value = 'Despublicado';
    var option2 = document.createElement('option');
    option2.text = 'Cancelado';
    option2.value = 'Cancelado';    
    fase.add(option4);
    fase.add(option);
    fase.add(option2);
    fase.add(option3);
    fase.add(option5);
} else if(estado.value === "prospecto"){
   
    
    var option4 = document.createElement('option');
    option4.text = 'Todos';
    option4.value = '';
    var option5 = document.createElement('option');
    option5.text = 'No Contactado';
    option5.value = 'No Contactado';
    var option = document.createElement('option');
    option.text = 'Contacto';
    option.value = 'Contacto';
    var option2 = document.createElement('option');
    option2.text = 'Primera Visita';
    option2.value = 'Primera Visita';
    var option3 = document.createElement('option');
    option3.text = 'Tour Virtual';
    option3.value = 'Tour Virtual';
    fase.add(option4);
    fase.add(option5);
    fase.add(option);
    fase.add(option2);
    fase.add(option3);
    

    
}else if(estado.value === ""){
    var option4 = document.createElement('option');
    option4.text = 'Todos';
    option4.value = '';
    fase.add(option4);

}
})
