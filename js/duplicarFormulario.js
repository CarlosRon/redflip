function duplicarForm(e, idForm) {
    e.preventDefault();
    let data = new FormData();
    data.append('idForm', idForm);
    fetch('../controlador/datosFormulario.php', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(datos => {
            console.log(datos);
            let contenido = `
            <form id="formDuplicado">
                <input type="hidden" id="idForm" value="${idForm}">
                <input type="hidden" id="fk_operario" value= "${datos.fk_operario}">
                <div class="form-group">
                    <label for="tipo_op_select" class="label_titulo">Tipo de Operación</label>
                    <select name="tipo_op_select" id="tipo_op_select" class="dropdown">
                        <option value="2" `;
            if (datos.operacion == 2) { contenido += `selected` }
            contenido += `>Arriendo</option>
                        <option value="1"`;
            if (datos.operacion == 1) { contenido += `selected` }
            contenido += `>Venta</option>
                        <option value="3"`;
            if (datos.operacion == 3) { contenido += `selected` }
            contenido += `>Ambos</option>
                    </select>
                    <label for="select_area" class="label_titulo">Área Redflip</label>
                    <select name="select_area" id="select_area" class="dropdown">
                        <option value="null" disabled>Área Redflip*</option>
                        <option value="Redflip Habitacional" `;
            if (datos.area_redflip == "Redflip Habitacional") { contenido += `selected` }
            contenido += `>Redflip Habitacional</option>
                        <option value="Redflip Comercial" `;
            if (datos.area_redflip == "Redflip Redflip Comercial") { contenido += `selected` }
            contenido += `>Redflip Comercial</option>
                        <option value="Redflip Luxury" `;
            if (datos.area_redflip == "Redflip Luxury") { contenido += `selected` }
            contenido += `>Redflip Luxury</option>
                    </select>
                </div>
                <div class="select-container">
                    <label for="corredor" class="label_titulo">Corredor</label>
                    <select name="corredor" id="corredor">
                        <option value="null" disabled>Nombre Corredor*</option>`;
            datos.corredores.forEach(corredor => {
                if (corredor.id == datos.fk_corredor) {
                    contenido += `<option selected value="${corredor.id}">${corredor.nombre}</option>"`;
                } else {
                    contenido += `<option value="${corredor.id}">${corredor.nombre}</option>"`;
                }
            });
            contenido += `</select>
                    </div>
                <div class="select-container">
                    <label for="operario" class="label_titulo">Operario Cámara</label>
                    <select name="operario" id="operario">
                        <option value="null" disabled selected>Operario cámara*</option>`;
            datos.operarios.forEach(operario => {
                if (datos.fk_operario == operario.id) {
                    contenido += `<option selected value="${operario.id}">${operario.nombre}</option">`;
                } else {
                    contenido += `<option value="${operario.id}">${operario.nombre}</option">`;
                }
            });
            contenido += `</select>
                </div>
                <label for="nombre" class="label_titulo">Nombre</label>
                <input class="fs-nom" type="text" value="${datos.nombre}" name="nombre" placeholder="Nombre*" id="nombre" onfocusout="validarNombre()"/>
                <label for="apellido" class="label_titulo">Apellido</label>
                <input class="fs-ape" type="text" value="${datos.apellido}"name="apellido" placeholder="Apellido*" id="apellido" onfocusout="validarApellido()"/>
                <label for="rut" class="label_titulo">Rut</label>
                <input class="fs-rut" type="text" value="${datos.rut}" name="rut" placeholder="Rut" id="rut" onfocusout="validarRut2()"/>
                <div class="select-container" id="">
                    <label for="origen" class="label_titulo">Origen</label>
                    <select name="origen" id="origen" class="fs-ori" onchange="cambioOrigen()">
                        <option value="" disabled>Origen*</option>
                        <option value="17"`;
            if (datos.origen1 == 17) { contenido += `selected` }
            contenido += ` >Captado por Redflip</option>
                        <option value="7"`;
            if (datos.origen1 == 7) { contenido += `selected` }
            contenido += ` >Referido Corredor</option>
                        <option value="6"`;
            if (datos.origen1 == 6) { contenido += `selected` }
            contenido += ` >Referido Cliente</option>
                        <option value="5"`;
            if (datos.origen1 == 5) { contenido += `selected` }
            contenido += ` >Referido Conserje</option>
                        <option value="18"`;
            if (datos.origen1 == 18) { contenido += `selected` }
            contenido += `>Partner Redflip</option>                         
                    </select>
                </div>
                <div class="select-container" id="div_captadaR" style="display: none">
                <label for="captadaR" class="label_titulo">Seleccione:</label>
                    <select name="captadaR" id="captadaR" class="fs-ori">
                            <option value="" disabled selected>Seleccione opción*</option>
                            <option value="9" `;
            if (datos.origen2 == 9) { contenido += `selected` }
            contenido += `>Base de Datos</option>
                            <option value="19" `;
            if (datos.origen2 == 19) { contenido += `selected` }
            contenido += `>Google</option>
                            <option value="1" `;
            if (datos.origen2 == 1) { contenido += `selected` }
            contenido += `>Instagram</option>
                            <option value="2" `;
            if (datos.origen2 == 2) { contenido += `selected` }
            contenido += `>Facebook</option>
                            <option value="23" `;
            if (datos.origen2 == 23) { contenido += `selected` }
            contenido += `>Desconocido</option>                     
                    </select>
                </div>
                <div class="select-container" id="div_partnerR" style="display: none">
                    <label for="partnerR" class="label_titulo">Seleccione:</label>
                    <select name="partnerR" id="partnerR" class="fs-ori">
                            <option value="" disabled selected>Seleccione opción*</option>
                            <option value="11"`;
            if (datos.origen2 == 11) { contenido += `selected` }
            contenido += ` >Doorlist</option>
                            <option value="20" `;
            if (datos.origen2 == 20) { contenido += `selected` }
            contenido += `>De Carlo</option>
                            <option value="12" `;
            if (datos.origen2 == 12) { contenido += `selected` }
            contenido += `>OpenCasa</option>
                            <option value="21" `;
            if (datos.origen2 == 21) { contenido += `selected` }
            contenido += `>UHomie</option>
                            <option value="13" `;
            if (datos.origen2 == 13) { contenido += `selected` }
            contenido += `>GI0</option>
                            <option value="2" `;
            if (datos.origen2 == 22) { contenido += `selected` }
            contenido += `>Stoffel & Valdivieso</option>
                            <option value="15" `;
            if (datos.origen2 == 15) { contenido += `selected` }
            contenido += `>Otro</option>
                            <option value="23" `;
            if (datos.origen2 == 23) { contenido += `selected` }
            contenido += `>Desconocido</option>                    
                    </select>
                </div>
                <div class="form-group">
                    <div class="selectDivisa-container">
                        <label for="telefono" class="label_titulo">Teléfono</label><br>
                        <select name="sub_fono" id="sub_fono" class="fs-suFono">
                            <option value="" disabled selected>Prefijo*</option>
                            <option value="+569" `;
            if (datos.telefono != null) {
                if ((datos.telefono.substring(0, 4)) == "+569") { contenido += `selected` }
            }
            contenido += `>+569</option>
                            <option value="+562" `;
            if (datos.telefono != null) {
                if ((datos.telefono.substring(0, 4)) == "+562") { contenido += `selected` }
            }
            contenido += `>+562</option>
                            <option value="+568"`;
            if (datos.telefono != null) {
                if ((datos.telefono.substring(0, 4)) == "+568") { contenido += `selected` }
            }
            contenido += `>+568</option>
                            <option value="+56">+56</option>
                        </select>
                    </div>
                    <input class="fs-fono" type="tel" value="`;
            if (datos.telefono != null) {
                contenido += `${(datos.telefono).substring(4)}`;
            }
            contenido += `" name="telefono" id="telefono" placeholder="Teléfono*" onfocusout="validarTelefono()" />
                </div>
                <label for="correo" class="label_titulo">Correo</label>
                <input type="mail" value="${datos.correo}" name="correo" id="correo" placeholder="Correo*" onfocusout="validarCorreo()" />
                <label for="fecha" class="label_titulo">Fecha</label>
                <input type="date" value="${datos.fecha}" name="fecha" id="fecha" placeholder="Fecha" />
                <button type="button" onclick="duplicacionarForm()" class="btn btn-primary">Duplicar</button>
            </form>
        `;
            $('.tablaModal2').html(contenido);
            $('#modal2').fadeIn();
            cambioOrigen();
        })

}

function closeModal2() {
    $('#modal2').fadeOut();
}

function cambioOrigen() {
    let select = document.getElementById("origen");
    let captadaR = document.getElementById("div_captadaR");
    let partnerR = document.getElementById("div_partnerR");
    switch (select.value) {
        case "17":
            captadaR.style.display = "";
            partnerR.style.display = "none";
            break;
        case "18":
            captadaR.style.display = "none";
            partnerR.style.display = "";
            break;
        default:
            captadaR.style.display = "none";
            partnerR.style.display = "none";
            break;
    }
}

// document.getElementById("formDuplicado").addEventListener("submit", function(e) {
//     e.preventDefault()

// })

function duplicacionarForm() {

    let form = new FormData();
    let idForm = document.getElementById("idForm").value;
    let select_area = document.getElementById("select_area").value;
    let tipo_op_select = document.getElementById("tipo_op_select").value;
    let corredor = document.getElementById("corredor").value;
    let operario = document.getElementById("operario").value;
    let nombre = document.getElementById("nombre").value;
    let apellido = document.getElementById("apellido").value;
    let rut = document.getElementById("rut").value;
    let origen = document.getElementById("origen").value;
    let captadaR = document.getElementById("captadaR").value;
    let partnerR = document.getElementById("partnerR").value;
    let sub_fono = document.getElementById("sub_fono").value;
    let telefono = document.getElementById("telefono").value;
    let correo = document.getElementById("correo").value;
    let fecha = document.getElementById("fecha").value;

    form.append('idForm', idForm);
    form.append('select_area', select_area);
    form.append('tipo_op_select', tipo_op_select);
    form.append('corredor', corredor);
    form.append('operario', operario);
    form.append('nombre', nombre);
    form.append('apellido', apellido);
    form.append('rut', rut);
    form.append('origen', origen);
    form.append('captadaR', captadaR);
    form.append('partnerR', partnerR);
    form.append('sub_fono', sub_fono);
    form.append('telefono', telefono);
    form.append('correo', correo);
    form.append('fecha', fecha);

    fetch('../controlador/duplicarFormulario.php', {
            method: 'POST',
            body: form
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            Swal.fire(
                '¡Listo!',
                'El formulario ha sido duplicado',
                'success'
            ).then(value => {
                if (value) {
                    location.reload();
                }
            })
        })
}