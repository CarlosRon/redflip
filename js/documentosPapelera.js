function restaurar(id) {
    fetch('documentosRestaurar.php?id=' + id, {
            method: 'GET'
        })
        .then(res => res.json())
        .then(datos => {
            console.log(datos);
            if (datos.respuesta === "2") {
                console.log("la respuesta es positiva");
                Swal.fire({
                        icon: 'success',
                        title: 'Excelente',
                        text: `${datos.ingresoBD}`
                    })
                    .then((result) => {
                        if (result.value) {
                            window.location = "documentosPapelera.php";
                        }
                    });
            } else {
                console.log("la respuesta es negativa");
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: `${datos.error}`
                })
            }
        })
        .catch(error => console.log('Error: ' + error));
}