/*jshint esversion: 6 */

// Graficos

let idCorredor = document.getElementById('idCorredor').value;


Chart.defaults.global.defaultFontFamily = "futura-pt";
Chart.defaults.global.defaultFontSize = 18;
Chart.defaults.global.legend.labels.usePointStyle = true;
var chartPreferences = document.getElementById("chartPreferences2");

chartPreferences.innerHTML = '<i class="fas fa-sync-alt fa-spin fa-2x" style="margin: 25%;"></i>';
let body = new FormData();
body.append('idCorredor', idCorredor);
fetch('../controlador/datosGrafCartera.php', { //fetch para el grafico de barras
        method: 'POST',
        body: body
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        chartPreferences.innerHTML = '<canvas id="graficoBarras" responsive="true" width="" height=""></canvas>';
        var popCanvasBarras = document.getElementById("graficoBarras");
        var barChart = new Chart(popCanvasBarras, {
            type: 'bar',
            options: {
                responsive: true,
                aspectRatio: 1.3,
                legend: {
                    // display: false,
                    position: 'bottom',
                    usePointStyle: true,
                    labels: {
                        // This more specific font property overrides the global property
                        // fontColor: 'black',
                        fontSize: 14,

                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true

                                }

                            }]

                        }
                    }
                }
            },
            data: {
                labels: ["Ventas", "Arriendos", "Ambos"],
                datasets: [{
                    label: 'Casa',
                    data: [data.casa.venta, data.casa.arriendo, data.casa.ambos],
                    backgroundColor: [
                        "#e8505b",
                        "#e8505b",
                        "#e8505b"
                    ]
                }, {
                    label: 'Depto',
                    data: [data.depto.venta, data.depto.arriendo, data.depto.ambos],
                    backgroundColor: [
                        "#a7a7a7",
                        "#a7a7a7",
                        "#a7a7a7"
                    ]
                }, {
                    label: 'Oficina',
                    data: [data.oficina.venta, data.oficina.arriendo, data.oficina.ambos],
                    backgroundColor: [
                        "#6ec3be",
                        "#6ec3be",
                        "#6ec3be"
                    ]
                }],
            }
        });






    });

var chartPreferencesTorta = document.getElementById("chartPreferences");

chartPreferencesTorta.innerHTML = '<i class="fas fa-sync-alt fa-spin fa-2x" style="margin: 25%;"></i>';
fetch('../controlador/datosGrafComunas.php', {
        method: 'POST',
        body: body
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        chartPreferencesTorta.innerHTML = '<canvas id="graficoTorta" responsive="true" width="" height=""></canvas>';
        var oilCanvasTorta = document.getElementById("graficoTorta");
        var oilData = {
            labels: data.comunas,
            datasets: [{
                data: data.conteo,
                backgroundColor: data.colores
            }]
        };

        var pieChart = new Chart(oilCanvasTorta, {
            type: 'pie',
            data: oilData,
            options: {
                responsive: true,
                aspectRatio: 1.3,
                legend: {
                    // display: false,
                    position: 'bottom',
                    usePointStyle: true,
                    labels: {
                        // This more specific font property overrides the global property
                        fontColor: 'black',
                        fontSize: 14
                    }
                }
            }
        });


    })







var formData = new FormData();
formData.append('id', idCorredor);
let match = document.getElementById("match");
let loaderMatch = '<thead class="thead-match"> <tr class ="tr-matchHead"><th>#</th> <th>Descripción</th> <th> </th> </tr> </thead> <tbody><tr><td></td><td class="center"><i class="fas fa-sync-alt fa-spin fa-2x"></i></td></tr>';
match.innerHTML = loaderMatch;
fetch("../controlador/peticionMatch.php", {
        method: 'POST',
        body: formData,

    }).then(res => res.json())
    .then(data => {
        console.log(data.resp[0].URL);

        if (data.error == "") {
            match.innerHTML = '<thead class="thead-match"> <tr class ="tr-matchHead"><th>#</th> <th>Descripción</th> <th> </th> </tr> </thead> <tbody> ';
            let cont = 0;
            data.resp.forEach(element => {

                match.innerHTML += `<tr class="tr-matchBody">
                    <td>${cont+1}</td>
                    <td>Tiene un match con ${data.array.length} propiedades</td>
                    <td>
                        <a href="${data.resp[cont].URL}" class="btn btn-matchGreen">
                            <i class="fas fa-eye"></i> Ver
                        </a>
                    </td>
                </tr>`;
            });

            cont++;
        } else {
            match.innerHTML = `<thead class="thead-match"> <tr class ="tr-matchHead"><th>#</th> <th>Descripción</th> <th> </th> </tr> </thead> <tbody> 
            <tr class="tr-matchBody tab-center">
    <td colspan="3">No hay match</td>
</tr>
                        `;
        }
        match.innerHTML += '</tbody>';
    })
    .catch(err => {
        console.log(err);
    });