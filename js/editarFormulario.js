/*jshint esversion: 6 */

var casa = document.getElementById("casa");
var depto = document.getElementById("depto");
var terreno = document.getElementById("terreno");
var dpto = document.getElementsByClassName("dpto");
var local = document.getElementById("local");
var bodegaTipoProp = document.getElementById("bodegaTipoProp");
var parcela = document.getElementById("parcela");
var ofi = document.getElementById("of");
var casas = document.getElementsByClassName("casa");
var field5 = document.getElementById("5");
var condominio = document.getElementById("condominio");
var CantPisosCasa = document.getElementById("CantPisosCasa");
var idForm = document.getElementById("idForm").value;

function exteriorD() {
    fetch("../controlador/obtenerDatosForm.php?idForm=" + idForm)
        .then(res => res.json())
        .then(data => {
            // console.log(data.riego);
            var exteriorCont = `<div class="column col-6 casa">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="riego">
                    Riego Automático
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="riego" name="riego"${data.riego == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="azotea">
                    Azotea habilitada
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="azotea" name="azotea" ${data.azotea == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="gimnasio">
                    Gimnasio
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="gimnasio" name="gimnasio" ${data.gimnasio == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="piscina">
                    Piscina
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="piscina" name="piscina" ${data.piscina == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="areas_verdes">
                    Áreas verdes
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="areas_verdes" name="areas_verdes" ${data.areas_verdes == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="lavanderia">
                    Lavandería Edificio
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="lavanderia" name="lavanderia" ${data.lavanderia == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="atejardin">
                    Antejardín
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="atejardin" name="atejardin" ${data.antejardin == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="quincho">
                    Quincho
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="quincho" name="quincho" ${data.quincho == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="piscina_temp">
                    Piscina temperada
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="piscina_temp" name="piscina_temp" ${data.piscina_temp == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="gourmet">
                    Gourmet room
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="gourmet" name="gourmet" ${data.gourmet_room == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="sala_multiuso">
                    Sala Multiuso
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="sala_multiuso" name="sala_multiuso" ${data.sala_multi_uso == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="juegos_infantiles">
                    Juegos Infantiles
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="juegos_infantiles" name="juegos_infantiles" ${data.juegos == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="cancha_tenis">
                    Cancha de tenis
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="cancha_tenis" name="cancha_tenis" ${data.cancha_tenis == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="panelSolar">
                    Panel solar
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="panelSolar" name="panelSolar" ${data.panelSolar == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>`;
            let titulo = document.getElementById("10.3");
            let exterior = document.getElementById("exterior");
            exterior.innerHTML = exteriorCont;
            titulo.innerHTML = "Paso 3 (10.3/11) Exterior";
        });
}

function aComunesExterior() {
    fetch("../controlador/obtenerDatosForm.php?idForm=" + idForm)
        .then(res => res.json())
        .then(data => {
            // console.log(data.riego);
            var aComunesExterior = `<div class="column col-6 casa">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="riego">
                    Riego Automático
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="riego" name="riego"${data.riego == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="azotea">
                    Azotea habilitada
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="azotea" name="azotea" ${data.azotea == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="gimnasio">
                    Gimnasio
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="gimnasio" name="gimnasio" ${data.gimnasio == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="piscina">
                    Piscina
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="piscina" name="piscina" ${data.piscina == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="areas_verdes">
                    Áreas verdes
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="areas_verdes" name="areas_verdes" ${data.areas_verdes == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="lavanderia">
                    Lavandería Edificio
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="lavanderia" name="lavanderia" ${data.lavanderia == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="atejardin">
                    Antejardín
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="atejardin" name="atejardin" ${data.antejardin == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="quincho">
                    Quincho
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="quincho" name="quincho" ${data.quincho == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="piscina_temp">
                    Piscina temperada
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="piscina_temp" name="piscina_temp" ${data.piscina_temp == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="gourmet">
                    Gourmet room
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="gourmet" name="gourmet" ${data.gourmet_room == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="sala_multiuso">
                    Sala Multiuso
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="sala_multiuso" name="sala_multiuso" ${data.sala_multi_uso == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="juegos_infantiles">
                    Juegos Infantiles
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="juegos_infantiles" name="juegos_infantiles" ${data.juegos == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="cancha_tenis">
                    Cancha de tenis
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="cancha_tenis" name="cancha_tenis" ${data.cancha_tenis == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="panelSolar">
                    Panel solar
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="panelSolar" name="panelSolar" ${data.panelSolar == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="conserjeria">
						Conserjería 24Hrs
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="conserjeria" name="conserjeria" ${data.conserje_24 == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="circuito">
						Circuito cerrado de TV
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="circuito" name="circuito" ${data.circ_tv == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="sala_reuniones">
						Sala de reuniones
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="sala_reuniones" name="sala_reuniones" ${data.sala_reuniones == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6 ">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="est_visita">
						Estac. de Visita
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="est_visita" name="est_visita" ${data.est_visita == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="porton_elec">
						Portón Eléctrico
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="porton_elec" name="porton_elec" ${data.porton_elec == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
    </div>
    <div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="bicicletero">
						Bicicletero
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="bicicletero" name="bicicletero" ${data.bicicletero == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
    </div>
    `;
            let titulo = document.getElementById("10.3");
            let exterior = document.getElementById("exterior");
            exterior.innerHTML = aComunesExterior;
            titulo.innerHTML = "Paso 3 (10.3/11) Exterior y Áreas comunes";

        });
}

function aComunesYExterior() {
    fetch("../controlador/obtenerDatosForm.php?idForm=" + idForm)
        .then(res => res.json())
        .then(data => {
            // console.log(data.riego);
            var exteriorCont = `<div class="column col-6 casa">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="riego">
                    Riego Automático
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="riego" name="riego"${data.riego == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="azotea">
                    Azotea habilitada
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="azotea" name="azotea" ${data.azotea == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="gimnasio">
                    Gimnasio
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="gimnasio" name="gimnasio" ${data.gimnasio == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="piscina">
                    Piscina
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="piscina" name="piscina" ${data.piscina == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="areas_verdes">
                    Áreas verdes
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="areas_verdes" name="areas_verdes" ${data.areas_verdes == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="lavanderia">
                    Lavandería Edificio
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="lavanderia" name="lavanderia" ${data.lavanderia == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="atejardin">
                    Antejardín
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="atejardin" name="atejardin" ${data.antejardin == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="quincho">
                    Quincho
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="quincho" name="quincho" ${data.quincho == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="piscina_temp">
                    Piscina temperada
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="piscina_temp" name="piscina_temp" ${data.piscina_temp == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6 dpto">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="gourmet">
                    Gourmet room
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="gourmet" name="gourmet" ${data.gourmet_room == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="sala_multiuso">
                    Sala Multiuso
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="sala_multiuso" name="sala_multiuso" ${data.sala_multi_uso == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="juegos_infantiles">
                    Juegos Infantiles
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="juegos_infantiles" name="juegos_infantiles" ${data.juegos == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="cancha_tenis">
                    Cancha de tenis
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="cancha_tenis" name="cancha_tenis" ${data.cancha_tenis == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="column col-6">
    <div class="fluid-container">
        <div class="row">
            <div class="column col-6">
                <label for="panelSolar">
                    Panel solar
                </label>
            </div>
            <div class="column col-6">
                <label class="switch">
                    <input type="checkbox" id="panelSolar" name="panelSolar" ${data.panelSolar == 1 ? "checked": ""}>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </div>
    </div>`;
            let titulo = document.getElementById("10.3");
            let exterior = document.getElementById("exterior");
            exterior.innerHTML = exteriorCont;
            titulo.innerHTML = "Paso 3 (10.3/11) Exterior";
        });
    fetch("../controlador/obtenerDatosForm.php?idForm=" + idForm)
        .then(res => res.json())
        .then(data => {
            aComunesCont = `<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="conserjeria">
						Conserjería 24Hrs
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="conserjeria" name="conserjeria" ${data.conserje_24 == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="circuito">
						Circuito cerrado de TV
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="circuito" name="circuito" ${data.circ_tv == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="sala_reuniones">
						Sala de reuniones
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="sala_reuniones" name="sala_reuniones" ${data.sala_reuniones == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6 ">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="est_visita">
						Estac. de Visita
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="est_visita" name="est_visita" ${data.est_visita == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="porton_elec">
						Portón Eléctrico
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="porton_elec" name="porton_elec" ${data.porton_elec == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
    </div>
    <div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="bicicletero">
						Bicicletero
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="bicicletero" name="bicicletero" ${data.bicicletero == 1 ? "checked": ""}>
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
    </div>`;
            let aComunes = document.getElementById("aComunes");
            aComunes.innerHTML = aComunesCont;
        });
}


// Paginación
var paginacion1 = document.getElementById("paginacion1").getElementsByTagName("option");
var paginacion2 = document.getElementById("paginacion2");
var paginacion3 = document.getElementById("paginacion3");
var paginacion4 = document.getElementById("paginacion4");
var paginacion5 = document.getElementById("paginacion5");
var paginacion6 = document.getElementById("paginacion6");
var paginacion7 = document.getElementById("paginacion7");
var paginacion8 = document.getElementById("paginacion8");
var paginacion9 = document.getElementById("paginacion9");
var paginacion10 = document.getElementById("paginacion10");
var paginacion11 = document.getElementById("paginacion11");
var paginacion12 = document.getElementById("paginacion12");
var paginacion13 = document.getElementById("paginacion13");
var paginacion14 = document.getElementById("paginacion14");
var paginacion15 = document.getElementById("paginacion15");
var paginacion16 = document.getElementById("paginacion16");

function desPaginacion(id) {
    for (let i = 0; i < paginacion1.length; i++) {
        if (paginacion1[i].value == id) {
            paginacion1[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion2.length; i++) {
        if (paginacion2[i].value == id) {
            paginacion2[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion3.length; i++) {
        if (paginacion3[i].value == id) {
            paginacion3[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion4.length; i++) {
        if (paginacion4[i].value == id) {
            paginacion4[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion5.length; i++) {
        if (paginacion5[i].value == id) {
            paginacion5[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion6.length; i++) {
        if (paginacion6[i].value == id) {
            paginacion6[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion7.length; i++) {
        if (paginacion7[i].value.value == id) {
            paginacion7[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion8.length; i++) {
        if (paginacion8[i].value == id) {
            paginacion8[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion9.length; i++) {
        if (paginacion9[i].value == id) {
            paginacion9[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion10.length; i++) {
        if (paginacion10[i].value == id) {
            paginacion10[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion11.length; i++) {
        if (paginacion11[i].value == id) {
            paginacion11[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion12.length; i++) {
        if (paginacion12[i].value == id) {
            paginacion12[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion13.length; i++) {
        if (paginacion13[i].value == id) {
            paginacion13[i].disabled = true;

        }
    }
    for (let i = 0; i < paginacion14.length; i++) {
        if (paginacion14[i].value == id) {
            paginacion14[i].disabled = true;

        }
    }
    for (let i = 0; i < paginacion15.length; i++) {
        if (paginacion15[i].value == id) {
            paginacion15[i].disabled = true;
        }
    }
    for (let i = 0; i < paginacion16.length; i++) {
        if (paginacion16[i].value == id) {
            paginacion16[i].disabled = true;
        }
    }

}

function habPaginacion(id) {
    for (let i = 0; i < paginacion1.length; i++) {
        if (paginacion1[i].value == id) {
            paginacion1[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion2.length; i++) {
        if (paginacion2[i].value == id) {
            paginacion2[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion3.length; i++) {
        if (paginacion3[i].value == id) {
            paginacion3[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion4.length; i++) {
        if (paginacion4[i].value == id) {
            paginacion4[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion5.length; i++) {
        if (paginacion5[i].value == id) {
            paginacion5[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion6.length; i++) {
        if (paginacion6[i].value == id) {
            paginacion6[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion7.length; i++) {
        if (paginacion7[i].value == id) {
            paginacion7[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion8.length; i++) {
        if (paginacion8[i].value == id) {
            paginacion8[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion9.length; i++) {
        if (paginacion9[i].value == id) {
            paginacion9[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion10.length; i++) {
        if (paginacion10[i].value == id) {
            paginacion10[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion11.length; i++) {
        if (paginacion11[i].value == id) {
            paginacion11[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion12.length; i++) {
        if (paginacion12[i].value == id) {
            paginacion12[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion13.length; i++) {
        if (paginacion13[i].value == id) {
            paginacion13[i].disabled = false;
        }
    }
    for (let i = 0; i < paginacion14.length; i++) {
        if (paginacion14[i].value == id) {
            paginacion14[i].disabled = false;
        }
    }
    for (var i = 0; i < paginacion15.length; i++) {
        if (paginacion15[i].value == id) {
            paginacion15[i].disabled = false;
        }
    }
    for (i = 0; i < paginacion16.length; i++) {
        if (paginacion16[i].value == id) {
            paginacion16[i].disabled = false;
        }
    }

}


$(document).ready(function() {
    desPaginacion(14);
    desPaginacion(15);
});



//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function() {
    if (animating) return false;
    animating = true;


    current_fs = $(this).parent();
    next_fs = $(this).parent().next();
    if (!depto.checked && !casa.checked) {
        if (next_fs[0].id == "14") {
            next_fs = next_fs.next().next();

        }
    }
    if (!condominio.checked && casa.checked) {
        if (next_fs[0].id == "15") {
            next_fs = next_fs.next();
        }
    }
    if (depto.checked) {
        if (next_fs[0].id == "15") {
            next_fs = next_fs.next();
            paginacion1[14].disabled = true;
        }
    }
    if (casa.checked) {
        if (next_fs[0].id == "5") {
            next_fs = next_fs.next();
        }
    }
    // next_fs = document.getElementById("ste");
    // console.log($(this).parent());
    //activate next step on progressbar using the index of next_fs
    $("#progressBar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    current_fs.removeClass("active");
    next_fs.addClass("active");
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.hide(),
                animating = false,
                current_fs.css({
                    'transform': 'scale()',
                    'position': 'absolute'
                }); // jshint ignore:line
            next_fs.css({ 'left': left, 'opacity': opacity });
        },
        duration: 800,
        complete: function() {

        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});

$(".previous").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    if (!depto.checked && !casa.checked) {
        if (previous_fs[0].id == "15") {
            previous_fs = previous_fs.prev().prev();
        }
    }
    if (!condominio.checked && casa.checked) {
        if (previous_fs[0].id == "15") {
            previous_fs = previous_fs.prev();
        }
    }
    if (depto.checked) {
        if (previous_fs[0].id == "15") {
            previous_fs = previous_fs.prev();
        }
    } else if (!casa.checked && condominio.checked) {
        if (previous_fs[0].id == "15") {
            previous_fs = previous_fs.prev();
        }
    }
    if (casa.checked) {
        if (previous_fs[0].id == "5") {
            previous_fs = previous_fs.prev();
        }
    }
    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    previous_fs.addClass("active");
    current_fs.removeClass("active");
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            // scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            // left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({ 'left': left });
            previous_fs.css({ 'transform': 'scale()', 'opacity': opacity });
            current_fs.hide();
            animating = false;
        },
        duration: 800,
        complete: function() {
            // current_fs.hide();
            // animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});

$(".submit").click(function() {
    return false;
});

// -----------------------------------------------------------------------------------------
function paginacion(id) {

    var select = document.getElementById(`paginacion${id}`).value;

    current_fs = $(`#${id}`);
    form = $('#formC');
    encontrado = form.find(`#${select}`);
    current_fs.removeClass("active");
    encontrado.addClass("active");
    // console.log(encontrado);
    document.getElementById(`paginacion${id}`).value = "0";
    encontrado.show();



    current_fs.animate({ opacity: 0 }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.hide(),
                animating = false,
                current_fs.css({
                    'transform': 'scale()',
                    'position': 'absolute',

                }); // jshint ignore:line
            encontrado.css({ 'left': left, 'opacity': opacity });
        },
        duration: 800,
        complete: function() {

        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

// -----------------------------------------------------------------------------------------

function condominio2() {
    console.log("entro");
    if (casa.checked && condominio.checked) {
        // paginacion1[13].disabled = true;
        console.log("si");
    } else {
        // paginacion1[15].disabled = false;
        console.log("no");
    }

}
var div_bodega = document.getElementById('div_bodega');
var select_bodega = document.getElementById('select_bodega');

function bodegas() {
    div_bodega.innerHTML = '';
    let casa = document.getElementById('casa');
    for (var i = 0; i < select_bodega.value; i++) {
        if (i == 9) {
            div_bodega.innerHTML += `
    <div class="form-group selectBod-container">  
                <label for="">${i+1}</label>
                <select name="bodega${i+1}" id="bodega${i+1}" class="fs-bodSub fs-alterSelect fs-bodSub10">
                    <option value="" disabled="" selected="">Bodega subterráneo</option>
                    <option value="1">Nivel calle</option>
                    <option value="2">-1</option>
                    <option value="3">-2</option>
                    <option value="4">-3</option>
                    <option value="5">-4</option>
                    <option value="6">-5</option>
                    <option value="7">-6</option>
                    <option value="8">No Tiene</option>								
                    <option value="9">Otro</option>
                </select>
                <input class="fs-numBodSub fs-alterSelect" type="text" id="nBod${i+1}" name="nBod${i+1}" placeholder="N°">
                `;
            if (casa.checked) {
                div_bodega.innerHTML += `
                    <hr>
                    </div>`;
            } else {
                div_bodega.innerHTML += `
                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td">Rol</td class="fs-td">
                            <td class="fs-td"><input class="fs-rol1 fs-alterSelect fs-tNumb" type="number" name="rolBod1-${i+1}" id="rolBod1-${i+1}" placeholder="" /></td>
                            <td class="fs-td"> - </td>
                            <td class="fs-td"><input class="fs-rol2 fs-alterSelect fs-tNumb" type="number" name="rolBod2-${i+1}" id="rolBod2-${i+1}" placeholder="" /></td>
                        </tr>
                    </table>
                    <hr>
                </div>`;
            }

        } else {
            div_bodega.innerHTML += `
<div class="form-group selectBod-container">  
                <label for="">${i+1}</label>
                <select name="bodega${i+1}" id="bodega${i+1}" class="fs-bodSub fs-alterSelect">
                    <option value="" disabled="" selected="">Bodega subterráneo</option>
                    <option value="1">Nivel calle</option>
                    <option value="2">-1</option>
                    <option value="3">-2</option>
                    <option value="4">-3</option>
                    <option value="5">-4</option>
                    <option value="6">-5</option>
                    <option value="7">-6</option>
                    <option value="8">No Tiene</option>								
                    <option value="9">Otro</option>
                </select>
                <input class="fs-numBodSub fs-alterSelect" type="text" id="nBod${i+1}" name="nBod${i+1}" placeholder="N°">

                `;
            if (casa.checked) {
                div_bodega.innerHTML += `
                    <hr>
                    </div>`;
            } else {
                div_bodega.innerHTML += `
                    <table class="fs-table">
                        <tr class="fs-tr">
                            <td class="fs-td">Rol</td class="fs-td">
                            <td class="fs-td"><input class="fs-rol1 fs-alterSelect fs-tNumb" type="number" name="rolBod1-${i+1}" id="rolBod1-${i+1}" placeholder="" /></td>
                            <td class="fs-td"> - </td>
                            <td class="fs-td"><input class="fs-rol2 fs-alterSelect fs-tNumb" type="number" name="rolBod2-${i+1}" id="rolBod2-${i+1}" placeholder="" /></td>
                        </tr>
                    </table>
                    <hr>
                </div>`;
            }
        }
    }
}
// ---------------------------------------------------------------------------------------------
var mail = document.getElementById("mail");

function selectMail() {
    var corredor = document.getElementById("corredor");
    var formulario = new FormData();

    formulario.append("corredor", corredor.value);

    var datos = { corredor: corredor.value };
    // console.log(datos);
    fetch('../controlador/obtenerCorreo.php', {
            method: 'POST',
            body: formulario
        })
        .then(res => res.json())
        .then(data => mail.value = data);



}

// -------------------------------------------------------------------------------------------------



function tipo_prop() {
    if (casa.checked) {
        document.getElementById("10.3").innerHTML = "Paso 3 (10.3/11) Exterior";
        let scCantPisosCasa = document.getElementById("scCantPisosCasa");
        let exterior = document.getElementById("exterior");
        let gastoC = document.getElementById("gastoC");
        let gastos_comun = document.getElementById("gastos_comun");
        habPaginacion(14);
        desPaginacion(15);
        desPaginacion(5);
        // exterior.innerHTML=exteriorCont;
        exteriorD();
        depto.disabled = true;
        local.disabled = true;
        bodegaTipoProp.disabled = true;
        if (parcela.checked == true || ofi.checked == true) {
            terreno.disabled = true;
        }
        if (terreno.checked == true || ofi.checked == true) {
            parcela.disabled = true;
        }
        if (terreno.checked == true || parcela.checked == true) {
            ofi.disabled == true;
        }
        CantPisosCasa.style.display = "";
        field5.style.display = "none";
        scCantPisosCasa.className = "select-container";
        // si es solo casa desaparece gastos comunes y que incluyen
        gastoC.style.display = "none";
        gastos_comun.style.display = "none";

        for (var i = 0; i < dpto.length; i++) {
            dpto[i].style.display = "none";
        }

        if (condominio.checked) {
            habPaginacion(15);
            //si es condominio y casa aparece gastos comunes y que incluyen
            gastoC.style.display = "";
            gastos_comun.style.display = "";
        }
        // console.log(dpto)      
    } else if (!casa.checked) {
        for (let i = 0; i < dpto.length; i++) {
            dpto[i].style.display = "";
        }
        // console.log(dpto)
        field5.style.display = "";
        checkButtons();
        CantPisosCasa.style.display = "none";
        desPaginacion(15);
        desPaginacion(14);
        habPaginacion(5);
        scCantPisosCasa.className = "";

        // si es solo casa desaparece gastos comunes y que incluyen
        gastoC.style.display = "";
        gastos_comun.style.display = "";
    }
    checkSwitchCondLot();
}

function condominioF() {
    let gastoC = document.getElementById("gastoC");
    let gastos_comun = document.getElementById("gastos_comun");
    let condominio = document.getElementById("condominio");
    let divCondominio = document.getElementById("divCondominio");
    let loteo = document.getElementById("loteo");
    if (condominio.checked) {
        divCondominio.style.display = "";
    } else {
        divCondominio.style.display = "none";
    }
    if (condominio.checked && casa.checked) {

        // let aComunes = document.getElementById("aComunes");
        // let exterior = document.getElementById("exterior");
        // exteriorD();
        aComunesYExterior();
        // aComunes.innerHTML = aComunesCont;
        habPaginacion(14);
        habPaginacion(15);
        gastoC.style.display = "";
        gastos_comun.style.display = "";

    } else if (casa.checked && !condominio.checked) {
        desPaginacion(15);
        habPaginacion(14);
        gastoC.style.display = "none";
        gastos_comun.style.display = "none";
    }
}

function check_depto() {
    let terreno = document.getElementById("supTerrTd");
    let patio = document.getElementById("supPatio");
    let goce = document.getElementById("usoGoceDiv");
    if (depto.checked) {
        casa.disabled = true;
        terreno.disabled = true;
        parcela.disabled = true;
        local.disabled = true;
        bodegaTipoProp.disabled = true;
        CantPisosCasa.style.display = "none";
        // cuando depto esta check desaparece la opcion "superficie terreno", aparece patio y goce
        terreno.style.display = "none";
        patio.style.display = "";
        goce.style.display = "";
        // ------------------------------------------------------------------------------------
        for (var i = 0; i < casas.length; i++) {
            casas[i].style.display = "none";
        }
        aComunesExterior();
        habPaginacion(14);
        desPaginacion(15);
        // desPaginacion(15);

    } else if (!depto.checked) {
        checkButtons();
        // cuando depto NO esta check aparece la opcion "superficie terreno" y desaparece patio y goce
        terreno.style.display = "";
        patio.style.display = "none";
        goce.style.display = "none";
        // -----------------------------------------------------------------
        for (let i = 0; i < casas.length; i++) {
            casas[i].style.display = "";
        }
        // desPaginacion(14);
        // desPaginacion(15);
    }
    checkSwitchCondLot();
}

function check_terreno() {
    let gastos_comun = document.getElementById("gastos_comun");
    let gastoC = document.getElementById("gastoC");
    if (terreno.checked) {
        depto.disabled = true;
        ofi.disabled = true;
        local.disabled = true;
        bodegaTipoProp.disabled = true;
        parcela.disabled = true;
        gastos_comun.style.display = "none";
        gastoC.style.display = "none";
    } else if (!terreno.checked) {
        checkButtons();
        gastos_comun.style.display = "";
        gastoC.style.display = "";
    }
    checkSwitchCondLot();
}

function oficina() {
    if (ofi.checked) {
        if (casa.checked == true) {
            depto.disabled = true;
        }
        if (depto.checked == true) {
            casa.disabled = true;
        }
        terreno.disabled = true;
        parcela.disabled = true;
        bodegaTipoProp.disabled = true;
        local.disabled = true;
    } else {
        checkButtons();
    }
    checkSwitchCondLot();
}

function local1() {
    if (local.checked) {
        casa.disabled = true;
        depto.disabled = true;
        ofi.disabled = true;
        terreno.disabled = true;
        bodegaTipoProp.disabled = true;
        parcela.disabled = true;
    } else {
        checkButtons();
    }
    checkSwitchCondLot();
}

function bodega() {
    if (bodegaTipoProp.checked) {
        casa.disabled = true;
        depto.disabled = true;
        ofi.disabled = true;
        terreno.disabled = true;
        local.disabled = true;
        parcela.disabled = true;
    } else {
        checkButtons();
    }
    checkSwitchCondLot();
}

function parcela1() {
    if (parcela.checked) {
        depto.disabled = true;
        ofi.disabled = true;
        terreno.disabled = true;
        local.disabled = true;
        bodegaTipoProp.disabled = true;
    } else {
        checkButtons();
    }
    checkSwitchCondLot();
}

function techado() {
    var techado_si = document.getElementById("techado_si");
    var techado_no = document.getElementById("techado_no");
    var techado_no_aplica = document.getElementById("techado_no_aplica");
    if (techado_si.checked || techado_no.checked) {
        techado_no_aplica.disabled = true;
    } else {
        techado_no_aplica.removeAttribute("disabled");
    }
}

function techadoNo() {
    var techado_no = document.getElementById("techado_no");
    var techado_si = document.getElementById("techado_si");
    var techado_no_aplica = document.getElementById("techado_no_aplica");
    if (techado_no.checked || techado_si.checked) {
        techado_no_aplica.disabled = true;
    } else {
        techado_no_aplica.removeAttribute("disabled");
    }
}

function techadoNoAplica() {
    var techado_no = document.getElementById("techado_no");
    var techado_si = document.getElementById("techado_si");
    var techado_no_aplica = document.getElementById("techado_no_aplica");
    if (techado_no_aplica.checked) {
        techado_no.disabled = true;
        techado_si.disabled = true;
    } else {
        techado_no.removeAttribute("disabled");
        techado_si.removeAttribute("disabled");
    }
}

function noExclusividad() {
    let exclusividad = document.getElementById("exclusividad");
    let cantCorredor = document.getElementById("cantCorredor");
    let select_tiempoPublicacion = document.getElementById("select_tiempoPublicacion");
    let scTiempoPublicacion = document.getElementById("scTiempoPublicacion");
    let scCantCorredor = document.getElementById("scCantCorredor");

    if (exclusividad.checked) {
        cantCorredor.style.display = "";
        select_tiempoPublicacion.style.display = "";
        scTiempoPublicacion.className = "select-container";
        scCantCorredor.className = "select-container";
    } else {
        cantCorredor.style.display = "none";
        select_tiempoPublicacion.style.display = "none";
        scTiempoPublicacion.className = "";
        scCantCorredor.className = "";
    }

}

function hipotecaCheck() {
    let hipoteca = document.getElementById("hipoteca");
    let scBanco = document.getElementById("scBanco");
    if (hipoteca.checked) {
        scBanco.style.display = "";
    } else {
        scBanco.style.display = "none";
    }
}

// ----------------------------------------------------------------------------------------------

var select_est = document.getElementById('select_estacionamientos');
var div_est = document.getElementById('estacionamientos');

function estacionamientos() {
    div_est.innerHTML = '';
    let casa = document.getElementById('casa');
    for (var i = 0; i < select_est.value; i++) {

        if (i == 9) {
            div_est.innerHTML += `
    <label for="">${i+1}</label>

    <div class="selectEst-container">
        <select name="est_sub${i+1}" id="est_sub${i+1}" class="fs-estSub fs-alterSelect fs-estSub10">
            <option value="" disabled="" selected="">Subterráneo</option>
            <option value="Nivel Calle">Nivel calle</option>
            <option value="-1">-1</option>
            <option value="-2">-2</option>
            <option value="-3">-3</option>
            <option value="-4">-4</option>
            <option value="-5">-5</option>
            <option value="-6">-6</option>
            <option value="otro">Otro</option>
        </select>
    </div>

    <input class="fs-numEstSub fs-alterSelect" type="text" name="num_est${i+1}" id="num_est${i+1}" placeholder="N°">

    Techado 
    <label class="switch">
        <input type="checkbox" id="Techado${i+1}" name="Techado${i+1}">
        <span class="slider round"></span>
    </label>
    `

            if (depto.checked) {
                div_est.innerHTML += `
		Uso y Goce
	    <label class="switch">
		    <input type="checkbox" id="UsoGoceEst${i+1}" name="UsoGoceEst${i+1}">
		    <span class="slider round"></span>
	    </label>
    `;
            }


            if (casa.checked) {
                div_est.innerHTML += `
        <hr>
    `;

            } else {
                div_est.innerHTML += `
        <table class="fs-table">
        <tr class="fs-tr">
            <td class="fs-td">Rol</td class="fs-td">
            <td class="fs-td"><input class="fs-rol1 fs-alterSelect fs-tNumb" type="number" name="rolEst1-${i+1}" id="rolEst1-${i+1}" placeholder="" /></td>
            <td class="fs-td"> - </td>
            <td class="fs-td"><input class="fs-rol2 fs-alterSelect fs-tNumb" type="number" name="rolEst2-${i+1}" id="rolEst2-${i+1}" placeholder="" /></td>
        </tr>
    </table>
    <hr>
    `;
            }


        } else {
            div_est.innerHTML += `
    <label for="">${i+1}</label>

    <div class="selectEst-container">
        <select name="est_sub${i+1}" id="est_sub${i+1}" class="fs-estSub fs-alterSelect">
            <option value="" disabled="" selected="">Subterráneo</option>
            <option value="Nivel Calle">Nivel calle</option>
            <option value="-1">-1</option>
            <option value="-2">-2</option>
            <option value="-3">-3</option>
            <option value="-4">-4</option>
            <option value="-5">-5</option>
            <option value="-6">-6</option>
            <option value="otro">Otro</option>
        </select>
    </div>

    <input class="fs-numEstSub fs-alterSelect" type="text" name="num_est${i+1}" id="num_est${i+1}" placeholder="N°">

    Techado 
    <label class="switch">
        <input type="checkbox" id="Techado${i+1}" name="Techado${i+1}">
        <span class="slider round"></span>
    </label>
    `

            if (depto.checked) {
                div_est.innerHTML += `
		Uso y Goce
	    <label class="switch">
		    <input type="checkbox" id="UsoGoceEst${i+1}" name="UsoGoceEst${i+1}">
		    <span class="slider round"></span>
	    </label>
    `;
            }

            if (casa.checked) {
                div_est.innerHTML += `
        <hr>
    `;

            } else {
                div_est.innerHTML += `
        <table class="fs-table">
        <tr class="fs-tr">
            <td class="fs-td">Rol</td class="fs-td">
            <td class="fs-td"><input class="fs-rol1 fs-alterSelect fs-tNumb" type="number" name="rolEst1-${i+1}" id="rolEst1-${i+1}" placeholder="" /></td>
            <td class="fs-td"> - </td>
            <td class="fs-td"><input class="fs-rol2 fs-alterSelect fs-tNumb" type="number" name="rolEst2-${i+1}" id="rolEst2-${i+1}" placeholder="" /></td>
        </tr>
    </table>
    <hr>
    `;
            }
        }

    }

}

// ----------------------------------------------------------------------------------------------
var tabla = document.getElementById("banos_aumentar");
var selectBanos = document.getElementById("cant_banos");

function bannos() {
    tabla.innerHTML = "";
    for (var i = 0; i < selectBanos.value; i++) {
        tabla.innerHTML += `<div class="select-container">
<select name="tipo_banno${i+1}" id="tipo_banno${i+1}" class="fs-nEst fs-alterSelect">
    <option value="0" disabled selected>Tipo de Baño</option>
    <option value="1">Visita</option>
    <option value="2">Medio Baño</option>
    <option value="3">Completo</option>
</select>
</div>`;
    }
}
// -------------------------------------------------------------------------------------------------
// guardar sin enviar notificacion

var formulario = document.getElementById("formC");
formulario.addEventListener("submit", function(e) {
    console.log("aqui");
    e.preventDefault();
    var area = document.getElementById("select_area");
    var corredor = document.getElementById("corredor");
    var operario = document.getElementById("operario");
    var nombre = document.getElementById("nombre");
    var rut = document.getElementById("rut");
    var telefono = document.getElementById("telefono");
    var correo = document.getElementById("correo");
    var fecha = document.getElementById("fecha");
    var calle = document.getElementById("calle");
    var numero = document.getElementById("numero");
    var comuna = document.getElementById("comuna");

    var prefijo = document.getElementById("sub_fono");

    if (area.value == "null") {
        // alert("area vacía (Paso 1)");
        area.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Area está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (corredor.value == "null") {
        // alert("Corredor vacío (Paso 1)");
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Corredor está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        corredor.focus();
    } else if (operario.value == "null") {
        // alert("Operario vacío (Paso 1)");
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Operario está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        operario.focus();
    } else if (nombre.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Nombre está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        nombre.focus();
    } else if (apellido.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Apellido está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        apellido.focus();
    } else if (rut.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Rut está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        rut.focus();
    } else if (validarRut2() == false) {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'El rut no es válido',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        rut.focus();
    } else if (origen.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Origen está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        origen.focus();
    } else if (prefijo.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Prefijo no seleccionado',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        prefijo.focus();
    } else if (telefono.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Teléfono está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        telefono.focus();
    } else if (validarTelefono() == false) {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'El largo del teléfono ingresado es incorrecto (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        telefono.focus();
    } else if (correo.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Correo está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        correo.focus();
    } else if (validarCorreo() == false) {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Ingrese una dirección de correo válida (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        correo.focus();
    } else if (calle.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Calle está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        calle.focus();
    } else if (numero.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Número está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        numero.focus();
    } else if (comuna.value == "null") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Comuna está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        comuna.focus();
    } else if (letra.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Letra/Dpto está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        comuna.focus();
    } else {



        var datos = new FormData(formulario);



        // console.log(datos.get('venta'));
        // console.log(datos.get('arriendo'));
        // console.log(datos.get('casa'));
        // console.log(datos.get('depto'));
        // console.log(datos.get('of'));
        // console.log(datos.get('terreno'));
        let source = document.getElementById("source");

        fetch(`../controlador/enviarForm.php`, {
                method: 'POST',
                body: datos
            }).then(res => res.json())
            .then(data => {
                console.log(data.calle);
                console.log("Error:" + data.error);
                if (data.error == null) {
                    Swal.fire({
                        position: 'bottom-end',
                        icon: 'success',
                        title: 'Los datos han sido guardados',
                        showConfirmButton: false,
                        toast: true,
                        timer: 1000
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `error ${data.error}`,
                        footer: '<a href="https://api.whatsapp.com/send?phone=+56959939612&text=error%20en%20formulario:%20' + data.error + '" target="_blank">Enviar error a soporte</a>'
                    })

                }
            });
    }
});

function guardadoAutomatico() {
    let formulario = document.getElementById("formC");
    let datos = new FormData(formulario);
    fetch('../controlador/enviarForm.php?guardar=4', {
            method: 'POST',
            body: datos
        }).then(res => res.json())
        .then(data => {
            console.log(data)
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "100",
                "timeOut": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.success('Guardado Automatico');
        })
}

setInterval('guardadoAutomatico()', 60000);
// -------------------------------------------------------------------------- Salir Y Guardar
document.getElementById("quit").addEventListener('click', function(e) {
    e.preventDefault();
    var area = document.getElementById("select_area");
    var corredor = document.getElementById("corredor");
    var operario = document.getElementById("operario");
    var nombre = document.getElementById("nombre");
    var rut = document.getElementById("rut");
    var telefono = document.getElementById("telefono");
    var correo = document.getElementById("correo");
    var fecha = document.getElementById("fecha");
    var calle = document.getElementById("calle");
    var numero = document.getElementById("numero");
    var comuna = document.getElementById("comuna");

    if (area.value == "null") {
        // alert("area vacía (Paso 1)");
        area.focus();
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Area está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
    } else if (corredor.value == "null") {
        // alert("Corredor vacío (Paso 1)");
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Corredor está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        corredor.focus();
    } else if (operario.value == "null") {
        // alert("Operario vacío (Paso 1)");
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Operario está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        operario.focus();
    } else if (nombre.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Nombre está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        nombre.focus();
    } else if (rut.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Rut está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        rut.focus();
    } else if (telefono.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Teléfono está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        telefono.focus();
    } else if (correo.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Correo está vacío (Paso 2)',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        correo.focus();
    } else if (calle.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Calle está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        calle.focus();
    } else if (numero.value == "") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Número está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        numero.focus();
    } else if (comuna.value == "null") {
        Swal.fire({
            position: 'bottom-end',
            icon: 'warning',
            title: 'Campo Comuna está vacío',
            showConfirmButton: false,
            toast: true,
            timer: 1000
        });
        comuna.focus();
    } else {



        var datos = new FormData(formulario);
        fetch('../controlador/enviarForm.php?guardar=4', {
                method: 'POST',
                body: datos
            })
            .then(res => res.json())
            .then(data => {
                // console.log(data);
                if (data.error == null) {
                    // console.log(data.error);
                    if (data.guardar != 2) {
                        Swal.fire({
                            title: '¿Estas Seguro?',
                            text: "Puedes guardarlo y enviarlo después si te falta información, o enviarlo ahora",
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#28a745',
                            confirmButtonText: 'Enviar',
                            cancelButtonText: 'Guardar',
                            showCloseButton: true,
                            reverseButtons: true,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then((result) => {
                            if (result.value) {
                                fetch('../controlador/enviarNotificacion.php?idForm=' + data.idForm)
                                    .then(res2 => res2.json())
                                    .then(data2 => {
                                        if (data2 == "yes") {
                                            Swal.fire({
                                                title: 'Datos enviados',
                                                text: "Se enviará una notificación para su aprobación",
                                                icon: 'success',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'OK!',
                                                allowOutsideClick: false,
                                                allowEscapeKey: false
                                            }).then((result) => {
                                                if (result.value) {
                                                    // console.log(result.value);
                                                    location.href = "../pages/index.php";
                                                }
                                            });
                                        } else {
                                            Swal.fire({
                                                title: 'Esta notificación ya se envió',
                                                text: "Espere mientras los administadores aprueban su formulario",
                                                icon: 'warning',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'OK!',
                                                allowOutsideClick: false,
                                                allowEscapeKey: false
                                            }).then((result) => {
                                                if (result.value) {
                                                    // console.log(result.value);
                                                    location.href = "https://redflip.net/pages/formulariosEnviados.php";
                                                }
                                            });
                                        }
                                    });

                            } else {
                                Swal.fire({
                                    title: 'Datos guardados',
                                    text: "Formulario almacenado",
                                    icon: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'OK!',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        // console.log(result.value);
                                        // location.href ="https://redflip.net/pages/misFormularios.php";
                                    }
                                });
                            }
                        });

                    } else {
                        Swal.fire({
                            title: 'Datos guardados',
                            text: "Formulario almacenado",
                            icon: 'success',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'OK!',
                            cancelButtonText: 'Volver a Propiedades',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then((result) => {
                            if (result.value) {
                                // console.log(result.value);
                                // location.href ="https://redflip.net/pages/misFormularios.php";
                            } else {
                                location.href = "https://indev9.com/redflip/pages/propVSpropiedad.php";
                            }
                        });
                    }
                }
            });


    }
});
// ---------------------------------------------------------------------------------------------
function checkBanno1(id) {
    var medio = document.getElementById(`medio${id}`);
    var bidet = document.getElementById(`bidet${id}`);
    var completo = document.getElementById(`completo${id}`);

    if (medio.checked) {
        bidet.disabled = true;
        completo.disabled = true;
    } else {
        bidet.removeAttribute("disabled");
        completo.removeAttribute("disabled");
    }

}

function checkBanno2(id) {
    var medio = document.getElementById(`medio${id}`);
    var bidet = document.getElementById(`bidet${id}`);
    var completo = document.getElementById(`completo${id}`);

    if (bidet.checked) {
        medio.disabled = true;
        completo.disabled = true;
    } else {
        medio.removeAttribute("disabled");
        completo.removeAttribute("disabled");
    }

}

function checkBanno3(id) {
    var medio = document.getElementById(`medio${id}`);
    var bidet = document.getElementById(`bidet${id}`);
    var completo = document.getElementById(`completo${id}`);

    if (completo.checked) {
        medio.disabled = true;
        bidet.disabled = true;
    } else {
        medio.removeAttribute("disabled");
        bidet.removeAttribute("disabled");
    }
}

// -----------------------------------------------------------------------------------

function NivelCalle() {

    var nivelCalle = document.getElementById('niv_calle');
    var subterraneo = document.getElementById('subterraneo1');
    var no_tiene = document.getElementById('no_tiene');

    if (nivelCalle.checked) {
        subterraneo.disabled = true;
        no_tiene.disabled = true;
    } else {
        subterraneo.removeAttribute('disabled');
        no_tiene.removeAttribute('disabled');
    }

}

function SubTerraneo() {

    var nivelCalle = document.getElementById('niv_calle');
    var subterraneo = document.getElementById('subterraneo1');
    var no_tiene = document.getElementById('no_tiene');

    if (subterraneo.checked) {
        nivelCalle.disabled = true;
        no_tiene.disabled = true;
    } else {
        nivelCalle.removeAttribute('disabled');
        no_tiene.removeAttribute('disabled');
    }

}

function Notiene() {

    var nivelCalle = document.getElementById('niv_calle');
    var subterraneo = document.getElementById('subterraneo1');
    var no_tiene = document.getElementById('no_tiene');

    if (no_tiene.checked) {
        subterraneo.disabled = true;
        nivelCalle.disabled = true;
    } else {
        subterraneo.removeAttribute('disabled');
        nivelCalle.removeAttribute('disabled');
    }

}

// --------------------------------------------------------------

function Habitacional() {
    var habitacional = document.getElementById('habitacional');
    var comercial = document.getElementById('comercial');

    if (habitacional.checked) {
        comercial.disabled = true;
    } else {
        comercial.removeAttribute("disabled");
    }
}

function Comercial() {
    var habitacional = document.getElementById('habitacional');
    var comercial = document.getElementById('comercial');

    if (comercial.checked) {
        habitacional.disabled = true;
    } else {
        habitacional.removeAttribute("disabled");
    }
}

// --------------------------------------------------------------------------------------
// var area = document.getElementById("select_area");
// if(area.value == ""){
// 	alert("oli")
// }

function nivelCalle1() {
    var nivelCalle = document.getElementById("nivelCalle");
    var noAplica = document.getElementById("noAplica");

    if (nivelCalle.checked) {
        noAplica.disabled = true;
    } else {
        noAplica.removeAttribute("disabled");
    }


}

function subTerraneo() {
    var subterraneo = document.getElementById("subterraneo");
    var noAplica = document.getElementById("noAplica");

    if (subterraneo.checked) {
        noAplica.disabled = true;
    } else {
        noAplica.removeAttribute("disabled");
    }


}

// -----------------------------------------------------------------------------
// Valores de arriendo y venta
function valores() {
    var div_valor = document.getElementById("valor2");
    var valor1 = document.getElementById("valor1");
    var div_Arr = document.getElementById("valorArr");
    var operacion = document.getElementById("select_op");
    var rolProp = document.getElementById("rolProp");
    var valorContri = document.getElementById("contri");
    let select_op = document.getElementById("select_op").value;
    let switch_hipoteca = document.getElementById("switch_hipoteca");
    let hipoteca = document.getElementById("hipoteca");

    if (operacion.value == "3") {
        div_valor.style = "";
        div_valor.children[0].placeholder = "Valor Venta";

        // div_Arr.style = "display:none";
        valor1.style = "";
        valor1.children[0].placeholder = "Valor Arriendo";
        rolProp.style.display = "";
        valorContri.style.display = "";
    } else if (operacion.value == "2") {
        div_valor.style = "display:none";
        valor1.children[0].placeholder = "Valor Arriendo";
        valor1.style = "";
        // div_Arr.style = "";
        rolProp.style.display = "";
        valorContri.style.display = "";
    } else {
        div_valor.style = "display:none";
        valor1.style = "";
        valor1.children[0].placeholder = "Valor Venta";
        // div_Arr.style = "display:none";
        rolProp.style.display = "";
        valorContri.style.display = "";
    }

    if (select_op == 2) {
        valorContri.style = "display: none";
        rolProp.style = "display: none";
        hipoteca.checked = false;
        switch_hipoteca.style = "display: none";
        hipotecaCheck();
    } else {
        valorContri.style.display = "";
        rolProp.style = "";
        switch_hipoteca.style = "";
        hipotecaCheck();
    }
}
// -----------------------------------------------------------------------------
function nDorm() {
    var tabla = document.getElementById("dorm_aumentar");
    var num_dorm = document.getElementById("num_dorm");
    tabla.innerHTML = "";
    for (var i = 0; i < num_dorm.value; i++) {
        tabla.innerHTML += `  <tr>
<td class="fs-td2" rowspan="2" style="text-align:center;">${i+1}</td>
<td class="fs-td2">
    <label class="fs-chkLbl">En suite
        <input type="checkbox" id="suite${i+1}" name="suite${i+1}">
        <span class="checkmark"></span>
    </label>
</td>
<td class="fs-td2">
    <label class="fs-chkLbl">WalkingC.
        <input type="checkbox" id="walking${i+1}" name="walking${i+1}">
        <span class="checkmark"></span>
    </label>
</td>
</tr>
<tr>
<td class="fs-td2">
    <label class="fs-chkLbl">ClosetN.
        <input type="checkbox" id="closet${i+1}" name="closet${i+1}">
        <span class="checkmark"></span>
    </label>
</td>
<td class="fs-td2">
    <label class="fs-chkLbl">Servicio
        <input type="checkbox" id="serv${i+1}" name="serv${i+1}">
        <span class="checkmark"></span>
    </label>
</td>

</tr>
<tr>
<td class="fs-td2" colspan="3">
    <hr>
</td>
</tr>`;
    }
}

$(document).keydown(function(e) {
    e = e || event;

    if (e.altKey && String.fromCharCode(e.keyCode) == 'S') {
        $("#save").click();
        // console.log("yes")
    }

    // if(e.keyCode === 39){
    // 	if(animating) return false;
    // 		animating = true;
    // 	// $(".next").click();	
    // 	var current_fs = $("fieldset.active");
    // 	var next_fs = $("fieldset.active").next();
    // 	if(current_fs.attr("id") == "16"){
    // 		return false;
    // 	} 		
    // 	console.log(next_fs);
    // 	if(casa.checked && !condominio.checked){
    // 		if(next_fs[0].id == "15"){
    // 			next_fs = next_fs.next();
    // 			paginacion1[15].disabled = true;
    // 			console.log("true")
    // 		}
    // 	}
    // 	if(casa.checked){
    // 		if(next_fs[0].id == "5"){
    // 			next_fs = next_fs.next();
    // 		}
    // 	}
    // 	// next_fs = document.getElementById("ste");
    // 	// console.log($(this).parent());
    // 	//activate next step on progressbar using the index of next_fs
    // 	$("#progressBar li").eq($("fieldset").index(next_fs)).addClass("active");

    // 	//show the next fieldset
    // 	next_fs.show();
    // 	current_fs.removeClass("active"); 
    // 	next_fs.addClass("active");
    // 	//hide the current fieldset with style
    // 	current_fs.animate({opacity: 0}, {
    // 		step: function(now, mx) {
    // 			//as the opacity of current_fs reduces to 0 - stored in "now"
    // 			//1. scale current_fs down to 80%
    // 			scale = 1 - (1 - now) * 0.2;
    // 			//2. bring next_fs from the right(50%)
    // 			left = (now * 50)+"%";
    // 			//3. increase opacity of next_fs to 1 as it moves in
    // 			opacity = 1 - now;
    // 			current_fs.hide(),
    // 			animating = false,
    // 			current_fs.css({
    // 		'transform': 'scale('+scale+')',
    // 		'position': 'absolute',

    // 	  });
    // 			next_fs.css({'left': left, 'opacity': opacity});
    // 		}, 
    // 		duration: 800, 
    // 		complete: function(){

    // 		}, 
    // 		//this comes from the custom easing plugin
    // 		easing: 'easeInOutBack'
    // 	});
    // }
    // if(e.keyCode === 37){
    // 	if(animating) return false;
    // 		animating = true;
    // 	var current_fs = $("fieldset.active");
    // 	console.log(current_fs.attr("id"))
    // 	if(current_fs.attr("id") != "1"){
    // 			var previous_fs = $("fieldset.active").prev();

    // 			if(casa.checked && !condominio.checked){
    // 				if(previous_fs[0].id == "15"){
    // 					previous_fs = previous_fs.prev();
    // 				}
    // 			}else if(!casa.checked && condominio.checked){
    // 					if(previous_fs[0].id == "15"){
    // 						previous_fs = previous_fs.prev();
    // 					}		
    // 			}
    // 			if(casa.checked){
    // 				if(previous_fs[0].id == "5"){
    // 					previous_fs = previous_fs.prev();
    // 				}
    // 			}
    // 			//de-activate current step on progressbar
    // 			$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    // 			//show the previous fieldset
    // 			previous_fs.show(); 
    // 			previous_fs.addClass("active");
    // 			current_fs.removeClass("active");
    // 			//hide the current fieldset with style
    // 			current_fs.animate({opacity: 0}, {
    // 				step: function(now, mx) {
    // 					//as the opacity of current_fs reduces to 0 - stored in "now"
    // 					//1. scale previous_fs from 80% to 100%
    // 					scale = 0.8 + (1 - now) * 0.2;
    // 					//2. take current_fs to the right(50%) - from 0%
    // 					left = ((1-now) * 50)+"%";
    // 					//3. increase opacity of previous_fs to 1 as it moves in
    // 					opacity = 1 - now;
    // 					current_fs.css({'left': left});
    // 					previous_fs.css({'transform': 'scale('+1+')', 'opacity': opacity});
    // 					current_fs.hide();
    // 					animating = false;
    // 				}, 
    // 				duration: 800, 
    // 				complete: function(){
    // 					// current_fs.hide();
    // 					// animating = false;
    // 				}, 
    // 				//this comes from the custom easing plugin
    // 				easing: 'easeInOutBack'
    // 			});
    // 	}else{

    // 	}

    // }
});

function agregarContacto(e) {
    if ($("#contactos > div").length >= 3) {
        console.log("no se pue");
        let zelda = document.getElementById("addC");
        zelda.disabled = true;
    } else {
        let contactos = document.getElementById("contactos");
        let contenido = '<li><input class="fs-nomC" type="text" name="nombreC" id="nombreC"placeholder="Nombre*"/><input class="fs-fonoC" type="tel" name="telefonoC" id="telefonoC" placeholder="Teléfono*" /><input class="fs-mailC" type="mail" name="correoC" id="correoC" placeholder="Correo*" /></li><hr class="hrC">';
        let li = document.createElement("li");
        let nombre = document.createElement("input");
        let tel = document.createElement("input");
        let correo = document.createElement("input");
        let hr = document.createElement("hr");
        let divMagico = document.createElement("div");
        // nombre
        nombre.className = "fs-nomC";
        nombre.type = "text";
        console.log($("#contactos > li").length)
            // console.log("dasdsa")
        switch ($("#contactos > li").length) {
            case 0:
                nombre.id = "nombreC1";
                nombre.name = "nombreC1";
                break;
            case 1:
                nombre.id = "nombreC2";
                nombre.name = "nombreC2";
                break;
            case 2:
                nombre.id = "nombreC3";
                nombre.name = "nombreC3";
                break;
        }
        nombre.placeholder = "Nombre*";
        // telefono
        tel.className = "fs-fonoC";
        tel.type = "tel";
        switch ($("#contactos > li").length) {
            case 0:
                tel.id = "telC1";
                tel.name = "telC1";
                break;
            case 1:
                tel.id = "telC2";
                tel.name = "telC2";
                break;
            case 2:
                tel.id = "telC3";
                tel.name = "telC3";
                break;
        }
        tel.placeholder = "Telefono*";
        // correo
        correo.className = "fs-mailC";
        correo.type = "mail";
        switch ($("#contactos > li").length) {
            case 0:
                correo.id = "correoC1";
                correo.name = "correoC1";
                break;
            case 1:
                correo.id = "correoC2";
                correo.name = "correoC2";
                break;
            case 2:
                correo.id = "correoC3";
                correo.name = "correoC3";
                break;
        }
        correo.placeholder = "Correo*";
        // hr
        hr.className = "hrC";

        li.appendChild(nombre);
        li.appendChild(tel);
        li.appendChild(correo);
        divMagico.appendChild(li);
        divMagico.appendChild(hr);
        contactos.appendChild(divMagico);
    }


}

function eliminarContacto(e) {
    let contactos = document.getElementById("contactos");
    let contactos2 = document.getElementById("contactos").getElementsByTagName("li");
    let hijo = document.createElement('li');
    contactos.removeChild(contactos.lastChild);
}

function validarRut(rut) {

    rut = rut.split('.');

    let valor = "";

    for (let i = 0; i < rut.length; i++) {
        valor += rut[i];
    }

    valor = valor.split('-');

    let valor2 = "";

    for (let i = 0; i < valor.length; i++) {
        valor2 += valor[i];
    }

    cuerpo = valor2.slice(0, -1);
    dv = valor2.slice(-1).toUpperCase();

    rut.value = cuerpo + '-' + dv;

    if (cuerpo.length < 7) {
        return 1;
    }

    suma = 0;
    multiplo = 2;

    for (i = 1; i <= cuerpo.length; i++) {
        index = multiplo * valor2.charAt(cuerpo.length - i);
        suma = suma + index;
        if (multiplo < 7) {
            multiplo = multiplo + 1;
        } else {
            multiplo = 2;
        }
    }
    dvEsperado = 11 - (suma % 11);

    if (dvEsperado == 10) {
        dvEsperado = 'K';
    } else if (dvEsperado == 11) {
        dvEsperado = 0;
    }

    if (dvEsperado != dv) {
        return 2;
    };

    formateado = cuerpo + "-" + dv;
    document.getElementById("rut").value = formateado;
    return 0;
}

function validarRut2() {
    let rut = document.getElementById('rut');
    let aux = validarRut(rut.value);

    switch (aux) {
        case 1:
            rut.style = "border-color: red";
            return false;
        case 2:
            rut.style = "border-color: red";
            return false;
        case 0:
            rut.style = "border-color: green";
            return true;
        default:
            rut.style = "border-color: red";
            return false;
    }

}

function validarTelefono() {
    let numero = document.getElementById("telefono").value;
    let prefijo = document.getElementById("sub_fono").value;

    if (prefijo == '') {
        sub_fono.style = "border-color: red";
        return false;

    } else {
        sub_fono.style = "border-color:";
    }
    if (prefijo == '+569' || prefijo == '+568' || prefijo == '+562') {
        if (!(/^\d{8}$/.test(numero))) {
            telefono.style = "border-color: red";
            return false;
        } else {
            telefono.style = "border-color: green";
        }
    }
    if (prefijo == '+56') {
        if (!(/^\d{9}$/.test(numero))) {
            telefono.style = "border-color: red";
            return false;
        } else {
            telefono.style = "border-color: green";
        }
    }
}

function validarCorreo() {
    correo = document.getElementById("correo").value;
    if (!(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/.test(correo))) {
        document.getElementById("correo").style = "border-color: red";
        return false;
    } else {
        document.getElementById("correo").style = "border-color: green";
        return true;
    }
}

//

function validarNombre() {
    let nombre = document.getElementById("nombre").value;
    let nombreCompleto = nombre.split(" ");
    let nombreString = "";
    for (i = 0; i < nombreCompleto.length; i++) {;
        nombreCompleto[i] = nombreCompleto[i].charAt(0).toUpperCase() + nombreCompleto[i].substring(1).toLowerCase();
        if (i == 0) {
            nombreString = nombreString + nombreCompleto[i];
        } else {
            nombreString = nombreString + " " + nombreCompleto[i];
        }
    }
    document.getElementById("nombre").value = nombreString;
}

function validarApellido() {
    let apellido = document.getElementById("apellido").value;
    let apellidoCompleto = apellido.split(" ");
    let apellidoString = "";
    for (i = 0; i < apellidoCompleto.length; i++) {
        apellidoCompleto[i] = apellidoCompleto[i].charAt(0).toUpperCase() + apellidoCompleto[i].substring(1).toLowerCase();
        if (i == 0) {
            apellidoString = apellidoString + apellidoCompleto[i];
        } else {
            apellidoString = apellidoString + " " + apellidoCompleto[i];
        }
    }
    document.getElementById("apellido").value = apellidoString;
}

function validarPrecio(id) {
    let valor = document.getElementById(id).value;
    let total = limpiarNumero(valor);
    if (isNaN(total)) {
        document.getElementById(id).style = "width:75%!important;border-color: red";
        document.getElementById(id).value = "";
        return false;
    } else {
        document.getElementById(id).style = "width:75%!important;border-color: none";
    }

    let numeroFinal = "";
    if (total.length > 3) {
        let j = 0;
        for (i = (total.length - 1); i >= 0; i--) {
            numeroFinal = total.charAt(i) + numeroFinal;
            j++
            if (j == 3 && i != 0) {
                numeroFinal = "." + numeroFinal;
                j = 0;
            }
        }
    } else {
        numeroFinal = valor;
    }
    document.getElementById(id).value = numeroFinal;
}

function limpiarNumero(paramNumero) {
    let numero = paramNumero.trim();
    let noPuntos = numero.split(".");
    numero = "";

    for (i = 0; i < noPuntos.length; i++) {
        numero = numero + noPuntos[i];
    }

    let noGuiones = numero.split("-");
    numero = "";

    for (i = 0; i < noGuiones.length; i++) {
        numero = numero + noGuiones[i];
    }

    let noEuler = numero.split("e");
    numero = "";

    for (i = 0; i < noEuler.length; i++) {
        numero = numero + noEuler[i];
    }

    let noComas = numero.split(",");
    numero = "";

    for (i = 0; i < noComas.length; i++) {
        numero = numero + noComas[i];
    }

    return numero;
}

function validarAno(id) {
    let valor = document.getElementById(id).value;
    if (valor.length != 4) {
        document.getElementById(id).style = "border-color: red";
    } else {
        document.getElementById(id).style = "border-color: none";
    }
}

function checkButtons() {
    if (casa.checked) {
        if (ofi.checked) {
            casa.removeAttribute("disabled");
            ofi.removeAttribute("disabled");
            depto.disabled = true;
            terreno.disabled = true;
            bodegaTipoProp.disabled = true;
            parcela.disabled = true;
            local.disabled = true;
        } else if (terreno.checked) {
            casa.removeAttribute("disabled");
            terreno.removeAttribute("disabled");
            depto.disabled = true;
            ofi.disabled = true;
            bodegaTipoProp.disabled = true;
            parcela.disabled = true;
            local.disabled = true;
        } else if (parcela.checked) {
            casa.removeAttribute("disabled");
            parcela.removeAttribute("disabled");
            depto.disabled = true;
            ofi.disabled = true;
            bodegaTipoProp.disabled = true;
            terreno.disabled = true;
            local.disabled = true;
        } else {
            terreno.removeAttribute("disabled");
            parcela.removeAttribute("disabled");
            ofi.removeAttribute("disabled");
            depto.disabled = true;
            local.disabled = true;
            bodegaTipoProp.disabled = true;
        }

    } else if (depto.checked) {
        if (ofi.checked) {
            depto.removeAttribute("disabled");
            ofi.removeAttribute("disabled");
            casa.disabled = true;
            terreno.disabled = true;
            bodegaTipoProp.disabled = true;
            parcela.disabled = true;
            local.disabled = true;
        } else {
            ofi.removeAttribute("disabled");
            casa.disabled = true;
            terreno.disabled = true;
            local.disabled = true;
            parcela.disabled = true;
            bodegaTipoProp.disabled = true;
        }

    } else if (ofi.checked) {
        casa.removeAttribute("disabled");
        depto.removeAttribute("disabled");
        terreno.disabled = true;
        local.disabled = true;
        parcela.disabled = true;
        bodegaTipoProp.disabled = true;
    } else if (parcela.checked) {
        casa.removeAttribute("disabled");
        depto.disabled = true;
        bodegaTipoProp.disabled = true;
        terreno.disabled = true;
        local.disabled = true;
        ofi.disabled = true;
    } else if (terreno.checked) {
        casa.removeAttribute("disabled");
        depto.disabled = true;
        local.disabled = true;
        ofi.disabled = true;
        bodegaTipoProp.disabled = true;
        parcela.disabled = true;
    } else if (bodegaTipoProp.checked) {
        casa.disabled = true;
        depto.disabled = true;
        terreno.disabled = true;
        local.disabled = true;
        parcela.disabled = true;
        ofi.disabled = true;
    } else if (local.checked) {
        casa.disabled = true;
        depto.disabled = true;
        terreno.disabled = true;
        bodegaTipoProp.disabled = true;
        parcela.disabled = true;
        ofi.disabled = true;
    } else {
        depto.removeAttribute("disabled");
        terreno.removeAttribute("disabled");
        local.removeAttribute("disabled");
        bodegaTipoProp.removeAttribute("disabled");
        parcela.removeAttribute("disabled");
        ofi.removeAttribute("disabled");
        casa.removeAttribute("disabled");
    }
    checkSwitchCondLot();
}

function loteoF() {

}

function checkSwitchCondLot() {
    let switchCond1 = document.getElementById("switchCond1");
    let switchCond2 = document.getElementById("switchCond2");
    let switchCond3 = document.getElementById("switchCond3");
    let switchCond4 = document.getElementById("switchCond4");
    let switchLot1 = document.getElementById("switchLot1");
    let switchLot2 = document.getElementById("switchLot2");
    let switchLot3 = document.getElementById("switchLot3");
    let switchLot4 = document.getElementById("switchLot4");
    if (casa.checked) {
        if (terreno.checked || parcela.checked) {
            switchCond1.style = "display: none";
            switchCond2.style = "display: none";
            switchCond3.style = "display: none";
            switchCond4.style = "display: none";
            switchLot1.style = "display: none";
            switchLot2.style = "display: none";
            switchLot3.style = "display: none";
            switchLot4.style = "display: none";
        } else {
            switchCond1.style = "display: ''";
            switchCond2.style = "display: ''";
            switchCond3.style = "display: ''";
            switchCond4.style = "display: ''";
            switchLot1.style = "display: ''";
            switchLot2.style = "display: ''";
            switchLot3.style = "display: ''";
            switchLot4.style = "display: ''";
        }
    } else if (depto.checked) {
        switchCond1.style = "display: none";
        switchCond2.style = "display: none";
        switchCond3.style = "display: none";
        switchCond4.style = "display: none";
        switchLot1.style = "display: none";
        switchLot2.style = "display: none";
        switchLot3.style = "display: none";
        switchLot4.style = "display: none";
    } else if (ofi.checked) {
        if (casa.checked) {
            switchCond1.style = "display: ''";
            switchCond2.style = "display: ''";
            switchCond3.style = "display: ''";
            switchCond4.style = "display: ''";
            switchLot1.style = "display: ''";
            switchLot2.style = "display: ''";
            switchLot3.style = "display: ''";
            switchLot4.style = "display: ''";
        } else {
            switchCond1.style = "display: none";
            switchCond2.style = "display: none";
            switchCond3.style = "display: none";
            switchCond4.style = "display: none";
            switchLot1.style = "display: none";
            switchLot2.style = "display: none";
            switchLot3.style = "display: none";
            switchLot4.style = "display: none";
        }
    } else if (terreno.checked) {
        switchCond1.style = "display: none";
        switchCond2.style = "display: none";
        switchCond3.style = "display: none";
        switchCond4.style = "display: none";
        switchLot1.style = "display: none";
        switchLot2.style = "display: none";
        switchLot3.style = "display: none";
        switchLot4.style = "display: none";
    } else if (local.checked) {
        switchCond1.style = "display: none";
        switchCond2.style = "display: none";
        switchCond3.style = "display: none";
        switchCond4.style = "display: none";
        switchLot1.style = "display: none";
        switchLot2.style = "display: none";
        switchLot3.style = "display: none";
        switchLot4.style = "display: none";
    } else if (bodegaTipoProp.checked) {
        switchCond1.style = "display: none";
        switchCond2.style = "display: none";
        switchCond3.style = "display: none";
        switchCond4.style = "display: none";
        switchLot1.style = "display: none";
        switchLot2.style = "display: none";
        switchLot3.style = "display: none";
        switchLot4.style = "display: none";
    } else if (parcela.checked) {
        switchCond1.style = "display: none";
        switchCond2.style = "display: none";
        switchCond3.style = "display: none";
        switchCond4.style = "display: none";
        switchLot1.style = "display: none";
        switchLot2.style = "display: none";
        switchLot3.style = "display: none";
        switchLot4.style = "display: none";
    } else {
        switchCond1.style = "display: ''";
        switchCond2.style = "display: ''";
        switchCond3.style = "display: ''";
        switchCond4.style = "display: ''";
        switchLot1.style = "display: ''";
        switchLot2.style = "display: ''";
        switchLot3.style = "display: ''";
        switchLot4.style = "display: ''";
    }
}