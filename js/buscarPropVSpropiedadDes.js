
$(buscar_datos());


function buscar_datos(consulta1,consulta2,consulta3){
    $.ajax({
        url: '../services/buscarPropVSpropiedadDes.php',
        type: 'POST',
        dataType: 'html',
        data: {consulta1: consulta1, consulta2: consulta2, consulta3:consulta3}
    })
    .done(function(respuesta){
       
        $("#datos").html(respuesta);
    })
    .fail(function(){
        console.log("error");
    })
}

function filtrar(consulta1){
    $.ajax({
        url: '../services/buscarPropVSpropiedadDes.php',
        type: 'POST',
        dataType: 'html',
        data: {consulta1: consulta1}
    })
    .done(function(respuesta){
      
        $("#datos").html(respuesta);
    })
    .fail(function(){
        console.log("error");
    })
}

var formulario = document.getElementById("filtro");

formulario.addEventListener('submit', function(e){
e.preventDefault();
var valor1 = document.getElementById("tipo_operacion").value;
var valor2 = document.getElementById("estado").value;
var valor3 = document.getElementById("fase").value;
buscar_datos(valor1,valor2,valor3);
})

$(document).on('keyup', '#search', function(){
    var valor = $(this).val();
    if(valor != ""){
        buscar_datos(valor);
    }else{
        buscar_datos();
    }
})