var formulario = document.getElementById("formulario");

var error = document.getElementById("error");



// var respuesta = document.getElementById("respuesta");



formulario.addEventListener('submit', function(e) {



    e.preventDefault();

    var datos = new FormData(formulario);

    fetch('controlador/comprobar.php', {

        method: 'POST',

        body: datos

    }).then(res => res.json())

    .then(data => {

        console.log(data);

        if (data.resp === "2") {
            if (data.lastP != null) {
                window.location = `${data.lastP}`;
            } else {
                var coo = getCookie("conectado");
                if (coo != "true") {
                    setCookie("conectado", "true");
                }
                window.location = "../redflip/pages/index.php";
                console.log(data);
            }
        } else if (data.resp === "1") {

            error.innerHTML = `</br>

            <div  class="alert alert-danger" role="alert">

               <label>Error: ${data.resp}</label>     

            </div>`;



            console.log('error: " ' + data + ' "');



        } else if (data.resp === "3") {

            error.innerHTML = `</br>

            <div  class="alert alert-danger" role="alert">

               <label>Error: ${data.resp}</label>     

            </div>`;



            console.log('error: " ' + data + ' "');



        } else if (data.resp === "4") {

            error.innerHTML = `</br>

            <div  class="alert alert-danger" role="alert">

               <label>Error: ${data.resp} Usuario o contraseña erróneos</label>     

            </div>`;



            console.log('error: " ' + data + ' "');



        } else if (data.resp === "5") {

            error.innerHTML = `</br>

            <div  class="alert alert-danger" role="alert">

               <label>Error: ${data}, Usuario Desactivado</label>     

            </div>`;

        } else if (data.resp == "corredor") {
            if (data.lastP != null) {
                window.location = `${data.lastP}`;
            } else {
                window.location = "../redflip/pages/index.php";
                console.log(data);
            }

        } else if (data.resp == "propietario") {

            window.location = "../redflip/pages/propietario.php";
            console.log(data);

        }

    });

});

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}