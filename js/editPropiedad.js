var formulario = document.getElementById("formulario");

formulario.addEventListener('submit', function(e){
    e.preventDefault();

    
    
    var datos = new FormData(formulario);
    fetch('../controlador/editarPropiedadSQL.php',{
        method: 'POST',
       
        body: datos
    
    })
    .then(res=>res.json())
    .then(data => {
        console.log(data);
      if(data.error != ""){
        console.log(data.error)
      }else{
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })
         // Alerta para saber si hay que agregar una propiedad al presente propietario
        swalWithBootstrapButtons.fire({
          title: 'Propiedad modificada correctamente!',
          icon: 'success',
          showCancelButton: false,
          reverseButtons: true
        }).then((result) => {
        if (result.value) {
          window.location="../pages/propVSpropiedad.php?pagina=1";
        }});
      }
        
    })
});