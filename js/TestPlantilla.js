			/*jshint esversion: 6 */
			function enviarForm() {
			    let otro = document.getElementsByName("ptradio");
			    let otro2 = document.getElementsByName("prop");
			    let op;
			    let op2;
			    otro.forEach(element => {
			        if (element.checked) {
			            op = element.value;
			        }
			    });
			    otro2.forEach(element => {
			        if (element.checked) {
			            op2 = element.value;
			        }
			    });

			    console.log(op, op2);
			    fetch('../controlador/CrearFormularioPlantillas.php', {

			        }).then(res => res.json())
			        .then(data => {
			            console.log(data);
			            window.location = `ControladorPlantillas.php?op=${op}&prop=${op2}&id=${data.id}&idDirec=${data.idDirec}&idPer=${data.idPer}&idProp=${data.idProp}&idPropiedad=${data.idPropiedad}`;
			            // id='.$idForm."&idPer=".$idPer."&idProp=".$idProp."&idPropiedad=".$idPropiedad."&idDirec=".$idDirec
			        })
			        .catch(error => console.log(error));
			    // 

			}
			var casa = document.getElementById("casa");
			var depto = document.getElementById("depto");
			var terreno = document.getElementById("terreno");
			var dpto = document.getElementsByClassName("dpto");
			var local = document.getElementById("local");
			var bodegaTipoProp = document.getElementById("bodegaTipoProp");
			var parcela = document.getElementById("parcela");
			var ofi = document.getElementById("of");
			var casas = document.getElementsByClassName("casa");
			var field5 = document.getElementById("5");
			var condominio = document.getElementById("condominio");
			var CantPisosCasa = document.getElementById("CantPisosCasa");
			var exteriorCont = `<div class="column col-6 casa">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="riego">
							Riego Automático
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="riego" name="riego">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6 dpto">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="azotea">
							Azotea habilitada
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="azotea" name="azotea">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6 dpto">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="gimnasio">
							Gimnasio
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="gimnasio" name="gimnasio">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="piscina">
							Piscina
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="piscina" name="piscina">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="areas_verdes">
							Áreas verdes
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="areas_verdes" name="areas_verdes">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6 dpto">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="lavanderia">
							Lavandería Edificio
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="lavanderia" name="lavanderia">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="atejardin">
							Antejardín
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="atejardin" name="atejardin">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="quincho">
							Quincho
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="quincho" name="quincho">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="piscina_temp">
							Piscina temperada
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="piscina_temp" name="piscina_temp">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6 dpto">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="gourmet">
							Gourmet room
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="gourmet" name="gourmet">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="sala_multiuso">
							Sala Multiuso
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="sala_multiuso" name="sala_multiuso">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="juegos_infantiles">
							Juegos Infantiles
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="juegos_infantiles" name="juegos_infantiles">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="cancha_tenis">
							Cancha de tenis
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="cancha_tenis" name="cancha_tenis">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="column col-6">
			<div class="fluid-container">
				<div class="row">
					<div class="column col-6">
						<label for="panelSolar">
							Panel solar
						</label>
					</div>
					<div class="column col-6">
						<label class="switch">
							<input type="checkbox" id="panelSolar" name="panelSolar">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>`;
			var aComunesCont = `<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="conserjeria">
						Conserjería 24Hrs
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="conserjeria" name="conserjeria">
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="circuito">
						Circuito cerrado de TV
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="circuito" name="circuito">
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="sala_reuniones">
						Sala de reuniones
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="sala_reuniones" name="sala_reuniones">
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6 ">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="est_visita">
						Estac. de Visita
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="est_visita" name="est_visita">
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="porton_elec">
						Portón Eléctrico
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="porton_elec" name="porton_elec">
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="column col-6">
		<div class="fluid-container">
			<div class="row">
				<div class="column col-6">
					<label for="bicicletero">
						Bicicletero
					</label>
				</div>
				<div class="column col-6">
					<label class="switch">
						<input type="checkbox" id="bicicletero" name="bicicletero">
						<span class="slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>`;




			//jQuery time
			var current_fs, next_fs, previous_fs; //fieldsets
			var left, opacity, scale; //fieldset properties which we will animate
			var animating; //flag to prevent quick multi-click glitches

			$(".next").click(function() {
			    if (animating) return false;
			    animating = true;

			    current_fs = $(this).parent();
			    next_fs = $(this).parent().next();

			    // next_fs = document.getElementById("ste");
			    // console.log($(this).parent());
			    //activate next step on progressbar using the index of next_fs
			    $("#progressBar li").eq($("fieldset").index(next_fs)).addClass("active");

			    //show the next fieldset
			    next_fs.show();
			    current_fs.removeClass("active");
			    next_fs.addClass("active");
			    //hide the current fieldset with style
			    current_fs.animate({ opacity: 0 }, {
			        step: function(now, mx) {
			            //as the opacity of current_fs reduces to 0 - stored in "now"
			            //1. scale current_fs down to 80%
			            scale = 1 - (1 - now) * 0.2;
			            //2. bring next_fs from the right(50%)
			            left = (now * 50) + "%";
			            //3. increase opacity of next_fs to 1 as it moves in
			            opacity = 1 - now;
			            current_fs.hide(),
			                animating = false,
			                current_fs.css({
			                    'transform': 'scale()',
			                    'position': 'absolute',

			                }); // jshint ignore:line
			            next_fs.css({ 'left': left, 'opacity': opacity });
			        },
			        duration: 800,
			        complete: function() {

			        },
			        //this comes from the custom easing plugin
			        easing: 'easeInOutBack'
			    });
			    document.body.scrollTop = 0;
			    document.documentElement.scrollTop = 0;

			});

			$(".previous").click(function() {
			    if (animating) return false;
			    animating = true;

			    current_fs = $(this).parent();
			    previous_fs = $(this).parent().prev();
			    // if(document.getElementById("casa").checked && !document.getElementById("condominio").checked){
			    // 	if(previous_fs[0].id == "10.3"){
			    // 		previous_fs = previous_fs.prev();
			    // 	}
			    // }

			    //de-activate current step on progressbar
			    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

			    //show the previous fieldset
			    previous_fs.show();
			    previous_fs.addClass("active");
			    current_fs.removeClass("active");
			    //hide the current fieldset with style
			    current_fs.animate({ opacity: 0 }, {
			        step: function(now, mx) {
			            //as the opacity of current_fs reduces to 0 - stored in "now"
			            //1. scale previous_fs from 80% to 100%
			            // scale = 0.8 + (1 - now) * 0.2;
			            //2. take current_fs to the right(50%) - from 0%
			            // left = ((1-now) * 50)+"%";
			            //3. increase opacity of previous_fs to 1 as it moves in
			            opacity = 1 - now;
			            current_fs.css({ 'left': left });
			            previous_fs.css({ 'transform': 'scale()', 'opacity': opacity });
			            current_fs.hide();
			            animating = false;
			        },
			        duration: 800,
			        complete: function() {
			            // current_fs.hide();
			            // animating = false;
			        },
			        //this comes from the custom easing plugin
			        easing: 'easeInOutBack'
			    });
			    document.body.scrollTop = 0;
			    document.documentElement.scrollTop = 0;
			});

			$(".submit").click(function() {
			    return false;
			});

			// -----------------------------------------------------------------------------------------
			function paginacion(id) {

			    var select = document.getElementById(`paginacion${id}`).value;

			    current_fs = $(`#${id}`);
			    form = $('#formC');
			    encontrado = form.find(`#${select}`);
			    current_fs.removeClass("active");
			    encontrado.addClass("active");
			    // console.log(encontrado);
			    document.getElementById(`paginacion${id}`).value = "0";
			    encontrado.show();



			    current_fs.animate({ opacity: 0 }, {
			        step: function(now, mx) {
			            //as the opacity of current_fs reduces to 0 - stored in "now"
			            //1. scale current_fs down to 80%
			            scale = 1 - (1 - now);
			            //2. bring next_fs from the right(50%)
			            left = (now * 50) + "%";
			            //3. increase opacity of next_fs to 1 as it moves in
			            opacity = 1 - now;
			            current_fs.hide(),
			                animating = false,
			                current_fs.css({
			                    'transform': 'scale()',
			                    'position': 'absolute',

			                }); // jshint ignore:line
			            encontrado.css({ 'left': left, 'opacity': opacity });
			        },
			        duration: 800,
			        complete: function() {

			        },
			        //this comes from the custom easing plugin
			        easing: 'easeInOutBack'
			    });

			}

			// -----------------------------------------------------------------------------------------

			function condominio2() {
			    console.log("entro");
			    if (casa.checked && condominio.checked) {
			        // paginacion1[13].disabled = true;
			        console.log("si");
			    } else {
			        // paginacion1[15].disabled = false;
			        console.log("no");
			    }

			}
			var div_bodega = document.getElementById('div_bodega');
			var select_bodega = document.getElementById('select_bodega');

			function bodegas() {
			    console.log("hola soy la funcion bodegas");
			    div_bodega.innerHTML = '';
			    let casa = document.getElementById('casa');
			    for (var i = 0; i < select_bodega.value; i++) {
			        if (i == 9) {
			            div_bodega.innerHTML += `
				<div class="form-group selectBod-container">  
                            <label for="">${i+1}</label>
                            <select name="bodega${i+1}" id="bodega${i+1}" class="fs-bodSub fs-alterSelect fs-bodSub10">
                                <option value="" disabled="" selected="">Bodega subterráneo</option>
                                <option value="1">Nivel calle</option>
                                <option value="2">-1</option>
                                <option value="3">-2</option>
                                <option value="4">-3</option>
                                <option value="5">-4</option>
                                <option value="6">-5</option>
                                <option value="7">-6</option>
                                <option value="8">No Tiene</option>								
                                <option value="9">Otro</option>
                            </select>
                            <input class="fs-numBodSub fs-alterSelect" type="text" id="nBod${i+1}" name="nBod${i+1}" placeholder="N°" maxlength="4">
							`;
			            if (casa.checked) {
			                console.log("el if casa.checked: " + casa.checked);
			                div_bodega.innerHTML += `
								<hr>
								</div>`;
			            } else {
			                console.log("el if casa.checked: " + casa.checked);
			                div_bodega.innerHTML += `
								<table class="fs-table">
                                <tr class="fs-tr">
                                    <td class="fs-td">Rol</td class="fs-td">
                                    <td class="fs-td"><input class="fs-rol1 fs-alterSelect fs-tNumb" type="number" name="rolBod1-${i+1}" id="rolBod1-${i+1}" placeholder="" /></td>
                                    <td class="fs-td"> - </td>
                                    <td class="fs-td"><input class="fs-rol2 fs-alterSelect fs-tNumb" type="number" name="rolBod2-${i+1}" id="rolBod2-${i+1}" placeholder="" /></td>
                                </tr>
                            </table>
                            <hr>
                        </div>`;
			            }

			        } else {
			            div_bodega.innerHTML += `
			<div class="form-group selectBod-container">  
                            <label for="">${i+1}</label>
                            <select name="bodega${i+1}" id="bodega${i+1}" class="fs-bodSub fs-alterSelect">
                                <option value="" disabled="" selected="">Bodega subterráneo</option>
                                <option value="1">Nivel calle</option>
                                <option value="2">-1</option>
                                <option value="3">-2</option>
                                <option value="4">-3</option>
                                <option value="5">-4</option>
                                <option value="6">-5</option>
                                <option value="7">-6</option>
                                <option value="8">No Tiene</option>								
                                <option value="9">Otro</option>
                            </select>
                            <input class="fs-numBodSub fs-alterSelect" type="text" id="nBod${i+1}" name="nBod${i+1}" placeholder="N°" maxlength="4">

                            `;
			            if (casa.checked) {
			                console.log("el if casa.checked: " + casa.checked);
			                div_bodega.innerHTML += `
								<hr>
								</div>`;
			            } else {
			                console.log("el if casa.checked: " + casa.checked);
			                div_bodega.innerHTML += `
								<table class="fs-table">
                                <tr class="fs-tr">
                                    <td class="fs-td">Rol</td class="fs-td">
                                    <td class="fs-td"><input class="fs-rol1 fs-alterSelect fs-tNumb" type="number" name="rolBod1-${i+1}" id="rolBod1-${i+1}" placeholder="" /></td>
                                    <td class="fs-td"> - </td>
                                    <td class="fs-td"><input class="fs-rol2 fs-alterSelect fs-tNumb" type="number" name="rolBod2-${i+1}" id="rolBod2-${i+1}" placeholder="" /></td>
                                </tr>
                            </table>
                            <hr>
                        </div>`;
			            }
			        }
			    }
			}
			// ---------------------------------------------------------------------------------------------
			var mail = document.getElementById("mail");

			function selectMail() {
			    var corredor = document.getElementById("corredor");
			    var formulario = new FormData();

			    formulario.append("corredor", corredor.value);

			    var datos = { corredor: corredor.value };
			    // console.log(datos);
			    fetch('../controlador/obtenerCorreo.php', {
			            method: 'POST',
			            body: formulario
			        })
			        .then(res => res.json())
			        .then(data => mail.value = data);



			}

			// -------------------------------------------------------------------------------------------------



			function tipo_prop() {
			    let scCantPisosCasa = document.getElementById("scCantPisosCasa");
			    let gastoC = document.getElementById("gastoC");
			    let gastos_comun = document.getElementById("gastos_comun");
			    if (casa.checked) {
			        document.getElementById("10.3").innerHTML = "Paso 3 (10.3/11) Exterior";
			        let exterior = document.getElementById("exterior");
			        habPaginacion(14);
			        desPaginacion(15);
			        desPaginacion(5);
			        exterior.innerHTML = exteriorCont;
			        depto.disabled = true;
			        local.disabled = true;
			        bodegaTipoProp.disabled = true;
			        if (parcela.checked == true || ofi.checked == true) {
			            terreno.disabled = true;
			        }
			        if (terreno.checked == true || ofi.checked == true) {
			            parcela.disabled = true;
			        }
			        if (terreno.checked == true || parcela.checked == true) {
			            ofi.disabled = true;
			        }


			        CantPisosCasa.style.display = "";
			        field5.style.display = "none";
			        scCantPisosCasa.className = "select-container";
			        // si es solo casa desaparece gastos comunes y que incluyen
			        gastoC.style.display = "none";
			        gastos_comun.style.display = "none";

			        for (var i = 0; i < dpto.length; i++) {
			            dpto[i].style.display = "none";
			        }

			        if (condominio.checked) {
			            habPaginacion(15);
			            document.getElementById("aComunes").innerHTML = aComunesCont;
			            //si es condominio y casa aparece gastos comunes y que incluyen
			            gastoC.style.display = "";
			            gastos_comun.style.display = "";
			        }
			        // console.log(dpto)      
			    } else if (!casa.checked) {
			        for (let i = 0; i < dpto.length; i++) {
			            dpto[i].style.display = "";
			        }
			        // console.log(dpto)
			        field5.style.display = "";
			        checkButtons();
			        CantPisosCasa.style.display = "none";
			        desPaginacion(15);
			        desPaginacion(14);
			        habPaginacion(5);
			        scCantPisosCasa.className = "";

			        // si es solo casa desaparece gastos comunes y que incluyen
			        gastoC.style.display = "";
			        gastos_comun.style.display = "";
			    }
			    checkSwitchCondLot();
			}

			function condominioF() {
			    let gastoC = document.getElementById("gastoC");
			    let gastos_comun = document.getElementById("gastos_comun");
			    let condominio = document.getElementById("condominio");
			    let divCondominio = document.getElementById("divCondominio");
			    let loteo = document.getElementById("loteo");
			    if (condominio.checked) {
			        divCondominio.style.display = "";
			    } else {
			        divCondominio.style.display = "none";
			    }
			    if (condominio.checked && casa.checked) {

			        let aComunes = document.getElementById("aComunes");
			        let exterior = document.getElementById("exterior");
			        exterior.innerHTML = exteriorCont;
			        aComunes.innerHTML = aComunesCont;
			        habPaginacion(14);
			        habPaginacion(15);
			        gastoC.style.display = "";
			        gastos_comun.style.display = "";

			    } else if (casa.checked && !condominio.checked) {
			        desPaginacion(15);
			        habPaginacion(14);
			        gastoC.style.display = "none";
			        gastos_comun.style.display = "none";
			    }
			}

			function check_depto() {
			    let terreno = document.getElementById("terreno");
			    let patio = document.getElementById("supPatTd");
			    let goce = document.getElementById("usoGoceDiv");
			    estacionamientos();
			    if (depto.checked) {
			        casa.disabled = true;
			        terreno.disabled = true;
			        parcela.disabled = true;
			        local.disabled = true;
			        bodegaTipoProp.disabled = true;
			        CantPisosCasa.style.display = "none";
			        // cuando depto esta check desaparece la opcion "superficie terreno", aparece patio y goce
			        terreno.style.display = "none";
			        patio.style.display = "";
			        goce.style.display = "";
			        // ------------------------------------------------------------------------------------
			        for (var i = 0; i < casas.length; i++) {
			            casas[i].style.display = "none";
			        }
			        var exterior = document.getElementById("exterior");
			        exterior.innerHTML = exteriorCont + aComunesCont;
			        habPaginacion(14);
			        desPaginacion(15);
			        var titulo = document.getElementById("10.3");
			        titulo.innerHTML = "Paso 3 (10.3/11) Exterior y Áreas comunes";
			        paginacion1[14].disabled = true;
			        paginacion2[14].disabled = true;
			        paginacion3[14].disabled = true;
			        paginacion4[14].disabled = true;
			        paginacion5[14].disabled = true;
			        paginacion6[14].disabled = true;
			        paginacion7[14].disabled = true;
			        paginacion8[14].disabled = true;
			        paginacion9[14].disabled = true;
			        paginacion10[14].disabled = true;
			        paginacion11[14].disabled = true;
			        paginacion12[14].disabled = true;
			        paginacion13[14].disabled = true;
			        paginacion14[14].disabled = true;
			        paginacion15[14].disabled = true;
			        paginacion16[14].disabled = true;
			        // console.log(casas)
			    } else if (!depto.checked) {
			        checkButtons();
			        // cuando depto NO esta check aparece la opcion "superficie terreno" y desaparece patio y goce
			        terreno.style.display = "";
			        patio.style.display = "none";
			        goce.style.display = "none";
			        // -----------------------------------------------------------------
			        for (var e = 0; e < casas.length; e++) {
			            casas[e].style.display = "";
			        }
			        desPaginacion(14);
			        desPaginacion(15);
			    }
			    checkSwitchCondLot();
			}

			function check_terreno() {
			    let gastos_comun = document.getElementById("gastos_comun");
			    let gastoC = document.getElementById("gastoC");
			    if (terreno.checked) {
			        depto.disabled = true;
			        ofi.disabled = true;
			        local.disabled = true;
			        bodegaTipoProp.disabled = true;
			        parcela.disabled = true;
			        gastos_comun.style.display = "none";
			        gastoC.style.display = "none";
			    } else if (!terreno.checked) {
			        checkButtons();
			        gastos_comun.style.display = "";
			        gastoC.style.display = "";
			    }
			    checkSwitchCondLot();
			}

			function oficina() {
			    if (ofi.checked) {
			        if (casa.checked == true) {
			            depto.disabled = true;
			        }
			        if (depto.checked == true) {
			            casa.disabled = true;
			        }
			        terreno.disabled = true;
			        parcela.disabled = true;
			        bodegaTipoProp.disabled = true;
			        local.disabled = true;
			    } else {
			        checkButtons();
			    }
			    checkSwitchCondLot();
			}

			function local1() {
			    if (local.checked) {
			        casa.disabled = true;
			        depto.disabled = true;
			        ofi.disabled = true;
			        terreno.disabled = true;
			        bodegaTipoProp.disabled = true;
			        parcela.disabled = true;
			    } else {
			        checkButtons();
			    }
			    checkSwitchCondLot();
			}

			function bodega() {
			    if (bodegaTipoProp.checked) {
			        casa.disabled = true;
			        depto.disabled = true;
			        ofi.disabled = true;
			        terreno.disabled = true;
			        local.disabled = true;
			        parcela.disabled = true;
			    } else {
			        checkButtons();
			    }
			    checkSwitchCondLot();
			}

			function parcela1() {
			    if (parcela.checked) {
			        depto.disabled = true;
			        ofi.disabled = true;
			        terreno.disabled = true;
			        local.disabled = true;
			        bodegaTipoProp.disabled = true;
			    } else {
			        checkButtons();
			    }
			    checkSwitchCondLot();
			}

			function techado() {
			    var techado_si = document.getElementById("techado_si");
			    var techado_no = document.getElementById("techado_no");
			    var techado_no_aplica = document.getElementById("techado_no_aplica");
			    if (techado_si.checked || techado_no.checked) {
			        techado_no_aplica.disabled = true;
			    } else {
			        techado_no_aplica.removeAttribute("disabled");
			    }
			}

			function techadoNo() {
			    var techado_no = document.getElementById("techado_no");
			    var techado_si = document.getElementById("techado_si");
			    var techado_no_aplica = document.getElementById("techado_no_aplica");
			    if (techado_no.checked || techado_si.checked) {
			        techado_no_aplica.disabled = true;
			    } else {
			        techado_no_aplica.removeAttribute("disabled");
			    }
			}

			function techadoNoAplica() {
			    var techado_no = document.getElementById("techado_no");
			    var techado_si = document.getElementById("techado_si");
			    var techado_no_aplica = document.getElementById("techado_no_aplica");
			    if (techado_no_aplica.checked) {
			        techado_no.disabled = true;
			        techado_si.disabled = true;
			    } else {
			        techado_no.removeAttribute("disabled");
			        techado_si.removeAttribute("disabled");
			    }
			}

			function noExclusividad() {
			    let exclusividad = document.getElementById("exclusividad");
			    let cantCorredor = document.getElementById("cantCorredor");
			    let select_tiempoPublicacion = document.getElementById("select_tiempoPublicacion");
			    let scTiempoPublicacion = document.getElementById("scTiempoPublicacion");
			    let scCantCorredor = document.getElementById("scCantCorredor");

			    if (exclusividad.checked) {
			        cantCorredor.style.display = "";
			        select_tiempoPublicacion.style.display = "";
			        scTiempoPublicacion.className = "select-container";
			        scCantCorredor.className = "select-container";

			    } else {
			        cantCorredor.style.display = "none";
			        select_tiempoPublicacion.style.display = "none";
			        scTiempoPublicacion.className = "";
			        scCantCorredor.className = "";
			    }

			}

			function hipotecaCheck() {
			    let hipoteca = document.getElementById("hipoteca");
			    let scBanco = document.getElementById("scBanco");
			    if (hipoteca.checked) {
			        scBanco.style.display = "";
			    } else {
			        scBanco.style.display = "none";
			    }
			}

			// ----------------------------------------------------------------------------------------------

			var select_est = document.getElementById('select_estacionamientos');
			var div_est = document.getElementById('estacionamientos');

			function estacionamientos() {
			    div_est.innerHTML = '';
			    let casa = document.getElementById('casa');
			    let depto = document.getElementById('depto');
			    for (var i = 0; i < select_est.value; i++) {

			        if (i == 9) {
			            div_est.innerHTML += `
				<label for="">${i+1}</label>

				<div class="selectEst-container">
					<select name="est_sub${i+1}" id="est_sub${i+1}" class="fs-estSub fs-alterSelect fs-estSub10">
						<option value="" disabled="" selected="">Subterráneo</option>
						<option value="Nivel Calle">Nivel calle</option>
						<option value="-1">-1</option>
						<option value="-2">-2</option>
						<option value="-3">-3</option>
						<option value="-4">-4</option>
						<option value="-5">-5</option>
						<option value="-6">-6</option>
						<option value="otro">Otro</option>
					</select>
				</div>

				<input class="fs-numEstSub fs-alterSelect" type="text" name="num_est${i+1}" id="num_est${i+1}" placeholder="N°" maxlength="4">

				Techado 
				<label class="switch">
					<input type="checkbox" id="Techado${i+1}" name="Techado${i+1}">
					<span class="slider round"></span>
				</label>
				`;

			            if (depto.checked) {
			                div_est.innerHTML += `
					Uso y Goce
					<label class="switch">
						<input type="checkbox" id="UsoGoceEst${i+1}" name="UsoGoceEst${i+1}">
						<span class="slider round"></span>
					</label>
					`;
			            }



			            if (casa.checked) {
			                div_est.innerHTML += `
					<hr>
				`;
			            } else {
			                div_est.innerHTML += `
					<table class="fs-table">
					<tr class="fs-tr">
						<td class="fs-td">Rol</td class="fs-td">
						<td class="fs-td"><input class="fs-rol1 fs-alterSelect fs-tNumb" type="number" name="rolEst1-${i+1}" id="rolEst1-${i+1}" placeholder="" /></td>
						<td class="fs-td"> - </td>
						<td class="fs-td"><input class="fs-rol2 fs-alterSelect fs-tNumb" type="number" name="rolEst2-${i+1}" id="rolEst2-${i+1}" placeholder="" /></td>
					</tr>
				</table>
				<hr>
				`;
			            }

			        } else {
			            div_est.innerHTML += `
				<label for="">${i+1}</label>

				<div class="selectEst-container">
					<select name="est_sub${i+1}" id="est_sub${i+1}" class="fs-estSub fs-alterSelect">
						<option value="" disabled="" selected="">Subterráneo</option>
						<option value="Nivel Calle">Nivel calle</option>
						<option value="-1">-1</option>
						<option value="-2">-2</option>
						<option value="-3">-3</option>
						<option value="-4">-4</option>
						<option value="-5">-5</option>
						<option value="-6">-6</option>
						<option value="otro">Otro</option>
					</select>
				</div>

				<input class="fs-numEstSub fs-alterSelect" type="text" name="num_est${i+1}" id="num_est${i+1}" placeholder="N°" maxlength="4">

				Techado 
				<label class="switch">
					<input type="checkbox" id="Techado${i+1}" name="Techado${i+1}">
					<span class="slider round"></span>
				</label>
				`;
			            if (depto.checked) {
			                div_est.innerHTML += `
					Uso y Goce
					<label class="switch">
						<input type="checkbox" id="UsoGoceEst${i+1}" name="UsoGoceEst${i+1}">
						<span class="slider round"></span>
					</label>
				`;

			            }

			            if (casa.checked) {
			                div_est.innerHTML += `
					<hr>
				`;
			            } else {
			                div_est.innerHTML += `
					<table class="fs-table">
					<tr class="fs-tr">
						<td class="fs-td">Rol</td class="fs-td">
						<td class="fs-td"><input class="fs-rol1 fs-alterSelect fs-tNumb" type="number" name="rolEst1-${i+1}" id="rolEst1-${i+1}" placeholder="" /></td>
						<td class="fs-td"> - </td>
						<td class="fs-td"><input class="fs-rol2 fs-alterSelect fs-tNumb" type="number" name="rolEst2-${i+1}" id="rolEst2-${i+1}" placeholder="" /></td>
					</tr>
				</table>
				<hr>
				`;
			            }
			        }

			    }

			}

			// ----------------------------------------------------------------------------------------------
			var tabla = document.getElementById("banos_aumentar");
			var selectBanos = document.getElementById("cant_banos");

			function bannos() {
			    tabla.innerHTML = "";
			    for (var i = 0; i < selectBanos.value; i++) {
			        tabla.innerHTML += `<div class="select-container">
			<select name="tipo_banno${i+1}" id="tipo_banno${i+1}" class="fs-nEst fs-alterSelect">
				<option value="0" disabled selected>Tipo de Baño</option>
				<option value="1">Visita</option>
				<option value="2">Medio Baño</option>
				<option value="3">Completo</option>
			</select>
		</div>`;
			    }
			}
			// -------------------------------------------------------------------------------------------------
			var formulario = document.getElementById("formC");
			formulario.addEventListener("submit", function(e) {
			    e.preventDefault();
			    var area = document.getElementById("select_area");
			    var corredor = document.getElementById("corredor");
			    var operario = document.getElementById("operario");
			    var nombre = document.getElementById("nombre");
			    var rut = document.getElementById("rut");
			    var telefono = document.getElementById("telefono");
			    var correo = document.getElementById("correo");
			    var fecha = document.getElementById("fecha");
			    var calle = document.getElementById("calle");
			    var numero = document.getElementById("numero");
			    var comuna = document.getElementById("comuna");

			    var prefijo = document.getElementById("sub_fono");

			    if (area.value == "null") {
			        // alert("area vacía (Paso 1)");
			        area.focus();
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Area está vacío',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			    } else if (corredor.value == "null") {
			        // alert("Corredor vacío (Paso 1)");
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Corredor está vacío',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        corredor.focus();
			    } else if (operario.value == "null") {
			        // alert("Operario vacío (Paso 1)");
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Operario está vacío',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        operario.focus();
			    } else if (nombre.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Nombre está vacío (Paso 2)',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        nombre.focus();
			    } else if (apellido.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Apellido está vacío (Paso 2)',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        apellido.focus();
			    } else if (rut.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Rut está vacío (Paso 2)',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        rut.focus();
			    } else if (validarRut2() == false) {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'El rut no es válido',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        rut.focus();
			    } else if (origen.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Origen está vacío (Paso 2)',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        origen.focus();
			    } else if (prefijo.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Prefijo no seleccionado',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        prefijo.focus();
			    } else if (telefono.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Teléfono está vacío (Paso 2)',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        telefono.focus();
			    } else if (validarTelefono() == false) {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'El largo del teléfono ingresado es incorrecto (Paso 2)',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        telefono.focus();
			    } else if (correo.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Correo está vacío (Paso 2)',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        correo.focus();
			    } else if (validarCorreo() == false) {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Ingrese una dirección de correo válida (Paso 2)',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        correo.focus();
			    } else if (calle.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Calle está vacío',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        calle.focus();
			    } else if (numero.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Número está vacío',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        numero.focus();
			    } else if (comuna.value == "null") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Comuna está vacío',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        comuna.focus();
			    } else if (letra.value == "") {
			        Swal.fire({
			            position: 'bottom-end',
			            icon: 'warning',
			            title: 'Campo Letra/Dpto está vacío',
			            showConfirmButton: false,
			            toast: true,
			            timer: 1000
			        });
			        comuna.focus();
			    } else {


			        console.log("holi");
			        var datos = new FormData(formulario);



			        // console.log(datos.get('venta'));
			        // console.log(datos.get('arriendo'));
			        // console.log(datos.get('casa'));
			        // console.log(datos.get('depto'));
			        // console.log(datos.get('of'));
			        // console.log(datos.get('terreno'));

			        fetch('../controlador/enviarForm.php?guardar=1', {
			                method: 'POST',
			                body: datos
			            }).then(res => res.json())
			            .then(data => {
			                console.log(data.calle);
			                console.log("Error:" + data.error);
			                if (data.error == null) {
			                    Swal.fire({
			                        position: 'bottom-end',
			                        icon: 'success',
			                        title: 'Los datos han sido guardados',
			                        showConfirmButton: false,
			                        toast: true,
			                        timer: 1000
			                    });

			                    // fetch('../controlador/enviarNotificacion.php?idForm=' + data.idForm).then(res2=> res2.json())
			                    // .then(data2 => console.log(data2))
			                }
			            });
			    }
			});
			// --------------------------------------------------------------------------------------------- Salir Y Enviar
			document.getElementById("quit").addEventListener('click', function(e) {
			    e.preventDefault();
			    var area = document.getElementById("select_area");
			    var corredor = document.getElementById("corredor");
			    var operario = document.getElementById("operario");
			    var nombre = document.getElementById("nombre");
			    var rut = document.getElementById("rut");
			    var telefono = document.getElementById("telefono");
			    var correo = document.getElementById("correo");
			    var fecha = document.getElementById("fecha");
			    var calle = document.getElementById("calle");
			    var numero = document.getElementById("numero");
			    var comuna = document.getElementById("comuna");

			    var prefijo = document.getElementById("sub_fono");

			});
			// ---------------------------------------------------------------------------------------------