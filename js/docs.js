let form = document.getElementById("ingresoDoc");

form.addEventListener('submit', function(e) {
    e.preventDefault();
    let data = new FormData(form);

    fetch('documentosSubir.php', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(datos => {
            console.log(datos);
            if (datos.respuesta === "2") {
                console.log("la respuesta es positiva");
                Swal.fire({
                        icon: 'success',
                        title: 'Excelente',
                        text: `${datos.ingresoBD}`
                    })
                    .then((result) => {
                        if (result.value) {
                            window.location = "documentosVer.php";
                        }
                    });
            } else if (datos.respuesta === "1") {
                console.log("la respuesta es negativa");
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: `${datos.error}`
                })
            } else if (datos.respuesta === "3") {
                console.log("la respuesta es negativa");
                Swal.fire({
                    icon: 'warning',
                    title: 'Oops...',
                    text: `${datos.error}`
                })
            }
        })
        // .catch(error => console.log('Error: ' + error));
});