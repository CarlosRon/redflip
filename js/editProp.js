validarRut2();
var formulario = document.getElementById("formulario");





formulario.addEventListener('submit', function(e){
  

    e.preventDefault();

    var tel = document.getElementById("tel");
    var err_tel = document.getElementById("error-tel");
	let nombre = document.getElementById("nombre");
	let apellido = document.getElementById("apellido");
	let correo = document.getElementById("correo");


    var datos = new FormData(formulario);

    
    if(tel.value.length < 12){
      err_tel.style = "color:red; font-size:16px";
      err_tel.innerHTML = "El campo debe tener almenos 12 digitos";
      return false;
    }

    
	if(nombre.value == ""){
		Swal.fire({
			position: 'bottom-end',
			icon: 'warning',
			title: 'Campo Nombre está vacío',
			showConfirmButton: false,
			toast: true,
			timer: 1000
		  });
		nombre.focus();
	}else if(apellido.value == ""){
		Swal.fire({
			position: 'bottom-end',
			icon: 'warning',
			title: 'Campo Apellido está vacío',
			showConfirmButton: false,
			toast: true,
			timer: 1000
		  });
		apellido.focus();
	}else if(correo.value == ""){
		Swal.fire({
			position: 'bottom-end',
			icon: 'warning',
			title: 'Campo Correo está vacío',
			showConfirmButton: false,
			toast: true,
			timer: 1000
		  });
		correo.focus();
	}else if(validarCorreo() == false){
		Swal.fire({
			position: 'bottom-end',
			icon: 'warning',
			title: 'Ingrese una dirección de correo válida',
			showConfirmButton: false,
			toast: true,
			timer: 1000
		  });
		correo.focus();
	}else{
		fetch('../controlador/editProp.php',{

			method: 'POST',
	
			body: datos
	
		})
		.then(res=>res.json())
		.then(data => {
        
            Swal.fire(
                'Modificado Correctamente!',
                '',
                'success'
              ).then(result2=>{
                if  (data.error === 0 ){location.href="../pages/PropietariosAct.php";}else{
                  console.log(data.error)
                }
              })

            
        
               }

    )

    


	}

});
   
	

    

    
   





function validarTelefono(){
  let numero = document.getElementById("tel");
  if(numero.value.substring(0, 3) == "+56"){
    console.log("ola");
    if(numero.value.length == 12){
      if(isNaN(numero.value)){
        console.log("son letras");
        numero.style = "border-color: red";
      }else{
        console.log("son numeros");
        numero.style = "border-color: green";
      }
    }else{
      numero.style = "border-color: red";
    }
    
  }else{
    console.log("xao");
    numero.style = "border-color: red";
  }
}

function validarRut(rut){
	
	rut = rut.split('.');

	let valor = "";
	
	for(let i = 0; i<rut.length; i++){
		valor += rut[i];
	}

	valor = valor.split('-');

	let valor2 = "";

	for(let i = 0; i<valor.length; i++){
  		valor2 += valor[i];
	}
	
	cuerpo = valor2.slice(0, -1);
	dv = valor2.slice(-1).toUpperCase();

	rut.value = cuerpo + '-' + dv;

	if (cuerpo.length < 7){
		return 1;
	}

	suma = 0;
	multiplo = 2;

	for (i = 1; i <= cuerpo.length; i++){
		index = multiplo * valor2.charAt(cuerpo.length - i);
		suma = suma + index;
		if(multiplo < 7) {
			multiplo = multiplo + 1;
		}else{
			multiplo = 2;
		}
	}
	dvEsperado = 11 - (suma % 11);

	if (dvEsperado == 10){
		dvEsperado = 'K';
	}else if (dvEsperado == 11){
		dvEsperado = 0;
	}

	if(dvEsperado != dv){
		return 2;
	};

	formateado = cuerpo + "-" + dv;
	document.getElementById("rut").value = formateado;
	return 0;
}

function validarRut2(){
	let rut = document.getElementById('rut');
	let aux = validarRut(rut.value);
	
	switch (aux){
	case 1:	
		rut.style = "border-color: red";
		return false;
	case 2:
		rut.style = "border-color: red";
		return false;
	case 0:
		rut.style = "border-color: green";
		return true;
	default:
		rut.style = "border-color: red";
		return false;
	}

}

function validarNombre(){
	let nombre = document.getElementById("nombre").value;
	let nombreCompleto = nombre.split(" ");
	let nombreString = "";
	for (i=0; i < nombreCompleto.length; i++){;
		nombreCompleto[i] = nombreCompleto[i].charAt(0).toUpperCase() + nombreCompleto[i].substring(1).toLowerCase();
		if (i == 0){
			nombreString = nombreString + nombreCompleto[i];
		}else{
			nombreString = nombreString + " " + nombreCompleto[i];
		}
	}
	document.getElementById("nombre").value = nombreString;
}

function validarApellido(){
	let apellido = document.getElementById("apellido").value;
	let apellidoCompleto = apellido.split(" ");
	let apellidoString = "";
	for (i=0; i < apellidoCompleto.length; i++){
		apellidoCompleto[i] = apellidoCompleto[i].charAt(0).toUpperCase() + apellidoCompleto[i].substring(1).toLowerCase();
		if (i == 0){
			apellidoString = apellidoString + apellidoCompleto[i];
		}else{
			apellidoString = apellidoString + " " + apellidoCompleto[i];
		}
	}
	document.getElementById("apellido").value = apellidoString;
}

function validarCorreo(){
	correo = document.getElementById("correo").value;
		if (!(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/.test(correo))){
			document.getElementById("correo").style = "border-color: red";
			return false;
		} else {
			document.getElementById("correo").style = "border-color: green";
			return true;
		}
}

function validarFecha(){

}